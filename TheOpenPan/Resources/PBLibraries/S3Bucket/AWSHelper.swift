//
//  Awshelper
//
// For reference - https://github.com/maximbilan/Swift-Amazon-S3-Uploading-Tutorial


import UIKit
import Crashlytics //For error logs
import AWSCore
import AWSS3

class AWSHelper: NSObject
{
    var credentialsProvider: AWSCognitoCredentialsProvider!
    var transferManager: AWSS3TransferManager!
    var serviceManager: AWSServiceManager!
    
    class var sharedInstance: AWSHelper {
        struct Singleton {
            static let instance = { () -> AWSHelper in
                let _ins = AWSHelper()
                _ins.configure()
                return _ins
            }
        }
        return Singleton.instance()
    }
    
    func configure() {
        //Initializing S3
        
        credentialsProvider = AWSCognitoCredentialsProvider(regionType: AWSRegionType.APSoutheast1, identityPoolId: S3Info.AWS_POOL_ID.rawValue)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.APSoutheast1, credentialsProvider: credentialsProvider)
        self.serviceManager = AWSServiceManager.default()
        self.serviceManager.defaultServiceConfiguration = configuration
        self.transferManager = AWSS3TransferManager.default()
    }
    
    
//    func upload(fileName: String, fileURL: URL,completion:AWSS3TransferUtilityUploadCompletionHandlerBlock) {
//        let uti = AWSS3TransferUtility.default()
////        uti.uploadFile(fileURL, bucket: S3Info.BUCKET_NAME.rawValue, key: fileName, contentType: "image/png", expression: nil) { (uploadTask, err) in
////            uploadTask.setProgressBlock { (task, progress) in
////
////            }
////        }
//        
//        uti.uploadFile(fileURL, bucket: S3Info.BUCKET_NAME.rawValue, key: fileName, contentType: "image/jpg", expression: nil) { (task, err) in
//            task.setCompletionHandler { (t, e) in
//                print("uploaded")
//                print(fileName)
//            }
//            task.setProgressBlock { (task1, progress) in
//                print(progress)
//                print(task1.key)
//            }
//        }
//    }
    
    
    //MARK: For Uploadrequest Required Bucketname,Filename,URl
    //Description:you have to pass fileName and required url for uploading file.
    func upload(fileName: String, fileURL: URL, response: @escaping (AWSHelperResponse)->())
    {
        //Initializing upload request object
        var uploadRequest: AWSS3TransferManagerUploadRequest?
        uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest!.bucket = S3Info.BUCKET_NAME.rawValue
        uploadRequest!.key = fileName
        uploadRequest!.body = fileURL
        uploadRequest!.acl = AWSS3ObjectCannedACL.publicRead
        
        AWSS3.default().configuration.timeoutIntervalForRequest = 999999
        AWSS3.default().configuration.timeoutIntervalForResource = 999999
        AWSS3.default().configuration.maxRetryCount = 20

        //transfermanager upload request
        
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: {(task: AWSTask) -> AnyObject in
            if let e = task.error
            {
                Crashlytics.sharedInstance().recordError(e)


                response(.error(fileURL: fileURL, error: e))
            }

            if let _ = task.result {
                let liveURL = (AWSS3.default().configuration.endpoint.url).appendingPathComponent((uploadRequest?.bucket!)!).appendingPathComponent((uploadRequest?.key!)!)
                response(.success(localURL: fileURL, liveURL: liveURL))
            }

            return 0 as AnyObject
        })
        
        //uploadprogress status
        uploadRequest?.uploadProgress = {(bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) ->
            Void in
            response(.progress(uploadProgressPercent: CGFloat(totalBytesSent)/CGFloat(totalBytesExpectedToSend) * 100))
        }
    }
    
    class func deleteFile(at url: URL?)
    {
        if url == nil {return}
        
        let s3 = AWSS3.default()
        let deleteObjectRequest = AWSS3DeleteObjectRequest()
        deleteObjectRequest?.bucket = S3Info.BUCKET_NAME.rawValue
        deleteObjectRequest?.key = url?.absoluteString.components(separatedBy: "/").last
        s3.deleteObject(deleteObjectRequest!).continueWith { (task:AWSTask) -> AnyObject? in
            print(s3)
            if let error = task.error {
                Crashlytics.sharedInstance().recordError(error)
                return nil
            }
            
            //SDImageCache.shared().removeImage(forKey: url.absoluteString, withCompletion: nil)
            
            return nil
        }
    }
    
    func downloadFile(fromUrl liveURL : URL, toLocalURL localURL : URL, response: @escaping (AWSHelperResponse)->())
    {
        
        let downloadRequest = AWSS3TransferManagerDownloadRequest()
        downloadRequest!.bucket = S3Info.BUCKET_NAME.rawValue
        downloadRequest!.key = liveURL.lastPathComponent
        
        
        AWSS3.default().configuration.timeoutIntervalForRequest = 999999
        AWSS3.default().configuration.timeoutIntervalForResource = 999999
        AWSS3.default().configuration.maxRetryCount = 20
        
        //transfermanager upload request
        transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: {(task: AWSTask) -> AnyObject in
            if let e = task.error
            {
                Crashlytics.sharedInstance().recordError(e)
                response(.error(fileURL: liveURL, error: e))
            }
            
            if let _ = task.result {
                
                if let output = task.result as? AWSS3TransferManagerDownloadOutput {
                    var outputURL = output.body as? URL
                    if let asString = output.body as? String {
                        outputURL = URL(string: asString)
                    }
                    
                    if outputURL != nil {
                        _ = moveFile(fromUrl: outputURL!, toUrl: localURL)
                    }
                    
                }
                
                response(.success(localURL: localURL, liveURL: liveURL))
            }
            
            return 0 as AnyObject
        })
        
        downloadRequest?.downloadProgress = {(bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) ->
            Void in
            
            response(.progress(uploadProgressPercent: CGFloat(totalBytesWritten)/CGFloat(totalBytesExpectedToWrite) * 100))
        }
    }
}

///S3 bucket information, poolid and bucket name
enum S3Info : String
{
    /* // Ymeet details
    Server IP     :    18.130.182.177
    S3bucket    :     ymeet4u
    PoolID         :     eu-west-2:5b77c8d0-d873-4d36-ac03-8ff3fe17fde7
    Access Key:     AKIAJUGWQL4Z3MLZMDPQ
    Secret Key  :     -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
    */
    
    case AWS_POOL_ID = "ap-southeast-1:47a19b85-0074-464c-bad6-9520f9443b9b"
    case BUCKET_NAME = "myopenpan"

    
}

///Results of addImagesToCustomer
enum AWSHelperResponse {
    case success(localURL: URL, liveURL: URL)
    case error(fileURL: URL, error: Error)
    case progress(uploadProgressPercent: CGFloat)
}
