//
//  ChatConstants.swift
//  PBChatManager
//
//  Created by Suhail Ranger on 30/11/18.
//  Copyright © 2018 Peerbits. All rights reserved.
//

import Foundation

//----- TABLES --------
let USER_MASTER     = "Users"
let THREAD_MASTER   = "Threads"

// ----------- PARAMS ----------------
let CHAT_ID             = "id"
let CHAT_NAME           = "name"
let CHAT_IMAGE          = "image"
let CHAT_ISONLINE       = "isOnline"
let CHAT_LASTSEENTIME   = "lastSeenTime"
let CHAT_DEVICEINFO     = "deviceInfo"
let CHAT_TYPE           = "type"
let CHAT_LASTUPDATETIME = "lastUpdateTime"
let CHAT_OWNER          = "owner"
let CHAT_OPPONENT       = "opponent"
let CHAT_MESSAGE        = "message"
let CHAT_TEXT           = "text"
let CHAT_SENDERID       = "senderId"
let CHAT_SENDERNAME     = "senderName"
let CHAT_SENDERIMAGE    = "senderImage"
let CHAT_ISREAD         = "isRead"
let CHAT_ISACTIVE       = "isActive"
let CHAT_ISDELETED      = "isDeleted"
let CHAT_UNREADCOUNT    = "unreadCount"
let CHAT_OTHERINFO      = "otherInfo"
let CHAT_CREATEDON      = "createdOn"
let CHAT_USERS          = "users"
let CHAT_LASTSEEN       = "lastSeen"
let CHAT_ISTYPING       = "isTyping"
let CHAT_MESSAGES       = "messages"
let CHAT_MEDIAURL       = "mediaUrl"
let CHAT_THUMBNAIL      = "thumbnail"
let CHAT_ISUPLOADED     = "isUploaded"
let CHAT_READTIME       = "readTime"
let CHAT_GROUPINFO      = "groupInfo"
let CHAT_DATETIME       = "datetime"
let CHAT_ADMINID        = "adminId"
let CHAT_ADMINNAME      = "adminName"
let CHAT_ADMINPIC       = "adminPic"
let CHAT_GROUPPIC       = "groupPic"
let CHAT_GROUPNAME      = "groupName"
let CHAT_CLEAR          = "clearChatTime"
