//
//  PBChatUser.swift
//

import Foundation
import FirebaseDatabase

class ChatUser: NSObject {
    
    var id: String = ""
    var image: URL? = nil
    var isOnline: Bool = false
    var lastSeenTime: Int? = nil
    var lastUpdateTime: Int? = nil
    var username: String = ""
    var isTyping: Bool = false // for Thread Master
    var unreadCount: Int = 0
    var otherInfo: [AnyHashable : Any] = [:]
    var readTime : Int? // Var for messages
    var Threads : [Thread] = [] // For list of chats
    
    convenience init(snapshot: DataSnapshot) {
        self.init()
        if let data = snapshot.value as? [AnyHashable : Any] {
            self.updateFrom(dictionary: data)
        }
    }
    
    convenience init(data: [AnyHashable : Any]) {
        self.init()
        self.updateFrom(dictionary: data)
    }
    
    convenience init(id: String, image: URL?, isOnline: Bool, lastSeenTime: Int?, lastUpdateTime: Int?, isTyping: Bool, username: String, otherInfo: [AnyHashable : Any] = [:]) {
        self.init()
        
        self.id = id//id.contains(PBChat.shared.userPrefix) ? id : "\(PBChat.shared.userPrefix)\(id)"
        self.image = image
        self.isOnline = isOnline
        self.lastSeenTime = lastSeenTime
        self.lastUpdateTime = lastUpdateTime
        self.username = username
        self.isTyping = isTyping
        self.otherInfo = otherInfo
    }
    
    func updateFrom(dictionary: [AnyHashable : Any]) {
        
        if let id = dictionary[CHAT_ID] as? String {
            //self.id = id.contains(PBChat.shared.userPrefix) ? id : "\(PBChat.shared.userPrefix)\(id)"
            self.id = id
        }
        if let image = dictionary[CHAT_IMAGE] as? String {
            self.image = URL(string: image)
        }
        if let isOnline = dictionary[CHAT_ISONLINE] as? Bool {
            self.isOnline = isOnline
        }
        if let lastSeenTime = dictionary[CHAT_LASTSEENTIME] as? Int {
            self.lastSeenTime = lastSeenTime
        }
        
        if let lastUpdate = dictionary[CHAT_LASTUPDATETIME] as? Int {
            self.lastUpdateTime = lastUpdate
        }
        
        if let name = dictionary[CHAT_NAME] as? String {
            self.username = name
        }
        
        if let isTyping = dictionary[CHAT_ISTYPING] as? Bool {
            self.isTyping = isTyping
        }
        
        if let unreadCount = dictionary[CHAT_UNREADCOUNT] as? Int {
            self.unreadCount = unreadCount
        }
        
        if let otherInfo = dictionary[CHAT_OTHERINFO] as? [AnyHashable : Any] {
            self.otherInfo = otherInfo
        }

    }
    
    
    
    func getDictionaryForUserMaster() -> [AnyHashable : Any] {
        
        return [
            CHAT_ID : self.id,
            CHAT_IMAGE : self.image?.absoluteString ?? "",
            CHAT_ISONLINE : self.isOnline,
            CHAT_ISTYPING : self.isTyping,
            CHAT_LASTSEENTIME : self.lastSeenTime ?? 0,
            CHAT_LASTUPDATETIME : self.lastUpdateTime ?? 0,
            CHAT_UNREADCOUNT : self.unreadCount.description,
            CHAT_NAME : self.username,
            CHAT_OTHERINFO : self.otherInfo
        ]
    }
    
    func getDictionaryForUserThreads() -> [AnyHashable : Any] {
        
        return [
            CHAT_ID : self.id,
            CHAT_IMAGE : self.image?.absoluteString ?? "",
            CHAT_NAME : self.username,
        ]
    }
    
    
    func getDictionaryForThreadMaster() -> [AnyHashable : Any] {
        
        return [
            CHAT_ID : self.id,
            CHAT_IMAGE : self.image?.absoluteString ?? "",
            CHAT_ISTYPING : self.isTyping,
            CHAT_LASTSEENTIME : self.lastSeenTime ?? 0,
            CHAT_NAME : self.username
        ]
    }
}
