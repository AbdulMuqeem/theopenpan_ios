//
//  PBChatHelper.swift
//  PBChatManager
//
//  Created by Suhail Ranger on 07/12/18.
//  Copyright © 2018 Peerbits. All rights reserved.
//

import Foundation
import ARKit


func getDocumentsDirectory() -> URL? {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths.first
}

func isFileExistAtPath(url : String) -> Bool {
    return FileManager.default.fileExists(atPath: url)
}

func moveFile(fromUrl source : URL, toUrl destination : URL) -> Bool {
    if source.path.isEmpty || destination.path.isEmpty {return false}
    
    do {
        try FileManager.default.moveItem(at: source, to: destination)
        print("File moved successfully")
        return true
    }catch{
        print("1st attempt to move is failed")
        do {
            try FileManager.default.moveItem(atPath: source.path, toPath: destination.path)
            return true
        }catch{
            print("2nd attempt to move is failed")
            
            do {
                print("Trying to copy file")
                try FileManager.default.copyItem(at: source, to: destination)
                return true
            }catch {
                print("copy file is also failed")
                return false
            }
        }
    }
}

func deleteFileAtPath(url : String){
    if url.isEmpty {return}
    
    do {
        try FileManager.default.removeItem(at: URL(string: url)!)
        print("File deleted successfully")
    }catch{
        print("1st attempt to delete is failed")
        do {try FileManager.default.removeItem(atPath: url)}catch{ print("2nd attempt to delete is failed")}
    }
}

func getThumbnailFrom(videoURL url : URL) -> UIImage? {
    let asset = AVAsset(url: url)
    let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
    assetImgGenerate.appliesPreferredTrackTransform = true
    let time = CMTimeMake(value: 1, timescale: 2)
    let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
    if img != nil {
        let frameImg  = UIImage(cgImage: img!)
        return frameImg
    }else{
        return nil
    }
}



func uploadFileToAWS(withUrl url: String, extension ext : String, completion : @escaping ((URL?) -> Void)) {
    AWSHelper.sharedInstance.upload(fileName: "\(Int(Date().timeIntervalSince1970)).\(ext)", fileURL: URL(string: url)!, response: { (res) in
        switch res {
        case .success(_, let liveURL):
            deleteFileAtPath(url: url)
            completion(liveURL)
            
        case .error(_, let error):
            print(error.localizedDescription)
            completion(nil)
            
        case .progress( _):
            break
        }
    })
}



func prepareUrls(fromFile url : URL) -> (filePath : URL, thumbnailPath : URL?, mediaType : MessageType)? {
    if let _url = getDocumentsDirectory() {
        
        let ext = url.pathExtension
        var filePath = _url.appendingPathComponent("\(Int(Date().timeIntervalSince1970))")
        filePath.appendPathExtension(ext)
        
        
        if moveFile(fromUrl: url, toUrl: filePath) {
            
            var mediaType : MessageType = .image
            
            if ext.lowercased().contains("mov") ||
                ext.lowercased().contains("mp4") ||
                ext.lowercased().contains("mpeg") {
                
                mediaType = .video
                
            }else if ext.lowercased().contains("mp3") ||
                ext.lowercased().contains("wav") ||
                ext.lowercased().contains("caf") ||
                ext.lowercased().contains("m4a") {
                
                mediaType = .audio
            }
            
            var thumbURL : URL?
            var thumbImage : UIImage?
            
            if mediaType == .image {
                do {
                    let data = try Data(contentsOf: filePath)
                    if let img = UIImage.init(data: data) {
                        thumbImage = img
                    }
                }catch{}
                
            }else if mediaType == .video {
                if let image = getThumbnailFrom(videoURL: filePath) {
                    thumbImage = image
                }
            }
            
            if thumbImage != nil {
                var thumbData = thumbImage!.jpegData(compressionQuality: 0)
                
                if let resizedImage = thumbImage!.resizedImage(withMinimumSize: CGSize(width: 100, height: 100))
                {
                    if let lowerData = resizedImage.jpegData(compressionQuality: 0) {
                        thumbData = lowerData
                    }
                }
                
                var thumbFilePath = _url.appendingPathComponent("thumb_" + filePath.lastPathComponent)
                thumbFilePath.deletePathExtension()
                thumbFilePath.appendPathExtension("png")
                
                do {
                    try thumbData?.write(to: thumbFilePath)
                    thumbURL = thumbFilePath
                    
                }catch{}
            }
            
            
            print(filePath.absoluteString)
            print(thumbURL?.absoluteString ?? "NO THUMBNAIL")
            
            return (filePath,thumbURL,mediaType)
            
        }else{
            return nil
        }
        
    }else {return nil}
}
