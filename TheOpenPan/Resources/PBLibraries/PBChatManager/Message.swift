//
//  Message.swift
//

import Foundation
import FirebaseDatabase

enum MessageType: String
{
    case text   = "T"
    case image  = "I"
    case audio  = "A"
    case video  = "V"
}

class Message: NSObject
{
    var senderId:           String!
    var datetime:               Int?
    var type:               MessageType!
    var id:        String?
    var isDeleted: Bool = false
    var isDownloaded : Bool = false
    var isRead: Bool = false // For last message in user_master
    var text: String = ""
    var isUploaded: Bool = false
    var otherInfo : [AnyHashable : Any]?
    var readUserList : [ChatUser]?
    var percentUploaded = 0.0
    var percentUploadedClosure: (Float)->() = {_ in}
    
    var thumbnail : URL?
    var localURL : URL?
    var mediaURL: URL?
    
    convenience init(senderId: String, text: String, time: Int?, type: MessageType, mediaURL: URL?, thumbnail: URL?, snapshotKey: String, isUploaded: Bool)
    {
        self.init()
        self.senderId           = senderId
        self.datetime           = time
        self.type               = type
        self.mediaURL           = mediaURL
        self.thumbnail          = thumbnail
        self.id                 = snapshotKey
        self.text               = text
        self.isUploaded         = isUploaded
    }
    
    convenience init(snapshot: DataSnapshot)
    {
        let data = snapshot.value as! [AnyHashable : Any]
        self.init(data: data)
        self.id = snapshot.key
    }
    
    convenience init(data: [AnyHashable : Any])
    {
        self.init()
        
        if let ID = data[CHAT_ID] as? String
        {
            self.id = ID
        }
        
        if let sender = data[CHAT_SENDERID] as? String
        {
            self.senderId = sender
        }
        
        if let time = data[CHAT_DATETIME] as? Int
        {
            self.datetime = time
        }
        
        if let type = data[CHAT_TYPE] as? String
        {
            self.type = MessageType(rawValue: type)
        }
        
        if let mediaURL = data[CHAT_MEDIAURL] as? String
        {
            self.mediaURL = URL(string: mediaURL)
        }
        
        if let text = data[CHAT_TEXT] as? String
        {
            self.text = text
        }
        
        if let isUploaded = data[CHAT_ISUPLOADED] as? Bool
        {
            self.isUploaded = isUploaded
        }
        
        if let thumb = data[CHAT_THUMBNAIL] as? String
        {
            self.thumbnail = URL(string: thumb)
        }
        
        if let other = data[CHAT_OTHERINFO] as? [AnyHashable : Any]
        {
            self.otherInfo = other
        }
        
        if let document = getDocumentsDirectory(), let url = mediaURL {
            let path = document.appendingPathComponent(url.lastPathComponent)
            if isFileExistAtPath(url: path.path) {
                self.localURL = path
                self.isDownloaded = true
            }
        }

    }
    
    func getDictionary() -> [AnyHashable : Any]
    {
        return [
            CHAT_SENDERID             : self.senderId ?? "0",
            CHAT_TEXT                 : self.text,
            CHAT_DATETIME             : self.datetime ?? 0,
            CHAT_TYPE                 : self.type.rawValue,
            CHAT_MEDIAURL             : (self.mediaURL != nil ? self.mediaURL!.absoluteString : ""),
            CHAT_ISUPLOADED           : self.isUploaded,
            CHAT_THUMBNAIL            : self.thumbnail?.absoluteString ?? ""
        ]
    }
    
    func getDictionaryForLastMessage() -> [AnyHashable : Any]
    {
        return [
            CHAT_SENDERID             : self.senderId ?? "0",
            CHAT_TEXT                 : self.text,
            CHAT_TYPE                 : self.type.rawValue,
            CHAT_ISREAD               : self.isRead
        ]
    }
    
}
