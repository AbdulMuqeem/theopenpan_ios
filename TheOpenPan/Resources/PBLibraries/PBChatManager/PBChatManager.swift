//
//  PBChat.swift
//  ymeet
//
//  Created by Suhail Ranger on 10/09/18.
//  Copyright © 2018 peerbits. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase
import Kingfisher

class PBChat: NSObject {

    // MARK: Variables
    static let shared : PBChat = PBChat()

    // base path for tables
    let threadMaster = Database.database().reference().child(THREAD_MASTER)
    let userMaster   = Database.database().reference().child(USER_MASTER)

    // prefix for identification for users in console
    let userPrefix   = "U"
    let groupPrefix  = "G"
    
    // current chat user info
    var currentChatUser : ChatUser?
    
    // MARK: INSTANCE FUNCTIONS
    func logIn(withUser user : ChatUser) {
        
        // Condition for safty purpose because user override will erase all threads data
        // TODO: Set current user as nil before calling login function if logout the user
        if currentChatUser != nil {
            fatalError("CHAT USER IS ALREADY CONFIGURED THIS WILL OVERRIDE ALL USER INFORMATION")
        }
        
        currentChatUser = user
        userMaster.child(user.id).child(CHAT_ID).setValue(user.id)
        userMaster.child(user.id).child(CHAT_NAME).setValue(user.username)
        if user.image != nil {
         userMaster.child(user.id).child(CHAT_IMAGE).setValue(user.image!.absoluteString)
        }
        
        goOnline()
        
    }
    
    // MARK: Function to sent required fields to be online
    func goOnline()
    {
        if currentChatUser != nil {
            let user_id = currentChatUser!.id
            let time = Int(Date().timeIntervalSince1970)
            
            userMaster.child(user_id).child(CHAT_ISONLINE).onDisconnectSetValue(false)
            userMaster.child(user_id).child(CHAT_ISONLINE).setValue(true)
            userMaster.child(user_id).child(CHAT_LASTSEENTIME).onDisconnectSetValue(time)
        }
    }
    
    // MARK: Function to notify other user about last seen time
    func goOffline() {
        if currentChatUser != nil {
            let user_id = currentChatUser!.id
            let time = Int(Date().timeIntervalSince1970)
            userMaster.child(user_id).child(CHAT_ISONLINE).setValue(false)
            userMaster.child(user_id).child(CHAT_LASTSEENTIME).onDisconnectSetValue(time)
        }
    }
    
    func logOut(forDeviceId ID : String?) {
        goOffline()
        if ID != nil {
            removeDeviceId(deviceId: ID!)
        }
        
        currentChatUser = nil
        
    }
    
    // MARK: Function will set device id in databse to send notifications on new message
    // deviceId: fcmToken received from firebase messeging delegate
    func setDeviceId(deviceId : String) {
        if deviceId.isEmpty || currentChatUser == nil {
            return
        }
        
        let user_id = currentChatUser!.id
        let deviceInfo = [CHAT_TYPE: "i", CHAT_ID : deviceId]
        goOnline()
        
        let pathToDevices = userMaster.child(user_id).child(CHAT_DEVICEINFO)
        
        pathToDevices.observeSingleEvent(of: .value) { (snap) in
            var isAvailable = false
            if let Ids = snap.value as? [AnyHashable : Any] {
                for ID in Ids {
                    if let data = ID.value as? [AnyHashable : Any] {
                        if (data[CHAT_ID] as? String) ?? "" == deviceId {
                            isAvailable = true
                        }
                    }
                }
            }
            
            if !isAvailable {
                pathToDevices.childByAutoId().setValue(deviceInfo, withCompletionBlock: { (error, referr) in
                })
            }
        }
    }
    
    // MARK: Function to update profile pic of user
    // TODO: This function will update the pic in user master only for all sent messages and threads need to add code
    func setProfilePic(url : String) {
        checkCurrentUser()
        let user_id = currentChatUser!.id
        userMaster.child(user_id).child(CHAT_IMAGE).setValue(url)
    }
    
    // MARK: Function to remove devide id from user master
    // deviceId: fcmToken received from firebase messeging delegate
    func removeDeviceId(deviceId:String) {
        
        if deviceId.isEmpty {return}
    
        checkCurrentUser()
        let user_id = currentChatUser!.id
        
        userMaster.child(user_id).child(CHAT_DEVICEINFO).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.value != nil {
                if let _data = snapshot.value as? NSDictionary {
                    
                    for key in _data.allKeys {
                        if let device = _data[key] as? NSDictionary {
                            if let _deviceId = device[CHAT_ID] as? String, let _key = key as? String {
                                if _deviceId == deviceId {
                                    self.userMaster.child(user_id).child(CHAT_DEVICEINFO).child(_key).removeValue()
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    
    // MARK: Function to initiate the one to one chat between users
    // opponent: Need other user informatino only current user us available in the class
    func startChat(withUser opponent : ChatUser, completion : @escaping ((Bool, Thread?) -> Void))  {
        
        checkCurrentUser()
        
        let owner = currentChatUser!.id
        let otherUsr = opponent.id
        
        // prepare chat ids to check
        let roomId = owner + "_\(otherUsr)"
        let reverseRoomId = otherUsr + "_\(owner)"
        
        // check that chat is already available or not
        threadMaster
            .child(roomId)
            .observeSingleEvent(of: .value)
            {
                (snapshot) in
                if !snapshot.exists() {
                    
                    // check if reverse chat id is available or not
                    self.threadMaster.child(reverseRoomId).observeSingleEvent(of: .value, with: { (snap) in
                        
                        if !snap.exists() {
                            
                            // create new chat between users
                            let thread = Thread()
                            thread.id = snapshot.key
                            thread.thisUser = self.currentChatUser!
                            thread.otherUser = opponent
                            
                            // set value for chat in DB
                            self.threadMaster.child(snapshot.key).setValue(thread.getDictionary(), withCompletionBlock: { (error, refer) in
                                
                                if error == nil {
                                    // set value for chat in user master for current user
                                    self.userMaster
                                        .child(thread.thisUser.id)
                                        .child(THREAD_MASTER)
                                        .child(snapshot.key)
                                        .setValue(thread.getDictionaryForUserMastr(), withCompletionBlock: { (error, referr) in
                                            
                                            if error == nil {
                                                // set value for chat in user master for opponent user
                                                self.userMaster
                                                    .child(thread.otherUser.id)
                                                    .child(THREAD_MASTER)
                                                    .child(snapshot.key)
                                                    .setValue(thread.getDictionaryForUserMastr(), withCompletionBlock: { (error, referr) in
                                                        if error == nil {
                                                            //All set, call the message button again to navigate to chat screen
                                                            self.startChat(withUser: opponent, completion: { (success, _thread) in
                                                                completion(success,_thread)
                                                            })
                                                        }
                                                        else {
                                                            completion(false, nil)
                                                        }
                                                    })
                                            }
                                            else {
                                                completion(false, nil)
                                            }
                                        })
                                }
                                else {
                                    completion(false, nil)
                                }
                            })
                            
                        }else {
                            completion(true, Thread(snapshot: snap))
                        }
                        
                    })
                    
                    
                }
                else {
                    completion(true, Thread(snapshot: snapshot))
                }
        }
    }
    
    
    // MARK: Function to create group chat
    // ID: Group id
    // groupName: Group Name
    // groupImage: Group Image
    // createdOn: Timestamp of group creation date
    func createGroup(withID ID : String, groupName name:String, groupImage image:String, createdOn date : Int = 0, completion : @escaping ((Bool, Thread?) -> Void)) {
        
        checkCurrentUser()
        let group_id = ID.contains(groupPrefix) ? ID : "\(groupPrefix)\(ID)"

        // Check for group already exist or not
        threadMaster
            .child(group_id)
            .observeSingleEvent(of: .value)
            {
                (snapshot) in
                if !snapshot.exists() {
                    
                    // initialize group details
                    let thread = Thread()
                    thread.id = snapshot.key
                    thread.thisUser = self.currentChatUser!
                    thread.type = .Group
                    thread.createdOn = date
                    thread.otherUser = ChatUser(id: group_id, image: URL(string: image) ?? nil, isOnline: true, lastSeenTime: nil, lastUpdateTime: date, isTyping: false, username: name)
                    thread.groupInfo = groupInfo(adminId: self.currentChatUser!.id, adminImage: self.currentChatUser!.image, adminName: self.currentChatUser!.username, groupImage: URL(string: image) ?? nil, groupName: name, createdOn: date)
                    
                    // set value for group in DB
                    self.threadMaster.child(group_id).setValue(thread.getDictionaryForGroup(), withCompletionBlock: { (error, refer) in
                        
                        if error == nil {
                            // Add admin in group as a first user
                            self.addUsers(inGroup: thread, users: [self.currentChatUser!], completion: { (result) in
                                completion(result, thread)
                            })
                        }
                        else {
                            completion(false, nil)
                        }
                    })
                }
                else {
                    completion(true, Thread(snapshot: snapshot))
                }
        }
    }
    
    // MARK: Function to add users into group
    // chat: Group instance
    // users: List of users to add in group
    func addUsers(inGroup chat : Thread, users: [ChatUser] , completion : @escaping ((Bool) -> Void)) {
        
        let pathToGroupUsers = threadMaster.child(chat.id).child(CHAT_USERS)
        
        for user in users {
            
            // add users information into group in DB
            pathToGroupUsers.child(user.id).setValue(user.getDictionaryForThreadMaster()) { (err, ref) in
                
                // Add group information in user master
                let pathToUserThreads = self.userMaster.child(user.id).child(THREAD_MASTER).child(chat.id)
                pathToUserThreads.setValue(chat.getDictionaryForUserMastr())
                
                if user.id == users.last?.id && err == nil {
                    completion(true)
                }
            }
        }
    }
    
    // MARK: Function to remove user from the group
    // ID: Group Id
    // user: User Id
    func deleteUserFromGroup(groupId ID: String, forUserIds user: String , completion : @escaping (Bool) -> Void) {
        
        let group_id = ID.contains(groupPrefix) ? ID : "\(groupPrefix)\(ID)"
        let user_id = user.contains(userPrefix) ? user : "\(userPrefix)\(user)"
        
        let pathForThread = self.threadMaster.child(group_id).child(CHAT_USERS).child(user_id)
        let pathForUser  = self.userMaster.child(THREAD_MASTER).child(group_id)

        // Check that group is exist in user's chats
        pathForUser.observeSingleEvent(of: .value) { (snap) in
            if snap.exists() {
                // remove group from user master
                snap.ref.removeValue(completionBlock: { (err, _) in
                    if err == nil {
                        
                        // Check that user is exist in group's users
                        pathForThread.observeSingleEvent(of: .value) { (snapshot) in
                            if snapshot.exists() {
                                // remove usre from chat master
                                snapshot.ref.removeValue(completionBlock: { (error, _) in
                                    completion(error == nil)
                                })
                            }
                        }
                    }else {
                        completion(false)
                    }
                })
            }else {
                completion(true)
            }
        }
    }
    
    // MARK: Function to chat typing observer in chat
    func startTypingStatusObserver(onChat chat : Thread, completion : @escaping (ChatUser) -> Void) {
        checkCurrentUser()
        let pathToUsers = self.threadMaster.child(chat.id).child(CHAT_USERS)

        pathToUsers.observe(.childChanged) { (snapshot) in
            if snapshot.exists() {
                if let userData = snapshot.value as? [AnyHashable : Any] {
                    let user = ChatUser(data: userData)
                    if user.id != self.currentChatUser!.id {
                        completion(user)
                    }
                }
            }
        }
    }
    
    
    // MARK: Function to set typing status in chat
    func setTypingStatus(inChat chat : Thread, status : Bool) {
        
        checkCurrentUser()
        
        let pathToUsers = self.threadMaster.child(chat.id).child(CHAT_USERS)
        
        pathToUsers.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                if let users = snapshot.value as? [AnyHashable : Any] {
                    for userData in users {
                        if let data = userData.value as? [AnyHashable : Any] {
                            let user = ChatUser(data: data)
                            if user.id == self.currentChatUser!.id {
                                user.isTyping = status
                                pathToUsers.child(user.id).setValue(user.getDictionaryForThreadMaster())
                                return
                            }
                        }
                    }
                }
            }
        }
    }
    
    // MARK: Function to reset the unread count in chat
    func resetUnreadCount(inChat chat: Thread) {
        checkCurrentUser()
        
        let pathToCount = self.userMaster.child(currentChatUser!.id).child(THREAD_MASTER).child(chat.id).child(CHAT_UNREADCOUNT)
        
        let pathToLastMessageIsRead = self.userMaster.child(currentChatUser!.id).child(THREAD_MASTER).child(chat.id).child(CHAT_MESSAGE).child(CHAT_ISREAD)
        
        pathToCount.setValue(0)
        pathToLastMessageIsRead.setValue(true)
        
    }
    
    // MARK: Function to delete chats from the chat master
    func deleteChatFromList(withId ID: String, completion : @escaping (Bool) -> Void) {
        
        checkCurrentUser()

        let user_id = self.currentChatUser!.id
        let pathToUserThread = self.userMaster.child(user_id).child(THREAD_MASTER).child(ID).child(CHAT_ISDELETED)
        
        pathToUserThread.observeSingleEvent(of: .value) { (snap) in
            if snap.exists() {
                pathToUserThread.setValue(true, withCompletionBlock: { (err, _) in
                    completion(err == nil)
                })
            }
        }
    }
    
    // MARK: Function to increase the unread count for opponents
    // chat: Chat instance
    // message: Message instance to set as last message in users chats
    // time: Time for the update in chat
    func increaseUsersUnreadCount(inChat chat: Thread, withMessage message : [AnyHashable : Any], time : Int) {

        checkCurrentUser()
        let pathToUsers = self.threadMaster.child(chat.id).child(CHAT_USERS)
        
        // get all users in chat
        pathToUsers.observeSingleEvent(of: .value) { (snap) in
            if snap.exists() {
                
                if let users = snap.value as? [AnyHashable : Any] {
                    
                    // loop for the users
                    for userData in users {
                        
                        if let data = userData.value as? [AnyHashable : Any] {
                            
                            let user = ChatUser(data: data)
                            
                            let basePathToUpdate = self.userMaster.child(user.id).child(THREAD_MASTER).child(chat.id)
                            
                            // Update the value in DB
                            var lastMessage = message
                            lastMessage[CHAT_ISREAD] = (user.id == self.currentChatUser!.id)
                            basePathToUpdate.child(CHAT_MESSAGE).setValue(lastMessage)
                            basePathToUpdate.child(CHAT_LASTUPDATETIME).setValue(time)
                            
                            // condition to prevent increase unread count for own message
                            if user.id != self.currentChatUser?.id {
                               // Update the previous unread count + 1
                            basePathToUpdate.child(CHAT_UNREADCOUNT).runTransactionBlock({ (currentValue) -> TransactionResult in
                                    
                                    if let count = currentValue.value as? Int {
                                        currentValue.value = count + 1
                                        return TransactionResult.success(withValue: currentValue)
                                    }else {
                                        currentValue.value = 1
                                    }
                                    
                                    return TransactionResult.success(withValue: currentValue)
                                }){ (error, committed, snapshot) in
                                    if let error = error {
                                        print(error.localizedDescription)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    // MARK: Function to Active/InActive chat
    func setStatus(fromChat chat: Thread, isActive status:Bool, forAllUsers : Bool) {
        checkCurrentUser()
        
        if forAllUsers {
           
            let pathToUsers = self.threadMaster.child(chat.id).child(CHAT_USERS)
            
            pathToUsers.observeSingleEvent(of: .value) { (snapshot) in
                if snapshot.exists() {
                    if let users = snapshot.value as? [AnyHashable : Any] {
                        for userData in users {
                            if let data = userData.value as? [AnyHashable : Any] {
                                let user = ChatUser(data: data)
                                let path = self.userMaster.child(user.id).child(THREAD_MASTER).child(chat.id)
                                path.observeSingleEvent(of: .value) { (snapshot) in
                                    if snapshot.exists() {
                                        path.child(CHAT_ISACTIVE).setValue(status)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }else {
         
            let pathToThisUser = self.userMaster.child(currentChatUser!.id).child(THREAD_MASTER)
            
            pathToThisUser.child(chat.id).observeSingleEvent(of: .value) { (snapshot) in
                if snapshot.exists() {
                    pathToThisUser.child(chat.id).child(CHAT_ISACTIVE).setValue(status)
                }
            }
        }
    }
    
    // MARK: Function to observe status of the chat
    func observeStatus(forChat chat: Thread, completion : @escaping (Bool) -> Void) {
        
        checkCurrentUser()
        
        let pathToChat = self.userMaster.child(currentChatUser!.id).child(THREAD_MASTER).child(chat.id)
        pathToChat.child(CHAT_ISACTIVE).observe(.value) { (snap) in
            if snap.value != nil {
                if let isActive = snap.value as? Bool {
                    completion(isActive)
                }
            }
        }
    }
    
    // MARK: Remove observer of the chat
    func removeStatusObserver(forChat chat: Thread) {
        checkCurrentUser()
        let pathToChat = self.userMaster.child(currentChatUser!.id).child(THREAD_MASTER).child(chat.id)
        pathToChat.child(CHAT_ISACTIVE).removeAllObservers()
    }
    
    // MARK: Function to get list of user in chat
    func getUsersList(forChat chat : Thread, completion : @escaping ([ChatUser]) -> Void) {
        
        let pathToUsers = self.threadMaster.child(chat.id).child(CHAT_USERS)
        
        pathToUsers.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.value != nil {
                if let users = snapshot.value as? [AnyHashable : Any] {
                    var arrUsers = [ChatUser]()
                    for userData in users {
                        if let data = userData.value as? [AnyHashable : Any] {
                         arrUsers.append(ChatUser(data: data))
                        }
                    }
                    completion(arrUsers)
                    
                }else{ completion([]) }
            }else{
                completion([])
            }
        }
    }
    
    
    // MARK: Function to get list of user in chat in dictionary format
    func getUsersDictionary(forChat chat : Thread, completion : @escaping ([String : ChatUser]) -> Void) {
        
        let pathToUsers = self.threadMaster.child(chat.id).child(CHAT_USERS)
        
        pathToUsers.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.value != nil {
                if let users = snapshot.value as? [AnyHashable : Any] {
                    
                    var dictUsers = [String : ChatUser]()
                    for userData in users {
                        if let data = userData.value as? [AnyHashable : Any] {
                            let chatUser = ChatUser(data: data)
                            dictUsers[chatUser.id] = chatUser
                        }
                    }
                    
                    completion(dictUsers)
                    
                }else{ completion([:]) }
            }else{
                completion([:])
            }
        }
    }
    
    // MARK: function to set value for timestemp
    func clearChat(forChat chat : Thread, fromTimestemp timestemp: Int, completion : @escaping (Bool) -> Void) {
        
        checkCurrentUser()
        let pathToUserChat  = self.userMaster.child(currentChatUser!.id).child(THREAD_MASTER).child(chat.id)
        let pathToChat      = self.threadMaster.child(chat.id)
        
        pathToUserChat.observeSingleEvent(of: .value) { (snap) in
            if snap.exists() {
                pathToUserChat.child(CHAT_CLEAR).setValue(timestemp)
            }
        }
        
        pathToChat.observeSingleEvent(of: .value) { (snap) in
            if snap.exists() {
                pathToChat.child(CHAT_CLEAR).setValue(timestemp, withCompletionBlock: { (err, _) in
                    completion(err == nil)
                })
            }else{
                completion(false)
            }
        }
        
    }
    
}

// MARK: OPERATIONS
extension PBChat {
    
    // MARK: Funtion to get user details from user master
    // ID: User ID
    func getUserWIthID(userID ID : String, completion : @escaping (ChatUser?) -> Void) {
        if ID.trim().isEmpty {return}
        let user_id = ID.first == userPrefix ? ID : "\(userPrefix)\(ID)"
        self.userMaster.child(user_id).observeSingleEvent(of: .value) { (snap) in
            if snap.exists() {
                if let data = snap.value as? [AnyHashable : Any] {
                    completion(ChatUser(data: data))
                }else{
                    completion(nil)
                }
            }else {
                completion(nil)
            }
        }
    }
    
    // MARK: Function to get all users available in DB
    func getAllUsersList(completion : @escaping ([ChatUser]) -> Void) {
        self.userMaster.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.value != nil {
                if let users = snapshot.value as? [AnyHashable : Any] {
                    var arrUsers = [ChatUser]()
                    for userData in users {
                        if let data = userData.value as? [AnyHashable : Any] {
                            if ChatUser(data: data).id != self.currentChatUser!.id {
                             arrUsers.append(ChatUser(data: data))
                            }
                        }
                    }
                    completion(arrUsers)
                    
                }else{ completion([]) }
            }else{
                completion([])
            }
        }
    }
    
    // MARK: Function to get list of chats for current user with sunnation of unread count
    func getListOfChatsAndUnreadCount(completion : @escaping ([Thread], Int) -> Void) {
        
        checkCurrentUser()
        
        let pathToChats = self.userMaster.child(currentChatUser!.id).child(THREAD_MASTER)
        
        pathToChats.observe(.childAdded, with: { (snapshot) -> Void in
            
            if snapshot.value! is NSNull {
                completion([], 0)
                return
            }
            
            let thread = Thread(snapshot: snapshot)
            self.currentChatUser!.Threads.append(thread)
            
            pathToChats.child(thread.id).child(CHAT_UNREADCOUNT).observe(.value, with: { (snap) in
                
                if !thread.isActive || thread.isDeleted { return }
                
                let count: Int!
                if let c = snap.value as? Int {
                    count = c
                }
                else if let c = snap.value as? String {
                    count = Int(c) ?? 0
                }
                else {
                    count = 0
                }
                thread.thisUserUnreadCount = count
                
                
                let sum = self.currentChatUser!.Threads.reduce(0, { (currentResult, nextThread) -> Int in
                    return currentResult + nextThread.thisUserUnreadCount
                })
                
                completion(self.currentChatUser!.Threads, sum)
                
            })
        })
        
    }
    
    // MARK: Start observation of updates in listing like unread count, last message etc.
    func startObservationOnListUpdates(completion : @escaping ([Thread]) -> Void) {
        checkCurrentUser()
        
        let pathToChats = self.userMaster.child(currentChatUser!.id).child(THREAD_MASTER)

        
        for thread in self.currentChatUser!.Threads {
            let pathToChat = pathToChats.child(thread.id)
            
            pathToChat.child(CHAT_LASTUPDATETIME).observe(DataEventType.value, with: {
                
                timeSnapshot in
                
                pathToChat.child(CHAT_MESSAGE).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
                    
                    if snapshot.value! is NSNull {
                        return
                    }
                    if let time = timeSnapshot.value as? Int
                    {
                        let message = Message(snapshot: snapshot)
                        thread.lastMessage = message
                        thread.lastMessage?.datetime = time
                        
                        if message.senderId == self.currentChatUser!.id {
                            thread.lastMessage?.senderId = self.currentChatUser!.id
                        }
                        else {
                            thread.lastMessage?.senderId = thread.otherUser.id
                        }
                        
                        self.currentChatUser!.Threads.sort(by: { (x, y) -> Bool in
                            (x.lastMessage?.datetime ?? 0) > (y.lastMessage?.datetime ?? 0)
                        })
                        
                        completion(self.currentChatUser!.Threads)
                    }
                })
                
                
                pathToChat.child(CHAT_ISDELETED).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
                    
                    if snapshot.value! is NSNull {
                        return
                    }
                    
                    if let isDeleted = snapshot.value as? Bool {
                        if !isDeleted && !self.currentChatUser!.Threads.contains(thread) {
                            thread.isDeleted = false
                            thread.isActive = true
                            
                            let isAvailable = self.currentChatUser!.Threads.contains(where: { (_thread) -> Bool in
                                return _thread.id == thread.id
                            })
                            
                            if !isAvailable {
                             self.currentChatUser!.Threads.append(thread)
                            }
                            
                            self.currentChatUser!.Threads.sort(by: { (x, y) -> Bool in
                                (x.lastMessage?.datetime ?? 0) > (y.lastMessage?.datetime ?? 0)
                            })
                            
                            completion(self.currentChatUser!.Threads)
                        }
                    }
                    
                })
                
            })
            
        }
        
        
        pathToChats.observe(.childRemoved, with: { (snapshot) -> Void in
            
            if snapshot.value! is NSNull {
                return
            }
            
            let thread = Thread(snapshot: snapshot)
            
            self.currentChatUser!.Threads.removeAll(where: { (thr) -> Bool in
                return thr.id == thread.id
            })
            
            self.currentChatUser!.Threads.sort(by: { (x, y) -> Bool in
                (x.lastMessage?.datetime ?? 0) > (y.lastMessage?.datetime ?? 0)
            })
            
            completion(self.currentChatUser!.Threads)
            
        })
    }
    
    // MARK: Function to send message in chat
    func sendMessage(toChat chat: Thread, withMessage msg : Message) {
        
        let pathToMessages = self.threadMaster.child(chat.id).child(CHAT_MESSAGES)
        let newMessage = pathToMessages.childByAutoId()
        let time = Int(Date().timeIntervalSince1970)
        let message = msg
        msg.id = newMessage.key
        
        newMessage.setValue(message.getDictionary())
        
        if message.isUploaded {
         PBChat.shared.increaseUsersUnreadCount(inChat: chat, withMessage: message.getDictionaryForLastMessage(), time: time)
        }
        
    }
    
    // MARK: Function to get list of messages for chat
    func getListOfMessages(forChat chat : Thread, completion : @escaping ([(date: Date,messages: [Message])]) -> Void) {
        
        let pathToMessages = self.threadMaster.child(chat.id).child(CHAT_MESSAGES).queryOrdered(byChild: CHAT_DATETIME)
            .queryStarting(atValue: chat.clearChatTime)
        //.queryOrderedByKey()
        
        var messagesWRTDates: [(date: Date,messages: [Message])] = []
        
        pathToMessages.observe(.childAdded) { (snapshot) in
            if snapshot.exists() {
                
                let message = Message(snapshot: snapshot)
                
                if let msgTime = message.datetime {
                    if msgTime < chat.clearChatTime {
                        return
                    }
                }
                
                if !message.isUploaded && message.type != .text {
                    if (message.senderId != chat.thisUser.id) {
                        return
                    }
                    
                    if isFileExistAtPath(url: message.mediaURL!.path) {
                        
                        self.uploadToAWSAndUpdateMessage( thread: chat, messageReference: snapshot.ref, message: message, percentClosure: { (percent) in
                            message.percentUploadedClosure(Float(percent))
                            message.percentUploaded = Double(percent)
                        }, errorClosure: { (error) in
                            print(error.localizedDescription)
                        })
                    }
                }
                
                if let lastMessage = messagesWRTDates.last?.1.last {
                    if Calendar.current.isDate(Date(timeIntervalSince1970: TimeInterval(message.datetime!)), inSameDayAs: Date(timeIntervalSince1970: TimeInterval(lastMessage.datetime!))) {
                        //One same day
                        messagesWRTDates[messagesWRTDates.count-1].messages.append(message)
                    }
                        //Both dates not on same day
                    else {
                        messagesWRTDates.append((date: Date(timeIntervalSince1970: TimeInterval(message.datetime!)), messages:  [message]))
                    }
                }
                else {
                    //There are no elements in messages array, append a new one
                    messagesWRTDates.append((date: Date(timeIntervalSince1970: TimeInterval(message.datetime!)), messages: [message]))
                }
                
                completion(messagesWRTDates)
            }
        }
        
        pathToMessages.observe(.childChanged) { (snapshot) in
            
            let message = Message(snapshot: snapshot)
            
            if let msgTime = message.datetime {
                if msgTime < chat.clearChatTime {
                    return
                }
            }
            
            for i in 0..<messagesWRTDates.count {
                for j in 0..<messagesWRTDates[i].messages.count {
                    if messagesWRTDates[i].messages[j].id == message.id {
                        messagesWRTDates[i].messages[j] = message
                        completion(messagesWRTDates)
                        return
                    }
                }
            }
            
            if let lastMessage = messagesWRTDates.last?.1.last {
                if Calendar.current.isDate(Date(timeIntervalSince1970: TimeInterval(message.datetime!)), inSameDayAs: Date(timeIntervalSince1970: TimeInterval(lastMessage.datetime!))) {
                    //One same day
                    messagesWRTDates[messagesWRTDates.count-1].messages.append(message)
                }
                    //Both dates not on same day
                else {
                    messagesWRTDates.append((date: Date(timeIntervalSince1970: TimeInterval(message.datetime!)), messages:  [message]))
                }
            }
            else {
                //There are no elements in messages array, append a new one
                messagesWRTDates.append((date: Date(timeIntervalSince1970: TimeInterval(message.datetime!)), messages: [message]))
            }
            
            completion(messagesWRTDates)
        }
    }
    
}


// MARK: PRIVATE FUNCTIONS
extension PBChat {

    // MARK: Check current user is initiated or not
    private func checkCurrentUser(){
        if currentChatUser == nil {
            // Crash the app if user is not coonfigured for chat
            fatalError("CHAT USER IS NOT CONFIGURED")
        }
    }
    
    // MARK: Function to upload media file on aws server
    // thread: Chat Instance
    // message: Message Instance
    // messageReference: Referenct of message instance to updtate values
    // percentClosure: Clouser to send upload percentages
    private func uploadToAWSAndUpdateMessage(thread: Thread, messageReference: DatabaseReference, message: Message, percentClosure: @escaping (CGFloat)->Void, errorClosure: @escaping (Error)->Void) {
        
        if let localURL = message.mediaURL {
            
            let ext = localURL.pathExtension
            
            //Setting up unread count and last message
            let lastMessage = [
                CHAT_SENDERID : thread.thisUser.id,
                CHAT_TEXT : message.text,
                CHAT_TYPE : message.type.rawValue
            ]
            
            let time = Int(Date().timeIntervalSince1970)
            
            AWSHelper.sharedInstance.upload(
                fileName: (message.id ?? thread.id) + "." + ext,
                fileURL: localURL) { (response) in
                    switch response {
                    case .success(let localURL, let liveURL):
                        message.mediaURL = liveURL
                        message.isUploaded = true
                        
                        if let thumbUrl = message.thumbnail {
                            
                            AWSHelper.sharedInstance.upload(fileName: "thumb_" + (message.id ?? thread.id) + "." + ext , fileURL: thumbUrl, response: { (res) in
                                
                                switch res {
                                case .success(let thumbLoc, let thumbLive):
                                    
                                    message.thumbnail = thumbLive
                                    deleteFileAtPath(url: thumbLoc.absoluteString)
                                    
                                    messageReference.setValue(message.getDictionary())
                                    var newLocalURL = localURL.deletingLastPathComponent()
                                    newLocalURL.appendPathComponent(liveURL.lastPathComponent)
                                    if moveFile(fromUrl: localURL, toUrl: newLocalURL) {
                                        deleteFileAtPath(url: localURL.path)
                                    }
                                    self.increaseUsersUnreadCount(inChat: thread, withMessage: lastMessage, time: time)
                                    
                                    
                                case .error(_, _):
                                    break
                                case .progress(_):
                                    break
                                }
                            })
                            
                        }else {
                            
                            messageReference.setValue(message.getDictionary())
                            
                            var newLocalURL = localURL.deletingLastPathComponent()
                            newLocalURL.appendPathComponent(liveURL.lastPathComponent)
                            if moveFile(fromUrl: localURL, toUrl: newLocalURL) {
                                deleteFileAtPath(url: localURL.path)
                            }
                            
                            self.increaseUsersUnreadCount(inChat: thread, withMessage: lastMessage, time: time)
                        }
                        
                    case .progress(let uploadProgressPercent):
                        percentClosure(uploadProgressPercent)
                        break;
                    case .error(_, let error):
                        errorClosure(error)
                        break;
                    }
            }
        }
    }
}
