//
//  Thread.swift
//

import Foundation
import FirebaseDatabase

enum ChatType : String {
    case OneToOne = "O"
    case Group = "G"
}

class Thread: NSObject {
    
    var id: String!
    var thisUser = ChatUser()
    var otherUser = ChatUser()
    var Users = [ChatUser]()
    var createdOn: Int?
    var lastUpdateTime: Int?
    var clearChatTime : Int = 0
    var lastMessage: Message?
    var type = ChatType.OneToOne
    var isDeleted = false
    var isActive = true
    var isMyGroup = false
    var thisUserUnreadCount: Int = 0
    var messages : [Message] = [] // For Thread Master
    var groupInfo : groupInfo?
    var otherInfo: [AnyHashable : Any] = [:]
    
//    var thisUserIsTypingRef: DatabaseReference!
//    var otherUserIsTypingRef: DatabaseReference!
//    var thisUserUnreadCountRef: DatabaseReference!
//    var otherUserUnreadCountRef: DatabaseReference!
    
    
    /*
     *  Initializes a thread for chat
     *
     *  @param current current user
     *  @param type Group or OneToOne
     *
     *  Parameters
     */
    convenience init(snapshot: DataSnapshot)
    {
        self.init()
        self.id = snapshot.key
        if let data = snapshot.value as? [AnyHashable : Any] {
         self.updateFrom(data: data)
        }
    }
    
    func updateFrom(data: [AnyHashable : Any])
    {
     
        if let ID = data[CHAT_ID] as? String {
            self.id = ID
        }
        
        if let type = data[CHAT_TYPE] as? String {
            self.type = type.lowercased() == "g" ? .Group : .OneToOne
        }
        
        if let owner = data[CHAT_OWNER] as? [AnyHashable : Any] {
            let user = ChatUser(data: owner)
            if user.id == PBChat.shared.currentChatUser?.id || self.type == .Group {
                thisUser = user
            }else {
                otherUser = user
            }
        }
        
        if let opponent = data[CHAT_OPPONENT] as? [AnyHashable : Any] {
            let user = ChatUser(data: opponent)
            if user.id == PBChat.shared.currentChatUser?.id && self.type == .OneToOne {
                thisUser = user
            }else {
                otherUser = user
            }
        }
        //FIXME: Need to bind data
        /*if let owner = data[CHAT_USERS] as? [AnyHashable : Any]
        {
            let user = ChatUser(data: owner)
            if user.id == PBChat.shared.currentChatUser?.id || self.type == .Group {
                thisUser = user
            }else {
                otherUser = user
            }
        }*/
        
        if let unreadCount = data[CHAT_UNREADCOUNT] as? Int {
            self.thisUserUnreadCount = unreadCount
        }
        
        if let clearChat = data[CHAT_CLEAR] as? Int {
            self.clearChatTime = clearChat
        }
        
        if let isDeleted = data[CHAT_ISDELETED] as? Bool {
            self.isDeleted = isDeleted
        }
        
        if let isActive = data[CHAT_ISACTIVE] as? Bool {
            self.isActive = isActive
        }
        
        if let lastMessage = data[CHAT_MESSAGE] as? [AnyHashable : Any]
        {
            self.lastMessage = Message(data: lastMessage)
        }
        
        if let otherInfo = data[CHAT_OTHERINFO] as? [AnyHashable : Any] {
            self.otherInfo = otherInfo
        }
        
        if let lastUpdateTime = data[CHAT_LASTUPDATETIME] as? Int {
            self.lastUpdateTime = lastUpdateTime
        }
    }
    
    
    func getDictionary() -> [AnyHashable : Any]
    {
        var returnValue : [AnyHashable : Any] = [
            CHAT_ID: self.id ?? "0",
            CHAT_TYPE: self.type.rawValue,
            CHAT_CREATEDON: self.createdOn ?? 0,
            CHAT_LASTUPDATETIME: self.lastUpdateTime ?? 0,
            CHAT_CLEAR: self.clearChatTime,
            CHAT_ISDELETED: self.isDeleted,
            CHAT_ISACTIVE: self.isActive,
            CHAT_OTHERINFO : self.otherInfo,
            CHAT_USERS : [
                thisUser.id : thisUser.getDictionaryForThreadMaster(),
                otherUser.id : otherUser.getDictionaryForThreadMaster()
            ]
        ]
        
        if let info = self.groupInfo?.getDictionary() {
            returnValue[CHAT_GROUPINFO] = info
        }
        
        return returnValue
    }
    
    func getDictionaryForGroup() -> [AnyHashable : Any]
    {
        var returnValue : [AnyHashable : Any] = [
            CHAT_ID: self.id ?? "0",
            CHAT_TYPE: self.type.rawValue,
            CHAT_CREATEDON: self.createdOn ?? 0,
            CHAT_LASTUPDATETIME: self.lastUpdateTime ?? 0,
            CHAT_CLEAR: self.clearChatTime,
            CHAT_ISDELETED: self.isDeleted,
            CHAT_ISACTIVE: self.isActive,
            CHAT_OTHERINFO : self.otherInfo
        ]
        
        if let info = self.groupInfo?.getDictionary() {
            returnValue[CHAT_GROUPINFO] = info
        }
        
        return returnValue
    }
    
    func getDictionaryForUserMastr() -> [AnyHashable : Any]
    {
        
        var returnValue: [AnyHashable : Any] = [
            CHAT_ID: self.id ?? "0",
            CHAT_TYPE: self.type.rawValue,
            CHAT_LASTUPDATETIME: self.lastUpdateTime ?? 0,
            CHAT_CLEAR: self.clearChatTime,
            CHAT_ISDELETED: self.isDeleted,
            CHAT_ISACTIVE: self.isActive,
            CHAT_OWNER: self.thisUser.getDictionaryForUserThreads(),
            CHAT_OPPONENT: self.otherUser.getDictionaryForUserThreads()
        ]
        
        if let message = self.lastMessage?.getDictionaryForLastMessage() {
            returnValue[CHAT_MESSAGE] = message
        }
        
        return returnValue
    }
    
}

extension Date
{
    static func timeForLastSeen(date: Date) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yy hh:mm a"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        //formatter.dateFormat = "MM/dd/yy HH:mm"
        return formatter.string(from: date)
    }
    
    static func timeForChat(date: Date) -> String
    {
        let calender = Calendar.current
        if calender.isDateInToday(date) {
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            formatter.locale = Locale(identifier: "en_US_POSIX")
            return formatter.string(from: date)
        }
        else if calender.isDateInYesterday(date) {
            return "yesterday"
        }
        else {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            //formatter.dateFormat = "MM/dd/yyyy"
            return formatter.string(from: date)
        }
    }
}


class groupInfo: NSObject {
    var adminId: String = ""
    var adminImage: URL? = nil
    var adminName: String = ""
    var groupImage: URL? = nil
    var groupName: String = ""
    var createdOn : Int?
    
    convenience init(snapshot: DataSnapshot) {
        self.init()
        if let data = snapshot.value as? [AnyHashable : Any] {
            self.updateFrom(dictionary: data)
        }
    }
    
    convenience init(adminId: String, adminImage: URL?, adminName: String, groupImage: URL?, groupName: String, createdOn : Int?) {
        self.init()
        
        self.adminId = adminId
        self.adminName = adminName
        self.adminImage = adminImage
        self.groupName = groupName
        self.groupImage = groupImage
        self.createdOn = createdOn
    }
    
    func updateFrom(dictionary: [AnyHashable : Any]) {
        
        if let adminId = dictionary[CHAT_ADMINID] as? String {
            self.adminId = adminId
        }
        
        if let adminName = dictionary[CHAT_ADMINNAME] as? String {
            self.adminName = adminName
        }
        
        if let adminImage = dictionary[CHAT_ADMINPIC] as? String {
            self.adminImage = URL(string: adminImage)
        }
        
        if let groupName = dictionary[CHAT_GROUPNAME] as? String {
            self.groupName = groupName
        }
        
        if let groupImage = dictionary[CHAT_GROUPPIC] as? String {
            self.groupImage = URL(string: groupImage)
        }
        
        if let createdOn = dictionary[CHAT_CREATEDON] as? Int {
            self.createdOn = createdOn
        }
    }
    
    func getDictionary() -> [AnyHashable : Any] {
        return [
            CHAT_ADMINID : self.adminId,
            CHAT_ADMINNAME : self.adminName,
            CHAT_ADMINPIC : self.adminImage?.absoluteString ?? "",
            CHAT_GROUPNAME : self.groupName,
            CHAT_GROUPPIC : self.groupImage?.absoluteString ?? "",
            CHAT_CREATEDON : self.createdOn ?? 0,
        ]
    }
}
