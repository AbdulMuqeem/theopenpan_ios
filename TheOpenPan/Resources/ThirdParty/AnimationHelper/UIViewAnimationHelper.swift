//  
//  UIViewAnimationHelper
//

import UIKit

extension UIView
{
    func addTo(superView: UIView, specificViewToAnimate: UIView? = nil, withAnimation animation: Animation = .fade, afterAnimation completion: @escaping ()->() = {}) {
        
        self.alpha = 0.0
        self.frame = superView.bounds
        self.layoutIfNeeded()
        
        var viewToAnimate = self
        var viewToAnimateFrame = self.frame
        
        if let v = specificViewToAnimate {
            viewToAnimate = v
            viewToAnimateFrame = v.frame
        }
        
        let animatedViewFinalFrame = viewToAnimateFrame
        
        switch animation {
        case .Top:
            viewToAnimateFrame.origin.y -= self.frame.height
        case .Left:
            viewToAnimateFrame.origin.x -= self.frame.width
        case .Right:
            viewToAnimateFrame.origin.x += self.frame.width
        case .Bottom:
            viewToAnimateFrame.origin.y += self.frame.height
        case .fade:
            self.alpha = 0.0
        case .none:
            break
        }
        
        switch animation {
        case .Bottom, .Left, .Right, .Top, .none:
            self.alpha = 1.0
        default:
            break
        }
        
        viewToAnimate.frame = viewToAnimateFrame
        superView.addSubview(self)
        
        UIView.animate(withDuration: 0.28, animations: {
            viewToAnimate.frame = animatedViewFinalFrame
            self.alpha = 1.0
        }) { (x) in
            completion()
        }
    }
    
    func removeFrom(superView: UIView, specificViewToAnimate: UIView? = nil, withAnimation animation: Animation = .fade, afterAnimation completion: @escaping ()->() = {}) {
        
        var tempFrameForAnimation = self.frame
        var viewToAnimate = self
        
        if let v = specificViewToAnimate {
            viewToAnimate = v
            tempFrameForAnimation = v.frame
        }
        
        viewToAnimate.frame = tempFrameForAnimation
        
        UIView.animate(withDuration: 0.28, animations: {
            switch animation {
            case .Top:
                tempFrameForAnimation.origin.y -= self.frame.height
            case .Left:
                tempFrameForAnimation.origin.x -= self.frame.width
            case .Right:
                tempFrameForAnimation.origin.x += self.frame.width
            case .Bottom:
                tempFrameForAnimation.origin.y += self.frame.height
            case .fade:
                self.alpha = 0.0
            case .none:
                break
            }
            viewToAnimate.frame = tempFrameForAnimation
        }) { (x) in
            if x
            {
                self.removeFromSuperview()
                completion()
            }
        }
    }
    
    enum Animation
    {
        case Top
        case Bottom
        case Left
        case Right
        case fade
        case none
    }
}
