//
//  Networking.swift
//

import Foundation
import UIKit
import Alamofire
import Firebase
import SwiftyJSON
import MagicalRecord

enum GetTokenResponse
{
    case failed(error: String)
    case error(error: String)
    case success(message: String)
}

func getToken(deviceid: String?, indicator: Indicator = .window, response: @escaping (GetTokenResponse)->Void)
{
    //PbCircleDotLoader.sharedInstance.startAnimation()
    indicator.show()
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getToken,
        parameters: [
            "device_id":deviceid ?? "",
            "device_type":"I"
        ],
        Header: [:],
        handler: { (res) in
            //PbCircleDotLoader.sharedInstance.stopAnimation()
            indicator.hide()
            
            if res.code != 0, let _tokenObj = res.Object.value(forKey: "auth")
            {
                apiAuthHeader.updateInfo(JSON(_tokenObj))
                response(.success(message: res.message))
            }
            else
            {
                response(.failed(error: res.message))
            }
            
    }) { (error) in
        //PbCircleDotLoader.sharedInstance.stopAnimation()
        indicator.hide()
        response(.error(error: error.localizedDescription))
    }
}


//Common Response for signin, signup, socialsignin
enum AuthenticationResponse
{
    case success(user: User, message: String)
    case failed(message: String)
    case error(error: String)
}

/*
 *  log In Social
 */
func logInSocial(username: String, email: String, profile_image: String,deviceid: String?, social_type: enum_social_type, social_id:String, indicator: Indicator = .window, response: @escaping (AuthenticationResponse)->Void)
{
    var param: [String: Any] = [
        "username"      : username,
        "device_id"     : deviceid ?? "",
        "device_type"   : "I",
        "social_type"   : social_type.rawValue,
        "social_id"     : social_id
    ]
    
    if email != "" {
        param.updateValue(email, forKey: "email")
    }
    if profile_image != "" {
        param.updateValue(profile_image, forKey: "profile_image")
    }
    
    PbCircleDotLoader.sharedInstance.startAnimation()
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .socialLogin,
        parameters: param,
        Header: [:],
        handler: { (res) in
           
            print(res.Object)
            
            if let user = res.Object.value(forKey: "user")
            {
                PbCircleDotLoader.sharedInstance.stopAnimation()
                let userObj = User(info: JSON(user))
                response(.success(user: userObj, message: res.message))
            }
            else
            {
                PbCircleDotLoader.sharedInstance.stopAnimation()
                response(.failed(message: res.message))
            }
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.error(error: e.localizedDescription))
    }
}

/*
 *  log In
 */

func logIn(deviceid: String?, email: String, password: String, indicator: Indicator = .window, response: @escaping (AuthenticationResponse)->Void)
{
    let param: [String: Any] = [
        "email"         : email,
        "password"      : password,
        "device_id"     : deviceid ?? "",
        "device_type"   : "I"
    ]
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .signIn,
        parameters: param,
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
            
            if let user = res.Object.value(forKey: "user")
            {
                let userObj = User(info: JSON(user))
                response(.success(user: userObj, message: res.message))
            }
            else
            {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        response(.error(error: e.localizedDescription))
    }
}

/*
 *  Sign Up
 */
func signUp(deviceid: String?, username: String,email: String,password: String, indicator: Indicator = .window, response: @escaping (AuthenticationResponse)->Void)
{
    //    PbCircleDotLoader.sharedInstance.startAnimation()
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .signUp,
        parameters: [
            "username"      : username,
            "email"         : email,
            "password"      : password,
            "device_id"     : deviceid ?? "",
            "device_type"   : "I"
        ],
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
            
            //            PbCircleDotLoader.sharedInstance.stopAnimation()
            
            if let user = res.Object.value(forKey: "user")
            {
                let userObj = User()
                userObj.updateInfo(JSON(user))
                
                response(.success(user: userObj, message: res.message))
            }
            else
            {
                response(.failed(message: res.message))
            }
    }) { (e) in
        //        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.error(error: e.localizedDescription))
    }
}



/*
 *  verify OTP
 */
//type: VerifyOtpTypesAPI = .Default,
func verifyOTP(code: String, dial_code: String, mobile: String,type: VerifyOtpTypesAPI, indicator: Indicator = .window, response: @escaping (AuthenticationResponse)->Void)
{
    PbCircleDotLoader.sharedInstance.startAnimation()
    
    var parameter =
        [
            "otp" : code,
            "dial_code": dial_code,
            "mobile": mobile
    ]
    
    if type == VerifyOtpTypesAPI.EditProfile
    {
        parameter["type"] = type.rawValue
    }
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .verifyOTP,
        parameters: parameter,
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
            PbCircleDotLoader.sharedInstance.stopAnimation()
            
            if res.code == 1
            {
                if let user = res.Object.value(forKey: "user")
                {
                    let userObj = User()
                    userObj.updateInfo(JSON(user))
                    response(.success(user: userObj, message: res.message))
                }
                else
                {
                    response(.success(user: User(), message: res.message))
                    //response(.failed(message: res.message))
                }
            }
            else
            {
                response(.failed(message: res.message))
            }
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.error(error: e.localizedDescription))
    }
}


/*enum ResendOTPResponse
 {
 case success(otp: String,message:String)
 case failed(message: String)
 case error(error: Error)
 }
 
 func resendOTP(dial_code : String , mobileNo : String,new_dial_code: String = "",new_mobile: String = "", type: VerifyOtpTypesAPI = .ResendOTP, indicator: Indicator = .window, response: @escaping (ResendOTPResponse)->Void)
 {
 PbCircleDotLoader.sharedInstance.startAnimation()
 AlamofireModel.alamofireMethod(
 .post,
 apiAction: .reSendOTP,
 parameters: [
 "dial_code"     : dial_code,
 "mobile"        : mobileNo,
 "type"          : type.rawValue,
 "new_dial_code" : new_dial_code,
 "new_mobile"    : new_mobile,
 ],
 Header: [:],
 handler: { (res) in
 
 print(res.Object)
 
 PbCircleDotLoader.sharedInstance.stopAnimation()
 
 if res.code == 1
 {
 response(.success(otp: "", message: res.message))
 }
 else
 {
 response(.failed(message: res.message))
 }
 }) { (e) in
 PbCircleDotLoader.sharedInstance.stopAnimation()
 response(.error(error: e))
 }
 }*/


/*
 *  Account Setup API
 */
/*func accountSetupAPI(email: String, password: String, referral_code: String, indicator: Indicator = .window, response: @escaping (AuthenticationResponse)->Void)
 {
 PbCircleDotLoader.sharedInstance.startAnimation()
 
 AlamofireModel.alamofireMethod(
 .post,
 apiAction: .accountSetup,
 parameters: [
 "email" : email,
 "password" : password,
 "referral_code": referral_code
 ],
 Header: [:],
 handler: { (res) in
 
 print(res.Object)
 PbCircleDotLoader.sharedInstance.stopAnimation()
 
 if res.code == 1
 {
 if let user = res.Object.value(forKey: "user")
 {
 let userObj = User()
 userObj.updateInfo(JSON(user))
 response(.success(user: userObj, message: res.message))
 }
 else
 {
 response(.failed(message: res.message))
 }
 }
 else
 {
 response(.failed(message: res.message))
 }
 }) { (e) in
 PbCircleDotLoader.sharedInstance.stopAnimation()
 response(.error(error: e.localizedDescription))
 }
 }*/


/*
 *  DescriptiveResponse : responses those contains only messages
 *  Forgot Password
 */
enum DescriptiveResponse
{
    case success(message: String)
    case failed(message: String)
    case error(error: Error)
}

func forgotPassword(email: String, indicator: Indicator = .window, response: @escaping (DescriptiveResponse)->Void)
{
    let param: [String: Any] = [
        "email"         : email,
        "type"          : 2
    ]
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .forgotPassword,
        parameters: param,
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
            
            if res.success.mk_boolValue
            {
                response(.success(message: res.message))
            }
            else
            {
                response(.failed(message: res.message))
            }
    }) { (e) in
        
        response(.error(error: e))
    }
}


enum resendVerificationLinkResponse
{
    case success(message: String)
    case failed(message: String)
    case error(error: Error)
}

func resendVerificationLink(email: String, indicator: Indicator = .window, response: @escaping (resendVerificationLinkResponse)->Void)
{
    let param: [String: Any] = [
        "email"         : email,
        "type"          : 1
    ]
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .forgotPassword,
        parameters: param,
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
            
            if res.success.mk_boolValue
            {
                response(.success(message: res.message))
            }
            else
            {
                response(.failed(message: res.message))
            }
    }) { (e) in
        
        response(.error(error: e))
    }
}

/*
 *  DescriptiveResponse : responses those contains only messages
 *  Change Password
 */
func changePassword(old_password: String, new_password: String, indicator: Indicator = .window, response: @escaping (DescriptiveResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .changePassword,
        parameters: [
            "old_password" : old_password,
            "new_password" : new_password,
        ],
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
            
            
            if res.success.mk_boolValue {
                response(.success(message: res.message))
            } else {
                response(.failed(message: res.message))
            }
    }) { (e) in
        
        response(.error(error: e))
    }
}

/*
 *  DescriptiveResponse : responses those contains only messages
 *  Reset Password
 */
func resetPassword(dial_code: String, mobile: String, new_password: String, indicator: Indicator = .window, response: @escaping (DescriptiveResponse)->Void)
{
    PbCircleDotLoader.sharedInstance.startAnimation()
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .resetPassword,
        parameters: [
            "dial_code": dial_code,
            "mobile": mobile,
            "new_password" : new_password,
        ],
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
            PbCircleDotLoader.sharedInstance.stopAnimation()
            
            if res.code == 1
            {
                response(.success(message: res.message))
            }
            else
            {
                response(.failed(message: res.message))
            }
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.error(error: e))
    }
}

/*
 *  DescriptiveResponse : responses those contains only messages
 *  Change Notification Preference
 */
func notificationSettingAPI(is_notify: String, indicator: Indicator = .window, response: @escaping (DescriptiveResponse)->Void)
{
    PbCircleDotLoader.sharedInstance.startAnimation()
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .notificationSetup,
        parameters: [
            "is_notify" : is_notify,
        ],
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
            PbCircleDotLoader.sharedInstance.stopAnimation()
            
            if res.code == 1
            {
                response(.success(message: res.message))
            }
            else
            {
                response(.failed(message: res.message))
            }
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.error(error: e))
    }
}




//Update Profile API
enum updateProfileResponse
{
    case success(user: User, message: String)
    case failed(message: String)
    case error(error: String)
}

func editProfile(username: String?, profile_image: String?,email: String?,is_screen_lock: Bool, indicator: Indicator = .window, response: @escaping (updateProfileResponse)->Void) {
    
    var param = [String:Any]()
    
    if username == nil {
        param = ["is_screen_lock":is_screen_lock.mk_intValue]
    } else {
        param = [
            "username"      : username!,
            "email"         : email!
        ]
    }
    
    if let profile_image = profile_image {
        param.updateValue(profile_image, forKey: "profile_image")
    }
    
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .updateProfile,
        parameters: param,
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
            
            if res.success.mk_boolValue {
                
                if let user = res.Object.value(forKey: "user") {
                    
                    let userObj = User()
                    userObj.updateInfo(JSON(user))
                    kCurrentUser = userObj
                    kCurrentUser.saveToDefault()
                    
                    if let _tokenObj = res.Object.value(forKey: "auth") {
                        apiAuthHeader.updateInfo(JSON(_tokenObj))
                    }
                    
                    response(.success(user: userObj, message: res.message))
                } else {
                    
                    response(.failed(message: res.message))
                }
            } else {
                
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        response(.failed(message: e.localizedDescription))
    }
}


//Update Profile API
enum getProfileResponse
{
    case success(user: User, message: String)
    case failed(message: String)
    case error(error: String)
}

func getProfile(indicator: Indicator = .window, response: @escaping (getProfileResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .get,
        apiAction: .getProfile,
        parameters: [:],
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
            
            if res.success.mk_boolValue {
                
                if let user = res.Object.value(forKey: "user") {
                    
                    let userObj = User()
                    userObj.updateInfo(JSON(user))
                    kCurrentUser = userObj
                    kCurrentUser.saveToDefault()
                    
                    if let _tokenObj = res.Object.value(forKey: "auth") {
                        apiAuthHeader.updateInfo(JSON(_tokenObj))
                    }
                    
                    response(.success(user: userObj, message: res.message))
                } else {
                    
                    response(.failed(message: res.message))
                }
            } else {
                
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        response(.failed(message: e.localizedDescription))
    }
}



/*enum SubjectsResponse
 {
 case sucess(subjects: SubjectListModel)
 case failed(error: String)
 }
 func subjectList(indicator: Indicator = .window, response: @escaping (SubjectsResponse)->Void)
 {
 PbCircleDotLoader.sharedInstance.startAnimation()
 AlamofireModel.alamofireMethod(
 .post,
 apiAction: .getSubjectList,
 parameters: [:],
 Header: [:],
 handler: { (res) in
 PbCircleDotLoader.sharedInstance.stopAnimation()
 if let subjects = res.Object.value(forKey: "subjects_list")
 {
 let list = SubjectListModel()
 list.updateInfo(JSON(subjects))
 response(.sucess(subjects: list))
 }
 else
 {
 response(.failed(error: res.message))
 }
 }) { (e) in
 PbCircleDotLoader.sharedInstance.stopAnimation()
 response(.failed(error: e.localizedDescription))
 }
 }*/


//Update Profile API
enum termsResponse
{
    case success(terms: String, message: String)
    case failed(message: String)
    case error(error: String)
}

func getTerms(indicator: Indicator = .window, response: @escaping (termsResponse)->Void) {
    
    AlamofireModel.alamofireMethod(
        .get,
        apiAction: .getTerms,
        parameters: [:],
        Header: [:],
        handler: { (res) in
            
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionary {
                    response(.success(terms: dictData["terms"]?.stringValue ?? "", message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}



func contactUs(subject : String , comment: String, indicator: Indicator = .window, response: @escaping (DescriptiveResponse)->Void)
{
    //    PbCircleDotLoader.sharedInstance.startAnimation()
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .contactUs,
        parameters: [
            "subject":subject,
            "comment":comment,
            "name":kCurrentUser.username ?? "",
            "email":kCurrentUser.email ?? ""
        ],
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
            //            PbCircleDotLoader.sharedInstance.stopAnimation()
            if res.success.mk_boolValue
            {
                response(.success(message: res.message))
            }
            else
            {
                response(.failed(message: res.message))
            }
    }) { (e) in
        //        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.error(error: e))
    }
}




func addCollection(collection_name : String , indicator: Indicator = .window, response: @escaping (DescriptiveResponse)->Void)
{
    //    PbCircleDotLoader.sharedInstance.startAnimation()
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .collectionAdd,
        parameters: [
            "collection_name":collection_name
        ],
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
            //            PbCircleDotLoader.sharedInstance.stopAnimation()
            if res.success.mk_boolValue
            {
                response(.success(message: res.message))
            }
            else
            {
                response(.failed(message: res.message))
            }
    }) { (e) in
        //        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.error(error: e))
    }
}


func logout(response: @escaping (DescriptiveResponse)->Void)
{
//    PbCircleDotLoader.sharedInstance.startAnimation()
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .logout,
        parameters: [:],
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
//            PbCircleDotLoader.sharedInstance.stopAnimation()
            
            if res.success.mk_boolValue
            {
                response(.success(message: res.message))
            }
            else
            {
                response(.failed(message: res.message))
            }
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.error(error: e))
    }
}



//for getMasterData responses
enum MasterDataResponse
{
    case success(objMasterData: Master_data, message: String)
    case failed(message: String)
    case error(error: String)
}

func getMasterData(indicator: Indicator = .window, response: @escaping (MasterDataResponse)->Void)
{
    
    //    PbCircleDotLoader.sharedInstance.startAnimation()
    AlamofireModel.alamofireMethod(
        .get,
        apiAction: .masterData,
        parameters: [:],
        Header: [:],
        handler: { (res) in
            
            //            PbCircleDotLoader.sharedInstance.stopAnimation()
            
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    
                    var mutableDictData = dictData
                    var arrNewCats = [[String:Any]]()
                    if let arrCat = dictData["categories"] as? [[String:Any]] {
                        for (i,dictCat) in arrCat.enumerated() {
                            var mutDictCat = dictCat
                            mutDictCat["displayIndex"] = i
                            arrNewCats.append(mutDictCat)
                        }
                    }
                    mutableDictData["categories"] = arrNewCats
                    print(mutableDictData)
                    
                    
                    Unit_option.mr_truncateAll(in: app_default_context)
                    CoreDataManager.sharedInstance.saveContext()
                    
                    if let arrUnitOptionDict = JSON(dictData)["unit_option"].arrayObject as? [[AnyHashable : Any]] {
                        
                        var arrUnitOption = [Unit_option]()
                        var arrServerSingular     = [String]()
                        var arrServerPlural       = [String]()
                        
                        for dict in arrUnitOptionDict {
                            let unitOption = Unit_option.mr_import(from: dict, in: app_default_context)
                            arrUnitOption.append(unitOption)
                            
                            if unitOption.tag_name_plural != "" && unitOption.tag_name_plural != nil {
                                arrServerSingular.append(unitOption.tag_name_singular ?? "")
                                arrServerPlural.append(unitOption.tag_name_plural ?? "")
                            }
                        }
                        if arrServerPlural.count > 0 {
                            arrPlurals      = arrServerPlural
                            arrSingulars    = arrServerSingular
                        }
                    }
                    
                    
                    let objMaster_data = Master_data.mr_import(from: mutableDictData, in: app_default_context)
                    
                    response(.success(objMasterData: objMaster_data, message: res.message))
                    
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}



////getInterestedCategories API
enum InterestedCategoriesResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func getInterestedCategories(indicator: Indicator = .window, response: @escaping (InterestedCategoriesResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .get,
        apiAction: .getInterestedCategory,
        parameters: [:],
        Header: [:],
        handler: { (res) in
            PbCircleDotLoader.sharedInstance.stopAnimation()
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    print(dictData)
                    
                    //                    let interestedCategoriesData = Master_data.mr_import(from: dictData, in: app_default_context)
                    response(.success(dictData: dictData, message: res.message))
                    
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
            
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}

////setinterestedcategory API
enum SetInterestedCategoriesResponse
{
    case success(message: String)
    case failed(message: String)
    case error(error: String)
}

func setInterestedCategories(tag_ids:String, time_id:String, is_one_pot:Int, indicator: Indicator = .window, response: @escaping (SetInterestedCategoriesResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .setinterestedcategory,
        parameters: ["tag_ids":tag_ids,
                     "time_id":time_id,
                     "is_one_pot":is_one_pot],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionary {
                    print(dictData)
                    response(.success(message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}




/////recipe/listbycategory API
enum getRecipeListByCategoriesResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func getRecipeListByCategories(start:Int, limit:Int, category_id:String, time_id:String, is_one_pot_meal:Bool, is_easy:Bool, search_keyword:String?, indicator: Indicator = .window, response: @escaping (getRecipeListByCategoriesResponse)->Void) -> DataRequest?
{
    
    var params = ["start":start,
                  "limit":limit] as [String : Any]
    
    
    if category_id != "" {
        params.updateValue(category_id, forKey: "category_id")
    }
    if let search_keyword = search_keyword {
        params.updateValue(search_keyword, forKey: "search_keyword")
    }
    if time_id != "" {
        params.updateValue(time_id, forKey: "time_id")
    }
    if is_one_pot_meal {
        params.updateValue(is_one_pot_meal, forKey: "is_one_pot_meal")
    }
    if is_easy {
        params.updateValue(is_easy, forKey: "is_easy")
    }
    
    let dataRequest = AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getRecipeListByCategories,
        parameters: params,
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
//                    print(dictData)
                    response(.success(dictData: dictData, message: res.message))
                    
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
    return dataRequest
}





////getMyRecipe API
enum getMyRecipesListResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func getMyRecipesList(start:Int, limit:Int, sort_type:Int, indicator: Indicator = .window, response: @escaping (getMyRecipesListResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getMyRecipeList,
        parameters: ["start":start,
                     "limit":limit,
                     "sort_type":sort_type],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    print(dictData)
                    response(.success(dictData: dictData, message: res.message))
                    
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}


////getCollectionList API
enum getCollectionListResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func getCollectionList(start:Int, limit:Int, indicator: Indicator = .window, response: @escaping (getCollectionListResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getCollectionList,
        parameters: ["start":start,
                     "limit":limit],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    print(dictData)
                    response(.success(dictData: dictData, message: res.message))
                    
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}

////getCollectionList API
enum getCollectionsRecipeListResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}


func getCollectionsRecipeList(start:Int, limit:Int, collection_id: Int64, category_id:String, time_id:String, is_one_pot_meal:Bool, is_easy:Bool, search_keyword:String?, indicator: Indicator = .window, response: @escaping (getCollectionsRecipeListResponse)->Void) -> DataRequest?
{
    
    let params = ["start":start,
                  "limit":limit,
                  "collection_id":collection_id] as [String : Any]
    
    
    let dataRequest = AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getCollectionsRecipeList,
        parameters: params,
        Header: [:],
        handler: { (res) in
            
            if res.success.mk_boolValue {
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
    return dataRequest
}


////getRecentSearchRecipeListResponse API
enum getRecentSearchRecipeListResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}


func getRecentSearchRecipeList(start:Int, limit:Int, indicator: Indicator = .window, response: @escaping (getRecentSearchRecipeListResponse)->Void) {
    
    let params = ["start":start,
                  "limit":limit] as [String : Any]
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getRecentSearchRecipeList,
        parameters: params,
        Header: [:],
        handler: { (res) in
            
            if res.success.mk_boolValue {
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}






//MARK:- Ingredients Parsing
enum getIngredientsParsingResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func getIngredientsParsing(ingredients:String, indicator: Indicator = .window, response: @escaping (getIngredientsParsingResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getParsingIngredients,
        parameters: ["ingredients":ingredients],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    print(dictData)
                    response(.success(dictData: dictData, message: res.message))
                    
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}


//MARK:- Ingredients Parsing
enum addRecipeToRecentSearchRes
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func addRecipeToRecentSearch(recipe_id:Int64, indicator: Indicator = .window, response: @escaping (addRecipeToRecentSearchRes)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .addRecipeToRecentSearch,
        parameters: ["recipe_id":recipe_id],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    print(dictData)
                    response(.success(dictData: dictData, message: res.message))
                    
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}


//MARK:- Parse Scrapper Recipes
enum parseScrapperRecipesRes
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func parseScrapperRecipes(url:String, indicator: Indicator = .window, response: @escaping (parseScrapperRecipesRes)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .parseScrapperRecipes,
        parameters: ["url":url],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    print(dictData)
                    response(.success(dictData: dictData, message: res.message))
                    
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}



///*
//*  Home screen apis
//*/
////MARK:- Home screen apis
//
////Update Profile API
//enum categoriesListResponse
//{
//    case success(resDataDict: [String:Any], message: String)
//    case failed(message: String)
//    case error(error: String)
//}
//
//func getAllCategoriesList(indicator: Indicator = .window, response: @escaping (categoriesListResponse)->Void)
//{
//
//    PbCircleDotLoader.sharedInstance.startAnimation()
//    AlamofireModel.alamofireMethod(
//        .post,
//        apiAction: .getAllCategoriesList,
//        parameters: [:],
//        Header: [:],
//        handler: { (res) in
//
//            PbCircleDotLoader.sharedInstance.stopAnimation()
//            if res.code == 200 {
//
//                if let dictData = JSON.init(res.Object).dictionary {
//                    print(dictData)
//
////                    Recipe_category.mr_truncateAll(in: app_default_context)
////                    Category_tag.mr_truncateAll(in: app_default_context)
////                    Unit_option.mr_truncateAll(in: app_default_context)
////                    CoreDataManager.sharedInstance.saveContext()
//
//                    if let arrDictCategories = dictData["categories"]?.arrayObject as? [[AnyHashable : Any]] {
//                        var arrCategory = [Recipe_category]()
//                        for dict in arrDictCategories {
//                            let recipeCategory = Recipe_category.mr_import(from: dict, in: app_default_context)
//                            arrCategory.append(recipeCategory)
//                        }
//                    }
//
//                    if let arrUnitOptionDict = dictData["unit_option"]?.arrayObject as? [[AnyHashable : Any]] {
//                        var arrUnitOption = [Unit_option]()
//                        for dict in arrUnitOptionDict {
//                            let unitOption = Unit_option.mr_import(from: dict, in: app_default_context)
//                            arrUnitOption.append(unitOption)
//                        }
//                    }
//
////                    CoreDataManager.sharedInstance.saveContext()
//                    response(.success(resDataDict: dictData, message: res.message))
//
//                } else {
//                    response(.failed(message: res.message))
//                }
//
//            } else {
//                response(.failed(message: res.message))
//            }
//
//    }) { (e) in
//        PbCircleDotLoader.sharedInstance.stopAnimation()
//        response(.failed(message: e.localizedDescription))
//    }
//}


////addRecipeResponse API
enum addRecipeResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func addRecipe(params : [String:Any] , indicator: Indicator = .window, response: @escaping (addRecipeResponse)->Void) {
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .recipeAdd,
        parameters: params,
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
            if res.success.mk_boolValue
            {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    print(dictData)
                    response(.success(dictData: dictData, message: res.message))
                    
                } else {
                    response(.failed(message: res.message))
                }
                
            }
            else
            {
                response(.failed(message: res.message))
            }
    }) { (e) in
        //        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.error(error: e.localizedDescription))
    }
}


////editRecipeResponse API
enum editRecipeResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func editRecipe(params : [String:Any] , indicator: Indicator = .window, response: @escaping (editRecipeResponse)->Void)
{
    //    PbCircleDotLoader.sharedInstance.startAnimation()
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .recipeEdit,
        parameters: params,
        Header: [:],
        handler: { (res) in
            
            print(res.Object)
            //            PbCircleDotLoader.sharedInstance.stopAnimation()
            if res.success.mk_boolValue
            {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    print(dictData)
                    response(.success(dictData: dictData, message: res.message))
                    
                } else {
                    response(.failed(message: res.message))
                }
                
            }
            else
            {
                response(.failed(message: res.message))
            }
    }) { (e) in
        //        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.error(error: e.localizedDescription))
    }
}





////getMyRecipe API
enum getRecipeDetailsResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func getRecipeDetails(recipe_id:Int64, is_from_cookbook : Int,indicator: Indicator = .window, response: @escaping (getRecipeDetailsResponse)->Void) {
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getRecipeDetails,
        parameters: ["recipe_id":recipe_id,"is_from_cookbook":is_from_cookbook],
        Header: [:],
        handler: { (res) in
            
            if res.success.mk_boolValue {
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    print(dictData)
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}



////getInterestedCategories API
enum DataResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func getEdmamNutritionData(arrIngredients:[String],indicator: Indicator = .window, response: @escaping (DataResponse)->Void) {
    
    let finalDict = ["ingr":arrIngredients]
    
    guard let jsonData = try? JSONSerialization.data(withJSONObject: finalDict, options: .prettyPrinted) else {
        return
    }
    
    let headers = ["Content-Type": "application/json"]
    var url = URL.init(string: "https://api.edamam.com/api/nutrition-details")
//    url?.appending("app_id", value: "47379841")
//    url?.appending("app_key", value: "d28718060b8adfd39783ead254df7f92")
    url?.appending("app_id", value: "a0853e07")
    url?.appending("app_key", value: "f53844bd4bddda351fee662f9544dfe2")
    
    
    let request = NSMutableURLRequest(url: url!,
                                      cachePolicy: .reloadIgnoringCacheData,
                                      timeoutInterval: 70.0)
    request.httpMethod = "POST"
    request.allHTTPHeaderFields = headers
    
    let json = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
    if let json = json {
        print(json)
    }
    request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
    
    let session = URLSession.shared
    let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, res, error) -> Void in
        
        DispatchQueue.main.sync {
            if (error != nil) {
                
                response(.failed(message: error?.localizedDescription ?? "error"))
            } else {
                if let data = data {
                    do {
                        if let dictIngredients = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                            response(.success(dictData: dictIngredients, message: "Success"))
                        } else {
                            response(.failed(message: "Parse error"))
                        }
                    } catch {
                        print(error.localizedDescription)
                        response(.failed(message: "Parse error"))
                    }
                } else {
                    response(.failed(message: "Parse error"))
                }
            }
        }
    })
    
    dataTask.resume()
    
}


////getMyRecipe API
enum getReviewRatingDetailsResponse
{
    case success(data: Data, dictData:Dictionary<String,Any>, message: String)
    case failed(message: String)
    case error(error: String)
}

func getReviewRatingDetails(recipe_id:Int64, start:Int, limit:Int, indicator: Indicator = .window, response: @escaping (getReviewRatingDetailsResponse)->Void) {
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getReviewlist,
        parameters: ["recipe_id":recipe_id,
                     "start":start,
                     "limit":limit],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                if let resData = res.resData, let dictData = res.Object as? Dictionary<String,Any>  {
                    response(.success(data: resData, dictData: dictData , message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}



////getMyRecipe API
enum submitReviewResponse
{
    case success(message: String)
    case failed(message: String)
    case error(error: String)
}

func submitReview(recipe_id:Int64, rating:Int, review:String, indicator: Indicator = .window, response: @escaping (submitReviewResponse)->Void) {
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getGiveRate,
        parameters: ["recipe_id":recipe_id,
                     "rating":rating,
                     "review":review],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                if let _ = res.Object as? Dictionary<String,Any>  {
                    response(.success(message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}

////getMealPlanResponse API
enum getMealPlanResponse
{
    case success(data: Data, dictData:Dictionary<String,Any>, message: String)
    case failed(message: String)
    case error(error: String)
}

func getMealPlan(date:Int64, indicator: Indicator = .window, response: @escaping (getMealPlanResponse)->Void) {
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getMealPlanList,
        parameters: ["date":date],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                if let dictData = res.Object as? Dictionary<String,Any>, let data = res.resData {
                    response(.success(data: data, dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}


////getMealPlanResponse API
enum getAllIngsByDatesResponse
{
    case success(data: Data, dictData:Dictionary<String,Any>, message: String)
    case failed(message: String)
    case error(error: String)
}

func getAllIngsByDates(dates:String, indicator: Indicator = .window, response: @escaping (getAllIngsByDatesResponse)->Void) {
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getAllIngsByDates,
        parameters: ["dates":dates],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                if let dictData = res.Object as? Dictionary<String,Any>, let data = res.resData {
                    response(.success(data: data, dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}

////getMealPlanResponse API
enum getMealDatesByMonthsRes
{
    case success(data: Data, dictData:Dictionary<String,Any>, message: String)
    case failed(message: String)
    case error(error: String)
}

func getMealDatesByMonths(month:Int, indicator: Indicator = .window, response: @escaping (getMealDatesByMonthsRes)->Void) {
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getMealDatesByMonths,
        parameters: ["month":month],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                if let dictData = res.Object as? Dictionary<String,Any>, let data = res.resData {
                    response(.success(data: data, dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}




////getFilterList API
enum getFilterListResponse
{
    case success(data: Data, dictData:Dictionary<String,Any>, message: String)
    case failed(message: String)
    case error(error: String)
}

func getFilterList(indicator: Indicator = .window, response: @escaping (getMealPlanResponse)->Void) {
    
    AlamofireModel.alamofireMethod(
        .get,
        apiAction: .getFilterList,
        parameters: [:],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                if let dictData = res.Object as? Dictionary<String,Any>, let data = res.resData {
                    response(.success(data: data, dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}



////getMyRecipe API
enum removeRecipeFromMealPlanResponse
{
    case success(message: String)
    case failed(message: String)
    case error(error: String)
}

func removeRecipeFromMealPlan(meal_plan_id:Int64, indicator: Indicator = .window, response: @escaping (removeRecipeFromMealPlanResponse)->Void) {
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .deleteMealPlan,
        parameters: ["meal_plan_id":meal_plan_id],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                if let _ = res.Object as? Dictionary<String,Any> {
                    response(.success(message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}


////addMealPlanResponse API
enum addMealPlanResponse
{
    case success(message: String)
    case failed(message: String)
    case error(error: String)
}

func addMealPlan(date:Int64, recipe_id:Int64, mealPlanTypes:String, is_saved_in_cookbook:Int, indicator: Indicator = .window, response: @escaping (addMealPlanResponse)->Void) {
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .addMealPlan,
        parameters: ["date":date,"recipe_id":recipe_id,"meal_plan_type":mealPlanTypes,"is_saved_in_cookbook":is_saved_in_cookbook],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                if let _ = res.Object as? Dictionary<String,Any> {
                    response(.success(message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}




enum updateMealPlanResponse
{
    case success(message: String)
    case failed(message: String)
    case error(error: String)
}

func updateMealPlan(date:Int64, meal_plan_type_id:Int64, indicator: Indicator = .window, response: @escaping (updateMealPlanResponse)->Void) {
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .updateMealPlan,
        parameters: ["date":date,"meal_plan_type_id":meal_plan_type_id],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                if let _ = res.Object as? Dictionary<String,Any> {
                    response(.success(message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}



////getInterestedCategories API
enum getNotificationListResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func getNotificationList(indicator: Indicator = .window, response: @escaping (getNotificationListResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getNotificationList,
        parameters: [:],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    print(dictData)
                    response(.success(dictData: dictData, message: res.message))
                    
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}

////getInterestedCategories API
enum getNotificationUnreadCountResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func getNotificationUnreadCount(indicator: Indicator = .window, response: @escaping (getNotificationUnreadCountResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getNotificationUnreadCount,
        parameters: [:],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    print(dictData)
                    response(.success(dictData: dictData, message: res.message))
                    
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}



////getInterestedCategories API
enum readNotificationResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func readNotification(notification_id:Int64, indicator: Indicator = .window, response: @escaping (readNotificationResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getNotificationRead,
        parameters: ["notification_id":notification_id],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}


////clear all notifications API
enum clearAllNotificationsResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func clearAllNotifications(indicator: Indicator = .window, response: @escaping (clearAllNotificationsResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .clearAllNotifications,
        parameters: [:],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}

////recipeApprovalApiCall API
enum recipeApprovalResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func recipeApproval(recipe_id:Int64, accept:Int, indicator: Indicator = .window, response: @escaping (readNotificationResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .recipeApproval,
        parameters: ["recipe_id":recipe_id,"accept":accept],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}


////recipeDeleteResponse API
enum recipeRemoveFromCookbookResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func recipeRemoveFromCookbook(cookbook_id:Int64, indicator: Indicator = .window, response: @escaping (recipeRemoveFromCookbookResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .recipeRemoveFromCookbook,
        parameters: ["cookbook_id":cookbook_id],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}


////recipeDeleteResponse API
enum recipeDeleteResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func recipeDelete(recipe_id:Int64, indicator: Indicator = .window, response: @escaping (recipeDeleteResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .recipeDelete,
        parameters: ["recipe_id":recipe_id],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}


////recipeAddToCollection API
enum recipeAddToCollectionResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func recipeAddToCollection(recipe_id:Int64, collection_id:Int64, indicator: Indicator = .window, response: @escaping (recipeAddToCollectionResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .recipeAddToCollection,
        parameters: ["collection_id": collection_id, "recipe_id": recipe_id],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}



////recipeAddToCollection API
enum clearRecentSearchResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func clearRecentSearch(indicator: Indicator = .window, response: @escaping (recipeAddToCollectionResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .get,
        apiAction: .clearRecentSearch,
        parameters: [:],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}


///recipeAddToCollection API
enum checkEdmamRecipeExistsInCookBookByUrlResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func checkEdmamRecipeExistsInCookBookByUrl(url:String, indicator: Indicator = .window, response: @escaping (recipeAddToCollectionResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .checkEdmamRecipeExistsInCookBookByUrl,
        parameters: ["url":url],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}


////recipeAddToCollection API
enum clearAllIngredientsShoppingListResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func clearAllIngredientsShoppingList(indicator: Indicator = .window, response: @escaping (clearAllIngredientsShoppingListResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .get,
        apiAction: .clearAllIngShoppingList,
        parameters: [:],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}

////recipeAddToCollection API
enum clearAllTickedIngredientsShoppingListResponse
{
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func clearAllTickedIngredientsShoppingList(indicator: Indicator = .window, response: @escaping (clearAllTickedIngredientsShoppingListResponse)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .get,
        apiAction: .clearAllTickedIngShoppingList,
        parameters: [:],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}



enum addRecipeIngsToShopListRes {
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func addRecipeIngsToShopList(recipes_list_with_ingredients: String, indicator: Indicator = .window, response: @escaping (addRecipeIngsToShopListRes)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .addRecipeIngsToShopList,
        parameters: ["recipes_list_with_ingredients":recipes_list_with_ingredients],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}

enum undoIngsShopListMealPlanScreenRes {
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func undoIngsShopListMealPlanScreen(recipes_list_with_ingredients: String, indicator: Indicator = .window, response: @escaping (undoIngsShopListMealPlanScreenRes)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .addRecipeIngsToShopListMealPlanScreen,
        parameters: ["recipes_list_with_ingredients":recipes_list_with_ingredients],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}



enum addRecipeIngsToShopListUndoRes {
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func addRecipeIngsToShopListUndo(recipe_id:Int64,is_from_cookbook:Int, ingredient_ids: String, indicator: Indicator = .window, response: @escaping (addRecipeIngsToShopListUndoRes)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .addRecipeIngsToShopListUndo,
        parameters: ["recipe_id":recipe_id,
                     "is_from_cookbook":is_from_cookbook,
                     "ingredient_ids":ingredient_ids],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}


enum addOtherIngToShopListRes {
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func addOREditOtherIngToShopList(shoping_list_item_id:String?, name:String, qty: String, unit:String, unit_id:Int64?, note:String, indicator: Indicator = .window, response: @escaping (addOtherIngToShopListRes)->Void)
{
    
    var params : [String : Any] = ["name":name,
                                   "qty":qty,
                                   "unit":unit,
                                   "note":note]
    if shoping_list_item_id != nil {
        params["shoping_list_item_id"] = shoping_list_item_id!
    }
    if unit_id != nil {
        params["unit_id"] = unit_id!
    }
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .addOtherIngsToShopList,
        parameters: params,
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}



enum removeIngFromShopListRes {
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func removeIngFromShopList(shoping_list_item_id:String, indicator: Indicator = .window, response: @escaping (removeIngFromShopListRes)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .removeIngFromShopList,
        parameters: ["shoping_list_item_id":shoping_list_item_id],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}


enum makeStrikeThroughIngRes {
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func makeStrikeThroughIng(shoping_list_item_id:String, is_strike:Int, indicator: Indicator = .window, response: @escaping (makeStrikeThroughIngRes)->Void) {
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .makeStrikeThrough,
        parameters: ["shoping_list_item_id":shoping_list_item_id,
                    "is_strike":is_strike],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}




enum getShopListRes {
    case success(dictData: [String:Any], message: String)
    case failed(message: String)
    case error(error: String)
}

func getShopList(shopping_list_sort_type:Int, start:Int, limit:Int, indicator: Indicator = .window, response: @escaping (getShopListRes)->Void)
{
    
    AlamofireModel.alamofireMethod(
        .post,
        apiAction: .getShopList,
        parameters: ["shopping_list_sort_type":shopping_list_sort_type,
                     "start":start,
                     "limit":limit],
        Header: [:],
        handler: { (res) in
            if res.success.mk_boolValue {
                
                if let dictData = JSON.init(res.Object).dictionaryObject {
                    response(.success(dictData: dictData, message: res.message))
                } else {
                    response(.failed(message: res.message))
                }
                
            } else {
                response(.failed(message: res.message))
            }
            
    }) { (e) in
        PbCircleDotLoader.sharedInstance.stopAnimation()
        response(.failed(message: e.localizedDescription))
    }
}







//func downloadImage(stringUrl:String,compHandler: @escaping (UIImage?)->()) {
//    Alamofire.request(stringUrl).responseImage { response in
//        debugPrint(response)
//        debugPrint(response.result)
//        if let image = response.result.value {
//            print("image downloaded: \(image)")
//            compHandler(image)
//        } else {
//            compHandler(nil)
//        }
//    }
//}
