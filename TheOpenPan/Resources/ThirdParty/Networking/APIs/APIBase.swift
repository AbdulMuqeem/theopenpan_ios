//
//  APIBase.swift
//

import Foundation
import UIKit
import Alamofire
import Firebase
import SwiftyJSON

enum Indicator
{
    case inside(view: UIView)
    case grayedOut(view: UIView)
    case window
    case none
    
    func show()
    {
        switch self {
        case .grayedOut(let view):
            view.isUserInteractionEnabled = false
            view.alpha = 0.5
        case .inside(let view):
            view.isUserInteractionEnabled = false
            view.startActivityIndicator()
        case .window:
            UIApplication.shared.keyWindow?.isUserInteractionEnabled = false
            UIApplication.shared.keyWindow?.startActivityIndicator()
        case .none:
            break
        }
    }
    
    func hide()
    {
        switch self {
        case .grayedOut(let view):
            view.isUserInteractionEnabled = true
            view.alpha = 1.0
        case .inside(let view):
            view.isUserInteractionEnabled = true
            view.stopActivityIndicator()
        case .window:
            UIApplication.shared.keyWindow?.isUserInteractionEnabled = true
            UIApplication.shared.keyWindow?.stopActivityIndicator()
        case .none:
            break
        }
    }
    
}

//
// Code to manage global Authentication Header
// This header will be removed from session on logout
//
var apiAuthHeader = authHeader.sharedInstance
private var _sharedAuth = authHeader()

class authHeader : BaseModel {
    
    var value           : String = ""
    var type            : String = "Bearer"
    
    let kAccessTokenType  = "app_token_type"
    let kAccessTokenValue = "app_token_value"
    let userDefault = UserDefaults.standard
    
    
    class var sharedInstance: authHeader
    {
        if _sharedAuth.identifier == nil
        {
            _sharedAuth.loadFromDefault()
        }
        return _sharedAuth
    }
    
    override init() {
        super.init()
    }
    
    override init(info: JSON) {
        super.init(info: info)
    }
    
    override func updateInfo(_ info: JSON)
    {
        if let _token = info["token"].string
        {
            print("Token \(_token)")
            value = _token
        }
        if let _type = info["type"].string
        {
            type = _type
        }
        
        let authorization = "\(type) \(value)"
        header = ["Authorization" : authorization]
        
        saveToDefault()
    }
    
    func token() -> String  {
        return "\(type) \(value)"
    }
    
    // MARK - Save to user default
    func saveToDefault()
    {
        userDefault.set(type, forKey: kAccessTokenType)
        userDefault.set(value, forKey: kAccessTokenValue)
        userDefault.synchronize()
    }
    
    func loadFromDefault()
    {
        if let tokenType = userDefault.object(forKey: kAccessTokenType) as? String {
            type = tokenType
        }
        if let tokenValue = userDefault.object(forKey: kAccessTokenValue) as? String {
            value = tokenValue
        }
    }
    
    func logout()
    {
        userDefault.removeObject(forKey: kAccessTokenType)
        userDefault.removeObject(forKey: kAccessTokenValue)
        userDefault.synchronize()
        
        apiAuthHeader.value = ""
        
        // create new token
        //AppInstance.manageAPIToken(_guestNotificationEnabled: guestNotificationEnabled)
        
        AppInstance.manageAPIToken(isPushEnable: isPushEnabled) { (true) in
            
            print("Success")
            
        }
        
    }
    
}
