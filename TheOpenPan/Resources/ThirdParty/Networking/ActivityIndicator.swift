//
//  ActivityIndicator.swift
//

import UIKit

fileprivate var indicators: [UIView:UIView] = [:]

extension UIView
{
    func startActivityIndicator()
    {
        if indicators[self] == nil
        {
            configure(view: self)
        }
    }
    
    func stopActivityIndicator()
    {
        if let view = indicators[self]
        {
            view.removeFromSuperview()
            indicators[self] = nil
        }
    }
}

fileprivate func configure(view: UIView)
{
    let _uiview_activityIndicator = UIView(frame: CGRect(x: 0,y: 0,width: 70,height: 70))
    _uiview_activityIndicator.clipsToBounds = false
    _uiview_activityIndicator.layer.masksToBounds = false
    _uiview_activityIndicator.layer.shadowOpacity = 1.0
    _uiview_activityIndicator.layer.cornerRadius = 5
    _uiview_activityIndicator.layer.shadowOffset = CGSize(width: 0, height: 3)
    _uiview_activityIndicator.layer.shadowColor = UIColor.darkGray.cgColor
    _uiview_activityIndicator.backgroundColor = UIColor.white.withAlphaComponent(1.0)
    
    let indicator = UIActivityIndicatorView(frame: _uiview_activityIndicator.bounds)
    indicator.style = .gray
    indicator.startAnimating()
    
    _uiview_activityIndicator.addSubview(indicator)
    indicators[view] = _uiview_activityIndicator
    view.addSubview(_uiview_activityIndicator)
    _uiview_activityIndicator.center = view.center
}


