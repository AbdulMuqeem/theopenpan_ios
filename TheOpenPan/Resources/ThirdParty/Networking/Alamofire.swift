//
//  Alamofire.swift
//
//

import Foundation
import Alamofire
import Firebase
import Crashlytics
import SwiftyJSON

enum Environment: String {
    case Production = ""
    case Prod = "https://app.theopenpan.sg/api/v1"
    case Dev = "http://development.peerbits.in:2004/api/v1"
    case Staging = "https://api-staging.theopenpan.sg/api/v1"
}

//API Environment set up
let apiEnvironment : Environment = .Staging
// Base Api URL
var baseURL: String = apiEnvironment.rawValue


var header: [String:String] = [:]
typealias reachability = (_ isReachable: Bool) -> Void

enum APIAction : String
{
    case getToken                       = "/common/gettoken"
    
    case socialLogin                    = "/auth/user-social"
    case getInterestedCategory          = "/common/getinterestedcategory"
    case setinterestedcategory          = "/auth/setinterestedcategory"
    case getNotificationList            = "/notification/list"
    case getNotificationUnreadCount     = "/notification/unreadcount"
    case getNotificationRead            = "/notification/read"
    case clearAllNotifications          = "/notification/clear"
    
    case recipeApproval                 = "/recipe/updatestatus"
    case recipeDelete                   = "/recipe/delete"
    case recipeRemoveFromCookbook       = "/collection/removerecipe"
    case recipeAddToCollection          = "/collection/addrecipe"
    case clearRecentSearch              = "/recipe/clearrecentsearch"
    case checkEdmamRecipeExistsInCookBookByUrl          = "/recipe/checkreciprbyurl"
    
    
    case clearAllIngShoppingList        = "/shop/clear"
    case clearAllTickedIngShoppingList  = "/shop/remove"
    
    case addRecipeIngsToShopList        = "/shop/add"
    case addRecipeIngsToShopListMealPlanScreen          = "/shop/undomulti"
    case addRecipeIngsToShopListUndo    = "/shop/undo"
    
    case addOtherIngsToShopList         = "/shop/addother"
    case makeStrikeThrough              = "/shop/strike"
    case getShopList                    = "/shop/list"
    case removeIngFromShopList          = "/shop/delete"
    
    case addRecipeToRecentSearch        = "/recipe/addrecentsearch"
    case parseScrapperRecipes           = "/common/scrap"
    
    
    
    case getMyRecipeList                = "/recipe/list"
    case getCollectionList              = "/collection/list"
    case getCollectionsRecipeList       = "/collection/recipelist"
    case getRecentSearchRecipeList      = "/recipe/recentsearch"
    
    case getParsingIngredients          = "/common/parse"
    
    case getRecipeListByCategories      = "/recipe/listbycategory"
    
    
    case getRecipeDetails       = "/recipe/detail"
    case addMealPlan            = "/mealplan/add"
    case updateMealPlan         = "/mealplan/edit"
    case getMealPlanList        = "/mealplan/list"
    case getAllIngsByDates      = "/mealplan/listbydates"
    case getMealDatesByMonths   = "/mealplan/getmealdatebymonth"
    
    case deleteMealPlan         = "/mealplan/delete"
    case getFilterList          = "/common/filterlist"
    case getReviewlist          = "/common/reviewlist"
    case getGiveRate            = "/common/giverate"
    
    
    
    case masterData             = "/common/masterdata"
    case logout                 = "/auth/logout"
    case signIn                 = "/auth/login"
    case signUp                 = "/auth/register"
    case forgotPassword         = "/auth/forgot-password"
    case getTerms               = "/common/getterms"
    case changePassword         = "/auth/change-password"
    case updateProfile          = "/auth/edit-profile"
    case getProfile             = "/auth/profile"
    case contactUs              = "/common/contact-us"
    case collectionAdd          = "/collection/add"
    
    case recipeAdd              = "/recipe/add"
    case recipeEdit             = "/recipe/edit"
    
    
    
    case reSendOTP              = "user/resendotp"
    case verifyOTP              = "user/verifyotp"
    
    
    
    
    
    
    
    case resetPassword          = "user/resetpassword"
    case resendEmail            = "user/resendemail"
    case notificationSetup      = "user/notification_setting"
    case cmslinks               = "user/getcms"
    case getReferralData        = "user/get_referral_data"
    case changeLanguage         = "user/language_change"
    
    
    
    case getAllCategoriesList   = "5e26a8ab2f00007000a4f552"
}

class AlamofireResponse
{
    var Object          : AnyObject
    var code            : Int = 0
    var message         : String = ""
    var success         : Int = 0
    var resData         : Data?
    
    init(Object: AnyObject, code: Int, message: String, success: Int, resData:Data?)
    {
        self.Object         = Object
        self.code           = code
        self.message        = message
        self.success        = success
        self.resData        = resData
    }
}


class AlamofireModel: NSObject
{
    
    typealias CompletionHandler = (_ response:AlamofireResponse) -> Void
    typealias ErrorHandler = (_ error : Error) -> Void
    
    @discardableResult class func alamofireMethod(_ method: Alamofire.HTTPMethod, stringUrl:String?=nil, apiAction: APIAction, parameters : [String : Any], Header: [String: String], retryCount: Int = 3, handler:@escaping CompletionHandler, errorhandler : @escaping ErrorHandler) -> DataRequest?
    {
        var header = Header
        header["Connection"] = "Close"
        // code to set auth header
        if !apiAuthHeader.value.isEmpty
        {
            header["Authorization"] = apiAuthHeader.token()
        }
        
        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "apple.com")
        
        reachabilityManager?.startListening(onUpdatePerforming: { (status) in
            
        })
        
        if let r = reachabilityManager
        {
            switch r.isReachable
            {
            case false:
                
                print("unreachable, retry no. -> " + (abs(retryCount - 3)).description)
                
                if retryCount == 0
                {
                    errorhandler(NSError(domain: "No Network", code: 0, userInfo: nil))
                }
                else
                {
                    usleep(10000) // = 0.01 seconds
                    alamofireMethod(method, apiAction: apiAction, parameters: parameters, Header: Header, retryCount: (retryCount-1), handler: handler, errorhandler: errorhandler)
                }
            case true:
                print("reachable")
                
                //Not reachable
                var alamofireManager : Alamofire.Session?
                
                var UrlFinal = ""
                do { try UrlFinal = baseURL + apiAction.rawValue.asURL().absoluteString } catch {}
                if let stringUrl = stringUrl {
                    UrlFinal = stringUrl
                }
                
                alamofireManager = Alamofire.Session.default
                alamofireManager?.session.configuration.timeoutIntervalForRequest = 31
                alamofireManager?.session.configuration.timeoutIntervalForResource = 31
                
                var _parameters = parameters
                _parameters["device_type"] = "I"
                _parameters["device_id"] = AppInstance.deviceToken //?? "123" //TODO : remove static value
                
                print(UrlFinal)
                print(header)
                print(_parameters)
                
                let dataRequest = alamofireManager?.request(UrlFinal, method: method, parameters: _parameters, encoding: URLEncoding.default, headers: HTTPHeaders.init(header)).responseJSON(completionHandler: { (response) in
                    
                    if response.data != nil
                    {
                        if let val = response.value
                        {
                            var message = ""
                            let code = response.response?.statusCode ?? 200
                            var responseData : AnyObject = val as AnyObject
                            var success = 0
                            
                            print(responseData)
                            if let _success = (val as AnyObject).value(forKey: "success") as? Int
                            {
                                //code = _success
                                
                                success = _success
                                
                                if _success == 1
                                {
                                    if let _data = (val as AnyObject).value(forKey: "data")
                                    {
                                        responseData = _data as AnyObject
                                        
                                        if let m = responseData.value(forKey: "message")
                                        {
                                            message = m as! String
                                        }
                                    }
                                }
                                else
                                {
                                    if let _error = (val as AnyObject).value(forKey: "error") as? [String]
                                    {
                                        if _error.count > 0
                                        {
                                            message = _error[0]
                                            if _error[0] == "Unauthorized" {
                                                AppInstance.goToLoginScreenPage(transition: true)
                                                kCurrentUser.logOut()
                                                return
                                            }
                                        }
                                    }
                                    
                                    if response.response?.statusCode == 401
                                    {
                                        if kCurrentUser.user_id == 0
                                        {
                                            if let msg = (val as AnyObject).value(forKey: "message") as? String
                                            {
                                                message = msg
                                            }
                                            AppInstance.goToLoginScreenPage(transition: true)
                                            kCurrentUser.logOut()
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if response.response?.statusCode == 401
                                {
                                    if let msg = (val as AnyObject).value(forKey: "message") as? String
                                    {
                                        message = msg
                                    }
                                    if kCurrentUser.user_id != 0
                                    {
                                        AppInstance.goToLoginScreenPage(transition: true)
                                    }
                                    kCurrentUser.logOut()
                                }
                            }
                            handler(AlamofireResponse(Object: responseData, code: code, message: message, success: success, resData: response.data))
                        }
                    }
                    else
                    {
                        if response.response?.statusCode == 401
                        {
                            AppInstance.goToLoginScreenPage(transition: true)
                            return
                        }
                        
                        if retryCount == 0
                        {
                            if let err = response.error {
                                Crashlytics.sharedInstance().recordError(err)
                                errorhandler(err)
                            }
                            
                        }
                        else
                        {
                            usleep(10000) // = 0.01 seconds
                            print("unreachable, retry no. -> " + (abs(retryCount - 3)).description)
                            alamofireMethod(method, apiAction: apiAction, parameters: parameters, Header: header, retryCount: (retryCount-1), handler: handler, errorhandler: errorhandler)
                        }
                    }
                })
                
                return dataRequest
            }
            
            return nil
        }
        return nil
    }
    
    class func alamofireMethodWithImages(_ method: Alamofire.HTTPMethod, apiAction: APIAction, parameters : [String : Any], Header: [String: String], retryCount: Int = 3, images : [String : UIImage], handler:@escaping CompletionHandler, errorhandler : @escaping ErrorHandler)
    {
        
        var header = Header
        header["Connection"] = "Close"
        // code to set auth header
        if !apiAuthHeader.value.isEmpty{
            header["Authorization"] = apiAuthHeader.token()
        }
        
        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "apple.com")
        
        reachabilityManager?.startListening(onUpdatePerforming: { (status) in
            
        })
        
        if let r = reachabilityManager
        {
            switch r.isReachable
            {
            case false:
                
                print("unreachable, retry no. -> " + (abs(retryCount - 3)).description)
                
                if retryCount == 0
                {
                    errorhandler(NSError(domain: "No Network", code: 0, userInfo: nil))
                }
                else
                {
                    usleep(10000) // = 0.01 seconds
                    alamofireMethodWithImages(method, apiAction: apiAction, parameters: parameters, Header: header, retryCount: (retryCount-1), images: images, handler: handler, errorhandler: errorhandler)
                }
            case true:
                print("reachable")
                
                //Not reachable
                var alamofireManager : Alamofire.Session?
                
                var UrlFinal = ""
                do
                {
                    try UrlFinal = baseURL + apiAction.rawValue.asURL().absoluteString
                }catch{}
                
                alamofireManager = Alamofire.Session.default
                alamofireManager?.session.configuration.timeoutIntervalForRequest = 31
                alamofireManager?.session.configuration.timeoutIntervalForResource = 31
                
                var _parameters = parameters
                _parameters["device_type"] = "I"
                _parameters["device_id"] = AppInstance.deviceToken //?? ""
                
                print(UrlFinal)
                print(header)
                print(parameters)
                
                AF.upload(multipartFormData: { multipartFormData in
                    multipartFormData.append(Data("one".utf8), withName: "one")
                    multipartFormData.append(Data("two".utf8), withName: "two")
                }, to: UrlFinal, method:.post, headers : HTTPHeaders.init(header))
                    .responseJSON { response in
                        
                        if response.data != nil
                        {
                            if let val = response.value
                            {
                                var message = ""
                                let code = response.response?.statusCode ?? 200
                                var responseData : AnyObject = val as AnyObject
                                var success = 0
                                
                                if let _success = (val as AnyObject).value(forKey: "success") as? Int
                                {
                                    //code = _success
                                    success = _success
                                    
                                    if _success == 1
                                    {
                                        if let _data = (val as AnyObject).value(forKey: "data")
                                        {
                                            responseData = _data as AnyObject
                                            
                                            if let m = responseData.value(forKey: "message")
                                            {
                                                message = m as! String
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if let _error = (val as AnyObject).value(forKey: "error") as? [String]
                                        {
                                            if _error.count > 0
                                            {
                                                message = _error[0]
                                            }
                                        }
                                        if response.response?.statusCode == 401
                                        {
                                            if kCurrentUser.user_id == 0
                                            {
                                                if let msg = (val as AnyObject).value(forKey: "message") as? String
                                                {
                                                    message = msg
                                                }
                                                AppInstance.goToLoginScreenPage(transition: true)
                                                kCurrentUser.logOut()
                                            }
                                        }
                                    }
                                }
                                
                                
                                handler(AlamofireResponse(Object: responseData, code: code, message: message, success: success, resData: response.data))
                                
                            }
                        }
                        else
                        {
                            if response.response?.statusCode == 401
                            {
                                AppInstance.goToLoginScreenPage(transition: true)
                                return
                            }
                            
                            
                            if retryCount == 0
                            {
                                if let err = response.error {
                                    Crashlytics.sharedInstance().recordError(err)
                                    errorhandler(response.error! as NSError)
                                }
                                
                            }
                            else
                            {
                                usleep(10000) // = 0.01 seconds
                                print("unreachable, retry no. -> " + (abs(retryCount - 3)).description)
                                alamofireMethodWithImages(method, apiAction: apiAction, parameters: parameters, Header: header, retryCount: (retryCount-1), images: images, handler: handler, errorhandler: errorhandler)
                            }
                        }
                        
                        
                        
                }
                
                //                alamofireManager?.upload(multipartFormData: { (multipartFormData) in
                //
                //                    for (key,value) in _parameters
                //                    {
                //                        multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
                //                    }
                //
                //                    for (key,value) in images
                //                    {
                //
                //                        if let data = value.jpegData(compressionQuality: 1)//UIImageJPEGRepresentation(value,1)
                //                        {
                //                            //multipartFormData.append(data, withName: key, mimeType: "image/jpeg")
                //                            multipartFormData.append(data, withName: key, fileName: "profile_image", mimeType: "image/jpeg")
                //                        }
                //                    }
                //
                //                }, to: UrlFinal, method : method, headers : header, encodingCompletion: { (encodingResult) in
                //
                //                    switch encodingResult {
                //                    case .success(let upload, _, _):
                //
                //                        upload.responseJSON(completionHandler: { (response) in
                //
                //
                //                            if response.result.isSuccess
                //                            {
                //                                if let val = response.result.value
                //                                {
                //                                    var message = ""
                //                                    let code = response.response?.statusCode ?? 200
                //                                    var responseData : AnyObject = val as AnyObject
                //                                    var success = 0
                //
                //                                    if let _success = (val as AnyObject).value(forKey: "success") as? Int
                //                                    {
                //                                        //code = _success
                //                                        success = _success
                //
                //                                        if _success == 1
                //                                        {
                //                                            if let _data = (val as AnyObject).value(forKey: "data")
                //                                            {
                //                                                responseData = _data as AnyObject
                //
                //                                                if let m = responseData.value(forKey: "message")
                //                                                {
                //                                                    message = m as! String
                //                                                }
                //                                            }
                //                                        }
                //                                        else
                //                                        {
                //                                            if let _error = (val as AnyObject).value(forKey: "error") as? [String]
                //                                            {
                //                                                if _error.count > 0
                //                                                {
                //                                                    message = _error[0]
                //                                                }
                //                                            }
                //                                            if response.response?.statusCode == 401
                //                                            {
                //                                                if kCurrentUser.user_id == 0
                //                                                {
                //                                                    if let msg = (val as AnyObject).value(forKey: "message") as? String
                //                                                    {
                //                                                        message = msg
                //                                                    }
                //                                                    AppInstance.goToLoginScreenPage(transition: true)
                //                                                    kCurrentUser.logOut()
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //
                //
                //                                    handler(AlamofireResponse(Object: responseData, code: code, message: message, success: success, resData: response.data))
                //
                //                                }
                //                            }
                //                            else
                //                            {
                //                                if response.response?.statusCode == 401
                //                                {
                //                                    AppInstance.goToLoginScreenPage(transition: true)
                //                                    return
                //                                }
                //
                //
                //                                if retryCount == 0
                //                                {
                //                                    Crashlytics.sharedInstance().recordError(response.result.error!)
                //                                    errorhandler(response.result.error! as NSError)
                //                                }
                //                                else
                //                                {
                //                                    usleep(10000) // = 0.01 seconds
                //                                    print("unreachable, retry no. -> " + (abs(retryCount - 3)).description)
                //                                    alamofireMethodWithImages(method, apiAction: apiAction, parameters: parameters, Header: header, retryCount: (retryCount-1), images: images, handler: handler, errorhandler: errorhandler)
                //                                }
                //                            }
                //
                //
                //                        })
                //
                //                    case .failure(let encodingError):
                //                        print(encodingError)
                //                        errorhandler(encodingError )
                //                    }
                //
                //                })
                
                
            }
        }
    }
    
    //    class func alamofireMethodWithMultipleImages(_ method: Alamofire.HTTPMethod, apiAction: APIAction, parameters : [String : Any], Header: [String: String], retryCount: Int = 3, images : [[String : UIImage]], handler:@escaping CompletionHandler, errorhandler : @escaping ErrorHandler)
    //    {
    //
    //        var header = Header
    //        header["Connection"] = "Close"
    //        // code to set auth header
    //        if !apiAuthHeader.value.isEmpty{
    //            header["Authorization"] = apiAuthHeader.token()
    //        }
    //
    //        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "apple.com")
    //
    //        reachabilityManager?.startListening()
    //
    //        if let r = reachabilityManager
    //        {
    //            switch r.isReachable
    //            {
    //            case false:
    //
    //                print("unreachable, retry no. -> " + (abs(retryCount - 3)).description)
    //
    //                if retryCount == 0
    //                {
    //                    errorhandler(NSError(domain: "No Network", code: 0, userInfo: nil))
    //                }
    //                else
    //                {
    //                    usleep(10000) // = 0.01 seconds
    //                    alamofireMethodWithMultipleImages(method, apiAction: apiAction, parameters: parameters, Header: header, retryCount: (retryCount-1), images: images, handler: handler, errorhandler: errorhandler)
    //
    //                }
    //            case true:
    //                print("reachable")
    //
    //                //Not reachable
    //                var alamofireManager : Alamofire.SessionManager?
    //
    //                var UrlFinal = ""
    //                do
    //                {
    //                    try UrlFinal = baseURL + apiAction.rawValue.asURL().absoluteString
    //
    //                }catch{}
    //
    //                alamofireManager = Alamofire.SessionManager.default
    //                alamofireManager?.session.configuration.timeoutIntervalForRequest = 31
    //                alamofireManager?.session.configuration.timeoutIntervalForResource = 31
    //
    //                var _parameters = parameters
    //                _parameters["device_type"] = "I"
    //                _parameters["device_id"] = AppInstance.deviceToken //?? ""
    //
    //                alamofireManager?.upload(multipartFormData: { (multipartFormData) in
    //
    //                    for (key,value) in _parameters {
    //                        multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
    //                    }
    //
    //                    for imagesObj in images{
    //                        for (key,value) in imagesObj {
    //
    //                            if let data = value.jpegData(compressionQuality: 1) {
    //                                multipartFormData.append(data, withName: key, mimeType: "image/jpeg")
    //                            }
    //                        }
    //                    }
    //
    //                }, to: UrlFinal, method : method, headers : header, encodingCompletion: { (encodingResult) in
    //
    //                    switch encodingResult {
    //                    case .success(let upload, _, _):
    //
    //                        upload.responseJSON(completionHandler: { (response) in
    //
    //
    //                            if response.result.isSuccess
    //                            {
    //                                if let val = response.result.value
    //                                {
    //                                    var message = ""
    //                                    let code = response.response?.statusCode ?? 200
    //                                    var responseData : AnyObject = val as AnyObject
    //                                    var success = 0
    //
    //                                    if let _success = (val as AnyObject).value(forKey: "success") as? Int
    //                                    {
    //                                        //code = _success
    //                                        success = _success
    //
    //                                        if _success == 1
    //                                        {
    //                                            if let _data = (val as AnyObject).value(forKey: "data")
    //                                            {
    //                                                responseData = _data as AnyObject
    //
    //                                                if let m = responseData.value(forKey: "message")
    //                                                {
    //                                                    message = m as! String
    //                                                }
    //                                            }
    //                                        }
    //                                        else
    //                                        {
    //                                            if let _error = (val as AnyObject).value(forKey: "error") as? [String]
    //                                            {
    //                                                if _error.count > 0
    //                                                {
    //                                                    message = _error[0]
    //                                                }
    //                                            }
    //                                            if response.response?.statusCode == 401
    //                                            {
    //                                                if kCurrentUser.user_id == 0
    //                                                {
    //                                                    if let msg = (val as AnyObject).value(forKey: "message") as? String
    //                                                    {
    //                                                        message = msg
    //                                                    }
    //                                                    AppInstance.goToLoginScreenPage(transition: true)
    //                                                    kCurrentUser.logOut()
    //                                                }
    //                                            }
    //                                        }
    //                                    }
    //
    //                                    handler(AlamofireResponse(Object: responseData, code: code, message: message, success: success, resData: response.data))
    //                                }
    //                            }
    //                            else
    //                            {
    //                                if response.response?.statusCode == 401
    //                                {
    //                                    //Go to login
    //                                    AppInstance.goToLoginScreenPage(transition: true)
    //                                    return
    //                                }
    //
    //
    //                                if retryCount == 0
    //                                {
    //                                    Crashlytics.sharedInstance().recordError(response.result.error!)
    //                                    errorhandler(response.result.error! as NSError)
    //                                }
    //                                else
    //                                {
    //                                    usleep(10000) // = 0.01 seconds
    //                                    print("unreachable, retry no. -> " + (abs(retryCount - 3)).description)
    //                                    alamofireMethodWithMultipleImages(method, apiAction: apiAction, parameters: parameters, Header: header, retryCount: (retryCount-1), images: images, handler: handler, errorhandler: errorhandler)
    //                                }
    //                            }
    //                        })
    //
    //                    case .failure(let encodingError):
    //                        print(encodingError)
    //                        errorhandler(encodingError )
    //                    }
    //
    //                })
    //
    //
    //            }
    //        }
    //    }
    class func reachabilityCheck( maxRetries: Int = 3, isRecheable: @escaping reachability)
    {
        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "apple.com")
        
        reachabilityManager?.startListening(onUpdatePerforming: { (status) in
            
        })
        
        if let r = reachabilityManager
        {
            switch r.isReachable
            {
            case false:
                print("unreachable, retry no. -> " + (abs(maxRetries - 3)).description)
                if maxRetries == 0
                {
                    isRecheable(false)
                }
                else
                {
                    usleep(10000) // = 0.01 seconds
                    reachabilityCheck(maxRetries: (maxRetries-1), isRecheable: isRecheable)
                }
            case true:
                
                isRecheable(true)
            }
        }
    }
    
    
    typealias CompletionHandlerExternal = (_ response:AnyObject) -> Void
    typealias ErrorHandlerExternal = (_ error : NSError) -> Void
    
    class func alamofireMethodExternalAPI(_ methods: Alamofire.HTTPMethod , url : URLConvertible , parameters : [String : Any],Header: [String: String], handler:@escaping CompletionHandlerExternal,errorhandler : @escaping ErrorHandlerExternal)
    {
        if (NetworkReachabilityManager()?.isReachable)! == false
        {
            AppInstance.showMessages(message: the_internet_connection_appears_to_be_offline)
            return
        }
        var alamofireManager : Alamofire.Session?
        
        var UrlFinal = ""
        do
        {
            try UrlFinal = url.asURL().absoluteString
        }catch{}
        
        print(UrlFinal)
        print(Header)
        print(parameters)
        print(methods.rawValue)
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 60
        configuration.timeoutIntervalForRequest = 60
        
        alamofireManager = Alamofire.Session(configuration: configuration)
        alamofireManager = Alamofire.Session.default
        
        alamofireManager?.request(UrlFinal, method: methods, parameters: parameters, encoding: URLEncoding.default, headers: HTTPHeaders.init(Header)).responseJSON(queue: .main, options: JSONSerialization.ReadingOptions.allowFragments, completionHandler: { (response) in
            
            
            if response.data != nil
            {
                if (response.value != nil)
                {
                    handler(response.value as AnyObject)
                }
            }
            else
            {
                errorhandler((response.error! as NSError))
            }
        })//.session.finishTasksAndInvalidate()
    }
}



