// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Master_data.swift instead.

import Foundation
import CoreData

public enum Master_dataRelationships: String {
    case categories = "categories"
    case time_option = "time_option"
    case unit_option = "unit_option"
}

open class _Master_data: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "Master_data"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    @nonobjc
    open class func fetchRequest() -> NSFetchRequest<Master_data> {
        return NSFetchRequest(entityName: self.entityName())
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _Master_data.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    // MARK: - Relationships

    @NSManaged open
    var categories: NSSet

    open func categoriesSet() -> NSMutableSet {
        return self.categories.mutableCopy() as! NSMutableSet
    }

    @NSManaged open
    var time_option: NSSet

    open func time_optionSet() -> NSMutableSet {
        return self.time_option.mutableCopy() as! NSMutableSet
    }

    @NSManaged open
    var unit_option: NSSet

    open func unit_optionSet() -> NSMutableSet {
        return self.unit_option.mutableCopy() as! NSMutableSet
    }

}

extension _Master_data {

    open func addCategories(_ objects: NSSet) {
        let mutable = self.categories.mutableCopy() as! NSMutableSet
        mutable.union(objects as Set<NSObject>)
        self.categories = mutable.copy() as! NSSet
    }

    open func removeCategories(_ objects: NSSet) {
        let mutable = self.categories.mutableCopy() as! NSMutableSet
        mutable.minus(objects as Set<NSObject>)
        self.categories = mutable.copy() as! NSSet
    }

    open func addCategoriesObject(_ value: Recipe_category) {
        let mutable = self.categories.mutableCopy() as! NSMutableSet
        mutable.add(value)
        self.categories = mutable.copy() as! NSSet
    }

    open func removeCategoriesObject(_ value: Recipe_category) {
        let mutable = self.categories.mutableCopy() as! NSMutableSet
        mutable.remove(value)
        self.categories = mutable.copy() as! NSSet
    }

}

extension _Master_data {

    open func addTime_option(_ objects: NSSet) {
        let mutable = self.time_option.mutableCopy() as! NSMutableSet
        mutable.union(objects as Set<NSObject>)
        self.time_option = mutable.copy() as! NSSet
    }

    open func removeTime_option(_ objects: NSSet) {
        let mutable = self.time_option.mutableCopy() as! NSMutableSet
        mutable.minus(objects as Set<NSObject>)
        self.time_option = mutable.copy() as! NSSet
    }

    open func addTime_optionObject(_ value: Category_tag) {
        let mutable = self.time_option.mutableCopy() as! NSMutableSet
        mutable.add(value)
        self.time_option = mutable.copy() as! NSSet
    }

    open func removeTime_optionObject(_ value: Category_tag) {
        let mutable = self.time_option.mutableCopy() as! NSMutableSet
        mutable.remove(value)
        self.time_option = mutable.copy() as! NSSet
    }

}

extension _Master_data {

    open func addUnit_option(_ objects: NSSet) {
        let mutable = self.unit_option.mutableCopy() as! NSMutableSet
        mutable.union(objects as Set<NSObject>)
        self.unit_option = mutable.copy() as! NSSet
    }

    open func removeUnit_option(_ objects: NSSet) {
        let mutable = self.unit_option.mutableCopy() as! NSMutableSet
        mutable.minus(objects as Set<NSObject>)
        self.unit_option = mutable.copy() as! NSSet
    }

    open func addUnit_optionObject(_ value: Category_tag) {
        let mutable = self.unit_option.mutableCopy() as! NSMutableSet
        mutable.add(value)
        self.unit_option = mutable.copy() as! NSSet
    }

    open func removeUnit_optionObject(_ value: Category_tag) {
        let mutable = self.unit_option.mutableCopy() as! NSMutableSet
        mutable.remove(value)
        self.unit_option = mutable.copy() as! NSSet
    }

}

