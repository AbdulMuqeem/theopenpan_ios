// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Category_tag.swift instead.

import Foundation
import CoreData

public enum Category_tagAttributes: String {
    case is_selected = "is_selected"
    case is_time_option_category = "is_time_option_category"
    case tag_id = "tag_id"
    case tag_image_url = "tag_image_url"
    case tag_name = "tag_name"
}

public enum Category_tagRelationships: String {
    case master_data_time_parent = "master_data_time_parent"
    case master_data_unit_parent = "master_data_unit_parent"
    case tags_parent = "tags_parent"
}

open class _Category_tag: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "Category_tag"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    @nonobjc
    open class func fetchRequest() -> NSFetchRequest<Category_tag> {
        return NSFetchRequest(entityName: self.entityName())
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _Category_tag.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var is_selected: Bool // Optional scalars not supported

    @NSManaged open
    var is_time_option_category: Bool // Optional scalars not supported

    @NSManaged open
    var tag_id: Int64 // Optional scalars not supported

    @NSManaged open
    var tag_image_url: String?

    @NSManaged open
    var tag_name: String?

    // MARK: - Relationships

    @NSManaged open
    var master_data_time_parent: Master_data?

    @NSManaged open
    var master_data_unit_parent: Master_data?

    @NSManaged open
    var tags_parent: Recipe_category?

}

