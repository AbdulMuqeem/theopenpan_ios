// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Unit_option.swift instead.

import Foundation
import CoreData

public enum Unit_optionAttributes: String {
    case displayIndex = "displayIndex"
    case tag_id = "tag_id"
    case tag_name_plural = "tag_name_plural"
    case tag_name_singular = "tag_name_singular"
}

open class _Unit_option: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "Unit_option"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    @nonobjc
    open class func fetchRequest() -> NSFetchRequest<Unit_option> {
        return NSFetchRequest(entityName: self.entityName())
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _Unit_option.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var displayIndex: Int64 // Optional scalars not supported

    @NSManaged open
    var tag_id: Int64

    @NSManaged open
    var tag_name_plural: String?

    @NSManaged open
    var tag_name_singular: String?

    // MARK: - Relationships

}

