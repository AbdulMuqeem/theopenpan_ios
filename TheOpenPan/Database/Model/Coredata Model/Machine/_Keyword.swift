// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Keyword.swift instead.

import Foundation
import CoreData

public enum KeywordAttributes: String {
    case desc = "desc"
}

open class _Keyword: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "Keyword"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    @nonobjc
    open class func fetchRequest() -> NSFetchRequest<Keyword> {
        return NSFetchRequest(entityName: self.entityName())
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _Keyword.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var desc: String?

    // MARK: - Relationships

}

