// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Recipe_category.swift instead.

import Foundation
import CoreData

public enum Recipe_categoryAttributes: String {
    case category_id = "category_id"
    case category_name = "category_name"
    case displayIndex = "displayIndex"
}

public enum Recipe_categoryRelationships: String {
    case master_data_reverse = "master_data_reverse"
    case tags = "tags"
}

open class _Recipe_category: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "Recipe_category"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    @nonobjc
    open class func fetchRequest() -> NSFetchRequest<Recipe_category> {
        return NSFetchRequest(entityName: self.entityName())
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _Recipe_category.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var category_id: Int64 // Optional scalars not supported

    @NSManaged open
    var category_name: String?

    @NSManaged open
    var displayIndex: Int64 // Optional scalars not supported

    // MARK: - Relationships

    @NSManaged open
    var master_data_reverse: Master_data?

    @NSManaged open
    var tags: NSSet

    open func tagsSet() -> NSMutableSet {
        return self.tags.mutableCopy() as! NSMutableSet
    }

}

extension _Recipe_category {

    open func addTags(_ objects: NSSet) {
        let mutable = self.tags.mutableCopy() as! NSMutableSet
        mutable.union(objects as Set<NSObject>)
        self.tags = mutable.copy() as! NSSet
    }

    open func removeTags(_ objects: NSSet) {
        let mutable = self.tags.mutableCopy() as! NSMutableSet
        mutable.minus(objects as Set<NSObject>)
        self.tags = mutable.copy() as! NSSet
    }

    open func addTagsObject(_ value: Category_tag) {
        let mutable = self.tags.mutableCopy() as! NSMutableSet
        mutable.add(value)
        self.tags = mutable.copy() as! NSSet
    }

    open func removeTagsObject(_ value: Category_tag) {
        let mutable = self.tags.mutableCopy() as! NSMutableSet
        mutable.remove(value)
        self.tags = mutable.copy() as! NSSet
    }

}

