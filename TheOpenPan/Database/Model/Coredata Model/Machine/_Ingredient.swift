// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Ingredient.swift instead.

import Foundation
import CoreData

public enum IngredientAttributes: String {
    case id = "id"
    case is_bought = "is_bought"
    case is_multiple = "is_multiple"
    case name = "name"
    case note = "note"
    case qty = "qty"
    case shopping_list_item_id = "shopping_list_item_id"
    case unit_name = "unit_name"
}

open class _Ingredient: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "Ingredient"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    @nonobjc
    open class func fetchRequest() -> NSFetchRequest<Ingredient> {
        return NSFetchRequest(entityName: self.entityName())
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _Ingredient.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var id: Int64 // Optional scalars not supported

    @NSManaged open
    var is_bought: Bool // Optional scalars not supported

    @NSManaged open
    var is_multiple: Bool // Optional scalars not supported

    @NSManaged open
    var name: String?

    @NSManaged open
    var note: String?

    @NSManaged open
    var qty: String?

    @NSManaged open
    var shopping_list_item_id: String?

    @NSManaged open
    var unit_name: String?

    // MARK: - Relationships

}

