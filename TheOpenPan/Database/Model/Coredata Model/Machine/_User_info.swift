// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to User_info.swift instead.

import Foundation
import CoreData

public enum User_infoAttributes: String {
    case user_id = "user_id"
}

open class _User_info: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "User_info"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    @nonobjc
    open class func fetchRequest() -> NSFetchRequest<User_info> {
        return NSFetchRequest(entityName: self.entityName())
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _User_info.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var user_id: String?

    // MARK: - Relationships

}

