// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Direction.swift instead.

import Foundation
import CoreData

public enum DirectionAttributes: String {
    case desc = "desc"
    case step = "step"
}

open class _Direction: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "Direction"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    @nonobjc
    open class func fetchRequest() -> NSFetchRequest<Direction> {
        return NSFetchRequest(entityName: self.entityName())
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _Direction.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var desc: String?

    @NSManaged open
    var step: Int64 // Optional scalars not supported

    // MARK: - Relationships

}

