import Foundation

@objc(Unit_option)
open class Unit_option: _Unit_option {
	// Custom logic goes here.
    
    
    open override func willImport(_ data: Any) {
        if let dictData = data as? [String:Any] {
            print(dictData)
            let displayIndex = nextAvailble("displayIndex", forEntityName: "Unit_option", in: app_default_context)
            self.displayIndex = Int64(displayIndex?.intValue ?? 0)
        }
    }
    
}


