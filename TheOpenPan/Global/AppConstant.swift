//
//  Constant.swift
//  Trade Master
//
//  Created by Shahabuddin MacMini13 on 12/12/18.
//  Copyright © 2017 Shahabuddin MacMini13. All rights reserved.
//

import Foundation
import UIKit
import CoreData

//MARK:- *********************** APP INFO ****************************************
var APPNAME:String = "The Open Pan"

var APP_DB_NAME:String = "TheOpenPan"



///App store link
var APPSTORE_USER = "https://itunesconnect.com"
var AppDesc: String = ""
var AppContanctMail:String = "nasrullahpatel@gmail.com"
var gshareText : String = "Enjoy with the FuelSwipe app with people and his ideas by clicking here \(APPSTORE_USER)"
let themeColor = UIColor(hexString: "#EE4136")
///User Default Object
var DEFAULT = UserDefaults.standard

///PlaceHolder Color
var placeHolderColor = "#939393"

let imagePlaceholderProfile = #imageLiteral(resourceName: "user_placeholder")

///User Defualts Key
let language_state = "language_state"
let is_intro_viewed = "is_intro_viewed"


let kMapStyle = "[" +
    "  {" +
    "    \"featureType\": \"poi.business\"," +
    "    \"elementType\": \"all\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"visibility\": \"off\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"transit\"," +
    "    \"elementType\": \"labels.icon\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"visibility\": \"off\"" +
    "      }" +
    "    ]" +
    "  }" +
"]"

//MARK:- *********************** Common Struct List ****************************************
struct ScreenSizes
{
    static let SCREEN_SIZE          = UIScreen.main.bounds.size
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSizes.SCREEN_WIDTH, ScreenSizes.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSizes.SCREEN_WIDTH, ScreenSizes.SCREEN_HEIGHT)
}

let kMainScreen         = UIScreen.main.bounds
let kMainScreenWidth    = UIScreen.main.bounds.size.width
let kMainScreenHeight   = UIScreen.main.bounds.size.height

let ssw = UIScreen.main.bounds.width
let ssh = UIScreen.main.bounds.height

var currentTimeStamp = Date().timeIntervalSince1970
var distanceLimit = 200

//MARK:- *********************** enum List ****************************************

///App font Type
//enum appCustomFont : String
//{
//    case regular = "NunitoSans-Regular"
//    case light = "NunitoSans-Light"
//    case bold = "NunitoSans-Bold"
//    case semiBold = "NunitoSans-SemiBold"
//    case italic = "NunitoSans-Italic"
//}

///App font Type
enum enum_font : String {
    
    case black = "QanelasSoft-Black"
    case bold = "QanelasSoft-Bold"
    case semi_bold = "QanelasSoft-SemiBold"
    case extra_bold = "QanelasSoft-ExtraBold"
    case light = "QanelasSoft-Light"
    case medium = "QanelasSoft-Medium"
    case regular = "QanelasSoft-Regular"
    case thin = "QanelasSoft-Thin"
    
    case sail_regular = "Sail-Regular"
    
    func font(_ size:CGFloat) -> UIFont {
        return UIFont.init(name: self.rawValue, size: size)!
    }
    
}

///StoryBoard Name List
enum enum_storyboard : String {
    
    case Authentication             = "Authentication"
    case MealPlanTab                = "MealPlanTab"
    case ShoppingListTab            = "ShoppingListTab"
    case Home                       = "Home"
    case HomeBrowseRecipeVC         = "HomeBrowseRecipeVC"
    case RecipeDetailsVC            = "RecipeDetailsVC"
    case ReviewAndRating            = "ReviewAndRating"
    case CookBookTab                = "CookBookTab"
    case Settings                   = "SettingsTab"
    case AddRecipe                  = "AddRecipe"
    
    
    func getStoryboard() -> UIStoryboard {
        switch self {
        case .Authentication:
            return UIStoryboard.init(name: enum_storyboard.Authentication.rawValue, bundle: nil)
        case .MealPlanTab:
            return UIStoryboard.init(name: enum_storyboard.MealPlanTab.rawValue, bundle: nil)
        case .ShoppingListTab:
            return UIStoryboard.init(name: enum_storyboard.ShoppingListTab.rawValue, bundle: nil)
        case .Home:
            return UIStoryboard.init(name: enum_storyboard.Home.rawValue, bundle: nil)
        case .HomeBrowseRecipeVC:
            return UIStoryboard.init(name: enum_storyboard.HomeBrowseRecipeVC.rawValue, bundle: nil)
        case .RecipeDetailsVC:
            return UIStoryboard.init(name: enum_storyboard.RecipeDetailsVC.rawValue, bundle: nil)
        case .CookBookTab:
            return UIStoryboard.init(name: enum_storyboard.CookBookTab.rawValue, bundle: nil)
        case .Settings:
            return UIStoryboard.init(name: enum_storyboard.Settings.rawValue, bundle: nil)
        case .AddRecipe:
            return UIStoryboard.init(name: enum_storyboard.AddRecipe.rawValue, bundle: nil)
        case .ReviewAndRating:
            return UIStoryboard.init(name: enum_storyboard.ReviewAndRating.rawValue, bundle: nil)
            
        }
    }
    
}

enum enum_meal_plan_type : Int {
    case breakfast      = 1
    case lunch          = 2
    case snack          = 3
    case dinner         = 4

    func getName() -> String {
        switch self {
        case .breakfast :
            return "breakfast".capitalized
        case .lunch :
            return "lunch".capitalized
        case .snack :
            return "snack".capitalized
        case .dinner :
            return "dinner".capitalized
        }
    }
}

enum enum_media_type : Int {
    case image      = 1
    case video      = 2

    func getName() -> String {
        switch self {
        case .image :
            return "image"
        case .video :
            return "video"
        }
    }
}

enum enum_recipe_owner_type : Int {
    
    case user_recipe            = 1
    case admin_recipe           = 2
    case third_party_recipe     = 3

    func getName() -> String {
        switch self {
        case .user_recipe :
            return "user_recipe"
        case .admin_recipe :
            return "admin_recipe"
        case .third_party_recipe :
            return "third_party_recipe"
        }
    }
}

///MARK: -  Social types
enum enum_social_type: Int {
    case google     = 1
    case facebook   = 2
    case apple      = 3
}


//enum AWS3Variable : String
//{
//    case AWS_POOL_ID = "ap-south-1:0da7883c-3b1b-469d-9186-c48c85bb0fd4"
//    case BUCKET_NAME = "theopenpan"
//}

///MARK: - Current Language Type -
enum Enum_LanguageType: Int
{
    case English = 0
    case Spanish = 1
}
///MARK: - Enum verify OTP Type
enum VerifyOtpTypesAPI: String
{
    case Default = ""
    case ForgotPassword = "f"
    case EditProfile = "e"
}

///MARK: - Enum for OTP Verification Screen -
enum Enum_OTP_verification: String
{
    case reset = "reset"
    case sign_up = "sign_up"
    case edit_profile = "edit_profile"
}
///MARK: - Diesel Types -
enum Enum_DieselType: Int
{
    case diesel = 1//"D"
    case red_diesel = 2//"RD"
    case none = 3
}
///MARK: - Enum CMS Page title
enum CMS_PAGE_Title: String
{
    case about_us = "About Us"
    case terms_condition = "Terms & Conditions"
    case privacy_policy = "Privacy Policy"
}
///MARK: - Check Vehicle API Parameter Value -
enum VehicleCheckParameter: String
{
    case api_nullitems  = "1"
    case auth_apikey = "dd3f0c3a-638e-4a9e-a079-b3951ff64a52"  //Testing
    //case auth_apikey = "bc6a14a3-d4a5-41cc-af65-c92b031ec268"  //Live
}


let GOOGLE_CLIENT_ID = "19345432833-003m0ru817vlrg6mk6j7tqb0aalj3nsn.apps.googleusercontent.com"

let fb_app_id = "1571827616189477"
let fb_app_secret = "ac127b9bbf7fc5de886959b6c6ce0c55"

let base_url = "www.theopenpan.com"

let apde = UIApplication.shared.delegate as! AppDelegate

let app_default_context = CoreDataManager.sharedInstance.managedObjectContext
//let app_memory_context = NSManagedObjectContext.init(concurrencyType: .mainQueueConcurrencyType)

