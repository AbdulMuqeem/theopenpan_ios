//
//  Colors.swift
//

import Foundation
import UIKit


let appColors = AppColors()

class AppColors : UIColor
{
    let orange = UIColor.orange
    let greylight = UIColor.lightGray
    //let themeColor = UIColor(red: 249.0/255.0, green: 130.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    let themeColor = UIColor(red: 252.0/255.0, green: 91.0/255.0, blue: 95.0/255.0, alpha: 1.0)
    
    let placeHolderColor = UIColor.lightGray.withAlphaComponent(0.5)
    
    
    let ErrorColor = UIColor(red: 255.0/255.0, green: 60.0/255.0, blue: 70.0/255.0, alpha: 1.0)
    let SuccessColor = UIColor(red: 0.0/255.0, green: 184.0/255.0, blue: 159.0/255.0, alpha: 1.0)
    
    
    let Upcoming = UIColor(red: 12.0/255.0, green: 142.0/255.0, blue: 214.0/255.0, alpha: 1.0)
    let Requested = UIColor(red: 249.0/255.0, green: 130.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    let Completed = UIColor(red: 214.0/255.0, green: 214.0/255.0, blue: 214.0/255.0, alpha: 1.0)
    let Cancelled = UIColor(red: 249.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    
    let viewBGColor = UIColor(hexString: "#ECDCEA", alpha: 0.5)
    let viewSelectedColor = UIColor(hexString: "#AF71A5")
    
    let ActiveDidBeginView = UIColor(red: 50.0/255.0, green: 56.0/255.0, blue: 77.0/255.0, alpha: 1)
    let DeActiveDidEndView = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1)
    
    let viewUserListBgNormal = UIColor.white
    let viewUserListBgOther = UIColor(red: 242.0/255.0, green: 245.0/255.0, blue: 247.0/255.0, alpha: 1)
}


