//
//  PBValidation.swift
//  TradeMaster
//
//  Created by Shahabuddin on 19/12/18.
//  Copyright © 2018 peerbits. All rights reserved.
//

import UIKit

/// ValidationParam Declaration. Check Example in Quick Help Section
///
///
///     let ValidateForm: ValidationParam =
///     [
///     .REQ_EMAIL: (0,txtEmail),
///     .VALID_EMAIL: (1,txtEmail),
///     .REQ_PASSWORD: (2,txtPassword),
///     .VALID_PASSWORD: (3,txtPassword),
///     .REQ_CONFIRM_PASS: (4,txtConfirmPassword),
///     ]
///
///     print(ValidateForm)
///
typealias ValidationParam = [validationType: (priority: Int,fieldToValidate: UITextField)]

enum validationType : String
{
    case REQ_NAME           = "reqName"
    case REQ_FULLNAME       = "reqFullName"
    case REQ_FIRSTNAME      = "reqFirstName"
    case REQ_LASTNAME       = "reqLastName"
    case REQ_EMAIL          = "reqEmail"
    case REQ_PASSWORD       = "reqPassword"
    case REQ_CONFIRM_PASS   = "reqConfirmPass"
    case REQ_COUNTRYCODE    = "reqCountryCode"
    case REQ_MOBILE         = "reqMobile"
    case REQ_PIN            = "reqPin"
    case REQ_PIN2           = "reqPin2"
    case REQ_PIN3           = "reqPin3"
    case REQ_PIN4           = "reqPin4"
    case REQ_LOCATION       = "reqLocation"
    
    case VALID_EMAIL        = "validEmail"
    case VALID_PASSWORD     = "validPassword"
    case VALID_MOBILE       = "validMobile"
    case VALID_URL          = "validUrl"
    //case VALID_PIN          = "validPin"
    
    case COMPARE_FIELD      = "compareField"
    
    case ContainWhiteSpace  = "space"
}

/// PBValidation Declaration. Check Example in Quick Help Section
///
///
/// **Normal parameter validation of screen**
///
///     let ValidateForm: ValidationParam =
///     [
///     .REQ_NAME: (0,txtFullName),
///     .REQ_EMAIL: (1,txtEmail),
///     .VALID_EMAIL: (2,txtEmail),
///     .REQ_PASSWORD: (3,txtPassword),
///     .VALID_PASSWORD: (4,txtPassword)
///     ]
///
///     if PBValidation(fieldName: ValidateForm)
///     {
///         print("success")
///     }
///
/// **Validation with Compare Validation of screen**
///
///     let ValidateForm: ValidationParam =
///     [
///     .REQ_EMAIL: (0,txtEmail),
///     .VALID_EMAIL: (1,txtEmail),
///     .REQ_PASSWORD: (2,txtPassword),
///     .VALID_PASSWORD: (3,txtPassword),
///     .REQ_CONFIRM_PASS: (4,txtConfirmPassword),
///     ]
///
///     print(ValidateForm)
///
///     if PBValidation(fieldName: ValidateForm, Compare: (CompareField: txtPassword, CompareFieldTo: txtConfirmPassword))
///     {
///         print("success")
///     }
///
func PBValidation(fieldName: ValidationParam, Compare: (CompareField: UITextField, CompareFieldTo: UITextField)? = nil) -> Bool
{
    
    var valuess = fieldName.sorted(by: { (arg0, arg1) -> Bool in
        
        return arg0.value.priority < arg1.value.priority
    })
    
    //print(valuess)
    
    if Compare != nil
    {
        valuess.append((key: .COMPARE_FIELD, value: (valuess.count + 1, Compare?.CompareField ?? UITextField())))
    }
    
    for (key: valType, value: (priority: _,fieldToValidate: txtField)) in valuess
    {
        switch valType
        {
        case .REQ_NAME:
            if(txtField.text?.trim() == "")
            {
                AppInstance.showMessages(message: MSG_NAME_EMPTY)
                txtField.shake()
                txtField.becomeFirstResponder()
                return false
            }
        case .REQ_FULLNAME:
            if(txtField.text?.trim() == "")
            {
                AppInstance.showMessages(message: MSG_FULLNAME_EMPTY)
                txtField.becomeFirstResponder()
                return false
            }
        case .REQ_FIRSTNAME:
            if(txtField.text?.trim() == "")
            {
                AppInstance.showMessages(message: MSG_FIRSTNAME_EMPTY)
                txtField.becomeFirstResponder()
                return false
            }
        case .REQ_LASTNAME:
            if(txtField.text?.trim() == "")
            {
                AppInstance.showMessages(message: MSG_FULLNAME_EMPTY)
                txtField.becomeFirstResponder()
                return false
            }
        case .REQ_EMAIL:
            if(txtField.text?.trim() == "")
            {
                AppInstance.showMessages(message: MSG_EMAIL_EMPTY)
                txtField.shake()
                txtField.becomeFirstResponder()
                return false
            }
        case .REQ_PASSWORD:
            if(txtField.text?.trim() == "")
            {
                AppInstance.showMessages(message: MSG_PASSWORD_EMPTY)
                txtField.becomeFirstResponder()
                return false
            }
        case .REQ_CONFIRM_PASS:
            if(txtField.text?.trim() == "")
            {
                AppInstance.showMessages(message: MSG_CONFIRM_PASSWORD_EMPTY)
                txtField.becomeFirstResponder()
                return false
            }
        case .REQ_MOBILE:
            if(txtField.text?.trim() == "")
            {
                AppInstance.showMessages(message: MSG_MOBILE_NUMBER_EMPTY)
                txtField.becomeFirstResponder()
                return false
            }
        case .REQ_COUNTRYCODE:
            if(txtField.text?.trim() == "")
            {
                AppInstance.showMessages(message: MSG_SELECT_COUNTRY_CODE)
                txtField.becomeFirstResponder()
                return false
            }
        case .REQ_PIN:
            if(txtField.text?.trim() == "")
            {
                AppInstance.showMessages(message: MSG_PIN_EMPTY)
                txtField.becomeFirstResponder()
                return false
            }
        case .REQ_PIN2:
            if(txtField.text?.trim() == "")
            {
                AppInstance.showMessages(message: MSG_PIN_EMPTY)
                txtField.becomeFirstResponder()
                return false
            }
        case .REQ_PIN3:
            if(txtField.text?.trim() == "")
            {
                AppInstance.showMessages(message: MSG_PIN_EMPTY)
                txtField.becomeFirstResponder()
                return false
            }
        case .REQ_PIN4:
            if(txtField.text?.trim() == "")
            {
                AppInstance.showMessages(message: MSG_PIN_EMPTY)
                txtField.becomeFirstResponder()
                return false
            }
        case .REQ_LOCATION:
            if(txtField.text?.trim() == "")
            {
                AppInstance.showMessages(message: MSG_LOCATION_EMPTY)
                txtField.becomeFirstResponder()
                return false
            }
        case .VALID_EMAIL:
            if !Utilities.isValidEmail(testStr: (txtField.text?.trim())!)
            {
                AppInstance.showMessages(message: MSG_EMAIL_INVALID)
                txtField.shake()
                txtField.becomeFirstResponder()
                return false
            }
        case .VALID_PASSWORD:
            if txtField.text?.trim().count != 0
            {
                if (txtField.text?.count)! < 6
                {
                    AppInstance.showMessages(message: MSG_PASSWORD_LENGTH)
                    txtField.shake()
                    BasicFunctions.showError(strError: MSG_PASSWORD_LENGTH)
                    return false
                }
                else if ((txtField.text?.count)! > 15)
                {
                    AppInstance.showMessages(message: MSG_PASSWORD_LENGTH)
                    txtField.shake()
                    BasicFunctions.showError(strError: MSG_PASSWORD_LENGTH)
                    return false
                }
            }
        case .VALID_MOBILE:
            if txtField.text?.trim().count != 0
            {
                if (txtField.text?.count)! < 8
                {
                    AppInstance.showMessages(message: MSG_INVALID_MOBILENO)
                    txtField.becomeFirstResponder()
                    return false
                }
                else if ((txtField.text?.count)! > 15)
                {
                    AppInstance.showMessages(message: MSG_INVALID_MOBILENO)
                    txtField.becomeFirstResponder()
                    return false
                }
            }
        case .COMPARE_FIELD:
            if Compare?.CompareField.text?.trim() != Compare?.CompareFieldTo.text?.trim()
            {
                AppInstance.showMessages(message: MSG_CONFIRM_PASSWORD_MISSMATCH)
                Compare?.CompareFieldTo.shake()
                Compare?.CompareFieldTo.becomeFirstResponder()
                return false
            }
            
        case .ContainWhiteSpace:
            break
        case .VALID_URL:
            if !Utilities.isValidateURL(stringURL: txtField.text?.trim() ?? "")
            {
                AppInstance.showMessages(message: MSG_URL_IS_NOT_VALID)
                txtField.becomeFirstResponder()
                return false
            }
            
            
        }
    }
    return true
}

