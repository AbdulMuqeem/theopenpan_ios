//
//  PickerViewManager.swift
//  TheOpenPan
//
//  Created by Pathan Mushrankhan on 19/01/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit

class MyPickerView: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {
    
    typealias Block = (Int)->()
    var compBlock:Block!
    
    var list = ["Photo Library","Camera"]

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return list.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return list[row]
    }
    
    override func selectRow(_ row: Int, inComponent component: Int, animated: Bool) {
        compBlock(row)
    }


}
