//
//  mk_extensions.swift
//  MealPlannerDemoApp
//
//  Created by ITMAC051 on 16/12/19.
//  Copyright © 2019 ITMAC051. All rights reserved.
//

import Foundation
import UIKit
import TagListView

extension UIButton {

    private class Action {
        var action: (UIButton) -> Void

        init(action: @escaping (UIButton) -> Void) {
            self.action = action
        }
    }

    private struct AssociatedKeys {
        static var ActionTapped = "actionTapped"
    }

    private var tapAction: Action? {
        set { objc_setAssociatedObject(self, &AssociatedKeys.ActionTapped, newValue, .OBJC_ASSOCIATION_RETAIN) }
        get { return objc_getAssociatedObject(self, &AssociatedKeys.ActionTapped) as? Action }
    }


    @objc dynamic private func handleAction(_ recognizer: UIButton) {
        tapAction?.action(recognizer)
    }


    func mk_addTapHandler(action: @escaping (UIButton) -> Void) {
        self.addTarget(self, action: #selector(handleAction(_:)), for: .touchUpInside)
        tapAction = Action(action: action)

    }
}


extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
}

extension String {

    public func isImage() -> Bool {
        // Add here your image formats.
        let imageFormats = ["jpg", "jpeg", "png"]

        if let ext = self.getExtension() {
            return imageFormats.contains(ext)
        }

        return false
    }

    public func getExtension() -> String? {
       let ext = (self as NSString).pathExtension

       if ext.isEmpty {
           return nil
       }

       return ext
    }

    public func isURL() -> Bool {
       return URL(string: self) != nil
    }

}


func matchPattern(string:String, regex:String) -> Bool {
//    return string.range
    return (string.range(of:regex, options:.regularExpression) != nil)
}


extension UIButton {

    private struct AssociatedKeys2 {
        static var _title = String()
        
    }
    
    var associated_title:String {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys2._title) as! String
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys2._title, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    
    func loadingIndicator(_ show: Bool) {
        let tag = 808404
        if show {
            self.isEnabled = false
            self.alpha = 0.5
            let indicator = UIActivityIndicatorView()
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
            indicator.color = #colorLiteral(red: 0.03942008317, green: 0.009498601779, blue: 0.01263471786, alpha: 1)
            indicator.tintColor = #colorLiteral(red: 0.03942008317, green: 0.009498601779, blue: 0.01263471786, alpha: 1)
            indicator.tag = tag
            self.addSubview(indicator)
            indicator.startAnimating()
            self.associated_title = self.titleLabel?.text ?? ""
            self.setTitle("", for: .normal)
        } else {
            self.isEnabled = true
            self.alpha = 1.0
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
                self.setTitle(self.associated_title, for: .normal)
            }
        }
    }
    
    func mk_makeEnable(_ boolValue:Bool) {
        self.isEnabled = boolValue
        self.alpha = boolValue ? 1.0 : 0.5
    }
}



func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}

func convertToArray(text: String) -> [[String: Any]]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}


extension String {
    /**
      Adds a given prefix to self, if the prefix itself, or another required prefix does not yet exist in self.
      Omit `requiredPrefix` to check for the prefix itself.
    */
    mutating func addPrefixIfNeeded(_ prefix: String, requiredPrefix: String? = nil) {
        guard !self.hasPrefix(requiredPrefix ?? prefix) else { return }
        self = prefix + self
    }
}


extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        return formatter
    }()
}


extension DateComponents {
    init?(ISO8601String string: String) {
        guard let unitValues = string.durationUnitValues else {
            return nil
        }
        print(unitValues)
        self.init()
        for (unit, value) in unitValues {
            self.setValue(value, for: unit)
        }
    }
}

func dateComponents(ISO8601String string: String) -> DateComponents? {
   guard let unitValues = string.durationUnitValues else {
      return nil
   }

   var components = DateComponents()
   for (unit, value) in unitValues {
      components.setValue(value, for: unit)
   }
   return components
}

private let dateUnitMapping: [Character: Calendar.Component] = ["Y": .year, "M": .month, "W": .weekOfYear, "D": .day]
private let timeUnitMapping: [Character: Calendar.Component] = ["H": .hour, "M": .minute, "S": .second]

private extension String {
   var durationUnitValues: [(Calendar.Component, Int)]? {
      guard self.hasPrefix("P") else {
         return nil
      }

#if swift(>=4)
      let duration = String(self.dropFirst())

      guard let separatorRange = duration.range(of: "T") else {
         return duration.unitValuesWithMapping(dateUnitMapping)
      }

      let date = String(duration[..<separatorRange.lowerBound])
      let time = String(duration[separatorRange.upperBound...])
#else
      let duration = String(self.characters.dropFirst())

      guard let separatorRange = duration.range(of: "T") else {
      return duration.unitValuesWithMapping(dateUnitMapping)
      }

      let date = duration.substring(to: separatorRange.lowerBound)
      let time = duration.substring(from: separatorRange.upperBound)
#endif
      guard let dateUnits = date.unitValuesWithMapping(dateUnitMapping),
         let timeUnits = time.unitValuesWithMapping(timeUnitMapping) else {
            return nil
      }

      return dateUnits + timeUnits
   }

   func unitValuesWithMapping(_ mapping: [Character: Calendar.Component]) -> [(Calendar.Component, Int)]? {
      if self.isEmpty {
         return []
      }

      var components: [(Calendar.Component, Int)] = []

      let identifiersSet = CharacterSet(charactersIn: String(mapping.keys))

      let scanner = Scanner(string: self)
      while !scanner.isAtEnd {
         var value: Int = 0
         if !scanner.scanInt(&value) {
            return nil
         }
         var identifier: NSString?
         if !scanner.scanCharacters(from: identifiersSet, into: &identifier) || identifier?.length != 1 {
            return nil
         }
         let unit = mapping[Character(identifier! as String)]!
         components.append((unit, value))
      }
      return components
   }
}


extension String {
    
    func getHoursMinutes() -> (String?,String?) {
        
        var strHours : String?
        var strMinutes : String?
        
        if let dtc = DateComponents.init(ISO8601String: self) {
            
            
            if let minutes = dtc.minute {
                strMinutes = "\(minutes)"
                if let hours = dtc.hour {
                    strHours = "\(hours)"
                    strMinutes = "\(minutes)"
                } else {
                    if minutes >= 60 {
                        let tuples = minutesToHoursMinutes(minutes: minutes)
                        if tuples.hours > 0 {
                            strHours = "\(tuples.hours)"
                        } else {
                            strHours = nil
                        }
                        if tuples.leftMinutes > 0 {
                            strMinutes = "\(tuples.leftMinutes)"
                        } else {
                            strMinutes = nil
                        }
                    }
                }
                return (strHours,strMinutes)
            }
            
            if let hours = dtc.hour {
                strHours = "\(hours)"
            }
            
        }
        
        return (strHours,strMinutes)
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
    
}

func minutesToHoursMinutes (minutes : Int) -> (hours : Int , leftMinutes : Int) {
    return (minutes / 60, (minutes % 60))
}



//extension NSAttributedString {
//    
//    public convenience init?(HTMLString html: String, font: UIFont? = nil) throws {
//        let options : [NSAttributedString.DocumentReadingOptionKey : Any] =
//            [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
//             NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue]
//
//        guard let data = html.data(using: .utf8, allowLossyConversion: true) else {
//            throw NSError(domain: "Parse Error", code: 0, userInfo: nil)
//        }
//
//        if let font = font {
//            guard let attr = try? NSMutableAttributedString(data: data, options: options, documentAttributes: nil) else {
//                throw NSError(domain: "Parse Error", code: 0, userInfo: nil)
//            }
//            var attrs = attr.attributes(at: 0, effectiveRange: nil)
//            attrs[NSAttributedString.Key.font] = font
//            attr.setAttributes(attrs, range: NSRange(location: 0, length: attr.length))
//            self.init(attributedString: attr)
//        } else {
//            try? self.init(data: data, options: options, documentAttributes: nil)
//        }
//    }
//    
//}



extension UIView
{
    func applyGradient(colors: [CGColor],shouldHideFirst:Bool=false)
    {
        if self.gradientLayer == nil {
            self.gradientLayer = CAGradientLayer()
            gradientLayer.colors = colors
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 1, y: 0)
            gradientLayer.frame = self.bounds
            self.layer.insertSublayer(gradientLayer, at: 0)
            if shouldHideFirst {
                self.gradientLayer.isHidden = true
            }
        } else {
            self.gradientLayer.frame = self.bounds
        }
    }
 
    private struct AssociatedKeys {
        static var gradientLayerObject : CAGradientLayer!
    }
    
    var gradientLayer:CAGradientLayer! {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.gradientLayerObject) as? CAGradientLayer
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.gradientLayerObject, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
}


extension UIViewController {
    func mk_hideNavBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    func mk_showNavBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
}


//private var viewBack: UIView!




extension UIViewController {
    class var className: String {
        return NSStringFromClass(self.classForCoder()).components(separatedBy: ".").last!;
    }
}


extension TagView {
    
    private struct AssociatedKeys {
        static var _id = Int64()
        static var _Category_tag : Category_tag!
        static var _filterTag : FilterTagsModel!
    }
    
    var id:Int64 {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys._id) as! Int64
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys._id, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var cat_tag_object:Category_tag {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys._Category_tag) as! Category_tag
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys._Category_tag, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var filterTag:FilterTagsModel {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys._filterTag) as! FilterTagsModel
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys._filterTag, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}



extension UITextField {

    private struct AssociatedKeys {
        static var viewBack = UIView()
        static var AllEditingEvent = "AllEditingEvent"
    }
    
    // viewBack
    var viewBack:UIView {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.viewBack) as! UIView
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.viewBack, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    // allEditingEventsHandler
    private class Action {
        var action: (UITextField) -> Void

        init(action: @escaping (UITextField) -> Void) {
            self.action = action
        }
    }

    private var allEditingEventAction: Action? {
        set { objc_setAssociatedObject(self, &AssociatedKeys.AllEditingEvent, newValue, .OBJC_ASSOCIATION_RETAIN) }
        get { return objc_getAssociatedObject(self, &AssociatedKeys.AllEditingEvent) as? Action }
    }


    @objc dynamic private func handleAction(_ recognizer: UITextField) {
        allEditingEventAction?.action(recognizer)
    }


    func mk_addAllEditingEventHandler(action: @escaping (UITextField) -> Void) {
        self.addTarget(self, action: #selector(handleAction(_:)), for: .allEditingEvents)
        allEditingEventAction = Action(action: action)
    }
}


extension Bool {
    var mk_intValue: Int {
        return self ? 1 : 0
    }
}
extension Int {
    var mk_boolValue: Bool {
        return self != 0
    }
}

extension UIImage {

    func mk_saveToDocuments(filename:String,folderName:String) -> String? {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let filePath =  documentsDirectory.appendingPathComponent("\(folderName)")
        if !FileManager.default.fileExists(atPath: filePath.path) {
            do {
                try FileManager.default.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                NSLog("Couldn't create document directory")
                return nil
            }
        }
        
        let fileURL = filePath.appendingPathComponent("\(filename).jpg")
        if let data = self.jpegData(compressionQuality: 1.0) {
            do {
                try data.write(to: fileURL)
                return fileURL.path
            } catch {
                print("error saving file to documents:", error)
                return nil
            }
        }
        return nil
    }
}

extension Data {
    
    func mk_saveVideoFileToDocuments(filename:String, ext:String = "mp4", folderName:String) -> String? {
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath =  documentsDirectory.appendingPathComponent("\(folderName)")
        if !FileManager.default.fileExists(atPath: filePath.path) {
            do {
                try FileManager.default.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                NSLog("Couldn't create document directory")
                return nil
            }
        }
        let fileURL = filePath.appendingPathComponent("\(filename).\(ext)")
        do {
            try self.write(to: fileURL)
            return fileURL.absoluteString
        } catch {
            print("error saving file to documents:", error)
            return nil
        }
    }
    
}





extension FileManager {
    func mk_removeFolderFromDocuments(folderName:String) -> Bool {
        
        let documentsDirectory = self.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let filePath =  documentsDirectory.appendingPathComponent("\(folderName)")
        if !self.fileExists(atPath: filePath.path) {
            do {
                try self.removeItem(at: filePath)
                return true
            } catch {
                NSLog("Couldn't create document directory")
                return false
            }
        }
        return false
    }
}


extension Date {
    var mk_ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}


extension String {
    var mk_digit: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
    var mk_digits: [String] {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
    }
}


extension UINavigationController {
   open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
//      return topViewController?.preferredStatusBarStyle ?? .default
   }
}

//
//extension Double {
//    /// Rounds the double to decimal places value
//    func roundToPlaces(places:Int) -> Double {
//        let divisor = pow(10.0, Double(places))
//        return round(self * divisor) / divisor
//    }
//}


extension String {
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}

extension UILabel {
    func strikeThrough(_ isStrikeThrough:Bool) {
        if isStrikeThrough {
            if let lblText = self.text {
                let attributeString =  NSMutableAttributedString(string: lblText)
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
                self.attributedText = attributeString
            }
        } else {
            if let attributedStringText = self.attributedText {
                let txt = attributedStringText.string
                self.attributedText = nil
                self.text = txt
                return
            }
        }
    }
    
    func strikeThroughRemove(newText:String) {
//        if let lblText = self.text {
//        let attributeString = NSMutableAttributedString(string: lblText)
//            attributeString.removeAttribute(NSAttributedString.Key.strikethroughStyle, range: NSMakeRange(0, attributeString.length))
//            attributeString.removeAttribute(NSAttributedString.Key.strikethroughColor, range: NSMakeRange(0, attributeString.length))
//            self.attributedText = NSAttributedString.init(string: lblText)
//        }
        
        if let lblText = self.text {
        let attributeString = NSMutableAttributedString(string: lblText)
            attributeString.setAttributes([:], range: NSMakeRange(0,attributeString.length))
            self.attributedText = nil
            self.text = ""
            self.text = newText
            self.attributedText = NSAttributedString.init(string: newText)
        }
    }
}


extension Dictionary {
    
    func getJsonData() -> Data? {
        return try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
    }
    
}

extension Array {
    
    func getJsonData() -> Data? {
        return try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
    }
    
}


extension CAShapeLayer {
    func drawCircleAtLocation(location: CGPoint, withRadius radius: CGFloat, andColor color: UIColor, filled: Bool) {
        fillColor = filled ? color.cgColor : UIColor.white.cgColor
        strokeColor = color.cgColor
        let origin = CGPoint(x: location.x - radius, y: location.y - radius)
        let offsetByMe : CGFloat = 1.5
        path = UIBezierPath(ovalIn: CGRect(origin: origin, size: CGSize(width: (radius * 2) + offsetByMe, height: (radius * 2) + offsetByMe))).cgPath
    }
}

private var handle: UInt8 = 0

extension UIBarButtonItem {
    private var badgeLayer: CAShapeLayer? {
        if let b: AnyObject = objc_getAssociatedObject(self, &handle) as AnyObject? {
            return b as? CAShapeLayer
        } else {
            return nil
        }
    }

    func addBadge(number: Int, withOffset offset: CGPoint = CGPoint.zero, andColor color: UIColor = .white, andFilled filled: Bool = true) {
        guard let view = self.value(forKey: "view") as? UIView else { return }

        badgeLayer?.removeFromSuperlayer()

        // Initialize Badge
        let badge = CAShapeLayer()
        let radius = CGFloat(8)
        let location = CGPoint(x: view.frame.width - (radius + offset.x), y: (radius + offset.y))
        badge.drawCircleAtLocation(location: location, withRadius: radius, andColor: color, filled: filled)
        view.layer.addSublayer(badge)

        // Initialiaze Badge's label
        let label = CATextLayer()
        
        if number > 99 {
            label.string = "99+"
            label.fontSize = 10
        } else {
            label.string = "\(number)"
            label.fontSize = 12
        }
        
        label.alignmentMode = CATextLayerAlignmentMode.center
        
        label.frame = CGRect(origin: CGPoint(x: location.x - 7.5, y: offset.y + 1.2), size: CGSize(width: 16, height: 16))
        label.foregroundColor = filled ? #colorLiteral(red: 0.631372549, green: 0.7764705882, blue: 0.7254901961, alpha: 1).cgColor : color.cgColor
        label.backgroundColor = UIColor.clear.cgColor
        label.contentsScale = UIScreen.main.scale
        badge.addSublayer(label)

        // Save Badge as UIBarButtonItem property
        objc_setAssociatedObject(self, &handle, badge, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }

    func updateBadge(number: Int) {
        if let text = badgeLayer?.sublayers?.filter({ $0 is CATextLayer }).first as? CATextLayer {
            text.string = "\(number)"
        }
    }

    func removeBadge() {
        badgeLayer?.removeFromSuperlayer()
    }
}

extension Date {
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }

    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }

    var startOfMonth: Date {
        let components = Calendar.current.dateComponents([.year, .month], from: startOfDay)
        return Calendar.current.date(from: components)!
    }

    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfMonth)!
    }
}
