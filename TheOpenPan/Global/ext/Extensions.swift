//
//  Extensions.swift
//  Tiffinwala
//
//  Created by Maitrey on 10/04/15.
//  Copyright (c) 2015 Vivacious. All rights reserved.
//

import UIKit
import CoreData
import SDWebImage
//import Alamofire
import AlamofireImage
//import SkyFloatingLabelTextField
//import NVActivityIndicatorView
//import STPopup


import Foundation
import UIKit

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}






enum enumModelName : String {
    case iPhone5 = "iPhone 5"
    case iPhone5c = "iPhone 5c"
    case iPhone5s = "iPhone 5s"
    case iPhone6 = "iPhone 6"
    case iPhone6s = "iPhone 6s"
    case iPad2 = "iPad 2"
    case iPad3 = "iPad 3"
    case iPadMini = "iPad Mini"
    
    //    case iPhone8Plus = "iPhone 8 Plus"
    
    static func all() -> [enumModelName] {
        return [.iPhone5,.iPhone5c,.iPhone5s,.iPhone6,.iPhone6s,.iPad2,.iPad3,.iPadMini]
    }
}

extension Date
{
    func isGreaterThanDate(_ dateToCompare: Date) -> Bool
    {
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending {
            return true
        } else {
            return false
        }
    }
}

public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            let str = String((UnicodeScalar(UInt8(value))))
            return identifier + str
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro" //return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro"//return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro"//return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro"//return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}

extension RangeReplaceableCollection where Iterator.Element : Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func removeObject(_ object : Iterator.Element) {
        if let index = self.firstIndex(of: object) {
            self.remove(at: index)
        }
    }
    
}

extension Optional where Wrapped: Collection {
    var mk_isNilOrZero: Bool {
        switch self {
        case .some(let collection):
            return collection.isEmpty
        case .none:
            return true
        }
    }
}

extension UIImage {
    
    func crop(to:CGSize) -> UIImage {
        guard let cgimage = self.cgImage else { return self }
        
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        
        let contextSize: CGSize = contextImage.size
        
        //Set to square
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        let cropAspect: CGFloat = to.width / to.height
        
        var cropWidth: CGFloat = to.width
        var cropHeight: CGFloat = to.height
        
        if to.width > to.height { //Landscape
            cropWidth = contextSize.width
            cropHeight = contextSize.width / cropAspect
            posY = (contextSize.height - cropHeight) / 2
        } else if to.width < to.height { //Portrait
            cropHeight = contextSize.height
            cropWidth = contextSize.height * cropAspect
            posX = (contextSize.width - cropWidth) / 2
        } else { //Square
            if contextSize.width >= contextSize.height { //Square on landscape (or square)
                cropHeight = contextSize.height
                cropWidth = contextSize.height * cropAspect
                posX = (contextSize.width - cropWidth) / 2
            }else{ //Square on portrait
                cropWidth = contextSize.width
                cropHeight = contextSize.width / cropAspect
                posY = (contextSize.height - cropHeight) / 2
            }
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cropWidth, height: cropHeight)
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let cropped: UIImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
        
        UIGraphicsBeginImageContextWithOptions(to, true, self.scale)
        cropped.draw(in: CGRect(x: 0, y: 0, width: to.width, height: to.height))
        let resized = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resized!
    }
}





private var xoAssociationKeyLastTitle: String!

extension UIButton {
    
    var lastTitle: String! {
        get {
            return objc_getAssociatedObject(self, &xoAssociationKeyLastTitle) as? String
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAssociationKeyLastTitle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    

    func loadingIndicator(show: Bool) {
        if show {
            
            self.lastTitle = self.currentTitle
            self.setTitle("                     ", for: .normal)
            let indicator = UIActivityIndicatorView()
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
            indicator.color = self.titleLabel?.textColor
//            self.setTitleColor(self.titleLabel?.textColor, for: .normal)
            self.isEnabled = false
            self.addSubview(indicator)
            self.bringSubviewToFront(indicator)
            indicator.startAnimating()
            
        } else {
            for view in self.subviews {
                if let indicator = view as? UIActivityIndicatorView {
                    indicator.stopAnimating()
                    indicator.removeFromSuperview()
                    self.setTitle(self.lastTitle ?? "", for: .normal)
                    self.isEnabled = true
//                    self.setTitleColor(self.titleLabel?.textColor, for: .normal)
                }
            }
        }
    }
}



extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String, fontName: String = enum_font.semi_bold.rawValue, fontSize: CGFloat = 17) -> NSMutableAttributedString {
        
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: fontName, size: fontSize)!]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String, fontName: String = enum_font.regular.rawValue, fontSize: CGFloat = 17) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: fontName, size: fontSize)!]
        let normalString = NSMutableAttributedString(string:text, attributes: attrs)
        append(normalString)
        
        return self
    }
    
    @discardableResult func changeFont(_ text: String,font:UIFont,textColor:UIColor = UIColor.white,textStrokeColor:UIColor = UIColor.white,strokeWidth:CGFloat = -0.0) -> NSMutableAttributedString {
        
        let attrs: [NSAttributedString.Key: Any] = [.font: font,
                                                    NSAttributedString.Key.strokeColor : textStrokeColor,
                                                    NSAttributedString.Key.foregroundColor : textColor,
                                                    NSAttributedString.Key.strokeWidth : strokeWidth]
        let normalString = NSMutableAttributedString(string:text, attributes: attrs)
        append(normalString)
        return self
    }
    
}









extension Array {
    
    mutating func removeObject<U: Equatable>(_ object: U) {
        
        var index: Int?
        for (idx, objectToCompare) in self.enumerated() {
            if let to = objectToCompare as? U {
                if object == to {
                    index = idx
                }
            }
        }
        
        if((index) != nil) {
            self.remove(at: index!)
        }
    }
}

extension Array where Element:Hashable {
    var unique: [Element] {
        var set = Set<Element>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(value) {
                set.insert(value)
                arrayOrdered.append(value)
            }
        }
        
        return arrayOrdered
    }
}

//extension NVActivityIndicatorPresenter {
//
//    func startAnimating() {
//        self.startAnimating(ActivityData())
//    }
//
//}

extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}


extension String {
    
    func contains(_ find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    public func trimWhiteSpace()->String?{
        
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    public func placeHolder (_ color : UIColor) -> NSAttributedString? {
        let strPassword = NSAttributedString(string: self, attributes: [NSAttributedString.Key.foregroundColor:color])
        return strPassword
    }
    
    public func AttributedStringFont(_ range : NSRange, font : UIFont) -> NSAttributedString? {
        let nameMutableString = NSMutableAttributedString(string: self)
        nameMutableString.addAttribute(NSAttributedString.Key.font, value: font, range: range)
        return nameMutableString
    }
    
    public func AttributedStringFont(_ font : UIFont, fontColor: UIColor) -> NSAttributedString? {
        let nameMutableString = NSMutableAttributedString(string: self)
        nameMutableString.addAttributes([NSAttributedString.Key.font:font,
                                         NSAttributedString.Key.foregroundColor:fontColor], range: NSMakeRange(0, self.count))
        return nameMutableString
    }
    
    func deleteHTMLTag(tag:String) -> String {
        return self.replacingOccurrences(of: "(?i)</?\(tag)\\b[^<]*>", with: "", options: .regularExpression, range: nil)
    }
    
    func deleteHTMLTags(tags:[String]) -> String {
        var mutableString = self
        for tag in tags {
            mutableString = mutableString.deleteHTMLTag(tag: tag)
        }
        return mutableString
    }
    
//    var html2AttributedString: NSAttributedString? {
//        do {
//
////            return try NSAttributedString
//            return try NSAttributedString(data: Data(utf8), options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
//        } catch {
//            print("error:", error)
//            return nil
//        }
//    }

//    var html2String: String {
//        return html2AttributedString?.string ?? ""
//    }
    
   
    
    var numbers: String { return components(separatedBy: Numbers.characterSet.inverted).joined() }
    var integer: Int { return Int(numbers) ?? 0 }
    
}
struct Numbers { static let characterSet = CharacterSet(charactersIn: "0123456789") }

private let characterEntities : [ String : Character ] = [
    // XML predefined entities:
    "&quot;"    : "\"",
    "&amp;"     : "&",
    "&apos;"    : "'",
    "&lt;"      : "<",
    "&gt;"      : ">",
    
    // HTML character entity references:
    "&nbsp;"    : "\u{00a0}",
    // ...
    "&diams;"   : "♦",
]

extension Int {
    
    func timestampToDate()->String{
        
        let date = Date(timeIntervalSince1970: TimeInterval(self))
        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd MMM YYYY"  As per design
        dateFormatter.dateFormat = "MM/dd/YYYY"
        //        dateFormatter.timeZone = NSTimeZone() as TimeZone!
        let localDate = dateFormatter.string(from: date)
        
        return localDate
    }
    
    func timestampToDateTime()->String{
        
        let date = Date(timeIntervalSince1970: TimeInterval(self))
        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd MMM YYYY HH:mm a"   As per Design
        dateFormatter.dateFormat = "MM/dd/YYYY"
        //        dateFormatter.timeZone = NSTimeZone() as TimeZone!
        let localDate = dateFormatter.string(from: date)
        
        return localDate
    }
    
    var stringValue:String {
        return "\(self)"
    }
}

extension Dictionary {
    
    func removeNilFromDict( _ dict:Dictionary<String,AnyObject?>) -> Dictionary<String,AnyObject?>{
        
        var dictemp = dict
        let x: AnyObject = NSNull()
        for (key,name) in dict{
            if name == nil || (name as? String == x as? String){
                dictemp.updateValue("" as AnyObject?, forKey: key)
            }
        }
        return dictemp
    }
    
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}

extension UILabel{
    
    func setAppFontSize(){
        self.font = UIFont(name: self.font.fontName, size: 13)
    }
    
    func animate(newText: String, characterDelay: TimeInterval) {
        
        DispatchQueue.main.async {
            
            self.text = ""
            
            for (index, character) in newText.enumerated() {
                DispatchQueue.main.asyncAfter(deadline: .now() + characterDelay * Double(index)) {
                    self.text?.append(character)
                }
            }
        }
    }

}


extension Date {
//    func isGreaterThanDate(_ dateToCompare: Date) -> Bool {
//        //Declare Variables
//        var isGreater = false
//        
//        //Compare Values
//        if self.compare(dateToCompare) == ComparisonResult.orderedDescending {
//            isGreater = true
//        }
//        
//        //Return Result
//        return isGreater
//    }
    
    func isLessThanDate(_ dateToCompare: Date) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(_ dateToCompare: Date) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(_ daysToAdd: Int) -> Date {
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: Date = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(_ hoursToAdd: Int) -> Date {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: Date = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
    
    
    var age: Int {
        return Calendar.current.dateComponents([.year], from: self, to: Date()).year!
    }

}
//extension UIViewController {
//
//
//    func presentInStPopUp(controller: UIViewController) {
//
//        let popup = STPopupController(rootViewController: controller)
//        popup.style = .bottomSheet
//        popup.transitionStyle = .slideVertical
//        popup.backgroundView?.addGestureRecognizer(UITapGestureRecognizer(target: popup, action: #selector(popup.close)))
//        popup.present(in: self)
//
//    }
//
//}
//extension STPopupController {
//
//    @objc public func close() {
//
//        self.dismiss {
//
//        }
//    }
//}

extension UITextView{
    
    func isEmpty()->Bool{
        if self.text?.count > 0 {
            return false
        }
        if self.text?.trimWhiteSpace()?.count > 0 {
            return false
        }
        return true
    }
    
    
    func mk_setHTMLFromString2(htmlText: String) {
        
        let modifiedFont = String(format:"<span style=\" color: \(self.textColor?.mk_toHexCode() ?? "000000"); font-family: '\(self.font!.fontName)', 'HelveticaNeue'; font-size: \(self.font!.pointSize)\">%@</span>", htmlText)
        
        //process collection values
        
        let data = modifiedFont.data(using: .utf8)
        do {
        let attrStr = try NSAttributedString(
            data: data!,
        options: [.documentType: NSAttributedString.DocumentType.html,
                  NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
        documentAttributes: nil)
            self.attributedText = attrStr
        } catch {
           print(error)
        }
        
        
        
//        self.attributedText = attrStr
    }
}

extension Date {
    
//    //An integer representation of age from the date object (read-only).
//    var age: Int {
//        get {
//            let now = Date()
//            let calendar = Calendar.current
//
//            let ageComponents = calendar.dateComponents([.year], from: self, to: now)
//            let age = ageComponents.year!
//            return age
//        }
//    }
    
    init(year: Int, month: Int, day: Int) {
        var dc = DateComponents()
        dc.year = year
        dc.month = month
        dc.day = day
        
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        if let date = calendar.date(from: dc) {
            self.init(timeInterval: 0, since: date)
        } else {
            fatalError("Date component values were invalid.")
        }
    }
    
}


extension UIImageView {
    
    public func mk_afSetImage(urlString: String?, defaultImage : String?,block mainBlock: ((_ img: UIImage?,_ isComplete: Bool)->Void)?) {
        
        
        
        if let strUrl = urlString {
            if let url = URL.init(string: strUrl) {
                
                self.sd_setImage(with: url) { (img, err, type, lastUrl) in
                    if let img = img {
                        self.image = img
                        mainBlock?(img,true)
                        return
                    }
                    if let _ = err {
                        if defaultImage != nil {
                            self.image = UIImage(named: defaultImage!)
                        }
                        mainBlock?(nil,true)
                        return
                    }
                }
                
//                self.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: { (p) in
////                    print(p)
//                }, progressQueue: .main, imageTransition: UIImageView.ImageTransition.noTransition, runImageTransitionIfCached: true) { (dataRes) in
//
//                    if let imgData = dataRes.data {
//                        if let imgFromData = UIImage.init(data: imgData) {
//                            self.image = imgFromData
//                            mainBlock?(imgFromData,true)
//                        } else {
//                            self.image = di
//                            mainBlock?(nil,false)
//                        }
//                        return
//                    }
//
//                    if let _ = dataRes.error {
//                        self.image = di
//                        mainBlock?(nil,false)
//                        return
//                    }
//                    if let img = dataRes.value {
//                        self.image = img
//                        mainBlock?(img,true)
//                        return
//                    }
//                }
            } else {
                if defaultImage != nil {
                    self.image = UIImage(named: defaultImage!)
                }
                mainBlock?(nil,false)
            }
        } else {
            if defaultImage != nil {
                self.image = UIImage(named: defaultImage!)
            }
            mainBlock?(nil,false)
        }
        
        
    }
    
//    func setServerUImg(URL: URL,block mainBlock:@escaping ((_ img: UIImage,_ isComplete: Bool)->Void)){
//
//        let placeholderImage =  UIImage()
//
//        self.sd_setImage(with: URL) { (image, err, chType, url) in
//            if let err = err {
//                mainBlock(placeholderImage,false)
//            } else {
//                if let image = image {
//                    self.image = image
//                    mainBlock(image,true)
//                } else {
//                    mainBlock(placeholderImage,false)
//                }
//            }
//        }
//
//    }
    
    func setRoundProfileStyle(){
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.borderColor = UIColor.darkText.cgColor
//        self.layer.borderColor = enumCCAppTheme.colorMain.setColor().cgColor
        self.layer.borderWidth = 1.0
        self.clipsToBounds = true
    }
    
    func addBlurEffect() {
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
    
    func setTintColor(_ color: UIColor) {
        self.tintColor = color
    }
}

extension UIViewController {
    
    func setCustomeTitleView(_ navigationTitle : String!, titleColor : UIColor, titleFont : UIFont)->Void{
        
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = NSTextAlignment.center
        label.text = navigationTitle
        label.sizeToFit()
        label.textColor = titleColor
        label.font = titleFont
        self.navigationItem.titleView = label
    }

    func setNoDataView(_ text : String, add : Bool){
        
        let visualEffects : UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffect.Style.light)) as UIVisualEffectView
        visualEffects.tag = 99
        visualEffects.frame = self.view.bounds
        
        let imgNoTiffin : UIImageView = UIImageView(frame: CGRect(x: (self.view.frame.size.width-60) / 2, y: 150, width: 80, height: 80))
        imgNoTiffin.image = UIImage(named: "notiffin")
        visualEffects.addSubview(imgNoTiffin)
        
        let vibrantLabel = UILabel()
        let ccenter:CGPoint? = self.view.center
        vibrantLabel.textColor = UIColor.lightGray
        vibrantLabel.backgroundColor = UIColor.clear
        vibrantLabel.font = UIFont.boldSystemFont(ofSize: 8 * UIScreen.main.scale)
        vibrantLabel.textAlignment = .center
        vibrantLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width-50, height: self.view.frame.size.height)
        vibrantLabel.numberOfLines = 0
        vibrantLabel.center = ccenter!
        vibrantLabel.text = text
        visualEffects.addSubview(vibrantLabel)
        
        if add == false {
            if self.view.viewWithTag(99) != nil {
                let obj : UIVisualEffectView = self.view.viewWithTag(99) as! UIVisualEffectView
                obj.removeFromSuperview()
            }
        }else{
            if self.view.viewWithTag(99) != nil {
                let obj : UIVisualEffectView = self.view.viewWithTag(99) as! UIVisualEffectView
                obj.removeFromSuperview()
            }
            self.view.addSubview(visualEffects)
        }
    }
    
//    func setBlurryBackground(){
//        
//        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .light)) as UIVisualEffectView
//        visualEffectView.frame = self.view.bounds
//        visualEffectView.tag = 999
//        self.tabBarController?.view.addSubview(visualEffectView)
//        visualEffectView.isHidden=true;
//        
//        let objGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("closeMenu"))
//        visualEffectView.addGestureRecognizer(objGesture)
//        visualEffectView.isUserInteractionEnabled = true;
//        
//    }
    
    class func currentViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return currentViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return currentViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return currentViewController(presented)
        }
        return base
    }
}

extension UITableView {
    func indexPathForView (_ view : UIView) -> IndexPath? {
        let location = view.convert(CGPoint.zero, to:self)
        return indexPathForRow(at: location)
    }
}

extension String {
    
    func is_PhoneNumber() -> Bool {
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = self.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return self == numberFiltered
    }
}




extension UITextField {
    
    func setAppFontSize(){
        self.font = UIFont(name: self.font!.fontName, size: 12)
    }
    
    func isEmpty()->Bool{
        
        if self.hasText {
            return false
        }
        
        if (self.text?.isEmpty)! {
            return true
        }
        if self.text?.count > 0 {
            return false
        }
        if self.text?.trimWhiteSpace()?.count > 0 {
            return false
        }
        return true
    }
    
    
    
    func addLeftView(){
        
        let leftView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 40))
        self.leftViewMode = UITextField.ViewMode.always
        self.leftView = leftView
                
    }
    
    func addLeftViewImage(_ imgName: String){
        
        let imgView: UIImageView = UIImageView(frame: CGRect(x: 0,y: 7.5, width: 25, height: 25))
        imgView.image = UIImage(named: imgName)
        imgView.contentMode = UIView.ContentMode.scaleAspectFit
        
        let leftView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 45, height: 40))
        leftView.addSubview(imgView)
        
        self.leftViewMode = UITextField.ViewMode.always
        self.leftView = leftView
    }
    
    func addRightView(width:CGFloat=20){
        let rightView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 40))
        self.rightViewMode = UITextField.ViewMode.always
        self.rightView = rightView
    }
    
    func addRightViewImage(_ imgName: String){
        
        let imgView: UIImageView = UIImageView(frame: CGRect(x: 10,y: 7.5, width: 25, height: 25))
        imgView.image = UIImage(named: imgName)
        imgView.contentMode = UIView.ContentMode.scaleAspectFit
        
        let rightView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 45, height: 40))
        rightView.addSubview(imgView)
        
        self.rightViewMode = UITextField.ViewMode.always
        self.rightView = rightView
    }
    
    func addRightViewButton(_ imgName: String) -> UIButton {
        
        let btn = UIButton.init()
        btn.frame.size = .init(width: 40, height: 60)
        btn.backgroundColor = .red
        btn.setImage(UIImage.init(named: imgName)!, for: .normal)
        
        self.rightViewMode = UITextField.ViewMode.always
        self.rightView = btn
        
        return btn
    }
    
    func setFocused(_ layer: CALayer, borderColor : UIColor){
        
        layer.borderColor = borderColor.cgColor
    }
    
    func setUnFocused(_ layer: CALayer, borderColor : UIColor){
       
        layer.borderColor = borderColor.cgColor
    }
    
    func setBottomBorder(borderColor : UIColor) -> CALayer{
        
        let border = CALayer()
        let width = CGFloat(1)
        border.borderColor = borderColor.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true

        return border
    }

    public func isValidEmail() -> Bool {
        let regex = try? NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", options: .caseInsensitive)
        return regex?.firstMatch(in: self.text!.trimWhiteSpace()!, options: [], range: NSMakeRange(0, self.text!.trimWhiteSpace()!.count)) != nil
    }
    
    func isValidContactNumber() -> Bool {
        let characterSet = CharacterSet(charactersIn: "1234567890")
        if let _ = self.text?.rangeOfCharacter(from: characterSet, options: .caseInsensitive) {
            return true
        }else{
            return false
        }
    }
}


extension UITextField {
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            if let color = self.attributedPlaceholder?.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor {
                return color
            }
            return nil
        }
        set (setOptionalColor) {
            if let setColor = setOptionalColor {
                let string = self.placeholder ?? ""
                self.attributedPlaceholder = NSAttributedString(string: string , attributes:[NSAttributedString.Key.foregroundColor: setColor,
                                                                                             NSAttributedString.Key.font:self.font as Any])
            }
        }
    }
}

extension UIView {
    
    /**
     Rounds the given set of corners to the specified radius
     
     - parameter corners: Corners to round
     - parameter radius:  Radius to round to
     */
    func round(corners: UIRectCorner, radius: CGFloat) {
        _round(corners: corners, radius: radius)
    }
    
    /**
     Rounds the given set of corners to the specified radius with a border
     
     - parameter corners:     Corners to round
     - parameter radius:      Radius to round to
     - parameter borderColor: The border color
     - parameter borderWidth: The border width
     */
    func round(corners: UIRectCorner, radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        let mask = _round(corners: corners, radius: radius)
        addBorder(mask: mask, borderColor: borderColor, borderWidth: borderWidth)
    }
    
    /**
     Fully rounds an autolayout view (e.g. one with no known frame) with the given diameter and border
     
     - parameter diameter:    The view's diameter
     - parameter borderColor: The border color
     - parameter borderWidth: The border width
     */
    func fullyRound(diameter: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        layer.masksToBounds = true
        layer.cornerRadius = diameter / 2
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor;
    }
    
}

private extension UIView {
    
    @discardableResult func _round(corners: UIRectCorner, radius: CGFloat) -> CAShapeLayer {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        return mask
    }
    
    func addBorder(mask: CAShapeLayer, borderColor: UIColor, borderWidth: CGFloat) {
        let borderLayer = CAShapeLayer()
        borderLayer.path = mask.path
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = borderColor.cgColor
        borderLayer.lineWidth = borderWidth
        borderLayer.frame = bounds
        layer.addSublayer(borderLayer)
    }
}




































//????????????????????????????????????????????????????????


















//
//  Extensions.swift
//  Tiffinwala
//
//  Created by Maitrey on 10/04/15.
//  Copyright (c) 2015 Vivacious. All rights reserved.
//



extension CGFloat {
    func byWidthRatio()->CGFloat {
        return (ssw * self)
    }
    func byHeightRatio()->CGFloat {
        return (ssh * self)
    }
}

extension Double {
    func byWidthRatio()->CGFloat {
        return (ssw * CGFloat(self))
    }
    func byHeightRatio()->CGFloat {
        return (ssh * CGFloat(self))
    }
}








extension UIViewController {
    
    private class Action {
        var action: (UIBarButtonItem) -> Void
        
        init(action: @escaping (UIBarButtonItem) -> Void) {
            self.action = action
        }
    }
    
    private struct AssociatedKeys {
        static var ActionName = "action"
        static var ActionRightBarName = "actionRightBar"
        static var ActionLeftBarName = "actionLeftBar"
    }
    
    private var backAction: Action? {
        set { objc_setAssociatedObject(self, &AssociatedKeys.ActionName, newValue, .OBJC_ASSOCIATION_RETAIN) }
        get { return objc_getAssociatedObject(self, &AssociatedKeys.ActionName) as? Action }
    }
    
    
    @objc dynamic private func handleAction(_ recognizer: UIBarButtonItem) {
        backAction?.action(recognizer)
    }
    
    
    func addLeftBackItem(action: @escaping (UIBarButtonItem) -> Void){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "BackButton"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(handleAction(_:)))
        
        backAction = Action(action: action)
        
    }
    
    func addLeftBackItem() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "AssetBackButton"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(backtoService(_:)))
    }
    
    ///////////
    
    private var rightBarAction: Action? {
        set { objc_setAssociatedObject(self, &AssociatedKeys.ActionRightBarName, newValue, .OBJC_ASSOCIATION_RETAIN) }
        get { return objc_getAssociatedObject(self, &AssociatedKeys.ActionRightBarName) as? Action }
    }
    
    
    func addRightBarButtonItem(img:UIImage? = nil, btnTitle:String? = nil, action: @escaping (UIBarButtonItem) -> Void) {
        
        if btnTitle != nil {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: btnTitle, style: .plain, target: self, action: #selector(handleActionRightBar(_:)))
        } else if img != nil {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItem.Style.plain, target: self, action: #selector(handleActionRightBar(_:)))
        }else if img == nil {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(handleActionRightBar(_:)))
        }
        
        rightBarAction = Action(action: action)
        
    }
    
    @objc dynamic private func handleActionRightBar(_ recognizer: UIBarButtonItem) {
        rightBarAction?.action(recognizer)
    }
    
    ////////////
    
    
    
    
    
    ///////////
    
    private var leftBarAction: Action? {
        set { objc_setAssociatedObject(self, &AssociatedKeys.ActionLeftBarName, newValue, .OBJC_ASSOCIATION_RETAIN) }
        get { return objc_getAssociatedObject(self, &AssociatedKeys.ActionLeftBarName) as? Action }
    }
    
    
    func addLeftBarButtonItem(img:UIImage? = nil, btnTitle:String? = nil, action: @escaping (UIBarButtonItem) -> Void){
        
        if btnTitle != nil {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: btnTitle, style: .plain, target: self, action: #selector(handleActionLeftBar(_:)))
        } else if img != nil {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItem.Style.plain, target: self, action: #selector(handleActionLeftBar(_:)))
        }else if img == nil {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(handleActionLeftBar(_:)))
        }
        
        leftBarAction = Action(action: action)
        
    }
    
    @objc dynamic private func handleActionLeftBar(_ recognizer: UIBarButtonItem) {
        leftBarAction?.action(recognizer)
    }
    
    //_________________
    
    
    func mk_makeTrasnparentTopBarWithAppColor() {
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = false

        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().isTranslucent = false

        let color = UIColor.white
        let font = UIFont(name: enum_font.medium.rawValue, size: 17)!

        let attributes = [
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.foregroundColor: color
        ]
        UINavigationBar.appearance().titleTextAttributes = attributes
        navigationController?.navigationBar.titleTextAttributes = attributes



//        if DeviceType.iPhoneXR || DeviceType.iPhoneXorXS || DeviceType.iPhoneXS_MAX {
//            print("This executes only on iPhoneX")
//            self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "AssetBackground"), for: UIBarMetrics.default)
//            UINavigationBar.appearance().setBackgroundImage(#imageLiteral(resourceName: "AssetBackground"), for: .default)
//        } else {
//            let imgAssetNavigationBack = #imageLiteral(resourceName: "AssetNavigationBack")
//            self.navigationController?.navigationBar.setBackgroundImage(imgAssetNavigationBack, for: UIBarMetrics.default)
//            UINavigationBar.appearance().setBackgroundImage(imgAssetNavigationBack, for: .default)
//        }

    }
    
    
    func mk_makeAppFontAttributesNavigationBar() {
        guard let navBar = self.navigationController?.navigationBar else {
            print("Nav bar nil found...")
            return
        }
        self.mk_makeAppFontAttributesWithNavigationBar(navBar: navBar)
    }
    
    func mk_makeAppFontAttributesWithNavigationBar(navBar:UINavigationBar) {
        navBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white,
             NSAttributedString.Key.font: UIFont.init(name: enum_font.medium.rawValue, size: 18)!]
    }
    
    
    
    
    
    @objc func backtoService(_ sender:UIBarButtonItem){
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    func setRightBarButtons(_ arrButtons: [UIBarButtonItem]){
        self.navigationItem.setRightBarButtonItems(arrButtons, animated: true)
    }
    
    func setLeftBarButtons(_ isString: Bool,Title title: String){
        
        if isString {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: title, style: .plain, target: self, action: nil)
            self.navigationItem.leftBarButtonItem?.isEnabled = false
        }else{
            
            let btnImg = UIBarButtonItem(image: UIImage(named: "AssetBackNavigation"), style: .plain, target: self, action:#selector(backtoService(_:)))
            let btnTitle = UIBarButtonItem(title: title, style: .plain, target: self, action: nil)
            btnTitle.isEnabled = false
            navigationItem.leftItemsSupplementBackButton = false
            navigationItem.setLeftBarButtonItems([btnImg, btnTitle], animated: true)
        }
    }
    
    func setNavigationBarTransperant(){
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true
        
    }
    
    func setNavigationProperties(_ isTranslucent : Bool, barColor : UIColor, barItemColor : UIColor, titleColor : UIColor){
        
        self.navigationController?.navigationBar.barTintColor = barColor
        self.navigationController?.navigationBar.tintColor = barItemColor
        self.navigationController?.navigationBar.isTranslucent = isTranslucent
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : titleColor]
        
    }
    
    func setCustomeNavigationImage(_ image : String)->Void{
        
        let imgView : UIImageView = UIImageView(image: UIImage(named: image))
        self.navigationItem.titleView = imgView
        
    }
    
    
    func setCustomeNavigationImage(_ image : UIImage)->Void{
        let imgView : UIImageView = UIImageView(image: image)
        imgView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imgView
    }
    
    func setCustomTitleView(_ navigationTitle : String!, titleColor : UIColor, titleFont : UIFont? = nil)->Void{
        
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = NSTextAlignment.center
        label.text = navigationTitle
        label.sizeToFit()
        label.textColor = titleColor
        label.font = titleFont
        self.navigationItem.titleView = label
    }
    
    func setCustomTitleViewLargeSmallLabels(_ navigationTitle : String!, navigationSubTitle : String!, titleColor : UIColor)->Void{
        
        let viewTitle = Bundle.main.loadNibNamed("ViewTitle", owner: self, options: nil)![0] as! UIView
        
        let lblTitle = (viewTitle.viewWithTag(10) as! UILabel)
        lblTitle.mk_setHTMLFromString(htmlText: navigationTitle)
        
        let lblSubTitle = (viewTitle.viewWithTag(20) as! UILabel)
        lblSubTitle.mk_setHTMLFromString(htmlText: navigationSubTitle)
        
        self.navigationItem.titleView = viewTitle
    }
    
    class func CVC(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return currentViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return currentViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return currentViewController(presented)
        }
        return base
    }
}






















//extension UIViewController {
//
//    private class Action {
//        var action: (UIBarButtonItem) -> Void
//
//        init(action: @escaping (UIBarButtonItem) -> Void) {
//            self.action = action
//        }
//    }
//
//    private struct AssociatedKeys {
//        static var ActionName = "action"
//        static var ActionRightBarName = "actionRightBar"
//        static var ActionLeftBarName = "actionLeftBar"
//    }
//
//    private var backAction: Action? {
//        set { objc_setAssociatedObject(self, &AssociatedKeys.ActionName, newValue, .OBJC_ASSOCIATION_RETAIN) }
//        get { return objc_getAssociatedObject(self, &AssociatedKeys.ActionName) as? Action }
//    }
//
//
//    @objc dynamic private func handleAction(_ recognizer: UIBarButtonItem) {
//        backAction?.action(recognizer)
//    }
//
//
//    func addLeftBackItem(action: @escaping (UIBarButtonItem) -> Void){
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: asset, style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleAction(_:)))
//
//        backAction = Action(action: action)
//
//    }
//
//    func addLeftBackItem() {
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "AssetBackNavigation"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backtoService(_:)))
//    }
//
//    ///////////
//
//    private var rightBarAction: Action? {
//        set { objc_setAssociatedObject(self, &AssociatedKeys.ActionRightBarName, newValue, .OBJC_ASSOCIATION_RETAIN) }
//        get { return objc_getAssociatedObject(self, &AssociatedKeys.ActionRightBarName) as? Action }
//    }
//
//
//    func addRightBarButtonItem(img:UIImage? = nil, btnTitle:String? = nil, action: @escaping (UIBarButtonItem) -> Void){
//
//        if btnTitle != nil {
//            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: btnTitle, style: .plain, target: self, action: #selector(handleActionRightBar(_:)))
//        } else if img != nil {
//            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleActionRightBar(_:)))
//        }else if img == nil {
//            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(handleActionRightBar(_:)))
//        }
//
//        rightBarAction = Action(action: action)
//
//    }
//
//    @objc dynamic private func handleActionRightBar(_ recognizer: UIBarButtonItem) {
//        rightBarAction?.action(recognizer)
//    }
//
//    ////////////
//
//
//
//
//
//    ///////////
//
//    private var leftBarAction: Action? {
//        set { objc_setAssociatedObject(self, &AssociatedKeys.ActionLeftBarName, newValue, .OBJC_ASSOCIATION_RETAIN) }
//        get { return objc_getAssociatedObject(self, &AssociatedKeys.ActionLeftBarName) as? Action }
//    }
//
//
//    func addLeftBarButtonItem(img:UIImage? = nil, btnTitle:String? = nil, action: @escaping (UIBarButtonItem) -> Void){
//
//        if btnTitle != nil {
//            self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: btnTitle, style: .plain, target: self, action: #selector(handleActionLeftBar(_:)))
//        } else if img != nil {
//            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleActionLeftBar(_:)))
//        }else if img == nil {
//            self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(handleActionLeftBar(_:)))
//        }
//
//        leftBarAction = Action(action: action)
//
//    }
//
//    @objc dynamic private func handleActionLeftBar(_ recognizer: UIBarButtonItem) {
//        leftBarAction?.action(recognizer)
//    }
//
//    //_________________
//
//
//    func mk_makeTrasnparentTopBarWithAppColor() {
//        UINavigationBar.appearance().barTintColor = .white
//        UINavigationBar.appearance().tintColor = .white
//        UINavigationBar.appearance().setBackgroundImage(#imageLiteral(resourceName: "AssetBackground"), for: .default)
//        UINavigationBar.appearance().shadowImage = UIImage()
//        UINavigationBar.appearance().isTranslucent = false
//
//        let color = UIColor.white
//        let font = UIFont(name: AppDelegate.xFontNameMedium, size: 17)!
//
//        let attributes = [
//            NSAttributedStringKey.font: font,
//            NSAttributedStringKey.foregroundColor: color
//        ]
//        UINavigationBar.appearance().titleTextAttributes = attributes
//    }
//
//
//    func mk_makeAppFontAttributesNavigationBar() {
//        guard let navBar = self.navigationController?.navigationBar else {
//            print("Nav bar nil found...")
//            return
//        }
//        self.mk_makeAppFontAttributesWithNavigationBar(navBar: navBar)
//    }
//
//    func mk_makeAppFontAttributesWithNavigationBar(navBar:UINavigationBar) {
//        navBar.titleTextAttributes =
//            [NSAttributedStringKey.foregroundColor: UIColor.white,
//             NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18.5)]
//    }
//
//
//
//
//
//    @objc func backtoService(_ sender:UIBarButtonItem){
//        if let navController = self.navigationController {
//            navController.popViewController(animated: true)
//        }
//    }
//
//    func setRightBarButtons(_ arrButtons: [UIBarButtonItem]){
//        self.navigationItem.setRightBarButtonItems(arrButtons, animated: true)
//    }
//
//    func setLeftBarButtons(_ isString: Bool,Title title: String){
//
//        if isString {
//            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: title, style: .plain, target: self, action: nil)
//            self.navigationItem.leftBarButtonItem?.isEnabled = false
//        }else{
//
//            let btnImg = UIBarButtonItem(image: UIImage(named: "AssetBackNavigation"), style: .plain, target: self, action:#selector(backtoService(_:)))
//            let btnTitle = UIBarButtonItem(title: title, style: .plain, target: self, action: nil)
//            btnTitle.isEnabled = false
//            navigationItem.leftItemsSupplementBackButton = false
//            navigationItem.setLeftBarButtonItems([btnImg, btnTitle], animated: true)
//        }
//    }
//
//    func setNavigationBarTransperant(){
//
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.tintColor = enumCCNavigation.colorWhite.setColor()
//        self.navigationController?.navigationBar.isTranslucent = true
//
//    }
//
//    func setNavigationProperties(_ isTranslucent : Bool, barColor : UIColor, barItemColor : UIColor, titleColor : UIColor){
//
//        self.navigationController?.navigationBar.barTintColor = barColor
//        self.navigationController?.navigationBar.tintColor = barItemColor
//        self.navigationController?.navigationBar.isTranslucent = isTranslucent
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : titleColor]
//
//    }
//
//    func setCustomeNavigationImage(_ image : String)->Void{
//
//        let imgView : UIImageView = UIImageView(image: UIImage(named: image))
//        self.navigationItem.titleView = imgView
//
//    }
//
//    func setCustomTitleView(_ navigationTitle : String!, titleColor : UIColor, titleFont : UIFont? = nil)->Void{
//
//        let label = UILabel()
//        label.numberOfLines = 2
//        label.textAlignment = NSTextAlignment.center
//        label.text = navigationTitle
//        label.sizeToFit()
//        label.textColor = titleColor
//        label.font = titleFont
//        self.navigationItem.titleView = label
//    }
//
//    func setCustomTitleViewLargeSmallLabels(_ navigationTitle : String!, navigationSubTitle : String!, titleColor : UIColor)->Void{
//
//        let viewTitle = Bundle.main.loadNibNamed("ViewTitle", owner: self, options: nil)![0] as! UIView
//
//        let lblTitle = (viewTitle.viewWithTag(10) as! UILabel)
//        lblTitle.mk_setHTMLFromString(htmlText: navigationTitle)
//
//        let lblSubTitle = (viewTitle.viewWithTag(20) as! UILabel)
//        lblSubTitle.mk_setHTMLFromString(htmlText: navigationSubTitle)
//
//        self.navigationItem.titleView = viewTitle
//    }
//
//    class func CVC(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
//        if let nav = base as? UINavigationController {
//            return currentViewController(nav.visibleViewController)
//        }
//        if let tab = base as? UITabBarController {
//            if let selected = tab.selectedViewController {
//                return currentViewController(selected)
//            }
//        }
//        if let presented = base?.presentedViewController {
//            return currentViewController(presented)
//        }
//        return base
//    }
//}



extension UITextField {
    
    public func isValidPassword() -> Bool {
        
        let passwordRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$"
        let passTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passTest.evaluate(with: self.text?.trimWhiteSpace())
    }
    
   
}




extension String {
    func getPref() -> Any! {
        let userDefault: UserDefaults = UserDefaults.standard
        return userDefault.object(forKey: self)
    }
    
    func setPrefNil() {
        let userDefault: UserDefaults = UserDefaults.standard
        userDefault.removeObject(forKey: self)
        userDefault.synchronize()
    }
    
    func setPref(value:Any) {
        let userDefault: UserDefaults = UserDefaults.standard
        userDefault.set(value, forKey: self)
        userDefault.synchronize()
    }
}

extension UITapGestureRecognizer {
    private class GestureAction {
        var action: (UITapGestureRecognizer) -> Void
        
        init(action: @escaping (UITapGestureRecognizer) -> Void) {
            self.action = action
        }
    }
    
    private struct AssociatedKeys {
        static var ActionName = "action"
    }
    
    private var gestureAction: GestureAction? {
        set { objc_setAssociatedObject(self, &AssociatedKeys.ActionName, newValue, .OBJC_ASSOCIATION_RETAIN) }
        get { return objc_getAssociatedObject(self, &AssociatedKeys.ActionName) as? GestureAction }
    }
    
    /**
     Convenience initializer, associating an action closure with the gesture recognizer (instead of the more traditional target/action).
     
     - parameter action: The closure for the recognizer to execute. There is no pre-logic to conditionally invoke the closure or not (e.g. only invoke the closure if the gesture recognizer is in a particular state). The closure is merely invoked directly; all handler logic is up to the closure.
     
     - returns: The UIGestureRecognizer.
     */
    public convenience init(action: @escaping (UITapGestureRecognizer) -> Void) {
        self.init()
        gestureAction = GestureAction(action: action)
        addTarget(self, action: #selector(handleAction(_:)))
    }
    
    @objc dynamic private func handleAction(_ recognizer: UITapGestureRecognizer) {
        gestureAction?.action(recognizer)
    }
}



extension UIView {
    func Xx() -> CGFloat {
        return self.frame.origin.x
    }
    
    func Yy() -> CGFloat {
        return self.frame.origin.y
    }
    
    func height() -> CGFloat {
        return self.frame.size.height
    }
    
    func width() -> CGFloat {
        return self.frame.size.width
    }
    
    func xPlusWidth() -> CGFloat {
        return self.frame.size.width + self.frame.origin.x
    }
    
    func yPlusHeight() -> CGFloat {
        return self.frame.size.height + self.frame.origin.y
    }
}


extension UIView {
    class func loadFromNib<T>(withName nibName: String) -> T? {
        let nib  = UINib.init(nibName: nibName, bundle: nil)
        let nibObjects = nib.instantiate(withOwner: nil, options: nil)
        for object in nibObjects {
            if let result = object as? T {
                return result
            }
        }
        return nil
    }
}


public extension UIImage {
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}


public extension String {
    func mk_openUrl() {
        guard let url = URL(string: self) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func mk_openMail() {
        if let url = URL(string: "mailto:\(self)") {
            
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                BasicFunctions.displayAlert("Can not open email client.")
            }
            
        }
    }
    
    func mk_openPhone() {
        if let url = URL(string: "tel://\(self)") {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                BasicFunctions.displayAlert("Can not open phone call client.")
            }
        }
    }
    
    
    
    
}

extension UIView {
    func mk_shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
}


extension UILabel {
    func mk_setHTMLFromString(htmlText: String) {
        
        let modifiedFont = String(format:"<center><span style=\" color: white;  font-family: '-apple-system-headline', 'HelveticaNeue'; font-size: \(self.font!.pointSize)\">%@</span><center>", htmlText)
        
        //process collection values
        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .utf8, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        
        
        self.attributedText = attrStr
    }
    
    
    func mk_setHTMLFromString2(htmlText: String) {
        
        let modifiedFont = String(format:"<span style=\" color: \(self.textColor?.mk_toHexCode() ?? "000000"); font-family: '\(self.font!.fontName)', 'HelveticaNeue'; font-size: \(self.font!.pointSize)\">%@</span>", htmlText)
        
        //process collection values
        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .utf8, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html,
                      NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        
//        var newString = NSMutableAttributedString(attributedString: attrStr)
//        var range: NSRange = [0, newString.length]
//        newString.enumerateAttribute(NSFontAttributeName, in: range, options: .longestEffectiveRangeNotRequired, using: {(_ value: Any, _ range: NSRange, _ stop: Bool) -> Void in
//            var replacementFont = UIFont(name: "Palatino-Roman", size: 14.0)
//            newString.addAttribute(NSFontAttributeName, value: replacementFont!, range: range)
//        })
        
        self.attributedText = attrStr
    }
    
    
    func mk_htmlAttString(text:String,withColor:String) {
        
        let htmlString = "<span style=\" color: \(withColor);  font-family: '-apple-system', 'HelveticaNeue'; font-size: \(self.font!.pointSize)\">\(text)</span>"
        let attrStr = try! NSAttributedString(
            data: htmlString.data(using: .utf8, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        self.attributedText = attrStr
    }
}

extension String {
    func mk_htmlAttString() -> NSAttributedString {
        let attrStr = try! NSAttributedString(
            data: self.data(using: .utf8, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        return attrStr
    }
    
    
    func mk_htmlAttString(withColor:String) -> NSAttributedString {
        let htmlString = "<font color=\"\(withColor)\">\(self)</font>"
        let attrStr = try! NSAttributedString(
            data: htmlString.data(using: .utf8, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        return attrStr
    }
    
    
    
}


extension Date {
    func isBetween(_ date1: Date, and date2: Date) -> Bool {
        return (min(date1, date2) ... max(date1, date2)).contains(self)
    }
}


extension UITextField {
    func modifyClearButtonWithImage(image : UIImage) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: -10, y: 0, width: 15, height: 15)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(self.clear(sender:)), for: .touchUpInside)
        self.rightView = clearButton
        self.rightViewMode = UITextField.ViewMode.always
        self.leftView = nil
        self.leftViewMode = UITextField.ViewMode.never
    }
    
    @objc func clear(sender : UITextField) {
        sender.text = ""
    }
}


//let url = URL(string: "")
//let semaphore = DispatchSemaphore(value: 0) // 1
////        let _ = DownloadPhoto(url: url!) {
////            _, error in
////            if let error = error {
////            }
//semaphore.signal() // 2
////        }
//let timeout = DispatchTime.now() + .seconds(5)
//if semaphore.wait(timeout: timeout) == .timedOut { // 3
//}












////////////////////////////////////////////////





















extension Int {
    func toString() -> String? {
        return String(format:"%d",self)
    }
    
    func toAbs() -> Int {
        return abs(self)
    }
}



@IBDesignable  class  GradientView2: UIView {
    var gradient:CAGradientLayer
    @IBInspectable var startColor:UIColor = UIColor.white {
        didSet {
            self.updateGradient()
        }
    }
    
    @IBInspectable var color1:UIColor? = nil {
        didSet {
            self.updateGradient()
        }
    }
    
    @IBInspectable var stop1:Double = (1.0 / 3.0) {
        didSet {
            self.updateGradient()
        }
    }
    
    @IBInspectable var color2:UIColor? = nil {
        didSet {
            self.updateGradient()
        }
    }
    
    @IBInspectable var stop2:Double = (2.0 / 3.0) {
        didSet {
            self.updateGradient()
        }
    }
    
    @IBInspectable var endColor:UIColor = UIColor.black {
        didSet {
            self.updateGradient()
        }
    }
    
    @IBInspectable var isHorizontal:Bool {
        get {
            return self.gradient.endPoint.y == self.gradient.startPoint.y
        }
        set {
            self.gradient.endPoint = newValue ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 1)
        }
    }
    
    override init(frame: CGRect) {
        gradient = CAGradientLayer()
        super.init(frame: frame)
        self.configGradient()
    }
    
    required init?(coder aDecoder: NSCoder) {
        gradient = CAGradientLayer()
        super.init(coder: aDecoder)
        self.configGradient()
    }
    
    func configGradient() {
        self.backgroundColor = UIColor.clear
        self.layer.insertSublayer(self.gradient, at: 0)
        self.gradient.masksToBounds = true
        self.gradient.frame = self.bounds
        self.gradient.startPoint = CGPoint(x: 0, y: 0)
        self.gradient.endPoint = CGPoint(x: 1, y: 0)
        self.updateGradient()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.gradient.frame = self.bounds
    }
    
    func updateGradient() {
        var colors:[CGColor] = []
        var locations:[NSNumber] = []
        colors.append(self.startColor.cgColor)
        locations.append(0.0)
        
        if let color = self.color1 {
            colors.append(color.cgColor)
            locations.append(NSNumber.init(value: self.stop1))}
        
        if let color = self.color2 {
            colors.append(color.cgColor)
            locations.append(NSNumber.init(value: self.stop2))
        }
        
        colors.append(self.endColor.cgColor)
        locations.append(1.0)
        
        self.gradient.colors = colors
        self.gradient.locations = locations
        
        self.layer.setNeedsDisplay()
    }
}

//  The converted code is limited to 1 KB.
//  Please Sign Up (Free!) to remove this limitation.
//
//  Converted to Swift 4 by Swiftify v4.1.6766 - https://objectivec2swift.com/
extension UIColor {
    
    open class func mk_ColorGradientLeftTopToRIghtBottom(withFrame frame: CGRect, andColors colors: [UIColor]) -> UIColor {
        let backgroundGradientLayer = CAGradientLayer()
        backgroundGradientLayer.frame = frame
        var cgColors: [AnyHashable] = []
        for color: UIColor in colors {
            cgColors.append(color.cgColor)
        }
        
        //        let gradientOffset = frame.height / frame.width / 2
        //        backgroundGradientLayer.startPoint = CGPoint(x: 0, y: (0.5 - gradientOffset))
        //        backgroundGradientLayer.endPoint = CGPoint(x: 1, y: (0.5 + gradientOffset))
        //        backgroundGradientLayer.locations = [0.25, 0.75]
        backgroundGradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        backgroundGradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        backgroundGradientLayer.locations = [0.38, 0.99]
        
        backgroundGradientLayer.colors = cgColors
        UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, false, UIScreen.main.scale)
        if let aContext = UIGraphicsGetCurrentContext() {
            backgroundGradientLayer.render(in: aContext)
        }
        let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let anImage = backgroundColorImage {
            return UIColor(patternImage: anImage)
        }
        return colors.first!
    }
    
    open class func mk_ColorGradientHorizontal(withFrame frame: CGRect, andColors colors: [UIColor]) -> UIColor {
        let backgroundGradientLayer = CAGradientLayer()
        backgroundGradientLayer.frame = frame
        var cgColors: [AnyHashable] = []
        for color: UIColor in colors {
            cgColors.append(color.cgColor)
        }

        backgroundGradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        backgroundGradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        backgroundGradientLayer.locations = [0.38, 0.99]
        
        backgroundGradientLayer.colors = cgColors
        UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, false, UIScreen.main.scale)
        if let aContext = UIGraphicsGetCurrentContext() {
            backgroundGradientLayer.render(in: aContext)
        }
        let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let anImage = backgroundColorImage {
            return UIColor(patternImage: anImage)
        }
        return colors.first!
    }
    
    open class func mk_ColorGradientVertical(withFrame frame: CGRect, andColors colors: [UIColor]) -> (UIColor?,UIImage?) {
        let backgroundGradientLayer = CAGradientLayer()
        backgroundGradientLayer.frame = frame
        var cgColors: [AnyHashable] = []
        for color: UIColor in colors {
            cgColors.append(color.cgColor)
        }

        backgroundGradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
        backgroundGradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
        backgroundGradientLayer.locations = [0.38, 0.99]
        backgroundGradientLayer.backgroundColor = UIColor.clear.cgColor
        
        backgroundGradientLayer.colors = cgColors
        UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, false, UIScreen.main.scale)
        if let aContext = UIGraphicsGetCurrentContext() {
            backgroundGradientLayer.render(in: aContext)
        }
        let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let anImage = backgroundColorImage {
            return (UIColor(patternImage: anImage),anImage)
        }
        return (colors.first,nil)
    }
}

extension UIView {
    
    // Example use: myView.addBorder(toSide: .Left, withColor: UIColor.redColor().CGColor, andThickness: 1.0)
    
    enum ViewSide {
        case Left, Right, Top, Bottom
    }
    
    func mk_addBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat) {
        
        let border = CALayer()
        border.backgroundColor = color
        
        switch side {
        case .Left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height); break
        case .Right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height); break
        case .Top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness); break
        case .Bottom: border.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness); break
        }
        
        layer.addSublayer(border)
    }
}



//
//
//@IBDesignable
//class MK_DesignableView: UIView {
//}
//
//@IBDesignable
//class MK_DesignableButton: UIButton {
//}
//
//@IBDesignable
//class MK_DesignableLabel: UILabel {
//}
//
//extension UIView {
//    
//    @IBInspectable
//    var mk_cornerRadius: CGFloat {
//        get {
//            return layer.cornerRadius
//        }
//        set {
//            layer.cornerRadius = newValue
//        }
//    }
//    
//    @IBInspectable
//    var mk_borderWidth: CGFloat {
//        get {
//            return layer.borderWidth
//        }
//        set {
//            layer.borderWidth = newValue
//        }
//    }
//    
//    @IBInspectable
//    var mk_borderColor: UIColor? {
//        get {
//            if let color = layer.borderColor {
//                return UIColor(cgColor: color)
//            }
//            return nil
//        }
//        set {
//            if let color = newValue {
//                layer.borderColor = color.cgColor
//            } else {
//                layer.borderColor = nil
//            }
//        }
//    }
//    
//    @IBInspectable
//    var mk_shadowRadius: CGFloat {
//        get {
//            return layer.shadowRadius
//        }
//        set {
//            layer.shadowRadius = newValue
//        }
//    }
//    
//    @IBInspectable
//    var mk_shadowOpacity: Float {
//        get {
//            return layer.shadowOpacity
//        }
//        set {
//            layer.shadowOpacity = newValue
//        }
//    }
//    
//    @IBInspectable
//    var mk_shadowOffset: CGSize {
//        get {
//            return layer.shadowOffset
//        }
//        set {
//            layer.shadowOffset = newValue
//        }
//    }
//    
//    @IBInspectable
//    var mk_shadowColor: UIColor? {
//        get {
//            if let color = layer.shadowColor {
//                return UIColor(cgColor: color)
//            }
//            return nil
//        }
//        set {
//            if let color = newValue {
//                layer.shadowColor = color.cgColor
//            } else {
//                layer.shadowColor = nil
//            }
//        }
//    }
//}



extension UICollectionView {
    
    func loadingIndicator(show: Bool) {
        let tag = 9876
        if show {
            let indicator = UIActivityIndicatorView()
            indicator.tag = tag
            self.addSubview(indicator)
            
            indicator.translatesAutoresizingMaskIntoConstraints = false
            let cntrCenterX = NSLayoutConstraint.init(item: indicator, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
            let cntrCenterY = NSLayoutConstraint.init(item: indicator, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
            self.addConstraints([cntrCenterX,cntrCenterY])
            
            indicator.startAnimating()
        } else {
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
    
}




func cHeight(heightPerc:CGFloat) -> CGFloat {
    return ssh * heightPerc
}
func cWidth(widthPerc:CGFloat) -> CGFloat {
    return ssw * widthPerc
}


extension UIView {
    class func fromNib<T : UIView>(nibName:String) -> T {
        return Bundle.main.loadNibNamed(nibName, owner: nil, options: nil)![0] as! T
    }
}


private var xoAssociationKey: UIView!

extension UIView {
    var parentFrameIndexView: UIView! {
        get {
            return objc_getAssociatedObject(self, &xoAssociationKey) as? UIView
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}

extension UIImage {
    class func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(origin: CGPoint(x: 0, y:0), size: CGSize(width: 1, height: 1))
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()!
        
        context.setFillColor(color.cgColor)
        context.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}



private var xoAsspciateStringCenterX: CGFloat!

extension UIImageView {
    var xoAssStringCenterX: CGFloat! {
        get {
            return objc_getAssociatedObject(self, &xoAsspciateStringCenterX) as? CGFloat
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAsspciateStringCenterX, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}


private var xoAsspciateStringCenterY: CGFloat!

extension UIImageView {
    var xoAssStringCenterY: CGFloat! {
        get {
            return objc_getAssociatedObject(self, &xoAsspciateStringCenterY) as? CGFloat
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAsspciateStringCenterY, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}



extension UIView {
    
    /**
     Removes all constrains for this view
     */
    func removeConstraints() {
        var list = [NSLayoutConstraint]()
        if let constraints = self.superview?.constraints {
            for c in constraints {
                if c.firstItem as? UIView == self || c.secondItem as? UIView == self {
                    list.append(c)
                }
            }
        }
        
        self.superview?.removeConstraints(list)
        self.removeConstraints(self.constraints)
    }
}


extension UIView {
    func mk_cornerRadious(radious:CGFloat) {
        self.layer.cornerRadius = radious
    }
}




///////////



extension UIImageView {
    
    
    
    public func mk_imageFromServerURL(urlString: String?, defaultImage : String?) {
        if let di = defaultImage {
            self.image = UIImage(named: di)
        }
        
        if let strUrl = urlString {
            if let url = URL.init(string: strUrl) {
                URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
                    
                    if error != nil {
                        print(error ?? "error")
                        return
                    }
                    
                    DispatchQueue.main.async(execute: { () -> Void in
                        let image = UIImage(data: data!)
                        self.image = image
                    })
                    
                }).resume()
            }
        }
        
        
    }
    
    public func mk_imageFromServerURL(url: URL, defaultImage : String?) {
        if let di = defaultImage {
            self.image = UIImage(named: di)
        }
        
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? "error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
            
        }).resume()
    }
}


extension UIImage{
    convenience init(view: UIView) {
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage)!)
        
    }
}

extension UIView {
    func mk_border(radious:CGFloat,borderColor:UIColor,borderWidth:CGFloat) {
        self.layer.cornerRadius = radious
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
    
    func mk_addBorderWithCornerRadious(rect:CGRect,corners:UIRectCorner, radious:CGFloat,borderColor:UIColor,borderWidth:CGFloat) {
        let v = self
        // Add rounded corners
        let maskLayer = CAShapeLayer()
        maskLayer.frame = rect
        maskLayer.path = UIBezierPath.init(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radious, height: radious)).cgPath
        v.layer.mask = maskLayer
        
        // Add border
        let borderLayer = CAShapeLayer()
        borderLayer.path = maskLayer.path // Reuse the Bezier path
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = borderColor.cgColor
        borderLayer.lineWidth = borderWidth
        borderLayer.frame = rect
        v.layer.addSublayer(borderLayer)
    }
    
    func mk_addBorderWithCornerRadiousAndShadow(rect:CGRect, corners:UIRectCorner, radious:CGFloat, borderColor:UIColor,borderWidth:CGFloat, shadowOffset:CGSize) {
        
        let v = self
        
        // Add rounded corners
        let maskLayer = CAShapeLayer()
        maskLayer.frame = rect
        maskLayer.path = UIBezierPath.init(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radious, height: radious)).cgPath
        v.layer.mask = maskLayer
        
        // Add border
        let borderLayer = CAShapeLayer()
        borderLayer.path = maskLayer.path // Reuse the Bezier path
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = borderColor.cgColor
        borderLayer.lineWidth = borderWidth
        borderLayer.frame = rect
        v.layer.addSublayer(borderLayer)
        
        
        // shadow
        v.layer.shadowColor = UIColor.black.cgColor
        v.layer.shadowOffset = shadowOffset
        v.layer.shadowOpacity = 0.7
        v.layer.shadowRadius = 4.0
        
        
    }
    
    
    
    func mk_addBorderWithCornerRadiousAndShadow(borderColor:UIColor,borderWidth:CGFloat,borderRadious:CGFloat, shadowColor:UIColor, shadowOffset:CGSize,shadowOpacity:Float = 0.5,shadowRadious:CGFloat = 0.0) {
        // corner radius
        self.layer.cornerRadius = borderRadious
        
        // border
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        
        // shadow
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowRadius = shadowRadious == 0.0 ? borderRadious : shadowRadious
    }
    
    func mk_addTopBorder(borderColor:UIColor,borderWidth:CGFloat) -> UIView? {
        let viewBorder = UIView.init()
        self.addSubview(viewBorder)
        viewBorder.backgroundColor = borderColor
        viewBorder.accessibilityHint = "mk_border"
        self.mk_addConstraintsInParentWithSides(childView: viewBorder, leftSide: true, rightSide: true, topSide: true, bottomSide: false)
        viewBorder.mk_addConstraintsHeight(height: 0.8)
        return viewBorder
    }
    
    func mk_removeAddedBorders() {
        for subView in self.subviews {
            if subView.accessibilityHint == "mk_border" {
                subView.removeFromSuperview()
            }
        }
    }
    
}

extension UIView {
    func mk_addFourSideConstraintsInParent(childView:UIView) {
        
        self.addSubview(childView)
        childView.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraint = NSLayoutConstraint(item: childView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        
        let leadingConstraint = NSLayoutConstraint(item: childView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint = NSLayoutConstraint(item: childView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        
        self.addConstraints([topConstraint, bottomConstraint, leadingConstraint, trailingConstraint])
    }
    
    func mk_addFourSideConstraintsInParent(childView:UIView,leftOffset:CGFloat,rightOffset:CGFloat,topOffset:CGFloat,bottomOffset:CGFloat) {
        
        self.addSubview(childView)
        childView.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraint = NSLayoutConstraint(item: childView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: topOffset)
        let bottomConstraint = NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: bottomOffset)
        
        let leadingConstraint = NSLayoutConstraint(item: childView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: leftOffset)
        let trailingConstraint = NSLayoutConstraint(item: childView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: rightOffset)
        
        self.addConstraints([topConstraint, bottomConstraint, leadingConstraint, trailingConstraint])
    }
    
    func mk_addConstraintsInParentWithSides(childView:UIView,leftSide:Bool=false,rightSide:Bool=false,topSide:Bool=false,bottomSide:Bool=false) {
        
        
        if childView.isDescendant(of: self) == false {
            self.addSubview(childView)
        }
        
        childView.translatesAutoresizingMaskIntoConstraints = false
        
        if leftSide {
            let leadingConstraint = NSLayoutConstraint(item: childView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
            self.addConstraints([leadingConstraint])
        }
        if rightSide {
            let trailingConstraint = NSLayoutConstraint(item: childView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
            self.addConstraints([trailingConstraint])
        }
        if topSide {
            let topConstraint = NSLayoutConstraint(item: childView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
            self.addConstraints([topConstraint])
        }
        if bottomSide {
            let bottomConstraint = NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
            self.addConstraints([bottomConstraint])
        }
    }
    
    
    func mk_addConstraintsHeight(height:CGFloat) {
        self.translatesAutoresizingMaskIntoConstraints = false
        let heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: height)
        self.addConstraints([heightConstraint])
    }
}


func mk_makeAppFontAttributesAllViews(view:UIView,fontColor:UIColor,fontName:String,fontSize:CGFloat) {
    
    let font = UIFont(name: fontName, size: fontSize)!
    
    let attributes = [
        NSAttributedString.Key.font: font,
        NSAttributedString.Key.foregroundColor: fontColor
    ]
    
    if let btn = view as? UIButton {
        btn.setTitleColor(fontColor, for: .normal)
        let stringName = btn.title(for: .normal)
        btn.setAttributedTitle(NSAttributedString.init(string: stringName ?? "", attributes: attributes), for: .normal)
    }
    
    if let lbl = view as? UILabel {
        lbl.font = font
        lbl.textColor = fontColor
    }
    
}

func mk_makeAppFontAttributesBarButtonItem(barButtonItem:UIBarButtonItem) {
    //        barButtonItem.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white,
    //                                              NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16)], for: .normal)
    //
    let color = UIColor.white
    let font = UIFont(name: enum_font.bold.rawValue, size: 15)!
    
    let attributes = [
        NSAttributedString.Key.font: font,
        NSAttributedString.Key.foregroundColor: color
    ]
    barButtonItem.setTitleTextAttributes(attributes, for: .normal)
    
}


extension Array {
    
    
    mutating func removeObjectFromArray<T>(_ obj: T) where T : Equatable {
        self = self.filter({$0 as? T != obj})
    }
    
    func contains<T>(_ obj: T) -> Bool where T : Equatable {
        return self.filter({$0 as? T == obj}).count > 0
    }
    
    
}


func mask(viewToMask: UIView, maskRect: CGRect, invert: Bool = false) {
    let maskLayer = CAShapeLayer()
    let path = CGMutablePath()
    if (invert) {
        path.addRect(viewToMask.bounds)
    }
    path.addRect(maskRect)
    
    maskLayer.path = path
    if (invert) {
        maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
    }
    
    // Set the mask of the view.
    viewToMask.layer.mask = maskLayer;
}



extension FileManager {
    
    func createTemporaryDirectoy(dirName:String) -> URL? {
        let url = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(dirName)
        
        var isDirExists : ObjCBool = false
        self.fileExists(atPath: url.relativePath, isDirectory: &isDirExists)
        if isDirExists.boolValue {
            return url
        } else {
            do {
                try self.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
            } catch {
                return nil
            }
        }
        return url
    }
}

extension CGRect {
    func cR(arr:[CGFloat]) -> CGRect {
        return CGRect.init(x: arr[0], y: arr[1], width: arr[2], height: arr[3])
    }
}

extension CGRect {
    var center: CGPoint {
        get {
            return CGPoint(x: origin.x+size.width/2, y: origin.y+size.height/2)
        }
    }
}


extension UIColor {
    /**
     * Returns random color
     * EXAMPLE: self.backgroundColor = UIColor.random
     */
    static var random: UIColor {
        let r:CGFloat  = .random(in: 0 ... 1)
        let g:CGFloat  = .random(in: 0 ... 1)
        let b:CGFloat  = .random(in: 0 ... 1)
        return UIColor(red: r, green: g, blue: b, alpha: 1)
    }
}




extension UIView {
    func mk_touchedView(at touchPoint: CGPoint,arrSubViews:[UIView]? = nil) -> UIView? {
        
        if let arrSubViews = arrSubViews {
            if arrSubViews.count > 0 {
                for subView in arrSubViews {
                    if subView.frame.contains(touchPoint) {
                        print("THIS IS THE ONE")
                        return subView
                    }
                }
            }
            return nil
        } else {
            for subView in self.subviews {
                if subView.frame.contains(touchPoint) {
                    print("THIS IS THE ONE")
                    return subView
                }
            }
        }
        
        
        
        //Nothing touched
        return nil
    }
    
    func mk_arrTouchedViews(at touchPoint: CGPoint) -> [UIView] {
        var arrTouchedView = [UIView]()
        
        for subView: UIView? in self.subviews {
            if let subView = subView {
                if subView.frame.contains(touchPoint) {
                    print("THIS IS THE ONE")
                    arrTouchedView.append(subView)
                }
            }
            
        }
        
        //Nothing touched
        return arrTouchedView
    }
}

extension FileManager {
    func mk_filemanager_clearTmpDirectory() {
        do {
            let tmpDirectory = try contentsOfDirectory(atPath: NSTemporaryDirectory())
            try tmpDirectory.forEach {[unowned self] file in
                let path = String.init(format: "%@%@", NSTemporaryDirectory(), file)
                try self.removeItem(atPath: path)
            }
        } catch {
            print(error)
        }
    }
}


extension UIView {
    private struct Holder {
        static var _myComputedProperty = [String:Bool]()
        static var _mkcpIsFreshView = [String:Bool]()
    }
    
    var myComputedProperty:Bool {
        get {
            return Holder._myComputedProperty[self.debugDescription] ?? false
        }
        set(newValue) {
            Holder._myComputedProperty[self.debugDescription] = newValue
        }
    }
    
    var mkcpIsFreshView:Bool {
        get {
            if (self.accessibilityValue ?? "") == "true" {
                return true
            } else {
                return false
            }
        }
        set(newValue) {
            if newValue {
                self.accessibilityValue = "true"
            } else {
                self.accessibilityValue = "false"
            }
        }
    }
}


extension UIView {
    func mk_animateAlpha(sec:Double,alpha:CGFloat,comp:(()->())? = nil) {
        UIView.animate(withDuration: sec, delay: 0, options: UIView.AnimationOptions.allowUserInteraction, animations: {
            self.alpha = alpha
        }) { (success) in
            comp?()
        }
    }
    func mk_animateLayout(sec:Double,comp:(()->())? = nil) {
        UIView.animate(withDuration: sec, delay: 0, options: UIView.AnimationOptions.allowUserInteraction, animations: {
            self.layoutIfNeeded()
        }) { (success) in
            comp?()
        }
    }
}


extension Float {
    public var cgFloat : CGFloat {
        get {
            return CGFloat.init(self)
        }
    }
}

extension CGFloat {
    public var float : Float {
        get {
            return Float.init(self)
        }
    }
}


extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}


extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOpacity = 0.42
        layer.shadowOffset = CGSize(width: 0, height: 3.8)
        layer.shadowRadius = 3
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}


let isIphoneXOrBigger = UIScreen.main.bounds.height >= 812


extension UIScrollView {
    
    func scrollToTopOrLeft() {
        
        var offset = CGPoint(
            x: -self.contentInset.left,
            y: -self.contentInset.top)
        
        if #available(iOS 11.0, *) {
            offset = CGPoint(
                x: -self.adjustedContentInset.left,
                y: -self.adjustedContentInset.top)
        }
        
        self.setContentOffset(offset, animated: true)
        
    }
}

//extension UIColor {
//    func toHexString() -> String {
//        var r:CGFloat = 0
//        var g:CGFloat = 0
//        var b:CGFloat = 0
//        var a:CGFloat = 0
//        
//        getRed(&r, green: &g, blue: &b, alpha: &a)
//        
//        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
//        
//        return String(format:"#%06x", rgb)
//    }
//}


extension CGRect {
    
    func mk_setRectInString() -> String {
        return "\(self.origin.x)-\(self.origin.y)-\(self.width)-\(self.height)"
    }
    
    func mk_getRectFromString(str:String) -> CGRect {
        let arr = str.split(separator: "-")
        return CGRect.init(x: CGFloat((arr[0] as NSString).floatValue), y: CGFloat((arr[1] as! NSString).floatValue), width: CGFloat((arr[2] as! NSString).floatValue), height: CGFloat((arr[3] as NSString).floatValue))
    }
}



extension AppDelegate {
    func getCurrentAppVersion() -> String? {
        return Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
    }
}










func getPlistValueForKey(key:String) -> String? {
    return Bundle.main.object(forInfoDictionaryKey: key) as? String
}


extension CGSize {
    func mk_inset(value:CGFloat) -> CGSize {
        return CGSize.init(width: self.width + value, height: self.height + value)
    }
}


extension UITextField {
    func setPlaceholderWithColor(strPlaceholder:String) {
        self.attributedPlaceholder = NSAttributedString(string: strPlaceholder,
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
}


extension UIFont {
    func mk_changeFont(size:CGFloat) -> UIFont {
        return UIFont.init(name: self.fontName, size: size)!
    }
}

extension UIButton {
    func mk_changeFont(size:CGFloat) {
        if let lbl = self.titleLabel {
            lbl.font = UIFont.init(name: lbl.font.fontName, size: size)!
        }
    }
}


extension UILabel {
    func mk_changeFont(size:CGFloat) {
        self.font = UIFont.init(name: self.font.fontName, size: size)!
    }
}




extension UIGestureRecognizer {
    @discardableResult convenience init(addToView targetView: UIView,
                                        closure: @escaping () -> Void) {
        self.init()
        
        GestureTarget.add(gesture: self,
                          closure: closure,
                          toView: targetView)
    }
}

fileprivate class GestureTarget: UIView {
    class ClosureContainer {
        weak var gesture: UIGestureRecognizer?
        let closure: (() -> Void)
        
        init(closure: @escaping () -> Void) {
            self.closure = closure
        }
    }
    
    var containers = [ClosureContainer]()
    
    convenience init() {
        self.init(frame: .zero)
        isHidden = true
    }
    
    class func add(gesture: UIGestureRecognizer, closure: @escaping () -> Void,
                   toView targetView: UIView) {
        let target: GestureTarget
        if let existingTarget = existingTarget(inTargetView: targetView) {
            target = existingTarget
        } else {
            target = GestureTarget()
            targetView.addSubview(target)
        }
        let container = ClosureContainer(closure: closure)
        container.gesture = gesture
        target.containers.append(container)
        
        gesture.addTarget(target, action: #selector(GestureTarget.target(gesture:)))
        targetView.addGestureRecognizer(gesture)
    }
    
    class func existingTarget(inTargetView targetView: UIView) -> GestureTarget? {
        for subview in targetView.subviews {
            if let target = subview as? GestureTarget {
                return target
            }
        }
        return nil
    }
    
    func cleanUpContainers() {
        containers = containers.filter({ $0.gesture != nil })
    }
    
    @objc func target(gesture: UIGestureRecognizer) {
        cleanUpContainers()
        
        for container in containers {
            guard let containerGesture = container.gesture else {
                continue
            }
            
            if gesture === containerGesture {
                container.closure()
            }
        }
    }
}




extension UIViewController {
    
    func mk_setOverCurrentContext() {
        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
    }
    
}



extension UIView {
    func mk_getLabel() -> UILabel? {
        let lbl = self.subviews.filter { (viewSub) -> Bool in
            viewSub is UILabel
            }.first as? UILabel
        return lbl
    }
    
    func mk_getButton() -> UIButton? {
        let btn = self.subviews.filter { (viewSub) -> Bool in
            viewSub is UIButton
            }.first as? UIButton
        return btn
    }
    
    func mk_getImageView() -> UIImageView? {
        let btn = self.subviews.filter { (viewSub) -> Bool in
            viewSub is UIImageView
            }.first as? UIImageView
        return btn
    }
    
    func mk_getStackView() -> UIStackView? {
        let btn = self.subviews.filter { (viewSub) -> Bool in
            viewSub is UIStackView
            }.first as? UIStackView
        return btn
    }
}



extension UIView {
    
    func mk_animateAngleCollapse(seconds:TimeInterval) {
        UIView.animate(withDuration: seconds, animations: {
            self.transform = CGAffineTransform(rotationAngle: 0)
        })
    }
    
    func mk_animateAngleExpand(seconds:TimeInterval) {
        UIView.animate(withDuration: seconds, animations: {
            self.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        })
    }
    
    func mk_animateAngle(seconds:TimeInterval,rotationAngle: CGFloat) {
        UIView.animate(withDuration: seconds, animations: {
            self.transform = CGAffineTransform(rotationAngle: rotationAngle)
        })
    }
    
    
}







extension UIImage {
    // MARK: - UIImage+Resize
    
    func scaleImageWithAspectToWidth(toWidth:CGFloat) -> UIImage {
        let oldWidth:CGFloat = size.width
        let scaleFactor:CGFloat = toWidth / oldWidth
        
        let newHeight = self.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor;
        
        UIGraphicsBeginImageContext(CGSize.init(width: newWidth, height: newHeight))
        draw(in: CGRect.init(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}



extension UIImage {
    // MARK: - UIImage+Resize
    func mk_compressTo(_ expectedSizeInMb:Int) -> UIImage? {
        let sizeInBytes = expectedSizeInMb * 1024 * 1024
        var needCompress:Bool = true
        var imgData:Data?
        var compressingValue:CGFloat = 1.0
        while (needCompress && compressingValue > 0.0) {
            if let data:Data = self.jpegData(compressionQuality: compressingValue) {
                if data.count < sizeInBytes {
                    needCompress = false
                    imgData = data
                } else {
                    compressingValue -= 0.1
                }
            }
        }
        
        if let data = imgData {
            if (data.count < sizeInBytes) {
                return UIImage(data: data)
            }
        }
        return nil
    }
}




extension UIView
{
    func copyView<T: UIView>() -> T {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as! T
    }
}



extension UITableView {
    func mk_selectIndexpath(_ indexPathToSelect:IndexPath) {
        if let selectedIndexpath = self.indexPathForSelectedRow {
            self.deselectRow(at: selectedIndexpath, animated: true)
            self.delegate?.tableView!(self, didDeselectRowAt: selectedIndexpath)
            self.selectRow(at: indexPathToSelect, animated: true, scrollPosition: UITableView.ScrollPosition.none)
            self.delegate?.tableView!(self, didSelectRowAt: indexPathToSelect)
        } else {
            self.selectRow(at: indexPathToSelect, animated: true, scrollPosition: UITableView.ScrollPosition.none)
            self.delegate?.tableView!(self, didSelectRowAt: indexPathToSelect)
        }
    }
}



extension UICollectionView {
    func mk_selectIndexpath(_ indexPathToSelect:IndexPath) {
        if let arrSelectedIndexpaths = self.indexPathsForSelectedItems {
            
            for selectedIndexpath in arrSelectedIndexpaths {
                self.deselectItem(at: selectedIndexpath, animated: true)
                self.delegate?.collectionView!(self, didDeselectItemAt: selectedIndexpath)
            }
            self.selectItem(at: indexPathToSelect, animated: true, scrollPosition: UICollectionView.ScrollPosition.top)
            self.delegate?.collectionView!(self, didSelectItemAt: indexPathToSelect)
        } else {
            self.selectItem(at: indexPathToSelect, animated: true, scrollPosition: UICollectionView.ScrollPosition.top)
            self.delegate?.collectionView!(self, didSelectItemAt: indexPathToSelect)
        }
    }
}



extension CGSize {
    func mk_landscape()->Bool {
        return self.width > self.height
    }
}


//extension UIColor {
//    var hexString: String? {
//        var red: CGFloat = 0
//        var green: CGFloat = 0
//        var blue: CGFloat = 0
//        var alpha: CGFloat = 0
//        
//        let multiplier = CGFloat(255.999999)
//        
//        guard self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) else {
//            return nil
//        }
//        
//        if alpha == 1.0 {
//            return String(
//                format: "#%02lX%02lX%02lX",
//                Int(red * multiplier),
//                Int(green * multiplier),
//                Int(blue * multiplier)
//            )
//        }
//        else {
//            return String(
//                format: "#%02lX%02lX%02lX%02lX",
//                Int(red * multiplier),
//                Int(green * multiplier),
//                Int(blue * multiplier),
//                Int(alpha * multiplier)
//            )
//        }
//    }
//}


extension AppDelegate {
    func mk_makeRootVC(vc:UIViewController) {
        apde.window?.rootViewController = vc
        apde.window?.makeKeyAndVisible()
    }
    
}



extension UILabel {
    func getLabelHeight(_ labelSize: CGSize, string: String?, font: UIFont?) -> CGFloat {
        
        var size: CGSize
        
        let context = NSStringDrawingContext()
        var boundingBox: CGSize? = nil
        if let font = font {
            boundingBox = string?.boundingRect(with: labelSize, options: .usesLineFragmentOrigin, attributes: [
                NSAttributedString.Key.font: font
                ], context: context).size
        }
        if let bb = boundingBox {
            size = CGSize(width: ceil(bb.width), height: ceil(bb.height))
        } else {
            size = self.frame.size
        }
        
        return size.height
    }
}



extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


//extension NSAttributedString {
//    public convenience init?(HTMLString html: String, font: UIFont? = nil) throws {
//        let options : [NSAttributedString.DocumentReadingOptionKey : Any] =
//            [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
//             NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue]
//        
//        guard let data = html.data(using: .utf8, allowLossyConversion: true) else {
//            throw NSError(domain: "Parse Error", code: 0, userInfo: nil)
//        }
//        
//        if let font = font {
//            guard let attr = try? NSMutableAttributedString(data: data, options: options, documentAttributes: nil) else {
//                throw NSError(domain: "Parse Error", code: 0, userInfo: nil)
//            }
//            var attrs = attr.attributes(at: 0, effectiveRange: nil)
//            attrs[NSAttributedString.Key.font] = font
//            attr.setAttributes(attrs, range: NSRange(location: 0, length: attr.length))
//            self.init(attributedString: attr)
//        } else {
//            try? self.init(data: data, options: options, documentAttributes: nil)
//        }
//    }
//}




extension UIColor {
    func mk_toHexCode() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format: "%06x", rgb)
    }
}




extension UIImage {
    
    class func textEmbededImage(image: UIImage, string: String, color:UIColor, imageAlignment: Int = 0, segFont: UIFont? = nil) -> UIImage {
        let font = segFont ?? UIFont.systemFont(ofSize: 16.0)
        let expectedTextSize: CGSize = (string as NSString).size(withAttributes: [NSAttributedString.Key.font: font])
        let width: CGFloat = expectedTextSize.width + image.size.width + 5.0
        let height: CGFloat = max(expectedTextSize.height, image.size.width)
        let size: CGSize = CGSize(width: width, height: height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        let context: CGContext = UIGraphicsGetCurrentContext()!
        context.setFillColor(color.cgColor)
        let fontTopPosition: CGFloat = (height - expectedTextSize.height) / 2.0
        let textOrigin: CGFloat = (imageAlignment == 0) ? image.size.width + 5 : 0
        let textPoint: CGPoint = CGPoint.init(x: textOrigin, y: fontTopPosition)
        string.draw(at: textPoint, withAttributes: [NSAttributedString.Key.font: font])
        let flipVertical: CGAffineTransform = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: size.height)
        context.concatenate(flipVertical)
        let alignment: CGFloat =  (imageAlignment == 0) ? 0.0 : expectedTextSize.width + 5.0
        context.draw(image.cgImage!, in: CGRect.init(x: alignment, y: ((height - image.size.height) / 2.0), width: image.size.width, height: image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    class func getSegementImage(image: UIImage, string: String, tintColor:UIColor) -> UIImage {
        
        let viewSegment = UIView.fromNib(nibName: "ViewSegment")
        viewSegment.frame = .init(x: 0, y: 0, width: 54, height: 54)
        apde.window?.addSubview(viewSegment)
        
        
        let sv = viewSegment.mk_getStackView()
        
        if let img = sv?.mk_getImageView() {
            img.image = image.withRenderingMode(.alwaysTemplate)
            img.tintColor = tintColor
        }
        
        if let lbl = sv?.mk_getLabel() {
            lbl.text = string
            lbl.textColor = tintColor
        }
        
        let img = viewSegment.takeScreenshot()
        viewSegment.removeFromSuperview()
        
        return img
    }
    
    
    func imageRotatedByDegrees(_ degrees: CGFloat) -> UIImage {
        
        let size = self.size
        UIGraphicsBeginImageContext(size)
        
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: size.width / 2, y: size.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat(Double.pi / 180)))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)
        
        let origin = CGPoint(x: -size.width / 2, y: -size.width / 2)
        
        bitmap.draw(self.cgImage!, in: CGRect(origin: origin, size: size))
        
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
        
    }
}

extension UIView {
    
    func takeScreenshot() -> UIImage {
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
}






extension UIAlertController {
    
    /**
     Adds a UIAlertAction, initialized with a title, style, and completion handler.
     
     - parameter title: The text to use for the action title.
     - parameter style: The style to use for the action (optional, defaults to `.default`).
     - parameter handler: A closure to execute when the user selects the action (optional, defaults to `nil`).
     
     - returns: self
     */
    @discardableResult
    public func addAction(title: String?, style: UIAlertAction.Style = .default, handler: ((UIAlertAction) -> Void)? = nil) -> Self {
        addAction(UIAlertAction(title: title, style: style, handler: handler))
        return self
    }
    
    /**
     Creates a UIAlertController instance, styled as an action sheet.
     
     - parameter title: The title for the controller (optional, defaults to `nil`).
     - parameter message: The message for the controller (optional, defaults to `nil`).
     
     - returns: A UIAlertController instance, styled as an action sheet.
     */
    public static func actionSheet(title: String? = nil, message: String? = nil) -> UIAlertController {
        return UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
    }
    
    /**
     Creates a UIAlertController instance, styled as an alert.
     
     - parameter title: The title for the controller (optional, defaults to `nil`).
     - parameter message: The message for the controller (optional, defaults to `nil`).
     
     - returns: A UIAlertController instance, styled as an alert.
     */
    public static func alert(title: String? = nil, message: String? = nil) -> UIAlertController {
        return UIAlertController(title: title, message: message, preferredStyle: .alert)
    }
    
    /**
     Presents the UIAlertController inside the specified view controller.
     
     - parameter controller: The controller in which to present the alert controller (optional, defaults to `.current`).
     - parameter animated: Pass `true` to animate the presentation; otherwise, pass `false` (optional, defaults to `true`).
     - parameter completion: The closure to execute after the presentation finishes (optional, defaults to `nil`).
     */
    public func present(in controller: UIViewController?, animated: Bool = true, completion: (() -> Void)? = nil) {
        if let objController = controller {
            objController.present(self, animated: animated, completion: completion)
        } else {
            UIViewController.currentViewController()?.present(self, animated: animated, completion: completion)
        }
    }
    
    public func present() {
        UIViewController.currentViewController()?.present(self, animated: true, completion: nil)
    }
}




extension UserDefaults {
    
    internal func colorData(forKey key: String) -> UIColor? {
        guard let colorData = data(forKey: key) else {
            return nil
        }
        
        return NSKeyedUnarchiver.unarchiveObject(with: colorData) as? UIColor
    }
    
    internal func setColorData(_ color: UIColor?, forKey key: String) {
        let colorData: Data?
        if let color = color {
            colorData = NSKeyedArchiver.archivedData(withRootObject: color)
        }
        else {
            colorData = nil
        }
        
        set(colorData, forKey: key)
    }
}



private var AssociatedLastIndexPath: IndexPath?

extension UICollectionView {
    
    var mk_lastIndexPathSelected:IndexPath? {
        get {
            return objc_getAssociatedObject(self, &AssociatedLastIndexPath) as? IndexPath
        }
        set {
            objc_setAssociatedObject(self, &AssociatedLastIndexPath, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}



extension FileManager {
    
}


func mk_saveImageInDocsDir(folderName:String,image:UIImage,imageName:String) -> String {
    
    // get the documents directory url
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentsDirectory = paths[0] // Get documents folder
    let dataPath = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(folderName).relativePath //Set folder name
    print(dataPath)
    //Check is folder available or not, if not create
    if !FileManager.default.fileExists(atPath: dataPath) {
        try! FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: true, attributes: nil) //Create folder if not
    }
    
    // create the destination file url to save your image
    let fileURL = URL(fileURLWithPath:dataPath).appendingPathComponent("\(imageName)")//Your image name
    print(fileURL)
    // get your UIImage jpeg data representation
    let data = image.pngData()//Set image quality here
    
    do {
        // writes the image data to disk
        try data?.write(to: fileURL, options: .atomic)
    } catch {
        print("error:", error)
    }
    
    return imageName
}

func mk_getImageInDocsDir(folderName:String,imageName:String) -> UIImage {
    
    // get the documents directory url
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentsDirectory = paths[0] // Get documents folder
    let dataPath = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(folderName).relativePath //Set folder relativePath
    print(dataPath)
    //Check is folder available or not, if not create
    if !FileManager.default.fileExists(atPath: dataPath) {
        try? FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: true, attributes: nil) //Create folder if not
    }
    
    // create the destination file url to save your image
    return UIImage.init(contentsOfFile: URL(fileURLWithPath:dataPath).appendingPathComponent("\(imageName)").path) ?? UIImage()
    
}



func mk_saveImageOfBackgroundScrollview(image:UIImage) -> String {
    let random = Int.random(in: 0...10000)
    let random2 = Int.random(in: 0...10000)
    
    return mk_saveImageInDocsDir(folderName: "images", image: image, imageName: FileManager.mk_generateFileName(withExtension: ".png") ?? "\(random)_\(random2).png")
}

func mk_getImageOfBackgroundScrollview(imageName:String) -> UIImage {
    return mk_getImageInDocsDir(folderName: "images", imageName: imageName)
}

extension UIImage {
    func mk_saveToDoc() -> String {
        return mk_saveImageOfBackgroundScrollview(image: self)
    }
}






extension String {
    func isLandscape() -> Bool {
        if self == "portrait" {
            return false
        }
        return true
    }
}


extension Int {
    func mkNumber() -> NSNumber {
        return NSNumber(value: self)
    }
}





extension FileManager {
    class func mk_generateFileName(withExtension extensionString: String?) -> String? {
        // Extenstion string is like @".png"

        let time = Date()
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy-hh-mm-ss"
        let timeString = df.string(from: time)
        let fileName = "File_\(timeString)\(extensionString ?? "")"

        return fileName
    }
}


extension Int {
    init(random range: Range<Int>) {

        let offset: Int
        if range.startIndex < 0 {
            offset = abs(range.startIndex)
        } else {
            offset = 0
        }

        let min = UInt32(range.startIndex + offset)
        let max = UInt32(range.endIndex   + offset)

        self = Int(min + arc4random_uniform(max - min)) - offset
    }
}



extension UILabel {
    func createCopy() -> UILabel {
        let archivedData = NSKeyedArchiver.archivedData(withRootObject: self)
        return NSKeyedUnarchiver.unarchiveObject(with: archivedData) as! UILabel
    }
}



extension CGFloat {
    func mk_number() -> NSNumber {
        return NSNumber(value: Float(self))
    }
}




private var AssociatedStickerID: NSNumber?

extension UIView {
    
    var mk_AssociatedStickerID:NSNumber? {
        get {
            return objc_getAssociatedObject(self, &AssociatedStickerID) as? NSNumber
        }
        set {
            objc_setAssociatedObject(self, &AssociatedStickerID, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}




func nextAvailble(_ idKey: String, forEntityName entityName: String, in context: NSManagedObjectContext) -> NSNumber? {

    let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: entityName)
    let entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
    req.entity = entity
    req.fetchLimit = 1
    req.propertiesToFetch = [idKey]
    let indexSort = NSSortDescriptor.init(key: idKey, ascending: false)
    req.sortDescriptors = [indexSort]
    
    do {
        let fetchedData = try context.fetch(req)
        let firstObject = fetchedData.first as! NSManagedObject
        if let foundValue = firstObject.value(forKey: idKey) as? NSNumber {
            return NSNumber.init(value: foundValue.intValue + 1)
        }
        
    } catch {
        print(error)
        
    }
    return nil
    
}



extension Dictionary where Key: ExpressibleByStringLiteral, Value:Any {
    
    var w_strTitle : String {
        return (self["title"] as? String) ?? ""
    }
    var w_strSubtitle : String {
        return (self["subTitle"] as? String) ?? ""
    }
    var w_colorText1 : String {
        return (self["colorText1"] as? String) ?? ""
    }
    var w_colorText2 : String {
        return (self["colorText2"] as? String) ?? ""
    }
    var w_colorBack : String {
        return (self["colorBack"] as? String) ?? ""
    }
    
}

extension String {
    func mk_toColor() -> UIColor{
        return UIColor.init(hexString: self)
//        return UIColor.init(mk_hexString: self) ?? .white
    }
}

//extension UIColor {
//    public convenience init?(mk_hexString: String) {
//        let r, g, b, a: CGFloat
//
////        if mk_hexString.hasPrefix("#") {
////            let start = mk_hexString.index(mk_hexString.startIndex, offsetBy: 1)
//            let hexColor = mk_hexString
//
//            if hexColor.count == 8 {
//                let scanner = Scanner(string: hexColor)
//                var hexNumber: UInt64 = 0
//
//                if scanner.scanHexInt64(&hexNumber) {
//                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
//                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
//                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
//                    a = CGFloat(hexNumber & 0x000000ff) / 255
//
//                    self.init(red: r, green: g, blue: b, alpha: a)
//                    return
//                }
//            }
////        }
//
//        return nil
//    }
//}


public extension UIColor {

    /**
     Creates an immuatble UIColor instance specified by a hex string, CSS color name, or nil.
     - parameter hexString: A case insensitive String? representing a hex or CSS value e.g.
     - **"abc"**
     - **"abc7"**
     - **"#abc7"**
     - **"00FFFF"**
     - **"#00FFFF"**
     - **"00FFFF77"**
     - **"Orange", "Azure", "Tomato"** Modern browsers support 140 color names (<http://www.w3schools.com/cssref/css_colornames.asp>)
     - **"Clear"** [UIColor clearColor]
     - **"Transparent"** [UIColor clearColor]
     - **nil** [UIColor clearColor]
     - **empty string** [UIColor clearColor]
     */
    convenience init(hex: String?) {
        let normalizedHexString: String = UIColor.normalize(hex)
        var c: CUnsignedInt = 0
        Scanner(string: normalizedHexString).scanHexInt32(&c)
        self.init(red:UIColorMasks.redValue(c), green:UIColorMasks.greenValue(c), blue:UIColorMasks.blueValue(c), alpha:UIColorMasks.alphaValue(c))
    }

    /**
     Returns a hex equivalent of this UIColor.
     - Parameter includeAlpha:   Optional parameter to include the alpha hex.
     color.hexDescription() -> "ff0000"
     color.hexDescription(true) -> "ff0000aa"
     - Returns: A new string with `String` with the color's hexidecimal value.
     */
    func hexDescription(_ includeAlpha: Bool = false) -> String {
        guard self.cgColor.numberOfComponents == 4 else {
            return "Color not RGB."
        }
        let a = self.cgColor.components!.map { Int($0 * CGFloat(255)) }
        let color = String.init(format: "%02x%02x%02x", a[0], a[1], a[2])
        if includeAlpha {
            let alpha = String.init(format: "%02x", a[3])
            return "\(color)\(alpha)"
        }
        return color
    }

    fileprivate enum UIColorMasks: CUnsignedInt {
        case redMask    = 0xff000000
        case greenMask  = 0x00ff0000
        case blueMask   = 0x0000ff00
        case alphaMask  = 0x000000ff

        static func redValue(_ value: CUnsignedInt) -> CGFloat {
            return CGFloat((value & redMask.rawValue) >> 24) / 255.0
        }

        static func greenValue(_ value: CUnsignedInt) -> CGFloat {
            return CGFloat((value & greenMask.rawValue) >> 16) / 255.0
        }

        static func blueValue(_ value: CUnsignedInt) -> CGFloat {
            return CGFloat((value & blueMask.rawValue) >> 8) / 255.0
        }

        static func alphaValue(_ value: CUnsignedInt) -> CGFloat {
            return CGFloat(value & alphaMask.rawValue) / 255.0
        }
    }

    fileprivate static func normalize(_ hex: String?) -> String {
        guard var hexString = hex else {
            return "00000000"
        }
        if let cssColor = cssToHexDictionairy[hexString.uppercased()] {
            return cssColor.count == 8 ? cssColor : cssColor + "ff"
        }
        if hexString.hasPrefix("#") {
            hexString = String(hexString.dropFirst())
        }
        if hexString.count == 3 || hexString.count == 4 {
            hexString = hexString.map { "\($0)\($0)" } .joined()
        }
        let hasAlpha = hexString.count > 7
        if !hasAlpha {
            hexString += "ff"
        }
        return hexString
    }

    /**
     All modern browsers support the following 140 color names (see http://www.w3schools.com/cssref/css_colornames.asp)
     */
    fileprivate static func hexFromCssName(_ cssName: String) -> String {
        let key = cssName.uppercased()
        if let hex = cssToHexDictionairy[key] {
            return hex
        }
        return cssName
    }

    fileprivate static let cssToHexDictionairy: [String: String] = [
        "CLEAR": "00000000",
        "TRANSPARENT": "00000000",
        "": "00000000",
        "ALICEBLUE": "F0F8FF",
        "ANTIQUEWHITE": "FAEBD7",
        "AQUA": "00FFFF",
        "AQUAMARINE": "7FFFD4",
        "AZURE": "F0FFFF",
        "BEIGE": "F5F5DC",
        "BISQUE": "FFE4C4",
        "BLACK": "000000",
        "BLANCHEDALMOND": "FFEBCD",
        "BLUE": "0000FF",
        "BLUEVIOLET": "8A2BE2",
        "BROWN": "A52A2A",
        "BURLYWOOD": "DEB887",
        "CADETBLUE": "5F9EA0",
        "CHARTREUSE": "7FFF00",
        "CHOCOLATE": "D2691E",
        "CORAL": "FF7F50",
        "CORNFLOWERBLUE": "6495ED",
        "CORNSILK": "FFF8DC",
        "CRIMSON": "DC143C",
        "CYAN": "00FFFF",
        "DARKBLUE": "00008B",
        "DARKCYAN": "008B8B",
        "DARKGOLDENROD": "B8860B",
        "DARKGRAY": "A9A9A9",
        "DARKGREY": "A9A9A9",
        "DARKGREEN": "006400",
        "DARKKHAKI": "BDB76B",
        "DARKMAGENTA": "8B008B",
        "DARKOLIVEGREEN": "556B2F",
        "DARKORANGE": "FF8C00",
        "DARKORCHID": "9932CC",
        "DARKRED": "8B0000",
        "DARKSALMON": "E9967A",
        "DARKSEAGREEN": "8FBC8F",
        "DARKSLATEBLUE": "483D8B",
        "DARKSLATEGRAY": "2F4F4F",
        "DARKSLATEGREY": "2F4F4F",
        "DARKTURQUOISE": "00CED1",
        "DARKVIOLET": "9400D3",
        "DEEPPINK": "FF1493",
        "DEEPSKYBLUE": "00BFFF",
        "DIMGRAY": "696969",
        "DIMGREY": "696969",
        "DODGERBLUE": "1E90FF",
        "FIREBRICK": "B22222",
        "FLORALWHITE": "FFFAF0",
        "FORESTGREEN": "228B22",
        "FUCHSIA": "FF00FF",
        "GAINSBORO": "DCDCDC",
        "GHOSTWHITE": "F8F8FF",
        "GOLD": "FFD700",
        "GOLDENROD": "DAA520",
        "GRAY": "808080",
        "GREY": "808080",
        "GREEN": "008000",
        "GREENYELLOW": "ADFF2F",
        "HONEYDEW": "F0FFF0",
        "HOTPINK": "FF69B4",
        "INDIANRED": "CD5C5C",
        "INDIGO": "4B0082",
        "IVORY": "FFFFF0",
        "KHAKI": "F0E68C",
        "LAVENDER": "E6E6FA",
        "LAVENDERBLUSH": "FFF0F5",
        "LAWNGREEN": "7CFC00",
        "LEMONCHIFFON": "FFFACD",
        "LIGHTBLUE": "ADD8E6",
        "LIGHTCORAL": "F08080",
        "LIGHTCYAN": "E0FFFF",
        "LIGHTGOLDENRODYELLOW": "FAFAD2",
        "LIGHTGRAY": "D3D3D3",
        "LIGHTGREY": "D3D3D3",
        "LIGHTGREEN": "90EE90",
        "LIGHTPINK": "FFB6C1",
        "LIGHTSALMON": "FFA07A",
        "LIGHTSEAGREEN": "20B2AA",
        "LIGHTSKYBLUE": "87CEFA",
        "LIGHTSLATEGRAY": "778899",
        "LIGHTSLATEGREY": "778899",
        "LIGHTSTEELBLUE": "B0C4DE",
        "LIGHTYELLOW": "FFFFE0",
        "LIME": "00FF00",
        "LIMEGREEN": "32CD32",
        "LINEN": "FAF0E6",
        "MAGENTA": "FF00FF",
        "MAROON": "800000",
        "MEDIUMAQUAMARINE": "66CDAA",
        "MEDIUMBLUE": "0000CD",
        "MEDIUMORCHID": "BA55D3",
        "MEDIUMPURPLE": "9370DB",
        "MEDIUMSEAGREEN": "3CB371",
        "MEDIUMSLATEBLUE": "7B68EE",
        "MEDIUMSPRINGGREEN": "00FA9A",
        "MEDIUMTURQUOISE": "48D1CC",
        "MEDIUMVIOLETRED": "C71585",
        "MIDNIGHTBLUE": "191970",
        "MINTCREAM": "F5FFFA",
        "MISTYROSE": "FFE4E1",
        "MOCCASIN": "FFE4B5",
        "NAVAJOWHITE": "FFDEAD",
        "NAVY": "000080",
        "OLDLACE": "FDF5E6",
        "OLIVE": "808000",
        "OLIVEDRAB": "6B8E23",
        "ORANGE": "FFA500",
        "ORANGERED": "FF4500",
        "ORCHID": "DA70D6",
        "PALEGOLDENROD": "EEE8AA",
        "PALEGREEN": "98FB98",
        "PALETURQUOISE": "AFEEEE",
        "PALEVIOLETRED": "DB7093",
        "PAPAYAWHIP": "FFEFD5",
        "PEACHPUFF": "FFDAB9",
        "PERU": "CD853F",
        "PINK": "FFC0CB",
        "PLUM": "DDA0DD",
        "POWDERBLUE": "B0E0E6",
        "PURPLE": "800080",
        "RED": "FF0000",
        "ROSYBROWN": "BC8F8F",
        "ROYALBLUE": "4169E1",
        "SADDLEBROWN": "8B4513",
        "SALMON": "FA8072",
        "SANDYBROWN": "F4A460",
        "SEAGREEN": "2E8B57",
        "SEASHELL": "FFF5EE",
        "SIENNA": "A0522D",
        "SILVER": "C0C0C0",
        "SKYBLUE": "87CEEB",
        "SLATEBLUE": "6A5ACD",
        "SLATEGRAY": "708090",
        "SLATEGREY": "708090",
        "SNOW": "FFFAFA",
        "SPRINGGREEN": "00FF7F",
        "STEELBLUE": "4682B4",
        "TAN": "D2B48C",
        "TEAL": "008080",
        "THISTLE": "D8BFD8",
        "TOMATO": "FF6347",
        "TURQUOISE": "40E0D0",
        "VIOLET": "EE82EE",
        "WHEAT": "F5DEB3",
        "WHITE": "FFFFFF",
        "WHITESMOKE": "F5F5F5",
        "YELLOW": "FFFF00",
        "YELLOWGREEN": "9ACD32"
    ]
}


extension UIImage {
    func fixRotationImage() -> UIImage? {
        if (self.imageOrientation == UIImage.Orientation.up ) {
            return self
        }
        UIGraphicsBeginImageContext(self.size)
        self.draw(in: CGRect(origin: CGPoint.zero, size: self.size))
        let copy = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return copy
    }
}


extension UIViewController {
    func pop() {
        self.navigationController?.popViewController(animated: true)
    }
}
