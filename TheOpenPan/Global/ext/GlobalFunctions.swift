//
//  GlobalFunctions.swift
//  GangaBoxShopify
//
//  Created by Team Peerbits on 12/31/19.
//  Copyright © 2019 ITMAC051. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField
import CoreData
//import SwiftMessages




func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
    var gradientImage:UIImage?
    UIGraphicsBeginImageContext(gradientLayer.frame.size)
    if let context = UIGraphicsGetCurrentContext() {
        gradientLayer.render(in: context)
        gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
    }
    UIGraphicsEndImageContext()
    return gradientImage
}

func setFont16(arrViews:[UIView],weight:enum_font) {
    setFont(arrViews: arrViews, weight: weight, size: 16)
}

func setFont20(arrViews:[UIView],weight:enum_font) {
    setFont(arrViews: arrViews, weight: weight, size: 20)
}

func setFont(arrViews:[UIView],weight:enum_font,size:CGFloat) {
    for view in arrViews {
        if let lbl = view as? UILabel {
            lbl.font = UIFont.init(name: weight.rawValue, size: size)
        }
        
        if let btn = view as? UIButton {
            btn.titleLabel?.font = UIFont.init(name: weight.rawValue, size: size)
        }
        
        if let txtField = view as? UITextField {
            txtField.font = UIFont.init(name: weight.rawValue, size: size)
        }
        
        if let txtField = view as? SkyFloatingLabelTextField {
            txtField.font = UIFont.init(name: weight.rawValue, size: size)
        }
    }
}


//func showError(strError:String?) {
//    
//    guard let error = strError else {
//        return
//    }
//    
//    let view = MessageView.viewFromNib(layout: .cardView)
//
//    // Theme message elements with the warning style.
////    view.configureTheme(.info)
//    view.configureTheme(.info, iconStyle: .default)
//
//    // Add a drop shadow.
//    view.configureDropShadow()
//
//    // Set message title, body, and icon. Here, we're overriding the default warning
//    // image with an emoji character.
////    let iconText = ["🤔", "😳", "🙄", "😶"].randomElement()!
//    view.configureContent(title: APPNAME, body: error)
////    view.configureContent(title: APPNAME, body: strError, iconText: iconText)
//
//    // Increase the external margin around the card. In general, the effect of this setting
//    // depends on how the given layout is constrained to the layout margins.
//    view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
//
//    // Reduce the corner radius (applicable to layouts featuring rounded corners).
//    (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
//    view.button?.isHidden = true
//    // Show the message.
//    SwiftMessages.show(view: view)
//}

struct Observation {
    static let VolumeKey = "outputVolume"
    static var Context = 0
}
