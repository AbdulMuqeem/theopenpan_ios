//
//  BaseValidationViewController.swift
//  Leazzer
//
//  Created by Manoj MacMini12 on 24/08/16.
//  Copyright © 2016 Manoj MacMini12. All rights reserved.
//

import UIKit

class BaseViewController : UIViewController
{
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return self.style
    }
    var style:UIStatusBarStyle = .default
    
    func changeStyle(_ myStyle : UIStatusBarStyle)
    {
        
        self.style = myStyle
        
        setNeedsStatusBarAppearanceUpdate()
    }
}

class BaseValidationViewController : BaseViewController {
    
    var isApiInCall : Bool = false
    
    func hideKeyboard(){
        self.view.endEditing(true)
    }
    
    func showLoader() {
        isApiInCall = true
        self.view.isUserInteractionEnabled = false
       
//        spinerindi.innerColor = UIColor(red: 145/255, green: 41/255, blue: 56/255, alpha: 1)
//    
//        
//        spinerindi.showLoading()
//        indicator.numSegments = 5
//        indicator.lineWidth = 4
//        indicator.animationDuration = 1.0
//        indicator.rotationDuration = 1.0
//        indicator.strokeColor = UIColor(red: 145/255, green: 41/255, blue: 56/255, alpha: 1)
//        indicator.startAnimating()
//        self.view.addSubview(indicator)


    }
    
    func hideLoader() {
        isApiInCall = false
        self.view.isUserInteractionEnabled = true
        //spinerindi.hideLoading()
//        if indicator.isAnimating {
//            indicator.stopAnimating()
//        } 

        //stopAnimating()
    }
    
    
    
    func locationapi(lat : Double , long : Double)
    {
//        let param : [String : String] =
//            [
//                p_userid : (GetUserDetails()?.userid.description)!,
//                p_latitude : lat.description,
//                p_longitude : long.description,
//                p_trip_id : "17",//tripidstart
//        ]
//        AlamofireModel.GetUpdateLocationtoServer(parameter: param as NSDictionary,URL:Endpoint.setupdateLocation.rawValue, handler: {(response) in
//            //   let data = JSON(response)
//            
//            print("Loc Updated Successfully")
//            
//        })
    }

}


