//
//  AppDelegate+Extension.swift
//

import Foundation
import UserNotifications
import Firebase

var isPushEnabled : Bool = false
var isTokenApiCalling : Bool = false

//MARK:- AppDelegate Remote Notification Manager
extension AppDelegate: UNUserNotificationCenterDelegate
{
    func configureForRemoteNotification(compHandler: @escaping ()->())
    {
        Messaging.messaging().delegate = self
        
        // iOS 10 support
//        if #available(iOS 10.0, *)
//        {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {success, _ in
                    if !success && apiAuthHeader.value.isEmpty {
                        self.callTokenAPI(completion: { (_) in
                            compHandler()
                        })
                    } else {
                        compHandler()
                    }
            })
            
            // For iOS 10 data message (sent via FCM)
//            Messaging.messaging().delegate = self
            
//        }
//        else
//        {
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            UIApplication.shared.registerUserNotificationSettings(settings)
//
//            if  apiAuthHeader.value.isEmpty {
//
//                callTokenAPI(completion: { (_) in
//                }) // call api to get token
//            }
//        }
        
        UIApplication.shared.registerForRemoteNotifications()
        
        

    }
    
    //MARK:- Notification Delegate -
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        // check environment
        let _type : MessagingAPNSTokenType = isDevelopmentProvisioningProfile() ? .sandbox : .prod
        
        // set token to firebase instance
        print(deviceToken)
        Messaging.messaging().setAPNSToken(deviceToken, type: _type)
        if Messaging.messaging().fcmToken != nil
        {
            self.deviceToken = Messaging.messaging().fcmToken!
            if apiAuthHeader.value.isEmpty
            {
                callTokenAPI(completion: { (_) in
                })
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        AppInstance.deviceToken = UUID().uuidString
        if apiAuthHeader.value.isEmpty
        {
            callTokenAPI(completion: { (_) in
            })
        }
    }
    
    func manageAPIToken(isPushEnable : Bool, completion : ((Bool) -> Void)?) {
        
        isPushEnabled = isPushEnable
        
        if isPushEnable {
            
            configureForRemoteNotification {
                completion?(true)
            }
            
        } else {
            
            if apiAuthHeader.value.isEmpty {
                callTokenAPI(completion: { (success) in
                    completion?(success)
                })
            } else {
                completion?(false)
            }
        }
        
    }
    
    func callTokenAPI(completion : @escaping ((Bool) -> Void))
    {
        // call api to get token from server
        // deviceid : Firebase Messaging token
        
        if isTokenApiCalling || !apiAuthHeader.value.isEmpty
        {
            print("Token API Call Duplication")
            return
        }
        
        isTokenApiCalling = true
        print(AppInstance.deviceToken)
        getToken(deviceid: AppInstance.deviceToken) { (response) in
            
            isTokenApiCalling = false
            
            switch response
            {
            case .success(let message):
                print(message)
                completion(true)
            case .failed(let error):
                print(error)
                completion(false)
            default: break
            }
        }
    }
    
    
    
     func isDevelopmentProvisioningProfile() -> Bool {
        #if IOS_SIMULATOR
            return true
        #else
            // there will be no provisioning profile in AppStore Apps
            guard let fileName = Bundle.main.path(forResource: "embedded", ofType: "mobileprovision") else {
                return false
            }
            
            let fileURL = URL(fileURLWithPath: fileName)
            // the documentation says this file is in UTF-8, but that failed
            // on my machine. ASCII encoding worked ¯\_(ツ)_/¯
            guard let data = try? String(contentsOf: fileURL, encoding: .ascii) else {
                return false
            }
            
             let cleared = data.components(separatedBy: .whitespacesAndNewlines).joined()
            
            return cleared.contains("<key>get-task-allow</key><true/>")
        #endif
    }
}

extension AppDelegate : MessagingDelegate
{    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
    {
        print("-------------------- FCM TOKEN --------------------")
        print(fcmToken)
        deviceToken = fcmToken
        PBChat.shared.setDeviceId(deviceId: fcmToken)
        
        if apiAuthHeader.value.isEmpty {
            callTokenAPI(completion: { (_) in
            }) // call api to get token
        }
    }
}

