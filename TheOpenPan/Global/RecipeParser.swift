//
//  RecipeParser.swift
//  TheOpenPan
//
//  Created by Pathan Mushrankhan on 01/05/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation





struct RecipeParser {
    
    struct RecipeParserObject {
        var name            : String?
        var ingredients     : [String]?
        var instructions    : [String]?
        var time            : [String:Any]?
        var servings        : String?
        var ratings         : String?
        var ratingscount    : String?
        var media_url       : String?
        var media_type      : Int?
        
    }
    
    static func getRecipeData(url:String, complhandler:@escaping (AddNewRecipeData?)->()) {
        
        parseScrapperRecipes(url: url) { (res) in
            
            switch res {
            case .success(let data, let message) :
                //                BasicFunctions.showInfo(strError: message)
                var objAddNewRecipeData = AddNewRecipeData.init()
                
                if let recipe_list = data["recipe_list"] as? Dictionary<String,Any> {
                    
                    guard let ingredients = recipe_list["ingredients"] as? [String] else {
                        complhandler(nil)
                        return
                    }
                    guard let instructions = recipe_list["instructions"] as? [String] else {
                        complhandler(nil)
                        return
                    }
                    
                    var direction_list = [Direction_list]()
                    for (i,instruction) in instructions.enumerated() {
                        direction_list.append(Direction_list.init(step: i, description: instruction))
                    }
                    
                    objAddNewRecipeData.direction_list = direction_list
                    
                    objAddNewRecipeData.title = (recipe_list["name"] as? String) ?? ""
                    objAddNewRecipeData.serves = ((recipe_list["servings"] as? String) ?? "0").integer
                    
                    if let time = recipe_list["time"] as? [String:Any] {
                        if let total = time["total"] as? String {
                            if total != "" {
                                if total.mk_digits.count == 2 {
                                    objAddNewRecipeData.ready_time_hours = total.mk_digits.first?.integer ?? 0
                                    objAddNewRecipeData.ready_time_mins = total.mk_digits.last?.integer ?? 0
                                } else {
                                    objAddNewRecipeData.ready_time_mins = total.mk_digits.first?.integer ?? 0
                                }
                            }
                        }
                    }
                    
                    objAddNewRecipeData.aggregateRatingValue = ((recipe_list["ratings"] as? String) ?? "0")
                    objAddNewRecipeData.aggregateRatingCount = ((recipe_list["ratingscount"] as? String) ?? "0")
                    objAddNewRecipeData.media_url = ((recipe_list["media_url"] as? String) ?? "")
                    objAddNewRecipeData.is_video = (((recipe_list["media_type"] as? String) ?? "0").integer == 2).mk_intValue
                    objAddNewRecipeData.web_recipe_url = url
                    
                    
                    //                    var mutArrIngredients = [String]()
                    //                            for ing in arrIngredients {
                    //                                let ingRemovedSymbols = self.replaceHtmlSymbols(strText: ing)
                    //                                let parsedIng = try? SwiftSoup.parse(ingRemovedSymbols)
                    //                                let finalIng = try? parsedIng?.text() ?? ""
                    //                                mutArrIngredients.append(finalIng ?? "")
                    //                            }
                    
                    //Ingredient_list
                    
                    guard let jsonDataArrIngredients = try? JSONSerialization.data(withJSONObject: ingredients,options: []) else {
                        return
                    }
                    
                    guard let textArrIngredienta = String(data: jsonDataArrIngredients, encoding: .utf8) else {
                        return
                    }
                    print("JSON string textArrIngredienta = \(textArrIngredienta)")
                    
                    getIngredientsParsing(ingredients: textArrIngredienta) { (res) in
                        switch res {
                        case .success(dictData: let dictData, message: _) :
                            
                            
                            print(dictData)
                            if let dictIngredientList = dictData["ingredient_list"] as? [[String:Any]] {
                                var ingredient_list = [Ingredient_list]()
                                for dictIngredient in dictIngredientList {
                                    
                                    let name            = dictIngredient["ingredient"] as? String ?? ""
                                    let unit_name       = dictIngredient["unit"] as? String ?? ""
                                    let qty             = dictIngredient["quantity"] as? String ?? ""
                                    
                                    let objIngredient = Ingredient_list.init(id: 0, unit_name: unit_name, name: name, note: "", qty: qty, isSelected: false)
                                    ingredient_list.append(objIngredient)
                                }
                                objAddNewRecipeData.ingredient_list = ingredient_list
                                complhandler(objAddNewRecipeData)
                                return
                            } else {
                                //                                        self.checkVideoAndDownload()
                                //                                BasicFunctions.showError(strError: "Something went wrong!".localize())
                                complhandler(nil)
                                return
                            }
                            
                            //                guard let dictRes = formattedResponseDict else {
                            //                    self.checkVideoAndDownload()
                            //                    return
                            //                }
                            
                            break
                        case .failed(message: let msg):
                            //                                    self.checkVideoAndDownload()
                            complhandler(nil)
                            return
                        //                            BasicFunctions.showError(strError: msg)
                        case .error(error: let error):
                            //                                    self.checkVideoAndDownload()
                            complhandler(nil)
                            return
                            //                            BasicFunctions.showError(strError: error)
                        }
                    }
                    
                } else {
                    complhandler(nil)
                }
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                complhandler(nil)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                complhandler(nil)
                break
            }
            
            apde.hideLoader()
            
        }
        
        
    }
}
