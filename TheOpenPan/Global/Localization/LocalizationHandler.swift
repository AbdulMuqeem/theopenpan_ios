//
//  LocalizationHandler.swift
//  GangaBoxShopify
//
//  Created by Team Peerbits on 1/1/20.
//  Copyright © 2020 ITMAC051. All rights reserved.
//

import Foundation

enum localization_language :String {
    case english = "en"
    case spanish = "es"
}


class LocalizationHandler: NSObject {
    
    static let shared = LocalizationHandler()
    
    
    class func changeLanguage(language:localization_language) {
        
        switch language {
            
        case .english:
            UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
            break
        case .spanish:
            UserDefaults.standard.set(["es"], forKey: "AppleLanguages")
            
            break
        
        }
        UserDefaults.standard.synchronize()
    }
    
    class func getCurrentLanguage() -> localization_language {
        if let currentSelectedLanguage = UserDefaults.standard.stringArray(forKey: "AppleLanguages")?.first {
            switch localization_language.init(rawValue: currentSelectedLanguage) {
            case .english:
                return .english
            case .spanish:
                return .spanish
            case .none:
                return .english
            }
        }
        return localization_language.english
    }
    
}




extension String {
    
    func localize() -> String {
//        if let currentSelectedLanguage = UserDefaults.standard.stringArray(forKey: "AppleLanguages")?.first {
//
//        }
        return NSLocalizedString(self, comment: self)
    }
    
}
