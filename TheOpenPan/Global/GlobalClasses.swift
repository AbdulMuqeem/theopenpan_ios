//
//  GlobalClasses.swift
//  DemoBasics
//
//  Created by Ranger on 27/07/18.
//  Copyright © 2018 Peerbits. All rights reserved.
//

import UIKit
import Foundation

class BackgroundImage: UIImageView {
    override func awakeFromNib() {
        self.backgroundColor = UIColor.clear
        //self.image = UIImage(named: "ic_MainBgImg")
    }
}

class ThemeButton : UIButton {
    
    override func awakeFromNib() {
        self.applyStyle(buttonFont: UIFont.applyRegular(fontSize: 14),
                        textColor: UIColor.black,
                        cornerRadius: 0.0,
                        backgroundColor: UIColor.green,
                        borderColor: nil,
                        borderWidth: nil,
                        state: .normal)
    }
}

class Time: Comparable, Equatable {
    init(_ date: Date) {
        //get the current calender
        let calendar = Calendar.current
        
        //get just the minute and the hour of the day passed to it
        let dateComponents = calendar.dateComponents([.hour, .minute],
                                                     
                                                     from: date)
        
        //calculate the seconds since the beggining of the day for comparisions
        let dateSeconds = dateComponents.hour! * 3600 + dateComponents.minute! * 60
        
        //set the varibles
        secondsSinceBeginningOfDay = dateSeconds
        hour = dateComponents.hour!
        minute = dateComponents.minute!
    }
    
    init(_ hour: Int, _ minute: Int) {
        //calculate the seconds since the beggining of the day for comparisions
        let dateSeconds = hour * 3600 + minute * 60
        
        //set the varibles
        secondsSinceBeginningOfDay = dateSeconds
        self.hour = hour
        self.minute = minute
    }
    
    var hour : Int
    var minute: Int
    
    var date: Date {
        //get the current calender
        let calendar = Calendar.current
        
        //create a new date components.
        var dateComponents = DateComponents()
        
        dateComponents.hour = hour
        dateComponents.minute = minute
        
        return calendar.date(byAdding: dateComponents, to: Date())!
    }
    
    /// the number or seconds since the beggining of the day, this is used for comparisions
    private let secondsSinceBeginningOfDay: Int
    
    //comparisions so you can compare times
    static func == (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay == rhs.secondsSinceBeginningOfDay
    }
    
    static func < (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay < rhs.secondsSinceBeginningOfDay
    }
    
    static func <= (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay <= rhs.secondsSinceBeginningOfDay
    }
    
    
    static func >= (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay >= rhs.secondsSinceBeginningOfDay
    }
    
    
    static func > (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay > rhs.secondsSinceBeginningOfDay
    }
}


//if #available(iOS 11.0, *) {
//    class ColumnFlowLayout: UICollectionViewFlowLayout {
//
//        let cellsPerRow: Int
//        
//        init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
//            self.cellsPerRow = cellsPerRow
//            super.init()
//
//            self.minimumInteritemSpacing = minimumInteritemSpacing
//            self.minimumLineSpacing = minimumLineSpacing
//            self.sectionInset = sectionInset
//        }
//
//        required init?(coder aDecoder: NSCoder) {
//            fatalError("init(coder:) has not been implemented")
//        }
//
//        override func prepare() {
//            super.prepare()
//
//            guard let collectionView = collectionView else { return }
//
//            let marginsAndInsets = sectionInset.left + sectionInset.right + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
//
//            let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
//            itemSize = CGSize(width: itemWidth, height: itemWidth)
//        }
//
//        override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
//            let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
//            context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
//            return context
//        }
//
//    }
//}


class NoBreakSectionCollectionViewLayout: UICollectionViewFlowLayout {

    var cachedItemAttributes = [IndexPath: UICollectionViewLayoutAttributes]()
    var cachedContentSize = CGSize.zero

    override func prepare() {
        super.prepare()
        calculateAttributes()
    }

    override func invalidateLayout() {
        super.invalidateLayout()
        cachedItemAttributes = [:]
        calculateAttributes()
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cachedItemAttributes[indexPath]
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return cachedItemAttributes.filter { rect.intersects($0.value.frame) }.map { $0.value }
    }

    override var collectionViewContentSize: CGSize {
        return cachedContentSize
    }

    func calculateAttributes() {
        var y = CGFloat(0)
        var x = CGFloat(0)
        var lastHeight = CGFloat(0)
        let xSpacing = CGFloat(5)
        let ySpacing = CGFloat(3)
        if let collectionView = collectionView, let datasource = collectionView.dataSource, let sizeDelegate = collectionView.delegate as? UICollectionViewDelegateFlowLayout {
            let sections = datasource.numberOfSections?(in: collectionView) ?? 1
            for section in 0..<sections {
                for item in 0..<datasource.collectionView(collectionView, numberOfItemsInSection: section){
                    let indexPath = IndexPath(item: item, section: section)
                    let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                    if let size = sizeDelegate.collectionView?(collectionView, layout: self, sizeForItemAt: indexPath) {
                        if x > 0 && (x + size.width + xSpacing) > collectionView.bounds.width {
                            y += size.height + ySpacing
                            x = CGFloat(0)
                        }
                        attributes.frame = CGRect(x: x, y: y, width: size.width, height: size.height)
                        lastHeight = size.height
                        x += size.width + xSpacing
                    }
                    cachedItemAttributes[indexPath] = attributes
                }
            }
            cachedContentSize = CGSize(width: collectionView.bounds.width, height: y + lastHeight)
        }

    }
}

extension UIViewController {
    func changeTabBar(hidden:Bool, animated: Bool,completion:(()->())?) {
        guard let tabBar = self.tabBarController?.tabBar else {
            completion?()
            return
        }
        if tabBar.isHidden == hidden { return }
        let frame = tabBar.frame
        let offset = hidden ? frame.size.height : -frame.size.height
        let duration:TimeInterval = (animated ? 0.5 : 0.0)
        tabBar.isHidden = false

        if animated {
            UIView.animate(withDuration: duration, animations: {
                tabBar.frame = frame.offsetBy(dx: 0, dy: offset)
            }, completion: { (true) in
                completion?()
            })
        } else {
            tabBar.isHidden = hidden
            completion?()
        }
        
        
    }
    
    
}


