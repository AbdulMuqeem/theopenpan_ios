//
//  GlobalExtensions.swift
//  DemoBasics
//
//  Created by Ranger on 27/07/18.
//  Copyright © 2018 Peerbits. All rights reserved.
//

import Foundation
import UIKit

//let APPLE_LANGUAGE_KEY = "AppleLanguages"

extension UIColor {
    
    class func colorFromHex(hex: Int) -> UIColor {
        return UIColor(red: (CGFloat((hex & 0xFF0000) >> 16)) / 255.0, green: (CGFloat((hex & 0xFF00) >> 8)) / 255.0, blue: (CGFloat(hex & 0xFF)) / 255.0, alpha: 1.0)
    }
}

extension UIFont {
    
    class func applyRegular(fontSize : CGFloat) -> UIFont
    {
       // return UIFont.init(name: "NunitoSans-Regular" , size: 18.0)!
        return UIFont.systemFont(ofSize:fontSize)
        
    }
    class func applyBold(fontSize : CGFloat) -> UIFont
    {
      //  return UIFont.init(name: "NunitoSans-Bold" , size: 18.0)!
        return UIFont.systemFont(ofSize:fontSize)
        
    }
    class func applyBoldItalic(fontSize : CGFloat) -> UIFont
    {
        //return UIFont.init(name: "NunitoSans-BoldItalic" , size: 18.0)!
        return UIFont.systemFont(ofSize:fontSize)
        
    }
    class func applyItalic(fontSize : CGFloat) -> UIFont
    {
        //return UIFont.init(name: "NunitoSans-Italic" , size: 18.0)!
        return UIFont.systemFont(ofSize:fontSize)
        
    }
    
    class func applyCustomRegular(fontSize : CGFloat) -> UIFont
    {
        return UIFont.init(name: enum_font.regular.rawValue, size: fontSize)!
    }
    
    class func applyCustomBold(fontSize : CGFloat) -> UIFont
    {
        return UIFont.init(name: enum_font.bold.rawValue, size: fontSize)!
    }
    
    class func applyCustomMedium(fontSize : CGFloat) -> UIFont
    {
        return UIFont.init(name: enum_font.medium.rawValue, size: fontSize)!
    }
    
    class func applyCustomThin(fontSize : CGFloat) -> UIFont
    {
        return UIFont.init(name: enum_font.thin.rawValue, size: fontSize)!
    }
    
  
}

extension UILabel
{
    func applyStyle(labelFont: UIFont? = nil,
                    textColor: UIColor? = nil,
                    cornerRadius: CGFloat? = nil,
                    backgroundColor: UIColor? = nil,
                    borderColor: UIColor? = nil,
                    borderWidth: CGFloat? = 1.5,
                    alignment: NSTextAlignment = .left) {
        if cornerRadius != nil {
            self.layer.cornerRadius = cornerRadius!
        } else {
            self.layer.cornerRadius = 0
        }
        
        if borderColor != nil {
            self.layer.borderColor = borderColor?.cgColor
        } else {
            self.layer.borderColor = UIColor.clear.cgColor
        }
        
        if backgroundColor != nil {
            self.backgroundColor = backgroundColor
        } else {
            self.backgroundColor = UIColor.clear
        }
        
        if borderWidth != nil {
            self.layer.borderWidth = borderWidth!
        } else {
            self.layer.borderWidth = 0
        }
        
        if labelFont != nil {
            self.font = labelFont
            self.setFontDynamically(labelFont!)
        } else {
            self.font = UIFont.applyRegular(fontSize: 14)
            self.setFontDynamically(UIFont.applyRegular(fontSize: 14))
        }
        
        if textColor != nil {
            self.textColor = textColor
        } else {
            self.textColor = UIColor.white
        }
        
        self.textAlignment = alignment
        
        /*if alignment != nil {
         self.textAlignment = alignment
         } else {
         self.textAlignment = .left
         }*/
    }
    
    func setAttributedString(_ arrStr : [String] , attributes : [[NSAttributedString.Key : Any]]) {
        let str = self.text!
        let attributedString = NSMutableAttributedString(string: str, attributes: [NSAttributedString.Key.font: self.font!])
        for index in 0...arrStr.count - 1 {
            let attr = attributes[index]
            attributedString.addAttributes(attr, range: (str as NSString).range(of: arrStr[index]))
        }
        self.attributedText = attributedString
    }
    
    func setFontDynamically(_ fontToSet : UIFont) {
        if #available(iOS 11.0, *) {
            self.font = UIFontMetrics.default.scaledFont(for: fontToSet)
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 10.0, *) {
            self.adjustsFontForContentSizeCategory = true
        } else {
            // Fallback on earlier versions
        }
    }
}

extension UITextField
{
    open override func awakeFromNib()
    {

        self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: self.placeHolderColor ?? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),NSAttributedString.Key.font:self.font as Any])
       
//        self.font = UIFont.applyRegular(fontSize: 18)

        let customFont = self.font!
        if #available(iOS 11.0, *) {
            self.font = UIFontMetrics.default.scaledFont(for: customFont)
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 10.0, *) {
            self.adjustsFontForContentSizeCategory = true
        } else {
            // Fallback on earlier versions
        }
    }
}

extension UIButton
{
    func applyStyle(buttonFont: UIFont? = nil,
                    textColor: UIColor? = nil,
                    cornerRadius: CGFloat? = nil,
                    backgroundColor: UIColor? = nil,
                    borderColor: UIColor? = nil,
                    borderWidth: CGFloat? = 1.5,
                    state: UIControl.State = .normal) {
        
        if cornerRadius != nil {
            self.layer.cornerRadius = cornerRadius!
        } else {
            self.layer.cornerRadius = 0
        }
        
        if borderColor != nil {
            self.layer.borderColor = borderColor?.cgColor
        } else {
            self.layer.borderColor = UIColor.clear.cgColor
        }
        
        if backgroundColor != nil {
            self.backgroundColor = backgroundColor
        } else {
            self.backgroundColor = UIColor.clear
        }
        
        if borderWidth != nil {
            self.layer.borderWidth = borderWidth!
        } else {
            self.layer.borderWidth = 0
        }
        
        if buttonFont != nil {
            self.titleLabel?.font =  buttonFont!
            self.titleLabel?.setFontDynamically(buttonFont!)
        } else {
            self.titleLabel?.font = UIFont.applyRegular(fontSize: 14)
            self.titleLabel?.setFontDynamically(UIFont.applyRegular(fontSize: 14))
        }
        
        if textColor != nil {
            self.setTitleColor(textColor, for: state)
        } else {
            self.setTitleColor(UIColor.white, for: state)
        }
        
    }
}

extension UINavigationController
{
    open override func awakeFromNib()
    {
        if #available(iOS 11.0, *) {
            //            self.navigationBar.prefersLargeTitles = true
            //            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.interactivePopGestureRecognizer?.delegate = nil
        //
        //        if #available(iOS 11.0, *) {
        //            UINavigationBar.appearance().largeTitleTextAttributes =
        //                [NSAttributedStringKey.foregroundColor: kColorBlack,
        //                 NSAttributedStringKey.font: UIFont.applyBold(fontSize: 34)]
        //        } else {
        //            // Fallback on earlier versions
        //        }
        
    }
    
    /**
     - PopViewController to Specific Viewcontroller in Hierarchy.
     - It will return blank if View controller not available in Heirarchy
     */
    func popToViewController<T: UIViewController>(withType type: T.Type) -> Bool
    {
        var isPoped: Bool = false
        for viewController in self.viewControllers
        {
            if viewController is T
            {
                self.popToViewController(viewController, animated: true)
                isPoped = true
                break
            }
        }
        return isPoped
    }

}

extension UINavigationBar {
    
    open override func awakeFromNib() {
        /*self.barTintColor = UIColor.white
         self.backgroundColor = UIColor.white
         self.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
         //        self.shadowImage = UIImage()
         self.isOpaque = true
         self.tintColor = UIColor.black
         self.titleTextAttributes = [ NSAttributedString.Key.font : UIFont.applyRegular(fontSize: 17) , NSAttributedString.Key.foregroundColor : UIColor(red: 38.0/255.0, green: 38.0/255.0, blue: 38.0/255.0, alpha: 1)]
         //        if #available(iOS 11.0, *) {
         //            UINavigationBar.appearance().largeTitleTextAttributes =
         //                [NSAttributedStringKey.foregroundColor: kColorBlack,
         //                 NSAttributedStringKey.font: UIFont.applyBold(fontSize: 34)]
         //        } else {
         //            // Fallback on earlier versions
         //        }
         
         // code for adjust shadow of inbuilt navigationbar
         
         //        self.layer.masksToBounds = false
         //        self.layer.shadowColor = UIColor.black.cgColor
         //        self.layer.shadowOpacity = 0.6
         //        self.layer.shadowOffset = CGSize(width: 0, height: 1)
         //        self.layer.shadowRadius = 0
         setBackgroundColor(UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1))
         shouldRemoveShadow(true)*/
        setupNavigation()
    }
    func setupNavigation()
    {
        self.barTintColor = UIColor.white
        //        self.backgroundColor = UIColor(red: 53.0/255.0, green: 95.0/255.0, blue: 81.0/255.1, alpha: 1)
        //        self.setBackgroundImage(UIImage(named: "ic_NavigationHeader"), for: UIBarMetrics.default)
        //        self.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        //        self.shadowImage = UIImage()
        //        self.isOpaque = true
        //        self.tintColor = UIColor.white
        
        let titleTextAttributes = [ NSAttributedString.Key.font :  UIFont.init(name: enum_font.sail_regular.rawValue, size: 24)!, NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)] as [NSAttributedString.Key : Any]
        
        self.titleTextAttributes = titleTextAttributes as [NSAttributedString.Key : Any]
        
        if #available(iOS 11.0, *) {
            self.largeTitleTextAttributes = titleTextAttributes as [NSAttributedString.Key : Any]
        } else {
            // Fallback on earlier versions
        }
        
        //        if #available(iOS 11.0, *) {
        //            UINavigationBar.appearance().largeTitleTextAttributes =
        //                [NSAttributedStringKey.foregroundColor: kColorBlack,
        //                 NSAttributedStringKey.font: UIFont.applyBold(fontSize: 34)]
        //        } else {
        //            // Fallback on earlier versions
        //        }
        
        // code for adjust shadow of inbuilt navigationbar
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.10).cgColor
        self.layer.shadowOpacity = 0.10
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowRadius = 0
//        setBackgroundColor(UIColor(red: 53.0/255.0, green: 95.0/255.0, blue: 181.0/255.0, alpha: 1))
        setBackgroundColor(#colorLiteral(red: 0.631372549, green: 0.7764705882, blue: 0.7254901961, alpha: 1))
//        setBackgroundColor(#colorLiteral(red: 0.631372549, green: 0.7764705882, blue: 0.7254901961, alpha: 1))
        
        shouldRemoveShadow(true)
    }
    
    func shouldRemoveShadow(_ value: Bool)
    {
        

//        if #available(iOS 13.0, *) {
//            let navigationBarAppearence = UINavigationBarAppearance()
//            navigationBarAppearence.shadowColor = .clear
//            scrollEdgeAppearance = navigationBarAppearence
//        } else {
//            // Fallback on earlier versions
//        }
        
        
        
        if value {
            self.setValue(true, forKey: "hidesShadow")
            shadowImage = UIImage()
        } else {
            self.setValue(false, forKey: "hidesShadow")
        }
    }
    
    func setBackgroundColor(_ color: UIColor) {
        self.barTintColor = color
        self.backgroundColor = color
        UIApplication.shared.statusBarView?.backgroundColor = color
    }
        
}

extension UIApplication
{
    var statusBarView: UIView?
    {
//        if responds(to: Selector(("statusBar")))
//        {
//            return value(forKey: "statusBar") as? UIView
//        }
        return nil
    }
}

extension UIBarButtonItem
{
    open override func awakeFromNib()
    {
        self.setTitleTextAttributes([ NSAttributedString.Key.font : UIFont.applyRegular(fontSize: 12) , NSAttributedString.Key.foregroundColor : UIColor(red: 66.0/255.0, green: 137.0/255.0, blue: 221.0/255.0, alpha: 1)], for: UIControl.State.normal)
        self.tintColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        
        
    }
}

@IBDesignable extension UIView
{
    
    /* The color of the shadow. Defaults to opaque black. Colors created
     * from patterns are currently NOT supported. Animatable. */
    
    @IBInspectable var shadowColor: UIColor?
        {
        set {
            layer.shadowColor = newValue!.cgColor
        }
        get {
            if let color = layer.shadowColor {
                return UIColor.init(cgColor: color)
            }
            else {
                return nil
            }
        }
    }
    
    /* The opacity of the shadow. Defaults to 0. Specifying a value outside the
     * [0,1] range will give undefined results. Animatable. */
    @IBInspectable var shadowOpacity: Float
        {
        set {
            layer.shadowOpacity = newValue
        }
        get {
            return layer.shadowOpacity
        }
    }
    
    /* The shadow offset. Defaults to (0, -3). Animatable. */
    @IBInspectable var shadowOffset: CGPoint
        {
        set {
            layer.shadowOffset = CGSize(width: newValue.x, height: newValue.y)
        }
        get {
            return CGPoint(x: layer.shadowOffset.width, y:layer.shadowOffset.height)
        }
    }
    
    /* The blur radius used to create the shadow. Defaults to 3. Animatable. */
    @IBInspectable var shadowRadius: CGFloat
        {
        set {
            layer.shadowRadius = newValue
        }
        get {
            return layer.shadowRadius
        }
    }
    
    @IBInspectable var CornerRadius: CGFloat
        {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBDesignable final class GradientView: UIView
    {
        @IBInspectable var startColor: UIColor = UIColor.clear
        @IBInspectable var endColor: UIColor = UIColor.clear
        
        override func draw(_ rect: CGRect) {
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.frame = CGRect(x: CGFloat(0),
                                    y: CGFloat(0),
                                    width: superview!.frame.size.width,
                                    height: superview!.frame.size.height)
            gradient.colors = [startColor.cgColor, endColor.cgColor]
            gradient.zPosition = -1
            layer.addSublayer(gradient)
        }
    }
    
    @IBInspectable public var borderWidth: CGFloat
        {
        set (borderWidth)
        {
            self.layer.borderWidth = borderWidth
        }
        get
        {
            return self.layer.borderWidth
        }
    }
    @IBInspectable public var borderColor:UIColor?
        {
        set (color) {
            self.layer.borderColor = color?.cgColor
        }
        
        get {
            if let color = self.layer.borderColor
            {
                return UIColor(cgColor: color)
            } else {
                return nil
            }
        }
    }
    
    @IBInspectable var leftBorderWidth: CGFloat {
        
        get {
            return 0.0   // Just to satisfy property
        }
        set {
            let line = UIView(frame: CGRect(x: 0.0, y: 0.0, width: newValue, height: bounds.height))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = UIColor(cgColor: layer.borderColor!)
            line.tag = 110
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[line(==lineWidth)]", options: [], metrics: metrics, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[line]|", options: [], metrics: nil, views: views))
        }
    }
    
    @IBInspectable var topBorderWidth: CGFloat {
        get {
            return 0.0   // Just to satisfy property
        }
        set {
            let line = UIView(frame: CGRect(x: 0.0, y: 0.0, width: bounds.width, height: newValue))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = borderColor
            line.tag = 110
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[line]|", options: [], metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[line(==lineWidth)]", options: [], metrics: metrics, views: views))
        }
    }
    
    @IBInspectable var rightBorderWidth: CGFloat {
        get {
            return 0.0   // Just to satisfy property
        }
        set {
            let line = UIView(frame: CGRect(x: bounds.width, y: 0.0, width: newValue, height: bounds.height))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = borderColor
            line.tag = 110
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "[line(==lineWidth)]|", options: [], metrics: metrics, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[line]|", options: [], metrics: nil, views: views))
        }
    }
    @IBInspectable var bottomBorderWidth: CGFloat {
        get {
            return 0.0   // Just to satisfy property
        }
        set {
            let line = UIView(frame: CGRect(x: 0.0, y: bounds.height, width: bounds.width, height: newValue))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = borderColor
            line.tag = 110
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[line]|", options: [], metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[line(==lineWidth)]|", options: [], metrics: metrics, views: views))
        }
    }
    
}

extension UIView
{
    func showToastWithMessgae(message:String, title:String? = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String)  {
        
    }
    
    func giveShadow()
    {
        self.layer.masksToBounds = false
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.1, height: 0.1)
        self.layer.shadowRadius = 2
        //view.layer.cornerRadius = 5
        self.layer.shadowOpacity = 1.0
        self.layer.shadowPath = shadowPath.cgPath
        
    }
}

/**
 Returns an UIImage with a specified background color.
 - parameter color: The color of the background
 */
extension UIImage {
    func imageWithColor(color: UIColor) -> UIImage?
    {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    func imageWithColorNew(color: UIColor) -> UIImage
    {
        let rect: CGRect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 1, height: 1), false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    func resizedImage(withMinimumSize size: CGSize) -> UIImage? {
        
        guard let imgRef = cgImageWithCorrectOrientation() else {return nil}
        let original_width = CGFloat(imgRef.width)
        let original_height = CGFloat(imgRef.height)
        
        let width_ratio: CGFloat = size.width / original_width
        let height_ratio: CGFloat = size.height / original_height
        
        let scale_ratio: CGFloat = width_ratio > height_ratio ? width_ratio : height_ratio
        
        return drawImage(inBounds: CGRect(x: 0, y: 0, width: CGFloat(round(Double(original_width * scale_ratio))), height: CGFloat(round(Double(original_height * scale_ratio)))))
    }
    func cgImageWithCorrectOrientation() -> CGImage?
    {
        
        if imageOrientation == .down {
            //retaining because caller expects to own the reference
            let cgImage = self.cgImage
            return cgImage
        }
        
        UIGraphicsBeginImageContextWithOptions(size, _: false, _: 0.0)
        
        let context = UIGraphicsGetCurrentContext()
        
        if imageOrientation == .right {
            context?.rotate(by: 90 * .pi / 180)
        } else if imageOrientation == .left {
            context?.rotate(by: -90 * .pi / 180)
        } else if imageOrientation == .up {
            context?.rotate(by: 180 * .pi / 180)
        }
        draw(at: CGPoint(x: 0, y: 0))
        let cgImage = context?.makeImage()
        UIGraphicsEndImageContext()
        return cgImage
    }
    func drawImage(inBounds bounds: CGRect) -> UIImage?
    {
        UIGraphicsBeginImageContextWithOptions(bounds.size, _: false, _: 0.0)
        //let context = UIGraphicsGetCurrentContext()
        draw(in: bounds)
        let resizedImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resizedImage
    }
}

extension UIScrollView
{
    var currentPage: Int
    {
        return Int((self.contentOffset.x + (0.5*self.frame.size.width))/self.frame.width)+1
    }
}

extension String
{
    
    var first: String {
        return String(prefix(1))
    }
    var last: String {
        return String(suffix(1))
    }
    
    var uppercaseFirst: String {
        return first.uppercased() + String(dropFirst())
    }
    
    /**
     :name:    trim
     */
    public func trim() -> String
    {
        return trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    func sizeForWidth(width: CGFloat, font: UIFont) -> CGSize
    {
        let attr = [NSAttributedString.Key.font: font]
        let height = NSString(string: self).boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options:.usesLineFragmentOrigin, attributes: attr, context: nil).height
        return CGSize(width: width, height: ceil(height))
    }
    
    var lastPathComponent: String {
        
        get
        {
            return (self as NSString).lastPathComponent
        }
        
        
    }
    
    var pathExtension: String
    {
        
        get
        {
            
            return (self as NSString).pathExtension
        }
    }
    
    var stringByDeletingLastPathComponent: String {
        
        get
        {
            
            return (self as NSString).deletingLastPathComponent
        }
    }
    
    var stringByDeletingPathExtension: String {
        
        get
        {
            
            return (self as NSString).deletingPathExtension
        }
    }
    
    var pathComponents: [String] {
        get
        {
            return (self as NSString).pathComponents
        }
    }
    
    func stringByAppendingPathComponent(path: String) -> String
    {
        let nsSt = self as NSString
        return nsSt.appendingPathComponent(path)
    }
    
    func stringByAppendingPathExtension(ext: String) -> String?
    {
        let nsSt = self as NSString
        return nsSt.appendingPathExtension(ext)
    }
    
    func containsWhiteSpace() -> Bool {
        
        // check if there's a range for a whitespace
        let range = self.rangeOfCharacter(from: .whitespaces)
        
        // returns false when there's no range for whitespace
        if let _ = range {
            return true
        } else {
            return false
        }
    }
    
    ///Added by Shahabuddin - 12-12-2018
    func trimMobile() -> String
    {
        return self.replacingOccurrences(of: " ", with: "")
    }
    func setFormattedMobile() -> String
    {
        return self.chunk(n: 3)
            .map{ String($0) }.joined(separator: " ")
    }
    func setDefaultVal() -> String
    {
        return self.trim() == "" ? "..." : self
    }
    func removeDefaultVal() -> String
    {
        return self.trim() == "..." ? "" : self
    }
    func setOrderIdVal() -> String
    {
        return self.trim() == "" ? "..." : "VT\(self)"
    }
    func setQuantityVal() -> String
    {
        return self.trim() == "" ? "..." : "Qty: \(self)"
    }
    func setPriceVal() -> String
    {
        return self.trim() == "" ? "..." : "KSh \(self)"
    }
    func setPriceFormattedVal() -> String
    {
        let largeNumber = self
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return self.trim() == "" ? "..." : "\(numberFormatter.string(for: largeNumber) ?? "0")"
        //return self.trim() == "" ? "..." : "KSh \(self)"
    }
    func setDiscountVal() -> String
    {
        return self.trim() == "" ? "..." : "\(self)% Discount"
    }
    func setNoDescriptionVal() -> String
    {
        return self.trim() == "" ? "No Description" : self
    }
    
    func setStrikeText() -> NSAttributedString
    {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
        return attributeString
    }
    func setHtmlText() -> NSAttributedString
    {
        return try! NSAttributedString(data: self.data(using: String.Encoding.unicode) ?? Data(), options: [NSAttributedString.DocumentReadingOptionKey.documentType : NSAttributedString.DocumentType.html], documentAttributes: nil)
    }
    func trimZeroAtStart() -> String
    {
        let scanner = Scanner(string: self)
        let zeros = CharacterSet(charactersIn: "0")
        scanner.scanCharacters(from: zeros, into: nil)
        let result = (NSString(string: self)).substring(from: scanner.scanLocation)
        print("\(self) reduced to \(String(describing: result))")
        
        return result
    }
    var toLocale: Locale
    {
        return Locale(identifier: self)
    }
}
extension Numeric
{
    func currency(numberStyle: NumberFormatter.Style = NumberFormatter.Style.currency, locale: String, groupingSeparator: String? = nil, decimalSeparator: String? = nil) -> String?
    {
        return currency(numberStyle: numberStyle, locale: locale.toLocale, groupingSeparator: groupingSeparator, decimalSeparator: decimalSeparator)
    }
    func currency(numberStyle: NumberFormatter.Style = NumberFormatter.Style.currency, locale: Locale = Locale.current, groupingSeparator: String? = nil, decimalSeparator: String? = nil) -> String?
    {
        if let num = self as? NSNumber {
            let formater = NumberFormatter()
            formater.locale = locale
            formater.numberStyle = numberStyle
            var formatedSting = formater.string(from: num)
            if let separator = groupingSeparator, let localeValue = locale.groupingSeparator {
                formatedSting = formatedSting?.replacingOccurrences(of: localeValue, with: separator)
            }
            if let separator = decimalSeparator, let localeValue = locale.decimalSeparator  {
                formatedSting = formatedSting?.replacingOccurrences(of: localeValue, with: separator)
            }
            return formatedSting
        }
        return nil
    }
}
extension Double
{
    func setPriceFormatted() -> String
    {
        let largeNumber = self
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return self.description.trim() == "" ? "0" : numberFormatter.string(for: largeNumber) ?? "0"
    }
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double
    {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
extension Collection
{
    public func chunk(n: Int) -> [SubSequence] {
        var res: [SubSequence] = []
        var i = startIndex
        var j: Index
        while i != endIndex {
            j = index(i, offsetBy: n, limitedBy: endIndex) ?? endIndex
            res.append(self[i..<j])
            i = j
        }
        return res
    }
}
/// Array Index Extension which is returns Index of object
extension Array where Array.Element: AnyObject
{
    func index(ofElement element: Element) -> Int?
    {
        for (currentIndex, currentElement) in self.enumerated()
        {
            if currentElement === element
            {
                return currentIndex
            }
        }
        return nil
    }
}

extension UISearchBar {
    
    func change(textFont : UIFont?) {
        
        for view : UIView in (self.subviews[0]).subviews {
            
            if let textField = view as? UITextField {
                textField.font = textFont
            }
        }
    }
    
}

extension Date {
    var time: Time {
        return Time(self)
    }
}


extension NSDate {
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(daysToAdd: Int) -> NSDate {
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: NSDate = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(hoursToAdd: Int) -> NSDate {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: NSDate = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
}

/*
 extension AppDelegate {
 
 func getBundle(_ lang : String) -> Bundle {
 let path = Bundle.main.path(forResource: lang, ofType: "lproj")
 let bundle = Bundle(path: path!)
 return bundle!
 }
 
 func changeLanguage(_ lang : String) {
 
 }
 
 /// get current Apple language
 class func currentAppleLanguage() -> String{
 let userdef = UserDefaults.standard
 let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
 let current = langArray.firstObject as! String
 let endIndex = current.startIndex
 let currentWithoutLocale = current.substring(to: current.index(endIndex, offsetBy: 2))
 return currentWithoutLocale
 }
 
 class func currentAppleLanguageFull() -> String{
 let userdef = UserDefaults.standard
 let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
 let current = langArray.firstObject as! String
 return current
 }
 
 /// set @lang to be the first in Applelanguages list
 class func setAppleLAnguageTo(lang: String) {
 let userdef = UserDefaults.standard
 userdef.set([lang,currentAppleLanguage()], forKey: APPLE_LANGUAGE_KEY)
 userdef.synchronize()
 }
 }
 */





extension String {
    var youtubeID: String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"

        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)

        guard let result = regex?.firstMatch(in: self, range: range) else {
            return nil
        }

        return (self as NSString).substring(with: result.range)
    }
}
