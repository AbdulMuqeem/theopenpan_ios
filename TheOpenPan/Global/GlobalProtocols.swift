//
//  GlobalProtocols.swift
//  DemoBasics
//
//  Created by Ranger on 27/07/18.
//  Copyright © 2018 Peerbits. All rights reserved.
//

import Foundation
import UIKit
//--------------------------------------------------------------------------------------
//MARK: - Generic Protocols -

protocol TableDelegateDataSource  : UITableViewDelegate , UITableViewDataSource {}
protocol CollectionViewDelegateDataSource : UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {}
