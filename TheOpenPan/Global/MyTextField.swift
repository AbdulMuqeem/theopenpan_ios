//
//  MyTextField.swift
//  Spooray
//
//  Created by Shahabuddin on 17/01/19.
//  Copyright © 2019 peerbits. All rights reserved.
//

import Foundation
import UIKit

protocol MyTextFieldDelegate
{
    func textFieldDidDelete(_ textField: UITextField)
}
class MyTextField: UITextField
{
    var myDelegate: MyTextFieldDelegate?
    
    override func deleteBackward()
    {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete(self)
    }
}
