//
//  DynamicLinkManager.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 3/12/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import Firebase

func createFirebaseDynamicLink(recipe_id:String,recipe_name:String) {
    
    var components = URLComponents()
    components.scheme = "https"
    components.host = "theopenpan.sg"
    components.path = "/"
    let workerTypeQueryItem = URLQueryItem(name: "recipe_id", value: recipe_id) //1-Worker Profile, 2-Worker Job Detail
    components.queryItems = [workerTypeQueryItem]
    guard let linkParameter = components.url else { return }
    print("I am sharing \(linkParameter.absoluteString.removingPercentEncoding ?? "")")
    //create the big dynamic link
    guard let shareLink = DynamicLinkComponents(link: linkParameter, domainURIPrefix: "https://theopenpan.page.link") else {
        print("Couldn't create FDL components")
        return
    }
    if let myBundleId = Bundle.main.bundleIdentifier {
        shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: myBundleId)
    }
    shareLink.iOSParameters?.fallbackURL = URL(string: "https://apps.apple.com/sg/app/the-open-pan-recipes-groceries/id1512341911?ls=1")
    shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: "com.google.android.apps.photos")
    guard let longURL = shareLink.url else { return }
    print("The long dynamic link is \(longURL.absoluteString.removingPercentEncoding ?? "")")
    
    shareLink.shorten { (url, warnings, error) in
        if let error = error {
            print("Oh no! Got an error! \(error)")
            return
        }
        if let warnings = warnings {
            _ = warnings.compactMap({ (warning) -> Void in
                print("FDL Warning: \(warning)")
            })
        }
        guard let url = url else { return }
        print("I have a short URL to share! \(url.absoluteString)")
        showShareSheet(url: url,recipe_name:recipe_name)
    }
    
}
func showShareSheet(url: URL,recipe_name:String) {
    let promoText = "I would like you to check out this recipe \(recipe_name) I found on The Open Pan app."
    //"Checkout this great profile of \(dataWorker.first_name ?? "") \(dataWorker.second_name ?? ""), I found on my home."
    let activityVC = UIActivityViewController(activityItems: [promoText, url], applicationActivities: nil)
    UIViewController.CVC()?.present(activityVC, animated: true, completion: nil)
}
