//
//  GlobalSetting.swift
//
//
//  Created by Peerbits on 26/12/16.
//  Copyright © 2016 peerbits. All rights reserved.
//


import UIKit
import SwiftyJSON
import SwiftMessages
import WebKit
import Haptica

//---------//------------//-------------//



func saveDefault(withObject: AnyObject ,forkey:String)
{
    let deft = UserDefaults.standard
    deft.set(withObject, forKey: forkey)
    deft.synchronize()
}

func getDefault(forkey:String) -> AnyObject?
{
    let deft = UserDefaults.standard
    return (deft.value(forKey: forkey) as AnyObject?) ?? nil
}


func addMessageLabelOnView(suprView:UIView, withMessage : String)
{
    removeAddedMessageLabel(suprView:suprView)
    
    DispatchQueue.main.async {
        let frm = suprView.bounds
        let label = UILabel(frame: CGRect.zero)
        label.textAlignment = .center
        label.textColor = UIColor.lightGray
        label.font = UIFont.init(name: enum_font.regular.rawValue, size: 16)
        label.text = withMessage
        label.numberOfLines = 2
        label.lineBreakMode = .byWordWrapping
        label.accessibilityIdentifier = "messagelabelidentifier"
        
        
        //label.sizeToFit()
        let sizel =  withMessage.sizeForWidth(width: frm.width - 30, font: label.font) //label.intrinsicContentSize
        label.frame = CGRect.init(x: 15, y: (frm.height - sizel.height)/2, width: frm.width - 30, height: sizel.height)
        suprView.addSubview(label)
        
        if let _scrView = suprView as? UIScrollView {
            if let _ = suprView.viewWithTag(-1) as? UIRefreshControl {return}
            _scrView.isScrollEnabled = false
        }
        
    }
}

func removeAddedMessageLabel(suprView:UIView)
{
    DispatchQueue.main.async {
        for subviw in suprView.subviews
        {
            if subviw.accessibilityIdentifier == "messagelabelidentifier"
            {
                subviw.removeFromSuperview()
                
                if let _scrView = suprView as? UIScrollView {
                    _scrView.isScrollEnabled = true
                }
            }
        }
    }
}


func checkTimeStamp(date: String!) -> Bool
{
    let dateFormatter: DateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.locale = Locale(identifier:"en_US_POSIX")
    let datecomponents = dateFormatter.date(from: date)
    
    let now = Date()
    
    if (datecomponents! >= now) {
        return true
    } else {
        return false
    }
}

func resizeImage(_ image: UIImage,withSize : CGFloat) -> UIImage
{
    let size = image.size
    let targetSize : CGSize = CGSize(width: withSize, height: withSize)
    let widthRatio  = targetSize.width  / image.size.width
    let heightRatio = targetSize.height / image.size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio)
    {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    }
    else
    {
        newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
    
}

func getCurrentCountryCode()->String
{
    let locale = Locale.current
    let code = ((locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?)!
    return code //getCountryCallingCode(countryRegionCode: code)
}


func removeSubviewWith(fromView:UIView, accessibilityIdentifier : String)
{
    DispatchQueue.main.async {
        for subviw in fromView.subviews
        {
            if subviw.accessibilityIdentifier == accessibilityIdentifier
            {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionCrossDissolve, animations: {
                    subviw.transform = CGAffineTransform.init(scaleX: 0.01, y: 0.01)
                }) { (done) in
                    subviw.removeFromSuperview()
                }
                
                
            }
        }
    }
}

func getResponseErrorMessage(err : NSArray)->String
{
    var dStr = ""
    if err.count > 0
    {
        dStr.append(err[0] as! String)
    }
    if err.count > 1
    {
        dStr.append(".\n")
        dStr.append(err[1] as! String)
    }
    if err.count > 2
    {
        dStr.append(".\n")
        dStr.append(err[2] as! String)
    }
    return dStr
}

func getCompareResponseErrorMessage(err : NSArray)->String
{
    var dStr = ""
    if err.count > 0
    {
        dStr.append(err[0] as! String)
    }
    
    return dStr
}

// MARK:- base 64
func encodeToBase64String(image: UIImage?) -> String? {
    return image!.pngData()?.base64EncodedString(options: .lineLength64Characters)
}

func decodeBase64ToImage(strEncodeData: String?) -> UIImage? {
    let data = Data(base64Encoded: strEncodeData ?? "", options: .ignoreUnknownCharacters)
    if let aData = data {
        return UIImage(data: aData)
    }
    return nil
}

func GetCurrentTimeStamp(time: String?) -> String? {
    let df = DateFormatter()
    df.dateFormat = "yyyy-MM-dd HH:mm:ss"
    df.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
    let date: Date? = df.date(from: time ?? "")
    let since1970: TimeInterval? = date?.timeIntervalSince1970
    // January 1st 1970
    let result = Double((since1970 ?? 0.0) * 1000)
    return "\(result)"
}

func GetDateFromTimeStamp(time: String?) -> String? {
    let _interval = TimeInterval(Double(time ?? "") ?? 0.0)
    let date = Date(timeIntervalSince1970: _interval)
    let _formatter = DateFormatter()
    _formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let _date = _formatter.string(from: date)
    return _date
}

func isIPhoneX() -> Bool {
    if UIDevice().userInterfaceIdiom == .phone {
        if UIScreen.main.nativeBounds.height == 2436 {
            return true
        }
    }
    
    return false
}
//Available in PBChatHelper.swift
/*func getDocumentsDirectory() -> URL? {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths.first
}*/

//Available in PBChatHelper.swift
/*func deleteFileAtPath(url : String){
    if url.isEmpty {return}
    
    do {
        try FileManager.default.removeItem(at: URL(string: url)!)
        print("File deleted successfully")
    }catch{
        print("1st attempt to delete is failed")
        do {try FileManager.default.removeItem(atPath: url)}catch{ print("2nd attempt to delete is failed")}
    }
}*/
//
//enum SnackType {
//    case Error
//    case Success
//    case Info
//}
//
//let sharedSnackBar = TTGSnackbar()
//func showMessage(text:String, type : SnackType = .Error, view: UIView? = nil)
//{
//    if view != nil {
//        addMessageLabelOnView(suprView: view!, withMessage: MSG_NO_DATA)
//    }
//
//    if text.lowercased() == "unauthenticated" {
//        showMessage(text: MSG_SOMETHING_WRONG +  "\nplease login again")
//        kCurrentUser.logOut()
//        AppInstance.goToLoginScreenPage(transition: true)
//        return
//    }
//
//    sharedSnackBar.message = text.isEmpty ? MSG_SOMETHING_WRONG : text
//    sharedSnackBar.duration = text.count > 50 ? .long : .middle
//    sharedSnackBar.topMargin = 20
//    sharedSnackBar.animationType = .slideFromTopBackToTop
//    sharedSnackBar.backgroundColor = UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1.0)
//    sharedSnackBar.shouldDismissOnSwipe = true
//    sharedSnackBar.messageTextFont = UIFont.applyRegular(fontSize: 16.0)
//    sharedSnackBar.contentInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
//
//    switch type {
//    case .Error:
//        sharedSnackBar.messageTextColor = appColors.ErrorColor
//        sharedSnackBar.icon = #imageLiteral(resourceName: "ic_error")
//    case .Success:
//        sharedSnackBar.messageTextColor = appColors.SuccessColor
//        sharedSnackBar.icon = #imageLiteral(resourceName: "ic_success")
//    case .Info:
//        sharedSnackBar.messageTextColor = appColors.greylight
//        sharedSnackBar.icon = #imageLiteral(resourceName: "ic_information")
//    }
//
//    sharedSnackBar.show()
//}
//
//func showMessageWithOptions(text:String, opt1 : String, opt2 : String, completion : @escaping ((String) -> Void)) {
//
//    let SnackBar = TTGSnackbar()
//
//    SnackBar.message = text.isEmpty ? MSG_SOMETHING_WRONG : text
//    SnackBar.duration = .long
//    SnackBar.topMargin = 20
//    SnackBar.animationType = .slideFromTopBackToTop
//    SnackBar.backgroundColor = UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1.0)
//    SnackBar.shouldDismissOnSwipe = true
//    SnackBar.messageTextFont = UIFont.applyRegular(fontSize: 16.0)
//    SnackBar.contentInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
//    SnackBar.messageTextColor = UIColor.gray
//
//    // Action 1
//    SnackBar.actionText = opt1
//    //let maxOPt = opt1.characters.count > opt2.characters.count ? opt1 : opt2
//    //SnackBar.actionMaxWidth = maxOPt.sizeForWidth(width: 150, height: 20, font: UIFont.applyBold(fontSize: 15.0)).width
//    SnackBar.actionTextFont = UIFont.applyBold(fontSize: 15.0)
//    SnackBar.actionTextColor = appColors.orange
//    SnackBar.actionBlock = { (snackbar) in snackbar.dismiss(); completion(opt1) }
//
//    // Action 2
//    SnackBar.secondActionText = opt2
//    SnackBar.secondActionTextFont = UIFont.applyBold(fontSize: 15.0)
//    SnackBar.secondActionTextColor = appColors.orange
//    SnackBar.secondActionBlock = { (snackbar) in snackbar.dismiss(); completion(opt2) }
//
//    SnackBar.show()
//}

//func manageDescriptiveResponse(withData data : DescriptiveResponse) -> Bool {
//    switch data {
//    case .success(let message):
//        showMessage(text: message, type: .Success, view:nil)
//        return true
//    case .failed(let message):
//        showMessage(text: message)
//        return false
//    case .error(let error):
//        showMessage(text: error.localizedDescription)
//        return false
//    }
//}


final class WebCacheCleaner {

    class func clear() {
        URLCache.shared.removeAllCachedResponses()

        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        print("[WebCacheCleaner] All cookies deleted")

        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                print("[WebCacheCleaner] Record \(record) deleted")
            }
        }
    }

}



func hap(style:Haptica.HapticFeedbackStyle) {
    Haptic.impact(style).generate()
}

func hapHeavy() {
    Haptic.impact(.heavy).generate()
}

func hapSoft() {
    if #available(iOS 13.0, *) {
        Haptic.impact(.soft).generate()
    } else {
        Haptic.impact(.medium).generate()
    }
}


func hapError() {
    Haptic.notification(.error).generate()
}

func matches(forRegex regex: String, in text: String) -> [String] {

    do {
        let regex = try NSRegularExpression(pattern: regex)
        let results = regex.matches(in: text,
                                    range: NSRange(text.startIndex..., in: text))
        return results.map {
            String(text[Range($0.range, in: text)!])
        }
    } catch let error {
        print("invalid regex: \(error.localizedDescription)")
        return []
    }
}


func convertFtoC(text: String) -> String {
    
    var newText = text
    let foundMatches = matches(forRegex: "(\\d+) ?°([Ff])", in: text)
    for match in foundMatches {
        print(match)
        if let fahrenhit_string = match.components(separatedBy: CharacterSet.decimalDigits.inverted).first {
            if let intFahrenhit = Int(fahrenhit_string) {
                let celcius = (Double(intFahrenhit) - 32) / 1.8
                newText = text.replacingOccurrences(of: match, with: "\(Int(celcius)) °C")
            }
        }
    }
    
    let foundMatchesWithSpaceAndWithOutRound = matches(forRegex:  "(\\d+) ?([Ff][ ])", in: newText)
    for match in foundMatchesWithSpaceAndWithOutRound {
        print(match)
        if let fahrenhit_string = match.components(separatedBy: CharacterSet.decimalDigits.inverted).first {
            if let intFahrenhit = Int(fahrenhit_string) {
                let celcius = (Double(intFahrenhit) - 32) / 1.8
                newText = newText.replacingOccurrences(of: match, with: "\(Int(celcius)) °C")
            }
        }
    }
    return newText
}

