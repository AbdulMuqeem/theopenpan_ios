//
//  AppDelegate.swift
//  TradeMaster
//
//  Created by Dipen on 08/12/18.
//  Copyright © 2018 peerbits. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import UserNotifications
import FirebaseCore
import Firebase
//import Fabric
//import Crashlytics
//import CoreLocation
import SwiftMessages
import SwiftyJSON
import CoreData
import AWSCore
import AWSS3
import NotificationBannerSwift
import FBSDKCoreKit
import FBSDKLoginKit
//sg.theopenpan

//protocol getCurruntLocation
//{
//    func GetCurruntLocation(Latitude:Double,Longitude:Double)
//}

var AppInstance: AppDelegate!


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    
    var deviceToken: String = UUID().uuidString
    
//    var LATITUDE: Double = 0.0//22.993573
//    var LONGITUDE: Double = 0.0//72.498686
//
//    var locationManager : CLLocationManager = CLLocationManager()
//    var LocationDelegate : getCurruntLocation?
    
    var ObjMasterData : Master_data!
    
    var lastDisplayedBanner : NotificationBanner?
    
    var xBarHeight = CGFloat()
    
    var preSelectedDateForAddToMealPlan : Date?
    
//    var floatingButtonController: FloatingButtonController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        floatingButtonController = FloatingButtonController()
        UserDefaults.standard.set(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
        
        //        do {
        //            sleep(2)
        //        }
        //        Usage - Clearing web cache
        //        WebCacheCleaner.clear()
        
        ApplicationDelegate.shared.application(application,didFinishLaunchingWithOptions: launchOptions)
        
        FirebaseApp.configure()
        
        Google.configure(withClientId: GOOGLE_CLIENT_ID)
        AppInstance = self
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        application.applicationIconBadgeNumber = 0
        
        initailizeIQKeyboard()
        
        self.awsSetup()
        
        
        manageAPIToken(isPushEnable: true) { (s) in
            self.getMasterDataFromServer { (objMasterData) in

            }
        }
        
        if kCurrentUser.user_id > 0 {
            goHomePage(transition: false)
        } else {
            if kCurrentUser.is_guest {
                goHomePage(transition: false)
            } else {
                apde.logoutAPI()
                goToLoginScreenPage(transition: false)
            }
        }
        
        return true
    }
    
    func awsSetup() {
        
        AWSHelper.sharedInstance.configure()
        AWSHelper.sharedInstance.credentialsProvider.getIdentityId().continueWith { (task) -> Any? in
            if (task.error != nil) {
                print("Error: " + task.error!.localizedDescription)
            }
            else
            {
                // the task result will contain the identity id
                //let cognitoId = task.result
            }
            return nil
        }
    }
    
    
    
    
    func openRecipeDetailsVC(recipe_id:Int64, objNavHomeController:UINavigationController?, animated:Bool=true) {
        let objRecipeDetailsVC = apde.getController(vc_name: "RecipeDetailsVC", sb: .RecipeDetailsVC) as! RecipeDetailsVC
        objRecipeDetailsVC.recipe_id = recipe_id
        objRecipeDetailsVC.recipeDeletedOrUpdatedBlock = { (updatedRecipeDetails, isReloadWhole) in
            
        }
        objNavHomeController?.pushViewController(objRecipeDetailsVC, animated: animated)
    }
    
    
    func applicationWillResignActive(_ application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application: UIApplication)
    {
        
        if let recipeDetailsVC = UIViewController.CVC() as? RecipeDetailsVC {
            NotificationCenter.default.removeObserver(recipeDetailsVC.audioSession)
            recipeDetailsVC.player?.stop()
        }
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    func applicationWillEnterForeground(_ application: UIApplication)
    {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if let recipeDetailsVC = UIViewController.CVC() as? RecipeDetailsVC {
            recipeDetailsVC.player?.stop()
        }
    }
    func applicationDidBecomeActive(_ application: UIApplication)
    {
        kCurrentUser.updateScreenLock()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func applicationWillTerminate(_ application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func readNotificationApiCall(notification_id : Int64, compHandler: @escaping ()->()) {
        
        readNotification(notification_id:notification_id) { (res) in
            
            switch res {
            case .success(_, _) :
                compHandler()
                break
            case .failed(_):
                break
            case .error(_):
                break
            }
            
        }
        
    }
    
    
    //MARK: - Push Notification Delegate & Configuration -
    //for displaying notification when app is in foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        //completionHandler()
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        
        let Notification_JSON = JSON(userInfo)
        
        let notType: enum_notification_type = enum_notification_type(rawValue: Notification_JSON["gcm.notification.notification_type"].string ?? "") ?? .none
        
        var dict : Dictionary<String,Any>?
        
        let string = Notification_JSON["gcm.notification.recipe_details"].string ?? ""
        let data = string.data(using: .utf8)!
        do {
            if let jsonDict = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any> {
                print(jsonDict) // use the json here
                dict = jsonDict
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(error)
            return
        }
        
        
        if let tabbarvc = UIViewController.CVC()?.tabBarController {
            
            tabbarvc.selectedIndex = 2
            
            if let navSettings = tabbarvc.viewControllers?[2] as? UINavigationController {
                if navSettings.viewControllers.count > 2 {
                    navSettings.popToRootViewController(animated: false)
                }
                let settingsVC = apde.getController(vc_name: SettingsVC.className, sb: .Settings) as! SettingsVC
                navSettings.pushViewController(settingsVC, animated: false)
                let objMyRecipesVC = apde.getController(vc_name: "MyRecipesVC", sb: .Settings) as! MyRecipesVC
                navSettings.pushViewController(objMyRecipesVC, animated: false)
                if let dict = dict {
                    if let recipe_id = dict["recipe_id"] as? Int64 {
                        checkRecipeIsAvailable(recipe_id: recipe_id) { (isRecipeAvailable,dictData) in
                            if isRecipeAvailable {
                                let objRecipeDetailsVC = apde.getVC(type: RecipeDetailsVC.self, sb: .RecipeDetailsVC)!
                                objRecipeDetailsVC.recipe_id = recipe_id
                                if notType == .admin_recipe_status_rejected {
                                    objRecipeDetailsVC.isDisplayRejectionNotePopup = true
                                }
                                navSettings.pushViewController(objRecipeDetailsVC, animated: false)
                                
                                if let notification_id = dict["notification_id"] as? Int64 {
                                    apde.readNotificationApiCall(notification_id: notification_id, compHandler: {
                                        
                                    })
                                }
                            }
                        }
                    }
                }
            }
        }
        
        
        if isLoggedin() {
            _ = userInfo as NSDictionary
            //self.navigateToView(userInfo: notificationDetails)
        }
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        
        let Notification_JSON = JSON(userInfo)
        
        let notType: enum_notification_type = enum_notification_type(rawValue: Notification_JSON["gcm.notification.notification_type"].string ?? "") ?? .none
        
        var dict : Dictionary<String,Any>?
        
        let string = Notification_JSON["gcm.notification.recipe_details"].string ?? ""
        let data = string.data(using: .utf8)!
        do {
            if let jsonDict = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any> {
                print(jsonDict) // use the json here
                dict = jsonDict
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(error)
            return
        }
        
        if let tabbarvc = UIViewController.CVC()?.tabBarController {
            
            if tabbarvc.selectedIndex == 2 {
                if let navSettings = tabbarvc.viewControllers?[2] as? UINavigationController {
                    for child in navSettings.viewControllers {
                        if let myRecipes = child as? MyRecipesVC {
                            if let dict = dict {
                                
                                var recipe_id : Int64 = 0
                                
                                if let recipe_id_str = dict["recipe_id"] as? String {
                                    recipe_id = Int64.init(recipe_id_str) ?? 0
                                }
                                
                                if let recipe_id_int64 = dict["recipe_id"] as? Int64 {
                                    recipe_id = recipe_id_int64
                                }
                                
                                if recipe_id != 0 {
                                    getRecipeDetails(recipe_id: recipe_id, is_from_cookbook: false.mk_intValue) { (res) in
                                        switch res {
                                        case .success(dictData: let dictData, message: let msg) :
                                            
                                            guard let dictRecipeDetails = JSON(dictData).dictionary?["recipe_details"]?.dictionary else {
                                                return
                                            }
                                            print(dictRecipeDetails)
                                            
                                            var index :Int = 0
                                            for (i,r) in myRecipes.arrMyRecipeList.enumerated() {
                                                if r.id == recipe_id {
                                                    index = i
                                                }
                                            }
                                            myRecipes.arrMyRecipeList[index].recipe_status = dictRecipeDetails["recipe_status"]?.int ?? 0
                                            if let _ = myRecipes.tblView.cellForRow(at: IndexPath.init(row: index, section: 0)) {
                                                myRecipes.tblView?.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                                            }
                                            
                                            break
                                        case .error(error: let err) :
                                            print("err\(err)")
                                            
                                            break
                                        case .failed(message: let msg) :
                                            
                                            print("msg\(msg)")
                                            break
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }
        
        completionHandler([.alert, .badge, .sound])
        
        if UIApplication.shared.applicationState == .active
        {
            /*if isLoggedin()
             {
             let Notification_JSON = JSON(userInfo)
             
             let NotifType: Enum_NotificationType = Enum_NotificationType(rawValue: Notification_JSON["notification_type"].string ?? "") ?? .None
             if NotifType == .OrderStatus
             {
             let is_accepted = Notification_JSON["is_accepted"].string ?? ""
             
             let str = Notification_JSON["data"].string ?? ""
             let data = str.data(using: String.Encoding.utf8, allowLossyConversion: false)!
             
             do {
             let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
             
             let finalJSON = JSON(json)
             
             let objModelData = ModelNotificationRider()
             objModelData.Populate(dictionary: finalJSON)
             
             if is_accepted == "Y"
             {
             goHomePage(transition: true)
             
             let resultVC = Utilities.viewController(name:"RequestAccptedVC", onStoryboard: StoryboardNames.Orders.rawValue) as! RequestAccptedVC
             resultVC.objModelData = objModelData
             ((window?.rootViewController as! MVYSideMenuController).contentViewController as! UINavigationController).present(resultVC, animated: true, completion: nil)
             }
             else
             {
             completionHandler([.alert, .badge, .sound])
             }
             
             }catch let error as NSError {
             print("Failed to load: \(error.localizedDescription)")
             completionHandler([.alert, .badge, .sound])
             }
             }
             else if NotifType == .NoRiderfound
             {
             let notificationDetails = userInfo as NSDictionary
             self.navigateToView(userInfo: notificationDetails)
             }
             else
             {
             completionHandler([.alert, .badge, .sound])
             }
             }*/
        }
        else
        {
            completionHandler([.alert, .badge, .sound])
        }
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])
    {
        print(userInfo)
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        application.applicationIconBadgeNumber = 0
        
        if isLoggedin()
        {
            _ = userInfo as NSDictionary
            //self.navigateToView(userInfo: notificationDetails)
        }
        
        let Notification_JSON = JSON(userInfo)
        
        let notType: enum_notification_type = enum_notification_type(rawValue: Notification_JSON["notification_type"].string ?? "") ?? .none
        
        switch notType {
        case .rating:
            
            if let tabbarvc = UIViewController.CVC()?.tabBarController {
                tabbarvc.selectedIndex = 2
                if let navSettings = tabbarvc.viewControllers?[2] as? UINavigationController {
                    if navSettings.viewControllers.count > 1 {
                        navSettings.popToRootViewController(animated: true)
                        Timer.after(0.4) {
                            let objRecipeDetailsVC = apde.getVC(type: RecipeDetailsVC.self, sb: .RecipeDetailsVC)!
                            objRecipeDetailsVC.recipe_id = 10
                            navSettings.pushViewController(objRecipeDetailsVC, animated: true)
                        }
                    } else {
                        let objRecipeDetailsVC = apde.getVC(type: RecipeDetailsVC.self, sb: .RecipeDetailsVC)!
                        objRecipeDetailsVC.recipe_id = 10
                        navSettings.pushViewController(objRecipeDetailsVC, animated: true)
                    }
                }
            }
            
            
            break
        case .none:
            break
        default:
            break
        }
        
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        self.application(application, didReceiveRemoteNotification: userInfo)
    }
    /*func navigateToView(userInfo : NSDictionary)
     {
     let Notification_JSON = JSON(userInfo)
     
     let NotifType: Enum_NotificationType = Enum_NotificationType(rawValue: Notification_JSON["notification_type"].string ?? "") ?? .None
     
     
     /*if window?.rootViewController != nil
     {
     let childVc = ((window?.rootViewController as! MVYSideMenuController).contentViewController as! UINavigationController).childViewControllers
     
     for vc in childVc
     {
     if vc.isKind(of: PreviewOrderViewController.self)
     {
     ((window?.rootViewController as! MVYSideMenuController).contentViewController as! UINavigationController).popToRootViewController(animated: true)
     }
     }
     }*/
     
     if NotifType == .Promocode
     {
     let resultVC = Utilities.viewController(name:"NotificationViewController", onStoryboard: StoryboardNames.Settings.rawValue) as! NotificationViewController
     ((window?.rootViewController as! MVYSideMenuController).contentViewController as! UINavigationController).pushViewController(resultVC, animated: true)
     }
     else if NotifType == .OrderStatus
     {
     goHomePage(transition: true)
     let is_accepted = Notification_JSON["is_accepted"].string ?? ""
     let message = Notification_JSON["message"].string ?? ""
     //DELEGATE.showMessages(message: message)
     
     let str = Notification_JSON["data"].string ?? ""
     let data = str.data(using: String.Encoding.utf8, allowLossyConversion: false)!
     
     do {
     let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
     
     let finalJSON = JSON(json)
     
     let objModelData = ModelNotificationRider()
     objModelData.Populate(dictionary: finalJSON)
     
     let masterId = finalJSON["master_id"].int ?? 0
     let orderStatus = orderStatusType(rawValue: finalJSON["status"].string ?? "1") ?? .OnGoing
     
     if is_accepted == "Y"
     {
     let resultVC = Utilities.viewController(name:"RequestAccptedVC", onStoryboard: StoryboardNames.Orders.rawValue) as! RequestAccptedVC
     resultVC.objModelData = objModelData
     ((window?.rootViewController as! MVYSideMenuController).contentViewController as! UINavigationController).present(resultVC, animated: true, completion: nil)
     }
     else
     {
     let resultVC = Utilities.viewController(name:"MyOrderDetailViewController", onStoryboard: StoryboardNames.Orders.rawValue) as! MyOrderDetailViewController
     resultVC.master_id = masterId.description
     resultVC.order_status = orderStatus
     ((window?.rootViewController as! MVYSideMenuController).contentViewController as! UINavigationController).pushViewController(resultVC, animated: true)
     }
     
     }catch let error as NSError {
     print("Failed to load: \(error.localizedDescription)")
     }
     }
     else if NotifType == .CancelStatusRider
     {
     goHomePage(transition: true)
     
     let message = Notification_JSON["message2"].string ?? you_have_new_notification
     let orderId = Notification_JSON["order_id"].string ?? "0"
     
     //DELEGATE.showMessages(message: message)
     
     let resultVC = Utilities.viewController(name:"MyOrderDetailViewController", onStoryboard: StoryboardNames.Orders.rawValue) as! MyOrderDetailViewController
     resultVC.master_id = orderId
     resultVC.order_status = .CancelByRider //self.objModelOrder[indexPath.row].order_status
     ((window?.rootViewController as! MVYSideMenuController).contentViewController as! UINavigationController).pushViewController(resultVC, animated: true)
     
     let AlertString = Notification_JSON["message"].string ?? you_have_new_notification
     
     let alert  = UIAlertController(title: "Order Cancel", message: AlertString, preferredStyle: .alert)
     alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
     window?.rootViewController?.present(alert, animated: true, completion: nil)
     }
     else if NotifType == .PaymentStatus
     {
     goHomePage(transition: true)
     
     let orderId = Notification_JSON["order_id"].string ?? ""
     let resultVC = Utilities.viewController(name:"MyOrderDetailViewController", onStoryboard: StoryboardNames.Orders.rawValue) as! MyOrderDetailViewController
     resultVC.master_id = orderId
     resultVC.order_status = .Delivered //self.objModelOrder[indexPath.row].order_status
     ((window?.rootViewController as! MVYSideMenuController).contentViewController as! UINavigationController).pushViewController(resultVC, animated: true)
     }
     else if NotifType == .NoRiderfound
     {
     //goHomePage(transition: true)
     
     if window?.rootViewController != nil
     {
     if window?.rootViewController?.presentedViewController != nil
     {
     if (window?.rootViewController?.presentedViewController?.isKind(of: CancelRequestView.self))!
     {
     goHomePage(transition: true) //window?.rootViewController?.presentedViewController?.removeFromParentViewController()
     }
     }
     }
     
     let AlertString = Notification_JSON["message"].string ?? you_have_new_notification
     
     let alert  = UIAlertController(title: "Notification", message: AlertString, preferredStyle: .alert)
     //alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
     alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
     //self.goHomePage(transition: true)
     }))
     window?.rootViewController?.present(alert, animated: true, completion: nil)
     }
     else
     {
     let AlertString = Notification_JSON["alert"].string ?? you_have_new_notification
     
     let alert  = UIAlertController(title: "Notification", message: AlertString, preferredStyle: .alert)
     alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
     window?.rootViewController?.present(alert, animated: true, completion: nil)
     }
     }*/
    
    //MARK: - Navigation methods -
    func goHomePage(transition: Bool)
    {
        
        let objTabbarViewController = apde.getController(vc_name: "TabbarViewController", sb: .Home) as! TabbarViewController
        
        
        /*let sideMenuNavigationController = Utilities.viewController(name: "sideNavigationViewController", onStoryboard: "SideMenu")
         let homeNavigationController = Utilities.viewController(name: "HomeNav", onStoryboard: "Home")
         
         let options : MVYSideMenuOptions = MVYSideMenuOptions()
         options.bezelWidth = UIScreen.main.bounds.width;
         options.contentViewScale = 1.0 //1.0 to disable scale
         options.contentViewOpacity = 0.5 // 0.0 to disable opacity
         options.animationDuration = 0.3
         options.panFromBezel = false
         
         let sideMenuController = MVYSideMenuController(menuViewController: sideMenuNavigationController, contentViewController: homeNavigationController,options: options)
         let screenSize = UIScreen.main.bounds.size
         sideMenuController?.menuFrame = CGRect(x: 0, y: 0, width: screenSize.width - 100, height: screenSize.height)
         */
        
        
        let transitionOption = transition ? UIView.AnimationOptions.transitionFlipFromLeft : UIView.AnimationOptions.showHideTransitionViews
        gotoViewController(viewController: objTabbarViewController, transition: transitionOption)
        
    }
    func goToLoginScreenPage(transition : Bool)
    {
        let objInitialController = apde.getController(vc_name: "InitialController", sb: .Authentication) as! UINavigationController
        let transitionOption = transition ? UIView.AnimationOptions.transitionFlipFromLeft : UIView.AnimationOptions.transitionFlipFromLeft
        gotoViewController(viewController: objInitialController, transition: transitionOption)
        
    }
    
    func goToSelectCategoriesList(transition : Bool)
    {
        let objNavSelectCategoriesVC = apde.getController(vc_name: "NavSelectCategoriesVC", sb: .Authentication) as! UINavigationController
        AppInstance.gotoViewController(viewController: objNavSelectCategoriesVC, transition: .transitionFlipFromLeft)
    }
    
    
    func gotoViewController(viewController: UIViewController, transition: UIView.AnimationOptions)
    {
        if transition != UIView.AnimationOptions.showHideTransitionViews
        {
            UIView.transition(with: self.window!, duration: 0.5, options: transition, animations: { () -> Void in
                self.window!.rootViewController = viewController
            }, completion: { (finished: Bool) -> Void in
                
            })
        }
        else
        {
            window!.rootViewController = viewController
        }
    }
    
    //MARK: - Keyboard Init methods -
    func initailizeIQKeyboard() {
        //IQKeyboard
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 5
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.toolbarManageBehaviour = IQAutoToolbarManageBehaviour.bySubviews
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
    }
    
    //MARK:- Logout API -
    func logoutAPI()
    {
        logout { (response) in
            switch response {
            case .success(_):
                //                DELEGATE.showMessages(message: message)
                kCurrentUser.logOut()
                self.goToLoginScreenPage(transition: true)
            //                DELEGATE.goToLoginScreenPage(transition: true)
            case .failed(_):
                //                DELEGATE.showMessages(message: message)
                break
            case .error(_):
                break
                //                DELEGATE.showMessages(message: error.localizedDescription)
            }
        }
    }
    
    //MARK: - Activity Indicator -
    func showLoader()
    {
        PbCircleDotLoader.sharedInstance.startAnimation()
    }
    func hideLoader()
    {
        PbCircleDotLoader.sharedInstance.stopAnimation()
    }
    
    //MARK: - Check user loggedin or Not -
    func isLoggedin() -> Bool
    {
        if kCurrentUser.user_id == 0
        {
            return false
        }
        return true
    }
    
    //MARK: - Coming soon Dialog -
    func showComingSoonDailog()
    {
        var dialog = ComingsoonClass()
        dialog = Bundle.main.loadNibNamed("ComingsoonView", owner: self, options: nil)?.first as! ComingsoonClass
        
        dialog.frame = CGRect(x: 0, y: 0, width: ScreenSizes.SCREEN_WIDTH, height: ScreenSizes.SCREEN_HEIGHT)
        
        //dialog.lblMessage.text = msg
        //dialog.btnOk.addTarget(self, action: #selector(alertViewOkAction), for: UIControlEvents.touchUpInside)
        
        //self.view.addSubview(alertView)
        self.window?.addSubview(dialog)
    }
    
    
    func topViewController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
    
    //MARK: - Function of Error Message view -
    func showMessages(message: String)
    {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default) { _ in
            
            alert.removeFromParent()
        })
        self.topViewController()!.present(alert, animated: true){}
    }
    
    //MARK: - Check Network connectivity -
    func isConnectedToNetwork() -> Bool
    {
        return (NetworkReachabilityManager()?.isReachable)!
    }
    
    //MARK: - Error Label for View -
    func showErrorLabelinView(isshow: Bool = true, view: UIView, Message: String)
    {
        for views in view.subviews
        {
            if views.accessibilityIdentifier == "errViewSub"
            {
                views.removeFromSuperview()
                //break
            }
        }
        if isshow
        {
            let lbl = UILabel()
            lbl.frame = CGRect(x: 20, y: view.frame.size.height/2 - 30, width: view.frame.size.width - 40, height: 60)
            print(lbl.frame)
            lbl.font = UIFont(name: "PT Sans", size: 16.0)
            lbl.text = Message
            lbl.textColor = UIColor.gray
            lbl.numberOfLines = 4
            lbl.lineBreakMode = .byTruncatingTail
            lbl.accessibilityIdentifier = "errViewSub"
            lbl.textAlignment = .center
            lbl.minimumScaleFactor = 0.5
            lbl.allowsDefaultTighteningForTruncation = true
            lbl.adjustsFontSizeToFitWidth = true
            view.addSubview(lbl)
            view.bringSubviewToFront(lbl)
        }
        else
        {
            for views in view.subviews
            {
                if views.accessibilityIdentifier == "errViewSub"
                {
                    views.removeFromSuperview()
                    //break
                }
            }
        }
    }
    func showErrorLabelinViewForOrder(isshow: Bool = true, view: UIView, Message: String)
    {
        for views in view.subviews
        {
            if views.accessibilityIdentifier == "errViewSubOrder"
            {
                views.removeFromSuperview()
                //break
            }
        }
        if isshow
        {
            let lbl = UILabel()
            lbl.frame = CGRect(x: 20, y: view.frame.size.height/2 - 30, width: view.frame.size.width - 40, height: 60)
            print(lbl.frame)
            lbl.font = UIFont(name: "PT Sans", size: 16.0)
            lbl.text = Message
            lbl.textColor = UIColor.gray
            lbl.numberOfLines = 4
            lbl.lineBreakMode = .byTruncatingTail
            lbl.accessibilityIdentifier = "errViewSubOrder"
            lbl.textAlignment = .center
            lbl.minimumScaleFactor = 0.5
            lbl.allowsDefaultTighteningForTruncation = true
            lbl.adjustsFontSizeToFitWidth = true
            view.addSubview(lbl)
            
            view.bringSubviewToFront(lbl)
        }
        else
        {
            for views in view.subviews
            {
                if views.accessibilityIdentifier == "errViewSubOrder"
                {
                    views.removeFromSuperview()
                    //break
                }
            }
        }
    }
    
    //MARK: - Share post function -
    /*func shareAppWithDefault(share_string: String , sender: UIView)
     {
     var shareItem: Array = Array<Any>()
     
     shareItem.append(share_string)
     shareItem.append("App Store: \(APPSTORE_USER)\n")
     
     let activityViewController = UIActivityViewController(activityItems: shareItem, applicationActivities: nil)
     
     if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
     {
     //print("Ipad")
     
     activityViewController.modalPresentationStyle = UIModalPresentationStyle.popover
     activityViewController.popoverPresentationController?.sourceView = sender
     
     DELEGATE.window?.rootViewController?.present(activityViewController, animated: true, completion: nil)
     
     activityViewController.completionWithItemsHandler = { activity, success, items, error in
     if success
     {
     //print("shared")
     }
     }
     }
     else
     {
     DELEGATE.window?.rootViewController?.present(activityViewController, animated: true, completion: nil)
     activityViewController.completionWithItemsHandler = { activity, success, items, error in
     if success
     {
     //print("shared")
     }
     }
     }
     }*/
    
}
////MARK: - Location Delegate methods -
//extension AppDelegate: CLLocationManagerDelegate
//{
//    /**
//     - Custom methods
//     */
//    func getLocationPermission()
//    {
//        print("startLocationTracking\n")
//
//        if CLLocationManager.locationServicesEnabled() == false
//        {
//            showMessages(message: DETERMINE_LOCATION_SETTING)
//        }
//        else
//        {
//            let authorizationStatus : CLAuthorizationStatus = CLLocationManager.authorizationStatus()
//            if (authorizationStatus == CLAuthorizationStatus.denied) || (authorizationStatus == CLAuthorizationStatus.restricted)
//            {
//                print("authorizationStatus failed")
//                OpenLocationServicesSettings()
//            }
//            else
//            {
//                locationManager.delegate = self
//                locationManager.desiredAccuracy = kCLLocationAccuracyBest //kCLLocationAccuracyBestForNavigation
//                locationManager.requestWhenInUseAuthorization()//requestAlwaysAuthorization()
//                locationManager.allowsBackgroundLocationUpdates = false
//                //locationManager.requestLocation()
//                locationManager.startUpdatingLocation()
//            }
//        }
//    }
//    /**
//     MARK: - Show location service dialog -
//     */
//    func OpenLocationServicesSettings()
//    {
//        let alertController = UIAlertController(
//            title:  "Location Settings",
//            message: "Please Turn on Location",
//            preferredStyle: .alert)
//
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//        alertController.addAction(cancelAction)
//        //NSURL(string:UIApplicationOpenSettingsURLString)
//        let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
//            if let url = NSURL(string:UIApplication.openSettingsURLString)
//            {
//                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
//            }
//        }
//        alertController.addAction(openAction)
//        AppInstance.window?.rootViewController?.present(alertController, animated: true, completion: nil)
//    }
//
//    /** Delegates methods -
//     - Location Service Delegate methods
//     */
//    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
//    {
//        print(error.localizedDescription)
//    }
//    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
//    {
//        switch status
//        {
//        case .restricted:
//            showMessages(message: DETERMINE_LOCATION_SETTING)
//        case .denied:
//            showMessages(message: DETERMINE_LOCATION_SETTING)
//        case .notDetermined:
//            showMessages(message: DETERMINE_LOCATION_SETTING)
//        default:
//            getLocationPermission()
//            break
//        }
//    }
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
//    {
//        print("AppDelegate Location Service Running\(NSDate())")
//        let locationArray = locations as NSArray
//        let locationObj = locationArray.lastObject as! CLLocation
//        let coord = locationObj.coordinate
//
//        LATITUDE = coord.latitude
//        LONGITUDE = coord.longitude
//
//        print("Lat: \(LATITUDE)")
//        print("Long: \(LONGITUDE)")
//
//        if LATITUDE != 0.0
//        {
//            locationManager.stopUpdatingLocation()
//            LocationDelegate?.GetCurruntLocation(Latitude: LATITUDE, Longitude: LONGITUDE)
//        }
//    }
//    func calculateDistance(userlat: String, userlong: String) -> Double
//    {
//        let driverlat = LATITUDE.description
//        let driverlong = LONGITUDE.description
//
//        if userlat == "" || userlong == "" || driverlat == "" || driverlong == ""
//        {
//            return 0.0
//        }
//        let Location1 = CLLocation(latitude: Double(userlat)!, longitude: Double(userlong)!)
//        let Location2 = CLLocation(latitude: Double(driverlat)!, longitude: Double(driverlong)!)
//
//        let distance = Location1.distance(from: Location2)
//        return distance
//    }
//
//}




extension AppDelegate {
    
    func getController(vc_name:String,sb:enum_storyboard) -> UIViewController {
        return sb.getStoryboard().instantiateViewController(withIdentifier: vc_name)
    }
    
    func getVC<T:UIViewController>(type:T.Type, sb:enum_storyboard) -> T? {
        return sb.getStoryboard().instantiateViewController(withIdentifier: T.className) as? T
    }
    
    func getMasterDataFromServer( compHandler:@escaping (Master_data?)->()) {
        
        getMasterData { (res) in
            
            switch res {
            case .success(objMasterData: let masterData, message: _) :
                
                self.ObjMasterData = masterData
                compHandler(self.ObjMasterData)
                break
            case .error(error: _) :
                compHandler(nil)
                break
            case .failed(message: _) :
                compHandler(nil)
                break
            }
            
        }
    }
    
    
    func presentLoginViewForGuest(errMsg:String) {
        
        let objInitialController = apde.getController(vc_name: "InitialController", sb: .Authentication) as! UINavigationController
        (objInitialController.viewControllers.first as? WelcomeVC)?.isDismissHide = false
        (objInitialController.viewControllers.first as? WelcomeVC)?.strMessage = errMsg
        objInitialController.modalPresentationStyle = .overCurrentContext
        
        if UIViewController.CVC() is UITabBarController {
            UIViewController.CVC()?.present(objInitialController, animated: true, completion: nil)
        } else {
            UIViewController.CVC()?.tabBarController?.present(objInitialController, animated: true, completion: nil)
        }
        
        
    }
}





extension AppDelegate {
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        
        print("I have received a URL through a custom scheme! \(url.absoluteString)")
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            self.handleIncomingDynamicLink(dynamicLink)
            return true
        }
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let incomingURL = userActivity.webpageURL{
            print("Incoming URL is \(incomingURL.absoluteString.removingPercentEncoding ?? "")")
            perform(#selector(handleIncomingURL(incomingURL:)), with: incomingURL, afterDelay: 0.1)
            return true
            //            return handleIncomingURL(incomingURL: incomingURL)
        }
        return false
    }
    @objc func handleIncomingURL(incomingURL: URL) -> Bool {
        let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { (dynamicLink, error) in
            guard error == nil else{
                print("Found an error! \(error!.localizedDescription)")
                return
            }
            if let dynamicLink = dynamicLink{
                self.handleIncomingDynamicLink(dynamicLink)
            }
        }
        if linkHandled {
            return true
        }
        return false
    }
    
    
    func handleIncomingDynamicLink(_ dynamicLink: DynamicLink)  {
        
        guard let url = dynamicLink.url else {
            print("That's weird. My dynamic link object has no url")
            return
        }
        print("Your incoming link parameter is \(url.absoluteString.removingPercentEncoding ?? "")")
        guard (dynamicLink.matchType == .unique) || (dynamicLink.matchType == .default) else {
            //Not a strong enough match. Let's just not do anything.
            print("Not a strong enough match to continue")
            return
        }
        //        https://theopenpan.sg/?recipe_id=5
        if let urlPath = url.absoluteString.removingPercentEncoding {
            
            let strUrlTrimmed = urlPath.replacingOccurrences(of: "https://theopenpan.sg/?", with: "")
            let arrStringParts = strUrlTrimmed.components(separatedBy: "=")
            if arrStringParts.count > 1 {
                if let first = arrStringParts.first {
                    if first == "recipe_id" {
                        
                        guard let recipe_id_str = arrStringParts.last else {
                            return
                        }
                        guard let recipe_id = Int64(recipe_id_str) else {
                            return
                        }
                        guard let tabBarController = UIViewController.CVC()?.tabBarController else {
                            return
                        }
                        
                        self.showLoader()
                        checkRecipeIsAvailable(recipe_id: recipe_id) { (isRecipeAvailable,dictData) in
                            self.hideLoader()
                            if isRecipeAvailable {
                                tabBarController.selectedIndex = 2
                                guard let navHomeController = tabBarController.selectedViewController as? UINavigationController else {
                                    return
                                }
                                if kCurrentUser.user_id > 0 {
                                    self.openRecipeDetailsVC(recipe_id: recipe_id, objNavHomeController: navHomeController,animated:false)
                                } else {
                                    if kCurrentUser.is_guest {
                                        self.openRecipeDetailsVC(recipe_id: recipe_id, objNavHomeController: navHomeController,animated:false)
                                    } else {
                                        //Not Logged in, even if not as guest also
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}


