//
//  CollectionRecipe.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/27/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation

struct CollectionRecipe {
    
    var id                              = Int64()
    var name                            = String()
    var total_no_of_recipes             = Int()
    var image_url                       = String()
    
}

//"id": 2,
//"name": "New Collection",
//"total_no_of_recipes": 0,
//"image_url": ""
