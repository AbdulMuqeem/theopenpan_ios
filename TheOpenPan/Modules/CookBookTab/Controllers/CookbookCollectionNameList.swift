//
//  CookbookCollectionNameList.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/16/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class CookbookCollectionNameList: UIViewController {

    
    //------------------------------------------
    //MARK: - Outlets -
    
    @IBOutlet weak var btnAddNewCollection: UIButton!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewBackAlpha : UIView!
    @IBOutlet weak var viewCenter : UIView!
    @IBOutlet weak var btnOk : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var cntrWidth : NSLayoutConstraint!
    
    var arrCollectionRecipe = [CollectionRecipe]()
    
    typealias Block = (CollectionRecipe?,_ isCancelTapped :Bool)->()
    var compBlock : Block!
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupLayout()
        self.getCollectionListApiCall(start: 0, limit: 100)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.showBackView()
//        self.tblView.layoutIfNeeded()
    }
    

    
    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        if ssw <= 320 {
            self.cntrWidth.constant = 290
        }
        
        self.viewBackAlpha.alpha = 0
        self.btnOk.mk_addTapHandler { (btn) in
            
            hapSoft()
            if let indexPathSelected = self.tblView.indexPathForSelectedRow {
                self.dismiss {
                    self.compBlock(self.arrCollectionRecipe[indexPathSelected.row],false)
                }
            } else {
                self.tblView.shake()
                hapError()
                return
            }
            
        }
        
        self.btnAddNewCollection.mk_addTapHandler { (btn) in
            hapSoft()
            self.dismiss {
                self.compBlock(nil,false)
            }
        }
        
        self.btnCancel.mk_addTapHandler { (btn) in
            hapSoft()
            self.dismiss {
                self.compBlock(nil,true)
            }
        }
        
        self.viewBackAlpha.addTapGesture { (gesture) in
            hapSoft()
            self.dismiss {
                self.compBlock(nil,true)
            }
        }
        
        setupTableView()
    }

    func showBackView() {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 1
        }) { (s) in
            
        }
    }
    
    func dismiss(comp:(()-> Void)?) {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 0
        }) { (s) in
            self.dismiss(animated: true, completion: comp)
        }
    }


}





extension CookbookCollectionNameList : UITableViewDelegate, UITableViewDataSource {

    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
        self.tblView.allowsMultipleSelection = false
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrCollectionRecipe.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellCollectionNameList = tableView.dequeueReusableCell(withIdentifier: "CellCollectionNameList", for: indexPath) as! CellCollectionNameList

        let objCollectionRecipe = self.arrCollectionRecipe[indexPath.row]
        cellCollectionNameList.lblTitle.text = objCollectionRecipe.name
        
        return cellCollectionNameList
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}



//MARK: - API CALLS
extension CookbookCollectionNameList {
    
    func getCollectionListApiCall(start:Int, limit:Int) {
        
        if start == 0 {
            apde.showLoader()
            self.arrCollectionRecipe.removeAll()
        }
        
        getCollectionList(start: 0, limit: 100) { (res) in
            
            switch res {
            case .success(let dictData, let msg):
                
                if let jsonDict = JSON.init(dictData).dictionary {
                    print(jsonDict)
                    if let arr = jsonDict["collection_list"]?.array {
                        for o in arr {
                            let objCollectionRecipe = CollectionRecipe.init(id: o["id"].int64 ?? 0, name: o["name"].stringValue, total_no_of_recipes: o["total_no_of_recipes"].intValue, image_url: o["image_url"].stringValue)
                            self.arrCollectionRecipe.append(objCollectionRecipe)
                            
                        }
                    }
                }
                self.tblView.reloadData()
                
                
                break
            case .failed(let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(let error):
                BasicFunctions.showError(strError: error)
                break
            }
            
            apde.hideLoader()
        }

        
    
    }
}
