//
//  MyCookBookVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/16/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class MyCookBookVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet var collView                  : UICollectionView!
    @IBOutlet var btnAddRecipe              : UIButton!
    @IBOutlet weak var searchTextField      : UITextField!
    
    //------------------------------------------
    //MARK: - Class Variables -
    let margin: CGFloat = 10
    let cellsPerRow = 5 

    var arrCollectionRecipe = [CollectionRecipe]()
    var arrCollectionRecipeSearch = [CollectionRecipe]()
    
    var isFromMealPlan              = false
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        setupCollView()
        
        self.navigationItem.title = "My Cookbook".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        
        self.btnAddRecipe.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.btnAddRecipe.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.btnAddRecipe.layer.shadowOpacity = 1.0
        self.btnAddRecipe.layer.shadowRadius = 10.0
        self.btnAddRecipe.layer.masksToBounds = false
        
        self.btnAddRecipe.mk_addTapHandler { (btn) in
            hapSoft()
            self.changeTabBar(hidden: true, animated: false) {
                let objAddRecipeOptionsVC = apde.getController(vc_name: "AddRecipeOptionsVC", sb: .AddRecipe) as! AddRecipeOptionsVC
                self.navigationController?.pushViewController(objAddRecipeOptionsVC, animated: true)
            }
        }
        
//        self.searchTextField.shouldBeginEditing { () -> Bool in
//            let objRecipeListVC = apde.getController(vc_name: "RecipeListVC", sb: .Home) as! RecipeListVC
//            objRecipeListVC.title = "My Cookbook"
//            objRecipeListVC.isDirectSearch = true
//            objRecipeListVC.isFromCookBook = true
//            self.navigationController?.pushViewController(objRecipeListVC, animated: true)
//            return false
//        }
        
        self.searchTextField.onChange { (text) in
            self.filterSearchResult(text: text)
        }
        

    }
    func filterSearchResult(text:String) {
        self.arrCollectionRecipeSearch = self.arrCollectionRecipe.filter { (objCollectionRecipe) -> Bool in
            objCollectionRecipe.name.lowercased().contains(text.lowercased())
        }
        self.collView.reloadData()
    }
    
 
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupLayout()
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        changeTabBar(hidden: false, animated: false, completion: nil)
        self.getCollectionListApiCall(start: 0, limit: 100)
        
        self.reflectBackBarButton_isFromMealPlan()
    }
    
    func reflectBackBarButton_isFromMealPlan() {
        if self.isFromMealPlan {
            self.addLeftBarButtonItem(img: #imageLiteral(resourceName: "AssetBackButton"), btnTitle: nil) { (barBtn) in
                self.tabBarController?.selectedIndex = 3
            }
        } else {
            self.navigationItem.leftBarButtonItem = nil
        }
    }
    
}


//------------------------------------------
//MARK: - Table View Setup Methods -
extension MyCookBookVC : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func setupCollView() {
        self.collView.delegate = self
        self.collView.dataSource = self
        
        if #available(iOS 11.0, *) {
            self.collView.contentInsetAdjustmentBehavior = .never
        }
        
        
        let layout = RDHCollectionViewGridLayout.init()
        layout.itemSpacing = 10
        layout.lineSpacing = 10
        layout.lineSize = 0
       
        if UIDevice.current.userInterfaceIdiom == .pad {
            layout.lineItemCount = 3
        } else {
            layout.lineItemCount = 2
        }
        
        layout.scrollDirection = .vertical
        layout.sectionsStartOnNewLine = false
        
        self.collView.collectionViewLayout = layout

        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        if searchTextField.isEmpty() {
            return self.arrCollectionRecipe.count
        }
        return self.arrCollectionRecipeSearch.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollViewCategoryManage", for: indexPath)
            
            return cell
        }
        
        let cellCoobookCollectionList = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCoobookCollectionList", for: indexPath) as! CellCoobookCollectionList
        
        if searchTextField.isEmpty() {
            let objCollectionRecipe = self.arrCollectionRecipe[indexPath.row]
            cellCoobookCollectionList.handleCell(objCollectionRecipe, indexPath: indexPath)
        } else {
            let objCollectionRecipe = self.arrCollectionRecipeSearch[indexPath.row]
            cellCoobookCollectionList.handleCell(objCollectionRecipe, indexPath: indexPath)
        }
        
        return cellCoobookCollectionList
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        hapSoft()
        
        if indexPath.section == 0 {
            let objAddCookBookCollectionVC = apde.getController(vc_name: "AddCookBookCollectionVC", sb: .CookBookTab) as! AddCookBookCollectionVC
            self.navigationController?.pushViewController(objAddCookBookCollectionVC, animated: true)
        } else {
            
            let objRecipeListVC = apde.getController(vc_name: "RecipeListVC", sb: .Home) as! RecipeListVC
            
            if searchTextField.isEmpty() {
                let objCollectionRecipe = self.arrCollectionRecipe[indexPath.row]
                objRecipeListVC.title = objCollectionRecipe.name
                objRecipeListVC.collection_id = objCollectionRecipe.id
            } else {
                let objCollectionRecipe = self.arrCollectionRecipeSearch[indexPath.row]
                objRecipeListVC.title = objCollectionRecipe.name
                objRecipeListVC.collection_id = objCollectionRecipe.id
            }
            
            objRecipeListVC.isFromCookBook = true
            self.navigationController?.pushViewController(objRecipeListVC, animated: true)
            
        }
    }
}





//MARK: - API CALLS
extension MyCookBookVC {
    
    func getCollectionListApiCall(start:Int, limit:Int) {
        
        if start == 0 {
            apde.showLoader()
            self.arrCollectionRecipe.removeAll()
        }
        
        getCollectionList(start: 0, limit: 100) { (res) in
            
            switch res {
            case .success(let dictData, let msg):
                
                if let jsonDict = JSON.init(dictData).dictionary {
                    print(jsonDict)
                    if let arr = jsonDict["collection_list"]?.array {
                        for o in arr {
                            let objCollectionRecipe = CollectionRecipe.init(id: o["id"].int64 ?? 0, name: o["name"].stringValue, total_no_of_recipes: o["total_no_of_recipes"].intValue, image_url: o["image_url"].stringValue)
                            self.arrCollectionRecipe.append(objCollectionRecipe)
                            
                        }
                    }
                }
                
                self.arrCollectionRecipeSearch = self.arrCollectionRecipe
                
                if self.searchTextField.isEmpty() == false {
                    self.filterSearchResult(text: self.searchTextField.text ?? "")
                }
                
                self.collView.reloadData()

                break
            case .failed(let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(let error):
                BasicFunctions.showError(strError: error)
                break
            }
            
            apde.hideLoader()
        }

    }
}


//getCollectionsRecipeList
