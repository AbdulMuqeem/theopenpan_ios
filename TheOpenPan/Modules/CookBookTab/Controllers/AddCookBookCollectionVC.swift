//
//  AddCookBookCollectionVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/16/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField

class AddCookBookCollectionVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    
    @IBOutlet weak var txtCollectionName: SkyFloatingLabelTextField!
    @IBOutlet weak var btnAdd: UIButton!
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    typealias Block = (_ isCreatedCollection:Bool)->()
    var compBlock : Block?
    var shouldDisplaySuccessPopup = true
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    
    
    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        self.navigationItem.title = "Add Collection".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        
        self.btnAdd.mk_addTapHandler { (btn) in
            self.addCookbookCollectionApiCall()
        }
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setupLayout()
        
        Timer.after(0.7) {
            self.txtCollectionName.becomeFirstResponder()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
}


//MARK: - API CALLS
extension AddCookBookCollectionVC {
    
    func addCookbookCollectionApiCall() {
        
        if self.txtCollectionName.isEmpty() {
            self.txtCollectionName.becomeFirstResponder()
            self.txtCollectionName.shake()
            hapError()
            return
        }
        hapSoft()
        
        self.btnAdd.loadingIndicator(show: true)
        
        addCollection(collection_name: txtCollectionName.text ?? "") { (res) in
            switch res {
            case .success(message: let msg):
                
                if self.shouldDisplaySuccessPopup {
                    BasicFunctions.showPopupOneButton(title: "Collection Add".localize(), desc: msg,baseVC: self) {
                        hapHeavy()
                        UIViewController.CVC()?.navigationController?.popViewController(animated: true)
                        self.compBlock?(true)
                    }
                } else {
                    hapHeavy()
                    UIViewController.CVC()?.navigationController?.popViewController(animated: true)
                    self.compBlock?(true)
                }
                
                
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error.localizedDescription)
                
                break
            }
            self.btnAdd.loadingIndicator(show: false)
        }
    
    }
}
