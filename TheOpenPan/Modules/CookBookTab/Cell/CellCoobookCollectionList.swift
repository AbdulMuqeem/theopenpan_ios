//
//  CellCoobookCollectionList.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/27/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit

class CellCoobookCollectionList: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle                 : UILabel!
    @IBOutlet weak var lblNoOfRecipes           : UILabel!
    @IBOutlet weak var imgViewRecipeCollection  : UIImageView!
    @IBOutlet weak var viewDarkOnImage          : UIView!
    
    override func awakeFromNib() {
        self.imgViewRecipeCollection?.image = nil
    }
    
    func handleCell(_ collectionRecipe:CollectionRecipe, indexPath:IndexPath) {
        
        self.viewDarkOnImage.isHidden = true
        self.lblTitle.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        self.lblNoOfRecipes.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        
        self.imgViewRecipeCollection.image =  UIImage.init(named: "AssetCollectionPlaceholder")
        
        self.imgViewRecipeCollection.mk_afSetImage(urlString: collectionRecipe.image_url, defaultImage: "AssetCollectionPlaceholder") { (img, boolValue) in
            if img == nil {
                self.lblTitle.textColor = #colorLiteral(red: 0.7450980392, green: 0.6823529412, blue: 0.6549019608, alpha: 1)
                self.lblNoOfRecipes.textColor = #colorLiteral(red: 0.7450980392, green: 0.6823529412, blue: 0.6549019608, alpha: 1)
            }
            self.viewDarkOnImage.isHidden = img == nil
        }
        
        self.lblTitle.text = collectionRecipe.name
        self.lblNoOfRecipes.text = "\(collectionRecipe.total_no_of_recipes) Recipes"
    }
    
}
