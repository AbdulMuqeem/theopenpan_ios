//
//  CellCollectionNameList.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/16/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class CellCollectionNameList: UITableViewCell {

    
    @IBOutlet weak var btnRadio     : UIButton!
    @IBOutlet weak var lblTitle     : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        self.btnRadio.isSelected = selected
        if selected {
            hapSoft()
        }
        
        
    }

}
