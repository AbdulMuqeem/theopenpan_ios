//
//  CellChangePassword.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class CellChangePassword: UITableViewCell {
    
    @IBOutlet weak var txtCurrentPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtConfirmPassword: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    typealias validationCompleted = (()->())
    var blockValidated : validationCompleted!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.txtCurrentPassword.delegate = self
        self.txtPassword.delegate = self
        self.txtConfirmPassword.delegate = self
        
        btnSubmit.mk_addTapHandler { (btn) in
            
            let ValidateForm1: ValidationParam = [.REQ_PASSWORD: (0,self.txtCurrentPassword)]
            
            let ValidateForm2: ValidationParam =
                [
                    .REQ_PASSWORD: (2,self.txtPassword),
                    .VALID_PASSWORD: (3,self.txtPassword),
                    .REQ_CONFIRM_PASS: (4,self.txtConfirmPassword)]
            
            if PBValidation(fieldName: ValidateForm1) && PBValidation(fieldName: ValidateForm2, Compare: (CompareField: self.txtPassword, CompareFieldTo: self.txtConfirmPassword)) {
                
                    self.blockValidated()
                    hapSoft()
            } else {
                hapError()
            }
            
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



extension CellChangePassword : UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let maxLength = 16
        let currentString: NSString = NSString(string: textField.text!)
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtCurrentPassword {
            txtPassword.becomeFirstResponder()
        } else if textField == txtPassword {
            txtConfirmPassword.becomeFirstResponder()
        } else {
            txtConfirmPassword.resignFirstResponder()
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == txtConfirmPassword
        {
            txtConfirmPassword.resignFirstResponder()
        }
    }
    
}
