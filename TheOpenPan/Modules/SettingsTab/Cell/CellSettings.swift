//
//  CellSettings.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

enum LoadingState {
    case notLoading
    case loading
//    case loaded(UIImage)
    case loaded
}

class CellSettings: UITableViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!
    @IBOutlet weak var lblNoOfRecipePublished: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var lblNormalTitle: UILabel!
    @IBOutlet weak var btnSwitch: UIButton!
    
    var loadingState: LoadingState = .notLoading {  // many ways you could do this, you just need one or more "update" mechanisms to start/stop the spinner and set your image
        didSet {
            switch loadingState {
            case .notLoading:
                activityIndicator?.stopAnimating()
            case .loading:
                activityIndicator?.startAnimating()
            case .loaded:
                activityIndicator?.stopAnimating()
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        loadingState = .notLoading
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func handleUserInfoCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath,userData:[String:Any]) {
        
        self.btnSwitch?.isHidden = true
        self.accessoryType = UITableViewCell.AccessoryType.none
        self.btnEdit.isHidden = kCurrentUser.is_social
        
        
        
        self.btnEdit.mk_addTapHandler { (btn) in
            hapSoft()
            let objMyProfileVC = apde.getController(vc_name: MyProfileVC.className, sb: .Settings) as! MyProfileVC
            UIViewController.CVC()?.navigationController?.pushViewController(objMyProfileVC, animated: true)
        }
        
        self.imgViewUser.image = UIImage.init(named: "AssetUserPlaceholder")
        
        if kCurrentUser.is_guest {
            self.lblUsername.text = "Guest User".localize()
            self.lblUserEmail.text = " "
            self.lblNoOfRecipePublished.text = " "
            self.btnEdit.isHidden = true
        }
        
    }
    
    func handleNormalAndLogoutSettingsCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath,userData:[String:Any]) {
        
        self.btnSwitch?.isHidden = true
        self.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        
        self.lblNormalTitle?.alpha = 1.0
        
        switch indexPath.row {
        case 1:
            self.lblNormalTitle.text = "Screen Lock".localize()
            self.btnSwitch?.isHidden = false
            self.accessoryType = UITableViewCell.AccessoryType.none
            
            self.btnSwitch.mk_addTapHandler { (btn) in
                btn.isSelected = !btn.isSelected
                hapSoft()
                
                if kCurrentUser.is_guest {
                    kCurrentUser.is_screen_lock = btn.isSelected
                    kCurrentUser.updateScreenLock()
                    return
                }
                
                self.editProfileApiCall()
            }
            
            break
        case 2:
            self.lblNormalTitle.text = "My Recipes".localize()
            self.lblNormalTitle?.alpha = kCurrentUser.is_guest ? 0.6 : 1.0
            
            break
        case 3:
            self.lblNormalTitle.text = "Change Password".localize()
            self.lblNormalTitle?.alpha = kCurrentUser.is_guest ? 0.6 : 1.0
            break
        case 4:
            self.lblNormalTitle.text = "Terms and Conditions".localize()
            break
        case 5:
            self.lblNormalTitle.text = "Contact Us".localize()
            self.lblNormalTitle?.alpha = kCurrentUser.is_guest ? 0.6 : 1.0
            break
        case 6:
            
            if kCurrentUser.user_id > 0 {
                self.lblNormalTitle.text = "Sign Out".localize()
                self.accessoryType = UITableViewCell.AccessoryType.none
            } else {
                self.lblNormalTitle.text = "Sign In".localize()
                self.accessoryType = UITableViewCell.AccessoryType.none
            }
            
            break
        default:
            print("default")
        }
        
    }
    

    
    

}


extension CellSettings {
    
    func editProfileApiCall() {
        
        self.loadingState = .notLoading
        
        editProfile(username: nil, profile_image: nil, email: nil, is_screen_lock: self.btnSwitch.isSelected) { (res) in
            
            self.loadingState = .loaded
            self.btnSwitch.isSelected = kCurrentUser.is_screen_lock
            
            switch res {
            case .success(user: let user, message: let msg) :
                print(user)
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                break
            }
            
            
        }
    }
}
