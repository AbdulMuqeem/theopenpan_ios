//
//  CellMyProfile.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class CellMyProfile: UITableViewCell {

    @IBOutlet weak var imgViewUserDarkAlphaView: UIView!
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var txtFieldUsername: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFieldEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnChangePicture: UIButton!
    
    typealias validationCompleted = (()->())
    var blockValidated : validationCompleted!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.txtFieldUsername.delegate = self
        self.txtFieldEmail.delegate = self
        
        btnUpdate.mk_addTapHandler { (btn) in
            let ValidateForm: ValidationParam =
                [
                        .REQ_NAME: (0,self.txtFieldUsername),
                        .REQ_EMAIL: (1,self.txtFieldEmail),
                        .VALID_EMAIL: (2,self.txtFieldEmail)
                ]
            
            print(ValidateForm)
            
            if PBValidation(fieldName: ValidateForm) {
                self.blockValidated()
                hapSoft()
                
            } else {
                hapError()
            }
        }
        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.imgViewUserDarkAlphaView.mk_cornerRadious(radious: 60)
            self.imgViewUser.mk_cornerRadious(radious: 60)
        } else {
            self.imgViewUserDarkAlphaView.mk_cornerRadious(radious: 40)
            self.imgViewUser.mk_cornerRadious(radious: 40)
        }
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func handleCell(vc:MyProfileVC) {
        self.imgViewUser.image = UIImage.init(named: "AssetUserPlaceholder")
        self.txtFieldUsername.text = kCurrentUser.username
        self.txtFieldEmail.text = kCurrentUser.email ?? ""
        self.imgViewUser.mk_afSetImage(urlString: kCurrentUser.profile_image, defaultImage: nil, block: nil)
    }

}



extension CellMyProfile : UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtFieldUsername
        {
            let maxLength = 50
            let currentString: NSString = NSString(string: self.txtFieldUsername.text!)
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else if textField == txtFieldEmail
        {
            let maxLength = 100
            let currentString: NSString = NSString(string: self.txtFieldEmail.text!)
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtFieldUsername {
            txtFieldEmail.becomeFirstResponder()
        } else if textField == txtFieldEmail {
            txtFieldEmail.resignFirstResponder()
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == txtFieldEmail
        {
            txtFieldEmail.resignFirstResponder()
        }
    }
    
}
