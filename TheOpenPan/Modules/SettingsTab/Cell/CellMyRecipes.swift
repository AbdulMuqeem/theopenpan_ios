//
//  CellRecipes.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class CellMyRecipes: UITableViewCell {

    @IBOutlet weak var btnAddRecipe: UIButton!
    
    @IBOutlet weak var imgViewRecipe: UIImageView!
    @IBOutlet weak var lblRecipeName: UILabel!
    @IBOutlet weak var lblRecipeStatus: UILabel!
    @IBOutlet weak var viewBack: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        
        
        if viewBack != nil {
            viewBack.mk_cornerRadious(radious: 4)
            let maskLayer = CAShapeLayer.init()
            maskLayer.path = UIBezierPath.init(roundedRect: self.imgViewRecipe.bounds, byRoundingCorners: [.topLeft,.bottomLeft], cornerRadii: CGSize.init(width: 4, height: 4)).cgPath
            imgViewRecipe?.layer.mask = maskLayer
        }
    }

}
