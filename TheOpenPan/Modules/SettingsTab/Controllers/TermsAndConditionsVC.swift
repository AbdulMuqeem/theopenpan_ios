//
//  TermsAndConditionsVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit


class TermsAndConditionsVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    
    @IBOutlet weak var tblView: UITableView!
    
    //------------------------------------------
    //MARK: - Class Variables -
    var progressBar : LinearProgressBar!
    var strTerms = ""
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        changeTabBar(hidden: true, animated: false, completion: nil)
        self.navigationItem.title = "Terms and Conditions".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        
        setupTableView()
        progressBar = LinearProgressBar()
        self.view.addSubview(progressBar)
        progressBar.startAnimating()
        progressBar.progressBarColor = #colorLiteral(red: 0.631372549, green: 0.7764705882, blue: 0.7254901961, alpha: 1)
        
        if let _ = self.navigationController?.viewControllers.first as? TermsAndConditionsVC {
            //presented from Authentication flow
            self.addLeftBarButtonItem(img: UIImage.init(named: "AssetBackButton")!, btnTitle: nil) { (barButton) in
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupLayout()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
}




extension TermsAndConditionsVC : UITableViewDelegate, UITableViewDataSource {

    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellTerms = tableView.dequeueReusableCell(withIdentifier: "CellTerms", for: indexPath)
        cellTerms.layoutIfNeeded()
        let lblTerms = cellTerms.viewWithTag(101) as! UILabel
        
        lblTerms.mk_setHTMLFromString2(htmlText: self.strTerms)
        
        getTerms { (res) in
            
            switch res {
            case .success(let terms, _) :
                
                print(terms)
                self.tblView.beginUpdates()
                self.strTerms = terms
                lblTerms.mk_setHTMLFromString2(htmlText: self.strTerms)
                cellTerms.layoutIfNeeded()
                self.tblView.endUpdates()
                
                break
            case .error(let error) :
                BasicFunctions.showError(strError: error)
                
                break
            case .failed(let message) :
                BasicFunctions.showError(strError: message)
                break
            }
            self.progressBar.stopAnimating()
        }
        
        return cellTerms
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let objTermsAndConditionsVC = apde.getController(vc_name: TermsAndConditionsVC.className, sb: .Settings) as! TermsAndConditionsVC
//        self.navigationController?.pushViewController(objTermsAndConditionsVC, animated: true)
        
    }
    
    
}



