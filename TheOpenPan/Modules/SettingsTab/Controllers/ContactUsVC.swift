//
//  ContactUsVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField

class ContactUsVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    
    @IBOutlet weak var viewBottomTextView: UIView!
    
    @IBOutlet weak var txtSubject: SkyFloatingLabelTextField!
    @IBOutlet weak var txtViewComments: UITextView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.txtViewComments.text = ""
        self.automaticallyAdjustsScrollViewInsets = false
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        self.navigationItem.title = "Contact Us".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        
        changeTabBar(hidden: true, animated: false, completion: nil)
        
        self.btnSubmit.mk_addTapHandler { (btn) in
            
            if self.txtSubject.isEmpty() {
                self.txtSubject.becomeFirstResponder()
                self.txtSubject.shake()
                hapError()
                return
            }
            
            if self.txtViewComments.isEmpty() {
                self.txtViewComments.becomeFirstResponder()
                self.txtViewComments.shake()
                hapError()
                return
            }
            
            
            hapSoft()
            self.contactUsApiCall()
            
        }
        
        self.txtViewComments.delegate = self
        
        
        
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
}


extension ContactUsVC : UITextViewDelegate {
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.viewBottomTextView.backgroundColor = #colorLiteral(red: 0.631372549, green: 0.7764705882, blue: 0.7254901961, alpha: 1)
        if let constraint = (self.viewBottomTextView.constraints.filter{$0.firstAttribute == .width}.first) {
            constraint.constant = 2.0
        }
        self.view.layoutIfNeeded()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.viewBottomTextView.backgroundColor = #colorLiteral(red: 0.8901960784, green: 0.8901960784, blue: 0.8901960784, alpha: 1)
        if let constraint = (self.viewBottomTextView.constraints.filter{$0.firstAttribute == .width}.first) {
            constraint.constant = 1.0
        }
        self.view.layoutIfNeeded()
    }
    
}



//MARK: - API CALLS
extension ContactUsVC {
    
    func contactUsApiCall() {
        
        self.btnSubmit.loadingIndicator(show: true)
        
        contactUs(subject: self.txtSubject.text ?? "", comment: self.txtViewComments.text ?? "") { (res) in
            switch res {
            case .success(message: let msg):
                
                BasicFunctions.showPopupOneButton(title: "Contact Us".localize(), desc: msg, baseVC: self) {
                    hapHeavy()
                    UIViewController.CVC()?.navigationController?.popViewController(animated: true)
                }
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error.localizedDescription)
                break
            }
            self.btnSubmit.loadingIndicator(show: false)
        }
        
        
    
    }
}
