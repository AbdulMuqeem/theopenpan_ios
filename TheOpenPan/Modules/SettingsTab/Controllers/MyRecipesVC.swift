//
//  MyRecipesVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import SwiftyJSON
import DZNEmptyDataSet

struct MyRecipeList {
    var id                              = Int64()
    var name                            = String()
    var media_type                      = Int()
    var media_url                       = String()
    var video_thumb_url                 = String()
    var recipe_status                   = Int()
    var web_url                         = String()
    var view_url                        = String()
//    var recipe_status_name              = String()
}

enum enum_recipe_status : Int {
    case in_review      = 1
    case rejected       = 2
    case approved       = 3
    case unpublished    = 4
    
    
    func getColor() -> UIColor {
        switch self {
        case .in_review :
            return #colorLiteral(red: 1, green: 0.6235294118, blue: 0.2431372549, alpha: 1)
            
        case .rejected :
            return #colorLiteral(red: 1, green: 0.4549019608, blue: 0.4549019608, alpha: 1)
            
        case .approved :
            return #colorLiteral(red: 0.2549019608, green: 0.8078431373, blue: 0.2156862745, alpha: 1)
            
        case .unpublished :
            return #colorLiteral(red: 0.2156862745, green: 0.6039215686, blue: 0.8078431373, alpha: 1)
            
        }
    }
    
    func getName() -> String {
        switch self {
        case .in_review :
            return "In-Review".localize()
            
        case .rejected :
            return "Rejected".localize()
            
        case .approved :
            return "Approved".localize()
            
        case .unpublished :
            return "Unpublished".localize()
            
        }
    }
    
}


class MyRecipesVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var viewBottomLoader: UIView!
    @IBOutlet weak var bottomLoader: UIActivityIndicatorView!
    //------------------------------------------
    //MARK: - Class Variables -
    
    var arrMyRecipeList = [MyRecipeList]()
    var currentSelectedSortOption : enum_sort_my_recipe! = .Alphabetic
    
    var fromIndex = 0
    let batchSize = 20
    var is_last = false
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        self.navigationItem.title = "My Recipes".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        
        changeTabBar(hidden: true, animated: false, completion: nil)
        
        setupTableView()
        
        self.addRightBarButtonItem(img: UIImage.init(named: "AssetSortBarButtonIcon"), btnTitle: nil) { (barButton) in
            
            hapSoft()
            
            let objSortMyRecipesVC = apde.getController(vc_name: "SortMyRecipesVC", sb: .Settings) as! SortMyRecipesVC
            objSortMyRecipesVC.currentSelectedSortOption = self.currentSelectedSortOption
            objSortMyRecipesVC.completionBlock = { sortOption in
                
                self.arrMyRecipeList.removeAll()
                self.tblView.reloadData()
                self.currentSelectedSortOption = sortOption

                Timer.after(0.3) {
                    self.fromIndex = 0
                    self.arrMyRecipeList.removeAll()
                    self.loadMoreItems()
                }
                
            }
            self.tabBarController?.present(objSortMyRecipesVC, animated: true, completion: nil)
        }
        
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupLayout()
        
        self.fromIndex = 0
        self.arrMyRecipeList.removeAll()
        self.loadMoreItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    func loadMoreItems() {
        self.getMyRecipesListApiCall(start: fromIndex, limit: batchSize)
    }
    
    
}



//------------------------------------------
//MARK: - Table View Setup Methods -
extension MyRecipesVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.estimatedRowHeight = 100
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMyRecipeList.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "CellMyRecipesHeader")
        
        if let btnAddRecipe = headerCell?.viewWithTag(101) as? UIButton {
            btnAddRecipe.mk_addTapHandler { (btn) in
                hapSoft()
                
                let objAddRecipeOptionsVC = apde.getController(vc_name: "AddRecipeOptionsVC", sb: .AddRecipe) as! AddRecipeOptionsVC
                self.navigationController?.pushViewController(objAddRecipeOptionsVC, animated: true)
                 
            }
        }
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 43.5
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let objMyRecipeList = self.arrMyRecipeList[indexPath.row]
        if enum_recipe_status.init(rawValue: objMyRecipeList.recipe_status) == .in_review {
            if objMyRecipeList.web_url != "" {
                return 0
            }
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellMyRecipes = tableView.dequeueReusableCell(withIdentifier: "CellMyRecipes", for: indexPath) as! CellMyRecipes
        let objMyRecipeList = self.arrMyRecipeList[indexPath.row]
        
        cellMyRecipes.lblRecipeName.text = objMyRecipeList.name
        
        cellMyRecipes.imgViewRecipe.image = UIImage.init(named: "AssetCollectionPlaceholder")!
        
        if objMyRecipeList.media_type == 1 {
            // Photo
            cellMyRecipes.imgViewRecipe.mk_afSetImage(urlString: objMyRecipeList.media_url, defaultImage: "AssetCollectionPlaceholder") { (img, boolValue) in
                if let img = img {
                    cellMyRecipes.imgViewRecipe.image = img
                }
            }
        } else {
            // Video
            cellMyRecipes.imgViewRecipe.mk_afSetImage(urlString: objMyRecipeList.video_thumb_url, defaultImage: "AssetCollectionPlaceholder") { (img, boolValue) in
                if let img = img {
                    cellMyRecipes.imgViewRecipe.image = img
                }
            }
        }
        if let status = enum_recipe_status.init(rawValue: objMyRecipeList.recipe_status) {
            cellMyRecipes.lblRecipeStatus.text = status.getName()
            cellMyRecipes.lblRecipeStatus.textColor = status.getColor()
        }
        
        if indexPath.row == self.arrMyRecipeList.count - 1 { // last cell
            if self.is_last == false {
                self.loadMoreItems()
            }
        }
        
        return cellMyRecipes
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objRecipeDetailsVC = apde.getController(vc_name: "RecipeDetailsVC", sb: .RecipeDetailsVC) as! RecipeDetailsVC
        objRecipeDetailsVC.recipe_id = self.arrMyRecipeList[indexPath.row].id
        objRecipeDetailsVC.recipeDeletedOrUpdatedBlock = { (updatedRecipeDetails, isReloadWhole) in
            
            apde.showLoader()
            self.fromIndex = 0
            self.arrMyRecipeList.removeAll()
            self.tblView.reloadData()
            self.loadMoreItems()
            
        }
        self.navigationController?.pushViewController(objRecipeDetailsVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.layoutIfNeeded()
    }
    
}


extension MyRecipesVC : DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return "No recipes...".AttributedStringFont(enum_font.medium.font(16), fontColor: #colorLiteral(red: 0.7450980392, green: 0.6823529412, blue: 0.6549019608, alpha: 1))
    }
}



//MARK: - API CALLS
extension MyRecipesVC {
    
    func getMyRecipesListApiCall(start:Int, limit:Int) {
        
//        if fromIndex == 0 {
//            apde.showLoader()
//        }
        self.bottomLoader.startAnimating()
        
        getMyRecipesList(start: start, limit: limit, sort_type: self.currentSelectedSortOption.rawValue) { (res) in
            
            switch res {
            case .success(let dictData, let msg):
                
                var items = self.arrMyRecipeList
                
                if let arrRecipe = dictData["my_recipe_list"] as? [[String:Any]] {
                    print(arrRecipe)
                    for dictRecipe in JSON.init(arrRecipe).arrayValue {
                        let objMyRecipeList = MyRecipeList.init(id: dictRecipe["id"].int64 ?? 0, name: dictRecipe["name"].stringValue, media_type: dictRecipe["media_type"].int ?? 0, media_url: dictRecipe["media_url"].stringValue, video_thumb_url: dictRecipe["video_thumb_url"].stringValue, recipe_status: dictRecipe["recipe_status"].int ?? 0,
                            web_url: dictRecipe["web_url"].stringValue, view_url: dictRecipe["view_url"].stringValue)
                        items.append(objMyRecipeList)
                    }
                }
                
                if self.fromIndex < items.count {
                    self.arrMyRecipeList = items
                    self.fromIndex = items.count
                    self.tblView.reloadData()
                }
                self.bottomLoader.stopAnimating()
                
                if let isLast = (dictData["is_last"] as? Int) {
                    self.is_last = isLast.mk_boolValue
                }
                
                
                break
            case .failed(let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(let error):
                BasicFunctions.showError(strError: error)
                break
            }
            
            apde.hideLoader()
        }

        
    
    }
}
