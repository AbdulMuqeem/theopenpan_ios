//
//  MyProfileVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit

class MyProfileVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet weak var tblView: UITableView!
    
    //------------------------------------------
    //MARK: - Class Variables -
    private lazy var imagePicker = ImagePicker()
    
    var currentSelectedImage : UIImage!
    var pathLocalImage : String!
    var cellMyProfile : CellMyProfile!
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }
    
    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        //        self.navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        setupTableView()
        
        self.navigationItem.title = "My Profile".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        changeTabBar(hidden: true, animated: false, completion: nil)
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupLayout()
        self.setupImagePicker()
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
}


//------------------------------------------
//MARK: - Table View Setup Methods -
extension MyProfileVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellMyProfile = tableView.dequeueReusableCell(withIdentifier: "CellMyProfile", for: indexPath) as! CellMyProfile
        
        self.cellMyProfile = cellMyProfile
        
        cellMyProfile.handleCell(vc: self)
        
        cellMyProfile.blockValidated = {
            print("Validated")
            
            if self.pathLocalImage != nil {
                
                let urlLocalImage = URL.init(fileURLWithPath: self.pathLocalImage)
                apde.showLoader()
                cellMyProfile.btnUpdate.loadingIndicator(show: true)
                AWSHelper.sharedInstance.upload(fileName: "\(Date().mk_ticks).jpg", fileURL: urlLocalImage) { (res) in
                    switch res {
                    case .success(let localURL,let liveURL) :
                        print(localURL)
                        print("----------------")
                        print(liveURL)
                        print(liveURL.path)
                        print("----------------")
                        print(liveURL.absoluteString)
                        print(liveURL.relativeString)
                        
                        deleteFileAtPath(url: localURL.absoluteString)
                        self.editProfileApiCall(username: cellMyProfile.txtFieldUsername.text ?? "", profile_image: liveURL.absoluteString, email: cellMyProfile.txtFieldEmail.text ?? "", cellMyProfile: cellMyProfile)

                        
                        break
                    case .error(let fileURL,let error) :
                        print(fileURL)
                        print("----------------")
                        print(error)
                        break
                    case .progress(let uploadProgressPercent):
                        print(uploadProgressPercent)
                        break

                    }
                }
                
            } else {
                apde.showLoader()
                cellMyProfile.btnUpdate.loadingIndicator(show: true)
                self.editProfileApiCall(username: cellMyProfile.txtFieldUsername.text ?? "", profile_image: nil, email: cellMyProfile.txtFieldEmail.text ?? "", cellMyProfile: cellMyProfile)
            }
            
            
        }
        
        
        
        cellMyProfile.btnChangePicture.mk_addTapHandler { (btn) in
            self.btnChangePictureTapped()
        }
        
        
        self.getMyProfile(cellMyProfile)
        
        return cellMyProfile
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


extension MyProfileVC: ImagePickerDelegate {
    
    func btnChangePictureTapped() {
        
        let alertVC = UIAlertController.init(title: "Please choose".localize(), message: "", preferredStyle: .actionSheet)
        alertVC.addAction(UIAlertAction.init(title: "Photo Library".localize(), style: .default, handler: { (action) in
            self.imagePicker.photoGalleryAsscessRequest()
        }))
        alertVC.addAction(UIAlertAction.init(title: "Camera".localize(), style: .default, handler: { (action) in
            self.imagePicker.cameraAsscessRequest()
        }))
        alertVC.addAction(UIAlertAction.init(title: "Cancel".localize(), style: .cancel, handler: { (action) in
            
        }))
        alertVC.popoverPresentationController?.sourceView = cellMyProfile.imgViewUser
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
    
    func imagePickerDelegate(didSelect mediaUrl: URL, delegatedForm: ImagePicker) {
        
    }
    
    func setupImagePicker() {
        imagePicker.delegate = self
    }
    
    private func presentImagePicker(sourceType: UIImagePickerController.SourceType) {
        imagePicker.present(parent: self, sourceType: sourceType)
    }
    
    func imagePickerDelegate(didSelect image: UIImage, delegatedForm: ImagePicker) {
        
        let compressedImage = image.mk_compressTo(2)
        
        cellMyProfile.imgViewUser.image         = compressedImage
        self.currentSelectedImage               = compressedImage
        
        if let filePath = (compressedImage ?? image).mk_saveToDocuments(filename: String(Date().mk_ticks), folderName: tmpDocFolderName) {
            self.pathLocalImage = filePath
        }
        
        imagePicker.dismiss()
    }
    
    func imagePickerDelegate(didCancel delegatedForm: ImagePicker) { imagePicker.dismiss() }
    
    func imagePickerDelegate(canUseGallery accessIsAllowed: Bool, delegatedForm: ImagePicker) {
        if accessIsAllowed { presentImagePicker(sourceType: .photoLibrary) }
    }
    
    func imagePickerDelegate(canUseCamera accessIsAllowed: Bool, delegatedForm: ImagePicker) {
        // works only on real device (crash on simulator)
        if accessIsAllowed { presentImagePicker(sourceType: .camera) }
    }
    

}






//MARK: - API CALLS
extension MyProfileVC {
    
    func getMyProfile(_ cellMyProfile: CellMyProfile) {
        
        cellMyProfile.btnUpdate.loadingIndicator(show: true)
        getProfile { (res) in
            
            switch res {
            case .success(user: let user, message: let msg) :
                print(user)
                print(msg)
                cellMyProfile.handleCell(vc: self)
                 
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                break
            }
            
            cellMyProfile.btnUpdate.loadingIndicator(show: false)
        }
    }
    
    
    func editProfileApiCall(username: String, profile_image: String?,email: String, cellMyProfile:CellMyProfile) {
        
        editProfile(username: username, profile_image: profile_image, email: email, is_screen_lock: kCurrentUser.is_screen_lock) { (res) in
            switch res {
            case .success(user: let user, message: let msg) :
                print(user)
                
                BasicFunctions.showPopupOneButton(title: "Update Profile".localize(), desc: msg, baseVC: self) {
                    hapHeavy()
                    UIViewController.CVC()?.navigationController?.popViewController(animated: true)
                }
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                break
            }
            
            apde.hideLoader()
            cellMyProfile.btnUpdate.loadingIndicator(show: false)
            
        }
    }
    
}
