//
//  ChangePasswordVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit

class ChangePasswordVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet weak var tblView: UITableView!
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        
//        self.navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        changeTabBar(hidden: true, animated: false, completion: nil)
        self.navigationItem.title = "Change Password".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        
        setupTableView()
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupLayout()
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
}


//------------------------------------------
//MARK: - Table View Setup Methods -
extension ChangePasswordVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }

    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellChangePassword = tableView.dequeueReusableCell(withIdentifier: "CellChangePassword", for: indexPath) as! CellChangePassword
        
        cellChangePassword.blockValidated = {
            print("Validated")
            
            self.changePasswordApiCall(oldPassword: cellChangePassword.txtCurrentPassword.text ?? "", newPassword: cellChangePassword.txtPassword.text ?? "", cellChangePassword: cellChangePassword)
            
        }
        
        
        return cellChangePassword
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}




//MARK: - API CALLS
extension ChangePasswordVC {
    
    func changePasswordApiCall(oldPassword:String, newPassword:String, cellChangePassword:CellChangePassword) {
        
        cellChangePassword.btnSubmit.loadingIndicator(show: true)
        
        changePassword(old_password: oldPassword, new_password: newPassword) { (res) in
            switch res {
            case .success(message: let msg):
                
                BasicFunctions.showPopupOneButton(title: "Change Password".localize(), desc: msg, baseVC: self) {
                    hapHeavy()
                    UIViewController.CVC()?.navigationController?.popViewController(animated: true)
                }
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error.localizedDescription)
                break
            }
            
            cellChangePassword.btnSubmit.loadingIndicator(show: false)

        }
        
    
    }
}
