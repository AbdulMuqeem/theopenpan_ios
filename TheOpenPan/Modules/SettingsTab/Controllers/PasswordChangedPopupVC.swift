//
//  PasswordChangedPopupVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit

class PasswordChangedPopupVC: UIViewController {

    @IBOutlet weak var viewBackAlpha : UIView!
    @IBOutlet weak var viewCenter : UIView!
    
    @IBOutlet weak var btnOk : UIButton!
    
    @IBOutlet weak var cntrWidth : NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupLayout()
    }
    
    func setupLayout() {
        
        if ssw <= 320 {
            self.cntrWidth.constant = 290
        }
        
        self.viewBackAlpha.alpha = 0
        self.btnOk.mk_addTapHandler { (btn) in
            hapSoft()
            self.dismiss()
        }
        
        self.viewBackAlpha.addTapGesture { (gesture) in
            hapSoft()
            self.dismiss()
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.showBackView()
    }

    func showBackView() {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 1
        }) { (s) in
            
        }
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 0
        }) { (s) in
            self.dismiss(animated: true, completion: nil)
        }
    }


}
