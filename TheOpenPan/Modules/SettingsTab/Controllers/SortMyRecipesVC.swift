//
//  SortMyRecipesVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//


enum enum_sort_my_recipe : Int {
    case Status = 1
    case Alphabetic = 2
}

import Foundation

class SortMyRecipesVC: UIViewController {

    @IBOutlet weak var viewBackAlpha : UIView!
    @IBOutlet weak var viewCenter : UIView!
    
    @IBOutlet weak var btnOk : UIButton!
    
    @IBOutlet weak var cntrWidth : NSLayoutConstraint!
    
    @IBOutlet weak var btnAlphabetic : UIButton!
    @IBOutlet weak var btnStatus : UIButton!
    
    var radioButtons: [UIButton]!
    
    typealias CompletionBlock = ((_ selectedSort:enum_sort_my_recipe)->())
    var completionBlock : CompletionBlock!
    
    var currentSelectedSortOption : enum_sort_my_recipe! = .Alphabetic
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupLayout()
    }
    
    func setupLayout() {
        
        radioButtons = [self.btnAlphabetic,self.btnStatus]
        
        if ssw <= 320 {
            self.cntrWidth.constant = 290
        }
        
        self.viewBackAlpha.alpha = 0
        self.btnOk.mk_addTapHandler { (btn) in
            hapHeavy()
            self.dismiss()
            self.completionBlock(self.currentSelectedSortOption)
        }
        
        if self.currentSelectedSortOption == .Alphabetic {
            self.setRadioButtons(button: self.btnAlphabetic)
        } else {
            self.setRadioButtons(button: self.btnStatus)
        }
        
        self.btnAlphabetic.mk_addTapHandler { (btn) in
            hapSoft()
            self.setRadioButtons(button: btn)
            self.currentSelectedSortOption = .Alphabetic
        }
        
        self.btnStatus.mk_addTapHandler { (btn) in
            hapSoft()
            self.setRadioButtons(button: btn)
            self.currentSelectedSortOption = .Status
        }
        
        self.viewBackAlpha.addTapGesture { (gesture) in
            hapHeavy()
            self.dismiss()
            self.completionBlock(self.currentSelectedSortOption)
        }
        
    }
    
    func setRadioButtons(button: UIButton) {
        for getradioButton in radioButtons {
            getradioButton.isSelected = false
        }
        button.isSelected = true
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.showBackView()
        self.btnStatus.layoutIfNeeded()
        self.btnAlphabetic.layoutIfNeeded()
    }

    func showBackView() {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 1
        }) { (s) in
            
        }
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 0
        }) { (s) in
            self.dismiss(animated: true, completion: nil)
        }
    }


}
