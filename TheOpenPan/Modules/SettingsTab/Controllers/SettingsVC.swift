//
//  SettingsVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/16/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit


class SettingsVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet weak var tblView: UITableView!
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    var cellSettingsUserInfo            : CellSettings!
    var cellSettingsScreenLockSwitch    : CellSettings!
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        
//        self.navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        self.navigationItem.title = "Settings".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupTableView()
        self.setupLayout()
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        changeTabBar(hidden: false, animated: false, completion: nil)
        
        
        if kCurrentUser.is_guest == false {
            self.getMyProfile()
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
}



extension SettingsVC : UITableViewDelegate, UITableViewDataSource {

    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
//        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 5 {
//            return kCurrentUser.is_guest ? 0 : UITableView.automaticDimension
//        }
        
        if indexPath.row == 3 {
            return kCurrentUser.is_social ? 0 : UITableView.automaticDimension
        }
        
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
            let cellSettingsUserInfo = tableView.dequeueReusableCell(withIdentifier: "CellSettingsUserInfo", for: indexPath) as! CellSettings
            
            cellSettingsUserInfo.handleUserInfoCell(tableView, cellForRowAt: indexPath, userData: ["":""])
            self.cellSettingsUserInfo = cellSettingsUserInfo
            cellSettingsUserInfo.layoutIfNeeded()
            
            return cellSettingsUserInfo
        }
        
        
        if indexPath.row == 6 {
            
            
            if kCurrentUser.user_id > 0 {
                
                let cellSettingsLogout = tableView.dequeueReusableCell(withIdentifier: "CellSettingsLogout", for: indexPath) as! CellSettings

                cellSettingsLogout.handleNormalAndLogoutSettingsCell(tableView, cellForRowAt: indexPath, userData: ["":""])
                cellSettingsLogout.loadingState = .loaded
                
                cellSettingsLogout.layoutIfNeeded()
                return cellSettingsLogout
                
            } else {
                
                let cellSettingsLoging = tableView.dequeueReusableCell(withIdentifier: "CellSettingsLogin", for: indexPath) as! CellSettings

                cellSettingsLoging.handleNormalAndLogoutSettingsCell(tableView, cellForRowAt: indexPath, userData: ["":""])
                cellSettingsLoging.loadingState = .loaded
                
                cellSettingsLoging.layoutIfNeeded()
                return cellSettingsLoging
                
            }
            
            
            
            
        }
        
        
        let cellSettingsNormal = tableView.dequeueReusableCell(withIdentifier: "CellSettingsNormal", for: indexPath) as! CellSettings
        
        cellSettingsNormal.handleNormalAndLogoutSettingsCell(tableView, cellForRowAt: indexPath, userData: ["":""])
        cellSettingsNormal.loadingState = .loaded
        
        cellSettingsNormal.layoutIfNeeded()
        return cellSettingsNormal
        
        
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 6 {
            hapHeavy()
        } else {
            hapSoft()
        }
        
        switch indexPath.row {
        case 1:
            
            break
        case 2:
            if kCurrentUser.is_guest {
                hapError()
                return
            }
            let objMyRecipesVC = apde.getController(vc_name: "MyRecipesVC", sb: .Settings) as! MyRecipesVC
            self.navigationController?.pushViewController(objMyRecipesVC, animated: true)
            break
        case 3:
            if kCurrentUser.is_guest {
                hapError()
                return
            }
            let objChangePasswordVC = apde.getController(vc_name: "ChangePasswordVC", sb: .Settings) as! ChangePasswordVC
            self.navigationController?.pushViewController(objChangePasswordVC, animated: true)
            
            break
        case 4:
            let objTermsAndConditionsVC = apde.getController(vc_name: "TermsAndConditionsVC", sb: .Settings) as! TermsAndConditionsVC
            self.navigationController?.pushViewController(objTermsAndConditionsVC, animated: true)
            break
        case 5:
            if kCurrentUser.is_guest {
                hapError()
                return
            }
            let objContactUsVC = apde.getController(vc_name: "ContactUsVC", sb: .Settings) as! ContactUsVC
            self.navigationController?.pushViewController(objContactUsVC, animated: true)
            
            break
        case 6:
            
            if kCurrentUser.user_id > 0 {
                
                BasicFunctions.showPopupTwoButton(title: "SIGN OUT".localize(), desc: "Are you sure you want to sign out?", btnTitleLeft: "CANCEL".localize(), btnTitleRight: "SIGN OUT".localize(),baseVC:self) { (boolValue) in
                    if boolValue {
                        apde.logoutAPI()
                        hapError()
                    }
                }
                
            } else {
                let objInitialController = apde.getController(vc_name: "InitialController", sb: .Authentication) as! UINavigationController
                (objInitialController.viewControllers.first as? WelcomeVC)?.isDismissHide = false
                objInitialController.modalPresentationStyle = .overCurrentContext
                self.tabBarController?.present(objInitialController, animated: true, completion: nil)
            }
            
            break
        default:
            print("default")
        }
    }
    
    
}

//API calls
extension SettingsVC {
    
    func getMyProfile() {
        
        self.cellSettingsUserInfo?.btnEdit.loadingIndicator(show: true)
        
        getProfile { (res) in
            
            switch res {
            case .success(user: let user, message: let msg) :
                print(user)
                print(msg)
                
                self.tblView.beginUpdates()
                self.cellSettingsUserInfo?.lblUsername.text = user.username ?? ""
                self.cellSettingsUserInfo?.lblUserEmail.text = user.email ?? ""
                self.cellSettingsUserInfo.lblNoOfRecipePublished.text = "\(user.no_of_published_recipes) \("Published Recipes".localize())"
                
                self.cellSettingsUserInfo.imgViewUser.image = UIImage.init(named: "AssetUserPlaceholder")
                self.cellSettingsUserInfo?.imgViewUser.mk_afSetImage(urlString: user.profile_image, defaultImage: "AssetUserPlaceholder", block: { (img, boolValue) in
                    self.cellSettingsUserInfo.loadingState = .loaded
                })
                self.cellSettingsScreenLockSwitch?.btnSwitch.isSelected = user.is_screen_lock
                self.tblView.endUpdates()
                
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                break
            }
            
            self.cellSettingsUserInfo?.btnEdit.loadingIndicator(show: false)
        }
    }
}
