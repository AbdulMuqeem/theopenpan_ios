//
//  CellMealPlan.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/12/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias ip = IndexPath

class CellMealPlan: UITableViewCell {

    @IBOutlet weak var lblMealTypeName      : UILabel!
    @IBOutlet weak var lblRecipeName        : UILabel!
    @IBOutlet weak var btnMenu              : UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func handleCell(vc:MealPlanVC,ip:ip) {
        
        let mealPlanModel = vc.arrMealPlanModel[ip.row]
        
        self.lblMealTypeName.text = enum_meal_plan_type(rawValue: Int(mealPlanModel.meal_plan_type))?.getName() ?? ""
        self.lblRecipeName.text = mealPlanModel.recipe_name
        
        self.btnMenu.mk_addTapHandler { (btn) in

            hapSoft()

            let alert = UIAlertController.init(title: "OPTIONS", message: "Recipe name : \(mealPlanModel.recipe_name)", preferredStyle: .actionSheet)
            
            alert.view.tintColor = #colorLiteral(red: 0.631372549, green: 0.7764705882, blue: 0.7254901961, alpha: 1)
            
            alert.addAction(title: "Change Date", style: .default) { (a) in
                
                hapSoft()
                
                let objCalendarVC = apde.getVC(type: CalendarVC.self, sb: .MealPlanTab)!
                objCalendarVC.mealPlanModel = mealPlanModel
                objCalendarVC.selectedDate = vc.calendar.selectedDate ?? Date()
                if #available(iOS 13.0, *) {
                } else {
                    objCalendarVC.modalPresentationStyle = .overCurrentContext
                }
                objCalendarVC.completionBlockDone = {
                    vc.viewWillAppear(true)
                }
                vc.present(objCalendarVC, animated: true, completion: nil)
            }
            
            alert.addAction(title: "Change Meal Type", style: .default) { (a) in
                hapSoft()
                self.handleChangeDateOption(btn: btn, vc: vc, ip: ip, mealPlanModel: mealPlanModel)
            }
            
            alert.addAction(title: "Add to Shopping List", style: .default) { (a) in
                hapSoft()
                self.addMealTypeRecipeIngsToShopList(vc: vc, ip: ip, mealPlanModel: mealPlanModel)
            }
            
            alert.addAction(title: "Remove", style: .default) { (a) in
                hapSoft()
                
                if let index = vc.tblView.indexPath(for: self) {
                    let mpm = vc.arrMealPlanModel[index.row]
                    self.removeRecipeFromMealPlanApiCall(vc: vc, meal_plan_id: mpm.meal_plan_id)
                }
            }
            
            alert.addAction(title: "Cancel", style: .cancel) { (a) in }
            vc.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    func handleChangeDateOption(btn:UIButton,vc:MealPlanVC,ip:ip,mealPlanModel:MealPlanModel) {
        let objAddToMealPlanVC = apde.getController(vc_name: "AddToMealPlanVC", sb: .MealPlanTab) as! AddToMealPlanVC
        objAddToMealPlanVC.recipe_id = mealPlanModel.recipe_id
        objAddToMealPlanVC.isForUpdateMealType = true
        if let selectedDate = vc.calendar.selectedDate {
            objAddToMealPlanVC.preSelectedDate = selectedDate
        }
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        vc.navigationController?.pushViewController(objAddToMealPlanVC, animated: true)
    }
    
    
    
    func showPopOverMenu(btn:UIButton,vc:MealPlanVC,ip:ip,mealPlanModel:MealPlanModel) {
        
        let foundPoint = vc.tblView.convert(btn.frame, from:btn.superview)
        let x = (foundPoint.origin.x - vc.viewPopoverMenu.frame.width) + btn.width()
        var y = foundPoint.origin.y
        vc.viewPopoverMenu.frame.origin = .init(x: x, y: y-4)
        
        if vc.view.convert(btn.frame, from:btn.superview).origin.y - vc.tblView.Yy() > 90 {
            y = y - vc.viewPopoverMenu.height()
            vc.viewPopoverMenu.frame.origin = .init(x: x, y: y + 40)
        }
        
        
        vc.tblView.addSubview(vc.viewPopoverMenu)
        
        vc.btnAddToListPopover.mk_addTapHandler { (btnAddToListPopover) in
            
            hapSoft()
            
            if mealPlanModel.is_saved_in_cookbook.mk_boolValue {
                BasicFunctions.showInfo(strError: "This recipe is already saved in cookbook.")
                return
            }
            
            vc.viewPopoverMenu.removeFromSuperview()
            if let index = vc.tblView.indexPath(for: self) {
                let mpm = vc.arrMealPlanModel[index.row]
                self.btnAddToListTapped(vc:vc, recipe_id: mpm.recipe_id)
            }
            
        }
        
        vc.btnRemovePopover.mk_addTapHandler { (btnRemovePopover) in
            hapSoft()
            vc.viewPopoverMenu.removeFromSuperview()
            if let index = vc.tblView.indexPath(for: self) {
                let mpm = vc.arrMealPlanModel[index.row]
                self.removeRecipeFromMealPlanApiCall(vc: vc, meal_plan_id: mpm.meal_plan_id)
            }
        }
    }

    
    func removeRecipeFromMealPlanApiCall(vc:MealPlanVC, meal_plan_id:Int64) {
        apde.showLoader()
        removeRecipeFromMealPlan(meal_plan_id: meal_plan_id) { (res) in
            switch res {
            case .success(let msg):
                
                BasicFunctions.showInfo(strError: msg)
                let seconds = (vc.calendar.selectedDate ?? Date()).timeIntervalSince1970
                vc.getMealPlanApiCall(date: Int64(seconds))
                vc.tblView.reloadData()
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                break
            }
            apde.hideLoader()
        }
    }
    
    func btnAddToListTapped(vc:MealPlanVC, recipe_id:Int64) {
        
        let objCookbookCollectionNameList = apde.getController(vc_name: "CookbookCollectionNameList", sb: .CookBookTab) as! CookbookCollectionNameList
        objCookbookCollectionNameList.compBlock = { (objCollection,isCancelTapped) in
            
            if isCancelTapped {
                return
            }
            
            if let objCollection = objCollection {
                // Selected Collection
                print(objCollection.id)
                
                apde.showLoader()
                recipeAddToCollection(recipe_id: recipe_id, collection_id: objCollection.id) { (res) in
                    
                    switch res {
                    case .success(_, let message) :
                        BasicFunctions.showInfo(strError: message)
                        break
                    case .failed(message: let msg):
                        BasicFunctions.showError(strError: msg)
                        break
                    case .error(error: let error):
                        BasicFunctions.showError(strError: error)
                        break
                    }
                    
                    apde.hideLoader()
                    
                }
            } else {
                // Selected Add New Collection
                let objAddCookBookCollectionVC = apde.getController(vc_name: "AddCookBookCollectionVC", sb: .CookBookTab) as! AddCookBookCollectionVC
                objAddCookBookCollectionVC.shouldDisplaySuccessPopup = false
                objAddCookBookCollectionVC.compBlock = { (boolValue) in
                    // if Collection Created
                    if boolValue {
                        self.btnAddToListTapped(vc: vc, recipe_id: recipe_id)
                    }
                }
                vc.navigationController?.pushViewController(objAddCookBookCollectionVC, animated: true)
            }
            
        }
        vc.tabBarController?.present(objCookbookCollectionNameList, animated: true, completion: nil)
    }
    
    
    func addMealTypeRecipeIngsToShopList(vc:MealPlanVC,ip:ip,mealPlanModel:MealPlanModel) {
        checkRecipeIsAvailable(recipe_id: mealPlanModel.recipe_id, is_from_cookbook: mealPlanModel.is_saved_in_cookbook.mk_boolValue) { (isAvailable, dictData) in
            
            guard let objIngredientListMealPlanVC = apde.getVC(type: IngredientListMealPlanVC.self, sb: .MealPlanTab) else {
                return
            }
            var recipeDetails = objIngredientListMealPlanVC.recipeDetails
            
            
            
//            guard let dictData = dictData else {
//                return
//            }
//            guard let dictRecipeDetails = JSON(dictData).dictionary?["recipe_details"]?.dictionary else {
//                return
//            }
//            var arrIngredients                  = [Ingredient_list]()
//            for dictIng in dictRecipeDetails["ingredient_list"]?.arrayValue ?? [JSON()] {
//                var ingredient                  = Ingredient_list()
//                ingredient.id                   = dictIng["id"].int64 ?? 0
//                ingredient.unit_name            = dictIng["unit"].string ?? ""
//                ingredient.name                 = dictIng["name"].string ?? ""
//                ingredient.note                 = dictIng["note"].string ?? ""
//                ingredient.qty                  = dictIng["qty"].string ?? ""
//                arrIngredients.append(ingredient)
//            }
//
//            //Ingredient_list
//            var arrDictIngredient = [[String:Any]]()
//            for objIngredient in arrIngredients {
//                let dict = ["ingredient_id":objIngredient.id,
//                            "qty":objIngredient.qty] as [String : Any]
//                arrDictIngredient.append(dict)
//            }
//            guard let jsonDataArrDictIngredient = try? JSONSerialization.data(
//                withJSONObject: arrDictIngredient,
//                options: []) else {
//                    return
//            }
//            guard let textArrIngredients = String(data: jsonDataArrDictIngredient, encoding: .utf8) else {
//                return
//            }
//            print("JSON string textArrIngredients = \(textArrIngredients)")
            
            
            // Set Interested Category tags selected
            guard let dictRecipeDetails = JSON(dictData).dictionary?["recipe_details"]?.dictionary else {
                return
            }
            print(dictRecipeDetails)
            
            recipeDetails.id                           = dictRecipeDetails["id"]?.int64 ?? 0
            recipeDetails.name                         = dictRecipeDetails["name"]?.string ?? ""
            recipeDetails.creator_name                 = dictRecipeDetails["creator_name"]?.string ?? ""
            recipeDetails.ratings                      = dictRecipeDetails["ratings"]?.double ?? 0.0
            recipeDetails.media_type                   = dictRecipeDetails["media_type"]?.int ?? 0
            recipeDetails.media_url                    = dictRecipeDetails["media_url"]?.stringValue ?? ""
            recipeDetails.video_thumb_url              = dictRecipeDetails["video_thumb_url"]?.stringValue ?? ""
            recipeDetails.ready_time_hours             = dictRecipeDetails["ready_time_hours"]?.int ?? 0
            recipeDetails.ready_time_mins              = dictRecipeDetails["ready_time_mins"]?.int ?? 0
            
            recipeDetails.serves                       = dictRecipeDetails["serves"]?.intValue ?? 0
            recipeDetails.original_serves              = dictRecipeDetails["serves"]?.intValue ?? 0
            
            recipeDetails.web_url                      = dictRecipeDetails["web_url"]?.stringValue ?? ""
            recipeDetails.view_url                     = dictRecipeDetails["view_url"]?.stringValue ?? ""
            recipeDetails.no_of_ratings                = dictRecipeDetails["no_of_ratings"]?.int64 ?? 0
            
            recipeDetails.recipe_owner_type            = dictRecipeDetails["recipe_owner_type"]?.int ?? 0
            recipeDetails.is_own_recipe                = dictRecipeDetails["is_own_recipe"]?.int ?? 0
            recipeDetails.is_saved_in_cookbook         = dictRecipeDetails["is_saved_in_cookbook"]?.int ?? 0
            recipeDetails.cookbook_id                  = dictRecipeDetails["cookbook_id"]?.int64 ?? 0
            recipeDetails.is_admin_edited              = dictRecipeDetails["is_admin_edited"]?.int ?? 0
            recipeDetails.admin_notes                  = dictRecipeDetails["admin_notes"]?.stringValue ?? ""
            
            recipeDetails.is_one_pot_meal              = dictRecipeDetails["is_one_pot_meal"]?.int ?? 0
            recipeDetails.is_easy                      = dictRecipeDetails["is_easy"]?.int ?? 0
            recipeDetails.recipe_status                = dictRecipeDetails["recipe_status"]?.int ?? 0

            var arrIngredients                  = [Ingredient_list]()
            for dictIng in dictRecipeDetails["ingredient_list"]?.arrayValue ?? [JSON()] {
                var ingredient                  = Ingredient_list()
                ingredient.id                   = dictIng["id"].int64 ?? 0
                ingredient.unit_name            = dictIng["unit"].string ?? ""
                ingredient.name                 = dictIng["name"].string ?? ""
                ingredient.note                 = dictIng["note"].string ?? ""
                ingredient.qty                  = dictIng["qty"].string ?? ""
                arrIngredients.append(ingredient)
            }

            recipeDetails.ingredient_list                      = arrIngredients
            recipeDetails.original_serves_ingredient_list      = arrIngredients

            var arrDirections                  = [Direction_list]()
            for dictDirection in dictRecipeDetails["direction_list"]?.arrayValue ?? [JSON()] {
                var direction                  = Direction_list()
                direction.step                 = dictDirection["steps"].int ?? 0
                direction.description          = dictDirection["direction"].string ?? ""
                arrDirections.append(direction)
            }
            recipeDetails.directions_list  = arrDirections
            
            
            var arrKeywords                  = [KeywordModel]()
            for dictDirection in dictRecipeDetails["keywords_list"]?.arrayValue ?? [JSON()] {
                var keyword                  = KeywordModel()
                keyword.description          = dictDirection["description"].string ?? ""
                arrKeywords.append(keyword)
            }
            recipeDetails.keywords_list  = arrKeywords
            
            recipeDetails.cuisine_type_id_list         = dictRecipeDetails["cuisine_type_id_list"]?.stringValue ?? ""
            recipeDetails.difficulty_level_id_list     = dictRecipeDetails["difficulty_level_id_list"]?.stringValue ?? ""
            recipeDetails.diet_id_list                 = dictRecipeDetails["diet_id_list"]?.stringValue ?? ""
            recipeDetails.meal_type_id_list            = dictRecipeDetails["meal_type_id_list"]?.stringValue ?? ""

            
            apde.hideLoader()
            
            objIngredientListMealPlanVC.recipeDetails = recipeDetails
            objIngredientListMealPlanVC.mealPlanModel = mealPlanModel
            if #available(iOS 13.0, *) {
            } else {
                objIngredientListMealPlanVC.modalPresentationStyle = .overCurrentContext
            }
            vc.tabBarController?.present(objIngredientListMealPlanVC, animated: true, completion: nil)
            
            
        }
        
        
    }
    
    
    
}



