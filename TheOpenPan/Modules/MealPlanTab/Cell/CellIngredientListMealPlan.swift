//
//  CellIngredientListMealPlan.swift
//  TheOpenPan
//
//  Created by Pathan Mushrankhan on 24/03/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class CellIngredientListMealPlan: UITableViewCell {
    
    @IBOutlet var btnIngredientCheckBox                 : UIButton!
    @IBOutlet var lblIngredientQty                      : UILabel!
    @IBOutlet var lblIngredientName                     : UILabel!
    
    var currentVC : IngredientListMealPlanVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    // Single Recipe Ingredient cell
    func handleCellSingleRecipeIngredientList(vc:IngredientListMealPlanVC, indexPath:IndexPath) {
        
        self.currentVC = vc
        
        //        if vc.recipeDetails.is_btn_add_ing_to_shoplist_tapped {
        //            self.btnIngredientCheckBox.isHidden     = false
        //        } else {
        //            self.btnIngredientCheckBox.isHidden     = true
        //        }
        
        let ingredient = vc.recipeDetails.ingredient_list[indexPath.row]
        self.btnIngredientCheckBox.isSelected   = ingredient.isSelected
        self.lblIngredientQty.text              = ingredient.qty
        self.lblIngredientName.text             = "\(ingredient.unit_name) \(ingredient.name)"
        
        self.btnIngredientCheckBox.mk_addTapHandler { (btn) in
            
            if let indexPathTapped = vc.tblView.indexPath(for: self) {
                
                var ingredientTapped                                        = vc.recipeDetails.ingredient_list[indexPathTapped.row]
                btn.isSelected                                              = !btn.isSelected
                ingredientTapped.isSelected                                 = !ingredientTapped.isSelected
                vc.recipeDetails.ingredient_list[indexPathTapped.row]       = ingredientTapped
                
                self.handleSelectAllBtnState(vc: vc, indexPath: indexPathTapped)
            }
        }
        
        
        vc.btnSelectAllIngs.mk_addTapHandler { (btn) in
            
            btn.isSelected = !btn.isSelected
            
            if btn.isSelected {
                for (i,_) in vc.recipeDetails.ingredient_list.enumerated() {
                    vc.recipeDetails.ingredient_list[i].isSelected = true
                    vc.tblView.reloadData()
                }
            } else {
                for (i,_) in vc.recipeDetails.ingredient_list.enumerated() {
                    vc.recipeDetails.ingredient_list[i].isSelected = false
                    vc.tblView.reloadData()
                }
            }
            
            
            
        }
        
    }
    
    func handleSelectAllBtnState(vc:IngredientListMealPlanVC, indexPath:IndexPath) {
        
        if vc.arrCombinedMealPlanIngsModel.count > 0 {
            let arrFoundDeSelected = vc.arrCombinedMealPlanIngsModel.filter({ (ing) -> Bool in
                ing.isSelected == false
            })
            vc.btnSelectAllIngs.isSelected = arrFoundDeSelected.first == nil ? true : false
        } else {
            let arrFoundDeSelected = vc.recipeDetails.ingredient_list.filter({ (ing) -> Bool in
                ing.isSelected == false
            })
            vc.btnSelectAllIngs.isSelected = arrFoundDeSelected.first == nil ? true : false
        }
        
    }
    
}


extension CellIngredientListMealPlan {
    
    // Single Recipe Ingredient cell
    func handleCellCombinedRecipeIngredientList(vc:IngredientListMealPlanVC, indexPath:IndexPath) {
        
        self.currentVC = vc
        
        let objCombinedMealPlanIngsModel = vc.arrCombinedMealPlanIngsModel[indexPath.row]
        
        self.btnIngredientCheckBox.isSelected   = objCombinedMealPlanIngsModel.isSelected
        self.lblIngredientQty.text              = objCombinedMealPlanIngsModel.combined_qty
        
        let firstObject = objCombinedMealPlanIngsModel.recipes_ingredients.first
        self.lblIngredientName.text             = "\(firstObject?.unit ?? "") \(firstObject?.name ?? "")"
        
        self.btnIngredientCheckBox.mk_addTapHandler { (btn) in
            
            if let indexPathTapped = vc.tblView.indexPath(for: self) {
                
                var objCombinedMealPlanIngsModelTapped = vc.arrCombinedMealPlanIngsModel[indexPathTapped.row]
                //                var ingredientTapped                                        = vc.recipeDetails.ingredient_list[indexPathTapped.row]
                btn.isSelected                                              = !btn.isSelected
                objCombinedMealPlanIngsModelTapped.isSelected               = !objCombinedMealPlanIngsModelTapped.isSelected
                vc.arrCombinedMealPlanIngsModel[indexPathTapped.row]              = objCombinedMealPlanIngsModelTapped
                
                self.handleSelectAllBtnState(vc: vc, indexPath: indexPathTapped)
            }
        }
        
        
        vc.btnSelectAllIngs.mk_addTapHandler { (btn) in
            
            btn.isSelected = !btn.isSelected
            
            if btn.isSelected {
                for (i,_) in vc.arrCombinedMealPlanIngsModel.enumerated() {
                    vc.arrCombinedMealPlanIngsModel[i].isSelected = true
                    vc.tblView.reloadData()
                }
            } else {
                for (i,_) in vc.arrCombinedMealPlanIngsModel.enumerated() {
                    vc.arrCombinedMealPlanIngsModel[i].isSelected = false
                    vc.tblView.reloadData()
                }
            }
        }
        
    }
    
    
}
