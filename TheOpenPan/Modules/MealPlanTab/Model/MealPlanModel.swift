//
//  MealPlanModel.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/14/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation

struct MealPlanModel : Codable {
    var is_saved_in_cookbook    = Int()
    var meal_plan_id            = Int64()
    var recipe_id               = Int64()
    var recipe_name             = String()
    var meal_plan_type          = Int64()
}



struct CombinedMealPlanIngsModel : Codable {
    var combined_qty                = String()
    var isSelected                  = false
    var recipes_ingredients         = [RecipeIngMealPlan]()
}

struct RecipeIngMealPlan : Codable {
    var id                  = Int64()
    var is_from_cookbook    = Int()
    var name                = String()
    var note                = String()
    var qty                 = String()
    var recipe_id           = Int64()
    var unit                = String()
    
}



//{
//       "combined_qty" = "3/4";
//       "recipes_ingredients" =                 (
//                               {
//               id = 1899;
//               "is_from_cookbook" = 0;
//               name = "Parboiled rice (idli-dosa rice)";
//               note = "";
//               qty = "3/4";
//               "recipe_id" = 183;
//               unit = cup;
//           }
//       );
//   },
