//
//  MealPlanVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/12/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import FSCalendar
import DZNEmptyDataSet
import SwiftyJSON

class MealPlanVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet weak var tblView                      : UITableView!
    @IBOutlet weak var calendar                     : FSCalendar!
    
    @IBOutlet weak var viewSelectDateRange              : UIView!
    @IBOutlet weak var viewSelectDateRange_btnDone      : UIButton!
    @IBOutlet weak var viewSelectDateRange_btnCancel    : UIButton!
    
    
    @IBOutlet weak var lblDate                      : UILabel!
    @IBOutlet weak var btnAdd                       : UIButton!
    @IBOutlet weak var btnCreateList                : UIButton!
    
    @IBOutlet var viewPopoverMenu                   : UIView!
    @IBOutlet weak var btnAddToListPopover          : UIButton!
    @IBOutlet weak var btnRemovePopover             : UIButton!
    
    
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    var arrMealPlanModel                            = [MealPlanModel]()
    var arrIntTimestampDate                         = [Int64]()
    
    // first date in the range
    private var firstDate: Date?
    // last date in the range
    private var lastDate: Date?
    
    private var datesRange: [Date]?
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }
    
    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        self.navigationItem.title = "Meal Plan".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        
        self.setupCalendar()
        self.setupTableView()
        
        self.viewSelectDateRange.alpha = 0
        
        self.btnAdd.mk_addTapHandler { (btn) in
            
            if let tabSecondNavController = (self.tabBarController?.viewControllers?[1] as? UINavigationController) {
                apde.preSelectedDateForAddToMealPlan = self.calendar.selectedDate ?? Date()
                if let objMyCookBookVC = tabSecondNavController.viewControllers.first as? MyCookBookVC {
                    objMyCookBookVC.isFromMealPlan = true
                }
                self.tabBarController?.selectedIndex = 1
                tabSecondNavController.popToRootViewController(animated: true)
            }
        }
        
        self.btnCreateList.onTap {
            
            self.lastDate = nil
            self.firstDate = nil
            self.datesRange = []
            for date in self.calendar.selectedDates {
                self.calendar.deselect(date)
            }
            self.calendar.allowsMultipleSelection = true
            self.viewSelectDateRange.mk_animateAlpha(sec: 0.3, alpha: 1.0)
        }
        
        self.viewSelectDateRange_btnCancel.onTap {
            
            self.lastDate = nil
            self.firstDate = nil
            self.datesRange = []
            for date in self.calendar.selectedDates {
                self.calendar.deselect(date)
            }
            
            self.calendar.allowsMultipleSelection = false
            self.viewSelectDateRange.mk_animateAlpha(sec: 0.3, alpha: 0.0)
            self.selectTodaysDate()
        }
        
        self.viewSelectDateRange_btnDone.onTap {
            
            if self.calendar.selectedDates.count > 0 {
                self.showAlertReviewDateRangeIngredients()
            } else {
                self.calendar.shake()
            }
        }
        
        
        
    }
    
    func selectTodaysDate() {
        
        self.lastDate = nil
        self.firstDate = nil
        self.datesRange = []
        for date in self.calendar.selectedDates {
            self.calendar.deselect(date)
        }
        self.calendar.allowsMultipleSelection = false
        self.viewSelectDateRange.mk_animateAlpha(sec: 0.3, alpha: 0.0)
        
        Timer.after(0.3) {
            
            let seconds = Date().timeIntervalSince1970
            self.getMealPlanApiCall(date: Int64(seconds))
            self.setDateText(date: Date())
            
            self.calendar.select(Date(), scrollToDate: true)
            self.setDateText(date: Date())
            let month = Calendar.current.component(.month, from: Date())
            self.getMealDatesByMonthsApiCall(month: month)
        }
    }
    
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        changeTabBar(hidden: false, animated: false, completion: nil)
        let seconds = (self.calendar.selectedDate ?? Date()).timeIntervalSince1970
        self.getMealPlanApiCall(date: Int64(seconds))
        
        let month = Calendar.current.component(.month, from: self.calendar.selectedDate ?? Date())
        self.getMealDatesByMonthsApiCall(month: month)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.viewPopoverMenu.removeFromSuperview()
    }
    
    func setupCalendar() {
        
        self.calendar.delegate                      = self
        self.calendar.dataSource                    = self
        self.calendar.appearance.titleFont          = enum_font.regular.font(17)
        self.calendar.appearance.weekdayFont        = enum_font.medium.font(17)
        self.calendar.appearance.subtitleFont       = enum_font.medium.font(19)
        self.calendar.appearance.headerTitleFont    = enum_font.medium.font(19)
        self.calendar.appearance.eventSelectionColor = #colorLiteral(red: 0.6924785972, green: 0.6451357007, blue: 0.6242417097, alpha: 1)
        
        self.calendar.select(Date(), scrollToDate: true)
        self.setDateText(date: Date())
        let month = Calendar.current.component(.month, from: Date())
        self.getMealDatesByMonthsApiCall(month: month)
        
    }
    
    
    
}


//------------------------------------------
//MARK: - Table View Setup Methods -
extension MealPlanVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.emptyDataSetSource = self
        self.tblView.emptyDataSetDelegate = self
        
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
        let viewGesture = UIView()
        self.tblView.backgroundView = viewGesture
        viewGesture.addTapGesture { (g) in
            self.viewPopoverMenu.removeFromSuperview()
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMealPlanModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellMealPlan = tableView.dequeueReusableCell(withIdentifier: "CellMealPlan", for: indexPath) as! CellMealPlan
        cellMealPlan.handleCell(vc: self, ip: indexPath)
        return cellMealPlan
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mealPlanModel = self.arrMealPlanModel[indexPath.row]
        
        let objRecipeDetailsVC = apde.getController(vc_name: "RecipeDetailsVC", sb: .RecipeDetailsVC) as! RecipeDetailsVC
        objRecipeDetailsVC.recipe_id = mealPlanModel.recipe_id
        objRecipeDetailsVC.recipeDeletedOrUpdatedBlock = { (updatedRecipeDetails, isReloadWhole) in
            let seconds = (self.calendar.selectedDate ?? Date()).timeIntervalSince1970
            self.getMealPlanApiCall(date: Int64(seconds))
        }
        self.navigationController?.pushViewController(objRecipeDetailsVC, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.viewPopoverMenu.removeFromSuperview()
    }
}

extension MealPlanVC : DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return "No meal plan for the day...".AttributedStringFont(enum_font.medium.font(16), fontColor: #colorLiteral(red: 0.7450980392, green: 0.6823529412, blue: 0.6549019608, alpha: 1))
    }
}


extension MealPlanVC {
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        let month = Calendar.current.component(.month, from: calendar.currentPage)
        self.getMealDatesByMonthsApiCall(month: month)
    }
    
    
    func setDateText(date:Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "d EE"
        self.lblDate.text = formatter.string(from: date).uppercased()
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        switch monthPosition {
        case .current :
            return true
        case .next :
            return false
        case .previous :
            return false
        case .notFound :
            return true
        @unknown default:
            return false
        }
    }
    
    
    func getCurrentCalendarDateStringMMDDYYYY() -> String {
        return getDateStringMMDDYYYY(date: self.calendar.selectedDate ?? Date())
    }
    
}

extension MealPlanVC : FSCalendarDelegateAppearance, FSCalendarDataSource {
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        
        return [#colorLiteral(red: 0.6924785972, green: 0.6451357007, blue: 0.6242417097, alpha: 1)]
    }
    
    func calendar(_ calendar: FSCalendar, labelOfDate label: UILabel, date: Date) -> UILabel? {
        label.font = enum_font.regular.font(15)
//        for timestamp in self.arrIntTimestampDate {
//            let dateWhichHaveMeals = Date.init(timeIntervalSince1970: TimeInterval(timestamp))
//            if dateWhichHaveMeals.startOfDay == date.startOfDay {
//                label.font = enum_font.bold.font(15)
//                return nil
//            }
//        }
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        for timestamp in self.arrIntTimestampDate {
            let dateWhichHaveMeals = Date.init(timeIntervalSince1970: TimeInterval(timestamp))
            if dateWhichHaveMeals.startOfDay == date.startOfDay {
                return 1
            }
        }
        
        return 0
    }
    
}





func getDateStringMMDDYYYY(date:Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "MM/dd/yyyy"
    let strDate = formatter.string(from: date).uppercased()
    return strDate
}


//MARK: - API CALLS
extension MealPlanVC {
    
    func getMealDatesByMonthsApiCall(month:Int) {
        getMealDatesByMonths(month: month) { (res) in
            switch res {
            case .success(_, let dictData,_) :
                
                print(dictData)
                self.arrIntTimestampDate.removeAll()
                
                let jsonDict = JSON.init(dictData)
                
                if let arrDates = jsonDict["mealplan_dates"].array {
                    for d in arrDates {
                        let timestamp = d["date"].int64Value
                        self.arrIntTimestampDate.append(timestamp)
                    }
                }
                
                
                var datesToBeBold = [Date]()
                for timestamp in self.arrIntTimestampDate {
                    let dateWhichHaveMeals = Date.init(timeIntervalSince1970: TimeInterval(timestamp)).startOfDay
                    datesToBeBold.append(dateWhichHaveMeals)
                }
                
                self.calendar.reloadData()
                
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                break
            }
        }
    }
    
    func getMealPlanApiCall(date: Int64) {
        
        apde.showLoader()
        self.tblView.mk_animateAlpha(sec: 0.2, alpha: 0.0)
        getMealPlan(date: date) { (res) in
            switch res {
            case .success(_, let dictData,_) :
                
                
                let jsonDict = JSON.init(dictData)
                
                if let arrDict = jsonDict["mealplan_list"].array {
                    
                    var arrMealPlanModel = [MealPlanModel]()
                    
                    for dict in arrDict {
                        let is_saved_in_cookbook    = dict["is_saved_in_cookbook"].intValue
                        let meal_plan_id            = dict["meal_plan_id"].int64Value
                        let recipe_id               = dict["recipe_id"].int64Value
                        let recipe_name             = dict["recipe_name"].stringValue
                        let meal_plan_type          = dict["meal_plan_type"].int64Value
                        let m = MealPlanModel.init(is_saved_in_cookbook: is_saved_in_cookbook, meal_plan_id: meal_plan_id, recipe_id: recipe_id, recipe_name: recipe_name, meal_plan_type: meal_plan_type)
                        arrMealPlanModel.append(m)
                    }
                    self.arrMealPlanModel.removeAll()
                    self.tblView.reloadData()
                    Timer.after(0.2) {
                        self.arrMealPlanModel = arrMealPlanModel
                        self.tblView.reloadData()
                        self.tblView.mk_animateAlpha(sec: 0.2, alpha: 1.0)
                    }
                }
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                break
            }
        }
        
        apde.hideLoader()
    }
}


//extension MealPlanVC : FSCalendarDelegate {
//
//    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
//        performDateDeselect(calendar, date: date)
//        return true
//    }
//
//    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
//        performDateSelection(calendar)
//    }
//
//    private func performDateDeselect(_ calendar: FSCalendar, date: Date) {
//        let sorted = calendar.selectedDates.sorted { $0 < $1 }
//        if let index = sorted.firstIndex(of: date)  {
//            let deselectDates = Array(sorted[index...])
//            calendar.deselectDates(deselectDates)
//        }
//    }
//
//    private func performDateSelection(_ calendar: FSCalendar) {
//        let sorted = calendar.selectedDates.sorted { $0 < $1 }
//        if let firstDate = sorted.first, let lastDate = sorted.last {
//            let ranges = datesRange(from: firstDate, to: lastDate)
////            calendar.selectDates(ranges)
////            calendar.select
//        }
//    }
//
//    func datesRange(from: Date, to: Date) -> [Date] {
//        if from > to { return [Date]() }
//        var tempDate = from
//        var array = [tempDate]
//        while tempDate < to {
//            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
//            array.append(tempDate)
//        }
//        return array
//    }
//}


extension MealPlanVC : FSCalendarDelegate {
    
    
    func datesRange(from: Date, to: Date) -> [Date] {
        if from > to { return [Date]() }
        var tempDate = from
        var array = [tempDate]
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        return array
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        
        if self.calendar.allowsMultipleSelection == true {
            
//            // nothing selected:
//            if firstDate == nil {
//                firstDate = date
//                datesRange = [firstDate!]
//
//                print("datesRange contains: \(datesRange!)")
//
//                return
//            }
//
//            // only first date is selected:
//            if firstDate != nil && lastDate == nil {
//                // handle the case of if the last date is less than the first date:
//                if date <= firstDate! {
//                    calendar.deselect(firstDate!)
//                    firstDate = date
//                    datesRange = [firstDate!]
//
//                    print("datesRange contains: \(datesRange!)")
//
//                    return
//                }
//
//                let range = datesRange(from: firstDate!, to: date)
//
//                lastDate = range.last
//
//                for d in range {
//                    calendar.select(d)
//                }
//
//                datesRange = range
//
//                print("datesRange contains: \(datesRange!)")
//
//                return
//            }
//
//            // both are selected:
//            if firstDate != nil && lastDate != nil {
//                for d in calendar.selectedDates {
//                    calendar.deselect(d)
//                }
//
//                lastDate = nil
//                firstDate = nil
//
//                datesRange = []
//
//                print("datesRange contains: \(datesRange!)")
//            }
            
            
            
        } else {
            
            hapSoft()
            
            self.viewPopoverMenu.removeFromSuperview()
            
            self.btnAdd.isEnabled = true
            self.btnAdd.alpha = 1.0
            if date < Date() {
                print("date1 is earlier than date2")
                self.btnAdd.isEnabled = false
                self.btnAdd.alpha = 0.5
            }
            if date.startOfDay == Date().startOfDay {
                self.btnAdd.isEnabled = true
                self.btnAdd.alpha = 1.0
            }
            
            let seconds = date.timeIntervalSince1970
            self.getMealPlanApiCall(date: Int64(seconds))
            self.setDateText(date: date)
        }
        
        
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // both are selected:
        
        // NOTE: the is a REDUANDENT CODE:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            lastDate = nil
            firstDate = nil
            
            datesRange = []
            print("datesRange contains: \(datesRange!)")
        }
    }
}


extension MealPlanVC {
    func showAlertReviewDateRangeIngredients() {
        let alert = UIAlertController.init()
        
//        let alert = UIAlertController.init(title: "RECIPES INGREDIENTS", message: "OPTIONS", preferredStyle: .actionSheet)
        
        alert.view.tintColor = #colorLiteral(red: 0.631372549, green: 0.7764705882, blue: 0.7254901961, alpha: 1)
        
        alert.addAction(title: "Review all ingredients", style: .default) { (a) in
            hapSoft()
            self.getAllMealIngsByDates { (arrCombinedMealPlanIngsModel) in
                if let arrCombinedMealPlanIngsModel = arrCombinedMealPlanIngsModel {
                    self.selectTodaysDate()
                    self.showIngsList(arrCombinedMealPlanIngsModel: arrCombinedMealPlanIngsModel)
                }
            }
        }
        
        alert.addAction(title: "Add without review", style: .default) { (a) in
            hapSoft()
            self.getAllMealIngsByDates { (arrCombinedMealPlanIngsModel) in
                if let arrCombinedMealPlanIngsModel = arrCombinedMealPlanIngsModel {
                    self.selectTodaysDate()
                    self.addIngsToShopListDirectly(arrCombinedMealPlanIngsModel: arrCombinedMealPlanIngsModel)
                }
            }
            
        }
        
        alert.addAction(title: "Cancel", style: .cancel) { (a) in }
        self.present(alert, animated: true, completion: nil)
    }
}


extension MealPlanVC {
    func getAllMealIngsByDates(completionHandler: @escaping ([CombinedMealPlanIngsModel]?)->()) {
        
        var arrStringDateTimestamp = [String]()
        for date in self.calendar.selectedDates {
            arrStringDateTimestamp.append("\(date.startOfDay.timeIntervalSince1970)")
        }
        let dates = arrStringDateTimestamp.joined(separator: ",")
        
        apde.showLoader()

        getAllIngsByDates(dates: dates) { (res) in
            switch res {
            case .success(_, let dictData,_) :
                
                let jsonDict = JSON.init(dictData)
                print(jsonDict)
                if let arrDict = jsonDict["mealplan_list"].array {

                    var arrCombinedMealPlanIngsModel = [CombinedMealPlanIngsModel]()

                    for dict in arrDict {
                        
                        let combined_qty            = dict["combined_qty"].stringValue
                        let recipes_ingredients     = dict["recipes_ingredients"].arrayValue
                        
                        var arrRecipeIngMealPlan = [RecipeIngMealPlan]()
                        
                        for dictIng in recipes_ingredients {
                            
                            let id                      = dictIng["id"].int64Value
                            let is_saved_in_cookbook    = dictIng["is_saved_in_cookbook"].intValue
                            let name                    = dictIng["name"].stringValue
                            let note                    = dictIng["note"].stringValue
                            let qty                     = dictIng["qty"].stringValue
                            let recipe_id               = dictIng["recipe_id"].int64Value
                            let unit                    = dictIng["unit"].stringValue
                            
                            let objRecipeIngMealPlan = RecipeIngMealPlan.init(id: id, is_from_cookbook: is_saved_in_cookbook, name: name, note: note, qty: qty, recipe_id: recipe_id, unit: unit)
                            arrRecipeIngMealPlan.append(objRecipeIngMealPlan)
                        }
                        let objCombinedMealPlanIngsModel = CombinedMealPlanIngsModel.init(combined_qty: combined_qty, recipes_ingredients: arrRecipeIngMealPlan)
                        arrCombinedMealPlanIngsModel.append(objCombinedMealPlanIngsModel)
                        
                    }

                    Timer.after(0.2) {
                        completionHandler(arrCombinedMealPlanIngsModel)
                    }
                }
                
                break
            case .failed(message: let msg):
                completionHandler(nil)
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                completionHandler(nil)
                BasicFunctions.showError(strError: error)
                break
            }
        }
        
        apde.hideLoader()
    }
    
    
    func showIngsList(arrCombinedMealPlanIngsModel:[CombinedMealPlanIngsModel]) {
        guard let objIngredientListMealPlanVC = apde.getVC(type: IngredientListMealPlanVC.self, sb: .MealPlanTab) else {
            return
        }
        objIngredientListMealPlanVC.arrCombinedMealPlanIngsModel = arrCombinedMealPlanIngsModel
        
        if #available(iOS 13.0, *) {
        } else {
            objIngredientListMealPlanVC.modalPresentationStyle = .overCurrentContext
        }
        self.tabBarController?.present(objIngredientListMealPlanVC, animated: true, completion: nil)
    }
    
    
    
    func addIngsToShopListDirectly(arrCombinedMealPlanIngsModel:[CombinedMealPlanIngsModel]) {
        
        var arrDictIngredient = [[String:Any]]()
        for combinedMealPlanIngsModel in arrCombinedMealPlanIngsModel {
            
            
            for objIngredient in combinedMealPlanIngsModel.recipes_ingredients {
                
                let dict = ["ingredient_id":objIngredient.id,
                            "recipe_id":objIngredient.recipe_id,
                            "is_from_cookbook":objIngredient.is_from_cookbook,
                            "qty":objIngredient.qty] as [String : Any]
                
                arrDictIngredient.append(dict)
            }
            
        }
        
        guard let jsonDataArrDictIngredient = try? JSONSerialization.data(
            withJSONObject: arrDictIngredient,
            options: []) else {
                return
        }
        
        guard let textArrIngredients = String(data: jsonDataArrDictIngredient, encoding: .utf8) else {
            return
        }
        print("JSON string textArrIngredients = \(textArrIngredients)")
        
        self.addIngsToShopListApiCall(recipes_list_with_ingredients: textArrIngredients)
    }
    
    func addIngsToShopListApiCall(recipes_list_with_ingredients:String) {
        
        addRecipeIngsToShopList(recipes_list_with_ingredients: recipes_list_with_ingredients) { (res) in
            
            switch res {
            case .success(dictData:_, message: let str) :
                
                BasicFunctions.showInfo(strError: str)
                break
            case .error(error: let err) :
                print(err)
                BasicFunctions.showError(strError: err)
                break
            case .failed(message: let msg) :
                BasicFunctions.showError(strError: msg)
                break
            }
            
        }
    }
}
