
//
//  IngredientListMealPlanVC.swift
//  TheOpenPan
//
//  Created by Pathan Mushrankhan on 24/03/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

extension IngredientListMealPlanVC {
    
    func setupKwStepper() {
        
        self.kwStepper = KWStepper(decrementButton: self.btnDecreaseServe, incrementButton: self.btnIncreaseServe)
        self.kwStepper.autoRepeat = true
        self.kwStepper.autoRepeatInterval = 0.10
        self.kwStepper.wraps = false
        self.kwStepper.minimumValue = 1
        self.kwStepper.maximumValue = 10000
        self.kwStepper.incrementStepValue = 1
        self.kwStepper.decrementStepValue = 1
        
        self.kwStepper.value = Double(self.recipeDetails.serves)
        
        let strServeCount = self.kwStepper.value > 9 ? String(Int(self.kwStepper.value)) : String(format: "%02d", Int(self.kwStepper.value))
        self.lblServeCount.text = "Serves " + strServeCount
        
        self.kwStepper.valueChangedCallback = { stepper in
            
            let strServeCount = stepper.value > 9 ? String(Int(stepper.value)) : String(format: "%02d", Int(stepper.value))
            self.lblServeCount.text = "Serves " + strServeCount
            hapSoft()
            
            let original_serves_ingredient_list = self.recipeDetails.original_serves_ingredient_list
            var arrIngredients                  = [Ingredient_list]()
            for original_serves_ingredient in original_serves_ingredient_list {
                
                var ingredient                  = Ingredient_list()
                ingredient                      = original_serves_ingredient
                
                if let dblQty = Double(original_serves_ingredient.qty) {
                    
                    let singleQty   = dblQty/Double(self.recipeDetails.original_serves)
                    ingredient.qty  = "\((singleQty*self.kwStepper.value).rounded(toPlaces: 2))"
                    
                } else {
                    
                    
                    if Int(self.kwStepper.value) == self.recipeDetails.original_serves {
                        
                    } else {
                        
                        var qty = removeSpace(original_serves_ingredient.qty)
                        
                        if qty.first == " " {
                            qty = String(qty.dropFirst())
                        }
                        
                        if qty.last == " " {
                            qty = String(qty.dropLast())
                        }
                        
                        if qty.components(separatedBy: "/").count > 1 {
                            
                            if let f = qty.components(separatedBy: "/").first, let l = qty.components(separatedBy: "/").last {
                                
                                if f != "" && l != "" {
                                    
                                    // case 1 1/2 --- one and a half
                                    if f.components(separatedBy: " ").count > 1 {
                                        if let intFirst = f.components(separatedBy: " ").first?.integer {
                                            if let intLast = f.components(separatedBy: " ").last?.integer {
                                                if let intLastBehindSlash = Int(l) {
                                                    let dbl = (Double(intLast)/Double(intLastBehindSlash)).rounded(toPlaces: 2)
                                                    let total = dbl + Double(intFirst)
                                                    let singleQty   = total/Double(self.recipeDetails.original_serves)
                                                    let increasedQty = (singleQty*self.kwStepper.value).rounded(toPlaces: 2)
                                                    
                                                    if increasedQty.whole != 0.0 && increasedQty.fraction != 0.0 {
                                                        let strFraction = float(toFraction: Float(increasedQty.fraction))
                                                        ingredient.qty = "\(Int(increasedQty.whole)) \(strFraction)"
                                                    } else if increasedQty.whole == 0.0 && increasedQty.fraction != 0.0 {
                                                        let strFraction = float(toFraction: Float(increasedQty.fraction))
                                                        ingredient.qty = "\(strFraction)"
                                                    } else {
                                                        ingredient.qty = "\(Int(increasedQty.whole))"
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        // case 1/2 --- half
                                        if let intFirst = Int(f),let intLast = Int(l) {
                                            
                                            let dbl = (Double(intFirst)/Double(intLast)).rounded(toPlaces: 4)
                                            let singleQty       = dbl/Double(self.recipeDetails.original_serves)
                                            let increasedQty    = (singleQty*self.kwStepper.value).rounded(toPlaces: 4)
                                            
                                            if increasedQty.whole != 0.0 && increasedQty.fraction != 0.0 {
                                                let strFraction = float(toFraction: Float(increasedQty.fraction.rounded(toPlaces: 2)))
                                                ingredient.qty = "\(Int(increasedQty.whole)) \(strFraction)"
                                            } else if increasedQty.whole == 0.0 && increasedQty.fraction != 0.0 {
                                                let strFraction = float(toFraction: Float(increasedQty.fraction.rounded(toPlaces: 3)))
                                                ingredient.qty = "\(strFraction)"
                                            } else {
                                                ingredient.qty = "\(Int(increasedQty.whole))"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                arrIngredients.append(ingredient)
            }
            self.recipeDetails.ingredient_list        = arrIngredients
            self.recipeDetails.serves                 = Int(stepper.value)
            
            self.tblView.reloadData()
            
        }
        self.kwStepper.longPressEnded { stepper in
            hapError()
        }.minValueClamped { (stepper) in
            hapError()
        }.maxValueClamped { (stepper) in
            hapError()
        }
    }
}

