
//
//  IngredientListMealPlanVC.swift
//  TheOpenPan
//
//  Created by Pathan Mushrankhan on 24/03/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class IngredientListMealPlanVC: UIViewController {
    
    @IBOutlet weak var tblView                          : UITableView!
    
    @IBOutlet weak var btnCancel                        : UIButton!
    @IBOutlet weak var btnAddToShopList                 : UIButton!
    
    @IBOutlet var lblServeCount                         : UILabel!
    @IBOutlet var btnIncreaseServe                      : UIButton!
    @IBOutlet var btnDecreaseServe                      : UIButton!
    
    @IBOutlet var btnSelectAllIngs                      : UIButton!
    
    var recipeDetails                                   = RecipeDetails()
    var mealPlanModel                                   : MealPlanModel!
    
    var arrCombinedMealPlanIngsModel                    = [CombinedMealPlanIngsModel]()
    //    var arrRecipeIngMealPlan                            = [RecipeIngMealPlan]()
    
    var kwStepper                                       : KWStepper!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.setupTableView()
        
        if self.arrCombinedMealPlanIngsModel.count > 0 {
            self.setupCombinedRecipeAddIngsToShopListSelector()
        } else {
            self.setupSingleRecipeAddIngsToShopListSelector()
        }
        
        self.setupCancelButtonSelector()
        self.setupKwStepper()
        
    }
    
    
    func setupCancelButtonSelector() {
        self.btnCancel.onTap {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func setupSingleRecipeAddIngsToShopListSelector() {
        
        
        self.btnAddToShopList.mk_addTapHandler { (btn) in
            
            let arrFoundDeSelected = self.recipeDetails.ingredient_list.filter({ (ing) -> Bool in
                ing.isSelected == true
            })
            
            if arrFoundDeSelected.first == nil {
                self.tblView.shake()
                return
            }
            
            self.btnAddToShopList.isSelected = !self.btnAddToShopList.isSelected
            
            if self.btnAddToShopList.isSelected {
                self.btnAddToShopList.setTitle("Added to shopping".capitalized, for: .normal)
                self.addIngsToShopList()
            } else {
                self.btnAddToShopList.setTitle("Add to shopping".capitalized, for: .normal)
                self.undoIngsFromShopList()
            }
            
        }
    }
    
    
    func addIngsToShopList()  {
        

        
        let selected_ingredient = self.recipeDetails.ingredient_list.filter { (i) -> Bool in
            return i.isSelected
        }
        
        var isFromCookBook = false
        if self.recipeDetails.is_saved_in_cookbook.mk_boolValue { isFromCookBook = true }
        if self.recipeDetails.is_own_recipe.mk_boolValue { isFromCookBook = false }
        
        //Ingredient_list
        var arrDictIngredient = [[String:Any]]()
        for objIngredient in selected_ingredient {
            
            let dict = ["ingredient_id":objIngredient.id,
                        "recipe_id":self.recipeDetails.id,
                        "is_from_cookbook":isFromCookBook.mk_intValue,
                        "qty":objIngredient.qty] as [String : Any]
            
            arrDictIngredient.append(dict)
        }
        
        guard let jsonDataArrDictIngredient = try? JSONSerialization.data(
            withJSONObject: arrDictIngredient,
            options: []) else {
                return
        }
        
        guard let textArrIngredients = String(data: jsonDataArrDictIngredient, encoding: .utf8) else {
            return
        }
        print("JSON string textArrIngredients = \(textArrIngredients)")
        
        self.addIngsToShopListApiCall(recipes_list_with_ingredients: textArrIngredients)
        
//        self.dismiss(animated: true, completion: nil)
    }
    
    func undoIngsFromShopList()  {
        
        let arrFoundDeSelected = self.recipeDetails.ingredient_list.filter({ (ing) -> Bool in
            ing.isSelected == true
        })
        
        if arrFoundDeSelected.first == nil {
            self.tblView.shake()
            return
        }
        
        let selected_ingredient = self.recipeDetails.ingredient_list.filter { (i) -> Bool in
            return i.isSelected
        }
        
        var isFromCookBook = false
        if self.recipeDetails.is_saved_in_cookbook.mk_boolValue { isFromCookBook = true }
        if self.recipeDetails.is_own_recipe.mk_boolValue { isFromCookBook = false }
        
        //Ingredient_list
        var arrDictIngredient = [[String:Any]]()
        for objIngredient in selected_ingredient {
            
            let dict = ["ingredient_id":objIngredient.id,
                        "recipe_id":self.recipeDetails.id,
                        "is_from_cookbook":isFromCookBook.mk_intValue,
                        "qty":objIngredient.qty] as [String : Any]
            
            arrDictIngredient.append(dict)
        }
        
        guard let jsonDataArrDictIngredient = try? JSONSerialization.data(
            withJSONObject: arrDictIngredient,
            options: []) else {
                return
        }
        
        guard let textArrIngredients = String(data: jsonDataArrDictIngredient, encoding: .utf8) else {
            return
        }
        print("JSON string textArrIngredients = \(textArrIngredients)")
        
        self.undoIngsShopListMealPlanScreenApiCall(recipes_list_with_ingredients: textArrIngredients)
        
//        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func addIngsToShopListApiCall(recipes_list_with_ingredients:String) {
        
        addRecipeIngsToShopList(recipes_list_with_ingredients: recipes_list_with_ingredients) { (res) in
            
            switch res {
            case .success(dictData:_, message: let str) :
                
                BasicFunctions.showInfo(strError: str)
                break
            case .error(error: let err) :
                print(err)
                BasicFunctions.showError(strError: err)
                break
            case .failed(message: let msg) :
                BasicFunctions.showError(strError: msg)
                break
            }
            
        }
    }
    
    func undoIngsShopListMealPlanScreenApiCall(recipes_list_with_ingredients:String) {
        
        undoIngsShopListMealPlanScreen(recipes_list_with_ingredients: recipes_list_with_ingredients) { (res) in
            
            switch res {
            case .success(dictData:_, message: let str) :
                
                BasicFunctions.showInfo(strError: str)
                break
            case .error(error: let err) :
                print(err)
                BasicFunctions.showError(strError: err)
                break
            case .failed(message: let msg) :
                BasicFunctions.showError(strError: msg)
                break
            }
            
        }
    }
    
    
    
    
    func setupCombinedRecipeAddIngsToShopListSelector() {
        

        self.btnAddToShopList.mk_addTapHandler { (btn) in
            
            let arrFoundDeSelected = self.arrCombinedMealPlanIngsModel.filter({ (ing) -> Bool in
                ing.isSelected == true
            })

            if arrFoundDeSelected.first == nil {
                self.tblView.shake()
                return
            }
            
            self.btnAddToShopList.isSelected = !self.btnAddToShopList.isSelected
            
            
            let selected_ingredient = self.arrCombinedMealPlanIngsModel.filter { (i) -> Bool in
                return i.isSelected
            }
            
            //Ingredient_list
            var arrDictIngredient = [[String:Any]]()
            for combinedMealPlanIngsModel in selected_ingredient {
                
                
                for objIngredient in combinedMealPlanIngsModel.recipes_ingredients {
                    
                    let dict = ["ingredient_id":objIngredient.id,
                                "recipe_id":objIngredient.recipe_id,
                                "is_from_cookbook":objIngredient.is_from_cookbook,
                                "qty":objIngredient.qty] as [String : Any]
                    
                    arrDictIngredient.append(dict)
                }
                
            }
            
            guard let jsonDataArrDictIngredient = try? JSONSerialization.data(
                withJSONObject: arrDictIngredient,
                options: []) else {
                    return
            }
            
            guard let textArrIngredients = String(data: jsonDataArrDictIngredient, encoding: .utf8) else {
                return
            }
            print("JSON string textArrIngredients = \(textArrIngredients)")
            
            
            if self.btnAddToShopList.isSelected {
                self.btnAddToShopList.setTitle("Added to shopping".capitalized, for: .normal)
                self.addIngsToShopListApiCall(recipes_list_with_ingredients: textArrIngredients)
            } else {
                self.btnAddToShopList.setTitle("Add to shopping".capitalized, for: .normal)
                self.undoIngsShopListMealPlanScreenApiCall(recipes_list_with_ingredients: textArrIngredients)
            }
            
            
            
            
            
            
            
//            self.dismiss(animated: true, completion: nil)
            
            
        }
    }
    
    
}


//------------------------------------------
//MARK: - Table View Setup Methods -
extension IngredientListMealPlanVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.estimatedRowHeight = 100
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
        self.tblView.tableFooterView = UIView()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrCombinedMealPlanIngsModel.count > 0 {
            return self.arrCombinedMealPlanIngsModel.count
        } else {
            return recipeDetails.ingredient_list.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIngredientListMealPlan = tableView.dequeueReusableCell(withIdentifier: "CellIngredientListMealPlan", for: indexPath) as! CellIngredientListMealPlan
        
        if self.arrCombinedMealPlanIngsModel.count > 0 {
            cellIngredientListMealPlan.handleCellCombinedRecipeIngredientList(vc: self, indexPath: indexPath)
        } else {
            cellIngredientListMealPlan.handleCellSingleRecipeIngredientList(vc: self, indexPath: indexPath)
        }
        
        
        
        
        return cellIngredientListMealPlan
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
}

