//
//  CalendarVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/12/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import FSCalendar
import DZNEmptyDataSet
import TagListView

class CalendarVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    
    @IBOutlet weak var calendar                     : FSCalendar!
    
    @IBOutlet weak var lblRecipeName                : UILabel!
    @IBOutlet weak var lblMealType                  : UILabel!
    @IBOutlet weak var btnUpdate                    : UIButton!
    @IBOutlet weak var btnCancel                    : UIButton!
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    var arrMealPlanModel                            = [MealPlanModel]()
    
    typealias BlockDone                             = ()->()
    typealias BlockRemove                           = ()->()
    var completionBlockDone                         : BlockDone!
    var completionBlockRemove                       : BlockRemove!
    var selectedDate                                : Date!
//    var meal_plan_type_id                           : Int64!
    
    var mealPlanModel                               : MealPlanModel!
    
    
//    var indexPath                   : IndexPath!
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        self.navigationItem.title = "Meal Plan".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        
        self.setupCalendar()
        
        self.lblMealType.text = enum_meal_plan_type.init(rawValue: Int(self.mealPlanModel.meal_plan_type))?.getName()
        self.lblRecipeName.text = self.mealPlanModel.recipe_name
        
        
        self.calendar.select(selectedDate, scrollToDate: true)
        
        
        self.btnUpdate.mk_addTapHandler { (btn) in
            hapSoft()
            
            if let dateTimestamp = self.calendar.selectedDate?.timeIntervalSince1970 {
                self.updateMealPlanApiCall(meal_plan_type_id: self.mealPlanModel.meal_plan_id, date: Int64(dateTimestamp))
            }
        }
        
        self.btnCancel.mk_addTapHandler { (btn) in
            self.completionBlockRemove?()
            self.dismiss(animated: true, completion: nil)
        }
        
        
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        changeTabBar(hidden: false, animated: false, completion: nil)
//        let seconds = (self.calendar.selectedDate ?? Date()).timeIntervalSince1970
        
    }
    
    
    func setupCalendar() {
        
        self.calendar.delegate                      = self
        self.calendar.dataSource                    = self
        self.calendar.appearance.titleFont          = enum_font.regular.font(17)
        self.calendar.appearance.weekdayFont        = enum_font.medium.font(17)
        self.calendar.appearance.subtitleFont       = enum_font.medium.font(19)
        self.calendar.appearance.headerTitleFont    = enum_font.medium.font(19)
        
        self.calendar.select(Date(), scrollToDate: true)
        self.setDateText(date: Date())
    }
    
    
    
}



extension CalendarVC : FSCalendarDelegate , FSCalendarDataSource {
    
    func calendar(_ calendar: FSCalendar, labelOfDate label: UILabel, date: Date) -> UILabel? {
        label.font = enum_font.regular.font(15)
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        hapSoft()
        
        self.btnUpdate.isEnabled = true
        self.btnUpdate.alpha = 1.0
        if date < Date() {
             print("date1 is earlier than date2")
            self.btnUpdate.isEnabled = false
            self.btnUpdate.alpha = 0.5
        }
        if date.startOfDay == Date().startOfDay {
            self.btnUpdate.isEnabled = true
            self.btnUpdate.alpha = 1.0
        }
           
        let seconds = date.timeIntervalSince1970
//        self.getMealPlanApiCall(date: Int64(seconds))
//        self.setDateText(date: date)
        
    }
    
    func setDateText(date:Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "d EE"
//        self.lblDate.text = formatter.string(from: date).uppercased()
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        switch monthPosition {
        case .current :
            return true
        case .next :
            return false
        case .previous :
            return false
        case .notFound :
            return true
        @unknown default:
           return false
        }
    }
    
    func getCurrentCalendarDateStringMMDDYYYY() -> String {
        return getDateStringMMDDYYYY(date: self.calendar.selectedDate ?? Date())
    }
    
    func updateMealPlanApiCall(meal_plan_type_id:Int64,date: Int64) {
        
        self.btnCancel.alpha = 0.5
        self.btnCancel.isEnabled = false
        self.btnUpdate.loadingIndicator(true)
        
        updateMealPlan(date: date, meal_plan_type_id: meal_plan_type_id) { (res) in
            switch res {
            case .success(message: let msg) :
                BasicFunctions.showInfo(strError: msg)
                self.completionBlockDone()
                self.dismiss(animated: true, completion: nil)
                break
            case .error(error: let err) :
                print(err)
                BasicFunctions.showError(strError: err)
                break
            case .failed(message: let msg) :
                BasicFunctions.showError(strError: msg)
                break
            }
            self.btnCancel.alpha = 1.0
            self.btnCancel.isEnabled = true
            self.btnUpdate.loadingIndicator(false)
        }
        
    }
    
}

//
//
//class EditDirectionVC: UIViewController {
//
//    //------------------------------------------
//    //MARK: - Outlets -
//    @IBOutlet var lblTitleDirection             : UILabel!
//    @IBOutlet var btnDone                       : UIButton!
//    @IBOutlet var btnRemove                     : UIButton!
//    @IBOutlet weak var txtViewDirection         : UITextView!
//    @IBOutlet weak var viewBottomTextView       : UIView!
//    @IBOutlet weak var lblPlaceholderTextView   : UILabel!
//
//    //------------------------------------------
//    //MARK: - Class Variables -
//
//    typealias BlockDone             = (Direction)->()
//    typealias BlockRemove           = (IndexPath)->()
//    var completionBlockDone         : BlockDone!
//    var completionBlockRemove       : BlockRemove!
//
//    var objDirection                : Direction!
//    var indexPath                   : IndexPath!
//
//    //------------------------------------------
//    //MARK: - Memory Management -
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//    }
//    deinit {
//
//    }
//
//    //------------------------------------------
//    //MARK: - Custom Methods -
//    @objc func setupLayout() {
//
//        self.automaticallyAdjustsScrollViewInsets = false
//
//        self.title = "Direction".localize()
//        self.lblTitleDirection.text = "Point \((indexPath.row + 1).stringValue)"
//
//        self.txtViewDirection.delegate = self
//
//        let desc = (self.objDirection.desc ?? "").replacingOccurrences(of: "\n", with: "<br>")
//        self.txtViewDirection.mk_setHTMLFromString2(htmlText: desc)
//        self.textViewDidChange(self.txtViewDirection)
//
//        self.btnDone.mk_addTapHandler { (btn) in
//            hapSoft()
//
//            if self.txtViewDirection.isEmpty() {
//                hapError()
//                self.txtViewDirection.shake()
//                self.txtViewDirection.becomeFirstResponder()
//                return
//            }
//
//            self.objDirection.desc = self.txtViewDirection.text
//            self.completionBlockDone(self.objDirection)
//            self.dismiss(animated: true, completion: nil)
//        }
//
//        self.btnRemove.mk_addTapHandler { (btn) in
//            self.completionBlockRemove(self.indexPath)
//            self.dismiss(animated: true, completion: nil)
//        }
//    }
//
//
//
//    //------------------------------------------
//    //MARK: - View Life Cycle Methods -
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.setupLayout()
//
//    }
//
//
//
//}
//
