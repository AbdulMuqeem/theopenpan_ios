//
//  AddToMealPlanVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/7/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import FSCalendar
import SwiftyJSON

class AddToMealPlanVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
//    @IBOutlet weak var tblView                      : UITableView!
    @IBOutlet weak var calendar                     : FSCalendar!
    
    @IBOutlet weak var lblCookingForTitle           : UILabel!
    @IBOutlet var arrButtonsCookingFor              : [UIButton]!
    @IBOutlet var arrButtonsRadioCookingFor         : [UIButton]!
    
    @IBOutlet var btnSave                           : UIButton!
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    var preSelectedDate                             : Date?
    var recipe_id                                   = Int64()
    var arrMealPlanModel                            = [MealPlanModel]()
    var arrIntTimestampDate                         = [Int64]()
    
    var isForUpdateMealType                         = false
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        self.navigationItem.title = "Add to Meal Plan".localize()
        
        if isForUpdateMealType {
            self.navigationItem.title = "Update Meal Plan".localize()
        }
        
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        
        self.setupCalendar()
        
        apde.showLoader()
        
        if self.isForUpdateMealType {
            self.calendar.alpha = 0.7
        }
        
        
        for (i,btn) in self.arrButtonsCookingFor.enumerated() {
            btn.tag = i+1
        }
        
        
        self.btnSave.mk_addTapHandler { (btn) in
            
            let filtered = self.arrButtonsCookingFor.filter { (btnSub) -> Bool in
                btnSub.isSelected
            }
            
            let selectedFound = filtered.compactMap { (btnSub) -> String in
                return "\(btnSub.tag)"
            }
            
            if selectedFound.count == 0 {
                hapError()
                self.lblCookingForTitle.shake()
                return
            }

            let seconds = (self.calendar.selectedDate ?? Date()).timeIntervalSince1970

            var is_saved_in_cookbook : Int = 0
            for (i,btn) in self.arrButtonsCookingFor.enumerated() {
                for (_,mealPlan) in self.arrMealPlanModel.enumerated() {
                    if self.recipe_id == mealPlan.recipe_id {
                        is_saved_in_cookbook = mealPlan.is_saved_in_cookbook
                    }
                }
            }
            
            self.addToMealPlanApiCall(date: Int64(seconds), recipe_id: self.recipe_id, mealPlanTypes: selectedFound.joined(separator: ","), is_saved_in_cookbook: is_saved_in_cookbook)
        }
        

    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
        self.changeTabBar(hidden: true, animated: false, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let month = Calendar.current.component(.month, from: self.calendar.selectedDate ?? Date())
        self.getMealDatesByMonthsApiCall(month: month)
    }
    
    func setupCalendar() {
        
        self.calendar.appearance.titleFont          = enum_font.regular.font(17)
        self.calendar.appearance.weekdayFont        = enum_font.medium.font(17)
        self.calendar.appearance.subtitleFont       = enum_font.medium.font(19)
        self.calendar.appearance.headerTitleFont    = enum_font.medium.font(19)
        self.calendar.appearance.eventSelectionColor = #colorLiteral(red: 0.6924785972, green: 0.6451357007, blue: 0.6242417097, alpha: 1)
        self.calendar.delegate = self
        self.calendar.dataSource = self
        
        
        self.calendar.select(apde.preSelectedDateForAddToMealPlan ?? Date(), scrollToDate: true)
        
        if let preSelectedDate = preSelectedDate {
            self.calendar.select(preSelectedDate, scrollToDate: true)
        }
        self.getMealPlanApiCall(date: Int64((self.calendar.selectedDate ?? Date()).timeIntervalSince1970))
    }
    
    @IBAction func btnCookingForTapped(_ sender: UIButton) {
        hapSoft()
        sender.isSelected = !sender.isSelected
        for (i,btn) in self.arrButtonsCookingFor.enumerated() {
            if btn == sender {
                self.arrButtonsRadioCookingFor[i].isSelected = !self.arrButtonsRadioCookingFor[i].isSelected
            }
        }
    }
    
    
}


extension AddToMealPlanVC : FSCalendarDelegateAppearance, FSCalendarDelegate , FSCalendarDataSource {
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        
        return [#colorLiteral(red: 0.6924785972, green: 0.6451357007, blue: 0.6242417097, alpha: 1)]
    }
    
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        for timestamp in self.arrIntTimestampDate {
            let dateWhichHaveMeals = Date.init(timeIntervalSince1970: TimeInterval(timestamp))
            if dateWhichHaveMeals.startOfDay == date.startOfDay {
                return 1
            }
        }
        
        return 0
    }
    
    func calendar(_ calendar: FSCalendar, labelOfDate label: UILabel, date: Date) -> UILabel? {
        label.font = enum_font.regular.font(15)
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        
        if self.isForUpdateMealType {
            
            BasicFunctions.showInfo(strError: "Sorry, you can't change date when updating meal type.")
            return false
            
        } else {
            
            if date.startOfDay == Date().startOfDay {
                return true
            }
            if date < Date() {
                 print("date1 is earlier than date2")
                return false
            }
            return true
        }
        
        
    }

    
        
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        hapSoft()

        apde.preSelectedDateForAddToMealPlan = nil
        let seconds = date.timeIntervalSince1970
        self.getMealPlanApiCall(date: Int64(seconds))
        
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        let month = Calendar.current.component(.month, from: calendar.currentPage)
        self.getMealDatesByMonthsApiCall(month: month)
    }
}


//MARK: - API CALLS
extension AddToMealPlanVC {
    
    func addToMealPlanApiCall(date:Int64, recipe_id:Int64, mealPlanTypes:String,is_saved_in_cookbook:Int) {
        
        apde.showLoader()
        
        addMealPlan(date: date, recipe_id: recipe_id, mealPlanTypes: mealPlanTypes, is_saved_in_cookbook: is_saved_in_cookbook) { (res) in
            switch res {
            case .success(message: let msg):
                apde.hideLoader()

                hapHeavy()

                let transition = CATransition()
                transition.duration = 0.5
                transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                transition.type = CATransitionType.push
                transition.subtype = CATransitionSubtype.fromLeft
                self.navigationController?.view.layer.add(transition, forKey: nil)
                self.navigationController?.popViewController(animated: true)
                
                Timer.after(0.5) {
                    BasicFunctions.showInfo(strError: msg)
                }
                
                break
            case .failed(message: let msg):
                apde.hideLoader()
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                apde.hideLoader()
                BasicFunctions.showError(strError: error)
                
                break
            }
            
            
        }
        
    }
    
    
    
    func getMealPlanApiCall(date: Int64) {

        apde.showLoader()
        
        for (i,btn) in self.arrButtonsCookingFor.enumerated() {
            btn.isSelected = false
            self.arrButtonsRadioCookingFor[i].isSelected = false
        }
        
        getMealPlan(date: date) { (res) in
            switch res {
            case .success(_, let dictData,_) :
                
                if let arrDict = dictData["mealplan_list"] as? Array<Dictionary<String,Any>> {
                    
                    self.arrMealPlanModel.removeAll()
                    
                    for dict in arrDict {
                        let jsonDict = JSON(dict)
                        
                        let is_saved_in_cookbook = jsonDict["is_saved_in_cookbook"].intValue
                        let meal_plan_id = jsonDict["meal_plan_id"].int64Value
                        let recipe_id = jsonDict["recipe_id"].int64Value
                        let recipe_name = jsonDict["recipe_name"].stringValue
                        let meal_plan_type = jsonDict["meal_plan_type"].int64Value
                        
                        let objMealPlanModel = MealPlanModel.init(is_saved_in_cookbook: is_saved_in_cookbook, meal_plan_id: meal_plan_id, recipe_id: recipe_id, recipe_name: recipe_name, meal_plan_type: meal_plan_type)
                        
                        self.arrMealPlanModel.append(objMealPlanModel)
                        
                    }
                    
                    for (i,btn) in self.arrButtonsCookingFor.enumerated() {
                        for (_,mealPlan) in self.arrMealPlanModel.enumerated() {
                            if btn.tag == mealPlan.meal_plan_type && self.recipe_id == mealPlan.recipe_id {
                                btn.isSelected = true
                                self.arrButtonsRadioCookingFor[i].isSelected = true
                            }
                        }
                    }
                    
                }
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                break
            }
            apde.hideLoader()
        }
        
    }
}

//MARK: - API CALLS
extension AddToMealPlanVC {
    
    func getMealDatesByMonthsApiCall(month:Int) {
        getMealDatesByMonths(month: month) { (res) in
            switch res {
            case .success(_, let dictData,_) :
                
                print(dictData)
                self.arrIntTimestampDate.removeAll()
                
                let jsonDict = JSON.init(dictData)
                
                if let arrDates = jsonDict["mealplan_dates"].array {
                    for d in arrDates {
                        let timestamp = d["date"].int64Value
                        self.arrIntTimestampDate.append(timestamp)
                    }
                }
                
                
                var datesToBeBold = [Date]()
                for timestamp in self.arrIntTimestampDate {
                    let dateWhichHaveMeals = Date.init(timeIntervalSince1970: TimeInterval(timestamp)).startOfDay
                    datesToBeBold.append(dateWhichHaveMeals)
                }
                
                self.calendar.reloadData()
                
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                break
            }
        }
    }

}
