//
//  ComingsoonClass.swift
//  Wentamashy
//
//  Created by Shahabuddin on 15/09/17.
//  Copyright © 2017 Shahabuddin. All rights reserved.
//

import UIKit

class ComingsoonClass: UIView
{

    @IBOutlet var viewDialog: UIView!
    @IBOutlet var btnOk: UIButton!
    
    @IBAction func btnOkAction(_ sender: UIButton)
    {
        self.removeFromSuperview()
    }

}
