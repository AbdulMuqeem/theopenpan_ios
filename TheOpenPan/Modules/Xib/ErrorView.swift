//
//  ErrorView.swift
//  Dujour
//
//  Created by Peerbits Ios on 22/12/16.
//  Copyright © 2016 Shahabuddin.Peerbits. All rights reserved.
//

import UIKit

class ErrorView: UIView
{
    @IBOutlet var errView: UIView!
    @IBOutlet var lblError: UILabel!
    @IBOutlet var lblCenterXConstraints: NSLayoutConstraint!  //0

}
