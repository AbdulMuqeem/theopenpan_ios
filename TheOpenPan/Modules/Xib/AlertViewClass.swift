//
//  AlertViewClass.swift
//  Wentamashy
//
//  Created by Shahabuddin on 23/08/17.
//  Copyright © 2017 Shahabuddin. All rights reserved.
//

import UIKit

class AlertViewClass: UIView
{
    @IBOutlet weak var viewDialog: UIView!
    
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    
    
    @IBAction func btnOkAction(_ sender: UIButton)
    {
        
    }
    @IBAction func btnCancelAction(_ sender: UIButton)
    {
        self.removeFromSuperview()
    }

}
