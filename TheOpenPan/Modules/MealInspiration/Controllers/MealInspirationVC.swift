//
//  MealInspirationVC.swift
//  TheOpenPan
//
//  Created by Pathan Mushrankhan on 15/04/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import WebKit

class MealInspirationVC: UIViewController {

    @IBOutlet weak var viewContainer        : UIView!
    @IBOutlet weak var viewBackBlur         : UIVisualEffectView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Meal Inspiration"
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        
        let myURLString = "https://theopenpan.sg/virtual-cooking-class-singapore/" //"https://theopenpan.sg/meal-inspiration/"
        let url = URL(string: myURLString)
        let request = URLRequest(url: url!)
        
        let webView = WKWebView(frame: self.view.frame)
        webView.navigationDelegate = self
        webView.load(request)
        
        self.viewContainer.mk_addFourSideConstraintsInParent(childView: webView)
    }
    

}

extension MealInspirationVC: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Started to load")
        viewBackBlur.mk_animateAlpha(sec: 0.2, alpha: 1.0)
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
        viewBackBlur.mk_animateAlpha(sec: 0.2, alpha: 0.0)
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
}
