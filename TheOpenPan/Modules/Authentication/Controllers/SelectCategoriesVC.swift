//
//  SelectCategoriesVC.swift
//  TheOpenPan
//
//  Created by Pathan Mushrankhan on 11/01/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import TagListView

class SelectCategoriesVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet var tblViewSelectCategories       : UITableView!
    @IBOutlet var btnDone                       : UIButton!
    
    //------------------------------------------
    //MARK: - Class Variables -
    var objMasterData                   : Master_data!
    var headerCell                      : UIView!
    var arrAllSelectedTags              = [TagView]()
//    var arrSlectedTagsForCountOnly = [Category_tag]()
    
    var dictTagsIds = [[String:Any]]()
    var isOnePotMeals = false
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        
    }
    
    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.setupNavigationBarCustom()
        setupTableView()
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.title = "Select Categories".localize()
        
        apde.getMasterDataFromServer { (objMasterData) in
            print(objMasterData)

            if kCurrentUser.user_id > 0 {
                self.getInterestedCategoriesApiCall(objMasterData: objMasterData)
            } else {
                if kCurrentUser.is_guest {
                    self.getInterestedCategoriesApiCall(objMasterData: objMasterData)
                } else {
                    self.objMasterData = objMasterData
                    self.tblViewSelectCategories.reloadData()
                    self.btnDone.isHidden = false
                }
            }
            
        }
        
        self.btnDone.mk_addTapHandler { (btn) in
            
            guard let arrCat = self.objMasterData.categories.allObjects as? [Recipe_category] else {
                return
            }
            
            // CuisineTypes, MealTypes, diet
//            self.getDictOfCategorySelection(arrCat: arrCat, index: 0)
//            self.getDictOfCategorySelection(arrCat: arrCat, index: 1)
//            self.getDictOfCategorySelection(arrCat: arrCat, index: 2)
            
            
            self.dictTagsIds.removeAll()
            for i in 0...self.objMasterData.categories.allObjects.count {
                self.getDictOfCategorySelection(arrCat: arrCat, index: i)
            }
            
            
            // Time Options
            let arrSelectedTimeOptions = (self.objMasterData.time_option.allObjects as? [Category_tag])?.filter({ (catTag) -> Bool in
                catTag.is_selected
            })
            
//            self.arrSlectedTagsForCountOnly.append(contentsOf: arrSelectedTimeOptions ?? [Category_tag]())
            
            let selectedTimeOptionsIds = arrSelectedTimeOptions?.map { (objTimeOption) -> String in
                "\(objTimeOption.tag_id)"
            }
            
            let finalStringTimeOptionsIds = selectedTimeOptionsIds?.joined(separator: ",")
            
            var totalCountTagsSelected = self.arrAllSelectedTags.count
            
            if self.isOnePotMeals {
                totalCountTagsSelected += 1
            }
            print(totalCountTagsSelected)
            
            
        
            
//            if totalCountTagsSelected < 6 {
//                self.headerCell?.shake()
//            } else {
                // final selection of tags
                self.setInterestedCategoriesApiCall(time_id: finalStringTimeOptionsIds ?? "")
//            }
            
            
            
        }
        
        
        self.btnDone.isHidden = true
        
    }
    
    

    
    
    func getDictOfCategorySelection(arrCat:[Recipe_category],index:Int) {
        
        let foundArrCat = arrCat.filter { (objCat) -> Bool in
            objCat.displayIndex == index
        }
        
        if let foundCuisineMealTypes = foundArrCat.first {
            
            let arrTags = foundCuisineMealTypes.tags.allObjects as! [Category_tag]
            let selectedCuisineMealTypes = arrTags.filter { (catTag) -> Bool in
                catTag.is_selected
            }
//            arrSlectedTagsForCountOnly.append(contentsOf: selectedCuisineMealTypes)
            let selectedCatIds = selectedCuisineMealTypes.map { (objCatTag) -> String in
                "\(objCatTag.tag_id)"
            }
            let finalString = selectedCatIds.joined(separator: ",")
            
            self.dictTagsIds.append(["category_id":foundCuisineMealTypes.category_id,
                                     "selected_tags_id":finalString])
        }
    }
    
    
    func setupNavigationBarCustom() {
        
        self.navigationController?.view.backgroundColor = #colorLiteral(red: 0.631372549, green: 0.7764705882, blue: 0.7254901961, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font : UIFont.applyCustomBold(fontSize: 20) , NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)] as [NSAttributedString.Key : Any]
        
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.largeTitleTextAttributes = [ NSAttributedString.Key.font : UIFont.applyCustomBold(fontSize: 30) , NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)] as [NSAttributedString.Key : Any]
            self.navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tblViewSelectCategories.layoutIfNeeded()
        self.tblViewSelectCategories.layoutSubviews()
    }
    
}



//------------------------------------------
//MARK: - Table View Setup Methods -
extension SelectCategoriesVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblViewSelectCategories.delegate = self
        self.tblViewSelectCategories.dataSource = self
        self.tblViewSelectCategories.estimatedRowHeight = 100
        if #available(iOS 11.0, *) {
            self.tblViewSelectCategories.contentInsetAdjustmentBehavior = .never
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if objMasterData != nil {
            return self.objMasterData.categories.allObjects.count + 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        self.headerCell = tableView.dequeueReusableCell(withIdentifier: "CellHeader")
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellSelectCategories = tableView.dequeueReusableCell(withIdentifier: "CellSelectCategories", for: indexPath) as! CellSelectCategories
        
        cellSelectCategories.handleCell(vc:self, objMasterData: self.objMasterData, indexPath: indexPath)
        
        return cellSelectCategories
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.layoutIfNeeded()
    }
    
}





//MARK: - API CALLS
extension SelectCategoriesVC {
    
    func setInterestedCategoriesApiCall(time_id: String) {
        
        
        
        guard let theJSONData = try? JSONSerialization.data(
            withJSONObject: self.dictTagsIds,
            options: []) else {
                return
        }
        
        guard let theJSONText = String(data: theJSONData, encoding: .utf8) else {
            return
        }
        print("JSON string = \(theJSONText)")
        
        let count_timeOption = time_id.components(separatedBy: ",").count
        var countAllDicts = 0
        for d in self.dictTagsIds {
            if let str = d["selected_tags_id"] as? String {
                if str != "" {
                    countAllDicts = countAllDicts + str.components(separatedBy: ",").count
                }
            }
        }
        var totalCount = count_timeOption + countAllDicts
        if self.isOnePotMeals {
            totalCount = totalCount + 1
        }
        print(totalCount)
        if totalCount < 6 {
            print(totalCount)
            BasicFunctions.showInfo(strError: "Please select at-least 6 categories.")
            return
        }
        
        self.btnDone.loadingIndicator(show: true)
        
        setInterestedCategories(tag_ids: theJSONText, time_id: time_id, is_one_pot: self.isOnePotMeals.mk_intValue) { (res) in
            
            switch res {
            case .success(message: let msg) :
                apde.goHomePage(transition: true)
                hapHeavy()
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                break
            }
            
            self.btnDone.loadingIndicator(false)
        }
        
        
    }
    
    
    func getInterestedCategoriesApiCall(objMasterData:Master_data?) {
        
        getInterestedCategories { (res) in
            
            switch res {
            case .success(dictData: let dictData, message: let msg) :
                
                
                // Set Interested Category tags selected
                guard let arrDictCategories = dictData["categories"] as? [[String:Any]] else {
                    return
                }
                guard let arrInterestedCategories = Recipe_category.mr_import_(from_Array: arrDictCategories, in: app_default_context) as? [Recipe_category] else {
                    return
                }
                
                let arrMasterCategories = objMasterData?.categories.allObjects as! [Recipe_category]
                
                for interestedCategory in arrInterestedCategories {
                    let arrFoundCat = arrMasterCategories.filter { (recipeCat) -> Bool in
                        recipeCat.category_id == interestedCategory.category_id
                    }
                    if let firstFoundCat = arrFoundCat.first {
                        
                        for interestedTag in (interestedCategory.tags.allObjects as? [Category_tag]) ?? [Category_tag]() {
                            let foundTag = (firstFoundCat.tags.allObjects as? [Category_tag])?.filter { (catTag) -> Bool in
                                catTag.tag_id == interestedTag.tag_id
                            }
                            foundTag?.first?.is_selected = true
                        }
                        
                    }
                }
                
                
                // Set Interested Time Option tags selected
                guard let arrDictTimeOption = dictData["time_option"] as? [[String:Any]] else {
                    return
                }
                
                guard let arrTimeOption = Category_tag.mr_import_(from_Array: arrDictTimeOption, in: app_default_context) as? [Category_tag] else {
                    return
                }
                
                let arrMasterTimeOptions = objMasterData?.time_option.allObjects as! [Category_tag]
                
                for objTimeOption in arrTimeOption {
                    let foundTimeOption = arrMasterTimeOptions.filter { (catTag) -> Bool in
                        catTag.tag_id == objTimeOption.tag_id
                    }
                    foundTimeOption.first?.is_selected = true
                }
                
                // Set Interested is_one_pot selected
                guard let is_one_pot = dictData["is_one_pot"] as? Int else {
                    return
                }
                
                self.isOnePotMeals = is_one_pot.mk_boolValue
                self.objMasterData = objMasterData
                self.tblViewSelectCategories.reloadData()
                self.btnDone.isHidden = false
                
                break
            case .error(error: let err) :
                BasicFunctions.showError(strError: err)
                break
            case .failed(message: let msg) :
                BasicFunctions.showError(strError: msg)
                break
            }
            
            
        }
    }
}
