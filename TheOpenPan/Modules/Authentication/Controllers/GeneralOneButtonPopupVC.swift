//
//  GeneralOneButtonPopupVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/10/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class GeneralOneButtonPopupVC: UIViewController {

    @IBOutlet weak var viewBackAlpha : UIView!
    @IBOutlet weak var viewCenter : UIView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var btnOk : UIButton!
    @IBOutlet weak var cntrWidth : NSLayoutConstraint!
    
    var strBtnTitle = "OKAY"
    var strTitle = ""
    var strDesc = ""
    typealias Block = ()->()
    var compBlock:Block!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupLayout()
    }
    
    func setupLayout() {
        
        if ssw <= 320 {
            self.cntrWidth.constant = 290
        }
        
        self.viewBackAlpha.alpha = 0
        
        self.lblDesc.text = strDesc
        self.lblTitle.text = strTitle
        self.btnOk.setTitle(self.strBtnTitle, for: .normal)
        
        
        self.btnOk.mk_addTapHandler { (btn) in
            hapSoft()
            self.dismiss()
        }
        self.viewBackAlpha.addTapGesture { (gesture) in
            hapSoft()
            self.dismiss()
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.showBackView()
    }

    func showBackView() {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 0.8
        }) { (s) in
            
        }
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 0
        }) { (s) in
            self.dismiss(animated: true) {
                self.compBlock()
            }
            
        }
    }


}
