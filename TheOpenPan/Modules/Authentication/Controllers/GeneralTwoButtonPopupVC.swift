//
//  GeneralTwoButtonPopupVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/10/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class GeneralTwoButtonPopupVC: UIViewController {

    @IBOutlet weak var viewBackAlpha        : UIView!
    @IBOutlet weak var viewCenter           : UIView!
    @IBOutlet weak var lblTitle             : UILabel!
    @IBOutlet weak var lblDesc              : UILabel!
    @IBOutlet weak var btnLeft              : UIButton!
    @IBOutlet weak var btnRight             : UIButton!
    @IBOutlet weak var cntrWidth            : NSLayoutConstraint!
    
    var strBtnTitleLeft = "CANCEL"
    var strBtnTitleRight = "OKAY"
    var strTitle = ""
    var strDesc = ""
    typealias Block = (Bool)->()
    var compBlock:Block!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupLayout()
    }
    
    func setupLayout() {
        
        if ssw <= 320 {
            self.cntrWidth.constant = 290
        }
        
        self.viewBackAlpha.alpha = 0
        
        self.btnLeft.mk_addTapHandler { (btn) in
            hapSoft()
            self.dismiss(false)
        }
        self.btnRight.mk_addTapHandler { (btn) in
            hapSoft()
            self.dismiss(true)
        }
        self.viewBackAlpha.addTapGesture { (gesture) in
            hapSoft()
            self.dismiss(false)
        }
        
        
        self.btnLeft.setTitle(self.strBtnTitleLeft, for: .normal)
        self.btnRight.setTitle(self.strBtnTitleRight, for: .normal)
        
        self.lblDesc.text = strDesc
        self.lblTitle.text = strTitle
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.showBackView()
    }

    func showBackView() {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 1
        }) { (s) in
            
        }
    }
    
    func dismiss(_ boolValue:Bool) {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 0
        }) { (s) in
            self.dismiss(animated: true) {
                self.compBlock(boolValue)
            }
            
        }
    }


}
