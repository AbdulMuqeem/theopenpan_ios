//
//  WelcomeVC.swift
//  SempreAlertaCitizen
//
//  Created by Dipen on 30/07/18.
//  Copyright © 2018 peerbits. All rights reserved.
//

import Foundation
import UIKit
import GoogleSignIn
import SwiftyJSON
import AuthenticationServices
import SwiftKeychainWrapper

class WelcomeVC : BaseViewController
{
    
    //------------------------------------------
    //MARK: - Outlets -
    
    @IBOutlet var btnEmailLogin         : UIButton!
    @IBOutlet var btnFacebook           : UIButton!
    @IBOutlet var btnGoogle             : UIButton!
    @IBOutlet var btnApple              : UIButton!
    @IBOutlet var btnDismiss            : UIButton!
    @IBOutlet var btnSkip               : UIButton!
    @IBOutlet var stackViewButtons      : UIStackView!
    @IBOutlet var viewAppleButtonContainer      : UIView!
    @IBOutlet var imgViewAppleLogo      : UIImageView!
    @IBOutlet var lblAppleLogin         : UILabel!
    
    
    //------------------------------------------
    //MARK: - Class Variables -
    var isDismissHide       = true
    var strMessage          = ""
    
    //------------------------------------------
    //MARK: - Memory Management -
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    deinit
    {
        
    }
    
    //------------------------------------------
    //MARK: - Custom Methods -
    
    @objc func setupLayout() {
        
        if #available(iOS 13.0, *) {
            self.btnApple.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)
        } else {
            if let firstView = self.stackViewButtons.arrangedSubviews.first {
                self.stackViewButtons.removeArrangedSubview(firstView)
            }
            self.imgViewAppleLogo.isHidden = true
            self.lblAppleLogin.isHidden = true
        }
        
        self.btnDismiss.isHidden = self.isDismissHide
        
        self.btnDismiss.mk_addTapHandler { (btn) in
            self.dismiss(animated: true, completion: nil)
        }
        
        self.btnSkip.mk_addTapHandler { (btn) in
            hapHeavy()
            
            if self.isDismissHide == false {
                BasicFunctions.showInfo(strError: self.strMessage)
                return
            }
            
            let u = User.init(info: JSON.init(["is_guest" : true.mk_intValue]))
            kCurrentUser = u
            kCurrentUser.saveToDefault()
            apde.goToSelectCategoriesList(transition: true)
        }
        
        self.btnFacebook.mk_addTapHandler { (btn) in
            hapSoft()
            Facebook.LogInWithFacebook(viewController: self, handler: { (response) in
                print(response.email)
                self.facebookLogin(fbvar: response)
            }) { (err) in
                print(err.localizedDescription)
            }
        }
        
        self.btnGoogle.mk_addTapHandler { (btn) in
            
            hapSoft()
            let arrayScopes = ["https://www.googleapis.com/auth/plus.login", "https://www.googleapis.com/auth/plus.me"]
            
            GIDSignIn.sharedInstance()?.presentingViewController = self
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().scopes = arrayScopes
            GIDSignIn.sharedInstance().clientID = GOOGLE_CLIENT_ID
            GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
            GIDSignIn.sharedInstance().signIn()
            
            // Automatically sign in the user.
            //            GIDSignIn.sharedInstance()?.restorePreviousSignIn()
            
        }
        
        
        self.btnEmailLogin.mk_addTapHandler { (btn) in
            hapSoft()
            let objLoginVC = apde.getController(vc_name: "LoginVC", sb: .Authentication) as! LoginVC
            UIViewController.CVC()?.navigationController?.pushViewController(objLoginVC, animated: true)
        }
        
//        if #available(iOS 13.0, *) {
//            self.setUpSignInAppleButton()
//        } else {
//            // or use some work around
//        }
        
    }
    
    //------------------------------------------
    //MARK: - API Methods -
    
    //------------------------------------------
    //MARK: - Delegate Methods -
    
    
    //------------------------------------------
    //MARK: - Action Methods -
    @IBAction func btnLetsStart(_ sender: Any) {
        
        let resultVC = Utilities.viewController(name:"LoginVC", onStoryboard: "Authentication") as! LoginVC
        self.navigationController!.pushViewController(resultVC, animated: true)
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadViewIfNeeded()
        // Do any additional setup after loading the view.
        self.setupLayout()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.changeStyle(.default)
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
        
    }
    
    //------------------------------------------
    
}

//Google Login
extension WelcomeVC : GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            print(error as Any)
        } else {
            
            print("User Name")
            print(user.profile.name as Any)
            print("User Email")
            print(user.profile.email as Any)
            print("User ID")
            print(user.userID as Any)
            print(user.profile.imageURL(withDimension: 200).absoluteString)
            
            
            logInSocial(username: user.profile.name, email: user.profile.email, profile_image: user.profile.imageURL(withDimension: 200).absoluteString, deviceid: AppInstance.deviceToken, social_type: .google, social_id: user.userID) { (authRes) in
                
                
                switch authRes
                {
                case .success(let userObj, let message):
                    
                    // AppInstance.showMessages(message: message)
                    
                    kCurrentUser = userObj
                    
                    /*if message != ""
                     {
                     AppInstance.showMessages(message: message)
                     }*/
                    
                    kCurrentUser.saveToDefault()
                    
                    
                    getInterestedCategories { (commonRes) in

                        switch commonRes
                        {
                        case .success(let res, let message):

                          //  AppInstance.showMessages(message: message)

                            PbCircleDotLoader.sharedInstance.stopAnimation()

                            let jsonRes = JSON.init(res)


                            guard let arrCategories = jsonRes["categories"].arrayObject else {

                                return
                            }

                            guard let arrTimeOption = jsonRes["time_option"].arrayObject else {

                                return
                            }

                            guard let isOnePot = jsonRes["is_one_pot"].int else {

                                return
                            }

                            
                            if arrCategories.count == 0 && arrTimeOption.count == 0 && !isOnePot.mk_boolValue {
                                apde.goToSelectCategoriesList(transition: true)
                            } else {
                                AppInstance.goHomePage(transition: true)
                            }
                            
                            
                            
                            hapError()


                            //                PbCircleDotLoader.sharedInstance.stopAnimation()
                            break
                        case .failed(let message):
                            AppInstance.showMessages(message: message)

                        case .error(let error):
                            AppInstance.showMessages(message: error)

                        }

                    }
                    
                    
                    //                PbCircleDotLoader.sharedInstance.stopAnimation()
                    
                case .failed(let message):
                    AppInstance.showMessages(message: message)
                    
                case .error(let error):
                    AppInstance.showMessages(message: error)
                    
                }
                
            }
            
            
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    
    
    
    
}



//Facebook Login
extension WelcomeVC {
    
    func facebookLogin(fbvar:fbVariable) {
        
        logInSocial(username: fbvar.userName, email: fbvar.email, profile_image: fbvar.profic_pic  , deviceid: AppInstance.deviceToken, social_type: .facebook, social_id: fbvar.id) { (authRes) in
            
            
            switch authRes
            {
            case .success(let userObj, let message):
                
              //  AppInstance.showMessages(message: message)
                
                kCurrentUser = userObj
                
                /*if message != ""
                 {
                 AppInstance.showMessages(message: message)
                 }*/
                
                kCurrentUser.saveToDefault()
                
                
                getInterestedCategories { (commonRes) in

                    switch commonRes
                    {
                    case .success(let res, let message):

                       // AppInstance.showMessages(message: message)

                        PbCircleDotLoader.sharedInstance.stopAnimation()

                        let jsonRes = JSON.init(res)


                        guard let arrCategories = jsonRes["categories"].arrayObject else {

                            return
                        }

                        guard let arrTimeOption = jsonRes["time_option"].arrayObject else {

                            return
                        }

                        guard let isOnePot = jsonRes["is_one_pot"].int else {

                            return
                        }

                        
                        if arrCategories.count == 0 && arrTimeOption.count == 0 && !isOnePot.mk_boolValue {
                            apde.goToSelectCategoriesList(transition: true)
                        } else {
                            AppInstance.goHomePage(transition: true)
                        }
                        hapError()


                        //                PbCircleDotLoader.sharedInstance.stopAnimation()
                        break
                    case .failed(let message):
                        AppInstance.showMessages(message: message)

                    case .error(let error):
                        AppInstance.showMessages(message: error)

                    }

                }
                
                
                //                PbCircleDotLoader.sharedInstance.stopAnimation()
                
            case .failed(let message):
                AppInstance.showMessages(message: message)
                
            case .error(let error):
                AppInstance.showMessages(message: error)
                
            }
            
        }
    }
    
    
}

@available(iOS 13.0, *)
extension WelcomeVC : ASAuthorizationControllerDelegate {
    
//    func setUpSignInAppleButton() {
//      let authorizationButton = ASAuthorizationAppleIDButton()
//      authorizationButton.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)
//      authorizationButton.cornerRadius = 10
//      //Add button on some view or stack
////        self.viewAppleButtonContainer.addSubview(authorizationButton)
//        self.viewAppleButtonContainer.mk_addFourSideConstraintsInParent(childView: authorizationButton)
//
//    }
    
    @objc func handleAppleIdRequest() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            
            let userIdentifier = appleIDCredential.user
            
            if let fullName = appleIDCredential.fullName {
                if let familyName = fullName.familyName {
                    if let givenName = fullName.givenName {
                        let saveSuccessful: Bool = KeychainWrapper.standard.set("\(givenName) \(familyName)", forKey: "apple_username")
                        print(saveSuccessful)
                    }
                }
            }
            if let email = appleIDCredential.email {
                let saveSuccessful: Bool = KeychainWrapper.standard.set(email, forKey: "apple_email")
                print(saveSuccessful)
            }
            
            var email = ""
            var username = ""
            
            if let email_ = KeychainWrapper.standard.string(forKey: "apple_email") {
                email = email_
                if email.contains("privaterelay.appleid.com") {
                    email = ""
                }
            }
            if let username_ = KeychainWrapper.standard.string(forKey: "apple_username") {
                username = username_
            }
            
            self.doAppleLogin(social_id: userIdentifier,username: username,email: email)
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        BasicFunctions.showError(strError: error.localizedDescription)
    }
    
    func doAppleLogin(social_id: String,username: String, email: String) {
        
        logInSocial(username: username, email: email, profile_image: ""  , deviceid: AppInstance.deviceToken, social_type: .apple, social_id: social_id) { (authRes) in
            
            switch authRes {
                
            case .success(let userObj, let message):
                
               
               // AppInstance.showMessages(message: message)
                
                kCurrentUser = userObj
                
                /*if message != ""
                 {
                 AppInstance.showMessages(message: message)
                 }*/
                
                kCurrentUser.saveToDefault()
                
                getInterestedCategories { (commonRes) in

                    switch commonRes
                    {
                    case .success(let res, let message):

                        //AppInstance.showMessages(message: message)

                        PbCircleDotLoader.sharedInstance.stopAnimation()

                        let jsonRes = JSON.init(res)


                        guard let arrCategories = jsonRes["categories"].arrayObject else {
                            return
                        }

                        guard let arrTimeOption = jsonRes["time_option"].arrayObject else {
                            return
                        }

                        guard let isOnePot = jsonRes["is_one_pot"].int else {
                            return
                        }

                        
                        if arrCategories.count == 0 && arrTimeOption.count == 0 && !isOnePot.mk_boolValue {
                            apde.goToSelectCategoriesList(transition: true)
                        } else {
                            AppInstance.goHomePage(transition: true)
                        }
                        hapError()

                        break
                    case .failed(let message):
                        AppInstance.showMessages(message: message)

                    case .error(let error):
                        AppInstance.showMessages(message: error)

                    }
                }
                
            case .failed(let message):
                AppInstance.showMessages(message: message)
                
            case .error(let error):
                AppInstance.showMessages(message: error)
                
            }
            
        }
    }
}
