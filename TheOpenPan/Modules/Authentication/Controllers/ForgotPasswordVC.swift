//
//  ForgotPasswordVC.swift
//  TheOpenPan
//
//  Created by Pathan Mushrankhan on 11/01/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet weak var tblView: UITableView!
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        setupTableView()
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupLayout()
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
}


//------------------------------------------
//MARK: - Table View Setup Methods -
extension ForgotPasswordVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
        if (ssh > 580) {
            self.tblView.isScrollEnabled = false
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if ssh > 500 {
            return ssh
        }
        return 500
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellForgotPass = tableView.dequeueReusableCell(withIdentifier: "CellForgotPass", for: indexPath) as! CellForgotPass
        
        cellForgotPass.blockValidated = {
            print("Validated")
            
            self.forgotPasswordApiCall(cellForgotPass: cellForgotPass)
            
        }
        
        setFont(arrViews: [cellForgotPass.txtEmail], weight: .regular, size: 20)
        
//        cellForgotPass.txtEmail.text = "Mushrankhan@gmail.com"
        
        return cellForgotPass
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

//MARK: - API CALLS
extension ForgotPasswordVC {
    func forgotPasswordApiCall(cellForgotPass:CellForgotPass) {
        
        cellForgotPass.btnSend.loadingIndicator(show: true)
        forgotPassword(email: cellForgotPass.txtEmail.text ?? "") { (res) in
            
            switch res {
            case .success(message: let msg):
                
                BasicFunctions.showPopupOneButton(title: "Forgot Password".localize(), desc: msg) {
                    hapHeavy()
                    UIViewController.CVC()?.navigationController?.popViewController(animated: true)
                }
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error.localizedDescription)
                break
            }
            
            cellForgotPass.btnSend.loadingIndicator(show: false)
        }
    }
}
