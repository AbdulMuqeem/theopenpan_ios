//
//  LoginVC.swift
//  Trade Master
//
//  Created by Dipen on 21/11/18.
//  Copyright © 2018 peerbits. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class LoginVC : UIViewController
{
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet var tblViewLogin: UITableView!
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    deinit
    {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarView?.backgroundColor = .clear
    }
    
    //------------------------------------------
    //MARK: - API Methods -
    
    //------------------------------------------
    //MARK: - Delegate Methods -
    
    //------------------------------------------
    //MARK: - Action Methods -
    

    @IBAction func btnForgotPinAction(_ sender: UIButton)
    {
        /*let resultVC = Utilities.viewController(name:"ForgotPasswordViewController", onStoryboard: StoryboardNames.Authentication.rawValue) as! ForgotPasswordViewController
        self.navigationController!.pushViewController(resultVC, animated: true)*/
    }
    @IBAction func btnRegister(_ sender: UIButton)
    {
        
    }
    @IBAction func btnFacebook(_ sender: UIButton)
    {
        
    }
    @IBAction func btnTwitter(_ sender: UIButton)
    {
        
    }
    @IBAction func btnGoogle(_ sender: Any)
    {
        
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupTableView()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
}




extension LoginVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblViewLogin.delegate = self
        self.tblViewLogin.dataSource = self
        if #available(iOS 11.0, *) {
            self.tblViewLogin.contentInsetAdjustmentBehavior = .never
        }
        if (ssh > 580) {
            self.tblViewLogin.isScrollEnabled = false
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if ssh > 580 {
            return ssh
        }
        return 580
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellLogin = tableView.dequeueReusableCell(withIdentifier: "CellLogin", for: indexPath) as! CellLogin
        
        cellLogin.blockValidated = {
            print("Validated")
            self.loginApiCall(email: cellLogin.txtEmail.text ?? "", password: cellLogin.txtPassword.text ?? "", btn: cellLogin.btnLogin)
        }
//        cellLogin.txtEmail.text = "test111@allmtr.com"
//        cellLogin.txtPassword.text = "123456"
        
        setFont(arrViews: [cellLogin.txtEmail,cellLogin.txtPassword], weight: .regular, size: 20)
        
        return cellLogin
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


//MARK: API CALLS
extension LoginVC {
    
    func loginApiCall(email: String,password: String, btn:UIButton) {
        
        btn.loadingIndicator(show: true)
        self.view.endEditing(true)
        
        logIn(deviceid: apde.deviceToken, email: email, password: password) { (authRes) in
            
            switch authRes
            {
            case .success(let userObj, let message):
                
             //   AppInstance.showMessages(message: message)
                
                if userObj.email_verified {
                    hapHeavy()
                    kCurrentUser = userObj
                    kCurrentUser.saveToDefault()
//                    apde.goToSelectCategoriesList(transition: true)
                    
                    
                    
                    getInterestedCategories { (commonRes) in

                        switch commonRes
                        {
                        case .success(let res, let message):

                         //   AppInstance.showMessages(message: message)

                            PbCircleDotLoader.sharedInstance.stopAnimation()

                            let jsonRes = JSON.init(res)


                            guard let arrCategories = jsonRes["categories"].arrayObject else {

                                return
                            }

                            guard let arrTimeOption = jsonRes["time_option"].arrayObject else {

                                return
                            }

                            guard let isOnePot = jsonRes["is_one_pot"].int else {

                                return
                            }

                            
                            if arrCategories.count == 0 && arrTimeOption.count == 0 && !isOnePot.mk_boolValue {
                                apde.goToSelectCategoriesList(transition: true)
                            } else {
                                AppInstance.goHomePage(transition: true)
                            }
                            hapError()


                            PbCircleDotLoader.sharedInstance.stopAnimation()
                            break
                        case .failed(let message):
                            AppInstance.showMessages(message: message)

                        case .error(let error):
                            AppInstance.showMessages(message: error)

                        }

                    }
                    
                    
                } else {
                    hapError()

                    BasicFunctions.showPopupTwoButton(title: "Verification".localize(), desc: "Please verify your email address first.".localize(), btnTitleLeft: "OKAY".localize(), btnTitleRight: "RESEND LINK".localize(), baseVC: nil) { (boolValue) in
                        hapHeavy()
                        if boolValue {
                            
                            resendVerificationLink(email: email) { (res) in
                                
                                switch res {
                                case .success(message: let msg):
                                    BasicFunctions.showInfo(strError: msg)
                                    break
                                case .failed(message: let msg):
                                    BasicFunctions.showError(strError: msg)
                                    break
                                case .error(error: let error):
                                    BasicFunctions.showError(strError: error.localizedDescription)
                                    break
                                }
                                
                            }
                            
                        } else {
                            
                        }
                    }
                }
                
            case .failed(let message):
                BasicFunctions.showError(strError: message)
                
            case .error(let error):
                BasicFunctions.showError(strError: error)
                
            }
            
            btn.loadingIndicator(show: false)
        }
    }
}
