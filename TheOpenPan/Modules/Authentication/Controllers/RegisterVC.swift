//
//  RegisterVC.swift
//  Trade Master
//
//  Created by Dipen on 21/11/18.
//  Copyright © 2018 peerbits. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField

class RegisterVC : BaseViewController
{
    
    //------------------------------------------
    //MARK: - Outlets -

    @IBOutlet var tblViewRegister: UITableView!
    
    
    
//    @IBOutlet var txtFullName: FloatLabelTextField!
//    @IBOutlet var txtEmail: FloatLabelTextField!
//    @IBOutlet var txtPassword: FloatLabelTextField!
//    @IBOutlet var txtCountry: FloatLabelTextField!
    
    
    
    @IBOutlet var btnCountry: UIButton!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnRegister: UIButton!
    @IBOutlet var btnFacebook: UIButton!
    @IBOutlet var btnTwitter: UIButton!
    @IBOutlet var btnGoogle: UIButton!
    
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    //------------------------------------------
    //MARK: - Memory Management -
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    deinit
    {
        
    }
    
    //------------------------------------------
    //MARK: - Custom Methods -
    
    @objc func setupLayout()
    {
        self.setupTableView()
    }
    
    //------------------------------------------
    //MARK: - API Methods -
    
    //------------------------------------------
    //MARK: - Delegate Methods -
    
    //------------------------------------------
    //MARK: - Action Methods -
    
    @IBAction func btnCountryPickerAction(_ sender: UIButton)
    {
        
    }
    @IBAction func btnRegister(_ sender: UIButton)
    {
        
    }
    @IBAction func btnLogin(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnFacebook(_ sender: UIButton)
    {
        
    }
    @IBAction func btnTwitter(_ sender: UIButton)
    {
        
    }
    @IBAction func btnGoogle(_ sender: Any)
    {
        
    }
    
    
    //---------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.setupLayout()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
    }
    
}





extension RegisterVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblViewRegister.delegate = self
        self.tblViewRegister.dataSource = self
        if #available(iOS 11.0, *) {
            self.tblViewRegister.contentInsetAdjustmentBehavior = .never
        }
        if (ssh >= 736) {
            self.tblViewRegister.isScrollEnabled = false
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if ssh > 736 {
            return ssh
        }
        return 736
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellRegister = tableView.dequeueReusableCell(withIdentifier: "CellRegister", for: indexPath) as! CellRegister
        
        cellRegister.blockValidated = {
            print("Validated")
            
            cellRegister.btnRegister.loadingIndicator(show: true)
            self.signUpApiCall(username: cellRegister.txtUsername.text ?? "", email: cellRegister.txtEmail.text ?? "", password: cellRegister.txtPassword.text ?? "", btn: cellRegister.btnRegister, cell:cellRegister)
            
        }
        
        setFont(arrViews: [cellRegister.txtUsername,cellRegister.txtEmail,cellRegister.txtPassword,cellRegister.txtConfirmPassword], weight: .medium, size: 19)
        
//        cellRegister.txtUsername.text = "Mushrankhan"
//        cellRegister.txtEmail.text = "Mushrankhan@gmail.com"
//        cellRegister.txtPassword.text = "123456"
//        cellRegister.txtConfirmPassword.text = "123456"
        
        return cellRegister
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

//MARK: API CALLS
extension RegisterVC {
    func signUpApiCall(username: String,email: String,password: String, btn:UIButton, cell: CellRegister) {
        
        signUp(deviceid: apde.deviceToken, username: username, email: email, password: password) { (authRes) in
            switch authRes {
            case .success(let user, _) :
                print(user)
                
                let strDesc = "This action requires email verification. Please check your inbox and follow the instructions. Email sent to : \(user.email ?? "")"
                BasicFunctions.showPopupOneButton(title: "Verification".localize(), desc: strDesc) {
                    hapHeavy()
                    cell.btnLoginTapped()
                }
                
                break
            case .error(let error) :
                BasicFunctions.showError(strError: error)
                
                break
            case .failed(let message) :
                BasicFunctions.showError(strError: message)
                break
            }
            
            btn.loadingIndicator(show: false)
        }
    }
}
