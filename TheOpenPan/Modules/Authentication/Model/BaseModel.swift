//
//  BaseModel.swift
//

import Foundation
import SwiftyJSON

class BaseModel: NSObject {
    
    var identifier: String?
    
    override init() {
        super.init()
    }
    
    init(info: JSON) {
        super.init()
        updateInfo(info)
    }
    
    
    func updateInfo(_ info: JSON) {
        
    }
    
    var pushsetting: NSMutableDictionary?
}
