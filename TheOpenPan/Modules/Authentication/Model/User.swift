//
//  User Object.swift
//

import Foundation
import SwiftyJSON

private var _sharedUser = User()
var kCurrentUser = User.sharedInstance
var didSawIntroScreen: Bool = false

enum LoginType: String
{
    case None = "none"
    case Mobile = "M" //"mobile"
    case Email = "E" //"email"
    case Guest = "guest"
}

class User: BaseModel
{
    let userDefaultKey = "user_information"
    let intrestInfoKey = "intrest_info"
    
    var tokendata                       : String = ""
    var user_id                         : Int = 0
    var username                        : String?
    var email                           : String?
    var email_verified                  : Bool = false
    var is_notification                 : Bool = false
    var is_screen_lock                  : Bool = false
    var is_social                       : Bool = false
    var is_guest                        : Bool = false
    var profile_image                   : String?
    var no_of_published_recipes         : Int = 0
    

    ///Local Variable
    //var is_current_corporate: Bool = false
    //var ObjModelHome = ModelHome()
    
    override init()
    {
        super.init()
    }
    override init(info: JSON)
    {
        super.init(info: info)
    }
    class var sharedInstance: User
    {
        if _sharedUser.identifier == nil
        {
            _sharedUser.loadFromDefault()
        }
        return _sharedUser
    }
    
    override func updateInfo(_ info: JSON)
    {
        user_id                     = info["user_id"].intValue
        username                    = info["username"].string ?? ""
        email                       = info["email"].string ?? ""
        email_verified              = info["email_verified"].intValue.mk_boolValue
        is_notification             = info["is_notification"].intValue.mk_boolValue
        is_screen_lock              = info["is_screen_lock"].intValue.mk_boolValue
        is_social                   = info["is_social"].intValue.mk_boolValue
        profile_image               = info["profile_image"].string ?? ""
        no_of_published_recipes     = info["no_of_published_recipes"].intValue
        is_guest                    = info["is_guest"].intValue.mk_boolValue
        
        updateScreenLock()
        //is_current_corporate = is_corporate_user ?? false
    }
    
    func updateScreenLock() {
        UIApplication.shared.isIdleTimerDisabled = is_screen_lock
    }
    
    func loadFromDefault()
    {
        let userDefault = UserDefaults.standard
        if let dicInfo = userDefault.object(forKey: userDefaultKey) as? [String: AnyObject] {
            update(dicInfo)
        }
    }
    
    // MARK - Loading from user default
    func update(_ dictionaryInfo: [String: AnyObject])
    {
        func boolFromDictionary(_ dicInfo: [String: AnyObject], key: String) -> Bool
        {
            if let optionalValue = dicInfo[key] as? Bool
            {
                return optionalValue
            }
            else
            {
                return false
            }
        }
        
        user_id                     = dictionaryInfo["user_id"] as? Int ?? 0
        username                    = dictionaryInfo["username"] as? String ?? ""
        email                       = dictionaryInfo["email"] as? String ?? ""
        email_verified              = dictionaryInfo["email_verified"]?.intValue.mk_boolValue ?? false
        is_notification             = dictionaryInfo["is_notification"]?.intValue.mk_boolValue ?? false
        is_screen_lock              = dictionaryInfo["is_screen_lock"]?.intValue.mk_boolValue ?? false
        is_social                   = dictionaryInfo["is_social"]?.intValue.mk_boolValue ?? false
        profile_image               = dictionaryInfo["profile_image"] as? String ?? ""
        no_of_published_recipes     = dictionaryInfo["no_of_published_recipes"] as? Int ?? 0
        is_guest                    = dictionaryInfo["is_guest"]?.intValue.mk_boolValue ?? false
        //is_current_corporate = dictionaryInfo["is_current_corporate"] as? Bool ?? false
        
        tokendata = dictionaryInfo["tokendata"] as? String ?? ""
        let bearer = dictionaryInfo["type"] as? String ?? "Bearer"
        let authorization = "\(bearer) \(tokendata)"
        header = ["Authorization" : authorization]
        

//        if (dictionaryInfo["login_type"] as? String) != nil
//        {
//            let logintype = (dictionaryInfo["login_type"] as! String).capitalized
//            switch logintype
//            {
//            case "M":
//                loginType = .Mobile
//            case "E":
//                loginType = .Email
//            case "guest":
//                loginType = .Guest
//            default:
//                loginType = .None
//            }
//        }
        
        saveToDefault()
    }
    func dictionaryFromInfos() -> [String: AnyObject]
    {
        var dictionaryInfo = [String: AnyObject]()
        
        dictionaryInfo["tokendata"] = self.tokendata as AnyObject?
        
//        switch loginType
//        {
//        case .Email:
//            dictionaryInfo["login_type"] = LoginType.Email.rawValue as AnyObject?
//        case .Mobile:
//            dictionaryInfo["login_type"] = LoginType.Mobile.rawValue as AnyObject?
//        case .Guest:
//            dictionaryInfo["login_type"] = LoginType.Guest.rawValue as AnyObject?
//        default:
//            dictionaryInfo["login_type"] = LoginType.None.rawValue as AnyObject?
//        }
//        dictionaryInfo["language"] = current_language == .Spanish ? 1 as AnyObject? : 0 as AnyObject?
        
        dictionaryInfo["user_id"] = self.user_id as AnyObject?
        dictionaryInfo["username"] = self.username as AnyObject?
        dictionaryInfo["email"] = self.email as AnyObject?
        dictionaryInfo["email_verified"] = self.email_verified.mk_intValue as AnyObject?
        dictionaryInfo["is_notification"] = self.is_notification.mk_intValue as AnyObject?
        dictionaryInfo["is_screen_lock"] = self.is_screen_lock.mk_intValue as AnyObject?
        dictionaryInfo["is_social"] = self.is_social.mk_intValue as AnyObject?
        dictionaryInfo["profile_image"] = self.profile_image as AnyObject?
        dictionaryInfo["no_of_published_recipes"] = self.no_of_published_recipes as AnyObject?
        
        dictionaryInfo["is_guest"] = self.is_guest as AnyObject?
        
        return dictionaryInfo
    }
    
    func logOut()
    {
        
        clearInstance()
        apiAuthHeader.logout()
    
        //AppInstance.manageAPIToken(_guestNotificationEnabled: guestNotificationEnabled)
        //AppInstance.manageAPIToken(_guestNotificationEnabled: false)
    }
    
    //MARK: - Save to user default -
    func saveToDefault()
    {
        let userDefault = UserDefaults.standard
        userDefault.set(dictionaryFromInfos(), forKey: userDefaultKey)
        userDefault.synchronize()
    }
    //MARK: - Save to Language in UserDefault -
    func saveLanguage()
    {
        let userDefault = UserDefaults.standard
//        userDefault.setValue(kCurrentUser.current_language.rawValue, forKey: language_state)
        userDefault.synchronize()
    }
    
    func clearInstance()
    {
        tokendata                   = ""
        user_id                     = 0
        username                    = ""
        email                       = ""
        email_verified              = false
        is_notification             = false
        is_screen_lock              = false
        is_social                   = false
        profile_image               = ""
        no_of_published_recipes     = 0
        
        //is_current_corporate = false
        
        //DEFAULT.removeObject(forKey: language_state)
        //DEFAULT.synchronize()
        
        saveToDefault()
        kCurrentUser = User()
    }
}

