//
//  CMSModel.swift
//  Grubshoot
//
//  Created by Suhail Ranger on 04/03/18.
//  Copyright © 2018 peerbits. All rights reserved.
//

import Foundation
import SwiftyJSON

let cmsLinks = CMSModel()

enum CMSTypes : String
{
    case terms = "terms_condtions"
    case privacy = "privacy_policy"
    case aboutus = "about_us"
}
enum CMSPageTitles : String
{
    case terms = "Terms and Conditions"
    case privacy = "Privacy policy"
    case aboutus = "About Us"
}

class CMSModel: BaseModel
{
    var terms : String = ""
    var privacy : String = ""
    var aboutus: String = ""
    
    override init()
    {
        super.init()
    }
    override init(info: JSON)
    {
        super.init(info: info)
        
        terms = info[CMSTypes.terms.rawValue].string ?? ""
        privacy = info[CMSTypes.privacy.rawValue].string ?? ""
        aboutus = info[CMSTypes.aboutus.rawValue].string ?? ""
    }
    override func updateInfo(_ info: JSON)
    {
        terms = info[CMSTypes.terms.rawValue].string ?? ""
        privacy = info[CMSTypes.privacy.rawValue].string ?? ""
        aboutus = info[CMSTypes.aboutus.rawValue].string ?? ""
    }
    func getTitleAndURL(type : CMSTypes) -> (title:String, url: String)
    {
        switch type
        {
            case .terms:
                return (CMSPageTitles.terms.rawValue, cmsLinks.terms)
            case .privacy:
                return (CMSPageTitles.privacy.rawValue, cmsLinks.privacy)
            case .aboutus:
                return (CMSPageTitles.aboutus.rawValue, cmsLinks.aboutus)
        }
    }
}
