//
//  CellForgotPass.swift
//  TheOpenPan
//
//  Created by Pathan Mushrankhan on 11/01/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class CellForgotPass: UITableViewCell {

    @IBOutlet var txtEmail: SkyFloatingLabelTextField!
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDesc: UILabel!
    
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnSend: UIButton!
    
    typealias validationCompleted = (()->())
    var blockValidated : validationCompleted!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.txtEmail.delegate = self
        
        self.btnBack.mk_addTapHandler { (btn) in
            UIViewController.CVC()?.navigationController?.popViewController(animated: true)
        }
        
        self.btnSend.mk_addTapHandler { (btn) in
            
            let ValidateForm: ValidationParam =
                [.REQ_EMAIL: (0,self.txtEmail),
                 .VALID_EMAIL: (1,self.txtEmail)]
            
            print(ValidateForm)
            
            if PBValidation(fieldName: ValidateForm) {
                self.blockValidated()
                hapSoft()
            } else {
                hapError()
            }
            
        }
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}



extension CellForgotPass : UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtEmail {
            let maxLength = 100
            let currentString: NSString = NSString(string: self.txtEmail.text!)
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
//        if textField == txtEmail {
//            txtPassword.becomeFirstResponder()
//        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == txtEmail
        {
            txtEmail.resignFirstResponder()
        }
    }
    
}
