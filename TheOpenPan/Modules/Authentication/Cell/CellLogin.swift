//
//  CellLogin.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/10/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class CellLogin: UITableViewCell {
    
    @IBOutlet var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet var txtPassword: SkyFloatingLabelTextField!
    
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnForgotPassword: UIButton!
    @IBOutlet var btnRegister: UIButton!
    
    typealias validationCompleted = (()->())
    var blockValidated : validationCompleted!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
        
        self.btnBack.mk_addTapHandler { (btn) in
            UIViewController.CVC()?.navigationController?.popViewController(animated: true)
        }
        
        btnLogin.mk_addTapHandler { (btn) in
            let ValidateForm: ValidationParam =
                [
                    
                    .REQ_EMAIL: (0,self.txtEmail),
                    .VALID_EMAIL: (1,self.txtEmail),
                    .REQ_PASSWORD: (2,self.txtPassword)
            ]
            
            print(ValidateForm)
            
            if PBValidation(fieldName: ValidateForm)
            {
                hapSoft()
                self.blockValidated()
                
            } else {
                hapError()
            }
        }
        
        btnRegister.mk_addTapHandler { (btn) in
            hapSoft()
            self.btnRegisterTapped()
        }
        
        btnForgotPassword.mk_addTapHandler { (btn) in
            hapSoft()
            let objForgotPasswordVC = apde.getController(vc_name: "ForgotPasswordVC", sb: .Authentication) as! ForgotPasswordVC
            UIViewController.CVC()?.navigationController?.pushViewController(objForgotPasswordVC, animated: true)
        }
        
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func btnRegisterTapped() {
        let objRegisterVC = apde.getController(vc_name: "RegisterVC", sb: .Authentication) as! RegisterVC
        UIViewController.CVC()?.navigationController?.pushViewController(objRegisterVC, animated: true)
    }
}


extension CellLogin : UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtEmail
        {
            let maxLength = 100
            let currentString: NSString = NSString(string: self.txtEmail.text!)
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else
        {
            let maxLength = 16
            let currentString: NSString = NSString(string: self.txtPassword.text!)
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtEmail
        {
            txtPassword.becomeFirstResponder()
        }
        else
        {
            txtPassword.resignFirstResponder()
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == txtPassword
        {
            txtPassword.resignFirstResponder()
        }
    }
    
}
