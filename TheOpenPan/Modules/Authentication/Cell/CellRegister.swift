//
//  CellRegister.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/10/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class CellRegister: UITableViewCell {

    @IBOutlet var btnTermsConditions: UIButton!
    @IBOutlet var btnCheckbox: UIButton!
    @IBOutlet var btnRegister: UIButton!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnLogin: UIButton!
    
    @IBOutlet var txtUsername: SkyFloatingLabelTextField!
    @IBOutlet var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet var txtConfirmPassword: SkyFloatingLabelTextField!
    
    typealias validationCompleted = (()->())
    var blockValidated : validationCompleted!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.txtUsername.delegate = self
        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
        self.txtConfirmPassword.delegate = self
        
        self.btnBack.mk_addTapHandler { (btn) in
            UIViewController.CVC()?.navigationController?.popViewController(animated: true)
        }
        
        let dict = [NSAttributedString.Key.underlineStyle : 1,
                    NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.3960784314, green: 0.5803921569, blue: 1, alpha: 1),
                    NSAttributedString.Key.font:UIFont.init(name: enum_font.regular.rawValue, size: 15)!] as [NSAttributedString.Key : Any]
        
        self.btnTermsConditions.setAttributedTitle(NSAttributedString.init(string: "Terms & Conditions".localize(), attributes: dict), for: .normal)
        
        self.btnTermsConditions.mk_addTapHandler { (btn) in
            hapHeavy()
            
            let objNavTermsAndConditionsVC = apde.getController(vc_name: "NavTermsAndConditionsVC", sb: .Settings) as! UINavigationController
            UIViewController.CVC()?.present(objNavTermsAndConditionsVC, animated: true, completion: nil)
        }
        
        self.btnCheckbox.mk_addTapHandler { (btn) in
            btn.isSelected = !btn.isSelected
            hapHeavy()
        }
        
        btnRegister.mk_addTapHandler { (btn) in
            
            let ValidateForm1: ValidationParam =
                [
                    .REQ_NAME: (0,self.txtUsername),
                    .REQ_EMAIL: (1,self.txtEmail),
                    .VALID_EMAIL: (2,self.txtEmail)
            ]
            
            
            let ValidateForm2: ValidationParam =
                [.REQ_PASSWORD: (3,self.txtPassword), .VALID_PASSWORD: (4,self.txtPassword),.REQ_CONFIRM_PASS: (4,self.txtConfirmPassword)]
            
            if PBValidation(fieldName: ValidateForm1) && PBValidation(fieldName: ValidateForm2, Compare: (CompareField: self.txtPassword, CompareFieldTo: self.txtConfirmPassword)) {
                
                if self.btnCheckbox.isSelected {
                    self.blockValidated()
                    hapSoft()
                } else {
                    self.btnCheckbox.superview?.shake()
                    hapError()
                }
                
            } else {
                hapError()
            }

            
        }
        
        self.btnLogin.mk_addTapHandler { (btn) in
            hapSoft()
            self.btnLoginTapped()
        }
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func btnLoginTapped() {
        UIViewController.CVC()?.navigationController?.popViewController(animated: true)
    }
    
}

extension CellRegister : UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtUsername
        {
            let maxLength = 50
            let currentString: NSString = NSString(string: self.txtUsername.text!)
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else if textField == txtEmail
        {
            let maxLength = 100
            let currentString: NSString = NSString(string: self.txtEmail.text!)
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else if textField == txtPassword
        {
            let maxLength = 16
            let currentString: NSString = NSString(string: self.txtPassword.text!)
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        } else if textField == txtConfirmPassword {
            
            let maxLength = 16
            let currentString: NSString = NSString(string: self.txtConfirmPassword.text!)
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtUsername
        {
            txtEmail.becomeFirstResponder()
        }
        else if textField == txtEmail
        {
            txtPassword.becomeFirstResponder()
        }
        else if textField == txtPassword
        {
            txtConfirmPassword.becomeFirstResponder()
        }
        else
        {
            txtPassword.resignFirstResponder()
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == txtConfirmPassword
        {
            txtPassword.resignFirstResponder()
        }
    }
    
}

