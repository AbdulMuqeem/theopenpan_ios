//
//  CellSelectCategories.swift
//  TheOpenPan
//
//  Created by Pathan Mushrankhan on 11/01/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import TagListView

class CellSelectCategories: UITableViewCell {
    
    @IBOutlet weak var tagList: TagListView!
    @IBOutlet weak var lblCategoryName: UILabel!
    
    var currentVC: SelectCategoriesVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func handleCell(vc:SelectCategoriesVC,objMasterData:Master_data, indexPath:IndexPath) {
        
        self.currentVC = vc
        tagList.delegate = self
        tagList.enableRemoveButton = false
        tagList.textFont = UIFont.init(name: enum_font.regular.rawValue, size: 16)!
        
        self.tagList.removeAllTags()
        
        guard let arrCategories = objMasterData.categories.allObjects as? [Recipe_category] else { return }
        
        if indexPath.row < arrCategories.count {
            let arrFoundCategories = arrCategories.filter { (objCat) -> Bool in
                objCat.displayIndex == Int64(indexPath.row)
            }
            if let objCategory = arrFoundCategories.first {
                self.lblCategoryName.text = (objCategory.category_name ?? "").uppercased()
                if let arrTags = objCategory.tags.allObjects as? [Category_tag] {
                    
                    var mutArrTags = arrTags
                    mutArrTags.sort { (t1, t2) -> Bool in
                        (t1.tag_name ?? "").localizedCompare(t2.tag_name ?? "") == .orderedAscending
                    }
                    
                    for tag in mutArrTags {
                        let tagView = self.tagList.addTag(tag.tag_name ?? "")
                        tagView.id = tag.tag_id
//                        tagView.accessibilityValue = "is_category_tag"
                        tagView.cat_tag_object = tag
                        tagView.isSelected = tag.is_selected
                    }
                }
            }
        }
        
        if indexPath.row == arrCategories.count {
            self.lblCategoryName.text = "TIME".localize()
            
            if let arrTags = objMasterData.time_option.allObjects as? [Category_tag] {
                
                var mutArrTags = arrTags
                mutArrTags.sort { (t1, t2) -> Bool in
                    (t1.tag_name ?? "").localizedCompare(t2.tag_name ?? "") == .orderedAscending
                }
                
                for tag in mutArrTags {
                    let tagView = self.tagList.addTag(tag.tag_name ?? "")
                    tagView.id = tag.tag_id
//                    tagView.accessibilityValue = "is_time_option"
                    tagView.cat_tag_object = tag
                    tagView.isSelected = tag.is_selected
                }
            }
//            let tagView = self.tagList.addTag("One-pot meals")
//            tagView.isSelected = self.currentVC.isOnePotMeals
//            tagView.accessibilityValue = "is_onepot_meals"
        }
        tagList?.layoutIfNeeded()
        self.layoutIfNeeded()
    }

    
}


// MARK: TagListViewDelegate
extension CellSelectCategories : TagListViewDelegate {
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        tagView.isSelected = !tagView.isSelected
        hapSoft()
        
        if self.currentVC.arrAllSelectedTags.contains(tagView) {
            self.currentVC.arrAllSelectedTags.removeObject(tagView)
        } else {
            self.currentVC.arrAllSelectedTags.append(tagView)
        }
        
        if tagView.accessibilityValue == "is_onepot_meals" {
            self.currentVC.isOnePotMeals = tagView.isSelected
        } else {
            tagView.cat_tag_object.is_selected = tagView.isSelected
        }
        
        
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
}
