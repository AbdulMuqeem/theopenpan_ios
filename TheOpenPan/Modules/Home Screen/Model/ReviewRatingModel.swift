//
//  ReviewRatingModel.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/14/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation

struct ReviewRatingModel : Codable {
    var total_no_of_ratings                 = Int64()
    var total_no_of_reviews                 = Int64()
    var final_rating                        = Double()
    var ratings_categories                  = RatingsCategoriesModel()
    var is_last                             : Int?
    var is_rated                            = Int()
    var review_list                         = [ReviewListModel]()
    
    struct RatingsCategoriesModel : Codable {
        var no_of_five_star                     = Int64()
        var no_of_four_star                     = Int64()
        var no_of_three_star                    = Int64()
        var no_of_two_star                      = Int64()
        var no_of_one_star                      = Int64()
    }
    
    struct ReviewListModel : Codable {
        var id                                  = Int64()
        var review_desc                         = String()
        var reviewer_name                       = String()
        var rating                              = Double()
        var created_at                          = Int64()
    }
    
    enum CodingKeys: String, CodingKey {
        case total_no_of_ratings
        case total_no_of_reviews
        case final_rating
        case ratings_categories
        case is_last
        case is_rated
        case review_list
    }
}



struct GroceryProduct: Codable {
    var name: String
    var points: Int
    var description: String?
    
    private enum CodingKeys: String, CodingKey {
        case name = "product_name"
        case points = "product_cost"
        case description
    }
}

