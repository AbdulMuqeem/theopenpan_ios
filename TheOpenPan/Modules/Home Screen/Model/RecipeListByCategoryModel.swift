//
//  RecipeListByCategoryModel.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation

//struct RecipeListByCategoryModel : Codable {
//    var id                              = Int64()
//    var name                            = String()
//    var creator_name                    = String()
//    var ratings                         = Double()
//    var media_type                      = Int()
//    var media_url                       = String()
//    var video_thumb_url                 = String()
//    var recipe_status                   = Int()
//    var web_url                         = String()
//    
//}



struct RecipeListByCategoryModel : Codable {
    var id                              = Int64()
    var name                            = String()
    var creator_name                    = String()
    var ratings                         = Double()
    var media_type                      = Int()
    var media_url                       = String()
    var video_thumb_url                 = String()
    var recipe_status                   = Int()
    var view_url                        = String()
    var web_url                         = String()
    var yield                           = Int()
    var ingredientLines                 = [String]()
    var is_edamam                       = Int()
    var is_from_scrapper                = Int()
    var ready_time_hours                = Int()
    var ready_time_mins                 = Int()
}


struct IngredientLines : Codable {
    var id                              = Int64()
    var name                            = String()
    var creator_name                    = String()
    var ratings                         = Double()
    var media_type                      = Int()
    var media_url                       = String()
    var video_thumb_url                 = String()
    var recipe_status                   = Int()
    var view_url                        = String()
    var web_url                         = String()
    var yield                           = Int()
    
    
}

//{
//    "id": 230,
//    "name": "Soon Kueh 笋粿",
//    "creator_name": "Shefali patel",
//    "ratings": 0,
//    "media_type": 2,
//    "media_url": "https://www.youtube.com/embed/AOIZ_YcQlBg?modestbranding=1&autohide=1&showinfo=0",
//    "video_thumb_url": "http://img.youtube.com/vi/AOIZ_YcQlBg/hqdefault.jpg",
//    "recipe_status": 3,
//    "web_url": "http://mysingaporefood.com/recipe/soon-kueh/",
//    "yield": 0,
//    "ingredientLines": [
//        "2kg turnip (about 5 small to medium-size turnips)",
//        "1 tin bamboo shoots",
//        "100g dried shrimps, washed",
//        "500g rice flour",
//        "750g tapioca flour",
//        "2 to 3 cloves garlic, minced",
//        "1 ½ tsp salt",
//        "1 to 2 tsp pepper",
//        "5 tbs oil",
//        "2.1 litres hot water",
//        "450ml room-temperature water"
//    ],
//    "is_edamam": 0,
//    "is_from_scrap": 1
//},
