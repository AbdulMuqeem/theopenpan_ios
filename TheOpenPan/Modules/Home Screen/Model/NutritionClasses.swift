//
//  NutritionClasses.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/4/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation



struct ParsedRes {
    
    var calories                = Int()
    
    var totalFatQty             = Int()
    var totalFatUnit            = String()
    var totalFatQty_n           = Double()
    var totalFatUnit_n          = String()
    
    var totalFatSatQty             = Int()
    var totalFatSatUnit            = String()
    var totalFatSatQty_n           = Double()
    var totalFatSatUnit_n          = String()
    
    var sodiumQty             = Int()
    var sodiumUnit            = String()
    var sodiumQty_n           = Double()
    var sodiumUnit_n          = String()
    
    var carbQty             = Int()
    var carbUnit            = String()
    var carbQty_n           = Double()
    var carbUnit_n          = String()
    
    var dietFiberQty             = Int()
    var dietFiberUnit            = String()
    var dietFiberQty_n           = Double()
    var dietFiberUnit_n          = String()
    
    var sugarQty_n           = Double()
    var sugarUnit_n          = String()
    
    var proteinQty             = Int()
    var proteinUnit            = String()
    var proteinQty_n           = Double()
    var proteinUnit_n          = String()
    
    var calciumQty             = Int()
    var calciumUnit            = String()
    var calciumQty_n           = Double()
    var calciumUnit_n          = String()
    
    var ironQty             = Int()
    var ironUnit            = String()
    var ironQty_n           = Double()
    var ironUnit_n          = String()
    
    var potassiumQty             = Int()
    var potassiumUnit            = String()
    var potassiumQty_n           = Double()
    var potassiumUnit_n          = String()
    
    
    
    var nutr                    = [Nutrients]()
    
    
}

struct Nutrients {
    var dict = [String:Any]()
}



struct RecipeDetails {
    
    
    var id                                  = Int64()
    var name                                = String()
    var creator_name                        = String()
    var ratings                             = Double()
    var media_type                          = Int()
    var media_url                           = String()
    var video_thumb_url                     = String()
    var ready_time_hours                    = Int()
    var ready_time_mins                     = Int()
    var serves                              = Int()
    var view_url                            = String()
    var web_url                             = String()
    var no_of_ratings                       = Int64()
    var recipe_owner_type                   = Int()
    var is_own_recipe                       = Int()
    var is_saved_in_cookbook                = Int()
    var cookbook_id                         = Int64()
    var is_admin_edited                     = Int()
    var admin_notes                         = String()
    var is_one_pot_meal                     = Int()
    var is_easy                             = Int()
    var recipe_status                       = Int()
    var ingredient_list                     = [Ingredient_list]()
    var directions_list                     = [Direction_list]()
    var keywords_list                       = [KeywordModel]()
    var cuisine_type_id_list                = String()
    var difficulty_level_id_list            = String()
    var diet_id_list                        = String()
    var meal_type_id_list                   = String()
    
    var original_serves                     : Int = 0
    var is_direction_selected               : Bool = false
    var is_btn_add_ing_to_shoplist_tapped   = false
    var original_serves_ingredient_list     = [Ingredient_list]()
    

}



struct KeywordModel {
    var description = String()
}
