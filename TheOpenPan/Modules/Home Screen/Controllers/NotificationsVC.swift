//
//  NotificationsVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/13/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import DZNEmptyDataSet
import SwiftyJSON

class NotificationsVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet weak var tblView: UITableView!
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    var arrNotificationModel = [NotificationModel]()
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        self.setupTableView()
        self.navigationItem.title = "Notifications".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        
        self.addRightBarButtonItem(img: nil, btnTitle: "CLEAR".localize()) { (barButton) in
            
            BasicFunctions.showPopupTwoButton(title: "Clear All Notifications".localize(), desc: "Are you sure you want to remove all notifications?".localize(), btnTitleLeft: "NO".localize(), btnTitleRight: "YES".localize(), baseVC: self) { (isYes) in
                if isYes {
                    apde.showLoader()
                    clearAllNotifications() { (res) in
                        
                        switch res {
                        case .success(_, _) :
                            self.getNotificationListApiCall()
                            break
                        case .failed(_):
                            break
                        case .error(_):
                            break
                        }
                        apde.hideLoader()
                    }
                }
            }
            
            

        }
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.setupLayout()
        changeTabBar(hidden: true, animated: false, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getNotificationListApiCall()
    }
    
}


//------------------------------------------
//MARK: - Table View Setup Methods -
extension NotificationsVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.emptyDataSetSource = self
        self.tblView.emptyDataSetDelegate = self
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNotificationModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellNotifications = tableView.dequeueReusableCell(withIdentifier: "CellNotifications", for: indexPath) as! CellNotifications
        
        cellNotifications.handleCell(vc: self, ip: indexPath)
        
        return cellNotifications
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let notification = self.arrNotificationModel[indexPath.row]
        let notType = enum_notification_type.init(rawValue: notification.notification_type) ?? .none
        
        if notType == .admin {
            apde.readNotificationApiCall(notification_id: notification.notification_id, compHandler: {
                self.getNotificationListApiCall()
            })
            return
        }
        
        
        
        
        checkRecipeIsAvailable(recipe_id: notification.recipe_id) { (isRecipeAvailable,_) in
            if isRecipeAvailable {
                let objRecipeDetailsVC = apde.getController(vc_name: "RecipeDetailsVC", sb: .RecipeDetailsVC) as! RecipeDetailsVC
                objRecipeDetailsVC.recipe_id = notification.recipe_id
                objRecipeDetailsVC.recipeDeletedOrUpdatedBlock = { (updatedRecipeDetails, isReloadWhole) in
                        
                }
                if notType == .admin_recipe_status_rejected {
                    objRecipeDetailsVC.isDisplayRejectionNotePopup = true
                }
                self.navigationController?.pushViewController(objRecipeDetailsVC, animated: true)
                
                apde.readNotificationApiCall(notification_id: notification.notification_id, compHandler: {
                    self.getNotificationListApiCall()
                })
            }
        }
        
        
        
        
    }
}



//MARK: - API CALLS
extension NotificationsVC {
    
    func getNotificationListApiCall() {

        apde.showLoader()
        self.tblView.mk_animateAlpha(sec: 0.1, alpha: 0.0)
        
        getNotificationList { (res) in
            
            switch res {
            case .success(let dictData, _) :
                
                if let arrDict = dictData["notifications"] as? Array<Dictionary<String,Any>> {
                    
                    self.arrNotificationModel.removeAll()
                    
                    for dict in arrDict {
                        
                        let jsonDict = JSON(dict)
                        
                        let created_at          = jsonDict["created_at"].int64Value
                        let description         = jsonDict["description"].stringValue
                        let is_read             = jsonDict["is_read"].intValue
                        let notification_id     = jsonDict["notification_id"].int64Value
                        let notification_type   = jsonDict["notification_type"].stringValue
                        let recipe_id           = jsonDict["recipe_id"].int64Value
                        let title               = jsonDict["title"].stringValue
                        
                        let objNotificationModel = NotificationModel.init(notification_id: notification_id, title: title, description: description, created_at: created_at, notification_type: notification_type, recipe_id: recipe_id, is_read: is_read)
                        
                        self.arrNotificationModel.append(objNotificationModel)
                        
                    }
                    
                    Timer.after(0.2) {
                        self.tblView.mk_animateAlpha(sec: 0.1, alpha: 1.0)
                        self.tblView.reloadData()
                    }
                }
                
                break
            case .failed(message: let msg):
                self.tblView.mk_animateAlpha(sec: 0.1, alpha: 1.0)
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                self.tblView.mk_animateAlpha(sec: 0.1, alpha: 1.0)
                BasicFunctions.showError(strError: error)
                break
            }
            apde.hideLoader()
        }
        
    }
    
    
    
    
    
//    func readNotificationApiCall(notification_id : Int64) {
//        
//        readNotification(notification_id:notification_id) { (res) in
//            
//            switch res {
//            case .success(let dictData, let message) :
//                self.getNotificationListApiCall()
//                
//                break
//            case .failed(message: let msg):
////                BasicFunctions.showError(strError: msg)
//                break
//            case .error(error: let error):
////                BasicFunctions.showError(strError: error)
//                break
//            }
////            apde.hideLoader()
//        }
//        
//    }
}



extension NotificationsVC : DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return "No notifications...".AttributedStringFont(enum_font.medium.font(16), fontColor: #colorLiteral(red: 0.7450980392, green: 0.6823529412, blue: 0.6549019608, alpha: 1))
    }
}



struct NotificationModel : Codable {
    var notification_id = Int64()
    var title = String()
    var description = String()
    var created_at = Int64()
    var notification_type = String()
    var recipe_id = Int64()
    var is_read = Int()
}


func checkRecipeIsAvailable(recipe_id:Int64, is_from_cookbook:Bool = false, comphandler:@escaping (Bool,[String:Any]?)->()) {
    getRecipeDetails(recipe_id: recipe_id, is_from_cookbook: is_from_cookbook.mk_intValue) { (res) in
        switch res {
        case .success(dictData: let dictData, message: _) :
            comphandler(true,dictData)
            break
        case .error(error: let err) :
            print("err\(err)")
            BasicFunctions.showError(strError: err)
            comphandler(false,nil)
            break
        case .failed(message: let msg) :
            if msg.contains("not found") {
                BasicFunctions.showInfo(strError: "This recipe is not available anymore.")
            }
            comphandler(false,nil)
            print("msg\(msg)")
            break
        }
    }
}
