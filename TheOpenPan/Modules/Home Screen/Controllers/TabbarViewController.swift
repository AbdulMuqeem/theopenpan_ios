//
//  TabbarViewController.swift
//  Gifmos
//
//  Created by macmini3 on 28/03/16.
//  Copyright © 2016 peerbits. All rights reserved.
//

import UIKit

let hidden : Bool = false
class TabbarViewController: UITabBarController,UITabBarControllerDelegate
{

//    @IBOutlet var Tabardujour: UITabBar!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.7450980392, green: 0.6823529412, blue: 0.6549019608, alpha: 1),NSAttributedString.Key.font: UIFont(name: enum_font.medium.rawValue, size: 12)!], for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(TabbarViewController.hideTabbar), name: NSNotification.Name(rawValue: "hidetabbar"), object: nil)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.2274509804, green: 0.2274509804, blue: 0.2274509804, alpha: 1),NSAttributedString.Key.font: UIFont(name: enum_font.medium.rawValue, size: 12)!], for: .selected)
        
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(red: 51.0/255.0, green: 55.0/255.0, blue: 78.0/255.0, alpha: 1)], for: .selected)
        
        
        tabBar.itemPositioning = .fill
        //tabBar.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
        //UITabBarItem.init(title: <#T##String?#>, image: <#T##UIImage?#>, selectedImage: <#T##UIImage?#>)
        //self.tabBar.shadowImage = UIImage()
        //self.tabBar.backgroundImage = UIImage()
        //self.tabBar.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
        
        
        //Selected Images
        
        let myTabBarItem = (self.tabBar.items?[0])! as UITabBarItem
        myTabBarItem.selectedImage = UIImage(named: "AssetTabSelected1")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
//        myTabBarItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        let myTabBarItem1 = (self.tabBar.items?[1])! as UITabBarItem
        myTabBarItem1.selectedImage = UIImage(named: "AssetTabSelected2")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
//        myTabBarItem1.imageInsets = UIEdgeInsets(top: 1, left: 0, bottom: -1, right: 0)

        let myTabBarItem2 = (self.tabBar.items?[2])! as UITabBarItem
        myTabBarItem2.selectedImage = UIImage(named: "AssetTabSelected3")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
//        myTabBarItem2.imageInsets = UIEdgeInsets(top: 6, left: 5, bottom: 4, right: 5)

        let myTabBarItem3 = (self.tabBar.items?[3])! as UITabBarItem
        myTabBarItem3.selectedImage = UIImage(named: "AssetTabSelected4")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
//        myTabBarItem3.imageInsets = UIEdgeInsets(top: 1, left: 0, bottom: -1, right: 0)

        let myTabBarItem4 = (self.tabBar.items?[4])! as UITabBarItem
        myTabBarItem4.selectedImage = UIImage(named: "AssetTabSelected5")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
//        myTabBarItem4.imageInsets = UIEdgeInsets(top: 1, left: 0, bottom: -1, right: 0)

        
        //UnSelected Images

        let myTabBarItem5 = (self.tabBar.items?[0])! as UITabBarItem
        myTabBarItem5.image = UIImage(named: "AssetTabDeSelected1")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
//        myTabBarItem5.imageInsets = UIEdgeInsets(top: 7, left: 0, bottom: -7, right: 0)

        let myTabBarItem6 = (self.tabBar.items?[1])! as UITabBarItem
        myTabBarItem6.image = UIImage(named: "AssetTabDeSelected2")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
//        myTabBarItem6.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)

        let myTabBarItem7 = (self.tabBar.items?[2])! as UITabBarItem
        myTabBarItem7.image = UIImage(named: "AssetTabDeSelected3")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
//        myTabBarItem7.imageInsets = UIEdgeInsets(top: 6, left: 5, bottom: 4, right: 5)

        let myTabBarItem8 = (self.tabBar.items?[3])! as UITabBarItem
        myTabBarItem8.image = UIImage(named: "AssetTabDeSelected4")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
//        myTabBarItem8.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)

        let myTabBarItem9 = (self.tabBar.items?[4])! as UITabBarItem
        myTabBarItem9.image = UIImage(named: "AssetTabDeSelected5")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
//        myTabBarItem9.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
        
               
        self.delegate = self
        
        self.selectedIndex = 2
        
    }
    
    
    @objc func hideTabbar()
    {
    
        if hidden != false {
        
            self.tabBar.isHidden = true
        }
        else{
        
            self.tabBar.isHidden = false
        }
    
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        for item in self.tabBar.items! {
            (item ).image! = (item ).image!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            (item ).selectedImage! = (item ).selectedImage!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        }
    
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        if kCurrentUser.is_guest {
            
            if (viewController as? UINavigationController)?.restorationIdentifier == "NavMyCookBookVC" {
//                BasicFunctions.showInfo(strError: "My cookbook feature is not available without signing in.")
                apde.presentLoginViewForGuest(errMsg: "My cookbook feature is not available without signing in.")
                return false
            }
            if (viewController as? UINavigationController)?.restorationIdentifier == "NavMealPlanVC" {
//                BasicFunctions.showInfo(strError: "Meal plan feature is not available without signing in.")
                apde.presentLoginViewForGuest(errMsg: "Meal plan feature is not available without signing in.")
                return false
            }
            if (viewController as? UINavigationController)?.restorationIdentifier == "NavShoppingListVC" {
//                BasicFunctions.showInfo(strError: "Shopping list feature is not available without signing in.")
                apde.presentLoginViewForGuest(errMsg: "Shopping list feature is not available without signing in.")
                return false
            }
        }
        
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)
    {
        hapHeavy()
        
        if let tabSecondNavController = (self.viewControllers?[1] as? UINavigationController) {
            if let objMyCookBookVC = tabSecondNavController.viewControllers.first as? MyCookBookVC {
                apde.preSelectedDateForAddToMealPlan = nil
                objMyCookBookVC.isFromMealPlan = false
                objMyCookBookVC.reflectBackBarButton_isFromMealPlan()
            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
 
    }
    
    override func viewDidLayoutSubviews() {
        // iOS 11: puts the titles to the right of image for horizontal size class regular. Only want offset when compact.
        // iOS 9 & 10: always puts titles under the image. Always want offset.
        var verticalOffset: CGFloat = 6.0

        if #available(iOS 13.0, *) {
            verticalOffset = 0.0
        }

        let imageInset = UIEdgeInsets(
            top: verticalOffset,
            left: 0.0,
            bottom: -verticalOffset,
            right: 0.0
        )

        for (i,tabBarItem) in (self.tabBar.items ?? []).enumerated() {
            if i == 2 {
                tabBarItem.imageInsets = imageInset
            }
        }
    }

    
    
}

