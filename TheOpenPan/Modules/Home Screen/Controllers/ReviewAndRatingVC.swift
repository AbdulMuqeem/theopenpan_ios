//
//  ReviewAndRatingVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/15/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import GSKStretchyHeaderView
import AVFoundation
import AVKit
import SwiftyJSON
import DZNEmptyDataSet

class ReviewAndRatingVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet weak var tblView                  : UITableView!
    @IBOutlet weak var btnProvideReview         : UIButton!
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    var reviewRatingModel                       = ReviewRatingModel.init(is_last: 0)
    var recipe_id                               = Int64()
    var is_own_recipe                           = false
    typealias Block = ()->()
    var blockReviewGiven : Block!
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        self.changeTabBar(hidden: true, animated: false, completion: nil)
        self.setupTableView()
        self.btnProvideReview.mk_addTapHandler { (btn) in
            
            let objProvideReviewVC = apde.getController(vc_name: "ProvideReviewVC", sb: .ReviewAndRating) as! ProvideReviewVC
            objProvideReviewVC.recipe_id = self.recipe_id
            objProvideReviewVC.compBlock = { isYes in
                print(isYes)
                if isYes {
                    self.blockReviewGiven()
                    self.getReviewRatingDetailsApiCall(recipe_id: self.recipe_id, start: 0, limit: 20)
                }
            }
            self.present(objProvideReviewVC, animated: true, completion: nil)
        }
        self.navigationItem.title = "Review & Rating".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        
        self.btnProvideReview.isEnabled = false
        self.btnProvideReview.alpha = 0.5
        
        
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLayout()
        self.getReviewRatingDetailsApiCall(recipe_id: self.recipe_id, start: 0, limit: 20)
        
    }
    
}


//------------------------------------------
//MARK: - Table View Setup Methods -
extension ReviewAndRatingVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
        self.tblView.emptyDataSetDelegate   = self
        self.tblView.emptyDataSetSource     = self
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.reviewRatingModel.total_no_of_ratings == 0 {
            return 0
        }
        if section == 0 {
            return 1
        }
        return self.reviewRatingModel.review_list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 160
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cellReviewRatingHeader = tableView.dequeueReusableCell(withIdentifier: "CellReviewRatingHeader", for: indexPath) as! CellReviewRating            
            cellReviewRatingHeader.handleCell(vc: self, ip: indexPath)
            
            return cellReviewRatingHeader
        }
        
        let cellReviewRatingUserComment = tableView.dequeueReusableCell(withIdentifier: "CellReviewRatingUserComment", for: indexPath) as! CellReviewRating
        
        cellReviewRatingUserComment.handleCellUserComment(vc: self, ip: indexPath)
        
        
        return cellReviewRatingUserComment
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


//MARK: - API CALLS
extension ReviewAndRatingVC {
    func getReviewRatingDetailsApiCall(recipe_id: Int64, start:Int, limit:Int) {

        apde.showLoader()
        
        getReviewRatingDetails(recipe_id: recipe_id,start:start, limit:limit) { (res) in
            switch res {
            case .success(data: _, dictData: let dictData, message: _):
                                
                
                if let jsonData = dictData.getJsonData() {
                    
                    let decoder = JSONDecoder()
                    do {
                        let reviewRatingModelDecoded = try decoder.decode(ReviewRatingModel.self, from: jsonData)
                        if start == 0 {
                            self.reviewRatingModel = reviewRatingModelDecoded
                        } else {
                            self.reviewRatingModel.review_list.append(contentsOf: reviewRatingModelDecoded.review_list)
                        }
                        
                        
                        self.btnProvideReview.isEnabled = true
                        self.btnProvideReview.alpha = 1.0
                        
                        if self.reviewRatingModel.is_rated.mk_boolValue {
                            self.btnProvideReview.isEnabled = false
                            self.btnProvideReview.alpha = 0.5
                        }
                        
                        if self.is_own_recipe {
                            self.btnProvideReview.isEnabled = false
                            self.btnProvideReview.alpha = 0.5
                        }
                        
                        if kCurrentUser.is_guest {
                            self.btnProvideReview.isEnabled = false
                            self.btnProvideReview.alpha = 0.5
                        }
                        
                        self.tblView.reloadData()
                    } catch {
                        print(error)
                    }
                    
                    
                }
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                break
            }
        }
        
        apde.hideLoader()

    }
}


extension ReviewAndRatingVC : DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return "No reviews and ratings yet...".AttributedStringFont(enum_font.medium.font(16), fontColor: #colorLiteral(red: 0.7450980392, green: 0.6823529412, blue: 0.6549019608, alpha: 1))
    }
}
