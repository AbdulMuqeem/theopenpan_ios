//
//  HomeController.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/13/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class HomeController: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    
    @IBOutlet var btnAddRecipe      : UIButton!
    
    @IBOutlet var collView          : UICollectionView!
    @IBOutlet weak var viewContainerBrowseRecipe: UIView!
    
    @IBOutlet var btnSegment1       : UIButton!
    @IBOutlet var btnSegment2       : UIButton!
    @IBOutlet var lblComingSoon     : UILabel!
    @IBOutlet var searchTextField   : UITextField!
    
    @IBOutlet weak var cntrHeightSearchView     : NSLayoutConstraint!
    
    
    //------------------------------------------
    //MARK: - Class Variables -
    let margin: CGFloat             = 10
    let cellsPerRow                 = 5
    
    var arrCategoryTags             = [Category_tag]()
    var is_one_pot                  = false
    var one_pot_image               = ""

    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.viewContainerBrowseRecipe.alpha = 0
        
        setupCollView()
        
        navigationItem.title = "The Open Pan".localize()
        
        self.btnAddRecipe.mk_addTapHandler { (btn) in
            hapSoft()
            
//            let testvc = UIStoryboard.init(name: "TestStoryboard", bundle:
//                nil).instantiateViewController(withIdentifier: "TestVC") as! TestVC
//            testvc.present(animated: true, completion: nil)
//            return
            
            if kCurrentUser.is_guest {
//                BasicFunctions.showInfo(strError: "Add recipe feature is not available without signing in.")
                apde.presentLoginViewForGuest(errMsg: "Add recipe feature is not available without signing in.")
                return
            }
            
            if self.btnSegment1.isSelected {
                self.changeTabBar(hidden: true, animated: false) {
                    let objAddRecipeOptionsVC = apde.getController(vc_name: "AddRecipeOptionsVC", sb: .AddRecipe) as! AddRecipeOptionsVC
                    self.navigationController?.pushViewController(objAddRecipeOptionsVC, animated: true)
                }
            }
            
            if self.btnSegment2.isSelected {
                for vc in self.children {
                    if let objHomeBrowseRecipeVC = vc as? HomeBrowseRecipeVC {
                        
                        print(objHomeBrowseRecipeVC.webView.url)
                        
                        let objImportRecipePopupVC = apde.getController(vc_name: "ImportRecipePopupVC", sb: .AddRecipe) as! ImportRecipePopupVC
                        objImportRecipePopupVC.compBlockWithUrl = { addNewRecipeData in
                            
                            let mutAddNewRecipeData = addNewRecipeData
                            let objAddNewRecipeVC = makeReadyAddNewRecipeVcFromImport(addNewRecipeData: mutAddNewRecipeData, arrImportedKeywords:objImportRecipePopupVC.arrImportedKeywords, arrImportedCuisineTags:objImportRecipePopupVC.arrImportedCuisineTags, arrImportedMealTypeTags:objImportRecipePopupVC.arrImportedMealTypeTags)
                            self.navigationController?.pushViewController(objAddNewRecipeVC, animated: true)
                        }
                        self.tabBarController?.present(objImportRecipePopupVC, animated: true, completion: nil)
                        Timer.after(0.6) {
                            objImportRecipePopupVC.txtUrl.text = objHomeBrowseRecipeVC.webView.url?.absoluteString
                            objImportRecipePopupVC.btnImportTapped(btn: objImportRecipePopupVC.btnImport)
                        }
                        
                    }
                }
            }
            
            
        }

        self.btnAddRecipe.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.btnAddRecipe.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.btnAddRecipe.layer.shadowOpacity = 1.0
        self.btnAddRecipe.layer.shadowRadius = 10.0
        self.btnAddRecipe.layer.masksToBounds = false
        
        self.setupSegmentControl()
        self.selectButton(btnSelect: self.btnSegment1, btnDeselect: self.btnSegment2)
        
        
        if kCurrentUser.is_guest {
            //            self.btnAddRecipe.isHidden = true
        } else {
            self.addRightBarButtonItem(img: UIImage.init(named: "AssetNotificationIcon"), btnTitle: nil) { (barButton) in
                hapSoft()
                let objNotificationsVC = apde.getController(vc_name: "NotificationsVC", sb: .Home) as! NotificationsVC
                self.navigationController?.pushViewController(objNotificationsVC, animated: true)
            }
            
            self.getNotificationUnreadCountApiCall()
        }
        
        
        self.searchTextField.shouldBeginEditing { () -> Bool in
            
            let objRecipeListVC = apde.getController(vc_name: "RecipeListVC", sb: .Home) as! RecipeListVC
            objRecipeListVC.title = ""
            objRecipeListVC.isDirectSearch = true
            self.navigationController?.pushViewController(objRecipeListVC, animated: true)
            
            return false
        }
        
    }
    
    func setupSegmentControl() {
        
        self.btnSegment1.mk_addTapHandler { (btn) in
            self.lblComingSoon.isHidden = true
            self.collView.mk_animateAlpha(sec: 0.15, alpha: 1)
            self.viewContainerBrowseRecipe.mk_animateAlpha(sec: 0.15, alpha: 0)
            self.selectButton(btnSelect: self.btnSegment1, btnDeselect: self.btnSegment2)
            
            self.cntrHeightSearchView.constant = 56
            UIView.animate(withDuration: 0.25, animations: {
                self.btnAddRecipe.setTitle("     Add recipe   ".localize(), for: .normal)
                self.searchTextField.superview?.alpha = 1
                self.view.layoutIfNeeded()
            }) { (s) in
                
            }
        }
        
        self.btnSegment2.mk_addTapHandler { (btn) in
            self.btnSegament2Tapped(btn: btn)
        }
        
    }
    
    
    func btnSegament2Tapped(btn:UIButton) {
        self.lblComingSoon.isHidden = false
        self.collView.mk_animateAlpha(sec: 0.15, alpha: 0)
        self.viewContainerBrowseRecipe.mk_animateAlpha(sec: 0.15, alpha: 1)
        self.selectButton(btnSelect: self.btnSegment2, btnDeselect: self.btnSegment1)
        
        self.cntrHeightSearchView.constant = 0
        UIView.animate(withDuration: 0.25, animations: {
            self.btnAddRecipe.setTitle("    Save & View ".localize(), for: .normal)
            self.searchTextField.superview?.alpha = 0
            self.view.layoutIfNeeded()
        }) { (s) in
            
        }
    }
    
    func selectButton(btnSelect:UIButton,btnDeselect:UIButton) {
        
        hapSoft()
        
        if btnSelect.isSelected {
            btnSelect.titleLabel?.font = UIFont.applyCustomMedium(fontSize: 18)
            btnSelect.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.8941176471, blue: 0.8784313725, alpha: 1)
            return
        }
        
        btnSelect.titleLabel?.font = UIFont.applyCustomMedium(fontSize: 18)
        btnSelect.isSelected = true
        btnSelect.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.8941176471, blue: 0.8784313725, alpha: 1)
        
        btnDeselect.titleLabel?.font = UIFont.applyCustomRegular(fontSize: 18)
        btnDeselect.isSelected = false
        btnDeselect.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
    }
    
    
    override func viewDidLayoutSubviews() {
        
        let shapelayer = CAShapeLayer.init()
        let bzPath = UIBezierPath.init(roundedRect: btnSegment1.bounds, byRoundingCorners: [.topLeft,.bottomLeft], cornerRadii: .init(width: 25, height: 25))
        shapelayer.path = bzPath.cgPath
        self.btnSegment1.layer.mask = shapelayer
        
        let shapelayer2 = CAShapeLayer.init()
        let bzPath2 = UIBezierPath.init(roundedRect: btnSegment1.bounds, byRoundingCorners: [.topRight,.bottomRight], cornerRadii: .init(width: 25, height: 25))
        shapelayer2.path = bzPath2.cgPath
        
        self.btnSegment2.layer.mask = shapelayer2
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setupLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        changeTabBar(hidden: false, animated: false, completion: nil)
        
//        if kCurrentUser.is_guest == false {
            self.getInterestedCategoriesApiCall()
//        }
        
        Timer.after(0.2) {
            apde.xBarHeight = (self.navigationController?.navigationBar.frame.size.height ?? 0.0) + (self.navigationController?.navigationBar.frame.origin.y ?? 0.0)
        }
        
        if kCurrentUser.is_guest == false {
            self.getNotificationUnreadCountApiCall()
        }
        
    }
    
    
}


//------------------------------------------
//MARK: - Table View Setup Methods -
extension HomeController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func setupCollView() {
        self.collView.delegate = self
        self.collView.dataSource = self
        
        if #available(iOS 11.0, *) {
            self.collView.contentInsetAdjustmentBehavior = .never
        }
        
        
        let layout = RDHCollectionViewGridLayout.init()
        layout.itemSpacing = 10
        layout.lineSpacing = 10
        layout.lineSize = 0
       
        if UIDevice.current.userInterfaceIdiom == .pad {
            layout.lineItemCount = 3
        } else {
            layout.lineItemCount = 2
        }
        
        layout.scrollDirection = .vertical
        layout.sectionsStartOnNewLine = false
        
        self.collView.collectionViewLayout = layout

        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 2 {
            return 0
//            return self.is_one_pot ? 1 : 0
        }
        
        return self.arrCategoryTags.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Manage category cell
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollViewCategoryManage", for: indexPath) as! CellCollViewCategory
            
            return cell
        }
        
        // is one pot cell
        if indexPath.section == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollViewCategory", for: indexPath) as! CellCollViewCategory
            cell.handleCellOnePotCell(self, indexPath: indexPath)
            return cell
        }
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollViewCategory", for: indexPath) as! CellCollViewCategory
        cell.handleCell(self.arrCategoryTags[indexPath.row], indexPath: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if indexPath.section == 0 {
            apde.goToSelectCategoriesList(transition: true)
            return
        }
        
        
        if indexPath.section == 1 {
            
            let categoryTag = self.arrCategoryTags[indexPath.row]
            
            if categoryTag.is_time_option_category {
                // Time options category cell selected
                let objRecipeListVC = apde.getController(vc_name: "RecipeListVC", sb: .Home) as! RecipeListVC
                objRecipeListVC.time_option_id = self.arrCategoryTags[indexPath.row].tag_id
                objRecipeListVC.title = self.arrCategoryTags[indexPath.row].tag_name ?? ""
                self.navigationController?.pushViewController(objRecipeListVC, animated: true)
            } else {
                // Normal categories cell selected
                let objRecipeListVC = apde.getController(vc_name: "RecipeListVC", sb: .Home) as! RecipeListVC
                objRecipeListVC.category_id = self.arrCategoryTags[indexPath.row].tag_id
                objRecipeListVC.title = self.arrCategoryTags[indexPath.row].tag_name ?? ""
                self.navigationController?.pushViewController(objRecipeListVC, animated: true)
            }
            
            
            return
        }
        
        
        
        if indexPath.section == 2 {
            // is one pot meal tapped
            
            let objRecipeListVC = apde.getController(vc_name: "RecipeListVC", sb: .Home) as! RecipeListVC
            objRecipeListVC.title = "One-pot meals".localize()
            objRecipeListVC.isOnePotSelected = true
            self.navigationController?.pushViewController(objRecipeListVC, animated: true)
            
        }
    }
}



//MARK: - API CALLS
extension HomeController {
    
    func getInterestedCategoriesApiCall() {
        
        apde.showLoader()
        self.arrCategoryTags.removeAll()
        self.collView.reloadData()
        self.is_one_pot = false
        
        getInterestedCategories { (res) in
               
               switch res {
               case .success(dictData: let dictData, _) :
                   
                   
                   // Set Interested Category tags selected
                   guard let arrDictCategories = dictData["categories"] as? [[String:Any]] else {
                       return
                   }
                   guard let arrInterestedCategories = Recipe_category.mr_import_(from_Array: arrDictCategories, in: app_default_context) as? [Recipe_category] else {
                       return
                   }
                   
                   for cat in arrInterestedCategories {
                        if let arrTags = cat.tags.allObjects as? [Category_tag] {
                            
                            let sorted = arrTags.sorted { (c1, c2) -> Bool in
                             (c1.tag_name ?? "").localizedCompare(c2.tag_name ?? "") == .orderedAscending
                            }
                            self.arrCategoryTags.append(contentsOf: sorted)
                        }
                   }

                   
                   // Set Interested Time Option tags selected
                   guard let arrDictTimeOption = dictData["time_option"] as? [[String:Any]] else {
                       return
                   }
                   
                   guard let arrTimeOption = Category_tag.mr_import_(from_Array: arrDictTimeOption, in: app_default_context) as? [Category_tag] else {
                       return
                   }
        
                   let sorted = arrTimeOption.sorted { (c1, c2) -> Bool in
                        (c1.tag_name ?? "").localizedCompare(c2.tag_name ?? "") == .orderedAscending
                    }
                   
                   for (i,_) in sorted.enumerated() {
                    sorted[i].is_time_option_category = true
                   }
                   
                    self.arrCategoryTags.append(contentsOf: sorted)

                   
                   // Set Interested is_one_pot selected
                   guard let is_one_pot = dictData["is_one_pot"] as? Int else {
                       return
                   }
                   guard let one_pot_image = dictData["one_pot_image"] as? String else {
                       return
                   }
                   
                   self.is_one_pot = is_one_pot.mk_boolValue
                   self.one_pot_image = one_pot_image
                   
                   self.collView.reloadData()
                   
                   
                   break
               case .error(error: let err) :
                   BasicFunctions.showError(strError: err)
                   break
               case .failed(message: let msg) :
                   BasicFunctions.showError(strError: msg)
                   break
               }
               apde.hideLoader()

           }

    }
}







//MARK: - API CALLS
extension HomeController {
    
    func getNotificationUnreadCountApiCall() {

        getNotificationUnreadCount { (res) in
            
            
            switch res {
            case .success(let dictData,_) :
                
                if let intCount = dictData["notification_count"] as? Int {
                    if intCount == 0 {
                        self.navigationItem.rightBarButtonItem?.removeBadge()
                    } else {
                        self.navigationItem.rightBarButtonItem?.addBadge(number: intCount)
                    }
                } else {
                    self.navigationItem.rightBarButtonItem?.removeBadge()
                }
                
                break
            case .failed(_):
                
                break
            case .error(_):
                
                break
            }
            
        }
        
    }
}




func gcdFunc(forNumber1 m: Int, andNumber2 n: Int) -> Int {
    var m = m
    var n = n
    while m != n {
        if m > n {
            m = m - n // large - small , store the results in large variable<br>
        } else {
            n = n - m
        }
    }
    return m // m or n is GCD
}

func tenRaisedTopower(_ decimalLength: Int) -> Int {
    var mutDecimalLength = decimalLength
    
    var answer = 10
    while mutDecimalLength != 1 {
        answer *= 10
        mutDecimalLength -= 1
    }
    return answer
}

func float(toFraction decimalNumber: Float) -> String {
    let decimalString = "\(decimalNumber)"
    let components = decimalString.components(separatedBy: ".")
    let decimalLength = components[1].count
    let n = tenRaisedTopower(decimalLength)
    let m = Int(components[1]) ?? 0
    let gcd = gcdFunc(forNumber1: m, andNumber2: n)
    let numer = m / gcd
    let deno = n / gcd
    let fractionnumer = (Int(components[0]) ?? 0 * deno) + numer
    print("answer>>\(fractionnumer)/\(deno)")
    return "\(fractionnumer)/\(deno)"
}


func removeSpace(_ string: String) -> String {
    if string.count == 0 { return string }
    var str: String = String(string[string.startIndex])
    for (index,value) in string.enumerated(){
        if index > 0 {
            let indexBefore = string.index(before: String.Index.init(encodedOffset: index))
            if value == " " && string[indexBefore] == " " {
            } else {
                str.append(value)
            }
        }
    }
    return str
}
