//
//  HomeBrowseRecipeVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 3/9/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import WebKit

class HomeBrowseRecipeVC: UIViewController {

    
    @IBOutlet weak var viewLoader: UIView!
    @IBOutlet weak var btnWebViewBack           : UIButton!
    @IBOutlet weak var btnWebViewReload         : UIButton!
    @IBOutlet weak var btnGoogleReload          : UIButton!
    @IBOutlet weak var btnWebViewFourth         : UIButton!
    @IBOutlet weak var viewParentWebView        : UIView!
    var webView                                 : WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webView = WKWebView(frame: self.view.frame)
        webView.navigationDelegate = self
        self.viewParentWebView.mk_addFourSideConstraintsInParent(childView: webView)
        
        self.loadGoogleRequest()
        
        
        self.btnWebViewBack.onTap {
            hapSoft()
            self.webView.goBack()
            Timer.after(1) {
                self.viewLoader.mk_animateAlpha(sec: 0.3, alpha: 0.0)
            }
        }
        
        self.btnWebViewFourth.onTap {
            hapSoft()
            self.webView.goForward()
            Timer.after(1) {
                self.viewLoader.mk_animateAlpha(sec: 0.3, alpha: 0.0)
            }
        }
        
        self.btnWebViewReload.onTap {
            hapSoft()
            self.webView.reload()
        }
        
        self.btnGoogleReload.mk_addTapHandler { (btn) in
            hapSoft()
            self.loadGoogleRequest()
            Timer.after(1) {
                self.viewLoader.mk_animateAlpha(sec: 0.3, alpha: 0.0)
            }
        }
        
    }
    
    func loadGoogleRequest() {
        let myURLString = "https://www.google.com/"
        let url = URL(string: myURLString)
        let request = URLRequest(url: url!)
        webView.load(request)
    }
    

}

extension HomeBrowseRecipeVC: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Started to load")
        self.viewLoader.mk_animateAlpha(sec: 0.3, alpha: 1.0)
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
        self.viewLoader.mk_animateAlpha(sec: 0.3, alpha: 0.0)
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    
}
