//
//  SearchFilterRecipeListVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/15/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import TagListView
import SwiftyJSON

class SearchFilterRecipeListVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet var tblViewSelectCategories       : UITableView!
    @IBOutlet weak var btnDone                  : UIButton!
    
    //------------------------------------------
    //MARK: - Class Variables -
    var objFilterList                           = FilterListModel()
    var isOnePotMealsSelected                   = false
    var isEasy                                  = false
//    var arrAllSelectedTags                      = [TagView]()
    
//    var selectedCategoriesTagsIds               = [[String:Any]]()
//    var selectedTimeOptionsIds                  = [Int64]()
    
    typealias BlockDone                         = ()->()
    var blockDone                               : BlockDone!
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
//        self.setupNavigationBarCustom()
        setupTableView()
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.title = "Filter".localize()
        

        self.addRightBarButtonItem(img: nil, btnTitle: "CLEAR") { (barButton) in
            self.clearFilters()
        }
        
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([
            NSAttributedString.Key.font: enum_font.semi_bold.font(14),
            NSAttributedString.Key.foregroundColor: UIColor.white],
        for: .normal)
        
        self.addLeftBarButtonItem(img: #imageLiteral(resourceName: "AssetCancelBarButton.png") , btnTitle: nil) { (barButton) in
            self.pop()
        }
        
        self.btnDone.mk_addTapHandler { (btn) in
            self.blockDone()
            self.pop()
        }
        
    }
    
    func clearFilters() {
        for (i,_) in self.objFilterList.time_option.enumerated() {
            self.objFilterList.time_option[i].is_selected = false
        }
        
        for (i,timeOption) in self.objFilterList.categories.enumerated() {
            for (j,_) in timeOption.tags.enumerated() {
                self.objFilterList.categories[i].tags[j].is_selected = false
            }
        }
        self.isOnePotMealsSelected = false
        self.isEasy = false
        self.tblViewSelectCategories?.reloadData()
    }
    
    
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupLayout()
//        self.getFilterListApiCall { (success) in
//            print(success)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
           
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tblViewSelectCategories.layoutIfNeeded()
        self.tblViewSelectCategories.layoutSubviews()
    }

}



//------------------------------------------
//MARK: - Table View Setup Methods -
extension SearchFilterRecipeListVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblViewSelectCategories.delegate = self
        self.tblViewSelectCategories.dataSource = self
        self.tblViewSelectCategories.estimatedRowHeight = 100
        if #available(iOS 11.0, *) {
            self.tblViewSelectCategories.contentInsetAdjustmentBehavior = .never
        }

    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objFilterList.categories.count + 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellSelectCategories = tableView.dequeueReusableCell(withIdentifier: "CellSelectCategories", for: indexPath) as! CellSelectCategories
        
        let tagList = cellSelectCategories.tagList!
        
        tagList.delegate = self
        tagList.enableRemoveButton = false
        tagList.textFont = UIFont.init(name: enum_font.regular.rawValue, size: 16)!
        
        tagList.removeAllTags()
        
        let arrCategories = self.objFilterList.categories
        
        if indexPath.row < arrCategories.count {
            var objCategory = arrCategories[indexPath.row]
            cellSelectCategories.lblCategoryName.text = (objCategory.category_name).uppercased()
            
            var mutArrTags = objCategory.tags
            mutArrTags.sort { (t1, t2) -> Bool in
                t1.tag_name.localizedCompare(t2.tag_name) == .orderedAscending
            }
            
            for (i,tag) in mutArrTags.enumerated() {
                
                let tagView = tagList.addTag(tag.tag_name)
                tagView.id = tag.tag_id
                tagView.filterTag = tag
                tagView.isSelected = tag.is_selected
                
                tagView.onTap {
                    tagView.isSelected = !tagView.isSelected
                    var mutTag = tag
                    mutTag.is_selected = tagView.isSelected
                    objCategory.tags[i] = mutTag
                    self.objFilterList.categories[indexPath.row] = objCategory
                }
            }
            
        }
        
        if indexPath.row == arrCategories.count {
            
            cellSelectCategories.lblCategoryName.text = "TIME".localize()
            
            let arrTagsTimeOption = self.objFilterList.time_option
            
            for (i,tag) in arrTagsTimeOption.enumerated() {
                let tagView = tagList.addTag(tag.tag_name)
                tagView.id = tag.tag_id
                tagView.filterTag = tag
                tagView.isSelected = tag.is_selected
                
                tagView.onTap {
                    
//                    for (j,_) in self.objFilterList.time_option.enumerated() {
//                        self.objFilterList.time_option[j].is_selected = false
//                    }
                    
                    tagView.isSelected = !tagView.isSelected
                    var mutTag = tag
                    mutTag.is_selected = tagView.isSelected
                    self.objFilterList.time_option[i] = mutTag
                    tableView.reloadRows(at: [indexPath], with: .automatic)
                }
            }
            
//            let tagView = tagList.addTag("One-pot meals")
//            tagView.isSelected = self.isOnePotMealsSelected
//            tagView.onTap {
//                tagView.isSelected = !tagView.isSelected
//                self.isOnePotMealsSelected = tagView.isSelected
//            }
        }
        
        
        // for Easy
        if indexPath.row == arrCategories.count + 1 {
            cellSelectCategories.lblCategoryName.text = "DIFFICULTY".localize()
            let tagView = tagList.addTag("Easy")
            tagView.isSelected = self.isEasy
            tagView.onTap {
                tagView.isSelected = !tagView.isSelected
                self.isEasy = tagView.isSelected
            }
        }
        
        tagList.layoutIfNeeded()
        cellSelectCategories.layoutIfNeeded()
        
        return cellSelectCategories
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.layoutIfNeeded()
    }
    
//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0{
//            changeTabBar(hidden: true, animated: true)
//        }
//        else{
//            changeTabBar(hidden: false, animated: true)
//        }
//    }
    
    
}


// MARK: TagListViewDelegate
extension SearchFilterRecipeListVC : TagListViewDelegate {
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
}





//MARK: - API CALLS
extension SearchFilterRecipeListVC {
//    (MM/dd/yyyy)
    func getFilterListApiCall(compHandler: @escaping (Bool)->()) {

        apde.showLoader()
        
        getFilterList() { (res) in
            switch res {
            case .success(_, let dictData,_) :
                
                
                let jsonData = JSON.init(dictData)

                var objFilterListModel = FilterListModel.init()

                // categories
                if let arrCat = jsonData["categories"].array {

                    var arrFilterCategoriesModel = [FilterListModel.FilterCategoriesModel]()
                    for dictCat in arrCat {
                        var objFilterCategoriesModel = FilterListModel.FilterCategoriesModel.init()
                        objFilterCategoriesModel.category_id = dictCat["category_id"].int64Value
                        objFilterCategoriesModel.category_name = dictCat["category_name"].stringValue

                        var arrFilterTagsModel = [FilterTagsModel]()
                        if let arrTags = dictCat["tags"].array {
                            for dictTag in arrTags {
                                var objFilterTagsModel = FilterTagsModel()
                                objFilterTagsModel.tag_image_url =  dictTag["tag_image_url"].stringValue
                                objFilterTagsModel.tag_name =  dictTag["tag_name"].stringValue
                                objFilterTagsModel.tag_id =  dictTag["tag_id"].int64Value
                                arrFilterTagsModel.append(objFilterTagsModel)
                            }
                        }
                        objFilterCategoriesModel.tags = arrFilterTagsModel
                        arrFilterCategoriesModel.append(objFilterCategoriesModel)
                    }
                    objFilterListModel.categories = arrFilterCategoriesModel
                }

//                // unit_option
//                if let arrUnitOption = jsonData["unit_option"].array {
//                    for dictUnitOption in arrUnitOption {
//                        let
//                    }
//                }

                // unit_option
                var arrFilterTagsModel = [FilterTagsModel]()

                if let arrTimeOption = jsonData["time_option"].array {
                    for dictTimeOption in arrTimeOption {
                        var objFilterTagsModel = FilterTagsModel.init()
                        objFilterTagsModel.tag_name = dictTimeOption["tag_name"].stringValue
                        objFilterTagsModel.tag_image_url = dictTimeOption["tag_image_url"].stringValue
                        objFilterTagsModel.tag_id = dictTimeOption["tag_id"].int64Value
                        arrFilterTagsModel.append(objFilterTagsModel)
                    }
                }

                objFilterListModel.time_option = arrFilterTagsModel

                // Sort Time Option
                var mutArrTags = self.objFilterList.time_option
                mutArrTags.sort { (t1, t2) -> Bool in
                    (t1.tag_name).localizedCompare(t2.tag_name) == .orderedAscending
                }
                self.objFilterList.time_option = mutArrTags


                self.objFilterList = objFilterListModel

                self.tblViewSelectCategories?.reloadData()
                compHandler(true)
                

                
//                if let jsonData = dictData.getJsonData() {
//
//                    let decoder = JSONDecoder()
//                    do {
//
//                        let objFilterList = try decoder.decode(FilterListModel.self, from: jsonData)
//                        print(objFilterList)
//
//
//                        self.objFilterList = objFilterList
//
//                        var mutArrTags = self.objFilterList.time_option
//                        mutArrTags.sort { (t1, t2) -> Bool in
//                            (t1.tag_name).localizedCompare(t2.tag_name) == .orderedAscending
//                        }
//                        self.objFilterList.time_option = mutArrTags
//
//                        self.tblViewSelectCategories?.reloadData()
//                        compHandler(true)
//                    } catch {
//                        print(error)
//                        compHandler(false)
//                    }
//                }
                
                
                break
            case .failed(message: let msg):
                compHandler(false)
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                compHandler(false)
                BasicFunctions.showError(strError: error)
                break
            }
        }
        
        
        apde.hideLoader()

    }
}



struct FilterListModel : Decodable {
    
    var categories                          = [FilterCategoriesModel]()
    var time_option                         = [FilterTagsModel]()
    
    struct FilterCategoriesModel : Decodable {
        var category_id                         = Int64()
        var category_name                       = String()
        var tags                                = [FilterTagsModel]()
    }
      
}

struct FilterTagsModel : Decodable {
    
    private enum CodingKeys : String, CodingKey {
        case tag_id, tag_name, tag_image_url
    }
    
    var tag_id                              = Int64()
    var tag_name                            = String()
    var tag_image_url                       = String()
    var is_selected                         = false
}
