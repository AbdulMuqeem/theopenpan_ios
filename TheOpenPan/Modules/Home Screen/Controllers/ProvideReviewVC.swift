//
//  ProvideReviewVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/15/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation

class ProvideReviewVC: UIViewController {

    @IBOutlet weak var viewBackAlpha        : UIView!
    @IBOutlet weak var viewCenter           : UIView!
    @IBOutlet weak var lblTitle             : UILabel!
    @IBOutlet weak var lblDesc              : UILabel!
    @IBOutlet weak var btnLeft              : UIButton!
    @IBOutlet weak var btnRight             : UIButton!
    @IBOutlet weak var txtViewReview        : UITextView!
    @IBOutlet weak var cntrWidth            : NSLayoutConstraint!
    @IBOutlet var arrButtonsStars           : [UIButton]!
    
    typealias Block                         = (Bool)->()
    var compBlock                           :Block!
    var recipe_id                           = Int64()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupLayout()
    }
    
    func setupLayout() {
        
        if ssw <= 320 {
            self.cntrWidth.constant = 290
        }
        
        self.viewBackAlpha.alpha = 0
        
        self.btnLeft.mk_addTapHandler { (btn) in
            hapSoft()
            self.dismiss(false)
        }
        self.btnRight.mk_addTapHandler { (btn) in
            hapSoft()
            
            
            var intRating = 0
            
            for (i,btnStar) in self.arrButtonsStars.enumerated() {
                if btnStar.isSelected {
                    intRating = i + 1
                }
            }
            
            if intRating == 0 {
                self.arrButtonsStars.first?.superview?.shake()
                return
            }
            
            self.submitReviewApiCall(recipe_id: self.recipe_id, rating: intRating, review: self.txtViewReview.text ?? "")
            
            self.dismiss(true)
        }
        self.viewBackAlpha.addTapGesture { (gesture) in
            hapSoft()
            self.dismiss(false)
        }
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.showBackView()
    }

    func showBackView() {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 1
        }) { (s) in
            
        }
    }
    
    func dismiss(_ boolValue:Bool) {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 0
        }) { (s) in
            self.dismiss(animated: true) {
                self.compBlock(boolValue)
            }
            
        }
    }
    
    func dismiss(_ boolValue:Bool, compHandler: @escaping ()->()) {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 0
        }) { (s) in
            self.dismiss(animated: true) {
                compHandler()
                self.compBlock(boolValue)
            }
            
        }
    }

    @IBAction func btnStarTapped(_ sender: UIButton) {
        hapHeavy()
        
        for btn in self.arrButtonsStars {
            btn.isSelected = false
        }
        
        var i = 1
        for (index,btn) in self.arrButtonsStars.enumerated() {
            if btn == sender {
                i = index+1
            }
        }
        for index in 0...i-1 {
            self.arrButtonsStars[index].isSelected = true
        }
        
    }
    
    

}


//MARK: - API CALLS
extension ProvideReviewVC {
    func submitReviewApiCall(recipe_id: Int64, rating: Int, review: String) {

        apde.showLoader()
        submitReview(recipe_id: recipe_id, rating: rating, review: review) { (res) in
            switch res {
            case .success(let msg):
                                
                self.dismiss(true) {
                    BasicFunctions.showInfo(strError: msg)
                }
                    
                
//                BasicFunctions.showPopupOneButton(title: "Review & Rating".localize(), desc: msg) { self.pop() }
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                break
            }
        }
        
        apde.hideLoader()

    }
}


////MARK: - API CALLS
//extension NotificationsVC {
//    func forgotPasswordApiCall(cellForgotPass:CellForgotPass) {
//
//        apde.showLoader()
//        
//        getNotificationList { (res) in
//            
//            switch res {
//            case .success(message: let msg):
//                
//
//                
//                break
//            case .failed(message: let msg):
//                BasicFunctions.showError(strError: msg)
//                break
//            case .error(error: let error):
//                BasicFunctions.showError(strError: error)
//                break
//            }
//            apde.hideLoader()
//        }
//        
//    }
//}
