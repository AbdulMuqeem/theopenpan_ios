//
//  RecipeListVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/15/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import CCBottomRefreshControl
import DZNEmptyDataSet
import IQKeyboardManagerSwift
import Alamofire

class RecipeListVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet var collView          : UICollectionView!
    @IBOutlet var tblView           : UITableView!
    
    //------------------------------------------
    //MARK: - Class Variables -
    var viewTitleSearchBar          : UIView!
    var category_id                 = Int64()
    var time_option_id              = Int64()
    var isOnePotSelected            = Bool()
    var arrRecipeListByCategory     = [RecipeListByCategoryModel]()
    var arrRecipeListRecentSearched = [RecipeListByCategoryModel]()
    
    var fromIndex                   = 0
    let batchSize                   = 30
    var is_last                     = false
    
    var objSearchFilterRecipeListVC : SearchFilterRecipeListVC!
    var timerSearch                 : Timer!
    var isDirectSearch              = false
    var isDirectSearch_sub          = false
    var isFromCookBook              = false
    var collection_id               = Int64()
    var lastDataRequest             : DataRequest?
    var apiRunning                  = false
    
    @IBOutlet var viewSearchKeywordTop              : UIView!
    @IBOutlet var cntrHeightViewSearchKeywordTop    : NSLayoutConstraint!
    @IBOutlet var lblSearchKeywordTop               : UILabel!
    @IBOutlet var btnClear                          : UIButton!
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if self.isDirectSearch {
            self.isDirectSearch_sub = self.isDirectSearch
            self.collView.isHidden = true
            Timer.after(1.2) {
                self.collView.isHidden = false
            }
        }
        
        self.setupLayout()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        changeTabBar(hidden: true, animated: false, completion: nil)
        
        self.objSearchFilterRecipeListVC = apde.getController(vc_name: "SearchFilterRecipeListVC", sb: .Home) as? SearchFilterRecipeListVC
        
        
        self.objSearchFilterRecipeListVC.getFilterListApiCall { (success) in
            print(success)
            
            if success {
                
                for (i,cat) in self.objSearchFilterRecipeListVC.objFilterList.categories.enumerated() {
                    for (j,tag) in cat.tags.enumerated() {
                        print("\(self.category_id) --- \(tag.tag_id)")
                        if tag.tag_id == self.category_id {
                            self.objSearchFilterRecipeListVC.objFilterList.categories[i].tags[j].is_selected = true
                        }
                    }
                }
                
                for (i,time_option) in self.objSearchFilterRecipeListVC.objFilterList.time_option.enumerated() {
                    if time_option.tag_id == self.time_option_id {
                        self.objSearchFilterRecipeListVC.objFilterList.time_option[i].is_selected = true
                    }
                }
                
                self.objSearchFilterRecipeListVC.isOnePotMealsSelected = self.isOnePotSelected
                
                apde.showLoader()
                self.fromIndex = 0
                self.is_last = false
                self.arrRecipeListByCategory.removeAll()
                self.collView.reloadData()
                self.loadMoreItems()
            }
        }
        
        self.btnClear.onTap {
            
            BasicFunctions.showPopupTwoButton(title: "Clear Recent Search", desc: "Are you sure you want to clear all recent searched recipes?", btnTitleLeft: "NO", btnTitleRight: "YES", baseVC: self) { (isYes) in
                if isYes {
                    
                    clearRecentSearch { (res) in
                        switch res {
                        case .success(_, let message) :
                            BasicFunctions.showInfo(strError: message)
                            self.getRecentSearchRecipeListApiCall(start: 0, limit: 100)
                            break
                        case .failed(message: let msg):
                            BasicFunctions.showError(strError: msg)
                            break
                        case .error(error: let error):
                            BasicFunctions.showError(strError: error)
                            break
                        }
                        apde.hideLoader()
                    }
                    
                }
            }
            
            
        }
        
        
    }


    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        self.hideSearchTopHeader()
        
        if self.isFromCookBook == false {
            self.removeSearchBarAndAddRightBarButtons()
        }
        
        self.viewSearchKeywordTop.isHidden = false
        
        setupCollView()
        setupTableView()
        
        self.tblView.mk_animateAlpha(sec: 0.5, alpha: 0.0)
        self.lblSearchKeywordTop.text = "".localize()
        self.viewSearchKeywordTop.mk_animateAlpha(sec: 0.5, alpha: 0.0)
        self.view.mk_animateLayout(sec: 0.5)
    }
    
    func hideSearchTopHeader() {
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        IQKeyboardManager.shared.enable = false
        
        Timer.after(0.3) {
            if self.isDirectSearch {
                self.isDirectSearch = false
                self.addSearchBar()
                let txtFieldSearchBar = self.viewTitleSearchBar.viewWithTag(10) as! UITextField
                txtFieldSearchBar.returnKeyType = .search
                txtFieldSearchBar.becomeFirstResponder()
            }
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
    }
    

    
    func loadMoreItems() {
        self.collView.bottomRefreshControl?.beginRefreshing()
        self.lastDataRequest?.cancel()
        if self.isFromCookBook {
            self.getCollectionsRecipeListApiCall(start: fromIndex, limit: batchSize)
        } else {
            self.getRecipeListByCategoriesApiCall(start: fromIndex, limit: batchSize)
        }
        
    }
    
    @objc func didTapFilterButton(sender: UIBarButtonItem){
        
        changeTabBar(hidden: true, animated: false, completion: nil)
        
        self.objSearchFilterRecipeListVC.blockDone = {
            apde.showLoader()
            self.fromIndex = 0
            self.is_last = false
            self.arrRecipeListByCategory.removeAll()
            self.collView.reloadData()
            self.collView.reloadEmptyDataSet()
            self.loadMoreItems()
        }
        
        self.navigationController?.pushViewController(self.objSearchFilterRecipeListVC, animated: true)
    }

    @objc func didTapSearchButton(sender: UIBarButtonItem){
        addSearchBar()
    }
    
    func addSearchBar() {
        
        self.viewTitleSearchBar = UIView.fromNib(nibName: "ViewTitleSearchBar")
        self.viewTitleSearchBar.sizeToFit()
        navigationItem.titleView = self.viewTitleSearchBar
        
        if #available(iOS 11, *) {
            self.viewTitleSearchBar.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint(item: self.viewTitleSearchBar as Any, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: ssw - 132).isActive = true
            NSLayoutConstraint(item: self.viewTitleSearchBar as Any, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 44).isActive = true
            
        } else {
            self.viewTitleSearchBar.frame = .init(x: 0, y: 0, width: ssw - 132, height: 44)
        }
        
        let imgCancelLeftBarButton = UIImage(named: "AssetCancelBarButton")!
        
        let cancelLeftBarButton = UIBarButtonItem(image: imgCancelLeftBarButton,  style: .plain, target: self, action: #selector(self.removeSearchBarAndAddRightBarButtonsAfterLayout))
        
        self.navigationItem.setRightBarButtonItems(nil, animated: true)
        self.navigationItem.setRightBarButton(nil, animated: true)
        
        let txtFieldSearchBar = self.viewTitleSearchBar.viewWithTag(10) as! UITextField
        txtFieldSearchBar.tintColor = .black
        txtFieldSearchBar.returnKeyType = .search
        txtFieldSearchBar.becomeFirstResponder()
        let indicator = self.viewTitleSearchBar.viewWithTag(1011) as? UIActivityIndicatorView
        indicator?.stopAnimating()
        
        txtFieldSearchBar.onReturn {
            self.collView.mk_animateAlpha(sec: 0.3, alpha: 0.0)
            self.objSearchFilterRecipeListVC.clearFilters()
            Timer.after(0.3) {
                indicator?.startAnimating()
                self.textFieldDidEndEditing(txtFieldSearchBar)
            }
        }
        
        self.tblView.mk_animateAlpha(sec: 0.5, alpha: 1.0)
        self.lblSearchKeywordTop.text = "Recently Viewed Recipes".localize()
        self.viewSearchKeywordTop.mk_animateAlpha(sec: 0.5, alpha: 1.0)
        self.view.mk_animateLayout(sec: 0.5)
        
        self.getRecentSearchRecipeListApiCall(start: 0, limit: 100)
        
        let filterImage  = UIImage(named: "AssetBarButtonFilter")!
        let filterButton = UIBarButtonItem(image: filterImage,  style: .plain, target: self, action: #selector(self.didTapFilterButton(sender:)))

        if isFromCookBook == false {
            navigationItem.leftBarButtonItems   = [cancelLeftBarButton]
            navigationItem.rightBarButtonItems  = [filterButton]
        }
        
    }
    
    @objc func removeSearchBarAndAddRightBarButtonsAfterLayout() {
        
        if isDirectSearch_sub {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        self.removeSearchBarAndAddRightBarButtons()
        
        let txtFieldSearchBar = self.viewTitleSearchBar.viewWithTag(10) as! UITextField
        txtFieldSearchBar.text = ""
        
        
        
        if (txtFieldSearchBar.text?.count ?? 0) > 0 {
            
            self.tblView.mk_animateAlpha(sec: 0.5, alpha: 1.0)
            self.lblSearchKeywordTop.text = "Recently Viewed Recipes".localize()
            self.viewSearchKeywordTop.mk_animateAlpha(sec: 0.5, alpha: 1.0)
            self.view.mk_animateLayout(sec: 0.5)
            
        } else {
            self.tblView.mk_animateAlpha(sec: 0.5, alpha: 0.0)
            self.lblSearchKeywordTop.text = "".localize()
            self.viewSearchKeywordTop.mk_animateAlpha(sec: 0.5, alpha: 0.0)
            self.view.mk_animateLayout(sec: 0.5)
        }
        
        self.fromIndex = 0
        self.is_last = false
        self.arrRecipeListByCategory.removeAll()
        self.collView.reloadData()
        self.loadMoreItems()
    }
    
    @objc func removeSearchBarAndAddRightBarButtons() {
        
        if self.viewTitleSearchBar != nil {
            self.navigationItem.setLeftBarButtonItems(nil, animated: true)
            self.navigationItem.setLeftBarButton(nil, animated: true)
            self.navigationItem.titleView = nil
        }
        
        let filterImage  = UIImage(named: "AssetBarButtonFilter")!
        let searchImage  = UIImage(named: "AssetSearchIcon")!

        let filterButton = UIBarButtonItem(image: filterImage,  style: .plain, target: self, action: #selector(self.didTapFilterButton(sender:)))
        let searchButton = UIBarButtonItem(image: searchImage,  style: .plain, target: self, action: #selector(self.didTapSearchButton(sender:)))

        navigationItem.rightBarButtonItems = [filterButton, searchButton]
        
        self.hideSearchTopHeader()
        
        
    }
    
}


//------------------------------------------
//MARK: - Table View Setup Methods -
extension RecipeListVC : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func setupCollView() {
        
        
        self.collView.delegate = self
        self.collView.dataSource = self
        
        
        if #available(iOS 11.0, *) {
            self.collView.contentInsetAdjustmentBehavior = .never
        }
        
        
        let layout = RDHCollectionViewGridLayout.init()
        layout.itemSpacing = 10
        layout.lineSpacing = 10
        
       
        if UIDevice.current.userInterfaceIdiom == .pad {
            layout.lineItemCount = 3
            layout.lineSize = ((ssw - 26) / 3) + 90
        } else {
            layout.lineItemCount = 2
            layout.lineSize = ((ssw - 26) / 2) + 90
        }
        
        layout.scrollDirection = .vertical
        layout.sectionsStartOnNewLine = false
        
        self.collView.collectionViewLayout = layout
        
        let refreshControl = UIRefreshControl.init()
        refreshControl.triggerVerticalOffset = 10
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        self.collView.bottomRefreshControl = refreshControl
        
    }
    
    @objc func refresh() {
        if self.is_last {
//            self.collView.bottomRefreshControl?.endRefreshing()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrRecipeListByCategory.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellRecipeList", for: indexPath) as! CellRecipeList
        
        cell.handleCell(vc: self, ip: indexPath)
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == self.arrRecipeListByCategory.count - 1 { // last cell
            if self.is_last == false && self.arrRecipeListByCategory.count > 10 {
                self.loadMoreItems()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let txtFieldSearchBar = self.viewTitleSearchBar?.viewWithTag(10) as? UITextField
        if (txtFieldSearchBar?.text ?? "").count > 0 {
            
            // SEARCHED RECIPE
            if self.arrRecipeListByCategory[indexPath.row].is_edamam.mk_boolValue {
                // edmam searched RECIPE
                self.goToRecipeDetails(indexPath: indexPath)
            } else {
                // DB searched RECIPE
                apde.showLoader()
                addRecipeToRecentSearch(recipe_id: self.arrRecipeListByCategory[indexPath.row].id) { (res) in
                    apde.hideLoader()
                    self.goToRecipeDetails(indexPath: indexPath)
                }
            }
            
        } else {
            
            // NORMAL RECIPE
            self.goToRecipeDetails(indexPath: indexPath)
        }
        
    }
    
    
    func checkEdmamRecipeExistsInCookBookByUrlApiCall(indexPath: IndexPath, compBlock: @escaping (Int64)->()) {
        let objRecipe = self.arrRecipeListByCategory[indexPath.row]
        checkEdmamRecipeExistsInCookBookByUrl(url: objRecipe.web_url) { (res) in
            switch res {
            case .success(let dictData,_) :
                print(dictData)
                if let recipe_id = dictData["recipe_id"] as? Int64 {
                    compBlock(recipe_id)
                } else {
                    compBlock(0)
                }
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                break
            }
            
            apde.hideLoader()
        }
    }
    
    
    func goToRecipeDetails(indexPath: IndexPath) {
        let objRecipeDetailsVC = apde.getController(vc_name: "RecipeDetailsVC", sb: .RecipeDetailsVC) as! RecipeDetailsVC
        objRecipeDetailsVC.recipe_id = self.arrRecipeListByCategory[indexPath.row].id
        objRecipeDetailsVC.isFromCookBook = self.isFromCookBook
        objRecipeDetailsVC.isFromSearch = true
        if self.isFromCookBook {
            objRecipeDetailsVC.isFromSearch = false
        }
        
        if self.arrRecipeListByCategory[indexPath.row].is_edamam.mk_boolValue {
            
            if kCurrentUser.is_guest {
                
                objRecipeDetailsVC.objRecipeListByCategory = self.arrRecipeListByCategory[indexPath.row]
                objRecipeDetailsVC.isThird = true
                self.blockForDeletedAndUpdated(objRecipeDetailsVC: objRecipeDetailsVC, indexPath: indexPath)
                
            } else {
                
                self.checkEdmamRecipeExistsInCookBookByUrlApiCall(indexPath: indexPath) { (recipe_id) in
                    if recipe_id == 0 {
                        objRecipeDetailsVC.objRecipeListByCategory = self.arrRecipeListByCategory[indexPath.row]
                        objRecipeDetailsVC.isThird = true
                    } else {
                        objRecipeDetailsVC.recipe_id = recipe_id
                        objRecipeDetailsVC.isThird = false
                    }
                    self.blockForDeletedAndUpdated(objRecipeDetailsVC: objRecipeDetailsVC, indexPath: indexPath)
                }
            }
            
        } else {
            self.blockForDeletedAndUpdated(objRecipeDetailsVC: objRecipeDetailsVC, indexPath: indexPath)
        }
        
        
    }
    
    
    
    func blockForDeletedAndUpdated(objRecipeDetailsVC:RecipeDetailsVC, indexPath: IndexPath) {
        objRecipeDetailsVC.recipeDeletedOrUpdatedBlock = { (updatedRecipeDetails, isReloadWhole) in
            
            if isReloadWhole == false {
                self.arrRecipeListByCategory[indexPath.row].ratings = updatedRecipeDetails.ratings
                self.collView.reloadItems(at: [indexPath])
                return
            }
            
            apde.showLoader()
            self.fromIndex = 0
            self.is_last = false
            self.arrRecipeListByCategory.removeAll()
            self.collView.reloadData()
            self.loadMoreItems()
        }
        self.navigationController?.pushViewController(objRecipeDetailsVC, animated: true)
    }
    
}


extension RecipeListVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
        self.tblView.tableFooterView = UIView()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRecipeListRecentSearched.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellRecentSearch = tableView.dequeueReusableCell(withIdentifier: "CellRecentSearch", for: indexPath) as! CellRecentSearch
        cellRecentSearch.handleCell(vc: self, ip: indexPath)
        return cellRecentSearch
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        addRecipeToRecentSearch(recipe_id: self.arrRecipeListRecentSearched[indexPath.row].id) { (res) in
        }
        let objRecipeDetailsVC = apde.getController(vc_name: "RecipeDetailsVC", sb: .RecipeDetailsVC) as! RecipeDetailsVC
        objRecipeDetailsVC.recipe_id = self.arrRecipeListRecentSearched[indexPath.row].id
        objRecipeDetailsVC.isFromCookBook = self.isFromCookBook
        objRecipeDetailsVC.recipeDeletedOrUpdatedBlock = { (updatedRecipeDetails, isReloadWhole) in
            self.getRecentSearchRecipeListApiCall(start: 0, limit: 100)
        }
        self.navigationController?.pushViewController(objRecipeDetailsVC, animated: true)
        
    }
    
}


extension RecipeListVC : DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        if self.isFromCookBook {
            return "No recipes available...".AttributedStringFont(enum_font.medium.font(20), fontColor: #colorLiteral(red: 0.7450980392, green: 0.6823529412, blue: 0.6549019608, alpha: 1))
        }
        
        return "Can't find what you're looking for?\nBrowse and save recipes from the internet here...".AttributedStringFont(enum_font.medium.font(20), fontColor: #colorLiteral(red: 0.7450980392, green: 0.6823529412, blue: 0.6549019608, alpha: 1))
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> NSAttributedString! {
        
        
        if self.isFromCookBook {
            return "".AttributedStringFont(enum_font.medium.font(22), fontColor: #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1))
        }
        return "Take Me There".AttributedStringFont(enum_font.medium.font(22), fontColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
    }
    
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        
        if self.isFromCookBook {
            return
        }
        
        if let tabBarVC = self.tabBarController {
            tabBarVC.selectedIndex = 2
            if let navHomeController = tabBarVC.viewControllers?[2] as? UINavigationController {
                navHomeController.popToRootViewController(animated: false)
                if let hc = navHomeController.viewControllers.first as? HomeController {
                    hc.btnSegament2Tapped(btn: hc.btnSegment2)
                }
            }
        }
    }
    
    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> UIImage! {
        
        let imagename = "EmptyDataButtonBack"
        let capInsets = UIEdgeInsets.init(top: 25.0, left: 25.0, bottom: 25.0, right: 25.0)
        let rectInsets = UIEdgeInsets.init(top: 0.0, left: 10, bottom: 0.0, right: 10)
        return UIImage.init(named: imagename)!.resizableImage(withCapInsets: capInsets, resizingMode: .stretch).withAlignmentRectInsets(rectInsets)
    }
}


extension RecipeListVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.lastDataRequest?.cancel()
            
        if ((textField.text ?? "").count) > 0 {
                self.tblView.mk_animateAlpha(sec: 0.5, alpha: 0.0)
                self.lblSearchKeywordTop.text = "".localize()
                self.viewSearchKeywordTop.mk_animateAlpha(sec: 0.5, alpha: 0.0)
                self.view.mk_animateLayout(sec: 0.5)
            } else {
                self.tblView.mk_animateAlpha(sec: 0.5, alpha: 1.0)
                self.lblSearchKeywordTop.text = "Recently Viewed Recipes".localize()
                self.viewSearchKeywordTop.mk_animateAlpha(sec: 0.5, alpha: 1.0)
                self.view.mk_animateLayout(sec: 0.5)
                self.getRecentSearchRecipeListApiCall(start: 0, limit: 100)
            }
            
            self.fromIndex = 0
            self.is_last = false
            self.arrRecipeListByCategory.removeAll()
            self.collView.reloadData()
            self.loadMoreItems()

        self.view.layoutIfNeeded()
    }
}




//MARK: - API CALLS // IS NOT FROM COOKBOOK
extension RecipeListVC {
    
    func getRecipeListByCategoriesApiCall(start:Int, limit:Int) {
        
        if apiRunning {
            return
        }
        self.apiRunning = true
        
        let arrCats = self.objSearchFilterRecipeListVC.objFilterList.categories
        
        var category_ids = [String]()
        var lastCatName = ""
        for cat in arrCats {
            for tag in cat.tags {
                if tag.is_selected {
                    category_ids.append("\(tag.tag_id)")
                    lastCatName = tag.tag_name
                }
            }
        }
        var str_category_ids = category_ids.joined(separator: ",")
        
        // time_id
        let arrTimeOptions = self.objSearchFilterRecipeListVC.objFilterList.time_option
        
        var arr_time_ids = [String]()
        var str_time_id = String()
        var lastTimeCatName = ""
        for tag in arrTimeOptions {
            if tag.is_selected {
                str_time_id = "\(tag.tag_id)"
                arr_time_ids.append("\(tag.tag_id)")
                lastTimeCatName = tag.tag_name
            }
        }
        str_time_id = arr_time_ids.joined(separator: ",")
        
        
        let is_one_pot_meal = self.objSearchFilterRecipeListVC.isOnePotMealsSelected
        let is_easy = self.objSearchFilterRecipeListVC.isEasy
        
        var search_keyword : String?
        
        let txtFieldSearchBar = self.viewTitleSearchBar?.viewWithTag(10) as? UITextField
        if txtFieldSearchBar?.isEmpty() == false {
            search_keyword = txtFieldSearchBar?.text ?? ""
        }
        
        print(str_category_ids)
        if str_category_ids == "" {
            if self.category_id != 0 {
                str_category_ids = "\(self.category_id)"
            }
        }
        
        if str_time_id == "" {
            if self.time_option_id != 0 {
                str_time_id = "\(self.time_option_id)"
            }
        }
        
        self.lastDataRequest = getRecipeListByCategories(start: start, limit: limit, category_id: str_category_ids, time_id:str_time_id, is_one_pot_meal: is_one_pot_meal, is_easy:is_easy, search_keyword:search_keyword) { (res) in
            switch res {
            case .success(dictData: let dictData, message: _) :
                
                let currentIndexPath = self.arrRecipeListByCategory.count - 1
                
                var items = self.arrRecipeListByCategory
                
                if let arrDict = dictData["recipe_list"] as? Array<Dictionary<String,Any>> {
                    
                    for dict in arrDict {
                        let id              = (dict["id"] as? Int64) ?? 0
                        let name            = (dict["name"] as? String) ?? ""
                        let creator_name    = (dict["creator_name"] as? String) ?? ""
                        let ratings         = (dict["ratings"] as? Double) ?? 0.0
                        let media_type      = (dict["media_type"] as? Int) ?? 0
                        let media_url       = (dict["media_url"] as? String) ?? ""
                        let video_thumb_url = (dict["video_thumb_url"] as? String) ?? ""
                        let web_url                         = (dict["web_url"] as? String) ?? ""
                        let view_url                         = (dict["view_url"] as? String) ?? ""
                        let yield                           = (dict["yield"] as? Int) ?? 0
                        let ingredientLines                 = (dict["ingredientLines"] as? [String]) ?? [""]
                        let is_edamam                       = (dict["is_edamam"] as? Int) ?? 0
                        var is_from_scrapper                = (dict["is_from_scrapper"] as? Int) ?? 0
                        
                        var ready_time_hours                : Int = 0
                        var ready_time_mins                 : Int = 0
                        
                        if let str_hours = (dict["ready_time_hours"] as? String) {
                            ready_time_hours = str_hours.integer
                        }
                        if let str_mins = (dict["ready_time_mins"] as? String) {
                            ready_time_mins = str_mins.integer
                        }
                        
                        ready_time_hours                = (dict["ready_time_hours"] as? Int) ?? 0
                        ready_time_mins                 = (dict["ready_time_mins"] as? Int) ?? 0
                        
                        if ingredientLines.count > 0 {
                            is_from_scrapper = 1
                        }
                        
                        let objRecipeListByCategoryModel = RecipeListByCategoryModel.init(id: id, name: name, creator_name: creator_name, ratings: ratings, media_type: media_type, media_url: media_url, video_thumb_url: video_thumb_url, recipe_status: 0, view_url: view_url, web_url: web_url, yield: yield, ingredientLines:ingredientLines, is_edamam: is_edamam, is_from_scrapper:is_from_scrapper, ready_time_hours:ready_time_hours, ready_time_mins:ready_time_mins)
                        
                        items.append(objRecipeListByCategoryModel)
                    }
                    
                }
                

                if self.fromIndex < items.count {
                    
                    self.arrRecipeListByCategory = items
                    self.fromIndex = items.count
                    var arrIndexPaths = [IndexPath]()
                    
                    for i in currentIndexPath+1...self.arrRecipeListByCategory.count - 1 {
                        arrIndexPaths.append(IndexPath.init(row: i, section: 0))
                    }
                        
                    self.collView.insertItems(at: arrIndexPaths)
                    
                }
                self.collView.bottomRefreshControl?.endRefreshing()

                if let isLast = (dictData["is_last"] as? Int) {
                    self.is_last = isLast.mk_boolValue
                    if self.is_last {
                        self.collView.bottomRefreshControl?.triggerVerticalOffset = 20
                        self.collView.bottomRefreshControl = nil
                    }
                }
                self.collView.emptyDataSetSource = self
                self.collView.emptyDataSetDelegate = self
                self.collView.reloadEmptyDataSet()
                
                print("------------------------------")
                print(self.arrRecipeListByCategory.count)
                print("------------------------------")
                
                self.tblView.reloadData()
                
                break
            case .error(error: let err) :
                BasicFunctions.showError(strError: err)
                break
            case .failed(message: let msg) :
                BasicFunctions.showError(strError: msg)
                break
            }
            apde.hideLoader()
            self.apiRunning = false
            let indicator = self.viewTitleSearchBar?.viewWithTag(1011) as? UIActivityIndicatorView
            indicator?.stopAnimating()
            self.collView.mk_animateAlpha(sec: 0.3, alpha: 1.0)
        }
    }
}

//{
//    "id": 230,
//    "name": "Soon Kueh 笋粿",
//    "creator_name": "Shefali patel",
//    "ratings": 0,
//    "media_type": 2,
//    "media_url": "https://www.youtube.com/embed/AOIZ_YcQlBg?modestbranding=1&autohide=1&showinfo=0",
//    "video_thumb_url": "http://img.youtube.com/vi/AOIZ_YcQlBg/hqdefault.jpg",
//    "recipe_status": 3,
//    "web_url": "http://mysingaporefood.com/recipe/soon-kueh/",
//    "yield": 0,
//    "ingredientLines": [
//        "2kg turnip (about 5 small to medium-size turnips)",
//        "1 tin bamboo shoots",
//        "100g dried shrimps, washed",
//        "500g rice flour",
//        "750g tapioca flour",
//        "2 to 3 cloves garlic, minced",
//        "1 ½ tsp salt",
//        "1 to 2 tsp pepper",
//        "5 tbs oil",
//        "2.1 litres hot water",
//        "450ml room-temperature water"
//    ],
//    "is_edamam": 0,
//    "is_from_scrap": 1
//},




//MARK: - // IS FROM COOKBOOK
extension RecipeListVC {
    
    func getCollectionsRecipeListApiCall(start:Int, limit:Int) {
        
        if self.apiRunning { return }
        self.apiRunning = true
        
        let arrCats = self.objSearchFilterRecipeListVC.objFilterList.categories
        
        var category_ids = [String]()
        var lastCatName = ""
        for cat in arrCats {
            for tag in cat.tags {
                if tag.is_selected {
                    category_ids.append("\(tag.tag_id)")
                    lastCatName = tag.tag_name
                }
            }
        }
        var str_category_ids = category_ids.joined(separator: ",")
        
        // time_id
        let arrTimeOptions = self.objSearchFilterRecipeListVC.objFilterList.time_option
        
        var arr_time_ids = [String]()
        var str_time_id = String()
        
        for tag in arrTimeOptions {
            if tag.is_selected {
                str_time_id = "\(tag.tag_id)"
                arr_time_ids.append("\(tag.tag_id)")
            }
        }
        str_time_id = arr_time_ids.joined(separator: ",")
        
        let is_one_pot_meal = self.objSearchFilterRecipeListVC.isOnePotMealsSelected
        let is_easy = self.objSearchFilterRecipeListVC.isEasy
        
        var search_keyword : String?
        
        let txtFieldSearchBar = self.viewTitleSearchBar?.viewWithTag(10) as? UITextField
        if txtFieldSearchBar?.isEmpty() == false {
            search_keyword = txtFieldSearchBar?.text ?? ""
        }
        
        print(str_category_ids)
        if str_category_ids == "" {
            if self.category_id != 0 {
                str_category_ids = "\(self.category_id)"
            }
        }
        
        if str_time_id == "" {
            if self.time_option_id != 0 {
                str_time_id = "\(self.time_option_id)"
            }
        }
        
        self.lastDataRequest = getCollectionsRecipeList(start: start, limit: limit, collection_id: self.collection_id , category_id: str_category_ids, time_id:str_time_id, is_one_pot_meal: is_one_pot_meal, is_easy:is_easy, search_keyword:search_keyword) { (res) in
            switch res {
            case .success(dictData: let dictData, message: _) :
                
                
                let currentIndexPath = self.arrRecipeListByCategory.count - 1
                
                var items = self.arrRecipeListByCategory

                if let arrDict = dictData["recipe_list"] as? Array<Dictionary<String,Any>> {
                    
                    for dict in arrDict {
                        let id              = (dict["id"] as? Int64) ?? 0
                        let name            = (dict["name"] as? String) ?? ""
                        let creator_name    = (dict["creator_name"] as? String) ?? ""
                        let ratings         = (dict["ratings"] as? Double) ?? 0.0
                        let media_type      = (dict["media_type"] as? Int) ?? 0
                        let media_url       = (dict["media_url"] as? String) ?? ""
                        let video_thumb_url = (dict["video_thumb_url"] as? String) ?? ""
                        let web_url         = (dict["web_url"] as? String) ?? ""
                        let view_url        = (dict["view_url"] as? String) ?? ""
                        
                        let objRecipeListByCategoryModel = RecipeListByCategoryModel.init(id: id, name: name, creator_name: creator_name, ratings: ratings, media_type: media_type, media_url: media_url, video_thumb_url: video_thumb_url, recipe_status: 0, view_url: view_url, web_url: web_url)
                        
                        items.append(objRecipeListByCategoryModel)
                    }
                    
                }

                if self.fromIndex < items.count {
                    
                    self.arrRecipeListByCategory = items
                    self.fromIndex = items.count
                    var arrIndexPaths = [IndexPath]()
                    
                    for i in currentIndexPath+1...self.arrRecipeListByCategory.count - 1 {
                        arrIndexPaths.append(IndexPath.init(row: i, section: 0))
                    }
                        
                    self.collView.insertItems(at: arrIndexPaths)
                    
                }
                self.collView.bottomRefreshControl?.endRefreshing()

                if let isLast = (dictData["is_last"] as? Int) {
                    self.is_last = isLast.mk_boolValue
                    if self.is_last {
                        self.collView.bottomRefreshControl?.triggerVerticalOffset = 20
                        self.collView.bottomRefreshControl = nil
                    }
                }
                self.collView.emptyDataSetSource = self
                self.collView.emptyDataSetDelegate = self
                self.collView.reloadEmptyDataSet()
                
                print("------------------------------")
                print(self.arrRecipeListByCategory.count)
                print("------------------------------")
                
                self.tblView.reloadData()
                
                break
            case .error(error: let err) :
                BasicFunctions.showError(strError: err)
                break
            case .failed(message: let msg) :
                BasicFunctions.showError(strError: msg)
                break
            }
            apde.hideLoader()
            self.apiRunning = false
            let indicator = self.viewTitleSearchBar?.viewWithTag(1011) as? UIActivityIndicatorView
            indicator?.stopAnimating()
            self.collView.mk_animateAlpha(sec: 0.3, alpha: 1.0)
        }
    }
}


//MARK: - // Recent Searched Recipes API CALL
extension RecipeListVC {
    
    func getRecentSearchRecipeListApiCall(start:Int, limit:Int) {
        
        getRecentSearchRecipeList(start: start, limit: limit) { (res) in
            switch res {
            case .success(dictData: let dictData, message: _) :
                
                self.arrRecipeListRecentSearched.removeAll()
                var items = self.arrRecipeListRecentSearched
                
                if let arrDict = dictData["recipe_list"] as? Array<Dictionary<String,Any>> {
                    
                    for dict in arrDict {
                        let id              = (dict["id"] as? Int64) ?? 0
                        let name            = (dict["name"] as? String) ?? ""
                        let creator_name    = (dict["creator_name"] as? String) ?? ""
                        let ratings         = (dict["ratings"] as? Double) ?? 0.0
                        let media_type      = (dict["media_type"] as? Int) ?? 0
                        let media_url       = (dict["media_url"] as? String) ?? ""
                        let video_thumb_url = (dict["video_thumb_url"] as? String) ?? ""
                        let web_url         = (dict["web_recipe_url"] as? String) ?? ""
                        let view_url         = (dict["view_url"] as? String) ?? ""
                        let is_from_scrapper = (dict["is_from_scrapper"] as? Int) ?? 0
                        
                        let objRecipeListByCategoryModel = RecipeListByCategoryModel.init(id: id, name: name, creator_name: creator_name, ratings: ratings, media_type: media_type, media_url: media_url, video_thumb_url: video_thumb_url, recipe_status: 0, view_url: view_url, web_url: web_url , is_from_scrapper: is_from_scrapper)
                        
                        items.append(objRecipeListByCategoryModel)
                    }
                    
                }
                
                self.arrRecipeListRecentSearched = items
                print("------------------------------")
                print(self.arrRecipeListRecentSearched.count)
                print("------------------------------")
                
                self.tblView.reloadData()
                
                break
            case .error(error: let err) :
                BasicFunctions.showError(strError: err)
                break
            case .failed(message: let msg) :
                BasicFunctions.showError(strError: msg)
                break
            }
            apde.hideLoader()
        }
    }
}
