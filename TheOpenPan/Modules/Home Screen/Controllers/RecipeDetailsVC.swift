//
//  RecipeDetailsVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/15/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import GSKStretchyHeaderView
import AVFoundation
import AVKit
import SwiftyJSON
import Player
import YoutubePlayer_in_WKWebView

var NrecipeDetailsIngredientSection = 1

class RecipeDetailsVC: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //------------------------------------------
    //MARK: - Outlets -
    
    @IBOutlet weak var btnBack                                  : UIButton!
    @IBOutlet weak var lblTitle                                 : UILabel!
    @IBOutlet weak var stretchyHeaderPlayButton                 : UIButton!
    @IBOutlet weak var stretchyHeaderSoundButton                : UIButton!
    @IBOutlet weak var stretchyHeaderScaleButton                : UIButton!
    @IBOutlet weak var stretchyHeaderRecipeImgView              : UIImageView!
    @IBOutlet weak var stretchyHeaderViewChild                  : UIView!
    @IBOutlet weak var stretchyHeaderGreenView                  : UIView!
    @IBOutlet weak var tblView                                  : UITableView!
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    var recipe_id                                               = Int64()
    var recipeDetails                                           = RecipeDetails()
    var isIngsAddedToShopList                                   = false
    var cellHeights                                             : [IndexPath : CGFloat] = [:]
    var stretchyHeader                                          : GSKStretchyHeaderView!
    var objParsedRes                                            = ParsedRes()
    var isApiNutritionRunning                                   = false
    var isApiNutritionResponseRecieved                          = false
    var player                                                  : Player!
    var recipeDeletedOrUpdatedBlock                             : ((RecipeDetails,Bool)->())?
    var isFromCookBook                                          = false
    let audioSession                                            = AVAudioSession.sharedInstance()
    var volume                                                  = AVAudioSession.sharedInstance().outputVolume
    var isNutritionDataAvailable                                = true
    var isDisplayRejectionNotePopup                             = false
    var jpsVolumeButtonHandler                                  : JPSVolumeButtonHandler!
    var isThird                                                 = false
    var isFromSearch                                            = false
    var objRecipeListByCategory                                 : RecipeListByCategoryModel?
    
    var youtubePlayer                                           : WKYTPlayerView!
    
    var webViewRecipeBrowse                                     : WKWebView!
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        print("deinitdeinitdeinitdeinitdeinitdeinitdeinitdeinitdeinitdeinitdeinitdeinitdeinitdeinit\ndeinitdeinitdeinitdeinitdeinit")
    }
    
    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        self.webViewRecipeBrowse = WKWebView.init()
        
        setupTableView()
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.mk_hideNavBar()
        self.setupLayout()
        
        self.stretchyHeaderRecipeImgView?.image = UIImage.init(named: "AssetCollectionPlaceholder")!
        
        self.btnBack.mk_addTapHandler { (btn) in
            
            self.recipeDeletedOrUpdatedBlock?(self.recipeDetails,false)
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            self.navigationController?.popViewController(animated: true)
            
        }
        
        do {
            try audioSession.setActive(true)
        } catch {
            print("Failed to activate audio session")
        }
        
        self.stretchyHeaderSoundButton.mk_addTapHandler { (btn) in
            hapSoft()
            btn.isSelected = !btn.isSelected
            try? AVAudioSession.sharedInstance().setCategory(.playback)
            try? AVAudioSession.sharedInstance().setActive(true, options: .notifyOthersOnDeactivation)
            if btn.isSelected {
                self.player?.volume = AVAudioSession.sharedInstance().outputVolume
            } else {
                self.player?.volume = 0.0
            }
        }
        
        if self.isThird {
            
            self.parseIngredientsApiCall(mutArrIngredients: objRecipeListByCategory?.ingredientLines ?? [""]) {
                print(self.recipeDetails.ingredient_list)
                
                if let objRecipeListByCategory = self.objRecipeListByCategory {
                    self.recipeDetails.name             = objRecipeListByCategory.name
                    self.recipeDetails.creator_name     = objRecipeListByCategory.creator_name
                    self.recipeDetails.ratings          = objRecipeListByCategory.ratings
                    self.recipeDetails.media_type       = objRecipeListByCategory.media_type
                    self.recipeDetails.media_url                = objRecipeListByCategory.media_url
                    self.recipeDetails.video_thumb_url          = objRecipeListByCategory.video_thumb_url
                    self.recipeDetails.recipe_status            = objRecipeListByCategory.recipe_status
                    self.recipeDetails.web_url                  = objRecipeListByCategory.web_url
                    self.recipeDetails.view_url                  = objRecipeListByCategory.view_url
                    self.recipeDetails.serves                   = objRecipeListByCategory.yield
                    self.recipeDetails.original_serves                      = objRecipeListByCategory.yield
                    self.recipeDetails.ready_time_hours                     = objRecipeListByCategory.ready_time_hours
                    self.recipeDetails.ready_time_mins                     = objRecipeListByCategory.ready_time_mins
                }
                self.tblView.reloadData()
                
                
                if self.recipeDetails.media_type == enum_media_type.image.rawValue {
                    self.player?.view.isHidden = true
                    self.stretchyHeaderScaleButton?.isHidden = true
                    self.stretchyHeaderSoundButton?.isHidden = true
                }
                
                self.lblTitle.text = self.recipeDetails.name
            }
        } else {
            self.getRecipeDetailsApiCall()
        }
        
        if self.stretchyHeader.stretchFactor > 0.3 {
            self.player?.playFromCurrentTime()
        }
        
        self.startObservingVolumeChanges()
        self.stretchyHeaderSoundButton.isSelected = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        changeTabBar(hidden: true, animated: false, completion: nil)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.player?.stop()
        self.player = nil
        UIViewController.CVC()?.navigationController?.isNavigationBarHidden = false
        NotificationCenter.default.removeObserver(self.audioSession)
    }
    
    
    
    func startObservingVolumeChanges() {
        audioSession.addObserver(self, forKeyPath: Observation.VolumeKey, options: [.initial, .new], context: &Observation.Context)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &Observation.Context {
            if keyPath == Observation.VolumeKey, let volume = (change?[NSKeyValueChangeKey.newKey] as? NSNumber)?.floatValue {
                print("Volume: \(volume)")
                try? AVAudioSession.sharedInstance().setCategory(.playback)
                try? AVAudioSession.sharedInstance().setActive(true, options: .notifyOthersOnDeactivation)
                self.player?.volume = volume
                self.stretchyHeaderSoundButton.isSelected = volume != 0.0
            }
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    
}


//------------------------------------------
//MARK: - Table View Setup Methods -
extension RecipeDetailsVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        
        // Top stretchy header setup
        self.stretchyHeader = GSKStretchyHeaderView.init(frame: CGRect.init(x: 0, y: 0, width: ssw, height: 280))
        self.stretchyHeader.frame.size.width = ssw
        self.stretchyHeader.stretchDelegate = self
        self.stretchyHeader.mk_addFourSideConstraintsInParent(childView: self.stretchyHeaderViewChild)
        
        self.stretchyHeaderViewChild.translatesAutoresizingMaskIntoConstraints = false
        stretchyHeader.contentInset = .init(top: apde.xBarHeight, left: 0, bottom: 0, right: 0)
        self.tblView.addSubview(self.stretchyHeader)
        
        self.stretchyHeader.minimumContentHeight = 0
        self.stretchyHeader.maximumContentHeight = ssw - 20
        
        
        // tblview delegate/datasource
        self.tblView.delegate = self
        self.tblView.dataSource = self
        
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
        
        self.stretchyHeaderPlayButton.isHidden = true
        self.stretchyHeaderPlayButton.mk_addTapHandler { (btn) in
            //            let videoURL = URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
            //            let player = AVPlayer(url: videoURL!)
            //            let playerViewController = AVPlayerViewController()
            //            playerViewController.player = player
            //            self.present(playerViewController, animated: true) {
            //                playerViewController.player!.play()
            //            }
        }
        
        
        
        
    }
    
    func manualTableViewReload() {
        self.tblView.performSelector(onMainThread: #selector(UITableView.reloadData), with: nil, waitUntilDone: true)
        
        Timer.after(0.06) {
            self.setImageToStretchyHeader()
        }
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // Header with serve and select all buttons
        if section == 0 {
            return 1
        }
        
        // CellRecipeDetailsIngredientItems
        if section == NrecipeDetailsIngredientSection {
            if recipeDetails.is_direction_selected {
                return 0
            }
            return recipeDetails.ingredient_list.count
        }
        
        //CellRecipeDetailsButtonAddToShoppingList
        if section == 2 {
            //            if self.isThird {
            //                return 0
            //            }
            if recipeDetails.is_direction_selected {
                return 0
            }
            return 1
        }
        
        if section == 3 {
            
            if recipeDetails.is_direction_selected {
                
                if isFromCookBook {
                    return self.recipeDetails.directions_list.count
                }
                
                if recipeDetails.web_url != "" && self.isFromSearch {
                    return 1
                }
                
                if recipeDetails.web_url != "" && (recipeDetails.is_own_recipe.mk_boolValue == false) {
                    return 1
                }
                return self.recipeDetails.directions_list.count
            }
            return 0
        }
        
        if section == 4 {
            if self.recipeDetails.is_direction_selected {
                return 0
            }
            return 1
        }
        
        if section == 5 {
            
            if self.isThird {
                return 0
            }
            if kCurrentUser.is_guest {
                return 0
            }
            if self.recipeDetails.is_admin_edited.mk_boolValue {
                if self.recipeDetails.is_own_recipe.mk_boolValue {
                    return 1
                }
            }
            if self.recipeDetails.is_saved_in_cookbook.mk_boolValue {
                return 1
            }
            return 0
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        //        let section = indexPath.section
        //
        //        if section == 4 {
        //            if self.recipeDetails.is_direction_selected {
        //                return 0
        //            }
        //            return UITableView.automaticDimension
        //        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? 100.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cellRecipeDetailsTopHeader = tableView.dequeueReusableCell(withIdentifier: "CellRecipeDetailsTopHeader", for: indexPath) as! CellRecipeDetails
            
            cellRecipeDetailsTopHeader.handleCellTopHeader(vc: self, indexPath: indexPath)
            
            cellRecipeDetailsTopHeader.btnSegment1TappedBlock = { btn in
                self.recipeDetails.is_direction_selected = false
                cellRecipeDetailsTopHeader.stackViewWithSelectAllButton.arrangedSubviews.first?.isHidden = true
                self.tblView.performSelector(onMainThread: #selector(UITableView.reloadData), with: nil, waitUntilDone: true)
            }
            cellRecipeDetailsTopHeader.btnSegment2TappedBlock = { btn in
                self.recipeDetails.is_direction_selected = true
                cellRecipeDetailsTopHeader.stackViewWithServeIncDecrButtons.arrangedSubviews.first?.isHidden = true
                self.tblView.performSelector(onMainThread: #selector(UITableView.reloadData), with: nil, waitUntilDone: true)
            }
            
            return cellRecipeDetailsTopHeader
        case NrecipeDetailsIngredientSection:
            
            let cellRecipeDetailsIngredientItems = tableView.dequeueReusableCell(withIdentifier: "CellRecipeDetailsIngredientItems", for: indexPath) as! CellRecipeDetails
            
            cellRecipeDetailsIngredientItems.handleCellIngredientList(vc: self, indexPath: indexPath)
            
            
            return cellRecipeDetailsIngredientItems
            
        case 2:
            
            let cellRecipeDetailsButtonAddToShoppingList = tableView.dequeueReusableCell(withIdentifier: "CellRecipeDetailsButtonAddToShoppingList", for: indexPath) as! CellRecipeDetails
            
            cellRecipeDetailsButtonAddToShoppingList.handleCellAddIngToShopListButton(vc: self, indexPath: indexPath)
            
            
            return cellRecipeDetailsButtonAddToShoppingList
            
        case 3 :
            
            
            if self.isFromCookBook {
                let cellRecipeDetailsDirections = tableView.dequeueReusableCell(withIdentifier: "CellRecipeDetailsDirections", for: indexPath) as! CellRecipeDetails
                cellRecipeDetailsDirections.handleCellDirectionList(vc: self, indexPath: indexPath)
                return cellRecipeDetailsDirections
            }
            
            if recipeDetails.web_url != "" && self.isFromSearch {
                let cellRecipeDetailsWebView = tableView.dequeueReusableCell(withIdentifier: "CellRecipeDetailsWebView", for: indexPath) as! CellRecipeDetailsWebView
                cellRecipeDetailsWebView.handleCell(vc: self)
                return cellRecipeDetailsWebView
            }
            
            if recipeDetails.web_url != "" && (recipeDetails.is_own_recipe.mk_boolValue == false) {
                let cellRecipeDetailsWebView = tableView.dequeueReusableCell(withIdentifier: "CellRecipeDetailsWebView", for: indexPath) as! CellRecipeDetailsWebView
                cellRecipeDetailsWebView.handleCell(vc: self)
                return cellRecipeDetailsWebView
            }
            
            let cellRecipeDetailsDirections = tableView.dequeueReusableCell(withIdentifier: "CellRecipeDetailsDirections", for: indexPath) as! CellRecipeDetails
            cellRecipeDetailsDirections.handleCellDirectionList(vc: self, indexPath: indexPath)
            return cellRecipeDetailsDirections
            
            
            
            
        case 4 :
                
            
            if self.isNutritionDataAvailable {
                
                
                let cellRecipeDetailsNutritions = tableView.dequeueReusableCell(withIdentifier: "CellRecipeDetailsNutritions", for: indexPath) as! CellRecipeDetailsNutritions
                cellRecipeDetailsNutritions.handleCell(vc: self, ip: indexPath)
                
                self.isApiNutritionRunning = false
                // This recipeDetails.name != "" --- recipe details api yet not called
                if recipeDetails.name != "" {
                    if self.isApiNutritionResponseRecieved == false {
                        if isApiNutritionRunning == false {
                            self.isApiNutritionRunning = true
                            if self.objParsedRes.calories == 0 {
                                
                                var original_serves_ingredient_list : [Ingredient_list]!
                                original_serves_ingredient_list = self.recipeDetails.original_serves_ingredient_list
                                if original_serves_ingredient_list.count == 0 {
                                    original_serves_ingredient_list = self.recipeDetails.ingredient_list
                                }
                                
                                var arrIngredients                  = [Ingredient_list]()
                                for original_serves_ingredient in original_serves_ingredient_list {
                                    
                                    var ingredient                  = Ingredient_list()
                                    ingredient                      = original_serves_ingredient
                                    
                                    
                                    let serve_one_when_user_not_added_serve = (recipeDetails.original_serves == 0) ? 1 : recipeDetails.original_serves
                                    
                                    if let dblQty = Double(original_serves_ingredient.qty) {
                                        
                                        let singleQty   = dblQty/Double(serve_one_when_user_not_added_serve)
                                        ingredient.qty  = "\((singleQty*1).rounded(toPlaces: 2).setPriceFormatted())"
                                        
                                    } else {
                                        
                                        
                                        if Int(1) == serve_one_when_user_not_added_serve {
                                            
                                        } else {
                                            
                                            var qty = removeSpace(original_serves_ingredient.qty)
                                            
                                            if qty.first == " " {
                                                qty = String(qty.dropFirst())
                                            }
                                            
                                            if qty.last == " " {
                                                qty = String(qty.dropLast())
                                            }
                                            
                                            if qty.components(separatedBy: "/").count > 1 {
                                                
                                                if let f = qty.components(separatedBy: "/").first, let l = qty.components(separatedBy: "/").last {
                                                    
                                                    if f != "" && l != "" {
                                                        
                                                        // case 1 1/2 --- one and a half
                                                        if f.components(separatedBy: " ").count > 1 {
                                                            if let intFirst = f.components(separatedBy: " ").first?.integer {
                                                                if let intLast = f.components(separatedBy: " ").last?.integer {
                                                                    if let intLastBehindSlash = Int(l) {
                                                                        let dbl = (Double(intLast)/Double(intLastBehindSlash)).rounded(toPlaces: 2)
                                                                        let total = dbl + Double(intFirst)
                                                                        let singleQty   = total/Double(serve_one_when_user_not_added_serve)
                                                                        let increasedQty = (singleQty*1).rounded(toPlaces: 2)
                                                                        
                                                                        if increasedQty.whole != 0.0 && increasedQty.fraction != 0.0 {
                                                                            let strFraction = float(toFraction: Float(increasedQty.fraction))
                                                                            ingredient.qty = "\(Int(increasedQty.whole)) \(strFraction)"
                                                                        } else if increasedQty.whole == 0.0 && increasedQty.fraction != 0.0 {
                                                                            let strFraction = float(toFraction: Float(increasedQty.fraction))
                                                                            ingredient.qty = "\(strFraction)"
                                                                        } else {
                                                                            ingredient.qty = "\(Int(increasedQty.whole))"
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            // case 1/2 --- half
                                                            if let intFirst = Int(f),let intLast = Int(l) {
                                                                
                                                                let dbl = (Double(intFirst)/Double(intLast)).rounded(toPlaces: 4)
                                                                let singleQty       = dbl/Double(serve_one_when_user_not_added_serve)
                                                                let increasedQty    = (singleQty*1).rounded(toPlaces: 4)
                                                                
                                                                if increasedQty.whole != 0.0 && increasedQty.fraction != 0.0 {
                                                                    let strFraction = float(toFraction: Float(increasedQty.fraction.rounded(toPlaces: 2)))
                                                                    ingredient.qty = "\(Int(increasedQty.whole)) \(strFraction)"
                                                                } else if increasedQty.whole == 0.0 && increasedQty.fraction != 0.0 {
                                                                    let strFraction = float(toFraction: Float(increasedQty.fraction.rounded(toPlaces: 3)))
                                                                    ingredient.qty = "\(strFraction)"
                                                                } else {
                                                                    ingredient.qty = "\(Int(increasedQty.whole))"
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    arrIngredients.append(ingredient)
                                    
                                }
                                let arrIngs = arrIngredients.map { (ing) -> String in
                                    return "\(ing.qty) \(ing.unit_name) \(ing.name)"
                                }
                                cellRecipeDetailsNutritions.viewLoader.isHidden = false
                                self.getEdmamNutritionDataApiCall(arrIngredients: arrIngs, compHandler: {
                                    
                                    cellRecipeDetailsNutritions.viewLoader.isHidden = true
                                    self.isApiNutritionRunning = false
                                    cellRecipeDetailsNutritions.handleCell(vc: self, ip: indexPath)
                                    
                                    if self.recipeDetails.is_direction_selected == false {
                                        self.tblView.reloadRows(at: [IndexPath.init(row: 0, section: 4)], with: .automatic)
                                    }
                                    
                                })
                            }
                        }
                        
                    }
                    
                }
                if self.objParsedRes.calories != 0 {
                    cellRecipeDetailsNutritions.viewLoader.isHidden = true
                }
                
                return cellRecipeDetailsNutritions
                
                
            } else {
                
                let cellRecipeDetailsNutritionsNotAvailable = tableView.dequeueReusableCell(withIdentifier: "CellRecipeDetailsNutritionsNotAvailable", for: indexPath) as! CellRecipeDetailsNutritions
                return cellRecipeDetailsNutritionsNotAvailable
                
            }
            
            
        case 5:
            
            
            if self.recipeDetails.is_admin_edited.mk_boolValue {
                if self.recipeDetails.is_own_recipe.mk_boolValue {
                    let cellRecipeDetailsApproval = tableView.dequeueReusableCell(withIdentifier: "CellRecipeDetailsApproval", for: indexPath) as! CellRecipeDetailsApproval
                    cellRecipeDetailsApproval.handleCell(vc: self, ip: indexPath)
                    return cellRecipeDetailsApproval
                }
            }
            if self.recipeDetails.is_saved_in_cookbook.mk_boolValue {
                if self.recipeDetails.is_own_recipe.mk_boolValue {
                    let cellRecipeDetailsBottomEditDeleteOwnRecipe = tableView.dequeueReusableCell(withIdentifier: "CellRecipeDetailsBottomEditDeleteOwnRecipe", for: indexPath) as! CellRecipeDetailsBottomEditDelete
                    cellRecipeDetailsBottomEditDeleteOwnRecipe.handleCell(vc: self, ip: indexPath)
                    return cellRecipeDetailsBottomEditDeleteOwnRecipe
                } else {
                    let cellRecipeDetailsBottomEditDeleteNotOwnRecipe = tableView.dequeueReusableCell(withIdentifier: "CellRecipeDetailsBottomEditDeleteNotOwnRecipe", for: indexPath) as! CellRecipeDetailsBottomEditDelete
                    cellRecipeDetailsBottomEditDeleteNotOwnRecipe.handleCell(vc: self, ip: indexPath)
                    return cellRecipeDetailsBottomEditDeleteNotOwnRecipe
                }
            }
            
        default:
            return UITableViewCell()
        }
        
        return UITableViewCell()
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.setImageToStretchyHeader()
    }
    
    
    
}



//MARK : GSKStretchyHeaderViewStretchDelegate methods



//MARK:- API Calls
extension RecipeDetailsVC {
    
    
    func getEdmamNutritionDataApiCall(arrIngredients:[String], compHandler: @escaping ()->()) {
        
        getEdmamNutritionData(arrIngredients: arrIngredients) { (res) in
            switch res {
            case .success(let dictData, let message) :
                
                print(dictData)
                
                let jsonDictData = JSON.init(dictData)
                
                if let _ = dictData["error"] as? String {
                    self.isNutritionDataAvailable = false
                    compHandler()
                    return
                }
                
                //totalDaily
                let dictTotalDaily = jsonDictData["totalDaily"].dictionaryValue
                //totalNutrients
                let dictTotalNutrients = jsonDictData["totalNutrients"].dictionaryValue
                
                //Calories
                let caloryDict = dictTotalNutrients["ENERC_KCAL"]?.dictionaryValue
                self.objParsedRes.calories = self.getQty(caloryDict)
                
                
                print("Calories :   \(self.objParsedRes.calories)")
                
                // FAT
                let fatDict = dictTotalDaily["FAT"]?.dictionaryValue
                self.objParsedRes.totalFatQty = self.getQty(fatDict)
                self.objParsedRes.totalFatUnit = self.getUnit(fatDict)
                
                let fatDict_n = dictTotalNutrients["FAT"]?.dictionaryValue
                self.objParsedRes.totalFatQty_n = self.getQtyDouble(fatDict_n)
                self.objParsedRes.totalFatUnit_n = self.getUnit(fatDict_n)
                
                print("Total Fat :  \(self.objParsedRes.totalFatQty_n) \(self.objParsedRes.totalFatUnit_n) -- \(self.objParsedRes.totalFatQty)-\(self.objParsedRes.totalFatUnit)")
                
                
                
                // Saturated
                let fasatDict = dictTotalDaily["FASAT"]?.dictionaryValue
                self.objParsedRes.totalFatSatQty = self.getQty(fasatDict)
                self.objParsedRes.totalFatSatUnit = self.getUnit(fasatDict)
                
                let fasatDict_n = dictTotalNutrients["FASAT"]?.dictionaryValue
                self.objParsedRes.totalFatSatQty_n = self.getQtyDouble(fasatDict_n)
                self.objParsedRes.totalFatSatUnit_n = self.getUnit(fasatDict_n)
                
                print("Saturated Fat :  \(self.objParsedRes.totalFatSatQty_n) \(self.objParsedRes.totalFatSatUnit_n) -- \(self.objParsedRes.totalFatSatQty)-\(self.objParsedRes.totalFatSatUnit)")
                
                
                // Sodium
                let sodiumDict = dictTotalDaily["NA"]?.dictionaryValue
                self.objParsedRes.sodiumQty = self.getQty(sodiumDict)
                self.objParsedRes.sodiumUnit = self.getUnit(sodiumDict)
                
                let sodiumDict_n = dictTotalNutrients["NA"]?.dictionaryValue
                self.objParsedRes.sodiumQty_n = self.getQtyDouble(sodiumDict_n)
                self.objParsedRes.sodiumUnit_n = self.getUnit(sodiumDict_n)
                
                print("Sodium : \(self.objParsedRes.sodiumQty_n) \(self.objParsedRes.sodiumUnit_n) -- \(self.objParsedRes.sodiumQty)-\(self.objParsedRes.sodiumUnit)")
                
                
                
                // Carbo
                let carboDict = dictTotalDaily["CHOCDF"]?.dictionaryValue
                self.objParsedRes.carbQty = self.getQty(carboDict)
                self.objParsedRes.carbUnit = self.getUnit(carboDict)
                
                let carboDict_n = dictTotalNutrients["CHOCDF"]?.dictionaryValue
                self.objParsedRes.carbQty_n = self.getQtyDouble(carboDict_n)
                self.objParsedRes.carbUnit_n = self.getUnit(carboDict_n)
                
                print("Carbo :  \(self.objParsedRes.carbQty_n) \(self.objParsedRes.carbUnit_n) -- \(self.objParsedRes.carbQty)-\(self.objParsedRes.carbUnit)")
                
                
                // Dietary Fiber
                let dietFiberDict = dictTotalDaily["FIBTG"]?.dictionaryValue
                self.objParsedRes.dietFiberQty = self.getQty(dietFiberDict)
                self.objParsedRes.dietFiberUnit = self.getUnit(dietFiberDict)
                
                let dietFiberDict_n = dictTotalNutrients["FIBTG"]?.dictionaryValue
                self.objParsedRes.dietFiberQty_n = self.getQtyDouble(dietFiberDict_n)
                self.objParsedRes.dietFiberUnit_n = self.getUnit(dietFiberDict_n)
                
                print("Dietary Fiber :  \(self.objParsedRes.dietFiberQty_n) \(self.objParsedRes.dietFiberUnit_n) -- \(self.objParsedRes.dietFiberQty)-\(self.objParsedRes.dietFiberUnit)")
                
                
                let sugarDict_n = dictTotalNutrients["SUGAR"]?.dictionaryValue
                self.objParsedRes.sugarQty_n = self.getQtyDouble(sugarDict_n)
                self.objParsedRes.sugarUnit_n = self.getUnit(sugarDict_n)
                
                print("Sugar :  \(self.objParsedRes.sugarQty_n) \(self.objParsedRes.sugarUnit_n)")
                
                
                
                
                // Protein
                let proteinDict = dictTotalDaily["PROCNT"]?.dictionaryValue
                self.objParsedRes.proteinQty = self.getQty(proteinDict)
                self.objParsedRes.proteinUnit = self.getUnit(proteinDict)
                
                let proteinDict_n = dictTotalNutrients["PROCNT"]?.dictionaryValue
                self.objParsedRes.proteinQty_n = self.getQtyDouble(proteinDict_n)
                self.objParsedRes.proteinUnit_n = self.getUnit(proteinDict_n)
                
                print("Protein :    \(self.objParsedRes.proteinQty_n) \(self.objParsedRes.proteinUnit_n) -- \(self.objParsedRes.proteinQty)-\(self.objParsedRes.proteinUnit)")
                
                
                
                // Calcium
                let calciumDict = dictTotalDaily["CA"]?.dictionaryValue
                self.objParsedRes.calciumQty = self.getQty(calciumDict)
                self.objParsedRes.calciumUnit = self.getUnit(calciumDict)
                
                let calciumDict_n = dictTotalNutrients["CA"]?.dictionaryValue
                self.objParsedRes.calciumQty_n = self.getQtyDouble(calciumDict_n)
                self.objParsedRes.calciumUnit_n = self.getUnit(calciumDict_n)
                
                print("Calcium :    \(self.objParsedRes.calciumQty_n) \(self.objParsedRes.calciumUnit_n) -- \(self.objParsedRes.calciumQty)-\(self.objParsedRes.calciumUnit)")
                
                
                
                // Iron
                let ironDict = dictTotalDaily["FE"]?.dictionaryValue
                self.objParsedRes.ironQty = self.getQty(ironDict)
                self.objParsedRes.ironUnit = self.getUnit(ironDict)
                
                let ironDict_n = dictTotalNutrients["FE"]?.dictionaryValue
                self.objParsedRes.ironQty_n = self.getQtyDouble(ironDict_n)
                self.objParsedRes.ironUnit_n = self.getUnit(ironDict_n)
                
                print("Iron :   \(self.objParsedRes.ironQty_n) \(self.objParsedRes.ironUnit_n) -- \(self.objParsedRes.ironQty)-\(self.objParsedRes.ironUnit)")
                
                
                
                // Potassium
                let potassiumDict = dictTotalDaily["K"]?.dictionaryValue
                self.objParsedRes.potassiumQty = self.getQty(potassiumDict)
                self.objParsedRes.potassiumUnit = self.getUnit(potassiumDict)
                
                let potassiumDict_n = dictTotalNutrients["K"]?.dictionaryValue
                self.objParsedRes.potassiumQty_n = self.getQtyDouble(potassiumDict_n)
                self.objParsedRes.potassiumUnit_n = self.getUnit(potassiumDict_n)
                
                print("Potassium :  \(self.objParsedRes.potassiumQty_n) \(self.objParsedRes.potassiumUnit_n) -- \(self.objParsedRes.potassiumQty)-\(self.objParsedRes.potassiumUnit)")
                
                self.isApiNutritionResponseRecieved = true
                self.isNutritionDataAvailable = true
                
                break
            case .failed(let message) :
                self.isNutritionDataAvailable = false
                break
            case .error(let message) :
                self.isNutritionDataAvailable = false
                break
            }
            
            compHandler()
        }
    }
    
    func getQtyDouble(_ dict:[String:JSON]?,roundToDigit:Int=2)->Double {
        return dict?["quantity"]?.doubleValue.roundTo(places: roundToDigit) ?? 0.0
    }
    func getQty(_ dict:[String:JSON]?)->Int {
        return Int(dict?["quantity"]?.doubleValue.roundTo(places: 0) ?? 0.0)
    }
    func getUnit(_ dict:[String:JSON]?)->String {
        return dict?["unit"]?.stringValue ?? ""
    }
    
}


//MARK:- API CALLS
extension RecipeDetailsVC {
    
    func getRecipeDetailsApiCall() {
        
        self.tblView.isHidden = true
        
        self.objParsedRes                       = ParsedRes()
        self.isApiNutritionResponseRecieved     = false
        self.isApiNutritionRunning              = false
        
        apde.showLoader()
        
        getRecipeDetails(recipe_id: self.recipe_id,is_from_cookbook:self.isFromCookBook.mk_intValue) { (res) in
            
            switch res {
                
            case .success(dictData: let dictData, message: _) :
                
                
                // Set Interested Category tags selected
                guard let dictRecipeDetails = JSON(dictData).dictionary?["recipe_details"]?.dictionary else {
                    return
                }
                print(dictRecipeDetails)
                
                self.recipeDetails.id                           = dictRecipeDetails["id"]?.int64 ?? 0
                self.recipeDetails.name                         = dictRecipeDetails["name"]?.string ?? ""
                self.recipeDetails.creator_name                 = dictRecipeDetails["creator_name"]?.string ?? ""
                self.recipeDetails.ratings                      = dictRecipeDetails["ratings"]?.double ?? 0.0
                self.recipeDetails.media_type                   = dictRecipeDetails["media_type"]?.int ?? 0
                self.recipeDetails.media_url                    = dictRecipeDetails["media_url"]?.stringValue ?? ""
                self.recipeDetails.video_thumb_url              = dictRecipeDetails["video_thumb_url"]?.stringValue ?? ""
                self.recipeDetails.ready_time_hours             = dictRecipeDetails["ready_time_hours"]?.int ?? 0
                self.recipeDetails.ready_time_mins              = dictRecipeDetails["ready_time_mins"]?.int ?? 0
                
                self.recipeDetails.serves                       = dictRecipeDetails["serves"]?.intValue ?? 0
                self.recipeDetails.original_serves              = dictRecipeDetails["serves"]?.intValue ?? 0
                
                self.recipeDetails.web_url                      = dictRecipeDetails["web_url"]?.stringValue ?? ""
                self.recipeDetails.view_url                     = dictRecipeDetails["view_url"]?.stringValue ?? ""
                self.recipeDetails.no_of_ratings                = dictRecipeDetails["no_of_ratings"]?.int64 ?? 0
                
                self.recipeDetails.recipe_owner_type            = dictRecipeDetails["recipe_owner_type"]?.int ?? 0
                self.recipeDetails.is_own_recipe                = dictRecipeDetails["is_own_recipe"]?.int ?? 0
                self.recipeDetails.is_saved_in_cookbook         = dictRecipeDetails["is_saved_in_cookbook"]?.int ?? 0
                self.recipeDetails.cookbook_id                  = dictRecipeDetails["cookbook_id"]?.int64 ?? 0
                self.recipeDetails.is_admin_edited              = dictRecipeDetails["is_admin_edited"]?.int ?? 0
                self.recipeDetails.admin_notes                  = dictRecipeDetails["admin_notes"]?.stringValue ?? ""
                
                self.recipeDetails.is_one_pot_meal              = dictRecipeDetails["is_one_pot_meal"]?.int ?? 0
                self.recipeDetails.is_easy                      = dictRecipeDetails["is_easy"]?.int ?? 0
                self.recipeDetails.recipe_status                = dictRecipeDetails["recipe_status"]?.int ?? 0
                
                var arrIngredients                  = [Ingredient_list]()
                for dictIng in dictRecipeDetails["ingredient_list"]?.arrayValue ?? [JSON()] {
                    var ingredient                  = Ingredient_list()
                    ingredient.id                   = dictIng["id"].int64 ?? 0
                    ingredient.unit_name            = dictIng["unit"].string ?? ""
                    ingredient.name                 = dictIng["name"].string ?? ""
                    ingredient.note                 = dictIng["note"].string ?? ""
                    ingredient.qty                  = dictIng["qty"].string ?? ""
                    arrIngredients.append(ingredient)
                }
                
                self.recipeDetails.ingredient_list                      = arrIngredients
                self.recipeDetails.original_serves_ingredient_list      = arrIngredients
                
                var arrDirections                  = [Direction_list]()
                for dictDirection in dictRecipeDetails["direction_list"]?.arrayValue ?? [JSON()] {
                    var direction                  = Direction_list()
                    direction.step                 = dictDirection["steps"].int ?? 0
                    direction.description          = dictDirection["direction"].string ?? ""
                    arrDirections.append(direction)
                }
                self.recipeDetails.directions_list  = arrDirections
                
                
                var arrKeywords                  = [KeywordModel]()
                for dictDirection in dictRecipeDetails["keywords_list"]?.arrayValue ?? [JSON()] {
                    var keyword                  = KeywordModel()
                    keyword.description          = dictDirection["description"].string ?? ""
                    arrKeywords.append(keyword)
                }
                self.recipeDetails.keywords_list  = arrKeywords
                
                self.recipeDetails.cuisine_type_id_list         = dictRecipeDetails["cuisine_type_id_list"]?.stringValue ?? ""
                self.recipeDetails.difficulty_level_id_list     = dictRecipeDetails["difficulty_level_id_list"]?.stringValue ?? ""
                self.recipeDetails.diet_id_list                 = dictRecipeDetails["diet_id_list"]?.stringValue ?? ""
                self.recipeDetails.meal_type_id_list            = dictRecipeDetails["meal_type_id_list"]?.stringValue ?? ""
                
                // IF RECIPE NOT OWNED AND NOT APPROVED
                if self.recipeDetails.is_own_recipe.mk_boolValue == false {
                    if self.recipeDetails.recipe_status != enum_recipe_status.approved.rawValue {
                        Timer.after(0.5) {
                            BasicFunctions.showInfo(strError: "Recipe currently not available.")
                            apde.hideLoader()
                            self.pop()
                        }
                        return
                    }
                }
                
                
                //                if self.recipeDetails.web_url != "" && (self.recipeDetails.is_own_recipe.mk_boolValue == false) {
                //                    self.isThird = true
                //                }
                
                if self.recipeDetails.media_type == enum_media_type.video.rawValue {
                    
                    let videoUrl = URL(string: self.recipeDetails.media_url)!
                    self.player?.stop()
                    self.player = nil
                    self.player = Player()
                    self.player.playbackDelegate = self
                    self.player.view.frame = self.view.bounds
                    
                    self.addChild(self.player)
                    self.view.addSubview(self.player.view)
                    self.player.didMove(toParent: self)
                    self.player.url = videoUrl
                    self.player.playbackLoops = true
                    self.player.playFromBeginning()
                    self.player.fillMode = .resizeAspectFill
                    self.player?.volume = 0
                    
                    
                    //                    if self.recipeDetails.media_url.contains("/embed/") {
                    //
                    //                        self.youtubePlayer = WKYTPlayerView.init()
                    //                        let videoId = self.recipeDetails.media_url.components(separatedBy: "/embed/").last ?? ""
                    //                        if videoId != "" {
                    //                            self.youtubePlayer.load(withVideoId: videoId, playerVars: ["playsinline" : 1, "fs": 0, "iv_load_policy": 3, "modestbranding": 1, "rel": 0])
                    //                        }
                    //
                    //                        self.youtubePlayer.delegate = self
                    //                        self.youtubePlayer.playVideo()
                    //                        self.stretchyHeaderViewChild.mk_addFourSideConstraintsInParent(childView: self.youtubePlayer)
                    //                    }
                    
                    if let videoId = Utilities.getQueryStringParameter(url: self.recipeDetails.media_url, param: "v") {
                        
                        self.youtubePlayer = WKYTPlayerView.init()
                        self.youtubePlayer.load(withVideoId: videoId, playerVars: ["playsinline" : 1, "fs": 0, "iv_load_policy": 3, "modestbranding": 1, "rel": 0])
                        self.youtubePlayer.delegate = self
                        self.youtubePlayer.playVideo()
                        self.stretchyHeaderViewChild.mk_addFourSideConstraintsInParent(childView: self.youtubePlayer)
                        self.stretchyHeaderGreenView.superview?.bringSubviewToFront(self.stretchyHeaderGreenView)
                        
                    }
                    
                    if self.recipeDetails.media_url.contains("youtube") {
                        
                        if self.youtubePlayer == nil {
                            let webConfiguration = WKWebViewConfiguration()
                            webConfiguration.websiteDataStore = WKWebsiteDataStore.default()
                            let webView = WKWebView(frame: .zero, configuration: webConfiguration)
                            let request = URLRequest.init(url: videoUrl)
                            webView.load(request)
                            self.stretchyHeaderViewChild.mk_addFourSideConstraintsInParent(childView: webView)
                            self.stretchyHeaderGreenView.superview?.bringSubviewToFront(self.stretchyHeaderGreenView)
                        }
                        
                    } else {
                        self.stretchyHeaderViewChild.mk_addFourSideConstraintsInParent(childView: self.player.view)
                        self.stretchyHeaderGreenView.superview?.bringSubviewToFront(self.stretchyHeaderGreenView)
                        self.stretchyHeaderScaleButton.superview?.bringSubviewToFront(self.stretchyHeaderScaleButton)
                        self.stretchyHeaderSoundButton.superview?.bringSubviewToFront(self.stretchyHeaderSoundButton)
                    }
                    
                    self.stretchyHeaderScaleButton.isHidden = false
                    self.stretchyHeaderSoundButton.isHidden = false
                    
                    self.stretchyHeaderScaleButton.onTap {
                        let player = AVPlayer(url: videoUrl)
                        let playerViewController = AVPlayerViewController()
                        playerViewController.player = player
                        self.present(playerViewController, animated: true) {
                            playerViewController.player!.play()
                        }
                    }
                    
                } else {
                    self.stretchyHeaderScaleButton.isHidden = true
                    self.stretchyHeaderSoundButton.isHidden = true
                }
                
                self.setImageToStretchyHeader()
                
                self.lblTitle.text = self.recipeDetails.name
                
                self.manualTableViewReload()
                
                Timer.after(0.1) {
                    self.setImageToStretchyHeader()
                }
                
                if self.isDisplayRejectionNotePopup {
                    Timer.after(0.35) {
                        self.isDisplayRejectionNotePopup = false
                        BasicFunctions.showPopupOneButton(title: "Recipe Rejected Admin Note".localize(), desc: self.recipeDetails.admin_notes) {
                            
                        }
                    }
                }
                
                updateRecipesListVC(newUpdatedRecipeDetails: self.recipeDetails, vcSelf: self)
                
                
                
                
                apde.hideLoader()
                self.tblView.isHidden = false
                
                break
            case .error(error: let err) :
                BasicFunctions.showError(strError: err)
                apde.hideLoader()
                self.tblView.isHidden = false
                break
            case .failed(message: let msg) :
                BasicFunctions.showError(strError: msg)
                apde.hideLoader()
                self.tblView.isHidden = false
                break
            }
            
        }
    }
    
    func setImageToStretchyHeader() {
        
        let media_type = enum_media_type.init(rawValue: self.recipeDetails.media_type) ?? enum_media_type.image
        switch media_type {
        case .image:
            self.stretchyHeaderRecipeImgView.mk_afSetImage(urlString: self.recipeDetails.media_url, defaultImage: "AssetCollectionPlaceholder") { (img, isImage) in
                if isImage {
                    self.stretchyHeaderRecipeImgView.image = img
                }
            }
            break
        case .video:
            self.stretchyHeaderRecipeImgView.mk_afSetImage(urlString: self.recipeDetails.video_thumb_url, defaultImage: "AssetCollectionPlaceholder") { (img, isImage) in
                if isImage {
                    self.stretchyHeaderRecipeImgView.image = img
                }
            }
            break
        }
    }
}

extension RecipeDetailsVC : GSKStretchyHeaderViewStretchDelegate {
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.stretchyHeader.stretchFactor > 0.3 {
            self.player?.playFromCurrentTime()
        }
    }
    
    func stretchyHeaderView(_ headerView: GSKStretchyHeaderView, didChangeStretchFactor stretchFactor: CGFloat) {
        
        self.stretchyHeaderGreenView.alpha = 1 - stretchFactor
        self.stretchyHeaderScaleButton.alpha = stretchFactor
        self.stretchyHeaderSoundButton.alpha = stretchFactor
        self.lblTitle.isHidden = stretchFactor > 0
        
        if stretchFactor > 0.3 {
            self.stretchyHeaderScaleButton.isHidden = false
            self.stretchyHeaderSoundButton.isHidden = false
        } else {
            self.stretchyHeaderScaleButton.isHidden = true
            self.stretchyHeaderSoundButton.isHidden = true
        }
        if stretchFactor == 0 {
            self.player?.stop()
        }
        
        if self.recipeDetails.media_type == enum_media_type.image.rawValue {
            self.player?.view.isHidden = true
            self.stretchyHeaderScaleButton?.isHidden = true
            self.stretchyHeaderSoundButton?.isHidden = true
        }
        self.setImageToStretchyHeader()
        
    }
}


extension RecipeDetailsVC : PlayerPlaybackDelegate {
    
    
    func playerCurrentTimeDidChange(_ player: Player) {
        print(player.currentTime)
        if let _ = UIViewController.CVC() as? RecipeDetailsVC { } else { player.stop() }
    }
    
    func playerPlaybackWillLoop(_ player: Player) {
    }
    
    func playerPlaybackDidLoop(_ player: Player) {
    }
    public func playerPlaybackWillStartFromBeginning(_ player: Player) {
    }
    
    public func playerPlaybackDidEnd(_ player: Player) {
    }
    
    //    public func playerCurrentTimeDidChange(_ player: Player) {
    //        let fraction = Double(player.currentTime) / Double(player.maximumDuration)
    //        self._playbackViewController?.setProgress(progress: CGFloat(fraction), animated: true)
    //    }
    //    public func playerPlaybackWillLoop(_ player: Player) {
    //        self. _playbackViewController?.reset()
    //    }
    
}


extension RecipeDetailsVC : WKYTPlayerViewDelegate {
    
    func playerView(_ playerView: WKYTPlayerView, didChangeTo state: WKYTPlayerState) {
        if state == .playing {
            
        }
    }
    
    
    
    func playerView(_ playerView: WKYTPlayerView, receivedError error: WKYTPlayerError) {
        print(error)
    }
    
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        print("playerViewDidBecomeReady")
    }
    
    //    public func playerCurrentTimeDidChange(_ player: Player) {
    //        let fraction = Double(player.currentTime) / Double(player.maximumDuration)
    //        self._playbackViewController?.setProgress(progress: CGFloat(fraction), animated: true)
    //    }
    //    public func playerPlaybackWillLoop(_ player: Player) {
    //        self. _playbackViewController?.reset()
    //    }
    
}





//"recipe_details": {
//    "id": 52,
//    "name": "test",
//    "creator_name": "123456zzz",
//    "ratings": 0,
//    "media_type": 1,
//    "media_url": "",
//    "video_thumb_url": "",
//    "ready_time_hours": 7,
//    "ready_time_mins": 0,
//    "serves": 2,
//    "web_url": "",
//    "no_of_ratings": 0,
//    "recipe_owner_type": 1,
//    "is_own_recipe": 1,
//    "is_saved_in_cookbook": 1,
//    "is_admin_edited": 0,
//    "admin_notes": "",
//    "is_one_poet_meal": 0,
//    "is_easy": 0,
//    "recipe_status": 3,
//    "ingredient_list": [
//        {
//            "id": 172,
//            "name": "hehdhdhd",
//            "qty": "34",
//            "unit": "g",
//            "note": ""
//        },
//        {
//            "id": 173,
//            "name": "with",
//            "qty": "45",
//            "unit": "g",
//            "note": ""
//        },
//        {
//            "id": 174,
//            "name": "testetst",
//            "qty": "1/2",
//            "unit": "g",
//            "note": ""
//        }
//    ],
//    "direction_list": [
//        {
//            "id": 289,
//            "steps": 1,
//            "direction": "Bshshhshs"
//        }
//    ],
//    "keywords_list": [],
//    "cuisine_type_id_list": "",
//    "difficulty_level_id_list": "",
//    "diet_id_list": "",
//    "meal_type_id_list": ""
//}






extension RecipeDetailsVC {
    
    func removeRecipeFlow(vc:RecipeDetailsVC) {
        
        if vc.recipeDetails.is_own_recipe.mk_boolValue {
            
            var desc = ""
            
            if enum_recipe_status.init(rawValue: vc.recipeDetails.recipe_status) == .approved {
                desc = "Are you sure you want to delete this recipe? Your recipe will no longer be available for user and your recipe will be removed from your cookbook."
            } else {
                desc = "Are you sure you want to delete this recipe? Your recipe will be removed from your Cookbook."
            }
            
            BasicFunctions.showPopupTwoButton(title: "Delete Recipe?".localize(), desc: desc, btnTitleLeft: "NO".localize(), btnTitleRight: "YES".localize(), baseVC: vc) { (isYes) in
                if isYes {
                    // Delete Api Call
                    recipeDeleteApiCall(vc: vc, recipe_id: vc.recipeDetails.id,compHandler: { (str) in
                        vc.navigationController?.popViewController(animated: true)
                        if vc.isFromSearch == false {
                            vc.recipeDeletedOrUpdatedBlock?(self.recipeDetails,true)
                        }
                    })
                }
            }
            
        } else {
            
            let desc = "Are you sure you want to delete this recipe? Your recipe will be removed from your Cookbook."
            
            BasicFunctions.showPopupTwoButton(title: "Delete Recipe?".localize(), desc: desc, btnTitleLeft: "NO".localize(), btnTitleRight: "YES".localize(), baseVC: vc) { (isYes) in
                if isYes {
                    // Delete Api Call
                    recipeRemoveFromCookbookApiCall(vc: vc, cookbook_id: vc.recipeDetails.cookbook_id,compHandler: { (str) in
                        vc.navigationController?.popViewController(animated: true)
                        if vc.isFromSearch == false {
                            vc.recipeDeletedOrUpdatedBlock?(self.recipeDetails,true)
                        }
                    })
                }
            }
            
        }
    }
}


//MARK:-  INGREDIENTS
extension RecipeDetailsVC {
    
    
    func parseIngredientsApiCall(mutArrIngredients:[String], comp: @escaping ()->()) {
        
        //Ingredient_list
        
        guard let jsonDataArrIngredients = try? JSONSerialization.data(withJSONObject: mutArrIngredients,options: []) else {
            comp()
            return
        }
        
        guard let textArrIngredienta = String(data: jsonDataArrIngredients, encoding: .utf8) else {
            comp()
            return
        }
        print("JSON string textArrIngredienta = \(textArrIngredienta)")
        
        getIngredientsParsing(ingredients: textArrIngredienta) { (res) in
            switch res {
            case .success(dictData: let dictData, message: _) :
                
                print(dictData)
                if let dictIngredientList = dictData["ingredient_list"] as? [[String:Any]] {
                    var a = [Ingredient_list]()
                    for dictIngredient in dictIngredientList {
                        
                        let name            = dictIngredient["ingredient"] as? String ?? ""
                        let unit_name       = dictIngredient["unit"] as? String ?? ""
                        let qty             = dictIngredient["quantity"] as? String ?? ""
                        
                        let objIngredient = Ingredient_list.init(id: 0, unit_name: unit_name, name: name, note: "", qty: qty, isSelected: false)
                        a.append(objIngredient)
                    }
                    self.recipeDetails.ingredient_list                      = a
                    self.recipeDetails.original_serves_ingredient_list      = a
                    comp()
                } else {
                    comp()
                    BasicFunctions.showError(strError: "Something went wrong!".localize())
                }
                
                break
            case .failed(message: let msg):
                comp()
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                comp()
                BasicFunctions.showError(strError: error)
                break
            }
        }
        
        
    }
}
