//
//  CellRecentSearch.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 3/13/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class CellRecentSearch: UITableViewCell {

    @IBOutlet weak var imgViewRecipe: UIImageView!
    @IBOutlet weak var lblRecipeName: UILabel!
    @IBOutlet weak var lblRecipeOwner: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func handleCell(vc:RecipeListVC,ip:ip) {
        
        let objRecipeListByCategory = vc.arrRecipeListRecentSearched[ip.row]
        
        let media_type = enum_media_type.init(rawValue: objRecipeListByCategory.media_type) ?? enum_media_type.image
        
        self.imgViewRecipe.image = UIImage.init(named: "AssetCollectionPlaceholder")!
        
        switch media_type {
        case .image:
            self.imgViewRecipe.mk_afSetImage(urlString: objRecipeListByCategory.media_url, defaultImage: "AssetCollectionPlaceholder") { (img, isImage) in
                if isImage {
                    self.imgViewRecipe.image = img
                }
            }
            break
        case .video:
            self.imgViewRecipe.mk_afSetImage(urlString: objRecipeListByCategory.video_thumb_url, defaultImage: "AssetCollectionPlaceholder") { (img, isImage) in
                if isImage {
                    self.imgViewRecipe.image = img
                }
            }
            break
        }
        
        self.lblRecipeName.text     = objRecipeListByCategory.name
        self.lblRecipeOwner.text    = objRecipeListByCategory.creator_name
        self.lblRating.text         = "\(objRecipeListByCategory.ratings.rounded(toPlaces: 1))"
        
        if objRecipeListByCategory.web_url != "" && objRecipeListByCategory.is_from_scrapper == 0  && objRecipeListByCategory.ingredientLines.count == 0 && objRecipeListByCategory.creator_name != "The Open Pan" {
            self.lblRecipeOwner.text = objRecipeListByCategory.view_url //objRecipeListByCategory.web_url
        }
        
        if objRecipeListByCategory.is_from_scrapper.mk_boolValue {
            self.lblRecipeOwner.text    = objRecipeListByCategory.view_url //objRecipeListByCategory.web_url
        }
        
    }

}
