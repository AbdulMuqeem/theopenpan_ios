//
//  CellRecipeDetailsNutritions.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/13/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class CellRecipeDetailsNutritions: UITableViewCell {

    
    @IBOutlet var lblCalories                       : UILabel!
    @IBOutlet var lblTotalFat                       : UILabel!
    @IBOutlet var lblTotalFat_n                     : UILabel!
    
    @IBOutlet var lblSatFat                         : UILabel!
    @IBOutlet var lblSatFat_n                       : UILabel!
    
    @IBOutlet var lblSodium                         : UILabel!
    @IBOutlet var lblSodium_n                       : UILabel!
    
    @IBOutlet var lblTotalCarbo                     : UILabel!
    @IBOutlet var lblTotalCarbo_n                   : UILabel!
    
    @IBOutlet var lblDietaryFiber                   : UILabel!
    @IBOutlet var lblDietaryFiber_n                 : UILabel!
    
    @IBOutlet var lblTotalSugar                     : UILabel!
    
    @IBOutlet var lblProtein                        : UILabel!
    @IBOutlet var lblProtein_n                      : UILabel!
    
    @IBOutlet var lblCalcium                        : UILabel!
    @IBOutlet var lblCalcium_n                      : UILabel!
    
    @IBOutlet var lblIron                           : UILabel!
    @IBOutlet var lblIron_n                         : UILabel!
    
    @IBOutlet var lblPotassium                      : UILabel!
    @IBOutlet var lblPotassium_n                    : UILabel!
    
    @IBOutlet var viewLoader                        : UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func handleCell(vc:RecipeDetailsVC,ip:ip) {
        if vc.objParsedRes.calories != 0 {
            self.lblCalories.text = "\(vc.objParsedRes.calories)"
            
            self.lblTotalFat_n.text = "Total Fat \(vc.objParsedRes.totalFatQty_n) \(vc.objParsedRes.totalFatUnit_n)"
            self.lblTotalFat.text = "\(vc.objParsedRes.totalFatQty) \(vc.objParsedRes.totalFatUnit)"
            
            self.lblSatFat_n.text = "Saturated Fat \(vc.objParsedRes.totalFatSatQty_n) \(vc.objParsedRes.totalFatSatUnit_n)"
            self.lblSatFat.text = "\(vc.objParsedRes.totalFatSatQty) \(vc.objParsedRes.totalFatSatUnit)"

            self.lblSodium_n.text = "Sodium \(vc.objParsedRes.sodiumQty_n) \(vc.objParsedRes.sodiumUnit_n)"
            self.lblSodium.text = "\(vc.objParsedRes.sodiumQty) \(vc.objParsedRes.sodiumUnit)"

            self.lblTotalCarbo_n.text = "Total Carbohydrate \(vc.objParsedRes.carbQty_n) \(vc.objParsedRes.carbUnit_n)"
            self.lblTotalCarbo.text = "\(vc.objParsedRes.carbQty) \(vc.objParsedRes.carbUnit)"

            self.lblDietaryFiber_n.text = "Dietary Fiber \(vc.objParsedRes.dietFiberQty_n) \(vc.objParsedRes.dietFiberUnit_n)"
            self.lblDietaryFiber.text = "\(vc.objParsedRes.dietFiberQty) \(vc.objParsedRes.dietFiberUnit)"

            self.lblTotalSugar.text = "Total Sugars \(vc.objParsedRes.sugarQty_n) \(vc.objParsedRes.sugarUnit_n)"
            
            self.lblProtein_n.text = "Protein \(vc.objParsedRes.proteinQty_n) \(vc.objParsedRes.proteinUnit_n)"
            self.lblProtein.text = "\(vc.objParsedRes.proteinQty) \(vc.objParsedRes.proteinUnit)"
            
            self.lblCalcium_n.text = "Calcium \(vc.objParsedRes.calciumQty_n) \(vc.objParsedRes.calciumUnit_n)"
            self.lblCalcium.text = "\(vc.objParsedRes.calciumQty) \(vc.objParsedRes.calciumUnit)"
            
            self.lblIron_n.text = "Iron \(vc.objParsedRes.ironQty_n) \(vc.objParsedRes.ironUnit_n)"
            self.lblIron.text = "\(vc.objParsedRes.ironQty) \(vc.objParsedRes.ironUnit)"
            
            self.lblPotassium_n.text = "Potassium \(vc.objParsedRes.potassiumQty_n) \(vc.objParsedRes.potassiumUnit_n)"
            self.lblPotassium.text = "\(vc.objParsedRes.potassiumQty) \(vc.objParsedRes.potassiumUnit)"
             
            
            
        }
    }

}
