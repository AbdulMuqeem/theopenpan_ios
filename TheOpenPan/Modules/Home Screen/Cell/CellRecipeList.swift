//
//  CellRecipeList.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/15/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class CellRecipeList: UICollectionViewCell {
    
    @IBOutlet weak var imgView:UIImageView!
    
    @IBOutlet weak var lblRecipeName:UILabel!
    @IBOutlet weak var lblRecipeMakerName:UILabel!
    
    @IBOutlet weak var lblStarRating:UILabel!
    
    func handleCell(vc:RecipeListVC,ip:ip) {
        
        let objRecipeListByCategory = vc.arrRecipeListByCategory[ip.row]
        
        let media_type = enum_media_type.init(rawValue: objRecipeListByCategory.media_type) ?? enum_media_type.image
        
        self.imgView.image = UIImage.init(named: "AssetCollectionPlaceholder")!
        
        switch media_type {
        case .image:
            self.imgView.mk_afSetImage(urlString: objRecipeListByCategory.media_url, defaultImage: "AssetCollectionPlaceholder") { (img, isImage) in
                if isImage {
                    self.imgView.image = img
                }
            }
            break
        case .video:
            self.imgView.mk_afSetImage(urlString: objRecipeListByCategory.video_thumb_url, defaultImage: "AssetCollectionPlaceholder") { (img, isImage) in
                if isImage {
                    self.imgView.image = img
                }
            }
            break
        }
        
        self.lblRecipeName.text         = objRecipeListByCategory.name
        self.lblRecipeMakerName.text    = objRecipeListByCategory.creator_name
        self.lblStarRating.text         = "\(objRecipeListByCategory.ratings.rounded(toPlaces: 1))"
        
//        if objRecipeListByCategory.web_url != "" {
//            self.lblRecipeMakerName.text    = objRecipeListByCategory.web_url
//        }
        
        if objRecipeListByCategory.view_url != "" {
            self.lblRecipeMakerName.text    = objRecipeListByCategory.view_url
        }
        
    }
    
    
    
}
