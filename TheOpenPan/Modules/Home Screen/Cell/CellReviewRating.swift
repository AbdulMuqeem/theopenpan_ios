//
//  CellReviewRating.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/12/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class CellReviewRating: UITableViewCell {
    
    
    @IBOutlet weak var lblTotalAverageStarRating        : UILabel!
    @IBOutlet weak var lblTotalNoOfRatingReview         : UILabel!
    
    @IBOutlet weak var progressBar1                     : LinearProgressView!
    @IBOutlet weak var progressBar2                     : LinearProgressView!
    @IBOutlet weak var progressBar3                     : LinearProgressView!
    @IBOutlet weak var progressBar4                     : LinearProgressView!
    @IBOutlet weak var progressBar5                     : LinearProgressView!
    
    @IBOutlet weak var lblNumberOfFiveStar              : UILabel!
    @IBOutlet weak var lblNumberOfFourStar              : UILabel!
    @IBOutlet weak var lblNumberOfThreeStar             : UILabel!
    @IBOutlet weak var lblNumberOfTwoStar               : UILabel!
    @IBOutlet weak var lblNumberOfOneStar               : UILabel!
    
    //USERS COMMENT
    @IBOutlet weak var lblReviewerName                  : UILabel!
    @IBOutlet weak var lblReviewDesc                    : UILabel!
    @IBOutlet weak var lblReviewerRating                : UILabel!
    @IBOutlet weak var lblReviewDateDaysAgo             : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func handleCell(vc:ReviewAndRatingVC, ip:ip) {
        
        let rrm = vc.reviewRatingModel
        
        self.lblTotalAverageStarRating.text     = "\(rrm.final_rating.rounded(toPlaces: 1))"
        self.lblTotalNoOfRatingReview.text      = "\(rrm.total_no_of_ratings) ratings and \(rrm.total_no_of_reviews) reviews"
        
        self.lblNumberOfFiveStar.text           = "\(rrm.ratings_categories.no_of_five_star)"
        self.lblNumberOfFourStar.text           = "\(rrm.ratings_categories.no_of_four_star)"
        self.lblNumberOfThreeStar.text          = "\(rrm.ratings_categories.no_of_three_star)"
        self.lblNumberOfTwoStar.text            = "\(rrm.ratings_categories.no_of_two_star)"
        self.lblNumberOfOneStar.text            = "\(rrm.ratings_categories.no_of_one_star)"
         
        self.progressBar5.progressValue = CGFloat(rrm.ratings_categories.no_of_five_star) / CGFloat(rrm.total_no_of_ratings) * 100
        self.progressBar4.progressValue = CGFloat(rrm.ratings_categories.no_of_four_star) / CGFloat(rrm.total_no_of_ratings) * 100
        self.progressBar3.progressValue = CGFloat(rrm.ratings_categories.no_of_three_star) / CGFloat(rrm.total_no_of_ratings) * 100
        self.progressBar2.progressValue = CGFloat(rrm.ratings_categories.no_of_two_star) / CGFloat(rrm.total_no_of_ratings) * 100
        self.progressBar1.progressValue = CGFloat(rrm.ratings_categories.no_of_one_star) / CGFloat(rrm.total_no_of_ratings) * 100
        
        self.progressBar2.barColor = #colorLiteral(red: 0.9921568627, green: 0.6274509804, blue: 0, alpha: 1)
        self.progressBar1.barColor = #colorLiteral(red: 1, green: 0.2549019608, blue: 0.2470588235, alpha: 1)
        
    }
    
    func handleCellUserComment(vc:ReviewAndRatingVC, ip:ip) {
        
        let rrm = vc.reviewRatingModel
        let review_list = rrm.review_list[ip.row]
        
        let date = Date.init(timeIntervalSince1970: TimeInterval(review_list.created_at))
        
        let calendar = Calendar.current
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: date)
        let date2 = calendar.startOfDay(for: Date())
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        
        self.lblReviewerName.text           = review_list.reviewer_name
        self.lblReviewDesc.text             = review_list.review_desc
        self.lblReviewerRating.text         = "\(review_list.rating)"
        
        if let days = components.day {
            if days == 0 {
                self.lblReviewDateDaysAgo.text = "Today"
            } else {
                self.lblReviewDateDaysAgo.text = "\(days) days ago"
            }
        } else {
            self.lblReviewDateDaysAgo.text = "Today"
        }
        
    }
    
}
