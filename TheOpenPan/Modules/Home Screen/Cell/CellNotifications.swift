//
//  CellNotifications.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/13/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

enum enum_notification_type : String {
    
    case rating                                 = "rating"
    case admin_recipe_status_accepted           = "admin_recipe_status_accepted"
    case admin_recipe_status_rejected           = "admin_recipe_status_rejected"
    case admin_recipe_updated                   = "admin_recipe_updated"
    case admin                                  = "admin"
    case none                                   = ""
    
}


class CellNotifications: UITableViewCell {

    @IBOutlet weak var lblDateTime                  : UILabel!
    @IBOutlet weak var lblNotificationTitle         : UILabel!
    @IBOutlet weak var lblNotificationText          : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func handleCell(vc:NotificationsVC,ip:ip) {
        
        let notification = vc.arrNotificationModel[ip.row]

        let date = Date.init(timeIntervalSince1970: TimeInterval(notification.created_at))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy • hh:mm a"
        let stringDate = dateFormatter.string(from: date)
        
        self.lblDateTime.text = stringDate
        self.lblNotificationTitle.text = notification.title
        self.lblNotificationText.text = notification.description
        
        if notification.is_read.mk_boolValue {
            self.lblDateTime.alpha = 0.6
            self.lblNotificationText.alpha = 0.6
            self.lblNotificationTitle.alpha = 0.6
            self.lblNotificationText.font = enum_font.regular.font(15)
        } else {
            self.lblDateTime.alpha = 1
            self.lblNotificationText.alpha = 1
            self.lblNotificationTitle.alpha = 1
            self.lblNotificationText.font = enum_font.medium.font(15)
        }
                
//        notification
    }

}
