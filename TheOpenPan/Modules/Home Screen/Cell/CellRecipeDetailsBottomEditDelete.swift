//
//  CellRecipeDetailsApproval.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/21/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class CellRecipeDetailsBottomEditDelete: UITableViewCell {
    
    @IBOutlet weak var btnEdit      : UIButton!
    @IBOutlet weak var btnRemove    : UIButton!
    var imgViewTempToDownloadImage  = UIImageView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func handleCell(vc:RecipeDetailsVC,ip:ip) {
        
        self.btnEdit.onTap {
            
//            var title                           = String()
//            var is_video                        = Int()
//            var media_url                       = String()
//            var video_thumb_url                 = String()
//            var imported_media_url              = String()
//            var imported_video_thumb_url        = String()
//
//            var ready_time_hours                : Int?
//            var ready_time_mins                 : Int?
//            var serves                          = Int()
//            var direction_list                  = [Direction_list]()
//            var ingredient_list                 = [Ingredient_list]()
//
//            var is_easy                         : Bool = false
//            var submit_recipe_for_review        : Bool = false
//            var is_one_pot                      : Bool = false
//            var is_own_recipe                   : Bool = false
//            var web_recipe_url                  = String()
//            var collection_id                   = Int64()
//
//            var live_media_url                  = String()
//            var live_video_thumb_url            = String()
//
//            var image_temp_to_crop              : UIImage!
            
            var objAddNewRecipeData = AddNewRecipeData()
            objAddNewRecipeData.title = vc.recipeDetails.name
            objAddNewRecipeData.is_video = (enum_media_type(rawValue: vc.recipeDetails.media_type) == .video).mk_intValue
            objAddNewRecipeData.media_url = vc.recipeDetails.media_url
            
            
            if (enum_media_type(rawValue: vc.recipeDetails.media_type) == .video) {
                objAddNewRecipeData.imported_media_url = vc.recipeDetails.media_url
                objAddNewRecipeData.video_thumb_url = vc.recipeDetails.video_thumb_url
                objAddNewRecipeData.imported_video_thumb_url = vc.recipeDetails.video_thumb_url
            }
            
            //            var imported_media_url              = String()
            //            var imported_video_thumb_url        = String()
            
            objAddNewRecipeData.ready_time_hours = vc.recipeDetails.ready_time_hours
            objAddNewRecipeData.ready_time_mins = vc.recipeDetails.ready_time_mins
            objAddNewRecipeData.serves = vc.recipeDetails.original_serves
            objAddNewRecipeData.direction_list = vc.recipeDetails.directions_list
            objAddNewRecipeData.ingredient_list = vc.recipeDetails.original_serves_ingredient_list
            objAddNewRecipeData.is_easy = vc.recipeDetails.is_easy.mk_boolValue
            
            if enum_recipe_status.init(rawValue: vc.recipeDetails.recipe_status) == .approved {
                objAddNewRecipeData.submit_recipe_for_review = true
            }

            objAddNewRecipeData.is_one_pot = vc.recipeDetails.is_one_pot_meal.mk_boolValue
            objAddNewRecipeData.is_own_recipe = vc.recipeDetails.is_own_recipe.mk_boolValue
            
            objAddNewRecipeData.web_recipe_url = vc.recipeDetails.web_url
            objAddNewRecipeData.collection_id = vc.recipeDetails.cookbook_id
            objAddNewRecipeData.recipe_status = vc.recipeDetails.recipe_status
            
            var arrKeywords = [Keyword]()
            
            arrKeywords = vc.recipeDetails.keywords_list.map({ (km) -> Keyword in
                let objKeyword = Keyword.mr_createEntity(in: app_default_context)!
                objKeyword.desc = km.description
                return objKeyword
                
            })
            
//            arrKeywords
            
            let arrIdsCuisine = vc.recipeDetails.cuisine_type_id_list.components(separatedBy: ",")
            let arrCuisineTags              = enum_tag_list_type.cusineType.getAllTags()
            var arrCuisineTagsSelected      = [Category_tag]()
            for tag in arrCuisineTags {
                if arrIdsCuisine.contains("\(tag.tag_id)") {
                    tag.is_selected = true
                    arrCuisineTagsSelected.append(tag)
                }
            }
            
            
            let arrIdsMealType = vc.recipeDetails.meal_type_id_list.components(separatedBy: ",")
            var arrMealTypeTags = [Category_tag]()
            let arrAllMealTypeTags = enum_tag_list_type.mealType.getAllTags()
            
            for tag in arrAllMealTypeTags {
                if arrIdsMealType.contains("\(tag.tag_id)") {
                    tag.is_selected = true
                    arrMealTypeTags.append(tag)
                }
            }
            
            
            let arrIdsDietType = vc.recipeDetails.diet_id_list.components(separatedBy: ",")
            var arrDietTags = [Category_tag]()
            let arrAllDietTags = enum_tag_list_type.diet.getAllTags()
            
            for tag in arrAllDietTags {
                if arrIdsDietType.contains("\(tag.tag_id)") {
                    tag.is_selected = true
                    arrDietTags.append(tag)
                }
            }
            
            
            apde.showLoader()
            
            if (enum_media_type(rawValue: vc.recipeDetails.media_type) == .image) {
                
                objAddNewRecipeData.video_thumb_url = ""
                objAddNewRecipeData.imported_media_url = vc.recipeDetails.media_url
                objAddNewRecipeData.media_url = vc.recipeDetails.media_url
                
                
                if vc.recipeDetails.media_url != "" && vc.recipeDetails.media_url != nil {
                    if let urlImage = URL.init(string: vc.recipeDetails.media_url) {
                        self.imgViewTempToDownloadImage.sd_setImage(with: urlImage) { (img, err, typecache, lastUrl) in
                            if let recipeImage = img {
                                if let imageLocalUrl = recipeImage.mk_saveToDocuments(filename: "\(Date().mk_ticks).jpg", folderName: tmpDocFolderName) {
//                                    objAddNewRecipeData.media_url                 = imageLocalUrl
                                    objAddNewRecipeData.is_video                  = false.mk_intValue
                                    objAddNewRecipeData.image_temp_to_crop        = recipeImage
                                    
                                    apde.hideLoader()
                                    self.pushAddNewRecipeVc(vc: vc, addNewRecipeData: objAddNewRecipeData, arrKeywords: arrKeywords, arrCuisineTags: arrCuisineTagsSelected, arrMealTypeTags: arrMealTypeTags, arrDietTags: arrDietTags)
                                }
                            }
                        }
                    }
                } else {
                    apde.hideLoader()
                    self.pushAddNewRecipeVc(vc: vc, addNewRecipeData: objAddNewRecipeData, arrKeywords: arrKeywords, arrCuisineTags: arrCuisineTagsSelected, arrMealTypeTags: arrMealTypeTags, arrDietTags: arrDietTags)
                }
                
                
            } else {
                
                apde.hideLoader()
                self.pushAddNewRecipeVc(vc: vc, addNewRecipeData: objAddNewRecipeData, arrKeywords: arrKeywords, arrCuisineTags: arrCuisineTagsSelected, arrMealTypeTags: arrMealTypeTags, arrDietTags: arrDietTags)
            }
            
            
            
            
        }
        
        self.btnRemove.onTap {
            
            vc.removeRecipeFlow(vc: vc)
                        
        }
    }
    
    func pushAddNewRecipeVc(vc:RecipeDetailsVC, addNewRecipeData:AddNewRecipeData,arrKeywords:[Keyword],arrCuisineTags:[Category_tag],arrMealTypeTags:[Category_tag],arrDietTags:[Category_tag]) {
        
        let objAddNewRecipeVC = apde.getController(vc_name: "AddNewRecipeVC", sb: .AddRecipe) as! AddNewRecipeVC
        
        objAddNewRecipeVC.arrAddedKeywords              = arrKeywords
        objAddNewRecipeVC.arrSelectedCuisineTags        = arrCuisineTags
        objAddNewRecipeVC.arrSelectedMealTypeTags       = arrMealTypeTags
        objAddNewRecipeVC.arrSelectedDietTags           = arrDietTags
        
        for (i,direction) in addNewRecipeData.direction_list.enumerated() {
            if let objDirection = Direction.mr_createEntity(in: app_default_context) {
                objDirection.step = Int64(i)
                objDirection.desc = direction.description
                objAddNewRecipeVC.arrDirections.append(objDirection)
            }
        }
        
        for (_,ingredient) in addNewRecipeData.ingredient_list.enumerated() {
            if let objIngredient = Ingredient.mr_createEntity(in: app_default_context) {
                objIngredient.id        = ingredient.id
                objIngredient.name      = ingredient.name
                objIngredient.note      = ingredient.note
                objIngredient.unit_name = ingredient.unit_name
                objIngredient.qty       = ingredient.qty
                objAddNewRecipeVC.arrIngredints.append(objIngredient)
            }
        }
        
        objAddNewRecipeVC.givenTitle        = "Edit Recipe".localize()
        objAddNewRecipeVC.isInEditMode      = true
        objAddNewRecipeVC.recipeIdEditMode  = vc.recipeDetails.id
        objAddNewRecipeVC.addNewRecipeData  = addNewRecipeData
        objAddNewRecipeVC.editedDoneEditMode = {
            vc.getRecipeDetailsApiCall()
            updateMyRecipesVC(vcSelf: vc)
        }
        
        vc.navigationController?.pushViewController(objAddNewRecipeVC, animated: true)
    }
    

    
    

    
    
//    func recipeApprovalApiCall(recipe_id: Int64, accept: Int, compHandler: @escaping ()->()) {
//
//        apde.showLoader()
//
//        recipeApproval(recipe_id: recipe_id, accept: accept) { (res) in
//            switch res {
//            case .success(let dictData, let message) :
//
//                break
//            case .failed(message: let msg):
//                BasicFunctions.showError(strError: msg)
//                break
//            case .error(error: let error):
//                BasicFunctions.showError(strError: error)
//                break
//            }
//            compHandler()
//            apde.hideLoader()
//        }
//
//
//    }
    
}


func recipeDeleteApiCall(vc:RecipeDetailsVC, recipe_id: Int64, compHandler: @escaping (String)->()) {
    
            apde.showLoader()
    
            recipeDelete(recipe_id: recipe_id) { (res) in
                switch res {
                case .success(_, let message) :
                    BasicFunctions.showInfo(strError: message)
                    compHandler(message)
                    break
                case .failed(message: let msg):
                    BasicFunctions.showError(strError: msg)
                    compHandler(msg)
                    break
                case .error(error: let error):
                    BasicFunctions.showError(strError: error)
                    compHandler(error)
                    break
                }
                
                apde.hideLoader()
            }
    
    
        }

func recipeRemoveFromCookbookApiCall(vc:RecipeDetailsVC, cookbook_id: Int64, compHandler: @escaping (String)->()) {

        apde.showLoader()

        recipeRemoveFromCookbook(cookbook_id: cookbook_id) { (res) in
            switch res {
            case .success(_, let message) :
                
                BasicFunctions.showInfo(strError: message)
                compHandler(message)
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                compHandler(msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                compHandler(error)
                break
            }
            
            apde.hideLoader()
        }


    }
