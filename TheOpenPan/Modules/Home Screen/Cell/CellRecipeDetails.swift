//
//  CellRecipeDetails.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/15/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import SwiftSoup
import SwiftyJSON
import Alamofire
import MultiProgressView

class CellRecipeDetails: UITableViewCell {
    
    var currentVC                                       : RecipeDetailsVC!
    
    @IBOutlet var lblRecipeTitle                        : UILabel!
    @IBOutlet var lblRecipeLinkOrCreatorName            : UILabel!
    @IBOutlet var lblReadyTime                          : UILabel!
    @IBOutlet var lblRatings                            : UILabel!
    @IBOutlet var viewBackTime                          : UIView!
    
    @IBOutlet var btnRatings                            : UIButton!
    @IBOutlet var btnTopHeaderBtn1SaveTag               : UIButton!
    @IBOutlet var btnTopHeaderBtn2Share                 : UIButton!
    @IBOutlet var btnTopHeaderBtn3AddToPlan             : UIButton!
    
    typealias ButtonTappedBlock                         = (UIButton)->()
    @IBOutlet var btnSegment1                           : UIButton!
    var btnSegment1TappedBlock                          : ButtonTappedBlock!
    @IBOutlet var btnSegment2                           : UIButton!
    var btnSegment2TappedBlock                          : ButtonTappedBlock!
    
    @IBOutlet var btnUsMetric                           : UIButton!
    //    @IBOutlet var btnIngredientSelectAll                : UIButton!
    @IBOutlet var stackViewWithSelectAllButton          : UIStackView!
    @IBOutlet var stackViewWithServeIncDecrButtons      : UIStackView!
    
    @IBOutlet var stackViewParentRatingsSaveSharePlanButton      : UIStackView!
    @IBOutlet var lblServeCount                         : UILabel!
    @IBOutlet var btnIncreaseServe                      : UIButton!
    @IBOutlet var btnDecreaseServe                      : UIButton!
    
    @IBOutlet var btnSelectAllIngs                      : UIButton!
    @IBOutlet var btnIngredientCheckBox                 : UIButton!
    @IBOutlet var lblIngredientQty                      : UILabel!
    @IBOutlet var lblIngredientName                     : UILabel!
    
    @IBOutlet var btnAddIngToShop                       : UIButton!
    
    @IBOutlet weak var lblIndexNumber                   : UILabel!
    @IBOutlet weak var txtViewDirection                 : UITextView!
    var kwStepper                                       : KWStepper!
    
    //Muqeem
    var arrImportedKeywords                     = [Keyword]()
    var arrImportedCuisineTags                  = [Category_tag]()
    var arrImportedMealTypeTags                 = [Category_tag]()
    
    typealias Block                             = (AddNewRecipeData)->()
    typealias BlockVoid                         = ()->()
    var compBlockError                          : BlockVoid!
    var compBlockWithUrl                        : Block!
    var doc                                     : Document!
    var addNewRecipeData                        = AddNewRecipeData.init()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if btnSegment1 != nil {
            self.setupSegmentControl()
        }
        
        if btnDecreaseServe != nil {
            self.kwStepper = KWStepper(decrementButton: self.btnDecreaseServe, incrementButton: self.btnIncreaseServe)
            self.kwStepper.autoRepeat = true
            self.kwStepper.autoRepeatInterval = 0.10
            self.kwStepper.wraps = false
            self.kwStepper.minimumValue = 1
            self.kwStepper.maximumValue = 10000
            self.kwStepper.incrementStepValue = 1
            self.kwStepper.decrementStepValue = 1
        }
        
        if self.btnUsMetric != nil {
            
            self.setSelected(btn: self.btnUsMetric)
            self.btnUsMetric.mk_addTapHandler { (btn) in
                self.setSelected(btn: btn)
            }
        }
        
    }
    
    func setSelected(btn:UIButton) {
        btn.isSelected = !btn.isSelected
        let formattedString = NSMutableAttributedString()
        if btn.isSelected {
            formattedString.bold("US").normal(" | METRIC")
        } else {
            formattedString.normal("US | ").bold("METRIC")
        }
        btn.setAttributedTitle(formattedString, for: .normal)
    }
    
    
    override func layoutSubviews() {
        
        if btnSegment1 != nil {
            let shapelayer = CAShapeLayer.init()
            let bzPath = UIBezierPath.init(roundedRect: btnSegment1.bounds, byRoundingCorners: [.topLeft,.bottomLeft], cornerRadii: .init(width: 25, height: 25))
            shapelayer.path = bzPath.cgPath
            btnSegment1.layer.mask = shapelayer
            
            let shapelayer2 = CAShapeLayer.init()
            let bzPath2 = UIBezierPath.init(roundedRect: btnSegment1.bounds, byRoundingCorners: [.topRight,.bottomRight], cornerRadii: .init(width: 25, height: 25))
            shapelayer2.path = bzPath2.cgPath
            btnSegment2.layer.mask = shapelayer2
        }
        
    }
    
    func setupSegmentControl() {
        
        self.btnSegment1.mk_addTapHandler { (btn) in
            
            if self.btnSegment1.isSelected {
                return
            }
            
            self.currentVC.recipeDetails.is_direction_selected = false
            self.selectButton(btnSelect: self.btnSegment1, btnDeselect: self.btnSegment2)
            self.btnSegment1TappedBlock(self.btnSegment1)
        }
        
        self.btnSegment2.mk_addTapHandler { (btn) in
            
            if self.btnSegment2.isSelected {
                return
            }
            
            self.currentVC.recipeDetails.is_direction_selected = true
            self.selectButton(btnSelect: self.btnSegment2, btnDeselect: self.btnSegment1)
            self.btnSegment2TappedBlock(self.btnSegment2)
        }
        
    }
    
    func selectButton(btnSelect:UIButton,btnDeselect:UIButton) {
        
        hapSoft()
        
        if btnSelect.isSelected {
            btnSelect.titleLabel?.font = UIFont.applyCustomMedium(fontSize: 18)
            btnSelect.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.8941176471, blue: 0.8784313725, alpha: 1)
            return
        }
        
        btnSelect.titleLabel?.font = UIFont.applyCustomMedium(fontSize: 18)
        btnSelect.isSelected = true
        btnSelect.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.8941176471, blue: 0.8784313725, alpha: 1)
        
        btnDeselect.titleLabel?.font = UIFont.applyCustomRegular(fontSize: 18)
        btnDeselect.isSelected = false
        btnDeselect.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
    }
    
    
    func handleCellTopHeader(vc:RecipeDetailsVC, indexPath:IndexPath) {
        
        self.currentVC = vc
        
        let rd = vc.recipeDetails
        
        self.lblRecipeTitle.text = rd.name
        
        self.viewBackTime?.isHidden = false
        self.stackViewParentRatingsSaveSharePlanButton.arrangedSubviews.first?.isHidden = false
        self.stackViewParentRatingsSaveSharePlanButton.arrangedSubviews[1].isHidden = false
        self.stackViewParentRatingsSaveSharePlanButton.arrangedSubviews[2].isHidden = false
        
        if vc.recipeDetails.ready_time_mins == 0 && vc.recipeDetails.ready_time_hours == 0 {
            self.viewBackTime?.isHidden = true
        }
        
        //        if self.currentVC.isThird {
        //            self.viewBackTime?.isHidden = true
        //            self.stackViewParentRatingsSaveSharePlanButton.arrangedSubviews.first?.isHidden = true
        //            self.stackViewParentRatingsSaveSharePlanButton.arrangedSubviews[1].isHidden = true
        //            self.stackViewParentRatingsSaveSharePlanButton.arrangedSubviews[2].isHidden = true
        //        }
        
        // Link or Owner name
        switch enum_recipe_owner_type(rawValue: rd.recipe_owner_type) {
        case .admin_recipe :
            self.lblRecipeLinkOrCreatorName.text = "By The Open Pan"
            if rd.view_url == "" {
                self.lblRecipeLinkOrCreatorName.text = "By " + rd.creator_name
            }
            else {
                self.lblRecipeLinkOrCreatorName.text = rd.view_url
            }
            break
        case .user_recipe :
            if rd.view_url == "" {
                self.lblRecipeLinkOrCreatorName.text = "By " + rd.creator_name
            } else {
                self.lblRecipeLinkOrCreatorName.text = rd.view_url
                self.lblRecipeLinkOrCreatorName.isUserInteractionEnabled = true
                self.lblRecipeLinkOrCreatorName.addTapGesture { (gesture) in
                    hapSoft()
                    rd.web_url.mk_openUrl()
                }
            }
            break
        case .third_party_recipe :
            self.lblRecipeLinkOrCreatorName.text = rd.view_url //rd.web_url
            self.lblRecipeLinkOrCreatorName.isUserInteractionEnabled = true
            self.lblRecipeLinkOrCreatorName.addTapGesture { (gesture) in
                hapSoft()
                rd.web_url.mk_openUrl()
            }
            break
        case .none:
            self.lblRecipeLinkOrCreatorName.text = rd.view_url //rd.web_url
            break
        }
        
        
        // Ready time
        if rd.ready_time_hours == 0 && rd.ready_time_mins != 0 {
            self.lblReadyTime.text = "\(rd.ready_time_mins) mins"
        }
        if rd.ready_time_hours != 0 && rd.ready_time_mins == 0 {
            if rd.ready_time_hours == 1 {
                self.lblReadyTime.text = "\(rd.ready_time_hours) hour"
            } else {
                self.lblReadyTime.text = "\(rd.ready_time_hours) hours"
            }
        }
        if rd.ready_time_hours == 1 && rd.ready_time_mins != 0 {
            self.lblReadyTime.text = "\(rd.ready_time_hours) hour \(rd.ready_time_mins) mins"
        }
        if rd.ready_time_hours != 0 && rd.ready_time_mins != 0 {
            self.lblReadyTime.text = "\(rd.ready_time_hours) hours \(rd.ready_time_mins) mins"
        }
        
        //Ratings
        
        if rd.no_of_ratings == 0 {
            self.lblRatings.text  = "No ratings yet"
        } else {
            self.lblRatings.text  = "\(rd.ratings.rounded(toPlaces: 1))  (\(rd.no_of_ratings) ratings)"
        }
        
        if vc.recipeDetails.is_btn_add_ing_to_shoplist_tapped {
            self.stackViewWithServeIncDecrButtons.arrangedSubviews.first?.isHidden      = true
            self.stackViewWithSelectAllButton.arrangedSubviews.first?.isHidden          = false
        } else {
            self.stackViewWithServeIncDecrButtons.arrangedSubviews.first?.isHidden      = false
            self.stackViewWithSelectAllButton.arrangedSubviews.first?.isHidden          = true
        }
        
        if vc.recipeDetails.is_direction_selected {
            self.selectButton(btnSelect: self.btnSegment2, btnDeselect: self.btnSegment1)
            self.stackViewWithServeIncDecrButtons.arrangedSubviews.first?.isHidden      = true
            self.stackViewWithSelectAllButton.arrangedSubviews.first?.isHidden          = true
        } else {
            self.selectButton(btnSelect: self.btnSegment1, btnDeselect: self.btnSegment2)
        }
        
        
        self.lblServeCount?.isHidden = false
        self.btnIncreaseServe?.isHidden = false
        self.btnDecreaseServe?.isHidden = false
        
        if vc.recipeDetails.serves == 0 {
            self.lblServeCount?.isHidden = true
            self.btnIncreaseServe?.isHidden = true
            self.btnDecreaseServe?.isHidden = true
        }
        
        self.kwStepper.value = Double(vc.recipeDetails.serves)
        self.kwStepper.valueChangedCallback = { stepper in
            
            let strServeCount = stepper.value > 9 ? String(Int(stepper.value)) : String(format: "%02d", Int(stepper.value))
            self.lblServeCount.text = "Serves " + strServeCount
            hapSoft()
            
            let original_serves_ingredient_list = vc.recipeDetails.original_serves_ingredient_list
            var arrIngredients                  = [Ingredient_list]()
            for original_serves_ingredient in original_serves_ingredient_list {
                
                var ingredient                  = Ingredient_list()
                ingredient                      = original_serves_ingredient
                
                if let dblQty = Double(original_serves_ingredient.qty) {
                    
                    let singleQty   = dblQty/Double(vc.recipeDetails.original_serves)
                    ingredient.qty  = "\((singleQty*self.kwStepper.value).rounded(toPlaces: 2).setPriceFormatted())"
                    
                } else {
                    
                    
                    if Int(self.kwStepper.value) == vc.recipeDetails.original_serves {
                        
                    } else {
                        
                        var qty = removeSpace(original_serves_ingredient.qty)
                        
                        if qty.first == " " {
                            qty = String(qty.dropFirst())
                        }
                        
                        if qty.last == " " {
                            qty = String(qty.dropLast())
                        }
                        
                        if qty.components(separatedBy: "/").count > 1 {
                            
                            if let f = qty.components(separatedBy: "/").first, let l = qty.components(separatedBy: "/").last {
                                
                                if f != "" && l != "" {
                                    
                                    // case 1 1/2 --- one and a half
                                    if f.components(separatedBy: " ").count > 1 {
                                        if let intFirst = f.components(separatedBy: " ").first?.integer {
                                            if let intLast = f.components(separatedBy: " ").last?.integer {
                                                if let intLastBehindSlash = Int(l) {
                                                    let dbl = (Double(intLast)/Double(intLastBehindSlash)).rounded(toPlaces: 2)
                                                    let total = dbl + Double(intFirst)
                                                    let singleQty   = total/Double(vc.recipeDetails.original_serves)
                                                    let increasedQty = (singleQty*self.kwStepper.value).rounded(toPlaces: 2)
                                                    
                                                    if increasedQty.whole != 0.0 && increasedQty.fraction != 0.0 {
                                                        let strFraction = float(toFraction: Float(increasedQty.fraction))
                                                        ingredient.qty = "\(Int(increasedQty.whole)) \(strFraction)"
                                                    } else if increasedQty.whole == 0.0 && increasedQty.fraction != 0.0 {
                                                        let strFraction = float(toFraction: Float(increasedQty.fraction))
                                                        ingredient.qty = "\(strFraction)"
                                                    } else {
                                                        ingredient.qty = "\(Int(increasedQty.whole))"
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        // case 1/2 --- half
                                        if let intFirst = Int(f),let intLast = Int(l) {
                                            
                                            let dbl = (Double(intFirst)/Double(intLast)).rounded(toPlaces: 4)
                                            let singleQty       = dbl/Double(vc.recipeDetails.original_serves)
                                            let increasedQty    = (singleQty*self.kwStepper.value).rounded(toPlaces: 4)
                                            
                                            if increasedQty.whole != 0.0 && increasedQty.fraction != 0.0 {
                                                let strFraction = float(toFraction: Float(increasedQty.fraction.rounded(toPlaces: 2)))
                                                ingredient.qty = "\(Int(increasedQty.whole)) \(strFraction)"
                                            } else if increasedQty.whole == 0.0 && increasedQty.fraction != 0.0 {
                                                let strFraction = float(toFraction: Float(increasedQty.fraction.rounded(toPlaces: 3)))
                                                ingredient.qty = "\(strFraction)"
                                            } else {
                                                ingredient.qty = "\(Int(increasedQty.whole))"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                arrIngredients.append(ingredient)
            }
            vc.recipeDetails.ingredient_list        = arrIngredients
            vc.recipeDetails.serves                 = Int(stepper.value)
            
            vc.manualTableViewReload()
            
        }
        self.kwStepper.longPressEnded { stepper in
            hapError()
        }.minValueClamped { (stepper) in
            hapError()
        }.maxValueClamped { (stepper) in
            hapError()
        }
        
        self.btnSelectAllIngs.mk_addTapHandler { (btn) in
            
            btn.isSelected = !btn.isSelected
            
            if btn.isSelected {
                for (i,_) in vc.recipeDetails.ingredient_list.enumerated() {
                    vc.recipeDetails.ingredient_list[i].isSelected = true
                    vc.tblView.reloadRows(at: [IndexPath.init(row: i, section: NrecipeDetailsIngredientSection)], with: .fade)
                }
            } else {
                for (i,_) in vc.recipeDetails.ingredient_list.enumerated() {
                    vc.recipeDetails.ingredient_list[i].isSelected = false
                    vc.tblView.reloadRows(at: [IndexPath.init(row: i, section: NrecipeDetailsIngredientSection)], with: .fade)
                }
            }
            
            vc.setImageToStretchyHeader()
        }
        
        self.handleSelectAllBtnState(vc: vc, indexPath: indexPath)
        
        self.btnRatings.mk_addTapHandler { (btn) in
            hapSoft()
            let objReviewAndRatingVC = apde.getController(vc_name: "ReviewAndRatingVC", sb: .ReviewAndRating) as! ReviewAndRatingVC
            objReviewAndRatingVC.recipe_id = vc.recipeDetails.id
            objReviewAndRatingVC.is_own_recipe = vc.recipeDetails.is_own_recipe.mk_boolValue
            objReviewAndRatingVC.blockReviewGiven = {
                vc.getRecipeDetailsApiCall()
            }
            vc.navigationController?.setNavigationBarHidden(false, animated: false)
            vc.navigationController?.pushViewController(objReviewAndRatingVC, animated: true)
        }
        
        self.btnTopHeaderBtn1SaveTag.isSelected = vc.recipeDetails.is_saved_in_cookbook.mk_boolValue
        
        self.btnTopHeaderBtn1SaveTag.mk_addTapHandler { (btn) in
            hapSoft()
            
            if kCurrentUser.is_guest {
                //                BasicFunctions.showInfo(strError: "Save recipe feature is not available without signing in.")
                apde.presentLoginViewForGuest(errMsg: "Save recipe feature is not available without signing in.")
                return
            }
            
            if vc.recipeDetails.is_own_recipe.mk_boolValue {
                if vc.recipeDetails.is_saved_in_cookbook.mk_boolValue {
                    vc.removeRecipeFlow(vc: vc)
                }
            } else {
                if vc.recipeDetails.is_saved_in_cookbook.mk_boolValue {
                    vc.removeRecipeFlow(vc: vc)
                } else {
                    if vc.isThird {
                        self.saveAndViewFlow(vc: vc)
                    } else {
                        self.btnTopHeaderBtn1SaveTagTapped(vc: vc, recipe_id: vc.recipeDetails.id)
                    }
                }
            }
        }
        
        //        self.btnTopHeaderBtn2Share.isHidden = true //Muqeem comment this line to show share button
        
        if vc.recipeDetails.recipe_status == enum_recipe_status.approved.rawValue {
            self.btnTopHeaderBtn2Share.isHidden = false
        }
        if vc.isThird {
            self.btnTopHeaderBtn2Share.isHidden = false
        }
        
        self.btnTopHeaderBtn2Share.mk_addTapHandler { (btn) in
            hapSoft()
            
            if kCurrentUser.is_guest {
                apde.presentLoginViewForGuest(errMsg: "Share recipe feature is not available without signing in.")
                return
            }
            
            //Muqeem comment this check
            if vc.isThird {
                //self.saveEdmamRecipe(vc: vc) // Muqeem add this line
                BasicFunctions.showError(strError: "We are working on this! For now, please save recipe first to use this function.")
                return
            } else {
                createFirebaseDynamicLink(recipe_id: "\(vc.recipeDetails.id)", recipe_name: vc.recipeDetails.name)
            }
            
            
            //            let objectsToShare = ["theopenpan:\\\\recipedetails\\130"]
            //            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            //            vc.present(activityVC, animated: true, completion: nil)
        }
        
        self.btnTopHeaderBtn3AddToPlan.mk_addTapHandler { (btn) in
            hapSoft()
            
            if kCurrentUser.is_guest {
                //                BasicFunctions.showInfo(strError: "Add to meal plan feature is not available without signing in.")
                apde.presentLoginViewForGuest(errMsg: "Add to meal plan feature is not available without signing in.")
                return
            }
            
            //Muqeem comment this check
            if self.currentVC.isThird {
                // self.saveAndViewFlow(vc: vc) // Muqeem add this line
                BasicFunctions.showError(strError: "We are working on this! For now, please save recipe first to use this function.")
                return
            }
                        
            let objAddToMealPlanVC = apde.getController(vc_name: "AddToMealPlanVC", sb: .MealPlanTab) as! AddToMealPlanVC
            objAddToMealPlanVC.recipe_id = vc.recipe_id
            vc.navigationController?.setNavigationBarHidden(false, animated: false)
            vc.navigationController?.pushViewController(objAddToMealPlanVC, animated: true)
        }
        
    }
    
    
    func btnTopHeaderBtn1SaveTagTapped(vc:RecipeDetailsVC, recipe_id:Int64) {
        saveRecipeDirectInCookbook(vc: vc, recipe_id: recipe_id)
    }
    
    
    
    func saveAndViewFlow(vc:RecipeDetailsVC) {
        
        let myURLString = vc.recipeDetails.web_url
        let objImportRecipePopupVC = apde.getController(vc_name: "ImportRecipePopupVC", sb: .AddRecipe) as! ImportRecipePopupVC
        objImportRecipePopupVC.compBlockWithUrl = { addNewRecipeData in
            
            var mutAddNewRecipeData = addNewRecipeData
            
            if vc.recipeDetails.serves > 0 {
                mutAddNewRecipeData.serves = vc.recipeDetails.serves
            }
            
            let objAddNewRecipeVC = makeReadyAddNewRecipeVcFromImport(addNewRecipeData: mutAddNewRecipeData, arrImportedKeywords:objImportRecipePopupVC.arrImportedKeywords, arrImportedCuisineTags:objImportRecipePopupVC.arrImportedCuisineTags, arrImportedMealTypeTags:objImportRecipePopupVC.arrImportedMealTypeTags)
            objAddNewRecipeVC.isEdmamRecipe = true
            objAddNewRecipeVC.edmamRecipeAdded = { (recipe_id) in
                vc.recipe_id = recipe_id
                vc.isThird = false
                vc.getRecipeDetailsApiCall()
                updateMyRecipesVC(vcSelf: vc)
            }
            vc.navigationController?.pushViewController(objAddNewRecipeVC, animated: true)
        }
        
        objImportRecipePopupVC.compBlockError = {
            RecipeParser.getRecipeData(url: myURLString) { (objAddNewRecipeData) in
                
                objImportRecipePopupVC.dismiss(animated: true) {
                    if let objAddNewRecipeData = objAddNewRecipeData {
                        
                        let objAddNewRecipeVC = makeReadyAddNewRecipeVcFromImport(addNewRecipeData: objAddNewRecipeData, arrImportedKeywords: [Keyword](), arrImportedCuisineTags: [Category_tag](), arrImportedMealTypeTags: [Category_tag]())
                        objAddNewRecipeVC.isEdmamRecipe = true
                        objAddNewRecipeVC.edmamRecipeAdded = { (recipe_id) in
                            vc.recipe_id = recipe_id
                            vc.isThird = false
                            vc.getRecipeDetailsApiCall()
                            updateMyRecipesVC(vcSelf: vc)
                        }
                        vc.navigationController?.pushViewController(objAddNewRecipeVC, animated: true)
                        
                    } else {
                        print("Error")
                        BasicFunctions.showError(strError: strErrorForRecipeNotFound)
                    }
                }
            }
        }
        
        vc.tabBarController?.present(objImportRecipePopupVC, animated: true, completion: nil)
        Timer.after(0.6) {
            objImportRecipePopupVC.txtUrl.text = myURLString
            objImportRecipePopupVC.btnImportTapped(btn: objImportRecipePopupVC.btnImport)
        }
        
    }
    
    
    
    func handleSelectAllBtnState(vc:RecipeDetailsVC, indexPath:IndexPath) {
        
        let arrFoundDeSelected = vc.recipeDetails.ingredient_list.filter({ (ing) -> Bool in
            ing.isSelected == false
        })
        if let cellTop = vc.tblView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? CellRecipeDetails {
            cellTop.btnSelectAllIngs.isSelected = arrFoundDeSelected.first == nil ? true : false
        }
        
    }
    
    
    // Ingredient cell
    func handleCellIngredientList(vc:RecipeDetailsVC, indexPath:IndexPath) {
        
        self.currentVC = vc
        
        if vc.recipeDetails.is_btn_add_ing_to_shoplist_tapped {
            self.btnIngredientCheckBox.isHidden     = false
        } else {
            self.btnIngredientCheckBox.isHidden     = true
        }
        
        let ingredient = vc.recipeDetails.ingredient_list[indexPath.row]
        self.btnIngredientCheckBox.isSelected   = ingredient.isSelected
        self.lblIngredientQty.text              = ingredient.qty
        self.lblIngredientName.text             = "\(ingredient.unit_name) \(ingredient.name)"
        
        self.btnIngredientCheckBox.mk_addTapHandler { (btn) in
            
            if let indexPathTapped = vc.tblView.indexPath(for: self) {
                
                var ingredientTapped                                        = vc.recipeDetails.ingredient_list[indexPathTapped.row]
                btn.isSelected                                              = !btn.isSelected
                ingredientTapped.isSelected                                 = !ingredientTapped.isSelected
                vc.recipeDetails.ingredient_list[indexPathTapped.row]       = ingredientTapped
                
                self.handleSelectAllBtnState(vc: vc, indexPath: indexPath)
            }
        }
        
    }
    
    func handleCellAddIngToShopListButton(vc:RecipeDetailsVC, indexPath:IndexPath) {
        
        self.currentVC = vc
        
        self.updateButtonTitleOfAddShopList()
        
        self.btnAddIngToShop.mk_addTapHandler { (btn) in
            
            //Muqeem comment this line
            if vc.isThird {
                BasicFunctions.showError(strError: "We are working on this! For now, please save recipe first to use this function.")
                return
            }
            
            
            if self.currentVC.isIngsAddedToShopList {
                
                //                self.currentVC.isIngsAddedToShopList = false
                //                self.updateButtonTitleOfAddShopList()
                //                self.undoAddedIngredients(vc: vc)
                
                
                self.currentVC.tabBarController?.selectedIndex = 0
                if let tabBarController = self.currentVC.tabBarController {
                    (tabBarController.viewControllers?.first as? UINavigationController)?.popToRootViewController(animated: true)
                }
                
            } else {
                
                self.currentVC.recipeDetails.is_btn_add_ing_to_shoplist_tapped = !self.currentVC.recipeDetails.is_btn_add_ing_to_shoplist_tapped
                self.btnAddIngToShop.isSelected = self.currentVC.recipeDetails.is_btn_add_ing_to_shoplist_tapped
                
                let selected_ingredient = vc.recipeDetails.ingredient_list.filter { (i) -> Bool in
                    return i.isSelected
                }
                
                var isFromCookBook = false
                if self.currentVC.isFromCookBook { isFromCookBook = true }
                if self.currentVC.recipeDetails.is_own_recipe.mk_boolValue { isFromCookBook = false }
                
                //Ingredient_list
                var arrDictIngredient = [[String:Any]]()
                for objIngredient in selected_ingredient {
                    let dict = ["ingredient_id":objIngredient.id,
                                "recipe_id":vc.recipeDetails.id,
                                "is_from_cookbook":isFromCookBook.mk_intValue,
                                "qty":objIngredient.qty] as [String : Any]
                    arrDictIngredient.append(dict)
                }
                
                guard let jsonDataArrDictIngredient = try? JSONSerialization.data(
                    withJSONObject: arrDictIngredient,
                    options: []) else {
                        return
                }
                
                guard let textArrIngredients = String(data: jsonDataArrDictIngredient, encoding: .utf8) else {
                    return
                }
                print("JSON string textArrIngredients = \(textArrIngredients)")
                
                
                if self.btnAddIngToShop.isSelected {
                    self.currentVC.tblView.reloadSections(IndexSet.init(integer: 1), with: .fade)
                } else {
                    if selected_ingredient.count == 0 {
                        self.shake()
                        return
                    }
                    self.addIngsToShopList(recipes_list_with_ingredients: textArrIngredients)
                }
                
                self.currentVC.tblView.reloadSections(IndexSet.init(integer: 0), with: .fade)
                vc.setImageToStretchyHeader()
            }
            
        }
        
    }
    
    
    func addIngsToShopList(recipes_list_with_ingredients:String) {
        
        
        self.currentVC.isIngsAddedToShopList = true
        self.updateButtonTitleOfAddShopList()
        
        addRecipeIngsToShopList(recipes_list_with_ingredients: recipes_list_with_ingredients) { (res) in
            
            
            switch res {
            case .success(dictData: _, message: let str) :
                
                BasicFunctions.showInfo(strError: str)
                
                //                for i in 0...self.currentVC.recipeDetails.ingredient_list.count-1 {
                //                    self.currentVC.recipeDetails.ingredient_list[i].isSelected = false
                //                }
                //                self.currentVC.tblView.reloadSections(IndexSet.init(integer: 1), with: .fade)
                //                self.currentVC.tblView.reloadSections(IndexSet.init(integer: 0), with: .fade)
                //                self.currentVC.setImageToStretchyHeader()
                
                self.currentVC.isIngsAddedToShopList = true
                self.updateButtonTitleOfAddShopList()
                
                break
            case .error(error: let err) :
                print(err)
                BasicFunctions.showError(strError: err)
                
                self.currentVC.isIngsAddedToShopList = false
                self.updateButtonTitleOfAddShopList()
                
                break
            case .failed(message: let msg) :
                BasicFunctions.showError(strError: msg)
                
                self.currentVC.isIngsAddedToShopList = false
                self.updateButtonTitleOfAddShopList()
                
                break
            }
            
        }
    }
    
    func undoAddedIngredients(vc:RecipeDetailsVC) {
        
        
        let selected_ingredient = vc.recipeDetails.ingredient_list.filter { (i) -> Bool in
            return i.isSelected
        }
        
        var isFromCookBook = false
        if self.currentVC.isFromCookBook { isFromCookBook = true }
        if self.currentVC.recipeDetails.is_own_recipe.mk_boolValue { isFromCookBook = false }
        
        //Ingredient_list
        //        var arrDictIngredient = [[String:Any]]()
        //        for objIngredient in selected_ingredient {
        //            let dict = ["ingredient_id":objIngredient.id,
        //                        "recipe_id":vc.recipeDetails.id,
        //                        "is_from_cookbook":isFromCookBook.mk_intValue,
        //                        "qty":objIngredient.qty] as [String : Any]
        //            arrDictIngredient.append(dict)
        //        }
        
        var arrDictIngredient = [[String:Any]]()
        for objIngredient in selected_ingredient {
            let dict = ["ingredient_id":objIngredient.id,
                        "qty":objIngredient.qty] as [String : Any]
            arrDictIngredient.append(dict)
        }
        
        guard let jsonDataArrDictIngredient = try? JSONSerialization.data(
            withJSONObject: arrDictIngredient,
            options: []) else {
                return
        }
        
        guard let textArrIngredients = String(data: jsonDataArrDictIngredient, encoding: .utf8) else {
            return
        }
        print("JSON string textArrIngredients = \(textArrIngredients)")
        
        //        var isFromCookBook = false
        //        if self.currentVC.isFromCookBook { isFromCookBook = true }
        //        if self.currentVC.recipeDetails.is_own_recipe.mk_boolValue { isFromCookBook = false }
        
        //            self.currentVC.isIngsAddedToShopList = false
        //            self.updateButtonTitleOfAddShopList()
        
        addRecipeIngsToShopListUndo(recipe_id: self.currentVC.recipeDetails.id, is_from_cookbook:isFromCookBook.mk_intValue, ingredient_ids: textArrIngredients) { (res) in
            
            
            switch res {
            case .success(dictData: _, message: let str) :
                
                BasicFunctions.showInfo(strError: str)
                
                for i in 0...self.currentVC.recipeDetails.ingredient_list.count-1 {
                    self.currentVC.recipeDetails.ingredient_list[i].isSelected = false
                }
                self.currentVC.tblView.reloadSections(IndexSet.init(integer: 1), with: .fade)
                self.currentVC.tblView.reloadSections(IndexSet.init(integer: 0), with: .fade)
                self.currentVC.setImageToStretchyHeader()
                
                self.currentVC.isIngsAddedToShopList = false
                self.updateButtonTitleOfAddShopList()
                
                break
            case .error(error: let err) :
                print(err)
                BasicFunctions.showError(strError: err)
                
                self.currentVC.isIngsAddedToShopList = false
                self.updateButtonTitleOfAddShopList()
                
                break
            case .failed(message: let msg) :
                BasicFunctions.showError(strError: msg)
                
                self.currentVC.isIngsAddedToShopList = false
                self.updateButtonTitleOfAddShopList()
                
                break
            }
            
        }
    }
    
    
    func updateButtonTitleOfAddShopList() {
        if self.currentVC.isIngsAddedToShopList {
            //            self.btnAddIngToShop.setTitle("Added to shopping list".uppercased().localize(), for: .normal)
            self.btnAddIngToShop.setTitle("Go to shopping list".uppercased().localize(), for: .normal)
        } else {
            self.btnAddIngToShop.setTitle("ADD INGREDIENTS TO SHOPPING LIST".localize(), for: .normal)
        }
    }
    
    
    func handleCellDirectionList(vc:RecipeDetailsVC, indexPath:IndexPath) {
        self.currentVC = vc
        let objDirection = self.currentVC.recipeDetails.directions_list[indexPath.row]
        self.lblIndexNumber.text = "\(objDirection.step). "
        let desc = (objDirection.description).replacingOccurrences(of: "\n", with: "<br>")
        self.txtViewDirection.mk_setHTMLFromString2(htmlText: desc)
        if indexPath.row % 2 == 0 {
            self.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        } else {
            self.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.8941176471, blue: 0.8784313725, alpha: 0.36)
        }
    }
    
}


extension FloatingPoint {
    var whole: Self { modf(self).0 }
    var fraction: Self { modf(self).1 }
}

extension Double {
    var whole: Self { modf(self).0 }
    var fraction: Self { modf(self).1 }
}



func saveRecipeDirectInCookbook(vc:RecipeDetailsVC, recipe_id:Int64) {
    
    let objCookbookCollectionNameList = apde.getController(vc_name: "CookbookCollectionNameList", sb: .CookBookTab) as! CookbookCollectionNameList
    objCookbookCollectionNameList.compBlock = { (objCollection,isCancelTapped) in
        
        if isCancelTapped {
            return
        }
        
        if let objCollection = objCollection {
            // Selected Collection
            print(objCollection.id)
            
            apde.showLoader()
            recipeAddToCollection(recipe_id: vc.recipeDetails.id, collection_id: objCollection.id) { (res) in
                
                switch res {
                case .success(_, let message) :
                    BasicFunctions.showInfo(strError: message)
                    
                    vc.recipeDeletedOrUpdatedBlock?(vc.recipeDetails,true)
                    vc.getRecipeDetailsApiCall()
                    
                    break
                case .failed(message: let msg):
                    BasicFunctions.showError(strError: msg)
                    break
                case .error(error: let error):
                    BasicFunctions.showError(strError: error)
                    break
                }
                
                apde.hideLoader()
                
            }
        } else {
            // Selected Add New Collection
            let objAddCookBookCollectionVC = apde.getController(vc_name: "AddCookBookCollectionVC", sb: .CookBookTab) as! AddCookBookCollectionVC
            objAddCookBookCollectionVC.shouldDisplaySuccessPopup = false
            objAddCookBookCollectionVC.compBlock = { (boolValue) in
                // if Collection Created
                if boolValue {
                    saveRecipeDirectInCookbook(vc: vc, recipe_id: recipe_id)
                }
            }
            vc.navigationController?.pushViewController(objAddCookBookCollectionVC, animated: true)
        }
        
    }
    vc.tabBarController?.present(objCookbookCollectionNameList, animated: true, completion: nil)
}


//MARK:- Share edmam recipe
extension CellRecipeDetails {
    
    func saveEdmamRecipe(vc:RecipeDetailsVC) {
        
        let myURLString = vc.recipeDetails.web_url
        //        let objImportRecipePopupVC = apde.getController(vc_name: "ImportRecipePopupVC", sb: .AddRecipe) as! ImportRecipePopupVC
        
        self.compBlockWithUrl = { addNewRecipeData in
            
            var mutAddNewRecipeData = addNewRecipeData
            
            if vc.recipeDetails.serves > 0 {
                mutAddNewRecipeData.serves = vc.recipeDetails.serves
            }
            
            let objAddNewRecipeVC = makeReadyAddNewRecipeVcFromImport(addNewRecipeData: mutAddNewRecipeData, arrImportedKeywords:self.arrImportedKeywords, arrImportedCuisineTags:self.arrImportedCuisineTags, arrImportedMealTypeTags:self.arrImportedMealTypeTags)
            objAddNewRecipeVC.isEdmamRecipe = true
            objAddNewRecipeVC.edmamRecipeAdded = { (recipe_id) in
                vc.recipe_id = recipe_id
                vc.isThird = false
                vc.getRecipeDetailsApiCall()
                updateMyRecipesVC(vcSelf: vc)
            }
            vc.navigationController?.pushViewController(objAddNewRecipeVC, animated: true)
        }
        
        self.compBlockError = {
            RecipeParser.getRecipeData(url: myURLString) { (objAddNewRecipeData) in
                
                //                objImportRecipePopupVC.dismiss(animated: true) {
                if let objAddNewRecipeData = objAddNewRecipeData {
                    
                    let objAddNewRecipeVC = makeReadyAddNewRecipeVcFromImport(addNewRecipeData: objAddNewRecipeData, arrImportedKeywords: [Keyword](), arrImportedCuisineTags: [Category_tag](), arrImportedMealTypeTags: [Category_tag]())
                    objAddNewRecipeVC.isEdmamRecipe = true
                    objAddNewRecipeVC.edmamRecipeAdded = { (recipe_id) in
                        vc.recipe_id = recipe_id
                        vc.isThird = false
                        vc.getRecipeDetailsApiCall()
                        updateMyRecipesVC(vcSelf: vc)
                    }
                    //                        vc.navigationController?.pushViewController(objAddNewRecipeVC, animated: true)
                    
                } else {
                    print("Error")
                    BasicFunctions.showError(strError: strErrorForRecipeNotFound)
                }
                //                }
            }
        }
        
        
        self.importDataUrl(url: URL.init(string: myURLString)!)
        
        //        vc.tabBarController?.present(objImportRecipePopupVC, animated: true, completion: nil)
        //        Timer.after(0.6) {
        //            objImportRecipePopupVC.txtUrl.text = myURLString
        //                    self.btnImportTapped(btn: objImportRecipePopupVC.btnImport)
        //        }
        
    }
    
    func importDataUrl(url:URL) {
        
        let request = URLRequest(url: url)
        // Set headers
        //            request.allHTTPHeaderFields = ["Content-Type" : "application/json"]
        
        let completionHandler = {(data: Data?, response: URLResponse?, error: Error?) -> Void in
            // Do something
            
            if let err = error {
                print(err)
                DispatchQueue.main.sync {
                    BasicFunctions.showError(strError: "Something went wrong with the server.".localize())
                    return
                }
            }
            
            guard let data = data else { return }
            
            DispatchQueue.main.sync {
                
                do {
                    if let string = String(data: data, encoding: .utf8) {
                        self.doc = try parseBodyFragment(string)
                        self.handleRespone()
                    } else {
                        BasicFunctions.showError(strError: strErrorForRecipeNotFound)
                    }
                } catch {
                    print("error : \(error)")
                    BasicFunctions.showError(strError: strErrorForRecipeNotFound)
                }
            }
        }
        
        URLSession.shared.dataTask(with: request, completionHandler: completionHandler).resume()
    }
    
    func handleRespone() {
        
        do {
            
            var isFoundJson = false
            let elements = try doc.getAllElements()
            for element in elements {
                switch element.tagName() {
                case "div" :
                    break
                case "script" :
                    
                    let foundJsonType = try element.attr("type")
                    if foundJsonType == "application/ld+json" {
                        
                        let rawJsonString = try element.html()
                        let dictData = convertToDictionary(text: rawJsonString)
                        if let dictData = dictData {
                            if let _ = getActualRecipeDict(dict: dictData) {
                                isFoundJson = true
                                _ = self.parseRecipeDict(jsonDict: dictData)
                            }
                        }
                        
                        let arrayData = convertToArray(text: rawJsonString)
                        if let finalArray = arrayData {
                            for dictSub in finalArray {
                                if let _ = getActualRecipeDict(dict: dictSub) {
                                    isFoundJson = true
                                    _ = self.parseRecipeDict(jsonDict: dictSub)
                                }
                            }
                        }
                    }
                    
                default:
                    break
                    
                }
            }
            
            if isFoundJson == false {
                
                self.html_handleRecipeImage()
                self.html_handleRecipeRatings()
                
                self.html_recipeYield()
                self.html_handleTime()
                let strRecipeName       = self.html_handleRecipeName()
                let arrDirections       = self.html_handleDirections()
                let arrIngredients      = self.html_handleIngredients()
                
                if arrDirections.count > 0 && arrIngredients.count > 0 && strRecipeName != nil {
                    
                    self.addNewRecipeData.title = strRecipeName ?? ""
                    
                    
                    for (i,d) in arrDirections.enumerated() {
                        let direction_list = Direction_list.init(step: i+1, description: d)
                        self.addNewRecipeData.direction_list.append(direction_list)
                    }
                    let dict = ["ingredient":arrIngredients]
                    //                    self.getRecipeIngredientsData_edamam(dictAllData: dict) { (formattedResponseDict) in
                    //                        self.parseResponseOfEdamam(formattedResponseDict: formattedResponseDict)
                    //                    }
                    self.parseIngredientsApiCall(dictAllData: dict)
                    
                } else {
                    
                    if compBlockError != nil {
                        compBlockError()
                    } else {
                        hapError()
                        //                            self.txtUrl.shake()
                        //                            self.hideActivityIndicator()
                        BasicFunctions.showError(strError: strErrorForRecipeNotFound)
                    }
                    
                }
            }
        } catch {
            print(error)
            DispatchQueue.main.sync {
                
                hapError()
                //                    self.txtUrl.shake()
                //                    self.hideActivityIndicator()
                BasicFunctions.showError(strError: strErrorForRecipeNotFound)
            }
        }
        
        
    }
    
    func parseRecipeDict(jsonDict:[String:Any]) -> Bool {
        
        if let finalRecipeDict = self.getActualRecipeDict(dict: jsonDict) {
            
            self.getAllData(jsonDict: finalRecipeDict)
            //            self.getRecipeIngredientsData_edamam(dictAllData: finalRecipeDict) { (formattedResponseDict) in
            //                self.parseResponseOfEdamam(formattedResponseDict: formattedResponseDict)
            //            }
            self.parseIngredientsApiCall(dictAllData: finalRecipeDict)
            return true
        }
        return false
    }
    
    func getFinalRecipeJsonDict(jsonDict:[String:Any]) -> [String:Any]? {
        
        let isFoundDict = self.isDictionaryOfRecipe(jsonDict: jsonDict)
        if isFoundDict {
            return jsonDict
        }
        
        for key1 in jsonDict.keys {
            
            //Has Array
            if let arrDict = jsonDict[key1] as? [[String:Any]] {
                for childDict in arrDict {
                    
                    let isFoundDict = self.isDictionaryOfRecipe(jsonDict: childDict)
                    if isFoundDict {
                        return childDict
                    }
                    
                    for key2 in childDict.keys {
                        if let dict = childDict[key2] as? [String:Any] {
                            let isFoundDict = self.isDictionaryOfRecipe(jsonDict: dict)
                            if isFoundDict {
                                return dict
                            }
                        }
                    }
                    
                    
                }
            }
            
            //Has Dict
            if let dict = jsonDict[key1] as? [String:Any] {
                let isFoundDict = self.isDictionaryOfRecipe(jsonDict: dict)
                if isFoundDict {
                    return dict
                }
            }
        }
        return nil
    }
    
    
    
    
    
    func getActualRecipeDict(dict:[String:Any]) -> [String:Any]?  {
        
        if let jsonDict = getFinalRecipeJsonDict(jsonDict: dict) {
            
            let isFoundDictTopLevel = self.isDictionaryOfRecipe(jsonDict: jsonDict)
            
            if isFoundDictTopLevel {
                //Top Level
                print("isFoundDictTopLevel\(isFoundDictTopLevel)")
                return jsonDict
                
            } else {
                
                // Search at child level
                for key in jsonDict.keys {
                    
                    //Found Dictionary
                    if let dictSub = jsonDict[key] as? [String:Any] {
                        
                        let isFoundDictChildLevel1 = self.isDictionaryOfRecipe(jsonDict: dictSub)
                        print("isFoundDictChildLevel1\(isFoundDictChildLevel1)")
                        if isFoundDictChildLevel1 {
                            return dictSub
                        }
                    }
                    
                    //Found Array
                    if let arrayDict = jsonDict[key] as? [[String:Any]] {
                        for dictSub in arrayDict {
                            let isFoundDictChildLevel1 = self.isDictionaryOfRecipe(jsonDict: dictSub)
                            if isFoundDictChildLevel1 {
                                print("isFoundDictChildLevel1\(isFoundDictChildLevel1)")
                                return dictSub
                            }
                        }
                    }
                    
                    
                    
                }
            }
            
        }
        
        return nil
    }
    
    func html_handleRecipeImage() {
        do {
            let srcs: Elements = try doc.select("img[src]")
            for src in srcs {
                print(src)
                if let foundImage = try src.getElementsByAttributeValue("itemprop", "image").array().first {
                    let linkImageSrc: String = try foundImage.attr("src");
                    print(linkImageSrc)
                    if let urlImage = URL.init(string: linkImageSrc) {
                        if let _ = try? Data.init(contentsOf: urlImage) {
                            self.saveRecipeImageDataToLocal(url: urlImage)
                        } else {
                            let linkImageSrcData: String = try foundImage.attr("src-data");
                            if let urlImage = URL.init(string: linkImageSrcData) {
                                if let _ = try? Data.init(contentsOf: urlImage) {
                                    self.saveRecipeImageDataToLocal(url: urlImage)
                                } else {
                                    if linkImageSrc.hasPrefix("\\") {
                                        if let url = URL.init(string: "http:\(linkImageSrc)") {
                                            if let _ = try? Data.init(contentsOf: url) {
                                                self.saveRecipeImageDataToLocal(url: url)
                                            }
                                        }
                                    }
                                }
                            } else {
                                if linkImageSrc.hasPrefix("//") {
                                    if let url = URL.init(string: "http:\(linkImageSrc)") {
                                        if let _ = try? Data.init(contentsOf: url) {
                                            self.saveRecipeImageDataToLocal(url: url)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch {
            print(error)
        }
    }
    
    func html_handleRecipeRatings() {
        //        aggregateRating
        
        if let ratingValue = try? self.doc.getElementsByAttributeValue("itemprop", "ratingValue").text() {
            self.addNewRecipeData.aggregateRatingValue = ratingValue
        }
        if let reviewCount = try? self.doc.getElementsByAttributeValue("itemprop", "reviewCount").text() {
            self.addNewRecipeData.aggregateRatingCount = reviewCount
        } else {
            if let ratingCount = try? self.doc.getElementsByAttributeValue("itemprop", "ratingCount").text() {
                self.addNewRecipeData.aggregateRatingCount = ratingCount
            }
        }
        
        print("------------------------------------------")
        print("RatingValue : \(self.addNewRecipeData.aggregateRatingValue)")
        print("ReviewCount : \(self.addNewRecipeData.aggregateRatingCount)")
        print("------------------------------------------")
        
    }
    
    
    
    func html_recipeYield() {
        do {
            if let yieldFound = try self.doc.getElementsByAttributeValue("itemprop", "recipeYield").array().first {
                self.handleYields(strServes: try yieldFound.text())
            }
        } catch {
            print(error)
        }
    }
    
    
    func html_handleTimeAndHours(itempropname:String? = nil) -> (String?,String?)? {
        do {
            if let itempropname = itempropname {
                
                let arrTime = try self.doc.getElementsByAttributeValue("itemprop", itempropname)
                
                if let arrAttributes = arrTime.array().first?.getAttributes()?.asList() {
                    for attr in arrAttributes {
                        if attr.getValue().contains("PT") {
                            let dtc = attr.getValue().getHoursMinutes()
                            return dtc
                        } else {
                            // Starting with P // P0Y0M0DT0H5M0.000S
                            
                            if attr.getValue().hasPrefix("P") && attr.getValue().contains(".") {
                                let arrTime = attr.getValue().components(separatedBy: ".")
                                if let timeValue = arrTime.first {
                                    let dtc = timeValue.getHoursMinutes()
                                    return dtc
                                }
                            }
                        }
                    }
                }
            }
            
        } catch {
            print(error)
        }
        return nil
    }
    
    
    
    func html_handleTime(itempropname:String? = nil) {
        
        var cookTime : (String?,String?)!
        var prepTime : (String?,String?)!
        var totalTime : (String?,String?)!
        
        cookTime = html_handleTimeAndHours(itempropname: "cookTime")
        prepTime = html_handleTimeAndHours(itempropname: "prepTime")
        totalTime = html_handleTimeAndHours(itempropname: "totalTime")
        
        
        if totalTime != nil {
            
            if let totalHours = totalTime.0?.integer {
                self.addNewRecipeData.ready_time_hours = totalHours
            }
            if let totalMins = totalTime.1?.integer {
                self.addNewRecipeData.ready_time_mins = totalMins
            }
            
        } else {
            
            if cookTime != nil && prepTime != nil {
                
                //cookTime+prepTime final
                if let cook_totalHours = cookTime.0?.integer {
                    if let prep_totalHours = prepTime.0?.integer {
                        self.addNewRecipeData.ready_time_hours = cook_totalHours+prep_totalHours
                    }
                }
                if let cook_totalMins = cookTime.1?.integer {
                    if let prep_totalMins = prepTime.1?.integer {
                        self.addNewRecipeData.ready_time_mins = cook_totalMins+prep_totalMins
                    }
                }
                
            } else if cookTime != nil && prepTime == nil {
                
                //cookTime final
                if let totalHours = cookTime.0?.integer {
                    self.addNewRecipeData.ready_time_hours = totalHours
                }
                if let totalMins = cookTime.1?.integer {
                    self.addNewRecipeData.ready_time_mins = totalMins
                }
            } else {
                
                
                if prepTime != nil {
                    
                    //prepTime final
                    if let totalHours = prepTime.0?.integer {
                        self.addNewRecipeData.ready_time_hours = totalHours
                    }
                    if let totalMins = prepTime.1?.integer {
                        self.addNewRecipeData.ready_time_mins = totalMins
                    }
                }
            }
        }
        
    }
    
    func html_handleRecipeName() -> String? {
        do {
            if let nameFound = try self.doc.getElementsByAttributeValue("itemprop", "name").array().first {
                return try nameFound.text()
            } else {
                return try self.doc.title()
            }
        } catch {
            print(error)
        }
        return nil
    }
    
    func html_handleDirections() -> [String] {
        
        var arrDirections = [String]()
        do {
            
            let arrRecipeInstructions = try self.doc.getElementsByAttributeValue("itemprop", "recipeInstructions").array()
            
            if arrRecipeInstructions.first?.tag().getName() == "ol" {
                if let items = arrRecipeInstructions.first?.children().array() {
                    for element in items {
                        arrDirections.append(try element.text())
                    }
                    return arrDirections
                }
            }
            
            if let childsLISTlitags = arrRecipeInstructions.first?.children().first()?.children() {
                for o in childsLISTlitags {
                    arrDirections.append(try o.text())
                }
                if arrDirections.count > 0 {
                    return arrDirections
                }
            }
            
            
            
            if arrRecipeInstructions.count > 1 {
                for (_,child) in arrRecipeInstructions.enumerated() {
                    arrDirections.append(try child.text())
                }
            } else {
                
                
                if let arrParentRecipeInstructions = try self.doc.getElementsByAttributeValue("itemprop", "recipeInstructions").array().first {
                    if arrParentRecipeInstructions.children().count > 1 {
                        
                        for (_,child) in arrParentRecipeInstructions.children().array().enumerated() {
                            if (try? child.html()) != "&nbsp;" {
                                arrDirections.append(try child.text())
                            }
                        }
                    } else {
                        let htmlStringDescription = try arrParentRecipeInstructions.text()
                        arrDirections.append(htmlStringDescription)
                    }
                    
                    if arrDirections.count == 0 {
                        if let nextElement = try? self.doc.getElementsByAttributeValue("itemprop", "recipeInstructions").array().first?.parent()?.nextElementSibling() {
                            if nextElement.children().count > 1 {
                                for (_,child) in nextElement.children().enumerated() {
                                    arrDirections.append(try child.text())
                                }
                            }
                        }
                    }
                }
            }
            
        } catch {
            print(error)
        }
        
        return arrDirections
    }
    
    
    func html_handleIngredients() -> [String] {
        var arrIngredients = [String]()
        do {
            
            let arrRecipeIngredientsFound = try self.doc.getElementsByAttributeValue("itemprop", "recipeIngredient").array()
            
            for (_,ingredient) in arrRecipeIngredientsFound.enumerated() {
                arrIngredients.append(try ingredient.text())
            }
            
            if arrIngredients.count == 0 {
                
                let arrAllItemProps = try self.doc.getElementsByAttribute("itemprop").array()
                for itemProp in arrAllItemProps {
                    let strItemPropValue: String = try itemProp.attr("itemprop");
                    if strItemPropValue.contains("ingredient") {
                        arrIngredients.append(try itemProp.text())
                    }
                }
            }
        } catch {
            print(error)
        }
        return arrIngredients
    }
    
    func parseIngredientsApiCall(dictAllData:[String:Any]) {
        
        let arrStringAllKeys = dictAllData.keys
        var arrIngredients = [String]()
        
        for strKey in arrStringAllKeys {
            
            let strKeyLowercase = strKey.lowercased()
            
            if strKeyLowercase.contains("ingredient") {
                if let dictIngredients = dictAllData[strKey] as? [String:Any] {
                    print(dictIngredients)
                }
                if let arrStringIngredients = dictAllData[strKey] as? [String] {
                    arrIngredients = arrStringIngredients
                    print(arrStringIngredients)
                }
                if let strIngredients = dictAllData[strKey] as? String {
                    print(strIngredients)
                }
            }
            
        }
        
        var mutArrIngredients = [String]()
        for ing in arrIngredients {
            let ingRemovedSymbols = self.replaceHtmlSymbols(strText: ing)
            let parsedIng = try? SwiftSoup.parse(ingRemovedSymbols)
            let finalIng = try? parsedIng?.text() ?? ""
            mutArrIngredients.append(finalIng ?? "")
        }
        
        //Ingredient_list
        
        guard let jsonDataArrIngredients = try? JSONSerialization.data(withJSONObject: mutArrIngredients,options: []) else {
            self.checkVideoAndDownload()
            return
        }
        
        guard let textArrIngredienta = String(data: jsonDataArrIngredients, encoding: .utf8) else {
            self.checkVideoAndDownload()
            return
        }
        
        print("JSON string textArrIngredienta = \(textArrIngredienta)")
        
        getIngredientsParsing(ingredients: textArrIngredienta) { (res) in
            switch res {
            case .success(dictData: let dictData, message: _) :
                
                
                print(dictData)
                if let dictIngredientList = dictData["ingredient_list"] as? [[String:Any]] {
                    var a = [Ingredient_list]()
                    for dictIngredient in dictIngredientList {
                        
                        let name            = dictIngredient["ingredient"] as? String ?? ""
                        let unit_name       = dictIngredient["unit"] as? String ?? ""
                        let qty             = dictIngredient["quantity"] as? String ?? ""
                        
                        let objIngredient = Ingredient_list.init(id: 0, unit_name: unit_name, name: name, note: "", qty: qty, isSelected: false)
                        a.append(objIngredient)
                    }
                    self.addNewRecipeData.ingredient_list = a
                    self.checkVideoAndDownload()
                } else {
                    self.checkVideoAndDownload()
                    BasicFunctions.showError(strError: "Something went wrong!".localize())
                }

                break
            case .failed(message: let msg):
                self.checkVideoAndDownload()
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                self.checkVideoAndDownload()
                BasicFunctions.showError(strError: error)
                break
            }
        }
        
        
    }
    
    func getAllData(jsonDict:[String:Any]) {
        print(jsonDict)
                
        //image
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("image") {
                
                if let arrStringUrlImages = jsonDict[key] as? [String] {
                    for strUrlImage in arrStringUrlImages {
                        if let url = URL.init(string: strUrlImage) {
                            saveRecipeImageDataToLocal(url:url)
                            break
                        }
                    }
                }
                
                if let stringUrlImages = jsonDict[key] as? String {
                    
                    if let url = URL.init(string: stringUrlImages) {
                        saveRecipeImageDataToLocal(url:url)
                        break
                    }
                }
                
                
                if let dictUrlImages = jsonDict[key] as? [String:Any] {
                    
                    let keyFilered = dictUrlImages.keys.filter { (key) -> Bool in
                        return key.contains("url")
                    }
                    
                    if keyFilered.count > 0 {
                        if let strUrl = dictUrlImages[keyFilered[0]] as? String {
                            if let url = URL.init(string: strUrl) {
                                saveRecipeImageDataToLocal(url:url)
                                break
                            }
                        }
                    }
                }
                
                break
            }
            if lowercasedKey.contains("img") {
                
                break
            }
        }
        
        
        //Category
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("category") {
                if let arrCourse = (jsonDict[key] as? [String]) {
                }
            }
        }
        //Cuisine
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            if lowercasedKey.contains("cuisine") {
                if let arrCuisine = (jsonDict[key] as? [String]) {
                }
            }
        }
        
        
        
        //Name/Title
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("name") {
                let htmlStringDescription = (jsonDict[key] as? String) ?? ""
                self.addNewRecipeData.title = htmlStringDescription
                break
            }
            if lowercasedKey.contains("title") {
                let htmlStringDescription = (jsonDict[key] as? String) ?? ""
                self.addNewRecipeData.title = htmlStringDescription
                break
            }
        }
        
        //Description
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("description") {
                
                let htmlStringDescription = (jsonDict[key] as? String) ?? ""
                break
            }
            if lowercasedKey.contains("desc") {
                
                let htmlStringDescription = (jsonDict[key] as? String) ?? ""
                break
            }
            if lowercasedKey.contains("descriptions") {
                let htmlStringDescription = (jsonDict[key] as? String) ?? ""
                break
            }
        }
        
        
        
        //Video
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("video") {
                if let arrVideo = (jsonDict[key] as? [[String:Any]]) {
                    if let f = arrVideo.first {
                        if let contentUrl = f["contentUrl"] as? String {
                            print(contentUrl)
                            self.addNewRecipeData.imported_media_url    = contentUrl
                            self.addNewRecipeData.media_url             = contentUrl
                            self.addNewRecipeData.is_video              = true.mk_intValue
                        }
                        if let thumbnailUrl = f["thumbnailUrl"] as? String {
                            print(thumbnailUrl)
                            self.addNewRecipeData.imported_video_thumb_url      = thumbnailUrl
                            self.addNewRecipeData.video_thumb_url               = thumbnailUrl
                        } else {
                            if let contentUrl = f["contentUrl"] as? String {
                                print(contentUrl)
                                if let url = URL.init(string: contentUrl) {
                                    let thumbnailImage = Utilities.generateThumbnail(path: url)
                                    if let filePath = thumbnailImage.mk_saveToDocuments(filename: String(Date().mk_ticks), folderName: tmpDocFolderName) {
                                        self.addNewRecipeData.imported_video_thumb_url      = filePath
                                        self.addNewRecipeData.video_thumb_url               = filePath
                                    }
                                }
                            }
                        }
                    }
                }
                
                
                if let vidObject = (jsonDict[key] as? [String:Any]) {
                    
                    if let contentUrl = vidObject["contentUrl"] as? String {
                        print(contentUrl)
                        if contentUrl != "" {
                            self.addNewRecipeData.imported_media_url    = contentUrl
                            self.addNewRecipeData.media_url             = contentUrl
                            self.addNewRecipeData.is_video              = true.mk_intValue
                        }
                    }
                    if let embedUrl = vidObject["embedUrl"] as? String {
                        print(embedUrl)
                        if embedUrl != "" {
                            self.addNewRecipeData.imported_media_url    = embedUrl
                            self.addNewRecipeData.media_url             = embedUrl
                            self.addNewRecipeData.is_video              = true.mk_intValue
                        }
                    }
                    
                    if let thumbnailUrl = vidObject["thumbnailUrl"] as? String {
                        print(thumbnailUrl)
                        
                        if thumbnailUrl != "" {
                            self.addNewRecipeData.imported_video_thumb_url      = thumbnailUrl
                            self.addNewRecipeData.video_thumb_url               = thumbnailUrl
                        }
                        
                    } else {
                        if let contentUrl = vidObject["contentUrl"] as? String {
                            print(contentUrl)
                            if let url = URL.init(string: contentUrl) {
                                let thumbnailImage = Utilities.generateThumbnail(path: url)
                                if let filePath = thumbnailImage.mk_saveToDocuments(filename: String(Date().mk_ticks), folderName: tmpDocFolderName) {
                                    self.addNewRecipeData.imported_video_thumb_url      = filePath
                                    self.addNewRecipeData.video_thumb_url               = filePath
                                }
                            }
                        }
                    }
                    
                }
                
            }
        }

        var cookTime : (String?,String?)!
        var prepTime : (String?,String?)!
        var totalTime : (String?,String?)!
        
        
        //Cooktime
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("cooktime") {
                
                cookTime = self.getHoursAndMins(strDate: ((jsonDict[key] as? String) ?? ""))
                
                break
            }
            if lowercasedKey.contains("cook time") {
                
                cookTime = self.getHoursAndMins(strDate: ((jsonDict[key] as? String) ?? ""))
                
                break
            }
            if lowercasedKey.contains("cktime") {
                
                cookTime = self.getHoursAndMins(strDate: ((jsonDict[key] as? String) ?? ""))
                
                break
            }
            
        }
        
        
        //Preptime
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("preptime") {
                
                prepTime = self.getHoursAndMins(strDate: ((jsonDict[key] as? String) ?? ""))
                
                break
            }
            if lowercasedKey.contains("preparationtime") {
                
                prepTime = self.getHoursAndMins(strDate: ((jsonDict[key] as? String) ?? ""))
                
                break
            }
            if lowercasedKey.contains("prep time") {
                
                prepTime = self.getHoursAndMins(strDate: ((jsonDict[key] as? String) ?? ""))
                
                break
            }
            
        }
        
        //totaltime
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("totaltime") {
                
                totalTime = self.getHoursAndMins(strDate: ((jsonDict[key] as? String) ?? ""))
                
                break
            }
            
        }
        
        
        if totalTime != nil {
            
            if let totalHours = totalTime.0?.integer {
                self.addNewRecipeData.ready_time_hours = totalHours
            }
            if let totalMins = totalTime.1?.integer {
                self.addNewRecipeData.ready_time_mins = totalMins
            }
            
        } else {
            
            if cookTime != nil && prepTime != nil {
                
                //cookTime+prepTime final
                if let cook_totalHours = cookTime.0?.integer {
                    if let prep_totalHours = prepTime.0?.integer {
                        self.addNewRecipeData.ready_time_hours = cook_totalHours+prep_totalHours
                    }
                }
                if let cook_totalMins = cookTime.1?.integer {
                    if let prep_totalMins = prepTime.1?.integer {
                        self.addNewRecipeData.ready_time_mins = cook_totalMins+prep_totalMins
                    }
                }
                
            } else if cookTime != nil && prepTime == nil {
                
                //cookTime final
                if let totalHours = cookTime.0?.integer {
                    self.addNewRecipeData.ready_time_hours = totalHours
                }
                if let totalMins = cookTime.1?.integer {
                    self.addNewRecipeData.ready_time_mins = totalMins
                }
            } else {
                
                
                if prepTime != nil {
                    
                    //prepTime final
                    if let totalHours = prepTime.0?.integer {
                        self.addNewRecipeData.ready_time_hours = totalHours
                    }
                    if let totalMins = prepTime.1?.integer {
                        self.addNewRecipeData.ready_time_mins = totalMins
                    }
                    
                }
                
                
            }
        }
        
        
        //serves
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            if lowercasedKey.contains("yield") {
                let strServes = (jsonDict[key] as? String) ?? ""
                self.handleYields(strServes: strServes)
            }
            
        }
        
        var arrDirections = [String]()
        
        //recipeInstructions
        
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("instruction") {
                
                arrDirections = [(jsonDict[key] as? String) ?? ""]
                if let arr = handleDirectionsOfRecipe(keyOfDirection: key, jsonDict: jsonDict) {
                    arrDirections = arr
                }
                if let arrStrings = (jsonDict[key] as? [String]) {
                    arrDirections = arrStrings
                }
                if let str = (jsonDict[key] as? String) {
                    arrDirections = [str]
                }
                
                break
            }
            
            if lowercasedKey.contains("direction") {
                if let arr = handleDirectionsOfRecipe(keyOfDirection: key, jsonDict: jsonDict) {
                    arrDirections = arr
                }
                break
            }
        }
        
        
        var arrDirection_list = [Direction_list]()
        for (i,strDirection) in arrDirections.enumerated() {
            let objDirection_list = Direction_list.init(step: i+1, description: strDirection)
            arrDirection_list.append(objDirection_list)
        }
        self.addNewRecipeData.direction_list = arrDirection_list
        
        
        print(self.addNewRecipeData.direction_list)
        
        // keywords
        
        var arrKeywords = [Keyword]()
        
        for key in jsonDict.keys {
            let lowercasedKey = key.lowercased()
            if lowercasedKey.contains("keywords") {
                if let str = (jsonDict[key] as? String) {
                    for stringKeyword in str.components(separatedBy: ",") {
                        if let objKeyword = Keyword.mr_createEntity(in: app_default_context) {
                            print(stringKeyword)
                            print(stringKeyword.htmlDecoded)
                            objKeyword.desc = stringKeyword.htmlDecoded
                            arrKeywords.insert(objKeyword, at: 0)
                        }
                    }
                }
                break
            }
        }
        
        self.arrImportedKeywords = arrKeywords
        
        
        // Ratings and rating's count
        
        var dictAggregateRating = [String:Any]()
        
        for key in jsonDict.keys {
            let lowercasedKey = key.lowercased()
            if lowercasedKey.contains("aggregaterating") {
                if let dict = (jsonDict[key] as? [String:Any]) {
                    dictAggregateRating = dict
                }
                break
            }
        }
        
        
        print("aggregaterating \(dictAggregateRating)")
        
        if let ratingValue = dictAggregateRating["ratingValue"] as? String {
            self.addNewRecipeData.aggregateRatingValue = ratingValue
        }
        
        if let ratingValue = dictAggregateRating["ratingValue"] as? Double {
            self.addNewRecipeData.aggregateRatingValue = "\(ratingValue)"
        }
        
        
        if let reviewCount = dictAggregateRating["reviewCount"] as? String {
            self.addNewRecipeData.aggregateRatingCount = reviewCount
        } else {
            if let ratingCount = dictAggregateRating["ratingCount"] as? String {
                self.addNewRecipeData.aggregateRatingCount = ratingCount
            }
        }
        
        if let reviewCount = dictAggregateRating["reviewCount"] as? Int {
            self.addNewRecipeData.aggregateRatingCount = "\(reviewCount)"
        } else {
            if let ratingCount = dictAggregateRating["ratingCount"] as? Int {
                self.addNewRecipeData.aggregateRatingCount = "\(ratingCount)"
            }
        }
        
        print("------------------------------------------")
        print("RatingValue : \(self.addNewRecipeData.aggregateRatingValue)")
        print("ReviewCount : \(self.addNewRecipeData.aggregateRatingCount)")
        print("------------------------------------------")
        
        
        
        // Review's comments
        
        
        var arrDictReviews = [[String:Any]]()
        
        for key in jsonDict.keys {
            let lowercasedKey = key.lowercased()
            if lowercasedKey.contains("review") {
                if let dict = (jsonDict[key] as? [String:Any]) {
                    arrDictReviews.append(dict)
                }
                if let arrDict = (jsonDict[key] as? [[String:Any]]) {
                    arrDictReviews.append(contentsOf: arrDict)
                }
                break
            }
        }
        
        print(arrDictReviews)
        
        var arrReviewModel = [ReviewModel]()
        
        for dictReview in arrDictReviews {
            var reviewModel = ReviewModel()
            for (key,_) in dictReview {
                if let arrObjects = dictReview[key] as? [[String:Any]] {
                    for dictSub in arrObjects {
                        reviewModel = fillDataInReviewModel(reviewModelSent: reviewModel, dictReview: dictSub)
                    }
                }
                if let dictObject = dictReview[key] as? [String:Any] {
                    reviewModel = fillDataInReviewModel(reviewModelSent: reviewModel, dictReview: dictObject)
                }
                
            }
            reviewModel = fillDataInReviewModel(reviewModelSent: reviewModel, dictReview: dictReview)
            
            if let datePublishedString = reviewModel.datePublishedString {
                if let date = convertDateFormat(inputDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", inputDate: datePublishedString) {
                    reviewModel.datePublished = date
                }
                if let date = convertDateFormat(inputDateFormat: "yyyy-MM-dd", inputDate: datePublishedString) {
                    reviewModel.datePublished = date
                }
            }
            
            
            arrReviewModel.append(reviewModel)
        }
        
        print(arrReviewModel)
        
        
        // recipeCuisine
        
        var arrCuisineTags = [Category_tag]()
        
        for key in jsonDict.keys {
            let lowercasedKey = key.lowercased()
            if lowercasedKey.contains("cuisine") {
                if let str = (jsonDict[key] as? String) {
                    if str.contains(",") {
                        for stringCuisine in str.components(separatedBy: ",") {
                            for tag in enum_tag_list_type.cusineType.getAllTags() {
                                if let tag_name = tag.tag_name {
                                    if stringCuisine.lowercased().contains(tag_name.lowercased()) {
                                        arrCuisineTags.append(tag)
                                    } else {
                                        if tag_name.lowercased().contains(stringCuisine.lowercased()) {
                                            arrCuisineTags.append(tag)
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        for tag in enum_tag_list_type.cusineType.getAllTags() {
                            if let tag_name = tag.tag_name {
                                if str.lowercased().contains(tag_name.lowercased()) {
                                    arrCuisineTags.append(tag)
                                } else {
                                    if tag_name.lowercased().contains(str.lowercased()) {
                                        arrCuisineTags.append(tag)
                                    }
                                }
                            }
                        }
                    }
                }
                break
            }
        }
        
        self.arrImportedCuisineTags = arrCuisineTags
        
        
        //recipeCategory //Meal types
        
        var arrMealTypeTags = [Category_tag]()
        
        for key in jsonDict.keys {
            let lowercasedKey = key.lowercased()
            if lowercasedKey.contains("category") {
                if let str = (jsonDict[key] as? String) {
                    if str.contains(",") {
                        for stringCuisine in str.components(separatedBy: ",") {
                            for tag in enum_tag_list_type.mealType.getAllTags() {
                                if let tag_name = tag.tag_name {
                                    if stringCuisine.lowercased().contains(tag_name.lowercased()) {
                                        arrMealTypeTags.append(tag)
                                    } else {
                                        if tag_name.lowercased().contains(stringCuisine.lowercased()) {
                                            arrMealTypeTags.append(tag)
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        for tag in enum_tag_list_type.mealType.getAllTags() {
                            if let tag_name = tag.tag_name {
                                if str.lowercased().contains(tag_name.lowercased()) {
                                    arrMealTypeTags.append(tag)
                                } else {
                                    if tag_name.lowercased().contains(str.lowercased()) {
                                        arrMealTypeTags.append(tag)
                                    }
                                }
                            }
                        }
                    }
                }
                break
            }
        }
        
        self.arrImportedMealTypeTags = arrMealTypeTags
        //self.addRecipeApiCall(vc: AddNewRecipeVC.self)
    }
    
    func isDictionaryOfRecipe(jsonDict:[String:Any]) -> Bool {
        let arrStringAllKeys = jsonDict.keys
        
        var foundSimilar = 1
        
        for strKey in arrStringAllKeys {
            
            let strKeyLowercase = strKey.lowercased()
            
            if strKeyLowercase.contains("ingredients") {
                foundSimilar += 1
            }
            if strKeyLowercase.contains("ingredient") {
                foundSimilar += 1
            }
            if strKeyLowercase.contains("instructions") {
                foundSimilar += 1
            }
            if strKeyLowercase.contains("instruction") {
                foundSimilar += 1
            }
            if strKeyLowercase.contains("directions") {
                foundSimilar += 1
            }
            if strKeyLowercase.contains("direction") {
                foundSimilar += 1
            }
            
            if strKeyLowercase.contains("preptime") {
                foundSimilar += 1
            }
            if strKeyLowercase.contains("preparationtime") {
                foundSimilar += 1
            }
            
        }
        
        if foundSimilar >= 2 {
            return true
        }
        return false
    }
    
    func replaceHtmlSymbols(strText:String) -> String {
        
        let mutStrText = strText
        if mutStrText.contains("&#188;") {
            return mutStrText.replacingOccurrences(of: "&#188;", with: "1/4")
        }
        if mutStrText.contains("¼") {
            return mutStrText.replacingOccurrences(of: "¼", with: "1/4")
        }
        
        if mutStrText.contains("&#189;") {
            return mutStrText.replacingOccurrences(of: "&#189;", with: "1/2")
        }
        if mutStrText.contains("½") {
            return mutStrText.replacingOccurrences(of: "½", with: "1/2")
        }
        
        
        if mutStrText.contains("&#190;") {
            return mutStrText.replacingOccurrences(of: "&#190;", with: "3/4")
        }
        if mutStrText.contains("¾") {
            return mutStrText.replacingOccurrences(of: "¾", with: "3/4")
        }
        
        
        if mutStrText.contains("&#8528;") {
            return mutStrText.replacingOccurrences(of: "&#8528;", with: "1/7")
        }
        if mutStrText.contains("⅐") {
            return mutStrText.replacingOccurrences(of: "⅐", with: "1/7")
        }
        
        
        if mutStrText.contains("&#8529;") {
            return mutStrText.replacingOccurrences(of: "&#8529;", with: "1/9")
        }
        if mutStrText.contains("⅑") {
            return mutStrText.replacingOccurrences(of: "⅑", with: "1/9")
        }
                
        if mutStrText.contains("&#8530;") {
            return mutStrText.replacingOccurrences(of: "&#8530;", with: "1/10")
        }
        if mutStrText.contains("⅒") {
            return mutStrText.replacingOccurrences(of: "⅒", with: "1/10")
        }
        
        if mutStrText.contains("&#8531;") {
            return mutStrText.replacingOccurrences(of: "&#8531;", with: "1/3")
        }
        if mutStrText.contains("⅓") {
            return mutStrText.replacingOccurrences(of: "⅓", with: "1/3")
        }
                
        if mutStrText.contains("&#8532;") {
            return mutStrText.replacingOccurrences(of: "&#8532;", with: "2/3")
        }
        if mutStrText.contains("⅔") {
            return mutStrText.replacingOccurrences(of: "⅔", with: "2/3")
        }
        
        
        if mutStrText.contains("&#8533;") {
            return mutStrText.replacingOccurrences(of: "&#8533;", with: "1/5")
        }
        if mutStrText.contains("⅕") {
            return mutStrText.replacingOccurrences(of: "⅕", with: "1/5")
        }
        
        if mutStrText.contains("&#8534;") {
            return mutStrText.replacingOccurrences(of: "&#8534;", with: "2/5")
        }
        if mutStrText.contains("⅖") {
            return mutStrText.replacingOccurrences(of: "⅖", with: "2/5")
        }
                
        if mutStrText.contains("&#8535;") {
            return mutStrText.replacingOccurrences(of: "&#8535;", with: "3/5")
        }
        if mutStrText.contains("⅗") {
            return mutStrText.replacingOccurrences(of: "⅗", with: "3/5")
        }
        
        if mutStrText.contains("&#8536;") {
            return mutStrText.replacingOccurrences(of: "&#8536;", with: "4/5")
        }
        if mutStrText.contains("⅘") {
            return mutStrText.replacingOccurrences(of: "⅘", with: "4/5")
        }
        
        if mutStrText.contains("&#8537;") {
            return mutStrText.replacingOccurrences(of: "&#8537;", with: "1/6")
        }
        if mutStrText.contains("⅙") {
            return mutStrText.replacingOccurrences(of: "⅙", with: "1/6")
        }
        
        if mutStrText.contains("&#8538;") {
            return mutStrText.replacingOccurrences(of: "&#8538;", with: "5/6")
        }
        if mutStrText.contains("⅚") {
            return mutStrText.replacingOccurrences(of: "⅚", with: "5/6")
        }
                
        if mutStrText.contains("&#8539;") {
            return mutStrText.replacingOccurrences(of: "&#8539;", with: "1/8")
        }
        if mutStrText.contains("⅛") {
            return mutStrText.replacingOccurrences(of: "⅛", with: "1/8")
        }
                
        if mutStrText.contains("&#8540;") {
            return mutStrText.replacingOccurrences(of: "&#8540;", with: "3/8")
        }
        if mutStrText.contains("⅜") {
            return mutStrText.replacingOccurrences(of: "⅜", with: "3/8")
        }
        
        if mutStrText.contains("&#8541;") {
            return mutStrText.replacingOccurrences(of: "&#8541;", with: "5/8")
        }
        if mutStrText.contains("⅝") {
            return mutStrText.replacingOccurrences(of: "⅝", with: "5/8")
        }
        
        if mutStrText.contains("&#8542;") {
            return mutStrText.replacingOccurrences(of: "&#8542;", with: "7/8")
        }
        if mutStrText.contains("⅞") {
            return mutStrText.replacingOccurrences(of: "⅞", with: "7/8")
        }
        return strText
    }
    
    func saveRecipeImageDataToLocal(url:URL) {
        if let recipeImageData = try? Data.init(contentsOf:url) {
            if let recipeImage = UIImage.init(data: recipeImageData) {
                if let imageLocalUrl = recipeImage.mk_saveToDocuments(filename: "\(Date().mk_ticks).jpg", folderName: tmpDocFolderName) {
                    self.addNewRecipeData.media_url                 = imageLocalUrl
                    self.addNewRecipeData.is_video                  = false.mk_intValue
                    self.addNewRecipeData.image_temp_to_crop        = recipeImage
                }
            }
        }
    }
    
    func handleYields(strServes:String) {
        
        let stringArray = strServes.components(separatedBy: CharacterSet.decimalDigits.inverted)
        var arrInts = [Int]()
        for item in stringArray {
            if let number = Int(item) {
                print("number: \(number)")
                arrInts.append(number)
            }
        }
        
        if let f = arrInts.first {
            self.addNewRecipeData.serves = f
        }
        
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        
        print("-------++++++++------- : \(strServes)")
        
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        
    }
    
    func checkVideoAndDownload() {
        
        // Imported Video Url (imported_media_url) Not Found
        //        self.dismiss {
        //            self.compBlockWithUrl(self.addNewRecipeData)
        //        }
        
    }
    
    func getHoursAndMins(strDate:String? = nil, itempropname:String? = nil) -> (String?,String?) {
        
        if let strDate = strDate {
            
            if strDate.contains("PT") {
                return strDate.getHoursMinutes()
            } else {
                // Starting with P // P0Y0M0DT0H5M0.000S
                if strDate.hasPrefix("P") && strDate.contains(".") {
                    let arrTime = strDate.components(separatedBy: ".")
                    if let timeValue = arrTime.first {
                        return timeValue.getHoursMinutes()
                    }
                }
            }
            
        }
        
        do {
            if let itempropname = itempropname {
                
                let arrTime = try self.doc.getElementsByAttributeValue("itemprop", itempropname)
                
                
                if let arrAttributes = arrTime.array().first?.getAttributes()?.asList() {
                    for attr in arrAttributes {
                        if attr.getValue().contains("PT") {
                            return attr.getValue().getHoursMinutes()
                        } else {
                            // Starting with P // P0Y0M0DT0H5M0.000S
                            
                            if attr.getValue().hasPrefix("P") && attr.getValue().contains(".") {
                                
                                let arrTime = attr.getValue().components(separatedBy: ".")
                                
                                if let timeValue = arrTime.first {
                                    return timeValue.getHoursMinutes()
                                }
                            }
                        }
                    }
                }
                
                //                if txtHours.text == "" && txtMinutes.text == "" {
                //                    if let timeFound = arrTime.array().first {
                //                        txtHours.text = try timeFound.text()
                //                    } else {
                //                        txtHours.text = ""
                //                        txtMinutes.text = ""
                //                    }
                //                }
            }
            
            
        } catch {
            print(error)
            return (nil,nil)
        }
        return (nil,nil)
    }
    
    
    func handleTime() {
        
        do {
            
            let prepTime = self.getHoursAndMins(itempropname: "prepTime")
            let cookTime = self.getHoursAndMins(itempropname: "cookTime")
            let totalTime = self.getHoursAndMins(itempropname: "totalTime")
            
            if let yieldFound = try self.doc.getElementsByAttributeValue("itemprop", "recipeYield").array().first {
                let serve = try yieldFound.text()
            } else {
                let serve = ""
            }
            
        } catch {
            print(error)
        }
        
    }
    
    func handleDirectionsOfRecipe(keyOfDirection:String, jsonDict:[String:Any]) -> [String]? {
        
        
        var arrDirection = [String]()
        
        if let arrDictDirections = jsonDict[keyOfDirection] as? [[String:Any]] {
            
            for (_,dict) in arrDictDirections.enumerated() {
                
                for key in dict.keys {
                    
                    let keyLowercased = key.lowercased()
                    if keyLowercased.contains("text") {
                        
                        arrDirection.append(("\(((dict[key] as? String) ?? ""))"))
                        
                    } else {
                        
                        if let arrTexts = dict[key] as? [[String:Any]] {
                            
                            for key2 in dict.keys {
                                if key2.lowercased().contains("name") {
                                    if let strName = dict[key2] as? String {
                                        print(strName)
                                        arrDirection.append(strName)
                                    }
                                }
                            }
                            
                            for (_,subDictText) in arrTexts.enumerated() {
                                for key in subDictText.keys {
                                    let keyLowercased = key.lowercased()
                                    if keyLowercased.contains("text") {
                                        arrDirection.append(((subDictText[key] as? String) ?? ""))
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            return arrDirection
        }
        
        if let dictDirections = jsonDict[keyOfDirection] as? [String:Any] {
            
            for (_,key) in dictDirections.keys.enumerated() {
                
                let keyLowercased = key.lowercased()
                if keyLowercased.contains("text") {
                    arrDirection.append((dictDirections[key] as? String) ?? "")
                }
                
            }
            
        }
        
        if let dictDirections = jsonDict[keyOfDirection] as? [String] {
            
            return dictDirections
            
        }
        
        return arrDirection
    }
    
    func fillDataInReviewModel(reviewModelSent:ReviewModel, dictReview:[String:Any]) -> ReviewModel {
        var reviewModel = reviewModelSent
        
        if let authorReview = dictReview["author"] as? String {
            reviewModel.author = authorReview
        }
        
        if let authorDictReview = dictReview["author"] as? [String:Any] {
            if let authorReview = authorDictReview["name"] as? String {
                reviewModel.author = authorReview
            }
        }
        
        if let datePublished = dictReview["datePublished"] as? String {
            reviewModel.datePublishedString = datePublished
        }
        
        if let reviewBody = dictReview["reviewBody"] as? String {
            reviewModel.reviewBody = reviewBody
        }
        
        if let ratingValue = dictReview["ratingValue"] as? String {
            reviewModel.ratingValue = ratingValue
        }
        
        if let bestRating = dictReview["bestRating"] as? String {
            reviewModel.bestRating = bestRating
        }
        
        if let worstRating = dictReview["worstRating"] as? String {
            reviewModel.worstRating = worstRating
        }
        
        if let ratingValue = dictReview["ratingValue"] as? Double {
            reviewModel.ratingValue = "\(ratingValue)"
        }
        
        if let bestRating = dictReview["bestRating"] as? Double {
            reviewModel.bestRating = "\(bestRating)"
        }
        
        if let worstRating = dictReview["worstRating"] as? Double {
            reviewModel.worstRating = "\(worstRating)"
        }
        
        return reviewModel
        
    }
    
}

//MARK:- ADD Edmam Recipe
extension CellRecipeDetails {
    
//    func btnSaveTapped(vc:AddNewRecipeVC) {
//
//        hapHeavy()
//
//        if vc.addNewRecipeData.title == "" {
//
//            vc.tblView.scrollToTopOrLeft()
//            Timer.after(0.4) {
//                vc.cellAddNewRecipeTitleImage.txtTitle.shake()
//                vc.cellAddNewRecipeTitleImage.txtTitle.becomeFirstResponder()
//                hapError()
//            }
//
//            return
//        }
//
//        if vc.arrIngredints.count == 0 {
//
//            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 2), at: .middle, animated: true)
//            Timer.after(0.4) {
//                vc.cellAddNewRecipeIngredients.shake()
//                hapError()
//            }
//
//            return
//        }
//
//        if vc.arrDirections.count == 0 {
//
//            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 4), at: .middle, animated: true)
//            Timer.after(0.4) {
//                vc.cellAddNewRecipeDirections.shake()
//                hapError()
//            }
//
//            return
//        }
//
//        if vc.addNewRecipeData.serves == 0 && vc.addNewRecipeData.web_recipe_url == "" {
//            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 6), at: .bottom, animated: true)
//            Timer.after(0.4) {
//                vc.cellAddNewRecipeBottomServesBtns.txtServes.shake()
//                vc.cellAddNewRecipeBottomServesBtns.txtServes.becomeFirstResponder()
//                hapError()
//            }
//
//            return
//        }
//
//
//        if vc.addNewRecipeData.ready_time_hours == nil && vc.addNewRecipeData.ready_time_mins == 0 && vc.addNewRecipeData.web_recipe_url == "" {
//            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 6), at: .bottom, animated: true)
//            Timer.after(0.4) {
//                vc.cellAddNewRecipeBottomServesBtns.txtMins.shake()
//                vc.cellAddNewRecipeBottomServesBtns.txtHours.shake()
//                hapError()
//            }
//            return
//        }
//
//        if vc.addNewRecipeData.ready_time_hours == 0 && vc.addNewRecipeData.ready_time_mins == nil && vc.addNewRecipeData.web_recipe_url == "" {
//            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 6), at: .bottom, animated: true)
//            Timer.after(0.4) {
//                vc.cellAddNewRecipeBottomServesBtns.txtMins.shake()
//                vc.cellAddNewRecipeBottomServesBtns.txtHours.shake()
//                hapError()
//            }
//            return
//        }
//
//        if vc.addNewRecipeData.ready_time_hours == nil && vc.addNewRecipeData.ready_time_mins == nil {
//            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 6), at: .bottom, animated: true)
//            Timer.after(0.4) {
//                vc.cellAddNewRecipeBottomServesBtns.txtMins.shake()
//                vc.cellAddNewRecipeBottomServesBtns.txtHours.shake()
//                hapError()
//            }
//            return
//        }
//
//        if vc.addNewRecipeData.ready_time_hours == 0 && vc.addNewRecipeData.ready_time_mins == 0 {
//            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 6), at: .bottom, animated: true)
//            Timer.after(0.4) {
//                vc.cellAddNewRecipeBottomServesBtns.txtMins.shake()
//                vc.cellAddNewRecipeBottomServesBtns.txtHours.shake()
//                hapError()
//            }
//
//            return
//        }
//
//        addRecipeApiCall(vc: vc)
//
//        hapHeavy()
//
//    }
    
    func addRecipeApiCall(vc:AddNewRecipeVC) {
        
        var dictParam : [String : Any] = ["title" : vc.addNewRecipeData.title]
        
        if vc.addNewRecipeData.web_recipe_url != "" {
            dictParam.updateValue(vc.addNewRecipeData.web_recipe_url, forKey: "web_recipe_url")
        }
        
        if vc.addNewRecipeData.media_url != "" {
            dictParam.updateValue(vc.addNewRecipeData.is_video, forKey: "is_video")
            dictParam.updateValue(vc.addNewRecipeData.media_url, forKey: "media_url")
        }
        
        if vc.addNewRecipeData.video_thumb_url != "" {
            dictParam.updateValue(vc.addNewRecipeData.video_thumb_url, forKey: "video_thumb_url")
        }
        
        //Ingredient_list
        var arrDictIngredient = [[String:Any]]()
        for objIngredient in vc.arrIngredints {
            
            let ingName = convertFtoC(text: objIngredient.name ?? "")
            
            var dict = ["name":ingName,
                        "qty":objIngredient.qty ?? "",
                        "note":objIngredient.note ?? ""] as [String : Any]
            dict.updateValue(objIngredient.unit_name ?? "", forKey: "unit")
            let id = findAndGetUnitId(unitName: objIngredient.unit_name ?? "")
            //            if id != "" {
            dict.updateValue(id, forKey: "unit_id")
            //            }
            arrDictIngredient.append(dict)
        }
        
        guard let jsonDataArrDictIngredient = try? JSONSerialization.data(
            withJSONObject: arrDictIngredient,
            options: []) else {
                return
        }
        
        guard let textArrIngredienta = String(data: jsonDataArrDictIngredient, encoding: .utf8) else {
            return
        }
        print("JSON string textArrIngredienta = \(textArrIngredienta)")
        dictParam.updateValue(textArrIngredienta, forKey: "ingredient_list")
                
        //direction_list
        
        var arrDirections = [[String:Any]]()
        for objDirection in vc.arrDirections {
            
            let descDirection = convertFtoC(text: objDirection.desc ?? "")
            let dict = ["step":objDirection.step,
                        "description":descDirection] as [String : Any]
            arrDirections.append(dict)
        }
        
        guard let jsonDataDirections = try? JSONSerialization.data(
            withJSONObject: arrDirections,
            options: []) else {
                return
        }
        
        guard let textDirections = String(data: jsonDataDirections, encoding: .utf8) else {
            return
        }
        print("JSON string textDirections = \(textDirections)")
        dictParam.updateValue(textDirections, forKey: "direction_list")
        
        //tag_ids -- enum_tag_list_type.cusineType
        var arrTagsIds = [[String:Any]]()
        let arrStrCuisineTagsIds = vc.arrSelectedCuisineTags.map { (c) -> String in
            "\(c.tag_id)"
        }
        if arrStrCuisineTagsIds.count > 0 {
            if let cuisineCat = enum_tag_list_type.cusineType.getCatObject() {
                arrTagsIds.append(["category_id"      : cuisineCat.category_id,
                                   "selected_tags_id" : arrStrCuisineTagsIds.joined(separator: ",")])
            }
        }
        
        //tag_ids -- enum_tag_list_type.mealType
        let arrStrMealTypeTagsIds = vc.arrSelectedMealTypeTags.map { (c) -> String in
            "\(c.tag_id)"
        }
        if arrStrMealTypeTagsIds.count > 0 {
            if let mealTypeCat = enum_tag_list_type.mealType.getCatObject() {
                arrTagsIds.append(["category_id"      : mealTypeCat.category_id,
                                   "selected_tags_id" : arrStrMealTypeTagsIds.joined(separator: ",")])
            }
        }
        
        //tag_ids -- enum_tag_list_type.diet
        let arrStrDietTagsIds = vc.arrSelectedDietTags.map { (c) -> String in
            "\(c.tag_id)"
        }
        if arrStrDietTagsIds.count > 0 {
            if let mealTypeCat = enum_tag_list_type.diet.getCatObject() {
                arrTagsIds.append(["category_id"      : mealTypeCat.category_id,
                                   "selected_tags_id" : arrStrDietTagsIds.joined(separator: ",")])
            }
        }
        
        if arrTagsIds.count > 0 {
            if let jsonDataArrTagsIds = try? JSONSerialization.data(withJSONObject: arrTagsIds, options: []) {
                if let textArrTagsIds = String(data: jsonDataArrTagsIds, encoding: .utf8) {
                    print("JSON string textDirections = \(textArrTagsIds)")
                    dictParam.updateValue(textArrTagsIds, forKey: "tag_ids")
                }
            }
        }
        
        
        ///////
        
        //        if vc.addNewRecipeData.is_easy {
        dictParam.updateValue(vc.addNewRecipeData.is_easy, forKey: "is_easy")
        //        }
        
        //        if vc.addNewRecipeData.is_one_pot {
        dictParam.updateValue(vc.addNewRecipeData.is_one_pot, forKey: "is_one_pot")
        //        }
        
        
        dictParam.updateValue(vc.addNewRecipeData.is_own_recipe, forKey: "is_own_recipe")
        dictParam.updateValue(vc.addNewRecipeData.submit_recipe_for_review, forKey: "submit_recipe_for_review")
        
        
        
        
        //keywords_list
        let arrStringKeywords = vc.arrAddedKeywords.map { (objKeyword) -> String in
            objKeyword.desc ?? ""
        }
        dictParam.updateValue(arrStringKeywords.joined(separator: ","), forKey: "keywords_list")
        
        //ready_time_hours, ready_time_mins, serves
        dictParam.updateValue(vc.addNewRecipeData.ready_time_hours ?? 0, forKey: "ready_time_hours")
        dictParam.updateValue(vc.addNewRecipeData.ready_time_mins ?? 0, forKey: "ready_time_mins")
        
        
        
        dictParam.updateValue(vc.addNewRecipeData.serves, forKey: "serves")
        
        //collection_id
        if vc.addNewRecipeData.collection_id == 0 {
            self.selectCollectionFlow(vc: vc)
            return
        }
        
        dictParam.updateValue(vc.addNewRecipeData.collection_id, forKey: "collection_id")
        
        
        if vc.addNewRecipeData.media_url != "" {
            
            if vc.addNewRecipeData.is_video.mk_boolValue {
                // Selected video for upload
                if vc.addNewRecipeData.imported_media_url == "" {
                    
                    let fileSizeDouble = fileSize(forURL: vc.addNewRecipeData.media_url)
                    if fileSizeDouble > 50.0 {
                        // fize size greater than 50 MB
                        BasicFunctions.showPopupOneButton(title: "Video", desc: "Video should be less than 50 mb.", btnTitle: "OKAY".localize()) {
                            
                        }
                        return
                    }
                }
                
            } else {
                // Selected photo for upload
                
                let fileSizeDouble = fileSize(forURL: URL.init(fileURLWithPath: vc.addNewRecipeData.media_url))
                if fileSizeDouble > 10.0 {
                    // fize size greater than 50 MB
                    BasicFunctions.showPopupOneButton(title: "Video", desc: "Photo should be less than 5 mb.", btnTitle: "OKAY".localize()) {
                        
                    }
                    return
                }
            }
            
            if vc.addNewRecipeData.imported_media_url == "" {
                self.uploadMedia(vc: vc, dictParam: dictParam) { (dict,success)  in
                    if success {
                        self.finalApiCallAddRecipe(vc:vc,dictParam: dict)
                    }
                }
            } else if vc.addNewRecipeData.imported_media_url.contains("http") && vc.addNewRecipeData.imported_video_thumb_url != "" && (vc.addNewRecipeData.imported_video_thumb_url.contains("http") == false) {
                
                self.uploadThumbnailAndGetLink(localThumbUrl: vc.addNewRecipeData.imported_video_thumb_url) { (liveThumbnailUrl) in
                    
                    vc.addNewRecipeData.imported_video_thumb_url = liveThumbnailUrl
                    vc.addNewRecipeData.video_thumb_url = liveThumbnailUrl
                    
                    dictParam["video_thumb_url"] = liveThumbnailUrl
                    self.finalApiCallAddRecipe(vc:vc,dictParam: dictParam)
                }
            } else {
                self.finalApiCallAddRecipe(vc:vc,dictParam: dictParam)
            }
            
        } else {
            self.finalApiCallAddRecipe(vc:vc,dictParam: dictParam)
        }
        
        
    }
    
    func finalApiCallAddRecipe(vc:AddNewRecipeVC, dictParam:[String:Any]) {
        
        if vc.isInEditMode {
            
            var mutParams = dictParam
            mutParams.updateValue(vc.recipeIdEditMode, forKey: "recipe_id")
            
            if vc.addNewRecipeData.web_recipe_url != "" {
                mutParams.removeValue(forKey: "submit_recipe_for_review")
            }
            
            editRecipe(params: mutParams) { (res) in
                switch res {
                case .success(let dictData, let msg):
                    print("__________________________________________")
                    print(dictData)
                    BasicFunctions.showPopupOneButton(title: "Edit Recipe".localize(), desc: msg, baseVC: vc) {
                        hapHeavy()
                        
                        UIViewController.CVC()?.navigationController?.popViewController(animated: true)
                        vc.editedDoneEditMode()
                    }
                    
                    break
                case .failed(let msg):
                    BasicFunctions.showError(strError: msg)
                    break
                case .error(let error):
                    BasicFunctions.showError(strError: error)
                    break
                }
                apde.hideLoader()
            }
            
        } else {
            
            var mutParams = dictParam
            mutParams.updateValue(vc.isEdmamRecipe, forKey: "imported_by_edmam")
            
            addRecipe(params: mutParams) { (res) in
                switch res {
                case .success(let dictData, let msg):
                    print("__________________________________________")
                    print(dictData)
                    BasicFunctions.showPopupOneButton(title: "Add Recipe".localize(), desc: msg, baseVC: vc) {
                        hapHeavy()
                        if vc.isEdmamRecipe {
                            if let recipe_details = dictData["recipe_details"] as? Dictionary<String,Any> {
                                if let recipe_id = recipe_details["id"] as? Int64 {
                                    vc.edmamRecipeAdded?(recipe_id)
                                }
                            }
                        }
                        updateMyRecipesVC(vcSelf: vc)
                        UIViewController.CVC()?.navigationController?.popViewController(animated: true)
                    }
                    
                    break
                case .failed(let msg):
                    BasicFunctions.showError(strError: msg)
                    break
                case .error(let error):
                    BasicFunctions.showError(strError: error)
                    break
                }
                apde.hideLoader()
            }
        }
        
    }

    func selectCollectionFlow(vc:AddNewRecipeVC) {
        
        let objCookbookCollectionNameList = apde.getController(vc_name: "CookbookCollectionNameList", sb: .CookBookTab) as! CookbookCollectionNameList
        objCookbookCollectionNameList.compBlock = { (objCollection,isCancelTapped) in
            
            if isCancelTapped {
                return
            }
            
            if let objCollection = objCollection {
                // Selected Collection
                print(objCollection.id)
                vc.addNewRecipeData.collection_id = Int64(objCollection.id)
                self.addRecipeApiCall(vc: vc)
                
            } else {
                // Selected Add New Collection
                let objAddCookBookCollectionVC = apde.getController(vc_name: "AddCookBookCollectionVC", sb: .CookBookTab) as! AddCookBookCollectionVC
                objAddCookBookCollectionVC.shouldDisplaySuccessPopup = false
                objAddCookBookCollectionVC.compBlock = { (boolValue) in
                    // if Collection Created
                    if boolValue {
                        self.selectCollectionFlow(vc: vc)
                    }
                }
                vc.navigationController?.pushViewController(objAddCookBookCollectionVC, animated: true)
            }
            
        }
        UIViewController.CVC()?.present(objCookbookCollectionNameList, animated: true, completion: nil)
    }

    func uploadMedia(vc:AddNewRecipeVC,dictParam:[String:Any],compHandler:@escaping ([String:Any],Bool)->()) {
        
        var mutDictParam = dictParam
        
        if vc.addNewRecipeData.is_video.mk_boolValue {
            
            // Upload video file
            
            apde.showLoader()
            
            AWSHelper.sharedInstance.upload(fileName: "\(Date().mk_ticks).mp4", fileURL: URL.init(string: vc.addNewRecipeData.media_url)!) { (res) in
                
                switch res {
                case .success(let localURL,let liveURL) :
                    deleteFileAtPath(url: localURL.absoluteString)
                    
                    vc.addNewRecipeData.live_media_url = liveURL.absoluteString
                    
                    mutDictParam.updateValue(vc.addNewRecipeData.live_media_url, forKey: "media_url")
                    
                    AWSHelper.sharedInstance.upload(fileName: "\(Date().mk_ticks)_thumb.jpg", fileURL: URL.init(fileURLWithPath: vc.addNewRecipeData.video_thumb_url)) { (res) in
                        
                        switch res {
                        case .success(let localURL,let liveURL) :
                            deleteFileAtPath(url: localURL.absoluteString)
                            
                            vc.addNewRecipeData.live_video_thumb_url = liveURL.absoluteString
                            
                            mutDictParam.updateValue(vc.addNewRecipeData.live_video_thumb_url, forKey: "video_thumb_url")
                            
                            compHandler(mutDictParam,true)
                            
                            
                            break
                        case .error( _,let error) :
                            BasicFunctions.showError(strError: error.localizedDescription)
                            print(error)
                            apde.hideLoader()
                            compHandler(mutDictParam,false)
                            
                            return
                            
                            
                        case .progress(let uploadProgressPercent):
                            print(uploadProgressPercent)
                            break
                            
                        }
                        
                    }
                    
                    
                    
                    break
                case .error( _,let error) :
                    print(error)
                    BasicFunctions.showError(strError: error.localizedDescription)
                    apde.hideLoader()
                    compHandler(mutDictParam,false)
                    return
                    
                    
                    
                case .progress(let uploadProgressPercent):
                    print(uploadProgressPercent)
                    break
                    
                }
                
            }
            
            
            
        } else {
            
            // Upload photo file
            
            apde.showLoader()
            
            AWSHelper.sharedInstance.upload(fileName: "\(Date().mk_ticks).jpg", fileURL: URL.init(fileURLWithPath: vc.addNewRecipeData.media_url)) { (res) in
                
                switch res {
                case .success(let localURL,let liveURL) :
                    
                    print(liveURL.path)
                    print("----------------")
                    print(liveURL.absoluteString)
                    print(liveURL.relativeString)
                    
                    deleteFileAtPath(url: localURL.absoluteString)
                    
                    vc.addNewRecipeData.live_media_url = liveURL.absoluteString
                    
                    mutDictParam.updateValue(vc.addNewRecipeData.live_media_url, forKey: "media_url")
                    
                    compHandler(mutDictParam,true)
                    return
                    
                    
                case .error(let fileURL,let error) :
                    print(fileURL)
                    print("----------------")
                    print(error)
                    BasicFunctions.showError(strError: error.localizedDescription)
                    apde.hideLoader()
                    compHandler(mutDictParam,false)
                    return
                    
                case .progress(let uploadProgressPercent):
                    print(uploadProgressPercent)
                    break
                    
                }
            }
        }
    }

    func uploadThumbnailAndGetLink(localThumbUrl:String, compBlock: @escaping (String)->()) {
        
        apde.showLoader()
        
        AWSHelper.sharedInstance.upload(fileName: "\(Date().mk_ticks).mp4", fileURL: URL.init(fileURLWithPath: localThumbUrl)) { (res) in
            
            switch res {
            case .success(let localURL,let liveURL) :
                deleteFileAtPath(url: localURL.absoluteString)
                compBlock(liveURL.absoluteString)
                break
            case .error( _,let error) :
                print(error)
                BasicFunctions.showError(strError: error.localizedDescription)
                apde.hideLoader()
                return
            case .progress(let uploadProgressPercent):
                print(uploadProgressPercent)
                break
                
            }
            
        }
    }

}
