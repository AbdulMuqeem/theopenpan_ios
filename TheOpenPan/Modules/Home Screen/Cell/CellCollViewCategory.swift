//
//  CellCollViewCategory.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/13/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class CellCollViewCategory: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var imgViewRecipeCategory : UIImageView!
    @IBOutlet weak var viewDarkOnImage : UIView!
    
    override func awakeFromNib() {
        self.imgViewRecipeCategory?.image = nil
    }
    
    
    
    func handleCell(_ categoryTag:Category_tag, indexPath:IndexPath) {
        
        self.imgViewRecipeCategory.image = nil
        self.viewDarkOnImage.isHidden = true
        self.lblTitle.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        
        self.imgViewRecipeCategory.mk_afSetImage(urlString: categoryTag.tag_image_url, defaultImage: nil) { (img, boolValue) in
            
            if img == nil {
                self.imgViewRecipeCategory.image = nil
                self.lblTitle.textColor = #colorLiteral(red: 0.7450980392, green: 0.6823529412, blue: 0.6549019608, alpha: 1)
            }
            self.viewDarkOnImage.isHidden = img == nil
        }
        
        self.lblTitle.text = categoryTag.tag_name
    }
    
    func handleCellOnePotCell(_ vc:HomeController, indexPath:IndexPath) {
        
        self.imgViewRecipeCategory.image = nil
        self.viewDarkOnImage.isHidden = true
        self.lblTitle.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        
        self.imgViewRecipeCategory.mk_afSetImage(urlString: vc.one_pot_image, defaultImage: nil) { (img, boolValue) in
            
            if img == nil {
                self.imgViewRecipeCategory.image = nil
                self.lblTitle.textColor = #colorLiteral(red: 0.7450980392, green: 0.6823529412, blue: 0.6549019608, alpha: 1)
            }
            self.viewDarkOnImage.isHidden = img == nil
        }
        
        self.lblTitle.text = "One-pot meals"
    }
    
}
