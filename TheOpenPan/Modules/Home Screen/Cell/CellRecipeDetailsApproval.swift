//
//  CellRecipeDetailsApproval.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/21/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class CellRecipeDetailsApproval: UITableViewCell {
    
    @IBOutlet weak var lblDesc      : UILabel!
    @IBOutlet weak var btnAccept    : UIButton!
    @IBOutlet weak var btnReject    : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func handleCell(vc:RecipeDetailsVC,ip:ip) {
        
        self.lblDesc.text = vc.recipeDetails.admin_notes
        
        self.btnAccept.onTap {
            self.recipeApprovalApiCall(recipe_id: vc.recipe_id, accept: true.mk_intValue) {
                vc.getRecipeDetailsApiCall()
                updateMyRecipesVC(vcSelf: vc)
            }
        }
        
        self.btnReject.onTap {
            self.recipeApprovalApiCall(recipe_id: vc.recipe_id, accept: false.mk_intValue) {
                vc.getRecipeDetailsApiCall()
                updateMyRecipesVC(vcSelf: vc)
            }
        }
    }
    
    
    func recipeApprovalApiCall(recipe_id: Int64, accept: Int, compHandler: @escaping ()->()) {
        
        apde.showLoader()
        
        recipeApproval(recipe_id: recipe_id, accept: accept) { (res) in
            switch res {
            case .success(let dictData, let message) :
                
                break
            case .failed(message: let msg):
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                BasicFunctions.showError(strError: error)
                break
            }
            compHandler()
            apde.hideLoader()
        }
        
        
    }
    
}

///recipe/updatestatus
