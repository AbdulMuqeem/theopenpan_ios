//
//  CellRecipeDetailsWebView.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 3/5/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import WebKit

class CellRecipeDetailsWebView: UITableViewCell {
    
    @IBOutlet weak var viewLoader           : UIView!
    @IBOutlet weak var btnCancel            : UIButton!
    @IBOutlet weak var txtUrlAddress        : UITextField!
    @IBOutlet weak var btnReload            : UIButton!
    @IBOutlet weak var btnSaveAndView       : UIButton!
    @IBOutlet weak var viewContainerWebView     : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    func handleCell(vc:RecipeDetailsVC) {
        
        self.btnSaveAndView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.btnSaveAndView.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.btnSaveAndView.layer.shadowOpacity = 1.0
        self.btnSaveAndView.layer.shadowRadius = 10.0
        self.btnSaveAndView.layer.masksToBounds = false
        
        let myURLString = vc.recipeDetails.web_url
        self.txtUrlAddress.text = myURLString
        
        vc.webViewRecipeBrowse.navigationDelegate = self
        self.viewContainerWebView.mk_addFourSideConstraintsInParent(childView: vc.webViewRecipeBrowse)
        
        if vc.webViewRecipeBrowse.url == nil {
            self.viewLoader.mk_animateAlpha(sec: 0.3, alpha: 1.0)
            let url = URL(string: myURLString)
            let request = URLRequest(url: url!)
            vc.webViewRecipeBrowse.load(request)
        }
        
        self.btnReload.onTap {
            hapSoft()
            let myURLString = vc.webViewRecipeBrowse.url?.absoluteString
            self.txtUrlAddress.text = vc.webViewRecipeBrowse.url?.absoluteString
            let url = URL(string: myURLString ?? "https://www.google.com")
            let request = URLRequest(url: url!)
            vc.webViewRecipeBrowse.load(request)
            Timer.after(1) {
                self.viewLoader.mk_animateAlpha(sec: 0.3, alpha: 0.0)
            }
        }
        
        
        self.btnSaveAndView?.onTap {
            
//            if vc.recipe_id != 0 {
//                saveRecipeDirectInCookbook(vc: vc, recipe_id: vc.recipe_id)
//                return
//            }
            
            
            let objImportRecipePopupVC = apde.getController(vc_name: "ImportRecipePopupVC", sb: .AddRecipe) as! ImportRecipePopupVC
            objImportRecipePopupVC.compBlockWithUrl = { addNewRecipeData in
                
                let mutAddNewRecipeData = addNewRecipeData
                let objAddNewRecipeVC = makeReadyAddNewRecipeVcFromImport(addNewRecipeData: mutAddNewRecipeData, arrImportedKeywords:objImportRecipePopupVC.arrImportedKeywords, arrImportedCuisineTags:objImportRecipePopupVC.arrImportedCuisineTags, arrImportedMealTypeTags:objImportRecipePopupVC.arrImportedMealTypeTags)
                objAddNewRecipeVC.isEdmamRecipe = true
                objAddNewRecipeVC.edmamRecipeAdded = { (recipe_id) in
                    vc.recipe_id = recipe_id
                    vc.isThird = false
                    vc.getRecipeDetailsApiCall()
                    updateMyRecipesVC(vcSelf: vc)
                }
                vc.navigationController?.pushViewController(objAddNewRecipeVC, animated: true)
            }
            
            objImportRecipePopupVC.compBlockError = {
                
                RecipeParser.getRecipeData(url: myURLString) { (objAddNewRecipeData) in
                    
                    objImportRecipePopupVC.dismiss(animated: true) {
                        
                        print("\n\ncompBlockError\n\n")
                        
                        if let objAddNewRecipeData = objAddNewRecipeData {
                            
                            let objAddNewRecipeVC = makeReadyAddNewRecipeVcFromImport(addNewRecipeData: objAddNewRecipeData, arrImportedKeywords: [Keyword](), arrImportedCuisineTags: [Category_tag](), arrImportedMealTypeTags: [Category_tag]())
                            objAddNewRecipeVC.isEdmamRecipe = true
                            objAddNewRecipeVC.edmamRecipeAdded = { (recipe_id) in
                                vc.recipe_id = recipe_id
                                vc.isThird = false
                                vc.getRecipeDetailsApiCall()
                                updateMyRecipesVC(vcSelf: vc)
                            }
                            vc.navigationController?.pushViewController(objAddNewRecipeVC, animated: true)
                            
                        } else {
                            print("Error")
                            BasicFunctions.showError(strError: strErrorForRecipeNotFound)
                        }
                    }
                }
            }
            
            vc.tabBarController?.present(objImportRecipePopupVC, animated: true, completion: nil)
            Timer.after(0.6) {
                objImportRecipePopupVC.txtUrl.text = vc.webViewRecipeBrowse.url?.absoluteString
                objImportRecipePopupVC.btnImportTapped(btn: objImportRecipePopupVC.btnImport)
            }
            
        }
    }
    
}

extension CellRecipeDetailsWebView: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Started to load")
        self.viewLoader.mk_animateAlpha(sec: 0.3, alpha: 1.0)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
        self.viewLoader.mk_animateAlpha(sec: 0.3, alpha: 0.0)
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    
}
