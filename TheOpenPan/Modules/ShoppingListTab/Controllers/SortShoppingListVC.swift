//
//  SortMyRecipesVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//


enum enum_sort_shopping_list : Int {
    case MostRecent     = 1
    case Recipe         = 2
    case Alphabetic     = 3
}

import Foundation

class SortShoppingListVC: UIViewController {

    @IBOutlet weak var viewBackAlpha        : UIView!
    @IBOutlet weak var viewCenter           : UIView!
    @IBOutlet weak var btnOk                : UIButton!
    @IBOutlet weak var cntrWidth            : NSLayoutConstraint!
    
    @IBOutlet weak var btnMostRecent        : UIButton!
    @IBOutlet weak var btnRecipe            : UIButton!
    @IBOutlet weak var btnAlphabetic        : UIButton!
    
    var radioButtons                        : [UIButton]!
    typealias CompletionBlock               = ((_ selectedSort:enum_sort_shopping_list)->())
    var completionBlock                     : CompletionBlock!
    var currentSelectedSortOption           : enum_sort_shopping_list! = .MostRecent
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
    }
    
    func setupLayout() {
        
        radioButtons = [self.btnMostRecent,self.btnRecipe,self.btnAlphabetic]
        
        if ssw <= 320 {
            self.cntrWidth.constant = 290
        }
        
        self.viewBackAlpha.alpha = 0
        self.btnOk.mk_addTapHandler { (btn) in
            hapHeavy()
            self.dismiss()
            self.completionBlock(self.currentSelectedSortOption)
        }
        
        switch self.currentSelectedSortOption {
        case .MostRecent:
            self.setRadioButtons(button: self.btnMostRecent)
        case .Recipe:
            self.setRadioButtons(button: self.btnRecipe)
        case .Alphabetic:
            self.setRadioButtons(button: self.btnAlphabetic)
        default:
            self.setRadioButtons(button: self.btnMostRecent)
        }
        
        
        self.btnMostRecent.mk_addTapHandler { (btn) in
            hapSoft()
            self.setRadioButtons(button: btn)
            self.currentSelectedSortOption = .MostRecent
        }
        
        self.btnRecipe.mk_addTapHandler { (btn) in
            hapSoft()
            self.setRadioButtons(button: btn)
            self.currentSelectedSortOption = .Recipe
        }
        
        self.btnAlphabetic.mk_addTapHandler { (btn) in
            hapSoft()
            self.setRadioButtons(button: btn)
            self.currentSelectedSortOption = .Alphabetic
        }
        
        
        self.viewBackAlpha.addTapGesture { (gesture) in
            hapHeavy()
            self.dismiss()
            self.completionBlock(self.currentSelectedSortOption)
        }
        
    }
    
    func setRadioButtons(button: UIButton) {
        for getradioButton in radioButtons {
            getradioButton.isSelected = false
        }
        button.isSelected = true
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.showBackView()
        self.btnMostRecent.layoutIfNeeded()
        self.btnRecipe.layoutIfNeeded()
        self.btnAlphabetic.layoutIfNeeded()
    }

    func showBackView() {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 1
        }) { (s) in
            
        }
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 0
        }) { (s) in
            self.dismiss(animated: true, completion: nil)
        }
    }


}
