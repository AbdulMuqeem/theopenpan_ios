//
//  ShoppingCartWebViewVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/27/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import WebKit

enum enum_buy_now_type : String {
    case redmart = "redmart"
    case fairprice = "fairprice"
    
    func getUrl() -> String {
        switch self {
        case .redmart:
            return "https://www.lazada.sg/catalog/?spm=a2o42.lazmart_channel.red-mart-search&q="
            
        case .fairprice:
            return "https://www.fairprice.com.sg/search?query="
        }
    }
}

class ShoppingCartWebViewVC: UIViewController, WKUIDelegate {
    
    //------------------------------------------
    //MARK: - Outlets -
    
    @IBOutlet weak var tabBarForBuyWebsites     : UITabBar!
    
    @IBOutlet weak var tblView                  : UITableView!
    @IBOutlet weak var webViewContainer         : UIView!
    @IBOutlet weak var viewBackBlackAlpha       : UIView!
    @IBOutlet weak var btnExpandCollapseList    : UIButton!
    @IBOutlet weak var btnSortBy                : UIButton!
    
    @IBOutlet weak var lblIngName               : UILabel!
    @IBOutlet weak var lblIngQty                : UILabel!
    
    @IBOutlet weak var btnLeftArrow             : UIButton!
    @IBOutlet weak var btnRightArrow            : UIButton!
    @IBOutlet weak var viewLoaderWebView        : UIView!
    
    var currentBuyNowType                       : enum_buy_now_type = .redmart
    var arrIngredientList                       = [Ingredient]()
    var arrRecipeSortIngredientList             = [[String:Any]]()
    var currentSelectedSortOption               : enum_sort_shopping_list! = .MostRecent
    
    var kwStepper                               : KWStepper!

    
    //------------------------------------------
    //MARK: - Class Variables -
    
    var webView                                 : WKWebView!
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        self.viewLoaderWebView.mk_animateAlpha(sec: 0.1, alpha: 1.0)
        
        let myURL = URL(string: currentBuyNowType.getUrl())
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
        webView.navigationDelegate = self
        
        self.navigationItem.title = "Shopping List".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        
        self.setupTableView()
        self.tblView.isHidden = true
        self.viewBackBlackAlpha.isHidden = true
        
        self.setupKWStepper()
        
        self.btnExpandCollapseList.mk_addTapHandler { (btn) in
            self.btnExpandCollapseTapped()
        }
        
        self.viewBackBlackAlpha.addTapGesture { (gesture) in
            self.btnExpandCollapseTapped()
        }
        
        changeTabBar(hidden: true, animated: false, completion: nil)
        
        self.tabBarForBuyWebsites.delegate = self
        
        if self.currentBuyNowType == .redmart {
            self.tabBarForBuyWebsites.selectedItem = self.tabBarForBuyWebsites.items?.first
        } else if self.currentBuyNowType == .fairprice {
            self.tabBarForBuyWebsites.selectedItem = self.tabBarForBuyWebsites.items?.last
        }

        self.btnSortBy.onTap {
            
            hapSoft()
            let objSortShoppingListVC = apde.getController(vc_name: "SortShoppingListVC", sb: .ShoppingListTab) as! SortShoppingListVC
            objSortShoppingListVC.currentSelectedSortOption = self.currentSelectedSortOption
            objSortShoppingListVC.completionBlock = { sortOption in
                self.currentSelectedSortOption = sortOption
                self.getShopListApiCall {
                    self.setupKWStepper()
                }
            }
            
            self.tabBarController?.present(objSortShoppingListVC, animated: true, completion: nil)
            
        }
        
    }
    
    func btnExpandCollapseTapped() {
        self.tblView.isHidden = !self.tblView.isHidden
        self.viewBackBlackAlpha.isHidden = !self.viewBackBlackAlpha.isHidden
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    
    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.backgroundColor = .clear
        self.webViewContainer.mk_addFourSideConstraintsInParent(childView: webView)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupLayout()
        self.setupKWStepper()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    func setupKWStepper(passedValue:Int=0) {
        
        self.kwStepper = KWStepper(decrementButton: self.btnLeftArrow, incrementButton: self.btnRightArrow)
        self.kwStepper.autoRepeat = true
        self.kwStepper.autoRepeatInterval = 0.10
        self.kwStepper.wraps = false
        self.kwStepper.minimumValue = 0
        
        var totalIngs = 0
        if self.currentSelectedSortOption == enum_sort_shopping_list.Recipe {
            for dict in arrRecipeSortIngredientList {
                if let arrIngs = dict["ingredient_list"] as? [Ingredient] {
                    totalIngs = totalIngs + arrIngs.count
                }
            }
            self.kwStepper.maximumValue = Double(totalIngs - 1)
        } else {
            self.kwStepper.maximumValue = Double(self.arrIngredientList.count - 1)
        }
        
        
        self.kwStepper.incrementStepValue = 1
        self.kwStepper.decrementStepValue = 1
        
        self.kwStepper.delegate = self
        
        self.kwStepper.value = Double(passedValue)
        
        self.stepperTapped(stepper: self.kwStepper)
        
        self.kwStepper.valueChangedCallback = { stepper in
            self.stepperTapped(stepper: stepper)
        }
    }
    
    func stepperTapped(stepper:KWStepper) {
        hapSoft()
        
        if stepper.value == 0 {
            self.btnLeftArrow.alpha = 0.5
            self.btnLeftArrow.isEnabled = false
        } else {
            self.btnLeftArrow.alpha = 1.0
            self.btnLeftArrow.isEnabled = true
        }
        
        if stepper.value == stepper.maximumValue {
            self.btnRightArrow.alpha = 0.5
            self.btnRightArrow.isEnabled = false
        } else {
            self.btnRightArrow.alpha = 1.0
            self.btnRightArrow.isEnabled = true
        }
        
        let currentIng = self.getCurrentIng(stepper: self.kwStepper)
        self.loadBrowserWithIng(ing: currentIng)
        
    }
 
    func getCurrentIng(stepper:KWStepper) -> Ingredient {
        return getIng(index: Int(stepper.value))
    }
    
    func getIng(index:Int) -> Ingredient {
        var arrIngs = [Ingredient]()
        if self.currentSelectedSortOption == enum_sort_shopping_list.Recipe {
            for dict in self.arrRecipeSortIngredientList {
                if let ings = dict["ingredient_list"] as? [Ingredient] {
                    arrIngs.append(contentsOf: ings)
                }
            }
            return arrIngs[index]
        } else {
            let currentIng = self.arrIngredientList[index]
            return currentIng
        }
    }
    
    func loadBrowserWithIng(ing:Ingredient) {
        
        let ingName = ing.name ?? ""
        self.lblIngName.text = ingName
        let qty = ing.qty ?? ""
        let qtyUnit = ing.unit_name ?? ""
        self.lblIngQty.text = qty + " " + qtyUnit

        let urlString = currentBuyNowType.getUrl() + (ingName)
        let spaced = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

        let myURL = URL(string: spaced)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }
    
}

extension ShoppingCartWebViewVC : KWStepperDelegate {
    func KWStepperDidIncrement() {
        let ing = self.getIng(index: Int(self.kwStepper.value - 1))
        self.btnRightArrow.loadingIndicator(true)
        Timer.after(1.5) {
            self.btnRightArrow.loadingIndicator(false)
            if self.kwStepper.value == self.kwStepper.maximumValue {
                self.btnRightArrow.alpha = 0.5
                self.btnRightArrow.isEnabled = false
            } else {
                self.btnRightArrow.alpha = 1.0
                self.btnRightArrow.isEnabled = true
            }
        }
        makeStrikeThroughIngApiCall(vc: self, objIng: ing, is_strike: true.mk_intValue)
    }
    
}

extension ShoppingCartWebViewVC : UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        self.viewLoaderWebView.mk_animateAlpha(sec: 0.3, alpha: 1.0)
        
        if tabBar.selectedItem == tabBar.items?.first {
            self.currentBuyNowType = .redmart
        }
        if tabBar.selectedItem == tabBar.items?.last {
            self.currentBuyNowType = .fairprice
        }
        let currentIng = self.getCurrentIng(stepper: self.kwStepper)
        self.loadBrowserWithIng(ing: currentIng)
        hapHeavy()
    }
    
}


extension ShoppingCartWebViewVC : WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.viewLoaderWebView.mk_animateAlpha(sec: 0.3, alpha: 0.0)
    }
    
}



//------------------------------------------
//MARK: - Table View Setup Methods -
extension ShoppingCartWebViewVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
        self.tblView.estimatedSectionHeaderHeight = 100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.currentSelectedSortOption == enum_sort_shopping_list.Recipe {
            
            let cellHeader = tableView.dequeueReusableCell(withIdentifier: "CellShoppingListItemHeader") as! CellShoppingListItem
            
            cellHeader.lblHeader.text = self.arrRecipeSortIngredientList[section]["recipe_name"] as? String
            
            return cellHeader
            
        } else {
            return nil
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.currentSelectedSortOption == enum_sort_shopping_list.Recipe {
            return self.arrRecipeSortIngredientList.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.currentSelectedSortOption == enum_sort_shopping_list.Recipe {
            
            return (self.arrRecipeSortIngredientList[section]["ingredient_list"] as? [Ingredient])?.count ?? 0
            
        } else {
            return self.arrIngredientList.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellShoppingListItem = tableView.dequeueReusableCell(withIdentifier: "CellShoppingListItem", for: indexPath) as! CellShoppingListItem

        cellShoppingListItem.handleCell(vc: self, ip: indexPath)
        return cellShoppingListItem
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var arrIngs = [Ingredient]()
        if self.currentSelectedSortOption == enum_sort_shopping_list.Recipe {
            for dict in self.arrRecipeSortIngredientList {
                if let ings = dict["ingredient_list"] as? [Ingredient] {
                    arrIngs.append(contentsOf: ings)
                }
            }
            for (i,ing) in arrIngs.enumerated() {
                if let arrIngsOnCell = (self.arrRecipeSortIngredientList[indexPath.section]["ingredient_list"] as? [Ingredient]) {
                    if arrIngsOnCell[indexPath.row].id == ing.id {
                        self.setupKWStepper(passedValue: i)
                    }
                }
            }
            
        } else {
            self.setupKWStepper(passedValue: indexPath.row)
        }
        
        self.btnExpandCollapseTapped()
        
    }
}
//
////MARK: - API CALLS
//extension ShoppingListVC {
//    func forgotPasswordApiCall(cellForgotPass:CellForgotPass) {
//
//        cellForgotPass.btnSend.loadingIndicator(show: true)
//        forgotPassword(email: cellForgotPass.txtEmail.text ?? "") { (res) in
//
//            switch res {
//            case .success(message: let msg):
//
//                BasicFunctions.showPopupOneButton(title: "Forgot Password".localize(), desc: msg) {
//                    hapHeavy()
//                    UIViewController.CVC()?.navigationController?.popViewController(animated: true)
//                }
//
//                break
//            case .failed(message: let msg):
//                BasicFunctions.showError(strError: msg)
//                break
//            case .error(error: let error):
//                BasicFunctions.showError(strError: error.localizedDescription)
//                break
//            }
//
//            cellForgotPass.btnSend.loadingIndicator(show: false)
//        }
//    }
//}



extension ShoppingCartWebViewVC {
    
    func getShopListApiCall(compHandler: @escaping ()->()) {
        
        getShopList(shopping_list_sort_type: self.currentSelectedSortOption.rawValue, start: 0, limit: 200) { (res) in
            
            
            
            switch res {
            case .success(dictData: let data, message: let str) :
                print(data)
                
                self.arrRecipeSortIngredientList.removeAll()
                self.arrIngredientList.removeAll()
                self.tblView.reloadData()
                
                if let arrShoppingList = data["shopping_list"] as? [[String:Any]] {
                    for shoppingList in arrShoppingList {
                        
                        if let recipe_id = shoppingList["recipe_id"] as? Int64 {
                            var dictRecipe = [String:Any]()
                            dictRecipe["recipe_id"] = recipe_id
                            dictRecipe["recipe_name"] = (shoppingList["recipe_name"] as? String) ?? ""
                            
                            var arrIngredient = [Ingredient]()
                            if let ingredient_list = shoppingList["ingredient_list"] as? [[String:Any]] {
                                for ing in ingredient_list {
                                    let objIngredient = Ingredient.mr_import(from: ing, in: app_default_context)
                                    objIngredient.unit_name = ing["unit"] as? String
                                    arrIngredient.append(objIngredient)
                                }
                            }
                            dictRecipe["ingredient_list"] = arrIngredient
                            self.arrRecipeSortIngredientList.append(dictRecipe)
                        } else {
                            
                            let objIngredient = Ingredient.mr_import(from: shoppingList, in: app_default_context)
                            objIngredient.unit_name = shoppingList["unit"] as? String
                            self.arrIngredientList.append(objIngredient)
                        }
                    }
                }
                

                self.tblView.reloadData()
                
                break
            case .error(error: let err) :
                print(err)
                BasicFunctions.showError(strError: err)
                
                self.arrIngredientList.removeAll()
                self.arrRecipeSortIngredientList.removeAll()
                
                self.tblView.reloadData()
                
                break
            case .failed(message: let msg) :
                BasicFunctions.showError(strError: msg)
                
                self.arrIngredientList.removeAll()
                self.arrRecipeSortIngredientList.removeAll()
                
                self.tblView.reloadData()
                
                break
            }
            
            compHandler()
        }
    }
}
