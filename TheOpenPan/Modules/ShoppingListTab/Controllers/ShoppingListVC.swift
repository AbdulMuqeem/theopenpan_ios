//
//  ShoppingListVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/13/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import DZNEmptyDataSet

class ShoppingListVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet weak var tblView                  : UITableView!
    @IBOutlet var btnClearAll                   : UIButton!
    @IBOutlet var btnClearAllTickedIngs         : UIButton!
    @IBOutlet var btnAddNewIng                  : UIButton!
    @IBOutlet var btnBuyNow                     : UIButton!
    
    @IBOutlet var btnRedMart                    : UIButton!
    @IBOutlet var btnFairPrice                  : UIButton!
    @IBOutlet var viewBackBuyNowButtons         : UIView!
    
    @IBOutlet weak var searchTextField          : UITextField!
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    var arrIngredientList                     = [Ingredient]()
    var arrIngredientListSearch               = [Ingredient]()
    
    var arrRecipeSortIngredientList           = [[String:Any]]()
    var arrRecipeSortIngredientListSearch     = [[String:Any]]()
    
    var currentSelectedSortOption             : enum_sort_shopping_list! = .MostRecent
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.btnRedMart.superview?.isHidden = true
        self.viewBackBuyNowButtons.isHidden = true
        
        self.automaticallyAdjustsScrollViewInsets = false
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        self.navigationItem.title = "Shopping List".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
        
        
        self.setupTableView()
        
        self.btnAddNewIng.mk_addTapHandler { (btn) in
            hapSoft()
            self.btnAddNewIngredientTapped(btn: btn)
        }
        
        self.btnClearAll.mk_addTapHandler { (btn) in
            hapSoft()
            BasicFunctions.showPopupTwoButton(title: "Clear All".localize(), desc: "Are you sure you want to clear all ingredients?".localize(), btnTitleLeft: "CANCEL".localize(), btnTitleRight: "YES".localize(), baseVC: self) { (isOkay) in
                if isOkay {
                    clearAllIngredientsShoppingList { (res) in
                        
                        switch res {
                        case .success(dictData: let data, message: let str) :
                            BasicFunctions.showInfo(strError: str)
                            self.getShopListApiCall()
                            break
                        case .error(error: let err) :
                            print(err)
                            BasicFunctions.showError(strError: err)
                            break
                        case .failed(message: let msg) :
                            BasicFunctions.showError(strError: msg)
                            break
                        }
                    }
                }
            }
        }
        
        self.btnClearAllTickedIngs.onTap {
            
            if self.arrIngredientList.count == 0 {
                self.btnClearAllTickedIngs.shake()
                hapError()
                return
            }
            
            if (self.arrIngredientList.filter { (objIng) -> Bool in
                return objIng.is_bought
            }).count == 0 {
                self.btnClearAllTickedIngs.shake()
                hapError()
                return
            }
            
            hapSoft()
            BasicFunctions.showPopupTwoButton(title: "Clear All Ticked Ingredients".localize(), desc: "Are you sure you want to clear all ticked ingredients?".localize(), btnTitleLeft: "CANCEL".localize(), btnTitleRight: "YES".localize(), baseVC: self) { (isOkay) in
                if isOkay {
                    clearAllTickedIngredientsShoppingList { (res) in

                        switch res {
                        case .success(dictData: let data, message: let str) :
//                            BasicFunctions.showInfo(strError: str)
                            self.getShopListApiCall()
                            break
                        case .error(error: let err) :
                            print(err)
                            BasicFunctions.showError(strError: err)
                            break
                        case .failed(message: let msg) :
                            BasicFunctions.showError(strError: msg)
                            break
                        }
                    }
                }
            }
        }
        
        
        func hideShowButtons() {
            self.btnRedMart.superview?.isHidden = !(self.btnRedMart.superview?.isHidden ?? false)
            self.viewBackBuyNowButtons.isHidden = !(self.viewBackBuyNowButtons.isHidden)
        }
        
        func pushShoppingCartWebViewVC(type:enum_buy_now_type) {
            apde.showLoader()
            getShopListMostRecentIngsApiCall { (arrIngs) in
                apde.hideLoader()
                hideShowButtons()
                let objShoppingCartWebViewVC = apde.getController(vc_name: "ShoppingCartWebViewVC", sb: .ShoppingListTab) as! ShoppingCartWebViewVC
                objShoppingCartWebViewVC.currentBuyNowType = type
                objShoppingCartWebViewVC.arrIngredientList = arrIngs
                self.navigationController?.pushViewController(objShoppingCartWebViewVC, animated: true)
            }
            
        }
        
        self.btnBuyNow.mk_addTapHandler { (btn) in
//            hideShowButtons()
            BasicFunctions.showInfo(strError: "Buy from your favourite grocers, coming soon!!")
        }
        
        self.btnRedMart.onTap {
            pushShoppingCartWebViewVC(type: .redmart)
        }
        
        self.btnFairPrice.onTap {
            pushShoppingCartWebViewVC(type: .fairprice)
        }
        
        
        self.searchTextField.onChange { (text) in
            self.filterSearchResult(text: text)
        }
        
        self.addRightBarButtonItem(img: UIImage.init(named: "AssetSortBarButtonIcon"), btnTitle: nil) { (barButton) in
            
            hapSoft()
            
            let objSortShoppingListVC = apde.getController(vc_name: "SortShoppingListVC", sb: .ShoppingListTab) as! SortShoppingListVC
            objSortShoppingListVC.currentSelectedSortOption = self.currentSelectedSortOption
            objSortShoppingListVC.completionBlock = { sortOption in
                self.currentSelectedSortOption = sortOption
                self.getShopListApiCall()
            }
            
            self.tabBarController?.present(objSortShoppingListVC, animated: true, completion: nil)
        }

        
        
    }
    
    
    
    func getShopListMostRecentIngsApiCall(compHandler: @escaping ([Ingredient])->()) {
        
        getShopList(shopping_list_sort_type: enum_sort_shopping_list.MostRecent.rawValue, start: 0, limit: 200) { (res) in
            
            var arrIngs = [Ingredient]()
            
            switch res {
            case .success(dictData: let data, message: _) :
                print(data)
                
                if let arrShoppingList = data["shopping_list"] as? [[String:Any]] {
                    for shoppingList in arrShoppingList {
                        let objIngredient = Ingredient.mr_import(from: shoppingList, in: app_default_context)
                        objIngredient.unit_name = shoppingList["unit"] as? String
                        arrIngs.append(objIngredient)
                    }
                }
                compHandler(arrIngs)
                
                break
            case .error(error: let err) :
                print(err)
                BasicFunctions.showError(strError: err)
                compHandler(arrIngs)
                break
            case .failed(message: let msg) :
                BasicFunctions.showError(strError: msg)
                compHandler(arrIngs)
                break
            }
            
        }
    }
    
    func getShopListApiCall() {
        
        getShopList(shopping_list_sort_type: self.currentSelectedSortOption.rawValue, start: 0, limit: 200) { (res) in
            
            switch res {
            case .success(dictData: let data, message: let str) :
                print(data)
                
                self.arrRecipeSortIngredientList.removeAll()
                self.arrRecipeSortIngredientListSearch.removeAll()
                self.arrIngredientList.removeAll()
                self.arrIngredientListSearch.removeAll()
                self.tblView.reloadData()
                
                if let arrShoppingList = data["shopping_list"] as? [[String:Any]] {
                    for shoppingList in arrShoppingList {
                        
                        if let recipe_id = shoppingList["recipe_id"] as? Int64 {
                            var dictRecipe = [String:Any]()
                            dictRecipe["recipe_id"] = recipe_id
                            dictRecipe["recipe_name"] = (shoppingList["recipe_name"] as? String) ?? ""
                            
                            var arrIngredient = [Ingredient]()
                            if let ingredient_list = shoppingList["ingredient_list"] as? [[String:Any]] {
                                for ing in ingredient_list {
                                    let objIngredient = Ingredient.mr_import(from: ing, in: app_default_context)
                                    objIngredient.unit_name = ing["unit"] as? String
                                    arrIngredient.append(objIngredient)
                                }
                            }
                            dictRecipe["ingredient_list"] = arrIngredient
                            self.arrRecipeSortIngredientList.append(dictRecipe)
                        } else {
                            
                            let objIngredient = Ingredient.mr_import(from: shoppingList, in: app_default_context)
                            objIngredient.unit_name = shoppingList["unit"] as? String
                            self.arrIngredientList.append(objIngredient)
                        }
                    }
                }
                
                if self.searchTextField.isEmpty() == false {
                    self.filterSearchResult(text: self.searchTextField.text ?? "")
                }
                self.tblView.reloadData()
                
                break
            case .error(error: let err) :
                print(err)
                BasicFunctions.showError(strError: err)
                
                self.arrIngredientListSearch.removeAll()
                self.arrRecipeSortIngredientListSearch.removeAll()
                
                self.arrIngredientList.removeAll()
                self.arrRecipeSortIngredientList.removeAll()
                
                self.tblView.reloadData()
                
                break
            case .failed(message: let msg) :
                BasicFunctions.showError(strError: msg)
                
                self.arrIngredientListSearch.removeAll()
                self.arrRecipeSortIngredientListSearch.removeAll()
                
                self.arrIngredientList.removeAll()
                self.arrRecipeSortIngredientList.removeAll()
                
                self.tblView.reloadData()
                
                break
            }
            
        }
    }
    
    
    func filterSearchResult(text:String) {
        
        self.arrIngredientListSearch = self.arrIngredientList.filter { (objarrIngredientListSearch) -> Bool in
            objarrIngredientListSearch.name?.lowercased().contains(text.lowercased()) ?? false
        }
        
        var filteredArrRecipeSortIngredientListSearch = [[String:Any]]()
        
        for (_,sectionRecipe) in self.arrRecipeSortIngredientList.enumerated() {
            
            var newSectionRecipe = [String:Any]()
            newSectionRecipe = sectionRecipe
            var newIngs = [Ingredient]()
            if let arrIngs = sectionRecipe["ingredient_list"] as? [Ingredient] {
                for ing in arrIngs {
                    if ing.name?.lowercased().contains(text.lowercased()) ?? false {
                        newIngs.append(ing)
                    }
                }
                
                newSectionRecipe["ingredient_list"] = newIngs
            }
            if newIngs.count > 0 {
                filteredArrRecipeSortIngredientListSearch.append(newSectionRecipe)
            }
            
        }
        
        self.arrRecipeSortIngredientListSearch = filteredArrRecipeSortIngredientListSearch
        
        self.tblView.reloadData()
    }
    
    
    func btnAddNewIngredientTapped(btn:UIButton) {

        self.view.endEditing(true)

        if let arrUnitOption = Unit_option.mr_findAllSorted(by: "displayIndex", ascending: true, in: app_default_context) as? [Unit_option] {

            //Object to edit
            let objIngredints = Ingredient.mr_createEntity(in: app_default_context)

            var mutArrUnitOption = arrUnitOption
            mutArrUnitOption.sort { (u1, u2) -> Bool in
                (u1.tag_name_singular ?? "").localizedCompare((u1.tag_name_singular ?? "")) == .orderedAscending
            }

            var arrDictTags = [[String:Any]]()
            for unit_option in mutArrUnitOption {
                var dict = [String:Any]()
                dict["name"] = unit_option.tag_name_singular ?? ""
                arrDictTags.append(dict)
            }

            let objEditIngredientVC = apde.getController(vc_name: "EditIngredientVC", sb: .AddRecipe) as! EditIngredientVC
            objEditIngredientVC.arrDictTags = arrDictTags
            objEditIngredientVC.strTitle = "Ingredient Units".localize()
            objEditIngredientVC.strRemoveButtonTitle = "CANCEL".localize()
            objEditIngredientVC.objIngredints = objIngredints
            objEditIngredientVC.isFromShopListTab = true
            
            objEditIngredientVC.completionBlockDone = { objUpdatedIngredient in

                print(objUpdatedIngredient)
                let id          = findAndGetUnitId(unitName: objUpdatedIngredient.unit_name ?? "")
                var unit_id     : Int64?
//                if id != "" {
//                    unit_id = id
//                }
                addOREditOtherIngToShopList(shoping_list_item_id: nil, name: objUpdatedIngredient.name ?? "", qty: objUpdatedIngredient.qty ?? "", unit: objUpdatedIngredient.unit_name ?? "", unit_id : unit_id, note: objUpdatedIngredient.note ?? "") { (res) in
                    
                    
                    switch res {
                    case .success(_, message: let str) :
                        BasicFunctions.showInfo(strError: str)
                        
                        self.getShopListApiCall()
                        
                        break
                    case .error(error: let err) :
                        print(err)
                        BasicFunctions.showError(strError: err)
                        break
                    case .failed(message: let msg) :
                        BasicFunctions.showError(strError: msg)
                        break
                    }
                    
                }


            }
            
            objEditIngredientVC.completionBlockRemove = {
                hapHeavy()
            }

            if #available(iOS 13.0, *) {
            } else {
                objEditIngredientVC.modalPresentationStyle = .overCurrentContext
            }

            self.tabBarController?.present(objEditIngredientVC, animated: true, completion: nil)
        }
    }
    
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupLayout()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeTabBar(hidden: false, animated: false, completion: nil)
        self.getShopListApiCall()
    }
    
}


//------------------------------------------
//MARK: - Table View Setup Methods -
extension ShoppingListVC : UITableViewDelegate, UITableViewDataSource , DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    
    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
        self.tblView.estimatedSectionHeaderHeight = 100
        self.tblView.emptyDataSetSource = self
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return "Shopping list is empty.".AttributedStringFont(enum_font.medium.font(16), fontColor: #colorLiteral(red: 0.7450980392, green: 0.6823529412, blue: 0.6549019608, alpha: 1))
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if self.currentSelectedSortOption == enum_sort_shopping_list.Recipe {
            
            let cellHeader = tableView.dequeueReusableCell(withIdentifier: "CellShoppingListItemHeader") as! CellShoppingListItem
            
            if searchTextField.isEmpty() {
                cellHeader.lblHeader.text = self.arrRecipeSortIngredientList[section]["recipe_name"] as? String
            } else {
                cellHeader.lblHeader.text = self.arrRecipeSortIngredientListSearch[section]["recipe_name"] as? String
            }
            
            return cellHeader
            
        } else {
            return nil
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.currentSelectedSortOption == enum_sort_shopping_list.Recipe {
            if searchTextField.isEmpty() {
                return self.arrRecipeSortIngredientList.count
            }
            return self.arrRecipeSortIngredientListSearch.count
        } else {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.currentSelectedSortOption == enum_sort_shopping_list.Recipe {
            if searchTextField.isEmpty() {
                return (self.arrRecipeSortIngredientList[section]["ingredient_list"] as? [Ingredient])?.count ?? 0
            } else {
                return (self.arrRecipeSortIngredientListSearch[section]["ingredient_list"] as? [Ingredient])?.count ?? 0
            }
            
        } else {
            if searchTextField.isEmpty() {
                return self.arrIngredientList.count
            }
            return self.arrIngredientListSearch.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellShoppingListItem = tableView.dequeueReusableCell(withIdentifier: "CellShoppingListItem", for: indexPath) as! CellShoppingListItem
        
        cellShoppingListItem.handleCell(vc: self, ip: indexPath)
        
        return cellShoppingListItem
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

//
////MARK: - API CALLS
//extension ShoppingListVC {
//    func forgotPasswordApiCall(cellForgotPass:CellForgotPass) {
//
//        cellForgotPass.btnSend.loadingIndicator(show: true)
//        forgotPassword(email: cellForgotPass.txtEmail.text ?? "") { (res) in
//
//            switch res {
//            case .success(message: let msg):
//
//                BasicFunctions.showPopupOneButton(title: "Forgot Password".localize(), desc: msg) {
//                    hapHeavy()
//                    UIViewController.CVC()?.navigationController?.popViewController(animated: true)
//                }
//
//                break
//            case .failed(message: let msg):
//                BasicFunctions.showError(strError: msg)
//                break
//            case .error(error: let error):
//                BasicFunctions.showError(strError: error.localizedDescription)
//                break
//            }
//
//            cellForgotPass.btnSend.loadingIndicator(show: false)
//        }
//    }
//}
