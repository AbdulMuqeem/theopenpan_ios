//
//  CellShoppingListItem.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/13/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class CellShoppingListItem: UITableViewCell {
    
    @IBOutlet var btnIngredientCheckBox                 : UIButton!
    @IBOutlet var lblIngredientQty                      : UILabel!
    @IBOutlet var lblIngredientName                     : UILabel!
    @IBOutlet var btnEditIng                            : UIButton!
    @IBOutlet var lblHeader                             : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func handleCell(vc:UIViewController,ip:ip) {
        
        //        ShoppingListVC
        
        var objIng : Ingredient!
        
        if let objShoppingListVC = vc as? ShoppingListVC {
            
            if objShoppingListVC.currentSelectedSortOption == enum_sort_shopping_list.Recipe {
                if objShoppingListVC.searchTextField.isEmpty() {
                    if let arr = objShoppingListVC.arrRecipeSortIngredientList[ip.section]["ingredient_list"] as? [Ingredient] {
                        objIng = arr[ip.row]
                    }
                } else {
                    if let arr = objShoppingListVC.arrRecipeSortIngredientListSearch[ip.section]["ingredient_list"] as? [Ingredient] {
                        objIng = arr[ip.row]
                    }
                }
            } else {
                if objShoppingListVC.searchTextField.isEmpty() {
                    objIng = objShoppingListVC.arrIngredientList[ip.row]
                } else {
                    objIng = objShoppingListVC.arrIngredientListSearch[ip.row]
                }
            }
            
        }
        
        if let objShoppingCartWebViewVC = vc as? ShoppingCartWebViewVC {
            if objShoppingCartWebViewVC.currentSelectedSortOption == enum_sort_shopping_list.Recipe {
                if let arr = objShoppingCartWebViewVC.arrRecipeSortIngredientList[ip.section]["ingredient_list"] as? [Ingredient] {
                    objIng = arr[ip.row]
                }
            } else {
                objIng = objShoppingCartWebViewVC.arrIngredientList[ip.row]
            }
        }
        
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "YourStringHere")
        attributeString.removeAttribute(NSAttributedString.Key.strikethroughStyle, range: NSMakeRange(0, attributeString.length))
        self.lblIngredientQty.attributedText = attributeString
        
        let attributeString2: NSMutableAttributedString =  NSMutableAttributedString(string: "YourStringHere")
        attributeString2.removeAttribute(NSAttributedString.Key.strikethroughStyle, range: NSMakeRange(0, attributeString2.length))
        self.lblIngredientName.attributedText = attributeString2
        
        self.lblIngredientQty.text = objIng.qty ?? ""
        self.lblIngredientName.text = (objIng.unit_name ?? "") + " " + (objIng.name ?? "")
        
        if objIng.is_bought {
            self.btnEditIng.isHidden = true
            self.lblIngredientQty.strikeThrough(objIng.is_bought)
            self.lblIngredientName.strikeThrough(objIng.is_bought)
        } else {
            self.btnEditIng.isHidden = false
            self.lblIngredientQty.strikeThroughRemove(newText: objIng.qty ?? "")
            self.lblIngredientName.strikeThroughRemove(newText: (objIng.unit_name ?? "") + " "  + (objIng.name ?? ""))
        }
        
        
        self.btnIngredientCheckBox.isSelected = objIng.is_bought
        
        self.btnIngredientCheckBox.mk_addTapHandler { (btn) in
            hapSoft()
            makeStrikeThroughIngApiCall(vc:vc, objIng: objIng, is_strike: (!objIng.is_bought).mk_intValue)
        }
        
        self.btnEditIng?.mk_addTapHandler(action: { (btn) in
            
            if objIng.is_multiple {
                BasicFunctions.showInfo(strError: "Ingredient in multiple recipes. Please sort ingredients by \"recipe\" to edit.")
            } else {
                
                if let arrUnitOption = Unit_option.mr_findAllSorted(by: "displayIndex", ascending: true, in: app_default_context) as? [Unit_option] {
                    
                    var mutArrUnitOption = arrUnitOption
                    mutArrUnitOption.sort { (u1, u2) -> Bool in
                        (u1.tag_name_singular ?? "").localizedCompare((u1.tag_name_singular ?? "")) == .orderedAscending
                    }
                    
                    var arrDictTags = [[String:Any]]()
                    for unit_option in mutArrUnitOption {
                        var dict = [String:Any]()
                        dict["name"] = unit_option.tag_name_singular ?? ""
                        arrDictTags.append(dict)
                    }
                    
                    let objEditIngredientVC = apde.getController(vc_name: "EditIngredientVC", sb: .AddRecipe) as! EditIngredientVC
                    objEditIngredientVC.arrDictTags             = arrDictTags
                    objEditIngredientVC.strTitle                = "Ingredient Units".localize()
                    objEditIngredientVC.strRemoveButtonTitle    = "REMOVE".localize()
                    objEditIngredientVC.isFromShopListTab       = true
                    
                    if let objShoppingListVC = vc as? ShoppingListVC {
                        if objShoppingListVC.currentSelectedSortOption == enum_sort_shopping_list.Recipe {
                            if objShoppingListVC.searchTextField.isEmpty() {
                                if let arr = objShoppingListVC.arrRecipeSortIngredientList[ip.section]["ingredient_list"] as? [Ingredient] {
                                    objIng = arr[ip.row]
                                    objEditIngredientVC.strRecipeName = (objShoppingListVC.arrRecipeSortIngredientList[ip.section]["recipe_name"] as? String) ?? ""
                                }
                            } else {
                                if let arr = objShoppingListVC.arrRecipeSortIngredientListSearch[ip.section]["ingredient_list"] as? [Ingredient] {
                                    objIng = arr[ip.row]
                                    objEditIngredientVC.strRecipeName = (objShoppingListVC.arrRecipeSortIngredientListSearch[ip.section]["recipe_name"] as? String) ?? ""
                                }
                            }
                        }
                    }
                    
                    if let objShoppingCartWebViewVC = vc as? ShoppingCartWebViewVC {
                        if objShoppingCartWebViewVC.currentSelectedSortOption == enum_sort_shopping_list.Recipe {
                            if let arr = objShoppingCartWebViewVC.arrRecipeSortIngredientList[ip.section]["ingredient_list"] as? [Ingredient] {
                                objIng = arr[ip.row]
                                objEditIngredientVC.strRecipeName = (objShoppingCartWebViewVC.arrRecipeSortIngredientList[ip.section]["recipe_name"] as? String) ?? ""
                            }
                        }
                    }
                    
                    objEditIngredientVC.objIngredints = objIng
                    objEditIngredientVC.completionBlockDone = { objUpdatedIngredient in
                        
                        print(objUpdatedIngredient)
                        
                        let id = findAndGetUnitId(unitName: objUpdatedIngredient.unit_name ?? "")
                        var unit_id : Int64?
//                        if id != "" {
                            unit_id = id
//                        }
                        
                        addOREditOtherIngToShopList(shoping_list_item_id: objIng.shopping_list_item_id ?? "", name: objUpdatedIngredient.name ?? "", qty: objUpdatedIngredient.qty ?? "", unit: objUpdatedIngredient.unit_name ?? "", unit_id:unit_id, note: objUpdatedIngredient.note ?? "") { (res) in
                            
                            
                            switch res {
                            case .success(dictData: _, message: let str) :
                                BasicFunctions.showInfo(strError: str)
                                
                                (vc as? ShoppingListVC)?.getShopListApiCall()
                                (vc as? ShoppingCartWebViewVC)?.getShopListApiCall(compHandler: {
                                    //                                    (vc as? ShoppingCartWebViewVC)?.setupKWStepper()
                                })
                                
                                break
                            case .error(error: let err) :
                                print(err)
                                BasicFunctions.showError(strError: err)
                                break
                            case .failed(message: let msg) :
                                BasicFunctions.showError(strError: msg)
                                break
                            }
                            
                        }
                        
                    }
                    objEditIngredientVC.completionBlockRemove = {
                        
                        hapHeavy()
                        
                        removeIngFromShopList(shoping_list_item_id: objIng.shopping_list_item_id ?? "") { (res) in
                            switch res {
                            case .success(dictData: _, message: let str) :
                                
                                BasicFunctions.showInfo(strError: str)
                                (vc as? ShoppingListVC)?.getShopListApiCall()
                                (vc as? ShoppingCartWebViewVC)?.getShopListApiCall(compHandler: {
                                    (vc as? ShoppingCartWebViewVC)?.setupKWStepper()
                                })
                                
                                break
                            case .error(error: let err) :
                                print(err)
                                BasicFunctions.showError(strError: err)
                                break
                            case .failed(message: let msg) :
                                BasicFunctions.showError(strError: msg)
                                break
                            }
                        }
                        
                    }
                    
                    
                    if #available(iOS 13.0, *) {
                    } else {
                        objEditIngredientVC.modalPresentationStyle = .overCurrentContext
                    }
                    
                    vc.tabBarController?.present(objEditIngredientVC, animated: true, completion: nil)
                }
            }
            
        })
        
    }
    
    
    
    func handleCellFromWebViewVC(vc:ShoppingCartWebViewVC,ip:ip) {
        self.btnIngredientCheckBox.mk_addTapHandler { (btn) in
            btn.isSelected = !btn.isSelected
            self.lblIngredientQty.strikeThrough(btn.isSelected)
            self.lblIngredientName.strikeThrough(btn.isSelected)
        }
    }
    
    
    
    
}



func makeStrikeThroughIngApiCall(vc:UIViewController, objIng:Ingredient, is_strike:Int) {
    
    makeStrikeThroughIng(shoping_list_item_id: objIng.shopping_list_item_id ?? "", is_strike: is_strike) { (res) in
        switch res {
        case .success(dictData: _, message: _) :
            
            (vc as? ShoppingListVC)?.getShopListApiCall()
            (vc as? ShoppingCartWebViewVC)?.getShopListApiCall(compHandler: {
                //                (vc as? ShoppingCartWebViewVC)?.setupKWStepper()
            })
            
            break
        case .error(error: let err) :
            print(err)
            BasicFunctions.showError(strError: err)
            break
        case .failed(message: let msg) :
            BasicFunctions.showError(strError: msg)
            break
        }
    }
}
