//
//  CellAddNewRecipeIngredients.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/20/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class CellAddNewRecipeIngredients: UITableViewCell {
    
    var currentVC : AddNewRecipeVC!
    
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblNoUnit: UILabel!
    @IBOutlet weak var lblIngredient: UILabel!
    @IBOutlet weak var btnRemoveIngredient: UIButton!
    
    @IBOutlet weak var btnNoUnit: UIButton!
    
    @IBOutlet weak var btnAddIngredientItem: UIButton!
    @IBOutlet weak var txtIngredientName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtIngredientUnit: SkyFloatingLabelTextField!
    @IBOutlet weak var txtIngredientQty: SkyFloatingLabelTextField!
    
    var unit_option : Unit_option!
    var arrSelected = [Int64]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)

        if editing {
            for view in subviews where view.description.contains("Reorder") {
                for case let subview as UIImageView in view.subviews {
                    subview.image = UIImage(named: "AssetSortIcon")?.withRenderingMode(.alwaysTemplate)
                    subview.tintColor = #colorLiteral(red: 0.337254902, green: 0.337254902, blue: 0.337254902, alpha: 0.75)
                    subview.frame.size = .init(width: 25, height: 30)
                }
            }
        }
    }
    
    func handleCellAddItem(vc:AddNewRecipeVC,indexPath:IndexPath) {
        
        self.currentVC = vc
        
        self.txtIngredientName?.addRightView(width: 30)
        
        vc.cellAddNewRecipeIngredients = self
        
        self.txtIngredientQty.mk_addAllEditingEventHandler { (txt) in
            if txt.text != "" {
                if isThisTextGreaterThenOne1(text: txt.text ?? "") {
                    let strPlural                       = getPluralString(self.txtIngredientUnit.text ?? "")
                    self.txtIngredientUnit.text         = strPlural
                } else {
                    let strSingular                     = getSingularString(self.txtIngredientUnit.text ?? "")
                    self.txtIngredientUnit.text         = strSingular
                }
            }
        }
        
        
        // txtCuisineType.shouldBeginEditing
        self.txtIngredientUnit.shouldBeginEditing { () -> Bool in
            
            self.currentVC.view.endEditing(true)
            
            if let arrUnitOption = Unit_option.mr_findAllSorted(by: "displayIndex", ascending: true, in: app_default_context) as? [Unit_option] {
                
                var mutArrUnitOption = arrUnitOption
                mutArrUnitOption.sort { (u1, u2) -> Bool in
                    (u1.tag_name_singular ?? "").localizedCompare((u1.tag_name_singular ?? "")) == .orderedAscending
                }
                
                var arrDictTags = [[String:Any]]()
                var arrFullDictTags = [[String:Any]]()
                for unit_option in mutArrUnitOption {
                    var dict = [String:Any]()
                    dict["name"] = unit_option.tag_name_singular ?? ""
                    arrDictTags.append(dict)
                    arrFullDictTags.append(dict)
                }
                
                if isThisTextGreaterThenOne1(text: self.txtIngredientQty.text ?? "") {
                    arrDictTags = removeSingular(true, arrUnitOption: arrFullDictTags)
                    for plural in arrPlurals {
                        var dict = [String:Any]()
                        dict["name"] = plural
                        arrDictTags.append(dict)
                    }
                }
                
                
                let objAddIngredientVC = apde.getController(vc_name: "AddIngredientVC", sb: .AddRecipe) as! AddIngredientVC
                objAddIngredientVC.arrDictTags = arrDictTags
                objAddIngredientVC.strTypeName = self.txtIngredientUnit.text ?? ""
                objAddIngredientVC.strTitle = "Ingredient Units".localize()
                objAddIngredientVC.completionBlock = { strTypeName in
                    self.txtIngredientUnit.text = strTypeName
                }
                if #available(iOS 13.0, *) {
                } else {
                    objAddIngredientVC.modalPresentationStyle = .overCurrentContext
                }
                
                UIViewController.CVC()?.present(objAddIngredientVC, animated: true, completion: nil)
            }
            
            return false
        }
        // txtCuisineType.shouldBeginEditing END--END--END--END
        
        self.btnAddIngredientItem.mk_addTapHandler { (btn) in
            
//            if self.txtIngredientQty.isEmpty() {
//                hapError()
//                self.txtIngredientQty.shake()
//                return
//            }
            
            if self.txtIngredientName.isEmpty() {
                hapError()
                self.txtIngredientName.shake()
                return
            }
            
            if let strIngredientName = self.txtIngredientName.text {
                if strIngredientName != "" {
                    let arrFound = vc.arrIngredints.filter { (ing) -> Bool in
                        (ing.name ?? "").lowercased() == strIngredientName.lowercased()
                    }
                    if let _ = arrFound.first {
                        BasicFunctions.showError(strError: "You can not add same ingredient again.")
                        return
                    }
                }
            }
            
            if let objIngredient = Ingredient.mr_createEntity(in: app_default_context) {
                
                self.btnAddIngredientItem.isEnabled = false
                hapSoft()
                
                objIngredient.name              = self.txtIngredientName.text ?? ""
                objIngredient.unit_name         = self.txtIngredientUnit.text ?? ""
                objIngredient.qty               = self.txtIngredientQty.text ?? ""
                
                vc.arrIngredints.append(objIngredient)
                vc.tblView.beginUpdates()
                vc.tblView.insertRows(at: [IndexPath(row: vc.arrIngredints.count-1, section: 1)], with: .left)
                vc.tblView.endUpdates()
                self.btnAddIngredientItem.isEnabled = true
                
                self.txtIngredientName.text = ""
                self.txtIngredientQty.text = ""
                self.unit_option = nil
                self.txtIngredientUnit.text = ""
            }
            
        }
        
    }
    
    func handleCell(vc:AddNewRecipeVC,indexPath:IndexPath) {
        
        self.currentVC = vc
        
        let objIngredient = vc.arrIngredints[indexPath.row]
        
        self.lblNoUnit?.text = ""
        self.lblIngredient.text = (objIngredient.unit_name ?? "") + " " + (objIngredient.name ?? "")

        
        self.lblNoUnit.isHidden = true
        
        self.lblQty.text = objIngredient.qty ?? ""
        self.btnRemoveIngredient.isHidden = true
        
        
        self.btnNoUnit.isHidden = true
        
//        self.btnNoUnit.mk_addTapHandler { (btn) in

//            guard let indexPath = vc.tblView.indexPath(for: self) else {
//                return
//            }
//
//            hapHeavy()
//            self.selectUnits(vc: vc, indexPath: indexPath, objIngredient: objIngredient)
//
//        }
        
        
    }
    
}


//            qt, gal, lb, stick, pinch, drop, dash, cup

//                    "tag_name": "cup"
//                    "tag_id": "cups",
            
//                    "tag_id": "stick",
//                    "tag_id": "sticks",
            
//                    "tag_id": "lb",
//                    "tag_id": "lbs",
            
//                    "tag_name": "dash"
//                    "tag_id": "dashes",
            
//                    "tag_id": "drop",
//                    "tag_id": "drops",
            
//                    "tag_name": "gal"
//                    "tag_id": "gals",
            
//                    "tag_id": "pinch",
//                    "tag_id": "pinches",
            
//                    "tag_id": "qt",
//                    "tag_id": "qts",



func removeSingular(_ removeSingularYes:Bool, arrUnitOption: Array<Dictionary<String,Any>>) -> [[String:Any]] {
    
    var mutArrUnitOption = arrUnitOption
    if removeSingularYes {
        
        var indexesToDelete = [Int]()
        for singular in arrSingulars {
            for (i,u) in mutArrUnitOption.enumerated() {
                if (u["name"] as? String ?? "") == singular {
                    indexesToDelete.append(i)
                    mutArrUnitOption.remove(at: i)
                    break
                }
            }
        }
        return mutArrUnitOption
        
    } else {
        
        var indexesToDelete = [Int]()
        for plural in arrPlurals {
            for (i,u) in mutArrUnitOption.enumerated() {
                if (u["name"] as? String ?? "") == plural {
                    indexesToDelete.append(i)
                    mutArrUnitOption.remove(at: i)
                    break
                }
            }
        }
        return mutArrUnitOption
    }
    
}

var arrSingulars    = ["cup","stick","lb","dash","drop","gal","pinch","qt"]
var arrPlurals      = ["cups","sticks","lbs","dashes","drops","gals","pinches","qts"]

func isSingular(_ str:String) -> Bool {
    return arrSingulars.contains(str)
}

func isPlural(_ str:String) -> Bool {
    return arrPlurals.contains(str)
}

func getSingularString(_ str:String) -> String {
    var isRecievedSingular = false
    isRecievedSingular = arrSingulars.contains(str)
    
    if isRecievedSingular {
        // returns as it is already singular
        return str
    } else {
        var isRecievedPlural = false
        isRecievedPlural = arrPlurals.contains(str)
        if isRecievedPlural {
            if let i = arrPlurals.firstIndex(of: str) {
                return arrSingulars[i]
            } else {
                return str
            }
        } else {
            // return simple unit (which have no plurals)
            return str
        }
    }
}


func getPluralString(_ str:String) -> String {
    var isRecievedPlural = false
    isRecievedPlural = arrPlurals.contains(str)
    
    if isRecievedPlural {
        // returns as it is already plural
        return str
    } else {
        var isRecievedSingular = false
        isRecievedSingular = arrSingulars.contains(str)
        if isRecievedSingular {
            if let i = arrSingulars.firstIndex(of: str) {
                return arrPlurals[i]
            } else {
                return str
            }
        } else {
            // return simple unit (which have no plurals)
            return str
        }
    }
}

func evaluateArithmeticStringExpression(_ expression: String?) -> NSNumber? {
    var calculatedResult: NSNumber? = nil
    let parsed = NSPredicate(format: "1.0 * \(expression ?? "") = 0")
    let left = (parsed as? NSComparisonPredicate)?.leftExpression
    calculatedResult = left?.expressionValue(with: nil, context: nil) as? NSNumber
    return calculatedResult
}
