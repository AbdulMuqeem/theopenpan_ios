//
//  CellAddNewRecipeTitleImage.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/20/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import AVFoundation
import AVKit
import CropViewController

class CellAddNewRecipeTitleImage: UITableViewCell {

    //------------------------------------------
    //MARK: - Class Variables -
    var imagePicker = ImagePicker()
    var currentVC : AddNewRecipeVC!
    
    @IBOutlet weak var textViewSourceUrl: UITextView!
    
    @IBOutlet weak var imgViewRecipeImage: UIImageView!
    @IBOutlet weak var btnChangePictureOrVideo: UIButton!
    @IBOutlet weak var btnVideoPlay: UIButton!
    
    @IBOutlet weak var txtTitle: SkyFloatingLabelTextField!
    
    @IBOutlet weak var stackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.btnVideoPlay?.isHidden = true
        setupImagePicker()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func handleCell(vc:AddNewRecipeVC,indexPath:IndexPath) {
        
        self.currentVC = vc
        
        self.textViewSourceUrl?.text = vc.addNewRecipeData.web_recipe_url
        
        if self.currentVC.addNewRecipeData.is_video.mk_boolValue {
            if self.currentVC.addNewRecipeData.video_thumb_url.contains("http") {
                self.imgViewRecipeImage.mk_afSetImage(urlString: self.currentVC.addNewRecipeData.video_thumb_url, defaultImage: nil, block: nil)
            } else {
                self.imgViewRecipeImage.image = UIImage.init(contentsOfFile: self.currentVC.addNewRecipeData.video_thumb_url)
            }
        } else {
            
            if self.currentVC.addNewRecipeData.media_url.contains("http") {
                self.imgViewRecipeImage.mk_afSetImage(urlString: self.currentVC.addNewRecipeData.media_url, defaultImage: nil, block: nil)
            } else {
                self.imgViewRecipeImage.image = UIImage.init(contentsOfFile: self.currentVC.addNewRecipeData.media_url)
            }
            
            
        }
        if vc.addNewRecipeData.title != "" {
            self.txtTitle.text = vc.addNewRecipeData.title
        }
        
        if vc.addNewRecipeData.is_own_recipe {
            self.txtTitle.isEnabled = true
            self.txtTitle.alpha = 1.0
        } else {
            self.txtTitle.isEnabled = false
            self.txtTitle.alpha = 0.75
        }
        
        btnChangePictureOrVideo.mk_addTapHandler { (btn) in
            
            hapSoft()
            
            let alertVC = UIAlertController.init(title: "Please choose".localize(), message: "", preferredStyle: .actionSheet)
            alertVC.addAction(UIAlertAction.init(title: "Photo Library".localize(), style: .default, handler: { (action) in
                self.imagePicker.photoGalleryAsscessRequest()
            }))
            alertVC.addAction(UIAlertAction.init(title: "Camera".localize(), style: .default, handler: { (action) in
                self.imagePicker.cameraAsscessRequest()
            }))
            
            if self.currentVC.addNewRecipeData.media_url != "" && (self.currentVC.addNewRecipeData.is_video.mk_boolValue == false) {
                alertVC.addAction(UIAlertAction.init(title: "Edit".localize(), style: .default, handler: { (action) in
                    if let img = self.currentVC.addNewRecipeData.image_temp_to_crop {
                        self.handleImageCrop(image: img)
                    }
                }))
            }

            alertVC.addAction(UIAlertAction.init(title: "Cancel".localize(), style: .cancel, handler: { (action) in
                
            }))
            
            alertVC.popoverPresentationController?.sourceView = self.btnChangePictureOrVideo
            vc.present(alertVC, animated: true, completion: nil)
        }
        
        self.txtTitle?.mk_addAllEditingEventHandler(action: { (txtField) in
            vc.addNewRecipeData.title = txtField.text ?? ""
        })

        self.btnVideoPlay?.isHidden = !vc.addNewRecipeData.is_video.mk_boolValue
        self.btnVideoPlay.mk_addTapHandler { (btn) in
            let videoURL = URL(string: vc.addNewRecipeData.media_url)
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            vc.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }

    
}


extension CellAddNewRecipeTitleImage: ImagePickerDelegate {
    
    func setupImagePicker() {
        
        imagePicker.delegate = self
        imagePicker.isVideoSupport = true
    }
    
    private func presentImagePicker(sourceType: UIImagePickerController.SourceType) {
        imagePicker.present(parent: self.currentVC, sourceType: sourceType)
    }

    func imagePickerDelegate(didSelect image: UIImage, delegatedForm: ImagePicker) {
        let compressedImage = image.mk_compressTo(5)
        self.currentVC.addNewRecipeData.image_temp_to_crop = compressedImage ?? image
        imagePicker.dismiss(animated: false)
        Timer.after(0.15) {
            self.handleImageCrop(image: compressedImage ?? image)
        }
        
    }
    
    func imagePickerDelegate(didCancel delegatedForm: ImagePicker) { imagePicker.dismiss() }

    func imagePickerDelegate(canUseGallery accessIsAllowed: Bool, delegatedForm: ImagePicker) {
        if accessIsAllowed { presentImagePicker(sourceType: .photoLibrary) }
    }

    func imagePickerDelegate(canUseCamera accessIsAllowed: Bool, delegatedForm: ImagePicker) {
        // works only on real device (crash on simulator)
        if accessIsAllowed { presentImagePicker(sourceType: .camera) }
    }
    
    func imagePickerDelegate(didSelect mediaUrl: URL, delegatedForm: ImagePicker) {
        print(mediaUrl)
        
        self.currentVC.addNewRecipeData.imported_media_url          = ""
        self.currentVC.addNewRecipeData.imported_video_thumb_url    = ""
        
        self.currentVC.tblView.beginUpdates()
        
        let thumbnailImage = Utilities.generateThumbnail(path: mediaUrl)
        if let filePath = thumbnailImage.mk_saveToDocuments(filename: String(Date().mk_ticks), folderName: tmpDocFolderName) {
            self.currentVC.addNewRecipeData.video_thumb_url = filePath
        }
        self.imgViewRecipeImage.image = thumbnailImage
        self.currentVC.addNewRecipeData.is_video = true.mk_intValue
        self.currentVC.addNewRecipeData.media_url = mediaUrl.absoluteString
        self.btnVideoPlay?.isHidden = !self.currentVC.addNewRecipeData.is_video.mk_boolValue
        
        self.currentVC.tblView.endUpdates()
        
        if let data = try? Data.init(contentsOf: mediaUrl.absoluteURL) {
            if let urlSavedVideo = data.mk_saveVideoFileToDocuments(filename: "\(Date().mk_ticks)", ext: mediaUrl.pathExtension, folderName: tmpDocFolderName) {
                self.currentVC.addNewRecipeData.media_url = urlSavedVideo
                imagePicker.dismiss()
            }
        }
        
    }
    
    
    func handleImageCrop(image:UIImage) {
        let cropViewController = CropViewController(croppingStyle: .default, image: image)
        cropViewController.aspectRatioPreset                = .presetSquare
        cropViewController.aspectRatioLockEnabled           = true
        cropViewController.resetButtonHidden                = true
        cropViewController.rotateButtonsHidden              = true
        cropViewController.rotateClockwiseButtonHidden      = true
        cropViewController.aspectRatioPickerButtonHidden    = true
        
        cropViewController.onDidCropToRect = { (imageCropped,rect,intValue) in
            if let filePath = imageCropped.mk_saveToDocuments(filename: String(Date().mk_ticks), folderName: tmpDocFolderName) {
                self.currentVC.addNewRecipeData.media_url = filePath
            }
            self.currentVC.addNewRecipeData.imported_media_url          = ""
            
            self.imgViewRecipeImage.image                           = imageCropped
            self.currentVC.addNewRecipeData.is_video                = false.mk_intValue
            self.currentVC.addNewRecipeData.video_thumb_url         = ""
            self.btnVideoPlay?.isHidden                             = !self.currentVC.addNewRecipeData.is_video.mk_boolValue
            cropViewController.dismiss(animated: true, completion: nil)
        }
        
        UIViewController.CVC()?.present(cropViewController, animated: true, completion: nil)
    }
    
    
}



//extension CellAddNewRecipeTitleImage : CropViewControllerDelegate {
//
//    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
//
//    }
//    func cropViewController(_ cropViewController: CropViewController, didCropImageToRect rect: CGRect, angle: Int) {
//
//    }
//    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
//
//    }
//}
