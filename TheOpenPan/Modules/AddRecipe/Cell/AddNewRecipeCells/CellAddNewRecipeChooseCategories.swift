//
//  CellAddNewRecipeChooseCategories.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/20/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import TagListView


enum enum_tag_list_type : String {
    
    case cusineType = "cusineType"
    case diet = "diet"
    case mealType = "mealType"
    
    func getRawName() -> String {
        switch self {
        case .cusineType:
            return "cuisine"
        case .diet:
            return "diet"
        case .mealType:
            return "meal"
            
        }
    }
    
    func getCatObject() -> Recipe_category? {
    //        if let objFoundCategory = Recipe_category.mr_findFirst(with: NSPredicate.init(format: "category_name contains[cd] %@", self.getRawName()), in: app_default_context) {
    //            return objFoundCategory.category_name ?? ""
    //        }
            
            if let cat = apde.ObjMasterData.categories.allObjects as? [Recipe_category] {
                let foundCat = cat.filter { (recCat) -> Bool in
                    (recCat.category_name?.lowercased().contains(self.getRawName())) ?? false
                }
                if let catFirst = foundCat.first {
                    return catFirst
                }
            }
            
            return nil
        }
    
    func getCategoryName() -> String {
//        if let objFoundCategory = Recipe_category.mr_findFirst(with: NSPredicate.init(format: "category_name contains[cd] %@", self.getRawName()), in: app_default_context) {
//            return objFoundCategory.category_name ?? ""
//        }
        
        if let cat = apde.ObjMasterData.categories.allObjects as? [Recipe_category] {
            let foundCat = cat.filter { (recCat) -> Bool in
                (recCat.category_name?.lowercased().contains(self.getRawName())) ?? false
            }
            if let catFirst = foundCat.first {
                return catFirst.category_name ?? ""
            }
        }
        
        return ""
    }
    
    func getAllTags() -> [Category_tag] {
//        if let objFoundCategory = Recipe_category.mr_findFirst(with: NSPredicate.init(format: "category_name contains[cd] %@", self.getRawName()), in: app_default_context) {
//            if let arrTags = objFoundCategory.tags.allObjects as? [Category_tag] {
//                return arrTags
//            }
//        }
//        return [Category_tag]()
        
        if let cat = apde.ObjMasterData.categories.allObjects as? [Recipe_category] {
            let foundCat = cat.filter { (recCat) -> Bool in
                (recCat.category_name?.lowercased().contains(self.getRawName())) ?? false
            }
            if let catFirst = foundCat.first {
                if let arrTags = catFirst.tags.allObjects as? [Category_tag] {
                    return arrTags.sorted { (c1, c2) -> Bool in
                        (c1.tag_name ?? "").localizedCompare((c2.tag_name ?? "")) == .orderedAscending
                    }
                }
            }
        }
        return [Category_tag]()
    }
    
}


class CellAddNewRecipeChooseCategories: UITableViewCell {
    
    @IBOutlet weak var txtCuisineType: SkyFloatingLabelTextField!
    @IBOutlet weak var tagListCuisineType: TagListView!
    
    @IBOutlet weak var txtDiet: SkyFloatingLabelTextField!
    @IBOutlet weak var tagListDiet: TagListView!
    
    @IBOutlet weak var txtMealType: SkyFloatingLabelTextField!
    @IBOutlet weak var tagListMealType: TagListView!
    
    @IBOutlet weak var txtKeywords: SkyFloatingLabelTextField!
    @IBOutlet weak var tagListKeywords: TagListView!
    
    @IBOutlet weak var btnAddRightViewKeywords: UIButton!
    @IBOutlet weak var btnEasyDifficulty: UIButton!
    
    @IBOutlet weak var btnKeywordsInfo: UIButton!
    
//    var arrAllRecipeCategoriesList = [Recipe_category]()
    
    typealias BlockTagsSelectionReflect = ()->()
    var blockTagsSelectionReflect : BlockTagsSelectionReflect!
    
    var currentVC : AddNewRecipeVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let font = UIFont.init(name: enum_font.regular.rawValue, size: 16)!
        
        for tagList in [tagListCuisineType,tagListMealType,tagListDiet,tagListKeywords] {
            tagList?.textFont = font
            tagList?.delegate = self
            tagList?.enableRemoveButton = true
        }
        
        
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func handleCell(vc:AddNewRecipeVC, indexPath:IndexPath) {
        
        self.currentVC = vc
        self.tagListKeywords.removeAllTags()
        self.tagListKeywords.delegate = self
        
        self.btnKeywordsInfo.mk_addTapHandler { (btn) in
            BasicFunctions.showInfo(strError: "Keywords are words for describing recipes. e.g. Spicey, Sweet etc.")
        }
        
        for (i,objKeyword) in vc.arrAddedKeywords.enumerated() {
            
            let tagView = self.tagListKeywords.addTag(objKeyword.desc ?? "")
            tagView.tag = i
            
            self.tagListKeywords.layoutSubviews()
            self.tagListKeywords.layoutIfNeeded()
            
        }
        
        self.tagListCuisineType?.delegate = self
        self.addSelectedCusineTagsToTaglist(vc: vc, arrTags: vc.arrSelectedCuisineTags, tagViewList: tagListCuisineType)
        
        self.tagListDiet?.delegate = self
        self.addSelectedCusineTagsToTaglist(vc: vc, arrTags: vc.arrSelectedDietTags, tagViewList: tagListDiet)
        
        self.tagListMealType?.delegate = self
        self.addSelectedCusineTagsToTaglist(vc: vc, arrTags: vc.arrSelectedMealTypeTags, tagViewList: tagListMealType)
        
        //UITextFieldDelegate
        self.handleCellTextFieldDelegates(vc: vc, indexPath: indexPath)
        
        if let arrRecipeCategories = self.getAllRecipeCategories() {
            if let arrTagsCuisine = getTagsArray(strCatName: "cuisine", arr: arrRecipeCategories) {
                
            }
        }
        
        self.btnEasyDifficulty.mk_addTapHandler { (btn) in
            btn.isSelected = !btn.isSelected
            hapHeavy()
            vc.addNewRecipeData.is_easy = btn.isSelected
        }
        
        self.btnEasyDifficulty.isSelected = vc.addNewRecipeData.is_easy
        
    }
    
    
    func getTagsArray(strCatName:String, arr: [Recipe_category]) -> [Category_tag]? {
        let arrCusineTypeTags = arr.filter { (recCategory) -> Bool in
            recCategory.category_name?.lowercased().contains(strCatName) ?? false
        }
        if let objCuisineType = arrCusineTypeTags.first {
            if let arrTags = objCuisineType.tags.allObjects as? [Category_tag] {
                return arrTags
            }
        }
        return nil
    }
    
    func getAllRecipeCategories() -> [Recipe_category]? {
        return Recipe_category.mr_findAll(in: app_default_context) as? [Recipe_category]
    }
    
    
}



// MARK: TagListViewDelegate
extension CellAddNewRecipeChooseCategories : TagListViewDelegate {
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        //        tagView.isSelected = !tagView.isSelected
        print(tagView.accessibilityValue)
        
        
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
        
        self.currentVC.tblView.beginUpdates()
        
        if self.tagListKeywords == sender {
            
            self.currentVC.arrAddedKeywords.remove(at: tagView.tag)
            self.tagListKeywords.layoutSubviews()
            self.tagListKeywords.layoutIfNeeded()
        }
        
        if self.tagListCuisineType == sender {
            
            let filteredObjects = self.currentVC.arrSelectedCuisineTags.filter { (cat) -> Bool in
                cat.tag_id == tagView.id
            }
            if let foundFirst = filteredObjects.first {
                self.currentVC.arrSelectedCuisineTags.removeObject(foundFirst)
            }
            
            sender.layoutSubviews()
            sender.layoutIfNeeded()
        }
        
        if self.tagListDiet == sender {
            
            let filteredObjects = self.currentVC.arrSelectedDietTags.filter { (cat) -> Bool in
                cat.tag_id == tagView.id
            }
            if let foundFirst = filteredObjects.first {
                self.currentVC.arrSelectedDietTags.removeObject(foundFirst)
            }
            
            sender.layoutSubviews()
            sender.layoutIfNeeded()
        }
        
        if self.tagListMealType == sender {
            
            let filteredObjects = self.currentVC.arrSelectedMealTypeTags.filter { (cat) -> Bool in
                cat.tag_id == tagView.id
            }
            if let foundFirst = filteredObjects.first {
                self.currentVC.arrSelectedMealTypeTags.removeObject(foundFirst)
            }
            
            sender.layoutSubviews()
            sender.layoutIfNeeded()
        }
        
        
        self.currentVC.tblView.endUpdates()
        
    }
}

//MARK:- UITextFieldDelegate
extension CellAddNewRecipeChooseCategories {
    
    private func handleCellTextFieldDelegates(vc:AddNewRecipeVC, indexPath:IndexPath) {
        
        
        // txtCuisineType.shouldBeginEditing
        self.txtCuisineType.shouldBeginEditing { () -> Bool in
            
            let filteredSelectedIds = vc.arrSelectedCuisineTags.map { (cat) -> Int64 in
                return cat.tag_id
            }
            self.presentTagListSelection(vc: vc, tagType: .cusineType, arrSelectedIds: filteredSelectedIds) { (arrSelectedIds) in
                
                print(arrSelectedIds)
                
                vc.arrSelectedCuisineTags.removeAll()
                for tag_id in arrSelectedIds {
                    
                    let arrCuisineTags = enum_tag_list_type.cusineType.getAllTags()
                    let foundObjectCatTag = arrCuisineTags.filter { (catTag) -> Bool in
                        catTag.tag_id == tag_id
                    }
                    
                    if let foundObjectCatTagFirst = foundObjectCatTag.first {
                        vc.arrSelectedCuisineTags.append(foundObjectCatTagFirst)
                    }
                    
                }
                self.addSelectedCusineTagsToTaglist(vc: vc, arrTags: vc.arrSelectedCuisineTags, tagViewList: self.tagListCuisineType)
            }
            
            return false
        }
        // txtCuisineType.shouldBeginEditing END--END--END--END
        
        // txtDiet.shouldBeginEditing
        self.txtDiet.shouldBeginEditing { () -> Bool in
            
            let filteredSelectedIds = vc.arrSelectedDietTags.map { (cat) -> Int64 in
                return cat.tag_id
            }
            self.presentTagListSelection(vc: vc, tagType: .diet, arrSelectedIds: filteredSelectedIds) { (arrSelectedIds) in
                
                print(arrSelectedIds)
                
                vc.arrSelectedDietTags.removeAll()
                for tag_id in arrSelectedIds {
                    let arrDietTags = enum_tag_list_type.diet.getAllTags()
                    let foundObjectCatTag = arrDietTags.filter { (catTag) -> Bool in
                        catTag.tag_id == tag_id
                    }
                    
                    if let foundObjectCatTagFirst = foundObjectCatTag.first {
                        vc.arrSelectedDietTags.append(foundObjectCatTagFirst)
                    }
                }
                
                
                
                self.addSelectedCusineTagsToTaglist(vc: vc, arrTags: vc.arrSelectedDietTags, tagViewList: self.tagListDiet)
            }
            
            return false
        }
        // txtDiet.shouldBeginEditing END--END--END--END
        
        // txtDiet.shouldBeginEditing
        self.txtMealType.shouldBeginEditing { () -> Bool in
            
            let filteredSelectedIds = vc.arrSelectedMealTypeTags.map { (cat) -> Int64 in
                return cat.tag_id
            }
            self.presentTagListSelection(vc: vc, tagType: .mealType, arrSelectedIds: filteredSelectedIds) { (arrSelectedIds) in
                
                print(arrSelectedIds)
                
                vc.arrSelectedMealTypeTags.removeAll()
                for tag_id in arrSelectedIds {
                    
                    
                    let arrMealTypeTags = enum_tag_list_type.mealType.getAllTags()
                    let foundObjectCatTag = arrMealTypeTags.filter { (catTag) -> Bool in
                        catTag.tag_id == tag_id
                    }
                    
                    if let foundObjectCatTagFirst = foundObjectCatTag.first {
                        vc.arrSelectedMealTypeTags.append(foundObjectCatTagFirst)
                    }
                }
                
                self.addSelectedCusineTagsToTaglist(vc: vc, arrTags: vc.arrSelectedMealTypeTags, tagViewList: self.tagListMealType)
            }
            
            return false
        }
        // txtDiet.shouldBeginEditing END--END--END--END
        
        
        
        // textfield right view buttons

        txtKeywords?.addRightView(width: 40)
        
        self.btnAddRightViewKeywords.mk_addTapHandler { (btn) in
            print("btnRightAddKeywords tapped")
            if self.txtKeywords.isEmpty() {
                hapError()
                self.txtKeywords.shake()
                return
            }
            
            if let strKeyword = self.txtKeywords.text {
                if strKeyword != "" {
                    let arrFound = vc.arrAddedKeywords.filter { (k) -> Bool in
                        (k.desc ?? "").lowercased() == strKeyword.lowercased()
                    }
                    if let _ = arrFound.first {
                        BasicFunctions.showError(strError: "You can not add same keyword again.")
                        return
                    }
                }
            }
            
            hapSoft()
            vc.tblView.beginUpdates()
            if let objKeyword = Keyword.mr_createEntity(in: app_default_context) {
                objKeyword.desc = self.txtKeywords.text ?? ""
//                vc.arrAddedKeywords.append(objKeyword)
//                self.tagListKeywords.addTag(objKeyword.desc ?? "")
                vc.arrAddedKeywords.insert(objKeyword, at: 0)
                self.tagListKeywords.insertTag(objKeyword.desc ?? "", at: 0)
                
                self.tagListKeywords.layoutSubviews()
                self.tagListKeywords.layoutIfNeeded()
            }
            vc.tblView.endUpdates()
            
            self.txtKeywords.text = ""
        }
        
    }
}


extension CellAddNewRecipeChooseCategories {
    
    
    func presentTagListSelection(vc:AddNewRecipeVC,tagType:enum_tag_list_type,arrSelectedIds : [Int64], compClouser: @escaping ([Int64])->()) {
        
        let objSelectCategoriesOfAddNewRecipeVC = apde.getController(vc_name: "SelectCategoriesOfAddNewRecipeVC", sb: .AddRecipe) as! SelectCategoriesOfAddNewRecipeVC
        objSelectCategoriesOfAddNewRecipeVC.recipeCategoryListType = tagType
        objSelectCategoriesOfAddNewRecipeVC.strTitle = tagType.getCategoryName()
        objSelectCategoriesOfAddNewRecipeVC.arrSelectedIds = arrSelectedIds
        objSelectCategoriesOfAddNewRecipeVC.completionBlock = compClouser
        
        if #available(iOS 13.0, *) {
        } else {
            objSelectCategoriesOfAddNewRecipeVC.modalPresentationStyle = .overCurrentContext
        }
        
        UIViewController.CVC()?.present(objSelectCategoriesOfAddNewRecipeVC, animated: true, completion: nil)
        
    }
}



extension CellAddNewRecipeChooseCategories {
    
    
    
    func addSelectedCusineTagsToTaglist(vc:AddNewRecipeVC,arrTags:[Category_tag],tagViewList:TagListView?) {
        
        vc.tblView.beginUpdates()
        
        tagViewList?.removeAllTags()
        for tag in arrTags {
            let tgView = tagViewList?.addTag(tag.tag_name ?? "")
            tgView?.id = tag.tag_id
        }
        tagViewList?.layoutSubviews()
        tagViewList?.layoutIfNeeded()
        
        vc.tblView.endUpdates()
    }
    
}


//extension CellAddNewRecipeChooseCategories : UITextFieldDelegate {
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//
//        if textField == self.txtCuisineType {
//
//
//
//
//            return false
//        } else if textField == self.txtDiet {
//
//            let objSelectCategoriesOfAddNewRecipeVC = apde.getController(vc_name: "SelectCategoriesOfAddNewRecipeVC", sb: .AddRecipe) as! SelectCategoriesOfAddNewRecipeVC
//            objSelectCategoriesOfAddNewRecipeVC.completionBlock = { arrSelected in
//                print(arrSelected)
//
//
//            }
//            if let arrRecipeCategories = self.getAllRecipeCategories() {
//                let arrCusineTypeCat = arrRecipeCategories.filter { (recCategory) -> Bool in
//                    return recCategory.category_name?.lowercased().contains("diet") ?? false
//                }
//                if let catCusineType = arrCusineTypeCat.first {
//                    objSelectCategoriesOfAddNewRecipeVC.objRecipeCategory = catCusineType
//                    UIViewController.CVC()?.present(objSelectCategoriesOfAddNewRecipeVC, animated: true, completion: nil)
//                }
//            }
//
//            return false
//        } else if textField == self.txtMealType {
//
//            return false
//        } else if textField == self.txtKeywords {
//
//            return true
//        }
//        return true
//    }
//}
