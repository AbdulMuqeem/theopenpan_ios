//
//  CellAddNewRecipeBottomServesBtns.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/20/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import AVFoundation
import CoreMedia

class CellAddNewRecipeBottomServesBtns: UITableViewCell {
    
    @IBOutlet weak var txtServes: SkyFloatingLabelTextField!
    @IBOutlet weak var txtHours: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMins: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnIsOnePotMeal: UIButton!
    @IBOutlet weak var btnSubmitRecipeToOpenPan: UIButton!
    @IBOutlet weak var stackViewBtnSubmitRecipe: UIStackView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var pickerViewHours: UIPickerView!
    var pickerViewMins: UIPickerView!
    
    var currentVC: AddNewRecipeVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.btnSubmitRecipeToOpenPan.mk_addTapHandler { (btn) in
            btn.isSelected = !btn.isSelected
            hapHeavy()
            self.currentVC.addNewRecipeData.submit_recipe_for_review = btn.isSelected
        }
        
        self.btnIsOnePotMeal.mk_addTapHandler { (btn) in
            btn.isSelected = !btn.isSelected
            hapHeavy()
            self.currentVC.addNewRecipeData.is_one_pot = btn.isSelected
        }
        
        pickerViewHours = UIPickerView()
        pickerViewMins = UIPickerView()
        
        pickerViewHours.dataSource = self
        pickerViewHours.delegate = self
        
        pickerViewMins.dataSource = self
        pickerViewMins.delegate = self
        
        txtHours.inputView = pickerViewHours
        txtMins.inputView = pickerViewMins
        
        txtHours.delegate = self
        txtMins.delegate = self
        
        txtServes.mk_addAllEditingEventHandler { (txt) in
            
            self.currentVC.addNewRecipeData.serves = Int(txt.text ?? "0") ?? 0
        }
        txtServes.shouldChangeString { (from, toString) -> Bool in
            if toString == "0" {
                return false
            }
            if toString.count < 4 {
                return true
            }
            return false
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func handleCell(vc:AddNewRecipeVC,indexPath:IndexPath) {
        
        self.currentVC = vc
        
        if vc.isInEditMode {
            self.btnSave.setTitle("UPDATE".localize(), for: .normal)
        } else {
            self.btnSave.setTitle("SAVE".localize(), for: .normal)
        }
        
        
        if vc.addNewRecipeData.web_recipe_url != "" {
            self.btnSubmitRecipeToOpenPan?.isHidden = true
        } else {
            self.btnSubmitRecipeToOpenPan?.isHidden = !vc.addNewRecipeData.is_own_recipe
        }
        
        
        
        self.btnIsOnePotMeal.isSelected             = vc.addNewRecipeData.is_one_pot
        self.btnSubmitRecipeToOpenPan.isSelected    = vc.addNewRecipeData.submit_recipe_for_review
        
        self.txtServes.text = ""
        if vc.addNewRecipeData.serves != 0 {
            self.txtServes.text = "\(vc.addNewRecipeData.serves)"
        }
        self.txtHours.text = ""
        if let h = vc.addNewRecipeData.ready_time_hours {
            self.txtHours.text = "\(h) Hour/s"
        }
        self.txtMins.text = ""
        
        if let m = vc.addNewRecipeData.ready_time_mins {
            self.txtMins.text = "\(m) Min/s"
        }


        self.btnSave.mk_addTapHandler { (btn) in
            
            vc.view.resignFirstResponder()
            hapHeavy()
            self.btnSaveTapped(vc: vc)
            
        }
        
        self.btnCancel.mk_addTapHandler { (btn) in
            hapError()
            
            BasicFunctions.showPopupTwoButton(title: "CANCEL".localize(), desc: "Are you sure you want to exit from add new recipe?".localize(), btnTitleLeft: "NO".localize(), btnTitleRight: "YES".localize(), baseVC: vc) { (isYes) in
                if isYes {
                    vc.pop()
                }
            }
            
        }
    }
    
    
    func btnSaveTapped(vc:AddNewRecipeVC) {
        
        hapHeavy()
        
        if vc.addNewRecipeData.title == "" {
            
            vc.tblView.scrollToTopOrLeft()
            Timer.after(0.4) {
                vc.cellAddNewRecipeTitleImage.txtTitle.shake()
                vc.cellAddNewRecipeTitleImage.txtTitle.becomeFirstResponder()
                hapError()
            }
            
            return
        }
        
        /*
         if vc.addNewRecipeData.media_url == "" {
         vc.tblView.scrollToTopOrLeft()
         Timer.after(0.4) {
         vc.cellAddNewRecipeTitleImage.imgViewRecipeImage.shake()
         vc.cellAddNewRecipeTitleImage.stackView.shake()
         hapError()
         }
         
         return
         } */
        
        if vc.arrIngredints.count == 0 {
            
            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 2), at: .middle, animated: true)
            Timer.after(0.4) {
                vc.cellAddNewRecipeIngredients.shake()
                hapError()
            }
            
            return
        }
        
        
        /*
        var i = 0
        for (index,ing) in vc.arrIngredints.enumerated() {
            if ing.unit_id == 0 {
                i = index
                break
            }
        }
        
        if i != 0 {
            let indexPath = IndexPath.init(item: i, section: 2)
            vc.tblView.scrollToRow(at: indexPath, at: .top, animated: true)
            Timer.after(0.4) {
                if let cell = vc.tblView.cellForRow(at: indexPath) {
                    cell.shake()
                }
            }
            return
        }
         */
        
        
        if vc.arrDirections.count == 0 {
            
            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 4), at: .middle, animated: true)
            Timer.after(0.4) {
                vc.cellAddNewRecipeDirections.shake()
                hapError()
            }
            
            return
        }
        
        //        if vc.arrSelectedCuisineTags.count == 0 {
        //
        //            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 5), at: .top, animated: true)
        //            Timer.after(0.4) {
        //                vc.cellAddNewRecipeChooseCategories.txtCuisineType.shake()
        //                hapError()
        //            }
        //
        //            return
        //        }
        
        //        if vc.arrSelectedDietTags.count == 0 {
        //
        //            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 5), at: .middle, animated: true)
        //            Timer.after(0.4) {
        //                vc.cellAddNewRecipeChooseCategories.txtDiet.shake()
        //                hapError()
        //            }
        //
        //            return
        //        }
        
        //        if vc.arrSelectedMealTypeTags.count == 0 {
        //
        //            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 5), at: .bottom, animated: true)
        //            Timer.after(0.4) {
        //                vc.cellAddNewRecipeChooseCategories.txtMealType.shake()
        //                hapError()
        //            }
        //
        //            return
        //        }
        
        if vc.addNewRecipeData.serves == 0 && vc.addNewRecipeData.web_recipe_url == "" {
            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 6), at: .bottom, animated: true)
            Timer.after(0.4) {
                vc.cellAddNewRecipeBottomServesBtns.txtServes.shake()
                vc.cellAddNewRecipeBottomServesBtns.txtServes.becomeFirstResponder()
                hapError()
            }
            
            return
        }
        
        
        if vc.addNewRecipeData.ready_time_hours == nil && vc.addNewRecipeData.ready_time_mins == 0 && vc.addNewRecipeData.web_recipe_url == "" {
            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 6), at: .bottom, animated: true)
            Timer.after(0.4) {
                vc.cellAddNewRecipeBottomServesBtns.txtMins.shake()
                vc.cellAddNewRecipeBottomServesBtns.txtHours.shake()
                hapError()
            }
            return
        }
        
        if vc.addNewRecipeData.ready_time_hours == 0 && vc.addNewRecipeData.ready_time_mins == nil && vc.addNewRecipeData.web_recipe_url == "" {
            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 6), at: .bottom, animated: true)
            Timer.after(0.4) {
                vc.cellAddNewRecipeBottomServesBtns.txtMins.shake()
                vc.cellAddNewRecipeBottomServesBtns.txtHours.shake()
                hapError()
            }
            return
        }
        
        if vc.addNewRecipeData.ready_time_hours == nil && vc.addNewRecipeData.ready_time_mins == nil {
            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 6), at: .bottom, animated: true)
            Timer.after(0.4) {
                vc.cellAddNewRecipeBottomServesBtns.txtMins.shake()
                vc.cellAddNewRecipeBottomServesBtns.txtHours.shake()
                hapError()
            }
            return
        }
        
        if vc.addNewRecipeData.ready_time_hours == 0 && vc.addNewRecipeData.ready_time_mins == 0 {
            vc.tblView.scrollToRow(at: IndexPath.init(row: 0, section: 6), at: .bottom, animated: true)
            Timer.after(0.4) {
                vc.cellAddNewRecipeBottomServesBtns.txtMins.shake()
                vc.cellAddNewRecipeBottomServesBtns.txtHours.shake()
                hapError()
            }
            
            return
        }
        //        var i = 0
        //        for (index,ing) in vc.arrIngredints.enumerated() {
        //            if ing.unit_id == 0 {
        //                i = index
        //                break
        //            }
        //        }
        //
        //        if i != 0 {
        //            let indexPath = IndexPath.init(item: i, section: 2)
        //            vc.tblView.scrollToRow(at: indexPath, at: .top, animated: true)
        //            Timer.after(0.4) {
        //                if let cell = vc.tblView.cellForRow(at: indexPath) {
        //                    cell.shake()
        //                }
        //            }
        //            return
        //        }
        
        if vc.isInEditMode {
            if self.btnSubmitRecipeToOpenPan.isSelected == false {
                if vc.addNewRecipeData.recipe_status == enum_recipe_status.approved.rawValue {
                    if vc.addNewRecipeData.is_own_recipe {
                        self.btnSubmitRecipeToOpenPan.shake()
                        return
                    }
                }
            }
        }
        
        addRecipeApiCall(vc: vc)
        
        hapHeavy()
        
    }
    
    
    func selectCollectionFlow(vc:AddNewRecipeVC) {
        
        let objCookbookCollectionNameList = apde.getController(vc_name: "CookbookCollectionNameList", sb: .CookBookTab) as! CookbookCollectionNameList
        objCookbookCollectionNameList.compBlock = { (objCollection,isCancelTapped) in
            
            if isCancelTapped {
                return
            }
            
            if let objCollection = objCollection {
                // Selected Collection
                print(objCollection.id)
                vc.addNewRecipeData.collection_id = Int64(objCollection.id)
                self.addRecipeApiCall(vc: vc)
                
            } else {
                // Selected Add New Collection
                let objAddCookBookCollectionVC = apde.getController(vc_name: "AddCookBookCollectionVC", sb: .CookBookTab) as! AddCookBookCollectionVC
                objAddCookBookCollectionVC.shouldDisplaySuccessPopup = false
                objAddCookBookCollectionVC.compBlock = { (boolValue) in
                    // if Collection Created
                    if boolValue {
                        self.selectCollectionFlow(vc: vc)
                    }
                }
                vc.navigationController?.pushViewController(objAddCookBookCollectionVC, animated: true)
            }
            
        }
        UIViewController.CVC()?.present(objCookbookCollectionNameList, animated: true, completion: nil)
    }
    
    
}



extension CellAddNewRecipeBottomServesBtns : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.pickerViewHours {
            return 14
        }
        if pickerView == self.pickerViewMins {
            return 61
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if row == 0 {
            return "Scroll to select"
        }
        
        if pickerView == self.pickerViewHours {
            return "\(row - 1)"
        }
        if pickerView == self.pickerViewMins {
            return "\(row - 1)"
        }
        return "\(row - 1)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if row == 0 {
            return
        }
        
        if pickerView == self.pickerViewHours {
            self.txtHours.text = "\(row-1) Hour/s"
            self.currentVC.addNewRecipeData.ready_time_hours = row-1
        }
        if pickerView == self.pickerViewMins {
            self.txtMins.text = "\(row-1) Min/s"
            self.currentVC.addNewRecipeData.ready_time_mins = row-1
        }
    }
    
}


extension CellAddNewRecipeBottomServesBtns : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
}



//func findAndGetUnitId(unitName:String) -> String {
//
//    if let arrUnitOption = Unit_option.mr_findFirst(with: NSPredicate.init(format: "tag_name_plural == %@", unitName), in: app_default_context) {
//        return arrUnitOption.tag_id
//    }
//    if let arrUnitOption = Unit_option.mr_findFirst(with: NSPredicate.init(format: "tag_name_singular == %@", unitName), in: app_default_context) {
//        return arrUnitOption.tag_id
//    }
//    return ""
//}
func findAndGetUnitId(unitName:String) -> Int64 {
    
    if let arrUnitOption = Unit_option.mr_findFirst(with: NSPredicate.init(format: "tag_name_plural == %@", unitName), in: app_default_context) {
        return arrUnitOption.tag_id
    }
    if let arrUnitOption = Unit_option.mr_findFirst(with: NSPredicate.init(format: "tag_name_singular == %@", unitName), in: app_default_context) {
        return arrUnitOption.tag_id
    }
    return 0
}


extension CellAddNewRecipeBottomServesBtns {
    
    func addRecipeApiCall(vc:AddNewRecipeVC) {
        
        var dictParam : [String : Any] = ["title" : vc.addNewRecipeData.title]
        
        if vc.addNewRecipeData.web_recipe_url != "" {
            dictParam.updateValue(vc.addNewRecipeData.web_recipe_url, forKey: "web_recipe_url")
        }
        
        if vc.addNewRecipeData.media_url != "" {
            dictParam.updateValue(vc.addNewRecipeData.is_video, forKey: "is_video")
            dictParam.updateValue(vc.addNewRecipeData.media_url, forKey: "media_url")
        }
        
        if vc.addNewRecipeData.video_thumb_url != "" {
            dictParam.updateValue(vc.addNewRecipeData.video_thumb_url, forKey: "video_thumb_url")
        }
        
        //Ingredient_list
        var arrDictIngredient = [[String:Any]]()
        for objIngredient in vc.arrIngredints {
            
            let ingName = convertFtoC(text: objIngredient.name ?? "")
            
            var dict = ["name":ingName,
                        "qty":objIngredient.qty ?? "",
                        "note":objIngredient.note ?? ""] as [String : Any]
            dict.updateValue(objIngredient.unit_name ?? "", forKey: "unit")
            let id = findAndGetUnitId(unitName: objIngredient.unit_name ?? "")
//            if id != "" {
                dict.updateValue(id, forKey: "unit_id")
//            }
            arrDictIngredient.append(dict)
        }
        
        guard let jsonDataArrDictIngredient = try? JSONSerialization.data(
            withJSONObject: arrDictIngredient,
            options: []) else {
                return
        }
        
        guard let textArrIngredienta = String(data: jsonDataArrDictIngredient, encoding: .utf8) else {
            return
        }
        print("JSON string textArrIngredienta = \(textArrIngredienta)")
        dictParam.updateValue(textArrIngredienta, forKey: "ingredient_list")
        
        
        
        //direction_list
        
        var arrDirections = [[String:Any]]()
        for objDirection in vc.arrDirections {
            
            let descDirection = convertFtoC(text: objDirection.desc ?? "")
            let dict = ["step":objDirection.step,
                        "description":descDirection] as [String : Any]
            arrDirections.append(dict)
        }
        
        guard let jsonDataDirections = try? JSONSerialization.data(
            withJSONObject: arrDirections,
            options: []) else {
                return
        }
        
        guard let textDirections = String(data: jsonDataDirections, encoding: .utf8) else {
            return
        }
        print("JSON string textDirections = \(textDirections)")
        dictParam.updateValue(textDirections, forKey: "direction_list")
        
        //tag_ids -- enum_tag_list_type.cusineType
        var arrTagsIds = [[String:Any]]()
        let arrStrCuisineTagsIds = vc.arrSelectedCuisineTags.map { (c) -> String in
            "\(c.tag_id)"
        }
        if arrStrCuisineTagsIds.count > 0 {
            if let cuisineCat = enum_tag_list_type.cusineType.getCatObject() {
                arrTagsIds.append(["category_id"      : cuisineCat.category_id,
                                   "selected_tags_id" : arrStrCuisineTagsIds.joined(separator: ",")])
            }
        }
        
        //tag_ids -- enum_tag_list_type.mealType
        let arrStrMealTypeTagsIds = vc.arrSelectedMealTypeTags.map { (c) -> String in
            "\(c.tag_id)"
        }
        if arrStrMealTypeTagsIds.count > 0 {
            if let mealTypeCat = enum_tag_list_type.mealType.getCatObject() {
                arrTagsIds.append(["category_id"      : mealTypeCat.category_id,
                                   "selected_tags_id" : arrStrMealTypeTagsIds.joined(separator: ",")])
            }
        }
        
        //tag_ids -- enum_tag_list_type.diet
        let arrStrDietTagsIds = vc.arrSelectedDietTags.map { (c) -> String in
            "\(c.tag_id)"
        }
        if arrStrDietTagsIds.count > 0 {
            if let mealTypeCat = enum_tag_list_type.diet.getCatObject() {
                arrTagsIds.append(["category_id"      : mealTypeCat.category_id,
                                   "selected_tags_id" : arrStrDietTagsIds.joined(separator: ",")])
            }
        }
        
        if arrTagsIds.count > 0 {
            if let jsonDataArrTagsIds = try? JSONSerialization.data(withJSONObject: arrTagsIds, options: []) {
                if let textArrTagsIds = String(data: jsonDataArrTagsIds, encoding: .utf8) {
                    print("JSON string textDirections = \(textArrTagsIds)")
                    dictParam.updateValue(textArrTagsIds, forKey: "tag_ids")
                }
            }
        }
        
        
        ///////
        
//        if vc.addNewRecipeData.is_easy {
            dictParam.updateValue(vc.addNewRecipeData.is_easy, forKey: "is_easy")
//        }
        
//        if vc.addNewRecipeData.is_one_pot {
            dictParam.updateValue(vc.addNewRecipeData.is_one_pot, forKey: "is_one_pot")
//        }
        
        
        dictParam.updateValue(vc.addNewRecipeData.is_own_recipe, forKey: "is_own_recipe")
        dictParam.updateValue(vc.addNewRecipeData.submit_recipe_for_review, forKey: "submit_recipe_for_review")
        
        
        
        
        //keywords_list
        let arrStringKeywords = vc.arrAddedKeywords.map { (objKeyword) -> String in
            objKeyword.desc ?? ""
        }
        dictParam.updateValue(arrStringKeywords.joined(separator: ","), forKey: "keywords_list")
        
        //ready_time_hours, ready_time_mins, serves
        dictParam.updateValue(vc.addNewRecipeData.ready_time_hours ?? 0, forKey: "ready_time_hours")
        dictParam.updateValue(vc.addNewRecipeData.ready_time_mins ?? 0, forKey: "ready_time_mins")
        
        
        
        dictParam.updateValue(vc.addNewRecipeData.serves, forKey: "serves")
        
        //collection_id
        if vc.addNewRecipeData.collection_id == 0 {
            self.selectCollectionFlow(vc: vc)
            return
        }
        
        dictParam.updateValue(vc.addNewRecipeData.collection_id, forKey: "collection_id")
        
        
        
        
        
        if vc.addNewRecipeData.media_url != "" {
            
            if vc.addNewRecipeData.is_video.mk_boolValue {
                // Selected video for upload
                if vc.addNewRecipeData.imported_media_url == "" {
                    
                    let fileSizeDouble = fileSize(forURL: vc.addNewRecipeData.media_url)
                    if fileSizeDouble > 50.0 {
                        // fize size greater than 50 MB
                        BasicFunctions.showPopupOneButton(title: "Video", desc: "Video should be less than 50 mb.", btnTitle: "OKAY".localize()) {
                            
                        }
                        return
                    }
                }
                
            } else {
                // Selected photo for upload
                
                let fileSizeDouble = fileSize(forURL: URL.init(fileURLWithPath: vc.addNewRecipeData.media_url))
                if fileSizeDouble > 10.0 {
                    // fize size greater than 50 MB
                    BasicFunctions.showPopupOneButton(title: "Video", desc: "Photo should be less than 5 mb.", btnTitle: "OKAY".localize()) {
                        
                    }
                    return
                }
            }
            
            if vc.addNewRecipeData.imported_media_url == "" {
                self.uploadMedia(vc: vc, dictParam: dictParam) { (dict,success)  in
                    if success {
                        self.finalApiCallAddRecipe(vc:vc,dictParam: dict)
                    }
                }
            } else if vc.addNewRecipeData.imported_media_url.contains("http") && vc.addNewRecipeData.imported_video_thumb_url != "" && (vc.addNewRecipeData.imported_video_thumb_url.contains("http") == false) {
                
                self.uploadThumbnailAndGetLink(localThumbUrl: vc.addNewRecipeData.imported_video_thumb_url) { (liveThumbnailUrl) in
                    
                    vc.addNewRecipeData.imported_video_thumb_url = liveThumbnailUrl
                    vc.addNewRecipeData.video_thumb_url = liveThumbnailUrl
                    
                    dictParam["video_thumb_url"] = liveThumbnailUrl
                    self.finalApiCallAddRecipe(vc:vc,dictParam: dictParam)
                }
            } else {
                self.finalApiCallAddRecipe(vc:vc,dictParam: dictParam)
            }
            
        } else {
            self.finalApiCallAddRecipe(vc:vc,dictParam: dictParam)
        }
        
        
    }
    
    
    
    func uploadThumbnailAndGetLink(localThumbUrl:String, compBlock: @escaping (String)->()) {
        
        apde.showLoader()
        
        AWSHelper.sharedInstance.upload(fileName: "\(Date().mk_ticks).mp4", fileURL: URL.init(fileURLWithPath: localThumbUrl)) { (res) in
            
            switch res {
            case .success(let localURL,let liveURL) :
                deleteFileAtPath(url: localURL.absoluteString)
                compBlock(liveURL.absoluteString)
                break
            case .error( _,let error) :
                print(error)
                BasicFunctions.showError(strError: error.localizedDescription)
                apde.hideLoader()
                return
            case .progress(let uploadProgressPercent):
                print(uploadProgressPercent)
                break
                
            }
            
        }
    }
    
    
    
    func uploadMedia(vc:AddNewRecipeVC,dictParam:[String:Any],compHandler:@escaping ([String:Any],Bool)->()) {
        
        var mutDictParam = dictParam
        
        if vc.addNewRecipeData.is_video.mk_boolValue {
            
            // Upload video file
            
            apde.showLoader()
            
            AWSHelper.sharedInstance.upload(fileName: "\(Date().mk_ticks).mp4", fileURL: URL.init(string: vc.addNewRecipeData.media_url)!) { (res) in
                
                switch res {
                case .success(let localURL,let liveURL) :
                    deleteFileAtPath(url: localURL.absoluteString)
                    
                    vc.addNewRecipeData.live_media_url = liveURL.absoluteString
                    
                    mutDictParam.updateValue(vc.addNewRecipeData.live_media_url, forKey: "media_url")
                    
                    AWSHelper.sharedInstance.upload(fileName: "\(Date().mk_ticks)_thumb.jpg", fileURL: URL.init(fileURLWithPath: vc.addNewRecipeData.video_thumb_url)) { (res) in
                        
                        switch res {
                        case .success(let localURL,let liveURL) :
                            deleteFileAtPath(url: localURL.absoluteString)
                            
                            vc.addNewRecipeData.live_video_thumb_url = liveURL.absoluteString
                            
                            mutDictParam.updateValue(vc.addNewRecipeData.live_video_thumb_url, forKey: "video_thumb_url")
                            
                            compHandler(mutDictParam,true)
                            
                            
                            break
                        case .error( _,let error) :
                            BasicFunctions.showError(strError: error.localizedDescription)
                            print(error)
                            apde.hideLoader()
                            compHandler(mutDictParam,false)
                            
                            return
                            
                            
                        case .progress(let uploadProgressPercent):
                            print(uploadProgressPercent)
                            break
                            
                        }
                        
                    }
                    
                    
                    
                    break
                case .error( _,let error) :
                    print(error)
                    BasicFunctions.showError(strError: error.localizedDescription)
                    apde.hideLoader()
                    compHandler(mutDictParam,false)
                    return
                    
                    
                    
                case .progress(let uploadProgressPercent):
                    print(uploadProgressPercent)
                    break
                    
                }
                
            }
            
            
            
        } else {
            
            // Upload photo file
            
            apde.showLoader()
            
            AWSHelper.sharedInstance.upload(fileName: "\(Date().mk_ticks).jpg", fileURL: URL.init(fileURLWithPath: vc.addNewRecipeData.media_url)) { (res) in
                
                switch res {
                case .success(let localURL,let liveURL) :
                    
                    print(liveURL.path)
                    print("----------------")
                    print(liveURL.absoluteString)
                    print(liveURL.relativeString)
                    
                    deleteFileAtPath(url: localURL.absoluteString)
                    
                    vc.addNewRecipeData.live_media_url = liveURL.absoluteString
                    
                    mutDictParam.updateValue(vc.addNewRecipeData.live_media_url, forKey: "media_url")
                    
                    compHandler(mutDictParam,true)
                    return
                    
                    
                case .error(let fileURL,let error) :
                    print(fileURL)
                    print("----------------")
                    print(error)
                    BasicFunctions.showError(strError: error.localizedDescription)
                    apde.hideLoader()
                    compHandler(mutDictParam,false)
                    return
                    
                case .progress(let uploadProgressPercent):
                    print(uploadProgressPercent)
                    break
                    
                }
                
            }
            
            
            
        }
    }
    
    
    func finalApiCallAddRecipe(vc:AddNewRecipeVC, dictParam:[String:Any]) {
        
        if vc.isInEditMode {
            
            var mutParams = dictParam
            mutParams.updateValue(vc.recipeIdEditMode, forKey: "recipe_id")
            
            if vc.addNewRecipeData.web_recipe_url != "" {
                mutParams.removeValue(forKey: "submit_recipe_for_review")
            }
            
            editRecipe(params: mutParams) { (res) in
                switch res {
                case .success(let dictData, let msg):
                    print("__________________________________________")
                    print(dictData)
                    BasicFunctions.showPopupOneButton(title: "Edit Recipe".localize(), desc: msg, baseVC: vc) {
                        hapHeavy()
                        
                        UIViewController.CVC()?.navigationController?.popViewController(animated: true)
                        vc.editedDoneEditMode()
                    }
                    
                    break
                case .failed(let msg):
                    BasicFunctions.showError(strError: msg)
                    break
                case .error(let error):
                    BasicFunctions.showError(strError: error)
                    break
                }
                apde.hideLoader()
            }
            
        } else {
            
            var mutParams = dictParam
            mutParams.updateValue(vc.isEdmamRecipe, forKey: "imported_by_edmam")
            
            addRecipe(params: mutParams) { (res) in
                switch res {
                case .success(let dictData, let msg):
                    print("__________________________________________")
                    print(dictData)
                    BasicFunctions.showPopupOneButton(title: "Add Recipe".localize(), desc: msg, baseVC: vc) {
                        hapHeavy()
                        if vc.isEdmamRecipe {
                            if let recipe_details = dictData["recipe_details"] as? Dictionary<String,Any> {
                                if let recipe_id = recipe_details["id"] as? Int64 {
                                    vc.edmamRecipeAdded?(recipe_id)
                                }
                            }
                        }
                        updateMyRecipesVC(vcSelf: vc)
                        UIViewController.CVC()?.navigationController?.popViewController(animated: true)
                    }
                    
                    break
                case .failed(let msg):
                    BasicFunctions.showError(strError: msg)
                    break
                case .error(let error):
                    BasicFunctions.showError(strError: error)
                    break
                }
                apde.hideLoader()
            }
        }
        
        
        
    }
    
}



func fileSize(forURL url: Any) -> Double {
    var fileURL: URL?
    var fileSize: Double = 0.0
    if (url is URL) || (url is String)
    {
        if (url is URL) {
            fileURL = url as? URL
        }
        else {
            fileURL = URL.init(string: url as! String)
        }
        var fileSizeValue = 0.0
        try? fileSizeValue = (fileURL?.resourceValues(forKeys: [URLResourceKey.fileSizeKey]).allValues.first?.value as! Double?)!
        if fileSizeValue > 0.0 {
            fileSize = (Double(fileSizeValue) / (1024 * 1024))
        }
    }
    return fileSize
}

func getLength(url:URL) -> Float64 {
    //    if let url = Bundle.main.url(forResource: "small", withExtension: "mp4") {
    let asset = AVAsset(url: url)
    let duration = asset.duration
    let durationTime = CMTimeGetSeconds(duration)
    return durationTime
    print(durationTime)
    //    }
}



//func createDirectory(videoURL:URL){
//         let Directorypath = getDirectoryPath()
//         var objcBool:ObjCBool = true
//         let isExist =  FileManager.default.fileExists(atPath:Directorypath,isDirectory: &objcBool)
//         // If the folder with the given path doesn't exist already, create it
//         if isExist == false{
//             do{
//                 try FileManager.default.createDirectory(atPath: Directorypath, withIntermediateDirectories: true, attributes: nil)
//             }catch{
//
//                 print("Something went wrong while creating a new folder")
//             }
//         }
//         let fileManager = FileManager()
//
//         do {
//             if fileManager.fileExists(atPath:Directorypath) {
//                 try? fileManager.removeItem(at: URL(fileURLWithPath:Directorypath))
//             }
//             try fileManager.copyItem(at:videoURL.absoluteURL, to: URL(fileURLWithPath:Directorypath))
//
//              self.imagePicker.dismiss(animated: true, completion:nil)
//           }catch let error {
//                           print(error.localizedDescription)
//           }
//
//}


func updateMyRecipesVC(vcSelf:UIViewController) {
    if let viewControllers = vcSelf.navigationController?.viewControllers {
        for child in viewControllers {
            if let myRecipeVC = child as? MyRecipesVC {
                myRecipeVC.fromIndex = 0
                myRecipeVC.arrMyRecipeList.removeAll()
                myRecipeVC.loadMoreItems()
            }
        }
    }
}


func updateRecipesListVC(newUpdatedRecipeDetails: RecipeDetails, vcSelf:UIViewController) {
    if let viewControllers = vcSelf.navigationController?.viewControllers {
        for child in viewControllers {
            if let recipeListVC = child as? RecipeListVC {
                var foundIndex : Int?
                for (i,recipe) in recipeListVC.arrRecipeListByCategory.enumerated() {
                    if recipe.id == newUpdatedRecipeDetails.id {
                        foundIndex = i
                    }
                }
                if foundIndex != nil {
                    recipeListVC.arrRecipeListByCategory[foundIndex!].name = newUpdatedRecipeDetails.name
                    recipeListVC.arrRecipeListByCategory[foundIndex!].ratings = newUpdatedRecipeDetails.ratings
                    if let _ = recipeListVC.collView.cellForItem(at: IndexPath.init(row: foundIndex!, section: 0)) {
                        recipeListVC.collView.reloadItems(at: [IndexPath.init(row: foundIndex!, section: 0)])
//                        recipeListVC.collView.reloadData()
                    }
                }
            }
        }
    }
}
