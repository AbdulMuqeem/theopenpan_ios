//
//  CellAddNewRecipeDirections.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/20/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class CellAddNewRecipeDirections: UITableViewCell {

    var currentVC                                       : AddNewRecipeVC!
    @IBOutlet weak var lblIndexNumber                   : UILabel!
    @IBOutlet weak var lblPlaceholderTextView           : UILabel!
    @IBOutlet weak var txtViewDirection                 : UITextView!
    @IBOutlet weak var btnRemoveDirection               : UIButton!
    @IBOutlet weak var btnAddDirectionItem              : UIButton!
    @IBOutlet weak var txtViewDirectionItem             : UITextView!
    @IBOutlet weak var viewBottomTextView               : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the vitheopenpanew for the selected state
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)

        if editing {
            for view in subviews where view.description.contains("Reorder") {
                for case let subview as UIImageView in view.subviews {
                    subview.image = UIImage(named: "AssetSortIcon")?.withRenderingMode(.alwaysTemplate)
                    subview.tintColor = #colorLiteral(red: 0.337254902, green: 0.337254902, blue: 0.337254902, alpha: 0.75)
                    subview.frame.size = .init(width: 25, height: 30)
                }
            }
        }
    }
    
    
    func recflectIndex(vc:AddNewRecipeVC, indexPath:IndexPath) {
        self.lblIndexNumber?.text = "\(indexPath.row + 1)."
    }
    
    func handleCellAddItem(vc:AddNewRecipeVC,indexPath:IndexPath) {
        
        self.currentVC = vc
        
        vc.cellAddNewRecipeDirections = self
        self.txtViewDirectionItem.delegate = self
        
        self.btnAddDirectionItem.mk_addTapHandler { (btn) in
            
            if self.txtViewDirectionItem.text == "" || self.txtViewDirectionItem.text == nil {
                hapError()
                self.txtViewDirectionItem.becomeFirstResponder()
                self.lblPlaceholderTextView.shake()
                self.txtViewDirectionItem.shake()
                return
            }
            
            if let objDirection = Direction.mr_createEntity(in: app_default_context) {
                
                hapSoft()
                
//                let lastContentOffset = vc.tblView.contentOffset
//                vc.tblView.beginUpdates()
//                objDirection.desc = self.txtViewDirectionItem.text ?? ""
//                objDirection.step = Int64(vc.arrDirections.count + 1)
//                vc.arrDirections.append(objDirection)
//                vc.tblView.insertRows(at: [IndexPath(row: vc.arrDirections.count - 1, section: 3)], with: .none)
//                vc.tblView.endUpdates()
//
//                vc.tblView.layer.removeAllAnimations()
//                vc.tblView.setContentOffset(lastContentOffset, animated: false)
//
//                self.txtViewDirectionItem.text = ""
//                self.textViewDidChange(self.txtViewDirectionItem)
                
                objDirection.desc = self.txtViewDirectionItem.text ?? ""
                objDirection.step = Int64(vc.arrDirections.count + 1)
                vc.arrDirections.append(objDirection)
                UIView.performWithoutAnimation {
                    let offset = vc.tblView.contentOffset
                    vc.tblView.reloadSections(IndexSet.init(integer: 3), with: .fade)
                    vc.tblView.contentOffset = offset
                }
                self.txtViewDirectionItem.text = ""
                self.textViewDidChange(self.txtViewDirectionItem)
                
            }
        }
    }

    func handleCell(vc:AddNewRecipeVC,indexPath:IndexPath) {
        
        self.currentVC = vc
        
        let objDirection = vc.arrDirections[indexPath.row]
        
        self.recflectIndex(vc: vc, indexPath: indexPath)
        
        objDirection.step = Int64(indexPath.row + 1)
        
        let desc = (objDirection.desc ?? "").replacingOccurrences(of: "\n", with: "<br>")
        self.txtViewDirection.mk_setHTMLFromString2(htmlText: desc)
        self.txtViewDirection.layoutIfNeeded()
        self.btnRemoveDirection.isHidden = true
        
        if indexPath.row % 2 == 0 {
            self.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        } else {
            self.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.8941176471, blue: 0.8784313725, alpha: 0.36)
        }
        
    }
    

}

extension CellAddNewRecipeDirections : UITextViewDelegate {
    
    
    func textViewDidChange(_ textView: UITextView) {
        lblPlaceholderTextView.isHidden = !textView.text.isEmpty
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.viewBottomTextView.backgroundColor = #colorLiteral(red: 0.631372549, green: 0.7764705882, blue: 0.7254901961, alpha: 1)
        if let constraint = (self.viewBottomTextView.constraints.filter{$0.firstAttribute == .width}.first) {
            constraint.constant = 2.0
        }
        self.layoutIfNeeded()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.viewBottomTextView.backgroundColor = #colorLiteral(red: 0.8901960784, green: 0.8901960784, blue: 0.8901960784, alpha: 1)
        if let constraint = (self.viewBottomTextView.constraints.filter{$0.firstAttribute == .width}.first) {
            constraint.constant = 1.0
        }
        self.layoutIfNeeded()
    }
    
    
    
}

