//
//  CellAddRecipeOption.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import UIKit

class CellAddRecipeOption: UITableViewCell {

    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var imgViewTitle: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func handleCell(dictData:[String:Any]) {
        imgViewTitle.image = UIImage.init(named: dictData["image_name"] as! String)
        lblTitle.text = dictData["title"] as? String
        lblTitle.textColor = dictData["text_color"] as? UIColor
        lblDesc.text = dictData["desc"] as? String
        lblDesc.textColor = dictData["text_color"] as? UIColor
        viewBack.backgroundColor = dictData["back_color"] as? UIColor
        
    }
}
