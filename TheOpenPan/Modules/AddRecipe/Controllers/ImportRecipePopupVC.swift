//
//  ImportRecipePopupVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/21/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import SkyFloatingLabelTextField
import SwiftSoup
import SwiftyJSON
import Alamofire
import MultiProgressView

var tmpDocFolderName = "addNewRecipeFolder"

var strErrorForRecipeNotFound = "Sorry, we are unable to find recipe data on this url."

class ImportRecipePopupVC: UIViewController {
    
    @IBOutlet weak var viewBackAlpha            : UIView!
    @IBOutlet weak var viewCenter               : UIView!
    @IBOutlet weak var btnCancel                : UIButton!
    @IBOutlet weak var btnImport                : UIButton!
    @IBOutlet weak var cntrWidth                : NSLayoutConstraint!
    @IBOutlet weak var txtUrl                   : SkyFloatingLabelTextField!
    @IBOutlet weak var progressView             : MultiProgressView!
    
    var arrImportedKeywords                     = [Keyword]()
    var arrImportedCuisineTags                  = [Category_tag]()
    var arrImportedMealTypeTags                 = [Category_tag]()
    
    typealias Block                             = (AddNewRecipeData)->()
    typealias BlockVoid                         = ()->()
    var compBlockError                          : BlockVoid!
    var compBlockWithUrl                        : Block!
    var doc                                     : Document!
    var addNewRecipeData                        = AddNewRecipeData.init()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLayout()
        
        //        self.txtUrl.text = "https://recipes.timesofindia.com/recipes/cheese-pizza/rs53110049.cms"
        //        self.txtUrl.text = "https://www.indianhealthyrecipes.com/pizza-recipe-make-pizza/"
        //        self.txtUrl.text = "https://www.vegrecipesofindia.com/veg-pizza-recipe-vegetable-pizza/"
        //        self.txtUrl.text = "https://www.simplyrecipes.com/recipes/homemade_pizza/"
        //        self.txtUrl.text = "https://www.sailusfood.com/how-to-make-pizza-home/"
        //        self.txtUrl.text = "https://cafedelites.com/creamy-garlic-shrimp-cauliflower-rice-risotto/"
        //        self.txtUrl.text = "https://theopenpan.sg/recipes/smashed-avocado-ham-and-poached-eggs-on-toast/"
        
        //        self.txtUrl.text = "https://theopenpan.sg/recipes/carrot-bundt-cake-with-cream-cheese-glaze/"
        
        //        self.txtUrl.text = "https://dailycookingquest.com/clay-pot-chicken-rice-in-rice-cooker.html#recipe-card-container"
        
        //        self.txtUrl.text = "https://cafedelites.com/creamy-garlic-shrimp-cauliflower-rice-risotto/"
        
//        self.txtUrl.text = "https://cafedelites.com/steak-creamy-garlic-shrimp/"
//        self.txtUrl.text = "https://www.delish.com/cooking/recipe-ideas/a29504585/slow-cooker-pork-shoulder-recipe/"
//        self.txtUrl.text = "https://tasty.co/recipe/scalloped-potatoes"
//        self.txtUrl.text = "https://tasty.co/recipe/valentine-s-day-red-velvet-truffles"
        
//        self.txtUrl.text = "https://www.biggerbolderbaking.com/microwave-christmas-pudding/"
//        self.txtUrl.text = "https://noobcook.com/stir-fried-sweet-potato-leaves-in-chilli/2/"
        
        
//        self.txtUrl.text = "https://noobcook.com/stir-fried-sweet-potato-leaves-in-chilli/2/"

//        self.txtUrl.text = "https://cafedelites.com/creamy-garlic-shrimp-cauliflower-rice-risotto/"
//        self.txtUrl.text = "https://recipes.timesofindia.com/recipes/chilli-fish/rs72158249.cms"
//        self.txtUrl.text = "https://recipes.timesofindia.com/recipes/red-velvet-fudge/rs74002796.cms"
        

//        if let myString = UIPasteboard.general.string {
//            self.txtUrl.text = myString
//        }
        
        
        self.progressView.dataSource = self
        self.progressView.clipsToBounds = true
        
    }
    
    func setupLayout() {
        
        if ssw <= 320 {
            self.cntrWidth.constant = 290
        }
        
        self.viewBackAlpha.alpha = 0
        
        self.btnCancel.mk_addTapHandler { (btn) in
            hapSoft()
            self.dismiss(comp: nil)
        }
        
        self.btnImport.mk_addTapHandler { (btn) in
            self.btnImportTapped(btn: btn)
        }
        
        self.viewBackAlpha.addTapGesture { (gesture) in
            hapSoft()
            if self.btnCancel.isEnabled {
                self.dismiss(comp: nil)
            }
        }
        
        
    }
    
    func btnImportTapped(btn:UIButton) {
        
        if self.txtUrl.isEmpty() {
            hapError()
            self.txtUrl.shake()
            return
        }
        
        guard let strText = self.txtUrl.text else {
            hapError()
            self.txtUrl.shake()
            return
        }
        
        if strText.contains("http") == false {
            hapError()
            self.txtUrl.shake()
            return
        }
        
        guard let url = URL.init(string: strText) else {
            hapError()
            self.txtUrl.shake()
            return
        }
        
        hapSoft()
        self.txtUrl.resignFirstResponder()
        self.addNewRecipeData.web_recipe_url = self.txtUrl.text ?? ""
        
        self.btnImport.loadingIndicator(true)
        self.btnCancel.mk_makeEnable(false)
        self.importDataUrl(url: url)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.showBackView()
        
    }
    
    func showBackView() {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 1
        }) { (s) in
            
        }
    }
    
    
    func dismiss(comp:(()-> Void)?) {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBackAlpha.alpha = 0
        }) { (s) in
            self.dismiss(animated: true, completion: comp)
        }
    }
    
}

//MARK:- Import Recipe
extension ImportRecipePopupVC {
    
    
    func importDataUrl(url:URL) {
        
        
        let request = URLRequest(url: url)
        // Set headers
        //            request.allHTTPHeaderFields = ["Content-Type" : "application/json"]
        
        let completionHandler = {(data: Data?, response: URLResponse?, error: Error?) -> Void in
            // Do something
            
            if let err = error {
                print(err)
                DispatchQueue.main.sync {
                    self.hideActivityIndicator()
                    BasicFunctions.showError(strError: "Something went wrong with the server.".localize())
                    return
                }
            }
            
            guard let data = data else { return }
            
            DispatchQueue.main.sync {
                
                do {
                    if let string = String(data: data, encoding: .utf8) {
                        self.doc = try parseBodyFragment(string)
                        self.handleRespone()
                    } else {
                        BasicFunctions.showError(strError: strErrorForRecipeNotFound)
                        self.hideActivityIndicator()
                    }
                } catch {
                    print("error : \(error)")
                    BasicFunctions.showError(strError: strErrorForRecipeNotFound)
                    self.hideActivityIndicator()
                }
            }
        }
        
        URLSession.shared.dataTask(with: request, completionHandler: completionHandler).resume()
    }
    
    func hideActivityIndicator() {
        self.btnImport.loadingIndicator(false)
        self.btnCancel.mk_makeEnable(true)
    }
    
    func handleRespone() {
        
        do {
            
            var isFoundJson = false
            let elements = try doc.getAllElements()
            for element in elements {
                switch element.tagName() {
                case "div" :
                    break
                case "script" :
                    
                    let foundJsonType = try element.attr("type")
                    if foundJsonType == "application/ld+json" {
                        
                        let rawJsonString = try element.html()
                        let dictData = convertToDictionary(text: rawJsonString)
                        if let dictData = dictData {
                            if let _ = getActualRecipeDict(dict: dictData) {
                                isFoundJson = true
                                _ = self.parseRecipeDict(jsonDict: dictData)
                            }
                        }
                        
                        let arrayData = convertToArray(text: rawJsonString)
                        if let finalArray = arrayData {
                            for dictSub in finalArray {
                                if let _ = getActualRecipeDict(dict: dictSub) {
                                    isFoundJson = true
                                    _ = self.parseRecipeDict(jsonDict: dictSub)
                                }
                            }
                        }
                    }
                    
                default:
                    break
                    
                }
            }
            
            if isFoundJson == false {
                
                self.html_handleRecipeImage()
                self.html_handleRecipeRatings()
                
                self.html_recipeYield()
                self.html_handleTime()
                let strRecipeName       = self.html_handleRecipeName()
                let arrDirections       = self.html_handleDirections()
                let arrIngredients      = self.html_handleIngredients()
                
                if arrDirections.count > 0 && arrIngredients.count > 0 && strRecipeName != nil {
                    
                    self.addNewRecipeData.title = strRecipeName ?? ""
                    
                    
                    for (i,d) in arrDirections.enumerated() {
                        let direction_list = Direction_list.init(step: i+1, description: d)
                        self.addNewRecipeData.direction_list.append(direction_list)
                    }
                    let dict = ["ingredient":arrIngredients]
                    //                    self.getRecipeIngredientsData_edamam(dictAllData: dict) { (formattedResponseDict) in
                    //                        self.parseResponseOfEdamam(formattedResponseDict: formattedResponseDict)
                    //                    }
                    self.parseIngredientsApiCall(dictAllData: dict)
                    
                } else {
                    
                    if compBlockError != nil {
                        compBlockError()
                    } else {
                        hapError()
                        self.txtUrl.shake()
                        self.hideActivityIndicator()
                        BasicFunctions.showError(strError: strErrorForRecipeNotFound)
                    }

                }
            }
        } catch {
            print(error)
            DispatchQueue.main.sync {
                
                hapError()
                self.txtUrl.shake()
                self.hideActivityIndicator()
                
            }
        }
        
        
    }
    
    func parseRecipeDict(jsonDict:[String:Any]) -> Bool {
        
        if let finalRecipeDict = self.getActualRecipeDict(dict: jsonDict) {
            
            self.getAllData(jsonDict: finalRecipeDict)
//            self.getRecipeIngredientsData_edamam(dictAllData: finalRecipeDict) { (formattedResponseDict) in
//                self.parseResponseOfEdamam(formattedResponseDict: formattedResponseDict)
//            }
            self.parseIngredientsApiCall(dictAllData: finalRecipeDict)
            return true
        }
        return false
    }
    
    func checkVideoAndDownload() {
        
        
        //        if self.addNewRecipeData.imported_media_url != "" {
        //            // Imported Video Url (imported_media_url) Found
        //
        //            Alamofire.download(self.addNewRecipeData.imported_media_url, method: .get, parameters: [:], encoding: JSONEncoding.default, headers: nil) { (url, res) -> (destinationURL: URL, options: DownloadRequest.DownloadOptions) in
        //                let directoryURLs = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        //                if !directoryURLs.isEmpty {
        //                    return (directoryURLs[0].appendingPathComponent("\(Date().mk_ticks).mp4"), [])
        //                }
        //                return (url,[])
        //            }.downloadProgress(closure: { (progress) in
        //                //progress closure
        //                print(progress.fractionCompleted)
        //
        //                self.progressView.isHidden = false
        //                self.progressView.setProgress(section: 0, to: Float.init(progress.fractionCompleted))
        //                self.btnImport.loadingIndicator(false)
        //                let intProgress = Int((progress.fractionCompleted.roundTo(places: 2)) * 100)
        //                self.btnImport.setTitle("\(intProgress == 0 ? 1 : intProgress) %", for: .normal)
        //                self.btnImport.setTitleColor(#colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1), for: .normal)
        //                self.btnImport.isEnabled = false
        //
        //            }).response(completionHandler: { (DefaultDownloadResponse) in
        //                //here you able to access the DefaultDownloadResponse
        //                //result closure
        //
        //                self.btnImport.isEnabled = true
        //
        //                if let err = DefaultDownloadResponse.error {
        //                    self.dismiss {
        //                        BasicFunctions.showError(strError: err.localizedDescription)
        //                    }
        //                }
        //
        //                if let url = DefaultDownloadResponse.destinationURL?.absoluteString {
        //                    self.addNewRecipeData.media_url = url
        //                    self.addNewRecipeData.is_video = true.mk_intValue
        //                    if let urlDestUrl = DefaultDownloadResponse.destinationURL {
        //                        let thumbnailImage = Utilities.generateThumbnail(path: urlDestUrl)
        //                        if let filePath = thumbnailImage.mk_saveToDocuments(filename: String(Date().mk_ticks), folderName: tmpDocFolderName) {
        //                            self.addNewRecipeData.video_thumb_url = filePath
        //                        }
        //                    }
        //
        //                    self.dismiss {
        //                        self.compBlockWithUrl(self.addNewRecipeData)
        //                    }
        //                } else {
        //                    self.dismiss {
        //                        self.addNewRecipeData.imported_media_url = ""
        //                        self.dismiss {
        //                            self.compBlockWithUrl(self.addNewRecipeData)
        //                        }
        //                    }
        //                }
        //
        //            })
        //
        //
        //        } else {
        
        // Imported Video Url (imported_media_url) Not Found
        self.dismiss {
            self.compBlockWithUrl(self.addNewRecipeData)
        }
        //        }
        
        
    }
    
    
    func parseResponseOfEdamam(formattedResponseDict:[String:Any]?) {
        
        print(formattedResponseDict as Any)
        
        guard let dictRes = formattedResponseDict else {
            self.checkVideoAndDownload()
            return
        }
        
        if let jsonDict = JSON(dictRes).dictionary {
            
            var arrIngredientList = [Ingredient_list]()
            
            if let arrIngredients = jsonDict["ingredients"]?.array {
                for objIngredient in arrIngredients {
                    
                    if let parsed = objIngredient["parsed"].array?.first {
                        
                        
                        let measure = parsed["measure"].stringValue
                        
                        if let doubleQty = parsed["quantity"].double {
                            if doubleQty.rounded(.up) == doubleQty.rounded(.down){
                                //number is integer
                                var objIngredientList = Ingredient_list.init(unit_name: measure, name: parsed["foodMatch"].stringValue, note: "", qty: parsed["quantity"].stringValue)
                                if objIngredientList.qty == "0" || objIngredientList.qty == "0.0" {
                                    objIngredientList.qty = ""
                                }
                                
                                arrIngredientList.append(objIngredientList)
                            } else {
                                //number is not integer
                                var objIngredientList = Ingredient_list.init(unit_name: measure, name: parsed["foodMatch"].stringValue, note: "", qty: "\(doubleQty.roundTo(places: 2))")
                                if objIngredientList.qty == "0" || objIngredientList.qty == "0.0" {
                                    objIngredientList.qty = ""
                                }
                                arrIngredientList.append(objIngredientList)
                            }
                        } else {
                            var objIngredientList = Ingredient_list.init(unit_name: measure, name: parsed["foodMatch"].stringValue, note: "", qty: parsed["quantity"].stringValue)
                            if objIngredientList.qty == "0" || objIngredientList.qty == "0.0" {
                                objIngredientList.qty = ""
                            }
                            arrIngredientList.append(objIngredientList)
                        }
                    }
                }
            }
            
            self.addNewRecipeData.ingredient_list = arrIngredientList
        }
        
        self.checkVideoAndDownload()
        
    }
    
    
    
    
    func getFinalRecipeJsonDict(jsonDict:[String:Any]) -> [String:Any]? {
        
        let isFoundDict = self.isDictionaryOfRecipe(jsonDict: jsonDict)
        if isFoundDict {
            return jsonDict
        }
        
        for key1 in jsonDict.keys {
            
            //Has Array
            if let arrDict = jsonDict[key1] as? [[String:Any]] {
                for childDict in arrDict {
                    
                    let isFoundDict = self.isDictionaryOfRecipe(jsonDict: childDict)
                    if isFoundDict {
                        return childDict
                    }
                    
                    for key2 in childDict.keys {
                        if let dict = childDict[key2] as? [String:Any] {
                            let isFoundDict = self.isDictionaryOfRecipe(jsonDict: dict)
                            if isFoundDict {
                                return dict
                            }
                        }
                    }
                    
                    
                }
            }
            
            //Has Dict
            if let dict = jsonDict[key1] as? [String:Any] {
                let isFoundDict = self.isDictionaryOfRecipe(jsonDict: dict)
                if isFoundDict {
                    return dict
                }
            }
        }
        return nil
    }
    
    
    
    
    
    func getActualRecipeDict(dict:[String:Any]) -> [String:Any]?  {
        
        if let jsonDict = getFinalRecipeJsonDict(jsonDict: dict) {
            
            let isFoundDictTopLevel = self.isDictionaryOfRecipe(jsonDict: jsonDict)
            
            if isFoundDictTopLevel {
                //Top Level
                print("isFoundDictTopLevel\(isFoundDictTopLevel)")
                return jsonDict
                
            } else {
                
                // Search at child level
                for key in jsonDict.keys {
                    
                    //Found Dictionary
                    if let dictSub = jsonDict[key] as? [String:Any] {
                        
                        let isFoundDictChildLevel1 = self.isDictionaryOfRecipe(jsonDict: dictSub)
                        print("isFoundDictChildLevel1\(isFoundDictChildLevel1)")
                        if isFoundDictChildLevel1 {
                            return dictSub
                        }
                    }
                    
                    //Found Array
                    if let arrayDict = jsonDict[key] as? [[String:Any]] {
                        for dictSub in arrayDict {
                            let isFoundDictChildLevel1 = self.isDictionaryOfRecipe(jsonDict: dictSub)
                            if isFoundDictChildLevel1 {
                                print("isFoundDictChildLevel1\(isFoundDictChildLevel1)")
                                return dictSub
                            }
                        }
                    }
                    
                    
                    
                }
            }
            
        }
        
        return nil
    }
    
    
    
    func displayFormattedIngredients(arrIngredients:[[String:Any]]?) {
        
        if let arrIngredients = arrIngredients {
            for dictIngredient in arrIngredients {
                //                        {
                //                          "unit": "cup",
                //                          "input": "2 cups shredded Cheddar cheese",
                //                          "name": "Cheddar cheese",
                //                          "qty": "2",
                //                          "comment": "shredded"
                //                        },
                let unit = (dictIngredient["unit"] as? String) ?? ""
                let input = (dictIngredient["input"] as? String) ?? ""
                let name = (dictIngredient["name"] as? String) ?? ""
                let qty = (dictIngredient["qty"] as? String) ?? ""
                let comment = (dictIngredient["comment"] as? String) ?? ""
                
                
                print("\n\nunit: \(unit) \nname: \(name) \nqty: \(qty) \ncomment: \(comment)\n")
                
            }
        }
        
        
    }
    
    
}







extension ImportRecipePopupVC {
    
    func replaceHtmlSymbols(strText:String) -> String {
        
        let mutStrText = strText
        if mutStrText.contains("&#188;") {
            return mutStrText.replacingOccurrences(of: "&#188;", with: "1/4")
        }
        if mutStrText.contains("¼") {
            return mutStrText.replacingOccurrences(of: "¼", with: "1/4")
        }
        
        if mutStrText.contains("&#189;") {
            return mutStrText.replacingOccurrences(of: "&#189;", with: "1/2")
        }
        if mutStrText.contains("½") {
            return mutStrText.replacingOccurrences(of: "½", with: "1/2")
        }
        
        
        if mutStrText.contains("&#190;") {
            return mutStrText.replacingOccurrences(of: "&#190;", with: "3/4")
        }
        if mutStrText.contains("¾") {
            return mutStrText.replacingOccurrences(of: "¾", with: "3/4")
        }
        
        
        if mutStrText.contains("&#8528;") {
            return mutStrText.replacingOccurrences(of: "&#8528;", with: "1/7")
        }
        if mutStrText.contains("⅐") {
            return mutStrText.replacingOccurrences(of: "⅐", with: "1/7")
        }
        
        
        if mutStrText.contains("&#8529;") {
            return mutStrText.replacingOccurrences(of: "&#8529;", with: "1/9")
        }
        if mutStrText.contains("⅑") {
            return mutStrText.replacingOccurrences(of: "⅑", with: "1/9")
        }
        
        
        
        if mutStrText.contains("&#8530;") {
            return mutStrText.replacingOccurrences(of: "&#8530;", with: "1/10")
        }
        if mutStrText.contains("⅒") {
            return mutStrText.replacingOccurrences(of: "⅒", with: "1/10")
        }
        
        
        if mutStrText.contains("&#8531;") {
            return mutStrText.replacingOccurrences(of: "&#8531;", with: "1/3")
        }
        if mutStrText.contains("⅓") {
            return mutStrText.replacingOccurrences(of: "⅓", with: "1/3")
        }
        
        
        
        if mutStrText.contains("&#8532;") {
            return mutStrText.replacingOccurrences(of: "&#8532;", with: "2/3")
        }
        if mutStrText.contains("⅔") {
            return mutStrText.replacingOccurrences(of: "⅔", with: "2/3")
        }
        
        
        if mutStrText.contains("&#8533;") {
            return mutStrText.replacingOccurrences(of: "&#8533;", with: "1/5")
        }
        if mutStrText.contains("⅕") {
            return mutStrText.replacingOccurrences(of: "⅕", with: "1/5")
        }
        
        
        if mutStrText.contains("&#8534;") {
            return mutStrText.replacingOccurrences(of: "&#8534;", with: "2/5")
        }
        if mutStrText.contains("⅖") {
            return mutStrText.replacingOccurrences(of: "⅖", with: "2/5")
        }
        
        
        
        if mutStrText.contains("&#8535;") {
            return mutStrText.replacingOccurrences(of: "&#8535;", with: "3/5")
        }
        if mutStrText.contains("⅗") {
            return mutStrText.replacingOccurrences(of: "⅗", with: "3/5")
        }
        
        
        if mutStrText.contains("&#8536;") {
            return mutStrText.replacingOccurrences(of: "&#8536;", with: "4/5")
        }
        if mutStrText.contains("⅘") {
            return mutStrText.replacingOccurrences(of: "⅘", with: "4/5")
        }
        
        
        if mutStrText.contains("&#8537;") {
            return mutStrText.replacingOccurrences(of: "&#8537;", with: "1/6")
        }
        if mutStrText.contains("⅙") {
            return mutStrText.replacingOccurrences(of: "⅙", with: "1/6")
        }
    
    
        if mutStrText.contains("&#8538;") {
            return mutStrText.replacingOccurrences(of: "&#8538;", with: "5/6")
        }
        if mutStrText.contains("⅚") {
            return mutStrText.replacingOccurrences(of: "⅚", with: "5/6")
        }
        
        
        
        if mutStrText.contains("&#8539;") {
            return mutStrText.replacingOccurrences(of: "&#8539;", with: "1/8")
        }
        if mutStrText.contains("⅛") {
            return mutStrText.replacingOccurrences(of: "⅛", with: "1/8")
        }
        
        
        
        if mutStrText.contains("&#8540;") {
            return mutStrText.replacingOccurrences(of: "&#8540;", with: "3/8")
        }
        if mutStrText.contains("⅜") {
            return mutStrText.replacingOccurrences(of: "⅜", with: "3/8")
        }
        
        
        if mutStrText.contains("&#8541;") {
            return mutStrText.replacingOccurrences(of: "&#8541;", with: "5/8")
        }
        if mutStrText.contains("⅝") {
            return mutStrText.replacingOccurrences(of: "⅝", with: "5/8")
        }
        
        
        if mutStrText.contains("&#8542;") {
            return mutStrText.replacingOccurrences(of: "&#8542;", with: "7/8")
        }
        if mutStrText.contains("⅞") {
            return mutStrText.replacingOccurrences(of: "⅞", with: "7/8")
        }
        return strText
    }
    
}




extension ImportRecipePopupVC : MultiProgressViewDataSource {
    
    func numberOfSections(in progressView: MultiProgressView) -> Int {
        return 1
    }
    func progressView(_ progressView: MultiProgressView, viewForSection section: Int) -> ProgressViewSection {
        let sectionView = ProgressViewSection.init()
        sectionView.backgroundColor = #colorLiteral(red: 0.631372549, green: 0.7764705882, blue: 0.7254901961, alpha: 1)
        return sectionView
    }
    
}



//MARK: - HTML PARSING
extension ImportRecipePopupVC {
    
    func html_handleDirections() -> [String] {
        
        var arrDirections = [String]()
        do {
            
            let arrRecipeInstructions = try self.doc.getElementsByAttributeValue("itemprop", "recipeInstructions").array()
            
            if arrRecipeInstructions.first?.tag().getName() == "ol" {
                if let items = arrRecipeInstructions.first?.children().array() {
                    for element in items {
                        arrDirections.append(try element.text())
                    }
                    return arrDirections
                }
            }
            
            if let childsLISTlitags = arrRecipeInstructions.first?.children().first()?.children() {
                for o in childsLISTlitags {
                    arrDirections.append(try o.text())
                }
                if arrDirections.count > 0 {
                    return arrDirections
                }
            }
            
            
            
            if arrRecipeInstructions.count > 1 {
                for (_,child) in arrRecipeInstructions.enumerated() {
                    arrDirections.append(try child.text())
                }
            } else {
                
                
                if let arrParentRecipeInstructions = try self.doc.getElementsByAttributeValue("itemprop", "recipeInstructions").array().first {
                    if arrParentRecipeInstructions.children().count > 1 {
                        
                        for (_,child) in arrParentRecipeInstructions.children().array().enumerated() {
                            if (try? child.html()) != "&nbsp;" {
                                arrDirections.append(try child.text())
                            }
                        }
                    } else {
                        let htmlStringDescription = try arrParentRecipeInstructions.text()
                        arrDirections.append(htmlStringDescription)
                    }
                    
                    if arrDirections.count == 0 {
                        if let nextElement = try? self.doc.getElementsByAttributeValue("itemprop", "recipeInstructions").array().first?.parent()?.nextElementSibling() {
                            if nextElement.children().count > 1 {
                                for (_,child) in nextElement.children().enumerated() {
                                    arrDirections.append(try child.text())
                                }
                            }
                        }
                    }
                }
            }
            
        } catch {
            print(error)
        }
        
        return arrDirections
    }
    
    
    func html_handleIngredients() -> [String] {
        var arrIngredients = [String]()
        do {
            
            let arrRecipeIngredientsFound = try self.doc.getElementsByAttributeValue("itemprop", "recipeIngredient").array()
            
            for (_,ingredient) in arrRecipeIngredientsFound.enumerated() {
                arrIngredients.append(try ingredient.text())
            }
            
            if arrIngredients.count == 0 {
                
                let arrAllItemProps = try self.doc.getElementsByAttribute("itemprop").array()
                for itemProp in arrAllItemProps {
                    let strItemPropValue: String = try itemProp.attr("itemprop");
                    if strItemPropValue.contains("ingredient") {
                        arrIngredients.append(try itemProp.text())
                    }
                }
            }
        } catch {
            print(error)
        }
        return arrIngredients
    }
    
    
    func html_handleRecipeName() -> String? {
        do {
            if let nameFound = try self.doc.getElementsByAttributeValue("itemprop", "name").array().first {
                return try nameFound.text()
            } else {
                return try self.doc.title()
            }
        } catch {
            print(error)
        }
        return nil
    }
    
    
    func html_handleRecipeImage() {
        do {
            let srcs: Elements = try doc.select("img[src]")
            for src in srcs {
                print(src)
                if let foundImage = try src.getElementsByAttributeValue("itemprop", "image").array().first {
                    let linkImageSrc: String = try foundImage.attr("src");
                    print(linkImageSrc)
                    if let urlImage = URL.init(string: linkImageSrc) {
                        if let _ = try? Data.init(contentsOf: urlImage) {
                            self.saveRecipeImageDataToLocal(url: urlImage)
                        } else {
                            let linkImageSrcData: String = try foundImage.attr("src-data");
                            if let urlImage = URL.init(string: linkImageSrcData) {
                                if let _ = try? Data.init(contentsOf: urlImage) {
                                    self.saveRecipeImageDataToLocal(url: urlImage)
                                } else {
                                    if linkImageSrc.hasPrefix("\\") {
                                        if let url = URL.init(string: "http:\(linkImageSrc)") {
                                            if let _ = try? Data.init(contentsOf: url) {
                                                self.saveRecipeImageDataToLocal(url: url)
                                            }
                                        }
                                    }
                                }
                            } else {
                                if linkImageSrc.hasPrefix("//") {
                                    if let url = URL.init(string: "http:\(linkImageSrc)") {
                                        if let _ = try? Data.init(contentsOf: url) {
                                            self.saveRecipeImageDataToLocal(url: url)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch {
            print(error)
        }
    }
    
    func html_handleRecipeRatings() {
        //        aggregateRating
        
        if let ratingValue = try? self.doc.getElementsByAttributeValue("itemprop", "ratingValue").text() {
            self.addNewRecipeData.aggregateRatingValue = ratingValue
        }
        if let reviewCount = try? self.doc.getElementsByAttributeValue("itemprop", "reviewCount").text() {
            self.addNewRecipeData.aggregateRatingCount = reviewCount
        } else {
            if let ratingCount = try? self.doc.getElementsByAttributeValue("itemprop", "ratingCount").text() {
                self.addNewRecipeData.aggregateRatingCount = ratingCount
            }
        }
        
        print("------------------------------------------")
        print("RatingValue : \(self.addNewRecipeData.aggregateRatingValue)")
        print("ReviewCount : \(self.addNewRecipeData.aggregateRatingCount)")
        print("------------------------------------------")
        
    }
    
    
    
    func html_recipeYield() {
        do {
            if let yieldFound = try self.doc.getElementsByAttributeValue("itemprop", "recipeYield").array().first {
                self.handleYields(strServes: try yieldFound.text())
            }
        } catch {
            print(error)
        }
    }
    
    
    func html_handleTimeAndHours(itempropname:String? = nil) -> (String?,String?)? {
        do {
            if let itempropname = itempropname {
                
                let arrTime = try self.doc.getElementsByAttributeValue("itemprop", itempropname)
                
                if let arrAttributes = arrTime.array().first?.getAttributes()?.asList() {
                    for attr in arrAttributes {
                        if attr.getValue().contains("PT") {
                            let dtc = attr.getValue().getHoursMinutes()
                            return dtc
                        } else {
                            // Starting with P // P0Y0M0DT0H5M0.000S
                            
                            if attr.getValue().hasPrefix("P") && attr.getValue().contains(".") {
                                let arrTime = attr.getValue().components(separatedBy: ".")
                                if let timeValue = arrTime.first {
                                    let dtc = timeValue.getHoursMinutes()
                                    return dtc
                                }
                            }
                        }
                    }
                }
            }
            
        } catch {
            print(error)
        }
        return nil
    }
    
    
    
    func html_handleTime(itempropname:String? = nil) {
        
        var cookTime : (String?,String?)!
        var prepTime : (String?,String?)!
        var totalTime : (String?,String?)!
        
        cookTime = html_handleTimeAndHours(itempropname: "cookTime")
        prepTime = html_handleTimeAndHours(itempropname: "prepTime")
        totalTime = html_handleTimeAndHours(itempropname: "totalTime")
        
        
        if totalTime != nil {
            
            if let totalHours = totalTime.0?.integer {
                self.addNewRecipeData.ready_time_hours = totalHours
            }
            if let totalMins = totalTime.1?.integer {
                self.addNewRecipeData.ready_time_mins = totalMins
            }
            
        } else {
            
            if cookTime != nil && prepTime != nil {
                
                //cookTime+prepTime final
                if let cook_totalHours = cookTime.0?.integer {
                    if let prep_totalHours = prepTime.0?.integer {
                        self.addNewRecipeData.ready_time_hours = cook_totalHours+prep_totalHours
                    }
                }
                if let cook_totalMins = cookTime.1?.integer {
                    if let prep_totalMins = prepTime.1?.integer {
                        self.addNewRecipeData.ready_time_mins = cook_totalMins+prep_totalMins
                    }
                }
                
            } else if cookTime != nil && prepTime == nil {
                
                //cookTime final
                if let totalHours = cookTime.0?.integer {
                    self.addNewRecipeData.ready_time_hours = totalHours
                }
                if let totalMins = cookTime.1?.integer {
                    self.addNewRecipeData.ready_time_mins = totalMins
                }
            } else {
                
                
                if prepTime != nil {
                    
                    //prepTime final
                    if let totalHours = prepTime.0?.integer {
                        self.addNewRecipeData.ready_time_hours = totalHours
                    }
                    if let totalMins = prepTime.1?.integer {
                        self.addNewRecipeData.ready_time_mins = totalMins
                    }
                    
                }
                
                
            }
        }

    }

}
