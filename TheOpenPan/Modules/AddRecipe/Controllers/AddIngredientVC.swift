//
//  AddIngredientVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/5/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import TagListView
import UIKit
import SkyFloatingLabelTextField

class AddIngredientVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet var lblCategoryTitle          : UILabel!
    @IBOutlet var tblView                   : UITableView!
    @IBOutlet var btnDone                   : UIButton!
    @IBOutlet var txtIngredientUnitName     : SkyFloatingLabelTextField!
    
    
    //------------------------------------------
    //MARK: - Class Variables -
    typealias BlockSelectedTags         = (String)->()
    var completionBlock                 : BlockSelectedTags!
    
    var arrDictTags                     : [[String:Any]]!
    var strTitle                        = ""
    var strTypeName                     = ""
    var tagList                         : TagListView!
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        setupTableView()
        self.automaticallyAdjustsScrollViewInsets = false
        self.title = "Select Categories".localize()
        self.txtIngredientUnitName.text = strTypeName
        
        for dict in self.arrDictTags {
            let strName = dict["name"] as! String
            if strName == self.strTypeName {
                self.txtIngredientUnitName.text = ""
            }
        }
        
        
        self.btnDone.mk_addTapHandler { (btn) in
            hapSoft()
            
            if self.tagList.selectedTags().count == 0 {
                self.completionBlock(self.txtIngredientUnitName.text ?? "")
                self.dismiss(animated: true, completion: nil)
                return
            }
            self.completionBlock(self.strTypeName)
            self.dismiss(animated: true, completion: nil)
            
        }
        
        
        self.txtIngredientUnitName.mk_addAllEditingEventHandler { (txt) in
            if txt.text?.isEmpty == false {
                self.tagList.selectedTags().first?.isSelected = false
            }
        }
    }
    
    
    

    
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
           
    }
    
    
}



//------------------------------------------
//MARK: - Table View Setup Methods -
extension AddIngredientVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.estimatedRowHeight = 100
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellSelectCategoriesAddNewRecipe = tableView.dequeueReusableCell(withIdentifier: "CellSelectCategoriesAddNewRecipe", for: indexPath) as! CellSelectCategoriesAddNewRecipe
        
        guard let tagList = cellSelectCategoriesAddNewRecipe.tagList else {
            return cellSelectCategoriesAddNewRecipe
        }
        
        self.tagList = tagList
        tagList.removeAllTags()
        tagList.delegate = self
        tagList.enableRemoveButton = false
        tagList.textFont = UIFont.init(name: enum_font.regular.rawValue, size: 16)!
        
        
        for dict in self.arrDictTags {
            let tagView = tagList.addTag(dict["name"] as! String)
        }
        
        
        let tagView = tagList.tagViews.filter { (tg) -> Bool in
            tg.titleLabel?.text == self.strTypeName
        }
        
        if let f = tagView.first {
            f.isSelected = true
            self.txtIngredientUnitName.text = ""
        }
        
        return cellSelectCategoriesAddNewRecipe
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.layoutIfNeeded()
    }
    
}


// MARK: TagListViewDelegate
extension AddIngredientVC : TagListViewDelegate {
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        
        
        hapSoft()
        for tg in sender.tagViews {
            tg.isSelected = false
        }
        tagView.isSelected = !tagView.isSelected
        self.txtIngredientUnitName.text = ""
        self.strTypeName = title
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        
        sender.removeTagView(tagView)
    }
}
