//
//  EditDirectionVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/3/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import TagListView
import UIKit

class EditDirectionVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet var lblTitleDirection             : UILabel!
    @IBOutlet var btnDone                       : UIButton!
    @IBOutlet var btnRemove                     : UIButton!
    @IBOutlet weak var txtViewDirection         : UITextView!
    @IBOutlet weak var viewBottomTextView       : UIView!
    @IBOutlet weak var lblPlaceholderTextView   : UILabel!
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    typealias BlockDone             = (Direction)->()
    typealias BlockRemove           = (IndexPath)->()
    var completionBlockDone         : BlockDone!
    var completionBlockRemove       : BlockRemove!
    
    var objDirection                : Direction!
    var indexPath                   : IndexPath!
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.title = "Direction".localize()
        self.lblTitleDirection.text = "Point \((indexPath.row + 1).stringValue)"
        
        self.txtViewDirection.delegate = self
        
        let desc = (self.objDirection.desc ?? "").replacingOccurrences(of: "\n", with: "<br>")
        self.txtViewDirection.mk_setHTMLFromString2(htmlText: desc)
        self.textViewDidChange(self.txtViewDirection)
        
        self.btnDone.mk_addTapHandler { (btn) in
            hapSoft()
            
            if self.txtViewDirection.isEmpty() {
                hapError()
                self.txtViewDirection.shake()
                self.txtViewDirection.becomeFirstResponder()
                return
            }
            
            self.objDirection.desc = self.txtViewDirection.text
            self.completionBlockDone(self.objDirection)
            self.dismiss(animated: true, completion: nil)
        }
        
        self.btnRemove.mk_addTapHandler { (btn) in
            self.completionBlockRemove(self.indexPath)
            self.dismiss(animated: true, completion: nil)
        }
    }

    
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
        
    }
    

    
}


extension EditDirectionVC : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        lblPlaceholderTextView.isHidden = !textView.text.isEmpty
    }
}
