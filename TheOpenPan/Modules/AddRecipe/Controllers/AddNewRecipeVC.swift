//
//  AddNewRecipeVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/20/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import TagListView

class AddNewRecipeVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet weak var tblView: UITableView!
    
    //------------------------------------------
    //MARK: - Class Variables -
    var arrAllRecipeCategoriesList          = [Recipe_category]()
    
    var arrIngredints                       = [Ingredient]()
    var arrDirections                       = [Direction]()
    var arrAddedKeywords                    = [Keyword]()
    
    var arrSelectedCuisineTags              = [Category_tag]()
    var arrSelectedDietTags                 = [Category_tag]()
    var arrSelectedMealTypeTags             = [Category_tag]()
    
    var addNewRecipeData                    = AddNewRecipeData.init()
    
    var cellAddNewRecipeTitleImage          : CellAddNewRecipeTitleImage!
    var cellAddNewRecipeIngredients         : CellAddNewRecipeIngredients!
    var cellAddNewRecipeDirections          : CellAddNewRecipeDirections!
    var cellAddNewRecipeChooseCategories    : CellAddNewRecipeChooseCategories!
    var cellAddNewRecipeBottomServesBtns    : CellAddNewRecipeBottomServesBtns!
    
    var cellHeights: [IndexPath : CGFloat] = [:]
    
    var givenTitle                          : String?
    var isInEditMode                        = false
    var recipeIdEditMode                    = Int64()
    var editedDoneEditMode                  : (()->())!
    var edmamRecipeAdded                    : ((Int64)->())!
    var isEdmamRecipe                       = false
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
        
    }
    
    
    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.navigationController?.isNavigationBarHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        setupTableView()
        
        if let givenTitle = givenTitle {
            self.navigationItem.title = givenTitle
        } else {
            self.navigationItem.title = "Add New Recipe".localize()
        }
     
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeTabBar(hidden: true, animated: false, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        _ = FileManager.default.mk_removeFolderFromDocuments(folderName: tmpDocFolderName)
    }
}


//------------------------------------------
//MARK: - Table View Setup Methods -
extension AddNewRecipeVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        
        self.tblView.delegate = self
        self.tblView.dataSource = self
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
        self.tblView.setEditing(true, animated: true)
        self.tblView.allowsSelectionDuringEditing = true
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
            // Title and imageview
        case 0:
            return 1
            
            // Ingredients name, unit, qty //Header
        case 1:
            return self.arrIngredints.count
            
            // Ingredients' Items
        case 2:
            return 1
            
            // Directions' Items
        case 3:
            return self.arrDirections.count
            
            // Directions textfield //Header
        case 4:
            return 1
            
            // Choose Categories List
        case 5:
            return 1
            
            // Bottom part serves, hours, mins, btns
        case 6:
            return 1
            
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? 100.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.section {
            
            // Title and imageview
        case 0:
            if self.addNewRecipeData.web_recipe_url == "" {
                let cellAddNewRecipeTitleImage = tableView.dequeueReusableCell(withIdentifier: "CellAddNewRecipeTitleImage", for: indexPath) as! CellAddNewRecipeTitleImage

                cellAddNewRecipeTitleImage.handleCell(vc: self, indexPath: indexPath)
                
                self.cellAddNewRecipeTitleImage = cellAddNewRecipeTitleImage
                return cellAddNewRecipeTitleImage
            } else {
                let cellAddNewRecipeTitleImage = tableView.dequeueReusableCell(withIdentifier: "CellAddNewRecipeTitleImage_imported", for: indexPath) as! CellAddNewRecipeTitleImage

                cellAddNewRecipeTitleImage.handleCell(vc: self, indexPath: indexPath)
                
                self.cellAddNewRecipeTitleImage = cellAddNewRecipeTitleImage
                return cellAddNewRecipeTitleImage
            }
            
            // Ingredients' Items
        case 1:
            let cellAddNewRecipeIngredients_item = tableView.dequeueReusableCell(withIdentifier: "CellAddNewRecipeIngredients_item", for: indexPath) as! CellAddNewRecipeIngredients
            cellAddNewRecipeIngredients_item.handleCell(vc: self, indexPath: indexPath)
            return cellAddNewRecipeIngredients_item
            
            // Ingredients name, unit, qty
        case 2:
            let cellAddNewRecipeIngredients = tableView.dequeueReusableCell(withIdentifier: "CellAddNewRecipeIngredients", for: indexPath) as! CellAddNewRecipeIngredients

            cellAddNewRecipeIngredients.handleCellAddItem(vc: self, indexPath: indexPath)
            
            return cellAddNewRecipeIngredients
            
        // Directions textfield //Header
        case 3:
            let cellAddNewRecipeDirections_item = tableView.dequeueReusableCell(withIdentifier: "CellAddNewRecipeDirections_item", for: indexPath) as! CellAddNewRecipeDirections

            cellAddNewRecipeDirections_item.handleCell(vc: self, indexPath: indexPath)
                        
            return cellAddNewRecipeDirections_item
            
        // Directions' Items
        case 4:
            let cellAddNewRecipeDirections = tableView.dequeueReusableCell(withIdentifier: "CellAddNewRecipeDirections", for: indexPath) as! CellAddNewRecipeDirections

            cellAddNewRecipeDirections.handleCellAddItem(vc: self, indexPath: indexPath)
            
            
            return cellAddNewRecipeDirections
            
            
        // Choose Categories List
        case 5:
            let cellAddNewRecipeChooseCategories = tableView.dequeueReusableCell(withIdentifier: "CellAddNewRecipeChooseCategories", for: indexPath) as! CellAddNewRecipeChooseCategories
            
            self.cellAddNewRecipeChooseCategories = cellAddNewRecipeChooseCategories
            
            cellAddNewRecipeChooseCategories.handleCell(vc: self, indexPath: indexPath)
            
            cellAddNewRecipeChooseCategories.layoutIfNeeded()
            
            return cellAddNewRecipeChooseCategories
            
            
        // Bottom part serves, hours, mins, btns
        case 6:
            let cellAddNewRecipeBottomServesBtns = tableView.dequeueReusableCell(withIdentifier: "CellAddNewRecipeBottomServesBtns", for: indexPath) as! CellAddNewRecipeBottomServesBtns
            
            self.cellAddNewRecipeBottomServesBtns = cellAddNewRecipeBottomServesBtns
            cellAddNewRecipeBottomServesBtns.handleCell(vc: self, indexPath: indexPath)
            cellAddNewRecipeBottomServesBtns.layoutIfNeeded()
            
            return cellAddNewRecipeBottomServesBtns
            
            
        default:
            return UITableViewCell()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.section == 3 {
            let objDirection = self.arrDirections[indexPath.row]
            let objEditDirectionVC = apde.getController(vc_name: "EditDirectionVC", sb: .AddRecipe) as! EditDirectionVC
            objEditDirectionVC.objDirection = objDirection
            objEditDirectionVC.indexPath = indexPath
            objEditDirectionVC.completionBlockDone = { (objUpdatedDirection) in
                
                self.arrDirections[indexPath.row] = objUpdatedDirection
                self.tblView.beginUpdates()
                if let cellAddNewRecipeDirections = self.tblView.cellForRow(at: indexPath) as? CellAddNewRecipeDirections {
                    
                    let desc = (objUpdatedDirection.desc ?? "").replacingOccurrences(of: "\n", with: "<br>")
                    cellAddNewRecipeDirections.txtViewDirection.mk_setHTMLFromString2(htmlText: desc)
                    cellAddNewRecipeDirections.layoutSubviews()
                }
                self.tblView.endUpdates()
            }
            objEditDirectionVC.completionBlockRemove = { indexPathToRemove in

                hapHeavy()
                let objDirection = self.arrDirections[indexPathToRemove.row]
                objDirection.mr_deleteEntity(in: app_default_context)
                self.arrDirections.removeObject(objDirection)
                self.tblView.beginUpdates()
                self.tblView.deleteRows(at: [indexPath], with: .left)
                self.tblView.endUpdates()
    
                Timer.after(0.5) {
                    self.tblView.reloadSections(IndexSet.init(arrayLiteral: indexPath.section), with: .none)
                }
            }
            
            if #available(iOS 13.0, *) {
            } else {
                objEditDirectionVC.modalPresentationStyle = .overCurrentContext
            }
            UIViewController.CVC()?.present(objEditDirectionVC, animated: true, completion: nil)
            return
        }

        
        if indexPath.section == 1 {
            
            if let arrUnitOption = Unit_option.mr_findAllSorted(by: "displayIndex", ascending: true, in: app_default_context) as? [Unit_option] {
                
                //Object to edit
                let objIngredints = self.arrIngredints[indexPath.row]
                
                var mutArrUnitOption = arrUnitOption
                mutArrUnitOption.sort { (u1, u2) -> Bool in
                    (u1.tag_name_singular ?? "").localizedCompare((u1.tag_name_singular ?? "")) == .orderedAscending
                }
                
                var arrDictTags = [[String:Any]]()
                for unit_option in mutArrUnitOption {
                    var dict = [String:Any]()
                    dict["name"] = unit_option.tag_name_singular ?? ""
                    arrDictTags.append(dict)
                }
                
                let objEditIngredientVC = apde.getController(vc_name: "EditIngredientVC", sb: .AddRecipe) as! EditIngredientVC
                objEditIngredientVC.arrDictTags = arrDictTags
                objEditIngredientVC.strTitle = "Ingredient Units".localize()
                objEditIngredientVC.objIngredints = objIngredints
                
                objEditIngredientVC.completionBlockDone = { objUpdatedIngredient in
                    
                    self.arrIngredints[indexPath.row] = objUpdatedIngredient
                    self.tblView.beginUpdates()
                    if let cellAddNewRecipeIngredients = self.tblView.cellForRow(at: indexPath) as? CellAddNewRecipeIngredients {
                        cellAddNewRecipeIngredients.handleCell(vc: self, indexPath: indexPath)
                        cellAddNewRecipeIngredients.layoutSubviews()
                    }
                    self.tblView.endUpdates()
                    
                }
                objEditIngredientVC.completionBlockRemove = {
                    
                    hapHeavy()
                    let objIngredient = self.arrIngredints[indexPath.row]
                    objIngredient.mr_deleteEntity(in: app_default_context)
                    self.arrIngredints.removeObject(objIngredient)
                    self.tblView.beginUpdates()
                    self.tblView.deleteRows(at: [indexPath], with: .left)
                    self.tblView.endUpdates()
                    
                }
                
                
                if #available(iOS 13.0, *) {
                } else {
                    objEditIngredientVC.modalPresentationStyle = .overCurrentContext
                }
                
                UIViewController.CVC()?.present(objEditIngredientVC, animated: true, completion: nil)
            }
            return
        }
        
    }
    
    
    // Cell rearrange methods
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
//        print("canMoveRowAt indexPath: \(indexPath)")
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        print("sourceIndexPath : \(sourceIndexPath)")
        print("destinationIndexPath : \(destinationIndexPath)")
        
        
        if sourceIndexPath.section == 1 {
            
            let movedObject = self.arrIngredints[sourceIndexPath.row]
            arrIngredints.remove(at: sourceIndexPath.row)
            arrIngredints.insert(movedObject, at: destinationIndexPath.row)
            
//            if let cell = tableView.cellForRow(at: sourceIndexPath) as? CellAddNewRecipeIngredients {
//                cell.handleCell(vc: self, indexPath: sourceIndexPath)
//            }
//
//            if let cellDest = tableView.cellForRow(at: destinationIndexPath) as? CellAddNewRecipeIngredients {
//                cellDest.handleCell(vc: self, indexPath: destinationIndexPath)
//            }
            
            Timer.after(0.1) {
                tableView.reloadSections(IndexSet.init(arrayLiteral: 1), with: .fade)
            }
            
            return
        }
        
        let movedObject = self.arrDirections[sourceIndexPath.row]
        arrDirections.remove(at: sourceIndexPath.row)
        arrDirections.insert(movedObject, at: destinationIndexPath.row)
        
        Timer.after(0.1) {
            tableView.reloadSections(IndexSet.init(arrayLiteral: 3), with: .fade)
        }
        
    }
    
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        
        print(proposedDestinationIndexPath)
        
        if sourceIndexPath.section != proposedDestinationIndexPath.section {
            return sourceIndexPath
        }
        
        if sourceIndexPath.section == 1 {
            
//            if let cell = tableView.cellForRow(at: sourceIndexPath) as? CellAddNewRecipeIngredients {
//                cell.handleCell(vc: self, indexPath: sourceIndexPath)
//            }
//            if let cell = tableView.cellForRow(at: proposedDestinationIndexPath) as? CellAddNewRecipeIngredients {
//                cell.handleCell(vc:self, indexPath: proposedDestinationIndexPath)
//            }
//
            if sourceIndexPath.section == proposedDestinationIndexPath.section {
                return proposedDestinationIndexPath
            }
            
        }
        
        
        if let cell = tableView.cellForRow(at: sourceIndexPath) as? CellAddNewRecipeDirections {
            cell.recflectIndex(vc:self, indexPath: sourceIndexPath)
        }
        if let cell = tableView.cellForRow(at: proposedDestinationIndexPath) as? CellAddNewRecipeDirections {
            cell.recflectIndex(vc:self, indexPath: proposedDestinationIndexPath)
        }
        
        if sourceIndexPath.section == proposedDestinationIndexPath.section {
            return proposedDestinationIndexPath
        }
        
        return sourceIndexPath
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 3 {
            return true
        }
        if indexPath.section == 1 {
            return true
        }
        return false
    }

    
}








