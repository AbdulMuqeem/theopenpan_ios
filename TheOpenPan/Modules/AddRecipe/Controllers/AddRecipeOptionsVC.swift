//
//  AddRecipeOptionsVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/17/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class AddRecipeOptionsVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet weak var tblView: UITableView!
    
    //------------------------------------------
    //MARK: - Class Variables -
    
    var arrDictData = [["image_name":"AssetAddRecipeOption1",
                     "title":"Add Your Own Recipe",
                     "desc":"Save your own recipes into your cookbook and choose to share them with others on The Open Pan.",
                     "text_color":#colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1),
                     "back_color":#colorLiteral(red: 0.631372549, green: 0.7764705882, blue: 0.7254901961, alpha: 1)],
                    ["image_name":"AssetAddRecipeOption2",
                     "title":"Import a Web Recipe",
                     "desc":"Import recipes from anywhere on the web and save it in your cookbook",
                     "text_color":#colorLiteral(red: 0.7450980392, green: 0.6823529412, blue: 0.6549019608, alpha: 1),
                     "back_color":#colorLiteral(red: 0.937254902, green: 0.8941176471, blue: 0.8784313725, alpha: 1)]]
    
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {

        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        
//        self.navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        
        setupTableView()
        self.navigationItem.title = "Add a Recipe".localize()
        Utilities.logEventScreen(title: self.navigationItem.title ?? "")
    }
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupLayout()
        
        apde.showLoader()
        apde.getMasterDataFromServer { (objMasterData) in
            apde.hideLoader()
        }
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        changeTabBar(hidden: false, animated: false, completion: nil)
    }
}


//------------------------------------------
//MARK: - Table View Setup Methods -
extension AddRecipeOptionsVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        if #available(iOS 11.0, *) {
            self.tblView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellAddRecipeOption = tableView.dequeueReusableCell(withIdentifier: "CellAddRecipeOption", for: indexPath) as! CellAddRecipeOption

        cellAddRecipeOption.handleCell(dictData: self.arrDictData[indexPath.row])
        
        return cellAddRecipeOption
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        hapSoft()
        
        if indexPath.row == 0 { // Add Your Own Recipe
            
            let objAddNewRecipeVC = apde.getController(vc_name: "AddNewRecipeVC", sb: .AddRecipe) as! AddNewRecipeVC
            objAddNewRecipeVC.addNewRecipeData.is_own_recipe = true
            self.navigationController?.pushViewController(objAddNewRecipeVC, animated: true)
            
        } else { // Import a Web Recipe
            
            let objImportRecipePopupVC = apde.getController(vc_name: "ImportRecipePopupVC", sb: .AddRecipe) as! ImportRecipePopupVC
            objImportRecipePopupVC.compBlockWithUrl = { addNewRecipeData in

                let mutAddNewRecipeData = addNewRecipeData
                let objAddNewRecipeVC = makeReadyAddNewRecipeVcFromImport(addNewRecipeData: mutAddNewRecipeData, arrImportedKeywords:objImportRecipePopupVC.arrImportedKeywords, arrImportedCuisineTags:objImportRecipePopupVC.arrImportedCuisineTags, arrImportedMealTypeTags:objImportRecipePopupVC.arrImportedMealTypeTags)
                self.navigationController?.pushViewController(objAddNewRecipeVC, animated: true)
                
            }
            UIViewController.CVC()?.present(objImportRecipePopupVC, animated: true, completion: nil)
            
        }
        
    }
    
}


func makeReadyAddNewRecipeVcFromImport(addNewRecipeData:AddNewRecipeData,arrImportedKeywords:[Keyword],arrImportedCuisineTags:[Category_tag],arrImportedMealTypeTags:[Category_tag]) -> AddNewRecipeVC {
    
    let objAddNewRecipeVC = apde.getController(vc_name: "AddNewRecipeVC", sb: .AddRecipe) as! AddNewRecipeVC
    
    objAddNewRecipeVC.arrAddedKeywords              = arrImportedKeywords
    objAddNewRecipeVC.arrSelectedCuisineTags        = arrImportedCuisineTags
    objAddNewRecipeVC.arrSelectedMealTypeTags       = arrImportedMealTypeTags
    
    for (i,direction) in addNewRecipeData.direction_list.enumerated() {
        if let objDirection = Direction.mr_createEntity(in: app_default_context) {
            objDirection.step = Int64(i)
            
            let descDirection = convertFtoC(text: direction.description)
            
            objDirection.desc = descDirection
            objAddNewRecipeVC.arrDirections.append(objDirection)
        }
    }
    
    for (_,ingredient) in addNewRecipeData.ingredient_list.enumerated() {
        if let objIngredient = Ingredient.mr_createEntity(in: app_default_context) {
            
            let ingName = convertFtoC(text: ingredient.name)
            objIngredient.name      = ingName
            objIngredient.note      = ingredient.note
            objIngredient.unit_name = ingredient.unit_name
            objIngredient.qty       = ingredient.qty
            objAddNewRecipeVC.arrIngredints.append(objIngredient)
        }
    }
    
    
    objAddNewRecipeVC.addNewRecipeData = addNewRecipeData
    objAddNewRecipeVC.addNewRecipeData.is_own_recipe = false
    return objAddNewRecipeVC
}
