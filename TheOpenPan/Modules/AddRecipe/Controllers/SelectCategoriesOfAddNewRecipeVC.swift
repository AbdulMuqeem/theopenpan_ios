//
//  SelectCategoriesOfAddNewRecipeVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/20/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import TagListView
import UIKit

class SelectCategoriesOfAddNewRecipeVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    @IBOutlet var lblCategoryTitle: UILabel!
    @IBOutlet var tblViewSelectCategories: UITableView!
    @IBOutlet var btnDone: UIButton!
    
    var arrSelectedIds = [Int64]()
    var recipeCategoryListType : enum_tag_list_type!
    var arrDictTags : [[String:Any]]!
    var strTitle = ""
    var isSingleSelection = false
    
    //------------------------------------------
    //MARK: - Class Variables -
    typealias BlockSelectedTags = ([Int64])->()
    var completionBlock : BlockSelectedTags!
    
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        setupTableView()
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.title = "Select Categories".localize()
        
        if strTitle != "" {
            self.lblCategoryTitle.text = strTitle.uppercased()
        }
        
        self.btnDone.mk_addTapHandler { (btn) in
            hapSoft()
            
            if self.arrSelectedIds.count > 0 {
                self.completionBlock(self.arrSelectedIds)
            }
            
            self.dismiss(animated: true, completion: nil)
        }
    }

    
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
           
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        self.tblViewSelectCategories.layoutIfNeeded()
//        self.tblViewSelectCategories.layoutSubviews()
    }
    
}



//------------------------------------------
//MARK: - Table View Setup Methods -
extension SelectCategoriesOfAddNewRecipeVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblViewSelectCategories.delegate = self
        self.tblViewSelectCategories.dataSource = self
        self.tblViewSelectCategories.estimatedRowHeight = 100
        if #available(iOS 11.0, *) {
            self.tblViewSelectCategories.contentInsetAdjustmentBehavior = .never
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellSelectCategoriesAddNewRecipe = tableView.dequeueReusableCell(withIdentifier: "CellSelectCategoriesAddNewRecipe", for: indexPath) as! CellSelectCategoriesAddNewRecipe
        
        guard let tagList = cellSelectCategoriesAddNewRecipe.tagList else {
            return cellSelectCategoriesAddNewRecipe
        }
        
        tagList.removeAllTags()
        
        tagList.delegate = self
        tagList.enableRemoveButton = false
        tagList.textFont = UIFont.init(name: enum_font.regular.rawValue, size: 16)!
        
        if self.recipeCategoryListType != nil {
            
            let arrTags = self.recipeCategoryListType.getAllTags()
            if arrTags.count > 0 {
                for catTag in arrTags {
                    let tagView = tagList.addTag(catTag.tag_name ?? "")
                    tagView.id = catTag.tag_id
                }
            }
        } else {
            for dict in self.arrDictTags {
                let tagView = tagList.addTag(dict["name"] as! String)
                tagView.id = dict["id"] as! Int64
            }
        }
        
 
        
        for idSelected in self.arrSelectedIds {
            let tagView = tagList.tagViews.filter { (tg) -> Bool in
                tg.id == idSelected
            }
            tagView.first?.isSelected = true
        }
        
        
        
        return cellSelectCategoriesAddNewRecipe
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.layoutIfNeeded()
    }
    
}


// MARK: TagListViewDelegate
extension SelectCategoriesOfAddNewRecipeVC : TagListViewDelegate {
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        
        if isSingleSelection {
            hapSoft()
            for tg in sender.tagViews {
                tg.isSelected = false
            }
            tagView.isSelected = !tagView.isSelected
            self.arrSelectedIds.removeAll()
            self.arrSelectedIds.append(tagView.id)
        } else {
            
            tagView.isSelected = !tagView.isSelected
            
            if tagView.isSelected {
                hapSoft()
                if self.arrSelectedIds.contains(tagView.id) == false {
                    self.arrSelectedIds.append(tagView.id)
                }
                
            } else {
                hap(style: .light)
                if self.arrSelectedIds.contains(tagView.id) {
                    self.arrSelectedIds.removeObject(tagView.id)
                }
            }
        }
        
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        
        sender.removeTagView(tagView)
    }
}
