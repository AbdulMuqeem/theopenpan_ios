//
//  EditIngredientVC.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 2/3/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import TagListView
import UIKit
import SkyFloatingLabelTextField

class EditIngredientVC: UIViewController {
    
    //------------------------------------------
    //MARK: - Outlets -
    
    @IBOutlet var stackViewRecipeName               : UIStackView!
    @IBOutlet var stackViewNotes                    : UIStackView!
    @IBOutlet weak var txtRecipeName                : SkyFloatingLabelTextField!
    
    @IBOutlet var lblCategoryTitle                  : UILabel!
    @IBOutlet var tblViewSelectCategories           : UITableView!
    @IBOutlet var btnDone                           : UIButton!
    @IBOutlet var btnRemove                         : UIButton!
    
    
    @IBOutlet weak var txtNotes                     : SkyFloatingLabelTextField!
    @IBOutlet weak var txtIngredientName            : SkyFloatingLabelTextField!
    @IBOutlet weak var txtIngredientQty             : SkyFloatingLabelTextField!
    @IBOutlet var txtIngredientUnitName             : SkyFloatingLabelTextField!
    
    //------------------------------------------
    //MARK: - Class Variables -
    typealias BlockDoneButton           = (Ingredient)->()
    var completionBlockDone             : BlockDoneButton!
    typealias BlockRemoveButton         = ()->()
    var completionBlockRemove           : BlockRemoveButton!
    
    var arrDictTags                     : [[String:Any]]!
    var strTitle                        = ""
    var strRemoveButtonTitle            = "REMOVE"
    var strRecipeName                   = ""
    var tagList                         : TagListView!
    var objIngredints                   : Ingredient!
    
    var unit_name_original              : String!
    var ing_name_original               : String!
    var ing_qty_original                : String!
    var notes_original                  : String!
    
    var isFromShopListTab               = false
    
    
    //------------------------------------------
    //MARK: - Memory Management -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        
    }

    //------------------------------------------
    //MARK: - Custom Methods -
    @objc func setupLayout() {
        
        if self.isFromShopListTab == false {
            for v in self.stackViewNotes.arrangedSubviews {
                v.isHidden = true
            }
        }
        
        self.unit_name_original = self.objIngredints.unit_name
        self.ing_name_original = self.objIngredints.name
        self.ing_qty_original = self.objIngredints.qty
        self.notes_original = self.objIngredints.note
        
        setupTableView()
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.title = "Select Categories".localize()
        self.btnRemove.setTitle(self.strRemoveButtonTitle, for: .normal)
        
        if self.strRecipeName == "" {
            for v in self.stackViewRecipeName.arrangedSubviews {
                v.isHidden = true
            }
        } else {
            self.txtRecipeName.text = self.strRecipeName
        }
        
        self.txtIngredientQty.mk_addAllEditingEventHandler { (txt) in
            
            if isThisTextGreaterThenOne1(text: txt.text ?? "") {
                let strPlural                       = getPluralString(self.objIngredints.unit_name ?? "")
                self.txtIngredientUnitName.text     = strPlural
                self.objIngredints.unit_name        = strPlural
            } else {
                let strSingular                     = getSingularString(self.objIngredints.unit_name ?? "")
                self.txtIngredientUnitName.text     = strSingular
                self.objIngredints.unit_name        = strSingular
            }
            
            self.tblViewSelectCategories.reloadData()
        }
        
        self.txtIngredientUnitName.text = self.objIngredints.unit_name ?? ""
        for dict in self.arrDictTags {
            let strName = dict["name"] as! String
            if strName == self.objIngredints.unit_name {
                self.txtIngredientUnitName.text = ""
            }
        }
        
        self.txtIngredientName.text = self.objIngredints.name
        self.txtIngredientQty.text = self.objIngredints.qty
        self.txtNotes?.text = self.objIngredints.note ?? ""
        
        self.btnDone.mk_addTapHandler { (btn) in
            
            if self.txtIngredientName.isEmpty() {
                hapError()
                self.txtIngredientName.shake()
                self.txtIngredientName.becomeFirstResponder()
                return
            }
            
            if self.isFromShopListTab {
                if self.txtIngredientQty.isEmpty() {
                    hapError()
                    self.txtIngredientQty.shake()
                    self.txtIngredientQty.becomeFirstResponder()
                    return
                }
            }
            
            hapSoft()
            
            self.objIngredints.name = self.txtIngredientName.text
            self.objIngredints.qty = self.txtIngredientQty.text
            self.objIngredints.note = self.txtNotes?.text ?? ""
            
            if self.isFromShopListTab {
                let bool1 = self.unit_name_original     == self.objIngredints.unit_name
                let bool2 = self.ing_name_original      == self.txtIngredientName.text
                let bool3 = self.ing_qty_original       == self.txtIngredientQty.text
                let bool4 = self.notes_original         == self.txtNotes.text
                if (bool1 && bool2 && bool3 && bool4) == false {
                    self.completionBlockDone(self.objIngredints)
                }
            } else {
                let bool1 = self.unit_name_original     == self.objIngredints.unit_name
                let bool2 = self.ing_name_original      == self.txtIngredientName.text
                let bool3 = self.ing_qty_original       == self.txtIngredientQty.text
                if (bool1 && bool2 && bool3) == false {
                    self.completionBlockDone(self.objIngredints)
                }
            }
            
            self.dismiss(animated: true, completion: nil)
        }
        
        self.btnRemove.mk_addTapHandler { (btn) in
            self.completionBlockRemove()
            self.dismiss(animated: true, completion: nil)
        }
        
        self.txtIngredientUnitName.mk_addAllEditingEventHandler { (txt) in
            if txt.text?.isEmpty == false {
                self.tagList.selectedTags().first?.isSelected = false
            }
            self.objIngredints.unit_name = self.txtIngredientUnitName.text ?? ""
        }
    }

    
    
    //------------------------------------------
    //MARK: - View Life Cycle Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
        
    }

    
}



//------------------------------------------
//MARK: - Table View Setup Methods -
extension EditIngredientVC : UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        self.tblViewSelectCategories.delegate = self
        self.tblViewSelectCategories.dataSource = self
        self.tblViewSelectCategories.estimatedRowHeight = 100
        if #available(iOS 11.0, *) {
            self.tblViewSelectCategories.contentInsetAdjustmentBehavior = .never
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellSelectCategoriesAddNewRecipe = tableView.dequeueReusableCell(withIdentifier: "CellSelectCategoriesAddNewRecipe", for: indexPath) as! CellSelectCategoriesAddNewRecipe
        
        guard let tagList = cellSelectCategoriesAddNewRecipe.tagList else {
            return cellSelectCategoriesAddNewRecipe
        }
        
        self.tagList = tagList
        tagList.removeAllTags()
        tagList.delegate = self
        tagList.enableRemoveButton = false
        tagList.textFont = UIFont.init(name: enum_font.regular.rawValue, size: 16)!
        
        if let a = self.arrDictTags {
            
            var mutArrDictTags = a
            
            if isThisTextGreaterThenOne1(text: self.txtIngredientQty.text ?? "") {
                mutArrDictTags = removeSingular(true, arrUnitOption: mutArrDictTags)
                for plural in arrPlurals {
                    var dict = [String:Any]()
                    dict["name"] = plural
                    mutArrDictTags.append(dict)
                }
            }
            
            for dict in mutArrDictTags {
                tagList.addTag(dict["name"] as! String)
            }
        }
        
        let tagView = tagList.tagViews.filter { (tg) -> Bool in
            tg.titleLabel?.text == self.objIngredints.unit_name
        }
        
        if let f = tagView.first {
            f.isSelected = true
            self.txtIngredientUnitName.text = ""
        }
        
        return cellSelectCategoriesAddNewRecipe
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.layoutIfNeeded()
    }
    
}


// MARK: TagListViewDelegate
extension EditIngredientVC : TagListViewDelegate {
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        
        hapSoft()
        for tg in sender.tagViews {
            tg.isSelected = false
        }
        tagView.isSelected = !tagView.isSelected
        self.txtIngredientUnitName.text = ""
        self.objIngredints.unit_name = title
        
        
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        
        sender.removeTagView(tagView)
    }
}


func isThisTextGreaterThenOne1(text:String) -> Bool {
    if text != "" {
        
        var textFieldText = removeSpace(text)
        
        if textFieldText.first == " " {
            textFieldText = String(textFieldText.dropFirst())
        }
        
        if textFieldText.last == " " {
            textFieldText = String(textFieldText.dropLast())
        }
        
        let floatUnitQty = NSString.init(string: textFieldText).floatValue
        
        if textFieldText.components(separatedBy: "/").count > 1 {
            if let f = textFieldText.components(separatedBy: "/").first, let l = textFieldText.components(separatedBy: "/").last {
                if f != "" && l != "" {
                    if f.components(separatedBy: " ").count > 1 {
                        if let _ = f.components(separatedBy: " ").first?.integer {
                            if let _ = f.components(separatedBy: " ").last?.integer {
                                return true
                            }
                        }
                    }
                }
            }
        }
        return floatUnitQty > 1
    }
    return false
}
