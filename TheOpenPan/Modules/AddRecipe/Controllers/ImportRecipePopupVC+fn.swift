//
//  ImportRecipePopupVC+fn.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/21/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation
import UIKit
import SwiftSoup

let api_key = "df497cd83df70ac7d829c273499767a6"
let api_id = "5f800800"


extension ImportRecipePopupVC {
    
    func isDictionaryOfRecipe(jsonDict:[String:Any]) -> Bool {
        let arrStringAllKeys = jsonDict.keys
        
        var foundSimilar = 1
        
        for strKey in arrStringAllKeys {
            
            let strKeyLowercase = strKey.lowercased()
            
            if strKeyLowercase.contains("ingredients") {
                foundSimilar += 1
            }
            if strKeyLowercase.contains("ingredient") {
                foundSimilar += 1
            }
            if strKeyLowercase.contains("instructions") {
                foundSimilar += 1
            }
            if strKeyLowercase.contains("instruction") {
                foundSimilar += 1
            }
            if strKeyLowercase.contains("directions") {
                foundSimilar += 1
            }
            if strKeyLowercase.contains("direction") {
                foundSimilar += 1
            }
            
            if strKeyLowercase.contains("preptime") {
                foundSimilar += 1
            }
            if strKeyLowercase.contains("preparationtime") {
                foundSimilar += 1
            }
            
        }
        
        if foundSimilar >= 2 {
            return true
        }
        return false
    }
    
    //    func getRecipeIngredientsData(dictAllData:[String:Any],completionBlock:(([[String:Any]]?)->())?) {
    //
    //        var stringToSend = ""
    //
    //        let arrStringAllKeys = dictAllData.keys
    //
    //        for strKey in arrStringAllKeys {
    //
    //            let strKeyLowercase = strKey.lowercased()
    //
    //            if strKeyLowercase.contains("ingredients") {
    //                if let ingredients = dictAllData[strKey] as? [String:Any] {
    //                    print(ingredients)
    //                }
    //            }
    //
    //            if strKeyLowercase.contains("ingredient") {
    //                if let dictIngredients = dictAllData[strKey] as? [String:Any] {
    //                    print(dictIngredients)
    //                }
    //                if let arrStringIngredients = dictAllData[strKey] as? [String] {
    //                    stringToSend = arrStringIngredients.joined(separator: "\n")
    //                    print(arrStringIngredients)
    //                }
    //                if let strIngredients = dictAllData[strKey] as? String {
    //                    print(strIngredients)
    //                }
    //            }
    //
    //        }
    //
    //
    //        let headers = [
    //            "content-type": "text/plain",
    //            "accept": "text/plain"
    //        ]
    //
    //        let postData = NSData(data: stringToSend.data(using: String.Encoding.utf8)!)
    //
    //        let request = NSMutableURLRequest(url: NSURL(string: "https://cookalo.com/v2/recipe")! as URL,
    //                                                cachePolicy: .useProtocolCachePolicy,
    //                                            timeoutInterval: 10.0)
    //        request.httpMethod = "POST"
    //        request.allHTTPHeaderFields = headers
    //        request.httpBody = postData as Data
    //
    //        let session = URLSession.shared
    //        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
    //
    //            DispatchQueue.main.sync {
    //                if (error != nil) {
    //
    //                    print(error)
    //                    completionBlock?(nil)
    //                } else {
    //                    let httpResponse = response as? HTTPURLResponse
    //                    print(httpResponse)
    //
    //                    if let data = data {
    //                        do {
    //                            print(String(data: data, encoding: String.Encoding.utf8) as? String)
    //                            if let arrayDictIngredients = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
    //
    //                                completionBlock?(arrayDictIngredients)
    //                            } else {
    //                                 completionBlock?(nil)
    //                            }
    //
    //                        } catch {
    //                            print(error.localizedDescription)
    //                             completionBlock?(nil)
    //                        }
    //                    } else {
    //                         completionBlock?(nil)
    //                    }
    //
    //                }
    //            }
    //
    //        })
    //
    //        dataTask.resume()
    //
    //    }
    
    
    func getRecipeIngredientsData_edamam(dictAllData:[String:Any],completionBlock:(([String:Any]?)->())?) {
        
        //        var stringToSend = ""
        
        let arrStringAllKeys = dictAllData.keys
        var arrIngredients = [String]()
        
        for strKey in arrStringAllKeys {
            
            let strKeyLowercase = strKey.lowercased()
            
            if strKeyLowercase.contains("ingredient") {
                if let dictIngredients = dictAllData[strKey] as? [String:Any] {
                    print(dictIngredients)
                }
                if let arrStringIngredients = dictAllData[strKey] as? [String] {
                    arrIngredients = arrStringIngredients
                    print(arrStringIngredients)
                }
                if let strIngredients = dictAllData[strKey] as? String {
                    print(strIngredients)
                }
            }
            
        }
        
        var mutArrIngredients = [String]()
        for ing in arrIngredients {
            let ingRemovedSymbols = self.replaceHtmlSymbols(strText: ing)
            let parsedIng = try? SwiftSoup.parse(ingRemovedSymbols)
            let finalIng = try? parsedIng?.text() ?? ""
            mutArrIngredients.append(finalIng ?? "")
        }
        
        let finalDict = ["ingr":mutArrIngredients]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: finalDict, options: .prettyPrinted) else {
            completionBlock?(nil)
            return
        }
        
        let headers = [
            "Content-Type": "application/json"
        ]
        //        Request URL: https://api.edamam.com/api/nutrition-details?app_id=47379841&app_key=d28718060b8adfd39783ead254df7f92
        
        var url = URL.init(string: "https://api.edamam.com/api/nutrition-details")
        url?.appending("app_id", value: "47379841")
        url?.appending("app_key", value: "d28718060b8adfd39783ead254df7f92")
        
        
        let request = NSMutableURLRequest(url: url!,
                                          cachePolicy: .reloadIgnoringCacheData,
                                          timeoutInterval: 70.0)
        request.httpMethod = "POST"
        
        request.allHTTPHeaderFields = headers
        
        
        let json = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        
        //        request.httpBody = jsonData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            DispatchQueue.main.sync {
                if (error != nil) {
                    
                    print(error?.localizedDescription ?? "")
                    completionBlock?(nil)
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    print(httpResponse as Any)
                    
                    if let data = data {
                        do {
                            print(String(data: data, encoding: String.Encoding.utf8) as Any)
                            if let arrayDictIngredients = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                                
                                completionBlock?(arrayDictIngredients)
                            } else {
                                completionBlock?(nil)
                            }
                            
                        } catch {
                            print(error.localizedDescription)
                            completionBlock?(nil)
                        }
                    } else {
                        completionBlock?(nil)
                    }
                    
                }
            }
            
        })
        
        dataTask.resume()
        
    }
    
    
    func parseIngredientsApiCall(dictAllData:[String:Any]) {
        
        let arrStringAllKeys = dictAllData.keys
        var arrIngredients = [String]()
        
        for strKey in arrStringAllKeys {
            
            let strKeyLowercase = strKey.lowercased()
            
            if strKeyLowercase.contains("ingredient") {
                if let dictIngredients = dictAllData[strKey] as? [String:Any] {
                    print(dictIngredients)
                }
                if let arrStringIngredients = dictAllData[strKey] as? [String] {
                    arrIngredients = arrStringIngredients
                    print(arrStringIngredients)
                }
                if let strIngredients = dictAllData[strKey] as? String {
                    print(strIngredients)
                }
            }
            
        }
        
        var mutArrIngredients = [String]()
        for ing in arrIngredients {
            let ingRemovedSymbols = self.replaceHtmlSymbols(strText: ing)
            let parsedIng = try? SwiftSoup.parse(ingRemovedSymbols)
            let finalIng = try? parsedIng?.text() ?? ""
            mutArrIngredients.append(finalIng ?? "")
        }
        
        //Ingredient_list

        guard let jsonDataArrIngredients = try? JSONSerialization.data(withJSONObject: mutArrIngredients,options: []) else {
            self.checkVideoAndDownload()
            return
        }
        
        guard let textArrIngredienta = String(data: jsonDataArrIngredients, encoding: .utf8) else {
            self.checkVideoAndDownload()
            return
        }
        print("JSON string textArrIngredienta = \(textArrIngredienta)")
        
        getIngredientsParsing(ingredients: textArrIngredienta) { (res) in
            switch res {
            case .success(dictData: let dictData, message: _) :
                
            
                print(dictData)
                if let dictIngredientList = dictData["ingredient_list"] as? [[String:Any]] {
                    var a = [Ingredient_list]()
                    for dictIngredient in dictIngredientList {
                        
                        let name            = dictIngredient["ingredient"] as? String ?? ""
                        let unit_name       = dictIngredient["unit"] as? String ?? ""
                        let qty             = dictIngredient["quantity"] as? String ?? ""
                        
                        let objIngredient = Ingredient_list.init(id: 0, unit_name: unit_name, name: name, note: "", qty: qty, isSelected: false)
                        a.append(objIngredient)
                    }
                    self.addNewRecipeData.ingredient_list = a
                    self.checkVideoAndDownload()
                } else {
                    self.checkVideoAndDownload()
                    BasicFunctions.showError(strError: "Something went wrong!".localize())
                }
                
//                guard let dictRes = formattedResponseDict else {
//                    self.checkVideoAndDownload()
//                    return
//                }
                
                break
            case .failed(message: let msg):
                self.checkVideoAndDownload()
                BasicFunctions.showError(strError: msg)
                break
            case .error(error: let error):
                self.checkVideoAndDownload()
                BasicFunctions.showError(strError: error)
                break
            }
        }
        
        
    }
    
    
    
    func getAllData(jsonDict:[String:Any]) {
        print(jsonDict)
        
        
        
        //image
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("image") {
                
                if let arrStringUrlImages = jsonDict[key] as? [String] {
                    for strUrlImage in arrStringUrlImages {
                        if let url = URL.init(string: strUrlImage) {
                            saveRecipeImageDataToLocal(url:url)
                            break
                        }
                    }
                }
                
                if let stringUrlImages = jsonDict[key] as? String {
                    
                    if let url = URL.init(string: stringUrlImages) {
                        saveRecipeImageDataToLocal(url:url)
                        break
                    }
                }
                
                
                if let dictUrlImages = jsonDict[key] as? [String:Any] {
                    
                    let keyFilered = dictUrlImages.keys.filter { (key) -> Bool in
                        return key.contains("url")
                    }
                    
                    if keyFilered.count > 0 {
                        if let strUrl = dictUrlImages[keyFilered[0]] as? String {
                            if let url = URL.init(string: strUrl) {
                                saveRecipeImageDataToLocal(url:url)
                                break
                            }
                        }
                    }
                }
                
                break
            }
            if lowercasedKey.contains("img") {
                
                break
            }
        }
        
        
        
        
        
        //Category
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("category") {
                if let arrCourse = (jsonDict[key] as? [String]) {
                    //                    self.dictMain["course"] = arrCourse.first ?? ""
                }
            }
        }
        //Cuisine
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            if lowercasedKey.contains("cuisine") {
                if let arrCuisine = (jsonDict[key] as? [String]) {
                    //                    self.dictMain["cuisine"] = arrCuisine.first ?? ""
                }
            }
        }
        
        
        
        //Name/Title
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("name") {
                let htmlStringDescription = (jsonDict[key] as? String) ?? ""
                self.addNewRecipeData.title = htmlStringDescription
                //                self.dictMain["cuisine"] = htmlStringDescription
                //                cell.txtViewTitle.attributedText = try? NSAttributedString.init(HTMLString: htmlStringDescription, font:  UIFont.systemFont(ofSize: fontPointSize_15))
                break
            }
            if lowercasedKey.contains("title") {
                let htmlStringDescription = (jsonDict[key] as? String) ?? ""
                self.addNewRecipeData.title = htmlStringDescription
                //                self.dictMain["cuisine"] = htmlStringDescription
                //                cell.txtViewTitle.attributedText = try? NSAttributedString.init(HTMLString: htmlStringDescription, font:  UIFont.systemFont(ofSize: fontPointSize_15))
                break
            }
        }
        
        //Description
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("description") {
                
                let htmlStringDescription = (jsonDict[key] as? String) ?? ""
                //                self.dictMain["desc"] = htmlStringDescription
                //                cell.txtViewDesc.attributedText = try? NSAttributedString.init(HTMLString: htmlStringDescription, font:  UIFont.systemFont(ofSize: fontPointSize_15))
                
                break
            }
            if lowercasedKey.contains("desc") {
                
                let htmlStringDescription = (jsonDict[key] as? String) ?? ""
                //                self.dictMain["desc"] = htmlStringDescription
                //                cell.txtViewDesc.attributedText = try? NSAttributedString.init(HTMLString: htmlStringDescription, font:  UIFont.systemFont(ofSize: fontPointSize_15))
                
                break
            }
            if lowercasedKey.contains("descriptions") {
                let htmlStringDescription = (jsonDict[key] as? String) ?? ""
                //                self.dictMain["desc"] = htmlStringDescription
                //                cell.txtViewDesc.attributedText = try? NSAttributedString.init(HTMLString: htmlStringDescription, font:  UIFont.systemFont(ofSize: fontPointSize_15))
                break
            }
        }
        
        
        
        //Video
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("video") {
                if let arrVideo = (jsonDict[key] as? [[String:Any]]) {
                    if let f = arrVideo.first {
                        if let contentUrl = f["contentUrl"] as? String {
                            print(contentUrl)
                            self.addNewRecipeData.imported_media_url    = contentUrl
                            self.addNewRecipeData.media_url             = contentUrl
                            self.addNewRecipeData.is_video              = true.mk_intValue
                        }
                        if let thumbnailUrl = f["thumbnailUrl"] as? String {
                            print(thumbnailUrl)
                            self.addNewRecipeData.imported_video_thumb_url      = thumbnailUrl
                            self.addNewRecipeData.video_thumb_url               = thumbnailUrl
                        } else {
                            if let contentUrl = f["contentUrl"] as? String {
                                print(contentUrl)
                                if let url = URL.init(string: contentUrl) {
                                    let thumbnailImage = Utilities.generateThumbnail(path: url)
                                    if let filePath = thumbnailImage.mk_saveToDocuments(filename: String(Date().mk_ticks), folderName: tmpDocFolderName) {
                                        self.addNewRecipeData.imported_video_thumb_url      = filePath
                                        self.addNewRecipeData.video_thumb_url               = filePath
                                    }
                                }
                            }
                        }
                    }
                }
                
                
                if let vidObject = (jsonDict[key] as? [String:Any]) {
                    
                    if let contentUrl = vidObject["contentUrl"] as? String {
                        print(contentUrl)
                        if contentUrl != "" {
                            self.addNewRecipeData.imported_media_url    = contentUrl
                            self.addNewRecipeData.media_url             = contentUrl
                            self.addNewRecipeData.is_video              = true.mk_intValue
                        }
                    }
                    if let embedUrl = vidObject["embedUrl"] as? String {
                        print(embedUrl)
                        if embedUrl != "" {
                            self.addNewRecipeData.imported_media_url    = embedUrl
                            self.addNewRecipeData.media_url             = embedUrl
                            self.addNewRecipeData.is_video              = true.mk_intValue
                        }
                    }
                    
                    if let thumbnailUrl = vidObject["thumbnailUrl"] as? String {
                        print(thumbnailUrl)
                        
                        if thumbnailUrl != "" {
                            self.addNewRecipeData.imported_video_thumb_url      = thumbnailUrl
                            self.addNewRecipeData.video_thumb_url               = thumbnailUrl
                        }
                        
                    } else {
                        if let contentUrl = vidObject["contentUrl"] as? String {
                            print(contentUrl)
                            if let url = URL.init(string: contentUrl) {
                                let thumbnailImage = Utilities.generateThumbnail(path: url)
                                if let filePath = thumbnailImage.mk_saveToDocuments(filename: String(Date().mk_ticks), folderName: tmpDocFolderName) {
                                    self.addNewRecipeData.imported_video_thumb_url      = filePath
                                    self.addNewRecipeData.video_thumb_url               = filePath
                                }
                            }
                        }
                    }
                    
                }
                
            }
        }
        
        
        
        
        
        
        
        var cookTime : (String?,String?)!
        var prepTime : (String?,String?)!
        var totalTime : (String?,String?)!
        
        
        //Cooktime
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("cooktime") {
                
                cookTime = self.getHoursAndMins(strDate: ((jsonDict[key] as? String) ?? ""))
                
                break
            }
            if lowercasedKey.contains("cook time") {
                
                cookTime = self.getHoursAndMins(strDate: ((jsonDict[key] as? String) ?? ""))
                
                break
            }
            if lowercasedKey.contains("cktime") {
                
                cookTime = self.getHoursAndMins(strDate: ((jsonDict[key] as? String) ?? ""))
                
                break
            }
            
        }
        
        
        //Preptime
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("preptime") {
                
                prepTime = self.getHoursAndMins(strDate: ((jsonDict[key] as? String) ?? ""))
                
                break
            }
            if lowercasedKey.contains("preparationtime") {
                
                prepTime = self.getHoursAndMins(strDate: ((jsonDict[key] as? String) ?? ""))
                
                break
            }
            if lowercasedKey.contains("prep time") {
                
                prepTime = self.getHoursAndMins(strDate: ((jsonDict[key] as? String) ?? ""))
                
                break
            }
            
        }
        
        //totaltime
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("totaltime") {
                
                totalTime = self.getHoursAndMins(strDate: ((jsonDict[key] as? String) ?? ""))
                
                break
            }
            
        }
        
        
        if totalTime != nil {
            
            if let totalHours = totalTime.0?.integer {
                self.addNewRecipeData.ready_time_hours = totalHours
            }
            if let totalMins = totalTime.1?.integer {
                self.addNewRecipeData.ready_time_mins = totalMins
            }
            
        } else {
            
            if cookTime != nil && prepTime != nil {
                
                //cookTime+prepTime final
                if let cook_totalHours = cookTime.0?.integer {
                    if let prep_totalHours = prepTime.0?.integer {
                        self.addNewRecipeData.ready_time_hours = cook_totalHours+prep_totalHours
                    }
                }
                if let cook_totalMins = cookTime.1?.integer {
                    if let prep_totalMins = prepTime.1?.integer {
                        self.addNewRecipeData.ready_time_mins = cook_totalMins+prep_totalMins
                    }
                }
                
            } else if cookTime != nil && prepTime == nil {
                
                //cookTime final
                if let totalHours = cookTime.0?.integer {
                    self.addNewRecipeData.ready_time_hours = totalHours
                }
                if let totalMins = cookTime.1?.integer {
                    self.addNewRecipeData.ready_time_mins = totalMins
                }
            } else {
                
                
                if prepTime != nil {
                    
                    //prepTime final
                    if let totalHours = prepTime.0?.integer {
                        self.addNewRecipeData.ready_time_hours = totalHours
                    }
                    if let totalMins = prepTime.1?.integer {
                        self.addNewRecipeData.ready_time_mins = totalMins
                    }
                    
                }
                
                
            }
        }
        
        
        //serves
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            if lowercasedKey.contains("yield") {
                let strServes = (jsonDict[key] as? String) ?? ""
                self.handleYields(strServes: strServes)
            }
            
        }
        
        var arrDirections = [String]()
        
        //recipeInstructions
        
        for key in jsonDict.keys {
            
            let lowercasedKey = key.lowercased()
            
            if lowercasedKey.contains("instruction") {
                
                arrDirections = [(jsonDict[key] as? String) ?? ""]
                if let arr = handleDirectionsOfRecipe(keyOfDirection: key, jsonDict: jsonDict) {
                    arrDirections = arr
                }
                if let arrStrings = (jsonDict[key] as? [String]) {
                    arrDirections = arrStrings
                }
                if let str = (jsonDict[key] as? String) {
                    arrDirections = [str]
                }
                
                break
            }
            
            if lowercasedKey.contains("direction") {
                if let arr = handleDirectionsOfRecipe(keyOfDirection: key, jsonDict: jsonDict) {
                    arrDirections = arr
                }
                break
            }
        }
        
        
        var arrDirection_list = [Direction_list]()
        for (i,strDirection) in arrDirections.enumerated() {
            let objDirection_list = Direction_list.init(step: i+1, description: strDirection)
            arrDirection_list.append(objDirection_list)
        }
        self.addNewRecipeData.direction_list = arrDirection_list
        
        
        print(self.addNewRecipeData.direction_list)
        
//        self.addNewRecipeData.direction_list.removeObjectFromArray("")
//        self.addNewRecipeData.direction_list.removeObjectFromArray("")
//        self.addNewRecipeData.direction_list.removeObjectFromArray("")
//        self.addNewRecipeData.direction_list.removeObjectFromArray("")
//        for d in self.addNewRecipeData.direction_list {
//            if d.description == " " {
//
//            }
//        }
        
        
        
        
//        "keywords": "Cheese Pizza recipe, Italian, cook Cheese Pizza, Date,Birthday,Anniversary,Kitty Party,Summer,Winter,Buffet",
//        "recipeYield": "4 Servings",
//        "recipeCategory": "Lunch",
//        "recipeCuisine": "Italian",
        
        
        // keywords
        
        var arrKeywords = [Keyword]()
        
        for key in jsonDict.keys {
            let lowercasedKey = key.lowercased()
            if lowercasedKey.contains("keywords") {
                if let str = (jsonDict[key] as? String) {
                    for stringKeyword in str.components(separatedBy: ",") {
                        if let objKeyword = Keyword.mr_createEntity(in: app_default_context) {
                            print(stringKeyword)
                            print(stringKeyword.htmlDecoded)
                            objKeyword.desc = stringKeyword.htmlDecoded
                            arrKeywords.insert(objKeyword, at: 0)
                        }
                    }
                }
                break
            }
        }
        
        self.arrImportedKeywords = arrKeywords
        
        
        // Ratings and rating's count
        
        var dictAggregateRating = [String:Any]()
        
        for key in jsonDict.keys {
            let lowercasedKey = key.lowercased()
            if lowercasedKey.contains("aggregaterating") {
                if let dict = (jsonDict[key] as? [String:Any]) {
                   dictAggregateRating = dict
                }
                break
            }
        }
        
        
        print("aggregaterating \(dictAggregateRating)")
        
        if let ratingValue = dictAggregateRating["ratingValue"] as? String {
            self.addNewRecipeData.aggregateRatingValue = ratingValue
        }
        
        if let ratingValue = dictAggregateRating["ratingValue"] as? Double {
            self.addNewRecipeData.aggregateRatingValue = "\(ratingValue)"
        }
        
        
        if let reviewCount = dictAggregateRating["reviewCount"] as? String {
            self.addNewRecipeData.aggregateRatingCount = reviewCount
        } else {
            if let ratingCount = dictAggregateRating["ratingCount"] as? String {
                self.addNewRecipeData.aggregateRatingCount = ratingCount
            }
        }
        
        if let reviewCount = dictAggregateRating["reviewCount"] as? Int {
            self.addNewRecipeData.aggregateRatingCount = "\(reviewCount)"
        } else {
            if let ratingCount = dictAggregateRating["ratingCount"] as? Int {
                self.addNewRecipeData.aggregateRatingCount = "\(ratingCount)"
            }
        }
        
        print("------------------------------------------")
        print("RatingValue : \(self.addNewRecipeData.aggregateRatingValue)")
        print("ReviewCount : \(self.addNewRecipeData.aggregateRatingCount)")
        print("------------------------------------------")
        
        
        
        // Review's comments
        
        
        var arrDictReviews = [[String:Any]]()
        
        for key in jsonDict.keys {
            let lowercasedKey = key.lowercased()
            if lowercasedKey.contains("review") {
                if let dict = (jsonDict[key] as? [String:Any]) {
                    arrDictReviews.append(dict)
                }
                if let arrDict = (jsonDict[key] as? [[String:Any]]) {
                    arrDictReviews.append(contentsOf: arrDict)
                }
                break
            }
        }
        
        print(arrDictReviews)
        
//        datePublished
//        author
//        reviewBody
//        ratingValue
        
        var arrReviewModel = [ReviewModel]()
        
        for dictReview in arrDictReviews {
            var reviewModel = ReviewModel()
            for (key,_) in dictReview {
                if let arrObjects = dictReview[key] as? [[String:Any]] {
                    for dictSub in arrObjects {
                        reviewModel = fillDataInReviewModel(reviewModelSent: reviewModel, dictReview: dictSub)
                    }
                }
                if let dictObject = dictReview[key] as? [String:Any] {
                    reviewModel = fillDataInReviewModel(reviewModelSent: reviewModel, dictReview: dictObject)
                }
                
            }
            reviewModel = fillDataInReviewModel(reviewModelSent: reviewModel, dictReview: dictReview)
            
            if let datePublishedString = reviewModel.datePublishedString {
                if let date = convertDateFormat(inputDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", inputDate: datePublishedString) {
                    reviewModel.datePublished = date
                }
                if let date = convertDateFormat(inputDateFormat: "yyyy-MM-dd", inputDate: datePublishedString) {
                    reviewModel.datePublished = date
                }
            }
            
            
            arrReviewModel.append(reviewModel)
        }
        
        print(arrReviewModel)

        
        // recipeCuisine
        
        var arrCuisineTags = [Category_tag]()
        
        for key in jsonDict.keys {
            let lowercasedKey = key.lowercased()
            if lowercasedKey.contains("cuisine") {
                if let str = (jsonDict[key] as? String) {
                    if str.contains(",") {
                        for stringCuisine in str.components(separatedBy: ",") {
                            for tag in enum_tag_list_type.cusineType.getAllTags() {
                                if let tag_name = tag.tag_name {
                                    if stringCuisine.lowercased().contains(tag_name.lowercased()) {
                                        arrCuisineTags.append(tag)
                                    } else {
                                        if tag_name.lowercased().contains(stringCuisine.lowercased()) {
                                            arrCuisineTags.append(tag)
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        for tag in enum_tag_list_type.cusineType.getAllTags() {
                            if let tag_name = tag.tag_name {
                                if str.lowercased().contains(tag_name.lowercased()) {
                                    arrCuisineTags.append(tag)
                                } else {
                                    if tag_name.lowercased().contains(str.lowercased()) {
                                        arrCuisineTags.append(tag)
                                    }
                                }
                            }
                        }
                    }
                }
                break
            }
        }
        
        self.arrImportedCuisineTags = arrCuisineTags

        
        //recipeCategory //Meal types
        
        var arrMealTypeTags = [Category_tag]()
        
        for key in jsonDict.keys {
            let lowercasedKey = key.lowercased()
            if lowercasedKey.contains("category") {
                if let str = (jsonDict[key] as? String) {
                    if str.contains(",") {
                        for stringCuisine in str.components(separatedBy: ",") {
                            for tag in enum_tag_list_type.mealType.getAllTags() {
                                if let tag_name = tag.tag_name {
                                    if stringCuisine.lowercased().contains(tag_name.lowercased()) {
                                        arrMealTypeTags.append(tag)
                                    } else {
                                        if tag_name.lowercased().contains(stringCuisine.lowercased()) {
                                            arrMealTypeTags.append(tag)
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        for tag in enum_tag_list_type.mealType.getAllTags() {
                            if let tag_name = tag.tag_name {
                                if str.lowercased().contains(tag_name.lowercased()) {
                                    arrMealTypeTags.append(tag)
                                } else {
                                    if tag_name.lowercased().contains(str.lowercased()) {
                                        arrMealTypeTags.append(tag)
                                    }
                                }
                            }
                        }
                    }
                }
                break
            }
        }
        
        self.arrImportedMealTypeTags = arrMealTypeTags
        
        
        
        
        
        
        
        //        print(jsonDict.keys)
        //        print(jsonDict)
        
        //        print(dictMain)
        
        //let date = Formatter.iso8601.date(from: jsonDict["cookTime"] as! String)
        //print(date)
        
        
        //let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "yyyyMMdd'T'HHmmssZ"
        //print(dateFormatter.date(from: jsonDict["cookTime"] as! String)) //2018-02-07 12:46:00 +0000
        
        
        // let dateString = jsonDict["cookTime"] ?? "20180207T124600Z"
        //  if #available(iOS 10.0, *) {
        
        //      let dateFormatter = ISO8601DateFormatter()
        //       dateFormatter.timeZone = .none
        //       dateFormatter.formatOptions = [
        //           .withTime
        //      ]
        //20191218T062631
        //            print(dateFormatter.string(from: Date()))
        //            print(dateFormatter.date(from: dateFormatter.string(from: Date()) as! String))
        //     let date = dateFormatter.date(from: dateString as! String)
        //       print(dateString)
        //        print(date)
        
        //        print("Hours : \(Calendar.current.component(.hour, from: date!))")
        //        print("Minutes : \(Calendar.current.component(.minute, from: date!))")
        
        //            NSDate *date = [NSDate date];
        //            NSCalendar *calendar = [NSCalendar currentCalendar];
        //            NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
        //            NSInteger hour = [components hour];
        //            NSInteger minute = [components minute]
        
        //  } else {
        // Fallback on earlier versions
        //    }
        
        
    }
    
    
    func saveRecipeImageDataToLocal(url:URL) {
        if let recipeImageData = try? Data.init(contentsOf:url) {
            if let recipeImage = UIImage.init(data: recipeImageData) {
                if let imageLocalUrl = recipeImage.mk_saveToDocuments(filename: "\(Date().mk_ticks).jpg", folderName: tmpDocFolderName) {
                    self.addNewRecipeData.media_url                 = imageLocalUrl
                    self.addNewRecipeData.is_video                  = false.mk_intValue
                    self.addNewRecipeData.image_temp_to_crop        = recipeImage
                }
            }
        }
    }
    
    
    func handleDirectionsOfRecipe(keyOfDirection:String, jsonDict:[String:Any]) -> [String]? {
        
        
        var arrDirection = [String]()
        
        if let arrDictDirections = jsonDict[keyOfDirection] as? [[String:Any]] {
            
            for (_,dict) in arrDictDirections.enumerated() {
                
                for key in dict.keys {
                    
                    let keyLowercased = key.lowercased()
                    if keyLowercased.contains("text") {
                        
                        arrDirection.append(("\(((dict[key] as? String) ?? ""))"))
                        
                    } else {
                        
                        if let arrTexts = dict[key] as? [[String:Any]] {
                            
                            for key2 in dict.keys {
                                if key2.lowercased().contains("name") {
                                    if let strName = dict[key2] as? String {
                                        print(strName)
                                        arrDirection.append(strName)
                                    }
                                }
                            }
                            
                            for (_,subDictText) in arrTexts.enumerated() {
                                for key in subDictText.keys {
                                    let keyLowercased = key.lowercased()
                                    if keyLowercased.contains("text") {
                                        arrDirection.append(((subDictText[key] as? String) ?? ""))
                                    }
                                }
                            }
                        }
                        
                    }
                }
                
            }
            
            return arrDirection
        }
        
        if let dictDirections = jsonDict[keyOfDirection] as? [String:Any] {
            
            for (_,key) in dictDirections.keys.enumerated() {
                
                let keyLowercased = key.lowercased()
                if keyLowercased.contains("text") {
                    arrDirection.append((dictDirections[key] as? String) ?? "")
                }
                
            }
            
        }
        
        if let dictDirections = jsonDict[keyOfDirection] as? [String] {
            
            return dictDirections
            
        }
        
        return arrDirection
    }
    
    
    func fillDataInReviewModel(reviewModelSent:ReviewModel, dictReview:[String:Any]) -> ReviewModel {
        var reviewModel = reviewModelSent
        
        if let authorReview = dictReview["author"] as? String {
            reviewModel.author = authorReview
        }
        
        if let authorDictReview = dictReview["author"] as? [String:Any] {
            if let authorReview = authorDictReview["name"] as? String {
                reviewModel.author = authorReview
            }
        }
        
        if let datePublished = dictReview["datePublished"] as? String {
            reviewModel.datePublishedString = datePublished
        }
        
        if let reviewBody = dictReview["reviewBody"] as? String {
            reviewModel.reviewBody = reviewBody
        }
        
        if let ratingValue = dictReview["ratingValue"] as? String {
            reviewModel.ratingValue = ratingValue
        }
        
        if let bestRating = dictReview["bestRating"] as? String {
            reviewModel.bestRating = bestRating
        }
        
        if let worstRating = dictReview["worstRating"] as? String {
            reviewModel.worstRating = worstRating
        }
        
        if let ratingValue = dictReview["ratingValue"] as? Double {
            reviewModel.ratingValue = "\(ratingValue)"
        }
        
        if let bestRating = dictReview["bestRating"] as? Double {
            reviewModel.bestRating = "\(bestRating)"
        }
        
        if let worstRating = dictReview["worstRating"] as? Double {
            reviewModel.worstRating = "\(worstRating)"
        }
        
        return reviewModel
    }
    
}









extension URL {
    
    mutating func appending(_ queryItem: String, value: String?) {
        
        guard var urlComponents = URLComponents(string: absoluteString) else { return }
        
        // Create array of existing query items
        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []
        
        // Create query item
        let queryItem = URLQueryItem(name: queryItem, value: value)
        
        // Append the new query item in the existing query items array
        queryItems.append(queryItem)
        
        // Append updated query items array in the url component object
        urlComponents.queryItems = queryItems
        
        // Returns the url from new url components
        self = urlComponents.url!
        //        return urlComponents.url!
    }
}








//////////////// HTML HANDLE

extension ImportRecipePopupVC {
    
    func getHoursAndMins(strDate:String? = nil, itempropname:String? = nil) -> (String?,String?) {
        
        if let strDate = strDate {
            
            if strDate.contains("PT") {
                return strDate.getHoursMinutes()
            } else {
                // Starting with P // P0Y0M0DT0H5M0.000S
                if strDate.hasPrefix("P") && strDate.contains(".") {
                    let arrTime = strDate.components(separatedBy: ".")
                    if let timeValue = arrTime.first {
                        return timeValue.getHoursMinutes()
                    }
                }
            }
            
        }
        
        do {
            if let itempropname = itempropname {
                
                let arrTime = try self.doc.getElementsByAttributeValue("itemprop", itempropname)
                
                
                if let arrAttributes = arrTime.array().first?.getAttributes()?.asList() {
                    for attr in arrAttributes {
                        if attr.getValue().contains("PT") {
                            return attr.getValue().getHoursMinutes()
                        } else {
                            // Starting with P // P0Y0M0DT0H5M0.000S
                            
                            if attr.getValue().hasPrefix("P") && attr.getValue().contains(".") {
                                
                                let arrTime = attr.getValue().components(separatedBy: ".")
                                
                                if let timeValue = arrTime.first {
                                    return timeValue.getHoursMinutes()
                                }
                            }
                        }
                    }
                }
                
                //                if txtHours.text == "" && txtMinutes.text == "" {
                //                    if let timeFound = arrTime.array().first {
                //                        txtHours.text = try timeFound.text()
                //                    } else {
                //                        txtHours.text = ""
                //                        txtMinutes.text = ""
                //                    }
                //                }
            }
            
            
        } catch {
            print(error)
            return (nil,nil)
        }
        return (nil,nil)
    }
    
    
    func handleTime() {
        
        do {
            
            let prepTime = self.getHoursAndMins(itempropname: "prepTime")
            let cookTime = self.getHoursAndMins(itempropname: "cookTime")
            let totalTime = self.getHoursAndMins(itempropname: "totalTime")
            
            if let yieldFound = try self.doc.getElementsByAttributeValue("itemprop", "recipeYield").array().first {
                let serve = try yieldFound.text()
            } else {
                let serve = ""
            }
            
        } catch {
            print(error)
        }
        
    }
}




extension ImportRecipePopupVC {
    
    func handleYields(strServes:String) {
        
        let stringArray = strServes.components(separatedBy: CharacterSet.decimalDigits.inverted)
        var arrInts = [Int]()
        for item in stringArray {
            if let number = Int(item) {
                print("number: \(number)")
                arrInts.append(number)
            }
        }
        
        if let f = arrInts.first {
            self.addNewRecipeData.serves = f
        }
        
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        
        print("-------++++++++------- : \(strServes)")
        
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
        print("-------++++++++-------")
   
    }
}


struct ReviewModel {
    var author                  : String?
    var reviewBody              : String?
    var datePublishedString     : String?
    var datePublished           : Date?
    var ratingValue             : String?
    var bestRating              : String?
    var worstRating             : String?
}


func GetOnlyDateMonthYearFromFullDate(currentDateFormate:String , conVertFormate:String , convertDate:String ) -> (Date?,String?)
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = currentDateFormate
    let formatter = DateFormatter()
    formatter.dateFormat = conVertFormate
    let finalDate = formatter.date(from: convertDate)
    formatter.dateFormat = conVertFormate
    let dateString = formatter.string(from: finalDate!)
    return (finalDate,dateString)
}

func convertDateFormat(inputDateFormat:String, inputDate: String) -> Date? {

//     let oldDateFormatter = DateFormatter()
//     oldDateFormatter.dateFormat = inputDateFormat
     let convertDateFormatter = DateFormatter()
     convertDateFormatter.dateFormat = inputDateFormat
     let newDate = convertDateFormatter.date(from: inputDate)
     return newDate
}

extension String {
    var htmlDecoded: String {
        let decoded = try? NSAttributedString(data: Data(utf8), options: [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ], documentAttributes: nil).string

        return decoded ?? self
    }
}
