//
//  AddNewRecipeData.swift
//  TheOpenPan
//
//  Created by Team Peerbits on 1/23/20.
//  Copyright © 2020 peerbits. All rights reserved.
//

import Foundation

struct AddNewRecipeData {
    
    var title                           = String()
    var is_video                        = Int()
    var media_url                       = String()
    var video_thumb_url                 = String()
    var imported_media_url              = String()
    var imported_video_thumb_url        = String()
    
    var ready_time_hours                : Int?
    var ready_time_mins                 : Int?
    var serves                          = Int()
    var direction_list                  = [Direction_list]()
    var ingredient_list                 = [Ingredient_list]()
    
    var is_easy                         : Bool = false
    var submit_recipe_for_review        : Bool = false
    var is_one_pot                      : Bool = false
    var is_own_recipe                   : Bool = false
    var web_recipe_url                  = String()
    var collection_id                   = Int64()
    var recipe_status                   = Int()
    var live_media_url                  = String()
    var live_video_thumb_url            = String()
    
    var image_temp_to_crop              : UIImage!
    
    var aggregateRatingValue            = String()
    var aggregateRatingCount            = String()
    
    
}

struct Direction_list {
    var step                 = Int()
    var description          = String()
}

struct Ingredient_list {
    var id                  = Int64()
    var unit_name           = String()
    var name                = String()
    var note                = String()
    var qty                 = String()
    var isSelected          = false
}


